package pages;


import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.MGService_Info;
import configuration.DriverConfig.DriverBase;
import custom_Func.Data_Optimize;

public class Transf_Mortgage_details extends PageObjects{
	
	
	
	public Transf_Mortgage_details(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String MG_MortgAmount_txt_xpath= "//div[@org-placeholder='Mortgage Amount']//input";
	private  String MG_IndexRate_txt_xpath= "//div[@org-placeholder='Index Rate']//input";
	private  String MG_AmountClose_txt_xpath= "//div[@org-placeholder='Amount Needed at Closing']//input";
	private  String MG_DownPayment_txt_xpath= "//div[@org-placeholder='Down Payment']//input";
	private  String MG_LockDate_txt_xpath= "//div[@org-placeholder='Lock In Date']//input";
	private  String MG_CloseDate_txt_xpath= "//input[@id ='closing_date']";
	
	private  String Edit_btn_xpath= "//a[@data-original-title ='Edit']";
	private  String Update_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='update']";
	private  String Cancel_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='cancel-update']";
	

	
	public  WebElement MG_CloseDate_txt() {
		  return GetElement(MG_CloseDate_txt_xpath);
		 }
	
	
	
	public  WebElement MG_MortgAmount_txt() {
	  return GetElement(MG_MortgAmount_txt_xpath);
	 }
	public  WebElement MG_IndexRate_txt() {
	  return GetElement(MG_IndexRate_txt_xpath);
	 }
	public  WebElement MG_AmountClose_txt() {
	  return GetElement(MG_AmountClose_txt_xpath);
	 }
	public  WebElement MG_DownPayment_txt() {
	  return GetElement(MG_DownPayment_txt_xpath);
	 }
	public  WebElement MG_LockDate_txt() {
	  return GetElement(MG_LockDate_txt_xpath);
	 }
	
	public  WebElement Edit_btn() {
		  return GetElement(Edit_btn_xpath);
		 }
	
	public  WebElement Update_btn() {
		  return GetElement(Update_btn_xpath);
		 }
	public  WebElement Cancel_btn() {
	  return GetElement(Cancel_btn_xpath);
	 }

	
	public  void Verify_MGServiceDetail_func(MGService_Info service)
	{
		String field_name = "";

		field_name = "MG_MortgAmount_txt";
		String mg_amount = Data_Optimize.Format_Double_WithCurrency(service.MortgageAmount);
				
		VerifyFieldValueEqual_func(field_name,MG_MortgAmount_txt(), mg_amount,"");

		field_name = "MG_IndexRate_txt";
		VerifyFieldValueEqual_func(field_name,MG_IndexRate_txt(), service.IndexRate,"");

		field_name = "MG_AmountClose_txt";
		VerifyFieldValueEqual_func(field_name,MG_AmountClose_txt(), service.AmountAtClose,"");

		field_name = "MG_DownPayment_txt";
		VerifyFieldValueEqual_func(field_name,MG_DownPayment_txt(), service.DownPayment,"");

		field_name = "MG_CloseDate_txt";
		VerifyFieldValueEqual_func(field_name,MG_CloseDate_txt(), service.CloseDate,"");

		field_name = "MG_LockDate_txt";
		VerifyFieldValueEqual_func(field_name,MG_LockDate_txt(), service.LockInDate,"");
	}//end void
	
	public  void FillMortgageInfo_func(MGService_Info service,String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Edit_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();


		//WaitForElementDisplay(MG_BuyOutDate_txt())
		String mg_amount = Data_Optimize.Convert_Double_To_String_func(service.MortgageAmount, "");
				
		ClearThenEnterValueToField(MG_MortgAmount_txt(), mg_amount);

		ClearThenEnterValueToField(MG_IndexRate_txt(), service.IndexRate);

		service.AmountAtClose.replace("$","");
		service.AmountAtClose.replace(",","");
		service.AmountAtClose.replace(".","");
		ClearThenEnterValueToField(MG_AmountClose_txt(), service.AmountAtClose);

		service.DownPayment.replace("$","");
		service.DownPayment.replace(",","");
		service.DownPayment.replace(".","");
		ClearThenEnterValueToField(MG_DownPayment_txt(), service.DownPayment);

		Optimize_ElementSendkey(MG_CloseDate_txt(), service.CloseDate);

		Optimize_ElementSendkey(MG_LockDate_txt(), service.LockInDate);

		if(button.equals("Submit")||button.equals("submit"))
			Update_btn().click();
		else if(button.equals("Cancel")||button.equals("cancel"))
			Cancel_btn().click();

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "when update Mortgage_ServiceInfo",false);


	}//end void

}
