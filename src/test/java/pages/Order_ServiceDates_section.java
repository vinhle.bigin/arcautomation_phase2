package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;

public class Order_ServiceDates_section extends PageObjects{

	public Order_ServiceDates_section(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	//Service Dates section
	private  String ServiceDte_Edit_btn_xpath = "//a[@id='icon-hhg-order-servicedate-panel']";
	private  String ServiceDte_Update_btn_xpath = "//div[@id='hhg-order-servicedate-panel']//button[@data-control='update']";
	private  String ServiceDte_Cancel_btn_xpath = "//div[@id='hhg-order-servicedate-panel']//button[@data-control='cancel-update']";
		//Packing Date
	private  String PackDte_EstStart_txt_xpath = "//input[@id='hhgbase_service_date_packing_estimated_start']";
	private  String PackDte_ActStart_txt_xpath = "//input[@id='hhgbase_service_date_packing_actual_start']";
	private  String PackDte_EstEnd_txt_xpath = "//input[@id='hhgbase_service_date_packing_estimated_end']";
	private  String PackDte_ActEnd_txt_xpath = "//input[@id='hhgbase_service_date_packing_actual_end']";
		//Delivery to Storage
	private  String Storage_EstStart_txt_xpath = "//input[@id='hhgbase_service_date_delivery_storage_estimated_start']";
	private  String Storage_ActStart_txt_xpath = "//input[@id='hhgbase_service_date_delivery_storage_actual_start']";
	private  String Storage_EstEnd_txt_xpath = "//input[@id='hhgbase_service_date_delivery_storage_estimated_end']";
	private  String Storage_ActEnd_txt_xpath = "//input[@id='hhgbase_service_date_delivery_storage_actual_end']";
		//Delivery to Residence
	private  String Residence_EstStart_txt_xpath = "//input[@id='hhgbase_service_date_sdt_delivery_to_residence_estimated_start']";
	private  String Residence_ActStart_txt_xpath = "//input[@id='hhgbase_service_date_sdt_delivery_to_residence_actual_start']";
	private  String Residence_EstEnd_txt_xpath = "//input[@id='hhgbase_service_date_sdt_delivery_to_residence_estimated_end']";
	private  String Residence_ActEnd_txt_xpath = "//input[@id='hhgbase_service_date_sdt_delivery_to_residence_actual_end']";	
		//Sit Date
	private  String SitDte_EstStart_txt_xpath = "//input[@id='hhgbase_service_date_sit_estimated_start']";
	private  String SitDte_ActStart_txt_xpath = "//input[@id='hhgbase_service_date_sit_actual_start']";
	private  String SitDte_EstEnd_txt_xpath = "//input[@id='hhgbase_service_date_sit_estimated_end']";
	private  String SitDte_ActEnd_txt_xpath = "//input[@id='hhgbase_service_date_sit_actual_end']";	
		//Delivery Date
	private  String DeliDte_EstStart_txt_xpath = "//input[@id='hhgbase_service_date_delivery_estimated_start']";
	private  String DeliDte_ActStart_txt_xpath = "//input[@id='hhgbase_service_date_delivery_actual_start']";
	private  String DeliDte_EstEnd_txt_xpath = "//input[@id='hhgbase_service_date_delivery_estimated_end']";
	private  String DeliDte_ActEnd_txt_xpath = "//input[@id='hhgbase_service_date_delivery_actual_end']";	

	
	
	public  WebElement ServiceDte_Edit_btn() {
		return GetElement(ServiceDte_Edit_btn_xpath);
	 }
	
	public  WebElement PackDte_EstStart_txt() {
		return GetElement(PackDte_EstStart_txt_xpath);
	 }
	
	public  WebElement PackDte_ActStart_txt() {
		return GetElement(PackDte_ActStart_txt_xpath);
	 }
	
	public  WebElement PackDte_EstEnd_txt() {
		return GetElement(PackDte_EstEnd_txt_xpath);
	 }
	
	public  WebElement PackDte_ActEnd_txt() {
		return GetElement(PackDte_ActEnd_txt_xpath);
	 }
	
	public  WebElement Storage_EstStart_txt() {
		return GetElement(Storage_EstStart_txt_xpath);
	 }
	
	public  WebElement Storage_ActStart_txt() {
		return GetElement(Storage_ActStart_txt_xpath);
	 }
	
	public  WebElement Storage_EstEnd_txt() {
		return GetElement(Storage_EstEnd_txt_xpath);
	 }
	
	public  WebElement Storage_ActEnd_txt() {
		return GetElement(Storage_ActEnd_txt_xpath);
	 }
	
	public  WebElement Residence_EstStart_txt() {
		return GetElement(Residence_EstStart_txt_xpath);
	 }
	
	public  WebElement Residence_ActStart_txt() {
		return GetElement(Residence_ActStart_txt_xpath);
	 }
	
	public  WebElement Residence_EstEnd_txt() {
		return GetElement(Residence_EstEnd_txt_xpath);
	 }
	
	public  WebElement Residence_ActEnd_txt() {
		return GetElement(Residence_ActEnd_txt_xpath);
	 }
	
	public  WebElement SitDte_EstStart_txt() {
		return GetElement(SitDte_EstStart_txt_xpath);
	 }
	
	public  WebElement SitDte_ActStart_txt() {
		return GetElement(SitDte_ActStart_txt_xpath);
	 }
	
	public  WebElement SitDte_EstEnd_txt() {
		return GetElement(SitDte_EstEnd_txt_xpath);
	 }
	
	public  WebElement SitDte_ActEnd_txt() {
		return GetElement(SitDte_ActEnd_txt_xpath);
	 }
	
	public  WebElement DeliDte_EstStart_txt() {
		return GetElement(DeliDte_EstStart_txt_xpath);
	 }
	
	public  WebElement DeliDte_ActStart_txt() {
		return GetElement(DeliDte_ActStart_txt_xpath);
	 }
	
	public  WebElement DeliDte_EstEnd_txt() {
		return GetElement(DeliDte_EstEnd_txt_xpath);
	 }
	
	public  WebElement DeliDte_ActEnd_txt() {
		return GetElement(DeliDte_ActEnd_txt_xpath);
	 }
	
	public  WebElement ServiceDte_Update_btn() {
	  	return GetElement(ServiceDte_Update_btn_xpath);
	 }
	
	public  WebElement ServiceDte_Cancel_btn() {
	  	return GetElement(ServiceDte_Cancel_btn_xpath);
	 }

	//===================================METHODS
	
	public  void Update_ServiceDates_Section_func(Service_Order_Info order, String button)
	{
		switch (order.ServiceType)
		{
		case "SIT": case "EXT": case "STG":
			Update_ServiceDates_Section_SIT_EXT_STG_func(order, "submit");
			break;
		
		default:
			Update_ServiceDates_Other_Section_func(order, "submit");
			break;
		}//end switch	
	}
	
	private  void Update_ServiceDates_Section_SIT_EXT_STG_func(Service_Order_Info order, String button)
	{
		Optimize_ElementClick(ServiceDte_Edit_btn());
		
		Optimize_ElementSendkey(PackDte_EstStart_txt(), order.ServiceDate_PackDate_EstStart);
		
		PackDte_EstStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(PackDte_ActStart_txt(), order.ServiceDate_PackDate_ActStart);
		
		PackDte_ActStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(PackDte_EstEnd_txt(), order.ServiceDate_PackDate_EstEnd);
		
		PackDte_EstEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(PackDte_ActEnd_txt(), order.ServiceDate_PackDate_ActEnd);
		
		PackDte_ActEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Storage_EstStart_txt(), order.ServiceDate_Storage_EstStart);
		
		Storage_EstStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Storage_ActStart_txt(), order.ServiceDate_Storage_ActStart);
		
		Storage_ActStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Storage_EstEnd_txt(), order.ServiceDate_Storage_EstEnd);
		
		Storage_EstEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Storage_ActEnd_txt(), order.ServiceDate_Storage_ActEnd);
		
		Storage_ActEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Residence_EstStart_txt(), order.ServiceDate_Residence_EstStart);
		
		Residence_EstStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Residence_ActStart_txt(), order.ServiceDate_Residence_ActStart);
		
		Residence_ActStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Residence_EstEnd_txt(), order.ServiceDate_Residence_EstEnd);
		
		Residence_EstEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Residence_ActEnd_txt(), order.ServiceDate_Residence_ActEnd);
		
		Residence_ActEnd_txt().sendKeys(Keys.ENTER);
		
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			Optimize_ElementClick(ServiceDte_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Service Dates",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
	}
	
	private  void Update_ServiceDates_Other_Section_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(ServiceDte_Edit_btn());
		
		Optimize_ElementSendkey(PackDte_EstStart_txt(), order.ServiceDate_PackDate_EstStart);
		
		PackDte_EstStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(PackDte_ActStart_txt(), order.ServiceDate_PackDate_ActStart);
		
		PackDte_ActStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(PackDte_EstEnd_txt(), order.ServiceDate_PackDate_EstEnd);
		
		PackDte_EstEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(PackDte_ActEnd_txt(), order.ServiceDate_PackDate_ActEnd);
		
		PackDte_ActEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(SitDte_EstStart_txt(), order.ServiceDate_SitDate_EstStart);
		
		SitDte_EstStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(SitDte_ActStart_txt(), order.ServiceDate_SitDate_ActStart);
		
		SitDte_ActStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(SitDte_EstEnd_txt(), order.ServiceDate_SitDate_EstEnd);
		
		SitDte_EstEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(SitDte_ActEnd_txt(), order.ServiceDate_SitDate_ActEnd);
		
		SitDte_ActEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(DeliDte_EstStart_txt(), order.ServiceDate_DeliDate_EstStart);
		
		DeliDte_EstStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(DeliDte_ActStart_txt(), order.ServiceDate_DeliDate_ActStart);
		
		DeliDte_ActStart_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(DeliDte_EstEnd_txt(), order.ServiceDate_DeliDate_EstEnd);
		
		DeliDte_EstEnd_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(DeliDte_ActEnd_txt(), order.ServiceDate_DeliDate_ActEnd);
		
		DeliDte_ActEnd_txt().sendKeys(Keys.ENTER);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(ServiceDte_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Service Dates",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
	}
	
	//=======================================METHODS VERIFY
	
	public  void Verify_OrderDetail_ServiceDates_Section_func(Service_Order_Info order)
	{
		switch (order.ServiceType)
		{
		case "SIT": case "EXT": case "STG":
			Verify_ServiceDates_Section_SIT_EXT_STG_func(order);
			break;
		
		default:
			Verify_ServiceDates_Other_Section_func(order);
			break;
		}//end switch	
	}
	
	private  void Verify_ServiceDates_Section_SIT_EXT_STG_func(Service_Order_Info order)
	{
		String area = "";
		String field = "";
		
		area = "Packing Date";
		
		field = "Estimated Start";
		
		VerifyFieldValueEqual_func(area+ field,PackDte_EstStart_txt(), order.ServiceDate_PackDate_EstStart,"");
		
		field = "Actual Start";
		
		VerifyFieldValueEqual_func(area+ field,PackDte_ActStart_txt(), order.ServiceDate_PackDate_ActStart,"");
		
		field = "Estimated End";
		
		VerifyFieldValueEqual_func(area+ field,PackDte_EstEnd_txt(), order.ServiceDate_PackDate_EstEnd,"");
		
		field = "Actual End";
		
		VerifyFieldValueEqual_func(area+ field,PackDte_ActEnd_txt(), order.ServiceDate_PackDate_ActEnd,"");
		
		area = "Delivery to Storage";
		
		field = "Estimated Start";
		
		VerifyFieldValueEqual_func(area+ field,Storage_EstStart_txt(), order.ServiceDate_Storage_EstStart,"");
		
		field = "Actual Start";
		
		VerifyFieldValueEqual_func(area+ field,Storage_ActStart_txt(), order.ServiceDate_Storage_ActStart,"");
		
		field = "Estimated End";
		
		VerifyFieldValueEqual_func(area+ field,Storage_EstEnd_txt(), order.ServiceDate_Storage_EstEnd,"");
		
		field = "Actual End";
		
		VerifyFieldValueEqual_func(area+ field,Storage_ActEnd_txt(), order.ServiceDate_Storage_ActEnd,"");
		
		area = "Delivery to Residence";
		
		field = "Estimated Start";
		
		VerifyFieldValueEqual_func(area+ field,Residence_EstStart_txt(), order.ServiceDate_Residence_EstStart,"");
		
		field = "Actual Start";
		
		VerifyFieldValueEqual_func(area+ field,Residence_ActStart_txt(), order.ServiceDate_Residence_ActStart,"");
		
		field = "Estimated End";
		
		VerifyFieldValueEqual_func(area+ field,Residence_EstEnd_txt(), order.ServiceDate_Residence_EstEnd,"");
		
		field = "Actual End";
		
		VerifyFieldValueEqual_func(area+ field,Residence_ActEnd_txt(), order.ServiceDate_Residence_ActEnd,"");
	}
	
	private  void Verify_ServiceDates_Other_Section_func(Service_Order_Info order)
	{
		String area = "";
		String field = "";
		
		area = "Packing Date";
		
		field = "Estimated Start";
		
		VerifyFieldValueEqual_func(area+ field,PackDte_EstStart_txt(), order.ServiceDate_PackDate_EstStart,"");
		
		field = "Actual Start";
		
		VerifyFieldValueEqual_func(area+ field,PackDte_ActStart_txt(), order.ServiceDate_PackDate_ActStart,"");
		
		field = "Estimated End";
		
		VerifyFieldValueEqual_func(area+ field,PackDte_EstEnd_txt(), order.ServiceDate_PackDate_EstEnd,"");
		
		field = "Actual End";
		
		VerifyFieldValueEqual_func(area+ field,PackDte_ActEnd_txt(), order.ServiceDate_PackDate_ActEnd,"");
		
		area = "Sit Date";
		
		field = "Estimated Start";
		
		VerifyFieldValueEqual_func(area+ field,SitDte_EstStart_txt(), order.ServiceDate_SitDate_EstStart,"");
		
		field = "Actual Start";
		
		VerifyFieldValueEqual_func(area+ field,SitDte_ActStart_txt(), order.ServiceDate_SitDate_ActStart,"");
		
		field = "Estimated End";
		
		VerifyFieldValueEqual_func(area+ field,SitDte_EstEnd_txt(), order.ServiceDate_SitDate_EstEnd,"");
		
		field = "Actual End";
		
		VerifyFieldValueEqual_func(area+ field,SitDte_ActEnd_txt(), order.ServiceDate_SitDate_ActEnd,"");
		
		area = "Delivery Date";
		
		field = "Estimated Start";
		
		VerifyFieldValueEqual_func(area+ field,DeliDte_EstStart_txt(), order.ServiceDate_DeliDate_EstStart,"");
		
		field = "Actual Start";
		
		VerifyFieldValueEqual_func(area+ field,DeliDte_ActStart_txt(), order.ServiceDate_DeliDate_ActStart,"");
		
		field = "Estimated End";
		
		VerifyFieldValueEqual_func(area+ field,DeliDte_EstEnd_txt(), order.ServiceDate_DeliDate_EstEnd,"");
		
		field = "Actual End";
		
		VerifyFieldValueEqual_func(area+ field,DeliDte_ActEnd_txt(), order.ServiceDate_DeliDate_ActEnd,"");
	
	}//end void
}
