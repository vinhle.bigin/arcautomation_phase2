package pages;



import org.junit.Assert;

import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Activity_Info;
import configuration.DriverConfig.DriverBase;
import configuration.TestConfigs;

import custom_Func.FileManage;

public class ActivityDetails_mdal extends PageObjects {
	
	public ActivityDetails_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String actived_detail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";
	
	private  String ActivityType_slect_xpath= actived_detail_mdal_xpath+"//select[@placeholder = 'Activity Type']";
	
	private  String ServiceType_slect_xpath= actived_detail_mdal_xpath + "//select[@placeholder = 'Service type']";
	
	private  String Start_txt_xpath= actived_detail_mdal_xpath + "//input[contains(@id,'from_date') or contains(@id,'from-date')]";
	
	private  String End_txt_xpath= actived_detail_mdal_xpath   + "//input[contains(@id,'to_date') or contains(@id,'end-date')]";
	
	private  String Title_txt_xpath= actived_detail_mdal_xpath + "//div[contains(@class,'content-body container-fluid')]//input[@placeholder = 'Title']";//"//div[contains(@class,'modal-body')]//input[@placeholder = 'Title']"
		
	private  String Attach_lnk_xpath= actived_detail_mdal_xpath + "//div[contains(@id, 'attachment') or contains(@id,'upload')]//div[@class = 'content dz-clickable']";
	
	private  String Edit_btn_xpath = actived_detail_mdal_xpath + "//a[contains(text(),'Edit')]";
	
	private  String Close_opt_xpath = actived_detail_mdal_xpath + "//label[contains(text(),'Close')]/../input[@type = 'checkbox']";
	
	private  String ActivityPriority_slect_xpath = actived_detail_mdal_xpath + "//select[@placeholder='Priority']";
	
	private  String ScheduledWith_slect_xpath = actived_detail_mdal_xpath + "//select[@placeholder='Scheduled With']";
			
	private  String NotiRemider_slect_xpath =actived_detail_mdal_xpath +  "//select[@placeholder='Notification Reminder']";
	
	private  String ResultActivity_slect_xpath = actived_detail_mdal_xpath + "//select[@placeholder='Result']";
	
	private  String ReasonClosing_txt_xpath = actived_detail_mdal_xpath + "//textarea[@placeholder='Reason']";
	
	

	private  String Submit_btn_xpath= actived_detail_mdal_xpath + "//button[@type = 'submit']";
	
	private  String Cancel_btn_xpath= actived_detail_mdal_xpath + "//button[@data-control = 'cancel-create']";
	//private  String UpdateSubmit_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'update']";
	private  String UpdateCancel_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'cancel-update']";
	
	private  String ModalClose_btn_xpath = actived_detail_mdal_xpath+"//i[@class='icon-close']";

	//private  String SubmitTaskVendor_btn_xpath = "//button[@id='create-new-transferee-activity']"

	
	private  String ClientName_box_xpath = actived_detail_mdal_xpath+"//label[contains(text(),'Company Name')]/..//div[@class='selected single']";
	
	private  String ClientName_arrow_xpath = actived_detail_mdal_xpath+"//label[contains(text(),'Company Name')]/..//div[@class='iconC']";
	
	private  String VendorName_box_xpath = actived_detail_mdal_xpath+"//label[contains(text(),'Vendor Name')]/..//div[@class='selected single']";
	private  String VendorName_arrow_xpath = actived_detail_mdal_xpath+"//label[contains(text(),'Vendor Name')]//div[@class='iconC']";
	
	private  String ClientName_txt_xpath = actived_detail_mdal_xpath+ "//input[@placeholder='Company Name']";

	private  String VendorName_txt_xpath = actived_detail_mdal_xpath+ "//input[contains(@placeholder,'Vendor Name')]";

	private  String TransfName_txt_xpath = actived_detail_mdal_xpath+ "//input[contains(@placeholder,'Transferee')]";

	private  String ClientList_ddl_xpath = actived_detail_mdal_xpath+"//label[contains(text(),'Company Name')]/..//ul";

	private  String VendorList_ddl_xpath = actived_detail_mdal_xpath+"//label[contains(text(),'Vendor Name')]/..//ul";

	private  String TransfList_ddl_xpath =  actived_detail_mdal_xpath+ "//div[@id = 'b-combobox-transferee']//ul";

	//===============VIEWMODE for special element:
	private  String CompanyName_vlue_xpath = actived_detail_mdal_xpath+ "//*[@placeholder='Company Name' or @placeholder ='Client Name']";
	
	private  String VendorName_vlue_xpath = actived_detail_mdal_xpath+ "//select[@placeholder='Vendor Name']";
	
	private  String Transferee_vlue_xpath = actived_detail_mdal_xpath+ "//select[@placeholder='Transferee']";
	
	private  String RelatedTo_vlue_xpath = "//select[@placeholder='Related To']";
	
	private  String VendorOfHHGList_ddl_xpath = "//select[@placeholder='Vendor']";

	private  String ClientView_btn_xpath = actived_detail_mdal_xpath+"//div[@class='b__components b-checkbox']//label[contains(text(),'Client View')]";
	
	private  String VendorView_btn_xpath = actived_detail_mdal_xpath+"//div[@class='b__components b-checkbox']//label[contains(text(),'Vendor View')]";
	
	private  String TransfereeView_btn_xpath = actived_detail_mdal_xpath+"//div[@class='b__components b-checkbox']//label[contains(text(),'Transferee View')]";
	
	private  String Remove_File_btn_xpath = actived_detail_mdal_xpath+"//span[text() = '<file_name>']/..//a[@class='remove-archive']";
	
	private  String FileName_title_xpath = actived_detail_mdal_xpath+"//div[@class='preview']//span[contains(text() ,'<file_name>')]";

	
	
	public  WebElement Remove_File_btn(String file_name){

		Remove_File_btn_xpath = Remove_File_btn_xpath.replace("<file_name>", file_name);
		
		return GetElement(Remove_File_btn_xpath);
	}
	
	public  WebElement FileName_title(String file_name){

		WebElement filename_title = GetElement(FileName_title_xpath.replace("<file_name>", file_name));
		
		if (filename_title == null) {
			
			String a = FileName_title_xpath.replace("<file_name>",file_name.replace(file_name,file_name.toLowerCase().replace('_', '-')));
			
			filename_title = GetElement(a);
		}
		
		return filename_title;
	}

	public  WebElement Transferee_vlue(){

		return GetElement(Transferee_vlue_xpath);
	}
	
	public  WebElement ClientName_box(){

		return GetElement(ClientName_box_xpath);
	}
	
	public  WebElement VendorName_box(){

		return GetElement(VendorName_box_xpath);
	}
	
	public  WebElement ClientName_arrow(){

		return GetElement(ClientName_arrow_xpath);
	}
	
	public  WebElement VendorName_arrow(){

		return GetElement(VendorName_arrow_xpath);
	}
	
	public  WebElement VendorName_vlue(){

		return GetElement(VendorName_vlue_xpath);
	}

	public  WebElement CompanyName_vlue(){
		return GetElement(CompanyName_vlue_xpath);
		
	}

	public  WebElement TransfName_txt(){
		return GetElement(TransfName_txt_xpath);
		
	}

	public  WebElement TransfList_ddl(){
		return GetElement(TransfList_ddl_xpath);
	}

	public  WebElement ClientName_txt(){
		return GetElement(ClientName_txt_xpath);
		
	}

	public  WebElement VendorName_txt(){
		return GetElement(VendorName_txt_xpath);
		
	}

	public  WebElement VendorList_ddl(){
		return GetElement(VendorList_ddl_xpath);
		}

	public  WebElement ClientList_ddl(){
		return GetElement(ClientList_ddl_xpath);
		
	}


	public  WebElement ModalClose_btn() {
		return GetElement(ModalClose_btn_xpath);
	}

	public  WebElement UpdateCancel_btn() {
		return GetElement(UpdateCancel_btn_xpath);
		
	}

	public  WebElement Edit_btn() {
		return GetElement(Edit_btn_xpath);
	}

	public  WebElement Close_opt() {
		return GetElement(Close_opt_xpath);
		
	}


	public  WebElement ResultActivity_slect() {
		return GetElement(ResultActivity_slect_xpath);
		
	}

	public  WebElement ReasonClosing_txt() {
		return GetElement(ReasonClosing_txt_xpath);
		
	}

	
	public  WebElement ActivityType_slect() {
		return GetElement(ActivityType_slect_xpath);
	}

	public  WebElement ActivityPriority_slect() {
		return GetElement(ActivityPriority_slect_xpath);
		
	}

	public  WebElement ScheduledWith_slect() {
		return GetElement(ScheduledWith_slect_xpath);
		
	}

	public  WebElement NotiRemider_slect() {
		return GetElement(NotiRemider_slect_xpath);
		
	}

	
	public  WebElement ServiceType_slect() {
		return GetElement(ServiceType_slect_xpath);
		
	}

	public  WebElement Start_txt() {
		return GetElement(Start_txt_xpath);
		
	}

	public  WebElement End_txt() {
		return GetElement(End_txt_xpath);
		
	}

	public  WebElement Title_txt() {
		return GetElement(Title_txt_xpath);
		
	}

	public  WebElement Submit_btn() {
		return GetElement(Submit_btn_xpath);
		
	}

	public  WebElement Cancel_btn() {
		return GetElement(Cancel_btn_xpath);
		
	}

	public  WebElement Attach_lnk() {
		return GetElement(Attach_lnk_xpath);
	
	}

	public  WebElement RelatedTo_select() {
		return GetElement(RelatedTo_vlue_xpath);
	}

	public  WebElement VendorOfHHGList_slect() {
		return GetElement(VendorOfHHGList_ddl_xpath);
	}
	
	public  WebElement ClientView_btn() {
		return GetElement(ClientView_btn_xpath);
	}
	
	public  WebElement VendorView_btn() {
		return GetElement(VendorView_btn_xpath);
	}
	
	public  WebElement TransfereeView_btn() {
		return GetElement(TransfereeView_btn_xpath);
	}
	
	//=================================METHOD
	public  void OpenDetailsModal_func(Activity_Info Act)
	{
			
	//	Wait_For_ElementDisplay(Activities_tab.ActivityList_tbl());
		
		//println(Activities_tab.ActivityName_lnk_xpath + "span[text()='"+Act.Title+"']");
		
		new Activities_tab(driverbase).ActivityName_lnk(Act.Title).click();
		
	}
	
	
	public  void FillModalDetails(Activity_Info activity_arg) {

	try {
		
		String note_str = "Activity Details - Activity Type Dropdown.";
		
		SelectDropdownItem(ActivityType_slect(), activity_arg.Type,note_str);
		
		Title_txt().sendKeys(Keys.TAB);

		
		switch (activity_arg.AssigneeType)
		{
		case "Transferee":
			
			//Fill transferee Name on Task List, not applied for Activity Tab
			if(TransfName_txt()!=null)
				
			{
			//	Title_txt().sendKeys(Keys.TAB);
				
				note_str = "Activity Detail - TransfList Dropdown.";
				
				TransfName_txt().sendKeys(activity_arg.Assignee);

				try {
					SelectDropdownItem(TransfList_ddl(), activity_arg.Assignee, note_str);
					
				} catch (StaleElementReferenceException e) {
					SelectDropdownItem(TransfList_ddl(), activity_arg.Assignee, note_str);
				}
				
				
				Title_txt().sendKeys(Keys.TAB);
			}
			
			note_str = "Activity Details - ServiceType Dropdown.";
			ServiceType_slect().click();
			SelectDropdownItem(ServiceType_slect(), activity_arg.Service,note_str);
			
			//Cover for the case vendor dropdown will be displayed for HHG Service
			if(activity_arg.Service.equals("Household goods"))
			{
				note_str = "Activity Details - RelatedTo DropDown.";
				SelectDropdownItem(RelatedTo_select(), activity_arg.RelatedTo,note_str);

				SelectDropdownItem(VendorOfHHGList_slect(), activity_arg.Vendor,note_str);

			}//end if
			
			break;
			
		//==============In case belongs to is Vendor or Client
		default:
			
			if(activity_arg.AssigneeType.equals("Client") && ClientName_box()!=null)
			{
				note_str = "Activity Detail - ClientList Dropdown.";
				
					Optimize_ElementClick(ClientName_box());
					
			//		Wait_For_ElementDisplay(ClientName_txt());
					
					ClearThenEnterValueToField(ClientName_txt(), activity_arg.Assignee);
					
					SelectDropdownItem(ClientList_ddl(), activity_arg.Assignee, note_str);
							
			}//end if
			else if(VendorName_box()!=null)
			{
				
				note_str = "Activity Detail - VendorList Dropdown.";
								
				VendorName_box().click();
				
		//		Wait_For_ElementDisplay(VendorName_txt());
				
				ClearThenEnterValueToField(VendorName_txt(), activity_arg.Assignee);
				
				SelectDropdownItem(VendorList_ddl(), activity_arg.Assignee, note_str);
			}
			
			//Enter Schedule for Client or Vendor Contact FullName
			
			if(!activity_arg.ScheduleWith.equals(""))
			{
				ScheduledWith_slect().click();
				
				note_str = "Activity Details - ScheduleWith Dropdown.";
				
				SelectDropdownItem(ScheduledWith_slect(), activity_arg.ScheduleWith,note_str);
			}
			break;
		}//end switch
	
		//View Activity in Transferee / Client / Vendor Portal
		if(activity_arg.ClientView == true)
		{
			Optimize_ElementClick(ClientView_btn());
		}//end if
		
		if(activity_arg.VendorView == true)
		{
			Optimize_ElementClick(VendorView_btn());
		}//end if
		
		if(activity_arg.TransfereeView == true)
		{
			Optimize_ElementClick(TransfereeView_btn());
		}//end if
		
		//Fill StartDate
		
		Optimize_ElementSendkey(Start_txt(), activity_arg.StartDate);
		
		Thread.sleep(1000);
		
		Start_txt().sendKeys(Keys.TAB);
		
	//	Wait_For_ElementDisplay(End_txt());
		
		if(End_txt()==null)
		{
			System.out.println("END DATE IS NOT CLICKABLE");
		}

		//Fill EndDate
		Optimize_ElementSendkey(End_txt(), activity_arg.EndDate);
		
		Thread.sleep(1000);
		
		End_txt().sendKeys(Keys.TAB);
		
		//Fill Content
		ClearThenEnterValueToField(Title_txt(), activity_arg.Title);

		Optimize_ElementSendkey(Modal_TinyContentBox(), activity_arg.Content);
		
		//Close
		if(Close_opt()!=null)
		{
			boolean IsClose = Boolean.parseBoolean(GetFieldText_func(Close_opt()));
			
			if(IsClose!=activity_arg.Closed)
			{
				Close_opt().click();
			}
		}
		
		//ATTACH DOCUMENT
		int doc_size = activity_arg.FilesName.size();
		
		if(doc_size>0)
		{
			for(int i =0;i<doc_size;i++)
			{
				new FileManage().UploadFile_func(Attach_lnk(), activity_arg.FilesName.get(i));
				
			}//end for
			
		}//end if doc_size
		
			
		
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}//end catch
	
	}//end void
	
	//Verify
	public  void VerifyModalDetailsViewMode_func(Activity_Info act_info)
	{
		try {
			ARCLoading ARCLoading = new ARCLoading(driverbase);
			Activities_tab Activities_tab = new Activities_tab(driverbase);
			//=========Check if the activity is genereated from SM so that update the STart, End Time correctly
			//act_info.Calculate_STart_End_Date_func();
			
			//================VERIFY
			
			String area = "Modal Detail - ViewMode - ";
			String field = "";
	
			Activities_tab.ActivityName_lnk(act_info.Title).click();
			
			ARCLoading.Wait_Util_FinishedPageLoading();
			
	//		Wait_For_ElementDisplay(Title_txt());
	
			Thread.sleep(5000);
			
			field = "ActivityType_slect";
			VerifyFieldValueEqual_func(area+field, ActivityType_slect(), act_info.Type,"");
			
			switch (act_info.AssigneeType)
			{
			case "Transferee":
				field = "ServiceType_slect";
			
				VerifyFieldValueEqual_func(area+field, ServiceType_slect(), act_info.Service,"");
				
				//Fill transferee Name on Task List, not applied for Activity Tab
				
				field = "Transferee Name";
					
				VerifyFieldValueEqual_func(area+field, Transferee_vlue(), act_info.Assignee,"");
					
				break;
			
			//In case belongs to is Vendor or Client
			default:
				
				if(act_info.AssigneeType.equals("Client"))
				{
					field = "Client Company Name";
					
					VerifyFieldValueEqual_func(field, CompanyName_vlue(), act_info.Assignee,"");
					
				}
				else
				{
					field = "VEndor Name";
					
					if(VendorName_vlue()!=null)
					{
						
						
						VerifyFieldValueEqual_func(field, VendorName_vlue(), act_info.Assignee,"");
					}
					else
					{
						VerifyFieldValueEqual_func(field, CompanyName_vlue(), act_info.Assignee,"");
						
					}
				
				}
				
				//Enter Schedule for Client or Vendor Contact FullName
				
				
				field = "ScheduleWith";
					
				VerifyFieldValueEqual_func(field, ScheduledWith_slect(), act_info.ScheduleWith,"");
							
				
				break;
			}//end switch
			
			field = "Start_txt";
			//VerifyFieldValueEqual_func(area+field,Start_txt() , act_info.StartDate,"");
			VerifyFieldValueEqual_func(area+field,Start_txt() , act_info.StartDate,"");
			
			field = "End_txt";
			VerifyFieldValueEqual_func(area+field,End_txt() , act_info.EndDate,"");
			
	
			field = "Title_txt";
			VerifyFieldValueEqual_func(area+field, Title_txt(), act_info.Title,"");
	
			field = "Content_txt";
			//VerifyTinyContentEqual_func(area+field,act_info.Content);
			VerifyFieldValueEqual_func(area+field,Modal_TinyContentBox() , act_info.Content,"");
			
			field = "Close_opt";
			VerifyFieldValueEqual_func(field, Close_opt(), Boolean.toString(act_info.Closed),"");
		
			int doc_size = act_info.FilesName.size();
			
			if(doc_size>0)
			{
				for(int i =0;i<doc_size;i++) 
				{
					field = "Attached File";
					
					VerifyFieldDisplayed_func(field, FileName_title(act_info.FilesName.get(i)), "");
				
				}//end for
				
			}//end if doc_size
			
		} catch (InterruptedException | NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			TestConfigs.glb_TCFailedMessage+="Can't open the activity["+act_info.Title+"] due to:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}

		
	}//end void
	
	public  void Verify_Activity_Modal_Not_Editable(Activity_Info input_activity, String note)
	{
		String fieldname_str = "";		
		
		fieldname_str = "Activity Modal - Activity Type";
		
		//Verify_Field_Not_Editable_func(fieldname_str, ActivityType_slect(), input_activity.Type, note);
		
		fieldname_str = "Activity Modal - Start Date";
		
		Verify_Field_Not_Editable_func(fieldname_str, Start_txt(), input_activity.StartDate, note);
		
		fieldname_str = "Activity Modal - End Date";
		
		Verify_Field_Not_Editable_func(fieldname_str, End_txt(), input_activity.EndDate, note);
		
		fieldname_str = "Activity Modal - Title";
		
		Verify_Field_Not_Editable_func(fieldname_str, Title_txt(), input_activity.Title, note);
		
		fieldname_str = "Activity Modal - Content Box";
		
		Optimize_ElementSendkey(Modal_TinyContentBox(),input_activity.Content);
		
		//SwitchIframe_func(null);
		
		VerifyFieldValueNotEqual_func(fieldname_str, Modal_TinyContentBox(), input_activity.Content, "When verify the field not editable.\n");

		
	}//end void

}
