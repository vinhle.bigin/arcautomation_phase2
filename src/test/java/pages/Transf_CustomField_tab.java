package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.CustomField_Info;
import configuration.DriverConfig.DriverBase;

public class Transf_CustomField_tab extends PageObjects{
	
	
	
	public Transf_CustomField_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public  String Custom_tab_xpath = "//a[contains(@href,'custom-fields')]";
	public  String Edit_btn_xpath = "//a[@data-original-title='Edit']";
	public  String Update_btn_xpath = "//button[@data-control='update']";
	public  String Cancel_btn_xpath = "//button[@data-control='cancel-update']";


	
	public  WebElement TextBox_txt(String fieldname) {
		
		String TextBox_txt_xpath = "//div[@org-placeholder='"+fieldname+"']//input";
		return GetElement(TextBox_txt_xpath);	
			
	}

	public  WebElement Dropdown_ddl(String fieldname) {
	
		String 	Dropdown_ddl_xpath = "//div[@org-placeholder='"+fieldname+"']//select";
		return GetElement(Dropdown_ddl_xpath);	
	}

	public  WebElement Checkbox_chk(String fieldname) {
		
			String 	Checkbox_chk_xpath = "//div[@org-placeholder='"+fieldname+"']//input";
			return GetElement(Checkbox_chk_xpath);	
	}

	public  WebElement TextArea_txt(String fieldname) {
		
			String 	TextArea_txt_xpath = "//div[@org-placeholder='"+fieldname+"']//textarea";
			return GetElement(TextArea_txt_xpath);	
	}

	public  WebElement Radio_rad(String fieldname) {
		
			String 	Radio_rad_xpath = "//div[@org-placeholder='"+fieldname+"']//input";
			return GetElement(Radio_rad_xpath);	
	}

	public  WebElement MultiDropdown_ddl(String fieldname) {
	
			String 	MultiDropdown_ddl_xpath = "//div[@org-placeholder='"+fieldname+"']//ul";
			return GetElement(MultiDropdown_ddl_xpath);	
		
	}

	public  WebElement MultiDropdown_arrow(String fieldname) {
	
			String 	MultiDropdown_arrow_xpath = "//div[@org-placeholder='"+fieldname+"']//div[@class='iconC']";
			return GetElement(MultiDropdown_arrow_xpath);	
	}


	public  WebElement Custom_tab() {
		return GetElement(Custom_tab_xpath);
	}


	public  WebElement GroupEdit_btn(String groupname) {
		
			String 	Group_xpath = "//h3[contains(text(),'"+groupname+"')]/.."+Edit_btn_xpath;
			return GetElement(Group_xpath);
	}


	public  WebElement GroupUpdate_btn(String groupname) {
	
			String 	Group_xpath = "//h3[contains(text(),'"+groupname+"')]/../.."+Update_btn_xpath;
			return GetElement(Group_xpath);
	}

	public  WebElement GroupCancel_btn(String groupname) {
		
			String 	Group_xpath = "//h3[contains(text(),'"+groupname+"')]/../.."+Cancel_btn_xpath;
			return GetElement(Group_xpath);
	}

	/////////================================METHOD
	//IDENTIFY FIELD
	private  WebElement Identify_Field(CustomField_Info field)
	{
		String controltype = field.ControlType;

		WebElement e_temp = null;

		switch(controltype)
		{
			case "Textbox":
				e_temp = TextBox_txt(field.FieldName);

				break;

			case "Checkbox":
				e_temp = Checkbox_chk(field.FieldName);

				break;

			case "Date Picker":
				e_temp = TextBox_txt(field.FieldName);

				break;

			case "Dropdown":
				e_temp = Dropdown_ddl(field.FieldName);
				break;

			case "Multiselect":

				break;

			case "Numeric Field":
				e_temp = TextBox_txt(field.FieldName);
				break;

			case "Radio Button":
				e_temp = Radio_rad(field.FieldName);
				break;

			case "TextArea":
				e_temp = TextArea_txt(field.FieldName);
				break;
		}
		
		return e_temp;
	}
	//Edit CUSTOM FIELD
	public  void EditCustomField_func(CustomField_Info field)
	{
		try
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			if(Custom_tab().getAttribute("aria-expanded")!="true")
			{
				Optimize_ElementClick(Custom_tab());

			}

			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

		//	Wait_For_ElementDisplay(GroupEdit_btn(field.custom_group.Name));


			ScrollElementtoViewPort_func(GroupEdit_btn(field.custom_group.Name));

			GroupEdit_btn(field.custom_group.Name).click();

			Thread.sleep(2000);

			String controltype = field.ControlType;
			String value_str;
			switch(controltype)
			{
				case "Textbox":
					ClearThenEnterValueToField(TextBox_txt(field.FieldName), field.ValueList.get(0));
					break;

				case "Checkbox":
					value_str = GetFieldText_func(Checkbox_chk(field.FieldName));

					if(value_str!=field.ValueList.get(0))
						Checkbox_chk(field.FieldName).click();
					break;

				case "Date Picker":
					Optimize_ElementSendkey(TextBox_txt(field.FieldName), field.ValueList.get(0));
					break;

				case "Dropdown":
					String note_str = "CustomField Tab - FieldName Dropdown.";
					SelectDropdownItem(Dropdown_ddl(field.FieldName), field.ValueList.get(0),note_str);
					break;

				case "Multiselect":

					break;

				case "Numeric Field":
					ClearThenEnterValueToField(TextBox_txt(field.FieldName), field.ValueList.get(0));
					break;

				case "Radio Button":
					value_str = GetFieldText_func(Radio_rad(field.FieldName));

					if(value_str!=field.ValueList.get(0))
						Radio_rad(field.FieldName).click();

					break;

				case "TextArea":
					ClearThenEnterValueToField(TextArea_txt(field.FieldName), field.ValueList.get(0));
					break;
			}

			GroupUpdate_btn(field.custom_group.Name).click();

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "Edit Custom Field",false);

		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

	//VERIFY VIEW MODE FOR 1 ELEMENT
	public  void VerifyViewModePerElement(CustomField_Info field)
	{
		if(Custom_tab().getAttribute("aria-expanded")!="true")
		{
			Custom_tab().click();
		}


		String controltype = field.ControlType;

		switch(controltype)
		{
			case "Textbox":
				VerifyFieldValueEqual_func(field.FieldName, TextBox_txt(field.FieldName), field.ValueList.get(0),"");
				break;

			case "Checkbox":
				VerifyFieldValueEqual_func(field.FieldName, Checkbox_chk(field.FieldName), field.ValueList.get(0),"");
				break;

			case "Date Picker":
				VerifyFieldValueEqual_func(field.FieldName, TextBox_txt(field.FieldName), field.ValueList.get(0),"");
				break;

			case "Dropdown":
				VerifyFieldValueEqual_func(field.FieldName, Dropdown_ddl(field.FieldName), field.ValueList.get(0),"");

				break;

			case "Multiselect":

				break;

			case "Numeric Field":
				VerifyFieldValueEqual_func(field.FieldName, TextBox_txt(field.FieldName), field.ValueList.get(0),"");

				break;

			case "Radio Button":
				VerifyFieldValueEqual_func(field.FieldName, Radio_rad(field.FieldName), field.ValueList.get(0),"");

				break;

			case "TextArea":
				VerifyFieldValueEqual_func(field.FieldName, TextArea_txt(field.FieldName), field.ValueList.get(0),"");

				break;
		}
	}


	//VERIFY CUSTOM FIELD DISPLAY AS EXPECTED
	public  void VerifyCustomFieldDisplay(CustomField_Info field)
	{
		if(Custom_tab().getAttribute("aria-expanded")!="true")
		{
			Custom_tab().click();
		}

		
		WebElement e_temp  = Identify_Field(field);

		VerifyFieldDisplayed_func(field.FieldName, e_temp,"");

	}//end void

	
	//Find the Custom Field display
	public  boolean IsCustomFieldDisplay(CustomField_Info field)
	{
		if(Custom_tab().getAttribute("aria-expanded")!="true")
		{
			Custom_tab().click();
		}

		//idendity field
		WebElement e_temp = Identify_Field(field);

		if(e_temp!=null && e_temp.isDisplayed())
		{
			return false;
		}//if
		
		return true;
	}

	//VERIFY CUSTOM FIELD NOT DISPLAY AS EXPECTED
	public  void VerifyCustomFieldNotDisplay(CustomField_Info field)
	{
		if(Custom_tab().getAttribute("aria-expanded")!="true")
		{
			Custom_tab().click();
		}

		String controltype = field.ControlType;

		WebElement e_temp = null;

		switch(controltype)
		{
			case "Textbox":
				e_temp = TextBox_txt(field.FieldName);

				break;

			case "Checkbox":
				e_temp = Checkbox_chk(field.FieldName);

				break;

			case "Date Picker":
				e_temp = TextBox_txt(field.FieldName);

				break;

			case "Dropdown":
				e_temp = Dropdown_ddl(field.FieldName);
				break;

			case "Multiselect":

				break;

			case "Numeric Field":
				e_temp = TextBox_txt(field.FieldName);
				break;

			case "Radio Button":
				e_temp = Radio_rad(field.FieldName);
				break;

			case "TextArea":
				e_temp = TextArea_txt(field.FieldName);
				break;
		}

		VerifyFieldNotDisplayed_func(field.FieldName, e_temp,"");

	}//end void

}
