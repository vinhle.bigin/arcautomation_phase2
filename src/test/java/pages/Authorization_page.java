package pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Auth_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class Authorization_page extends PageObjects{
	public Authorization_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private ARCLoading ARCLoading;
	private NewAuth_mdal NewAuth_mdal;
	private Messages_Notification Messages_Notification;
	
	
	private String AuthList_tbl_xpath = "//div[@id = 'table-list-authorization']";
	private String NewAuth_btn_xpath = "//button[contains(text(),'New Authorization')]";
	private String SearchAuthName_txt_xpath = "//input[@name ='transferee_name']";
	private String Search_btn_xpath = "//button[contains(text(),'Search')]";
	private String AlsoApprove_btn_xpath = "//button[contains(text(),'OK')]";
	
		
	public WebElement AuthName_lnk(String authname) {
		try{
			
			String xpath_str =  "//a//span[contains(text(),'" +authname+"')] | //a[contains(text(),'" + authname + "')]";
			WebElement temp_element = GetElement(xpath_str);
					
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}
	public WebElement AuthList_tbl() {
		return GetElement(AuthList_tbl_xpath);
	}	
	public WebElement NewAuth_btn() {
		return GetElement(NewAuth_btn_xpath);
	}
	public WebElement SearchAuth_txt() {
		return GetElement(SearchAuthName_txt_xpath);
	}
	public WebElement Search_btn() {
		return GetElement(Search_btn_xpath);
	}
	public WebElement AlsoApprove_btn() {
		return GetElement(AlsoApprove_btn_xpath);
	}
	
	
	
	
	
	//=============================METHOD
	
	
	public void CreateAuth_func(Auth_Info auth,String submit)
	{
		Messages_Notification = new Messages_Notification(driverbase);	
		
		NewAuth_mdal = new NewAuth_mdal(driverbase);

		Optimize_ElementClick(NewAuth_btn());
		
			NewAuth_mdal.FillAuthModal_func(auth, submit);
			
			if(Messages_Notification.notification_msg()!=null)
			{
				String msg = Messages_Notification.notification_msg().getText();
				
				String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
				
				if(msg.equals(expected_msg))
				{
					TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Client Creation.Msg: '"+msg+"'.\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
			}//end if of message
			
			if(AlsoApprove_btn()!=null) {
				Optimize_ElementClick(AlsoApprove_btn());
			}
			
			
	}//end void

	public  void SearchAuth_func(Auth_Info auth) 
	{
		ARCLoading = new ARCLoading(driverbase);
		
		SearchAuth_txt().sendKeys(auth.FullName);
		
		Optimize_ElementClick(Search_btn());
		
		ARCLoading.Wait_Util_FinishedARClogoLoading();
		
	
	}

	public  void VerifyAuthExistTable_func(Auth_Info auth) 
	{
		boolean exist = false;;
		List<String> records = GetTableRecordsPerPaging_func(AuthList_tbl());

		for(String index_str: records)
		{
			if(index_str.contains(auth.FullName))
			{
				exist = true;
				break;
			}//end if
		}//end foreach

		if(!exist)
		{
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "Client["+auth.FullName+"] NOT exist in table.\n";
		}//end if
	}//endvoid
	
	public  void Verify_Auth_Not_Exist_Table_func(Auth_Info auth,String note)
	{
		VerifyDataNotExistTable_func(AuthList_tbl(), auth.FullName, note);
	}
	
}
