package pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import baseClasses.PageObjects;
import businessObjects.CustomField_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class CustomFieldManage_tab extends PageObjects{
	public CustomFieldManage_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private ARCLoading ARCLoading;
	private Messages_Notification Messages_Notification;
	LeftMenu_page LeftMenu_page;
	
	private  String ManageCustField_tab_xpath= "//label[@href ='#manage-custom-fields']";
	private  String Search_FieldName_txt_xpath= "//input[@placeholder = 'Field Name']";
	private  String Search_btn_xpath= "//button[@data-control ='search']";
	private  String Clear_btn_xpath= "//button[@data-control ='cancel-search']";
	private  String New_CustomField_btn_xpath= "//button[contains(text(), 'New Custom Field')]";
	private  String CustFieldList_tbl_xpath= "//table[@id = 'customization-table']/tbody";
	private  String Dtail_FieldType_ddl_xpath= "//select[@placeholder = 'Field Type']";
	private  String Dtail_FieldName_txt_xpath= "//input[@placeholder = 'Field Name']";
	private  String ControlType_ddl_xpath= "//select[@placeholder = 'Control Type']";
	private  String Dtail_Group_ddl_xpath= "//select[@placeholder = 'Group']";
	private  String Dtail_IsActive_opt_xpath= "//div[@class='col-md-3']//label[text() = 'Active']/../input";
	private  String Dtail_IsRequire_opt_xpath= "//label[text() = 'Required']/../input";
	private  String Dtail_Submit_btn_xpath= "//button[@id ='submit-template']";
	private  String Dtail_Cancel_btn_xpath= "//button[@cancel ='cancel-template']";
	private  String Dtail_Option_txt_xpath= "//input[@placeholder ='Option']";
	private  String Dtail_OptionAdd_btn_xpath= "//button[@id = 'submit-new-option']";
	private  String Dtail_OptionList_tbl_xpath= "//div[@class = 'container-fluid table']//div[contains(@class,'row option-item move')]";
	private  String FieldName_lnk_xpath = "//a";
	private  String Remove_icon_xpath = "//span[@data-original-title='Remove']";
	private  String Dtail_IsIncludeSmartTask_opt_xpath = "//label[text() = 'Included In Smart Task Update Action']/../input";
	private  String Dtail_IsSpecificClients_opt_xpath = "//label[text() = 'Specific Clients']/../input";

	
	private  String Dtail_ApplyGovernment_opt_xpath = "//div[@org-placeholder='All Government Clients']//input";
	
	private  String Dtail_ApplyCorporate_opt_xpath = "//div[@org-placeholder='All Corporate Clients']//input";
	
	private  String Dtail_ApplyAll_opt_xpath = "//div[@org-placeholder='All Clients']//input";
	
	private  String Dtail_ApplySpecific_opt_xpath = "//div[@org-placeholder='Specific Clients']//input";
	
	private  String Dtail_ApplySpcefic_arrow_xpath = "//div[contains(@class,'form-group-client')]//div[contains(@class,'iconC')]";
	
	private  String Dtail_ApplySpcefic_Client_txt_xpath = "//div[contains(@class,'form-group-client')]//input";
	
	private  String Dtail_ApplySpcefic_ClientList_ddl_xpath = "//div[contains(@class,'form-group-client')]//ul[@class='b__multi__select__list']";
	
	private String CustomField_Group_Name_txt_xpath = "//div[contains(@id, \"loading-custom-field-group\")]//h3[@class = \"panel-heading\"]";

	private String CustomField_Name_txt_xpath = "//div[contains(@id, \"loading-custom-field-group\")]//label[@class = \"panel-body\"]";
	
	
	public  WebElement CustomField_Name_txt() {
		  return GetElement(CustomField_Name_txt_xpath);
		 }
	
	public  WebElement CustomField_Group_Name_txt() {
		  return GetElement(CustomField_Group_Name_txt_xpath);
		 }
	
	public  WebElement Dtail_ApplySpcefic_arrow() {
		  return GetElement(Dtail_ApplySpcefic_arrow_xpath);
		 }
	
	public  WebElement Dtail_ApplySpcefic_Client_txt() {
		  return GetElement(Dtail_ApplySpcefic_Client_txt_xpath);
		 }
	
	public  WebElement Dtail_ApplySpcefic_ClientList_ddl() {
		  return GetElement(Dtail_ApplySpcefic_ClientList_ddl_xpath);
		 }
	
	public  WebElement Dtail_ApplyGovernment_opt() {
		  return GetElement(Dtail_ApplyGovernment_opt_xpath);
		 }
	
	public  WebElement Dtail_ApplyCorporate_opt() {
		  return GetElement(Dtail_ApplyCorporate_opt_xpath);
		 }
	
	public  WebElement Dtail_ApplyAll_opt() {
		  return GetElement(Dtail_ApplyAll_opt_xpath);
		 }
	
	public  WebElement Dtail_ApplySpecific_opt() {
		  return GetElement(Dtail_ApplySpecific_opt_xpath);
		 }
	
	public  WebElement Dtail_IsSpecificClients_opt() {
		  return GetElement(Dtail_IsSpecificClients_opt_xpath);
		 }
	
	public  WebElement Dtail_IsIncludeSmartTask_opt() {
		  return GetElement(Dtail_IsIncludeSmartTask_opt_xpath);
		 }
	
	public  WebElement ManageCustField_tab() {
	  return GetElement(ManageCustField_tab_xpath);
	 }
	
public  WebElement Search_FieldName_txt() {
	  return GetElement(Search_FieldName_txt_xpath);
	 }

public  WebElement Search_btn() {
	  return GetElement(Search_btn_xpath);
	 }

public  WebElement Clear_btn() {
	  return GetElement(Clear_btn_xpath);
	 }

public  WebElement New_CustomField_btn() {
	  return GetElement(New_CustomField_btn_xpath);
	 }

public  WebElement CustFieldList_tbl() {
	  return GetElement(CustFieldList_tbl_xpath);
	 }

public  WebElement Dtail_FieldType_ddl() {
	  return GetElement(Dtail_FieldType_ddl_xpath);
	 }

public  WebElement Dtail_FieldName_txt() {
	  return GetElement(Dtail_FieldName_txt_xpath);
	 }

public  WebElement ControlType_ddl() {
	  return GetElement(ControlType_ddl_xpath);
	 }

public  WebElement Dtail_Group_ddl() {
	  return GetElement(Dtail_Group_ddl_xpath);
	 }

public  WebElement Dtail_IsActive_opt() {
	  return GetElement(Dtail_IsActive_opt_xpath);
	 }

public  WebElement Dtail_IsRequire_opt() {
	  return GetElement(Dtail_IsRequire_opt_xpath);
	 }

public  WebElement Dtail_Submit_btn() {
	  return GetElement(Dtail_Submit_btn_xpath);
	 }

public  WebElement Dtail_Cancel_btn() {
	  return GetElement(Dtail_Cancel_btn_xpath);
	 }

public  WebElement Dtail_Option_txt() {
	  return GetElement(Dtail_Option_txt_xpath);
	 }

public  WebElement Dtail_OptionAdd_btn() {
	  return GetElement(Dtail_OptionAdd_btn_xpath);
	 }

public  WebElement Remove_icon() {
	  return GetElement(Remove_icon_xpath);
	 }

	public  List<WebElement> Dtail_OptionList_tbl() {
		return GetElements(Dtail_OptionList_tbl_xpath);
	}

	public  WebElement FieldName_lnk(String fieldname_str) {
		try{

			String xpath_str = FieldName_lnk_xpath+"[contains(text(),'"+fieldname_str+"')]";

			WebElement temp_element = GetElement(xpath_str);
					
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}

	public  WebElement Remove_icon(String fieldname_str) {
		try{

			WebElement temp_element = GetElementInTable(fieldname_str, CustFieldList_tbl(), Remove_icon_xpath);
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}
	
/////////================================METHOD

//VERIFY INFO IN DETAILS PAGE
public  void VerifyInfoDetailPage_func(CustomField_Info cus_field)
{
	String area = "";
	String field_name = "";

	Messages_Notification = new Messages_Notification(driverbase);

	ARCLoading = new ARCLoading(driverbase);
	
	ARCLoading.Wait_Util_FinishedPageLoading();
	
	
	Optimize_ElementClick(FieldName_lnk(cus_field.FieldName));

	ARCLoading.Wait_Util_FinishedPageLoading();

	//Wait_For_ElementDisplay(Dtail_FieldName_txt());

	///ORG RESIDENCE
	area = "Details Custom Fields - ";

	field_name = "Dtail_FieldType_ddl";
	String note = "- View Mode";

	VerifyFieldValueEqual_func(area+field_name, Dtail_FieldType_ddl(), cus_field.FieldType,note);

	field_name = "ControlType_ddl";
	VerifyFieldValueEqual_func(area+field_name, ControlType_ddl(), cus_field.ControlType,note);

	field_name = "Dtail_Group_ddl";
	String group_str = cus_field.custom_group.Tab+" - " + cus_field.custom_group.Name;
	VerifyFieldValueEqual_func(area+field_name, Dtail_Group_ddl(), group_str,note);

	field_name = "Dtail_FieldName_txt";
	VerifyFieldValueEqual_func(area+field_name, Dtail_FieldName_txt(), cus_field.FieldName,note);


	if(cus_field.ControlType=="Dropdown" || cus_field.ControlType=="Multiselect" || cus_field.ControlType=="Radio Button")
	{
		int size_int = cus_field.OptionList.size();
		String tmp_str = "";
		for(int i=0;i<size_int;i++)
		{
			tmp_str = cus_field.OptionList.get(i);
			VerifyOptionExistDetails(tmp_str);
		}//for

	}//end if

}//end void



//VERIFY OPTION IS DISPLAYED IN THE OPTION LIST IN DETAILS PAGE

public  void VerifyOptionExistDetails(String opt_name)
{
	Boolean exist = false;
	String observed = "";
	int e_size = Dtail_OptionList_tbl().size();
	for(int j =0;j<e_size;j++)
	{
		observed +=Dtail_OptionList_tbl().get(j).getText()+"\n";
		if(Dtail_OptionList_tbl().get(j).getText().contains(opt_name))
		{
			exist = true;
		}
	}

	if(exist==false)
	{
		TestConfigs.glb_TCStatus=false;
		TestConfigs.glb_TCFailedMessage+="Option does not exist.[Observed: "+observed+"- Expected: "+opt_name+"].\n";
	}


}

public  void Verify_CustomField_Display_TransferDetails(CustomField_Info cus_info)
{
	String field_name = "";
	String area = "";
	
	field_name = "Custom Field - ";
	VerifyFieldValueEqual_func(area+field_name, CustomField_Group_Name_txt(), field_name + cus_info.custom_group.Name, "");
	
	field_name = "Field Name";
	VerifyFieldValueEqual_func(area+field_name, CustomField_Name_txt(), cus_info.FieldName, "");

}


//FILL INFO TO DETAIL PAGE
public  void FillModalInfo_func(CustomField_Info cus_field,String Action)
{
	try {
			
		new Messages_Notification(driverbase);

		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		ARCLoading.Wait_Util_FinishedPageLoading();
			
		//Wait_For_ElementDisplay(Dtail_FieldName_txt());
		String note_str = "";
		if(Action=="Create")
		{
			note_str = "Custom Field Details - FieldType Dropdown.";
			
			SelectDropdownItem(Dtail_FieldType_ddl(), cus_field.FieldType,note_str);
	
			note_str = "Custom Field Details - ControlType Dropdown.";
			SelectDropdownItem(ControlType_ddl(), cus_field.ControlType,note_str);
		}
	
	
		String group_str = cus_field.custom_group.Tab+" - " + cus_field.custom_group.Name;
		
		note_str = "Custom Field Details - Group Dropdown.";
		SelectDropdownItem(Dtail_Group_ddl(), group_str,note_str);
	
		Dtail_FieldName_txt().sendKeys(cus_field.FieldName);
		
		String vlue_str = GetFieldText_func(Dtail_IsActive_opt());
		
		if(vlue_str!=cus_field.IsActive.toString())
			Dtail_IsActive_opt().click();
		
		SetUp_ClientApplyType_func(cus_field.Apply_ClientType,cus_field.client.CompanyName);
	
		if(cus_field.ControlType=="Dropdown" || cus_field.ControlType=="Multiselect" || cus_field.ControlType=="Radio Button")
		{
			
				int size_int = cus_field.OptionList.size();
				for(int i=0;i<size_int;i++)
				{
					Dtail_Option_txt().sendKeys(cus_field.OptionList.get(i));
					Dtail_OptionAdd_btn().click();
					Thread.sleep(1000);
		
				}//For
		}//IF
		
		if(cus_field.IsIncludeSmartTask == true)
		{
			String vlue_str1 = GetFieldText_func(Dtail_IsIncludeSmartTask_opt());
			
			if(vlue_str1!=cus_field.IsIncludeSmartTask.toString())
				Dtail_IsIncludeSmartTask_opt().click();
		}
		
	}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}//end catch
	
}//end void



//SEARCH THE CUSTOM FIELD

public  void SearchCustomField_func(String fieldname_str)
{
	new LeftMenu_page(driverbase).GotoCustomFieldPage();

	Optimize_ElementClick(ManageCustField_tab());

	Search_FieldName_txt().sendKeys(fieldname_str);

	Optimize_ElementClick(Search_btn());

	Boolean exist = IsItemExistTable(CustFieldList_tbl(), fieldname_str);

	if(exist==false)
	{
		TestConfigs.glb_TCFailedMessage+="CustomField["+fieldname_str+"] NOT found.";
		Assert.fail(TestConfigs.glb_TCFailedMessage);
	}//end if exist


}//end void



public  void Create_func(CustomField_Info field_info ,String button)
//NOTE: button value = "Submit"
{
	Messages_Notification = new Messages_Notification(driverbase);
	
	Optimize_ElementClick(ManageCustField_tab());
	
	Optimize_ElementClick(New_CustomField_btn());

	FillModalInfo_func(field_info, "Create");

	if(button=="Submit")
	{
		Dtail_Submit_btn().click();
	}
	else if(button=="Cancel")
	{
		Dtail_Cancel_btn().click();
	}

}//end void

public  void Update_func(String oldfieldname, CustomField_Info new_field , String button)
{
	Messages_Notification = new Messages_Notification(driverbase);
	
	FieldName_lnk(oldfieldname).click();

	FillModalInfo_func(new_field, "Update");

	if(button.equals("Submit"))
	{
		Dtail_Submit_btn().click();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "Update Custom Field",false);
	}
	else if(button.equals("Cancel"))
	{
		Dtail_Cancel_btn().click();
	}

}//end void

//Remove the Custom Field

public  void Remove_func(CustomField_Info field_info)
{
	Messages_Notification = new Messages_Notification(driverbase);
	
	Remove_icon(field_info.FieldName).click();
	
	Messages_Notification.Delete_btn().click();
	Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Remove Custom Field.\n",false);

}

public  String GetFieldID_func(CustomField_Info field_info)
{
	
	SearchCustomField_func(field_info.FieldName);

	String vlue_str = FieldName_lnk(field_info.FieldName).getAttribute("onclick");

	vlue_str = vlue_str.toString().replace("vueApp.$router.push({name:'edit', params: { id:", "");

	vlue_str = vlue_str.toString().replace(" } })", "");

	return vlue_str;
}

private  void Assign_SpecificClientList_func(String clientname)
{
	Optimize_ElementClick(Dtail_ApplySpcefic_arrow());
				
	Optimize_ElementSendkey(Dtail_ApplySpcefic_Client_txt(), clientname);
	
	String note_str = "SmartTask Detail - Client List";
	
	SelectDropdownItem(Dtail_ApplySpcefic_ClientList_ddl(), clientname, note_str);
	
	Optimize_ElementClick(Dtail_ApplySpcefic_arrow());
	
}


private  void SetUp_ClientApplyType_func(String appy_type,String specific_clientname)
{
	switch(appy_type)
	{
	case "Specific Clients":
		Optimize_ElementClick(Dtail_ApplySpecific_opt());
		
		Assign_SpecificClientList_func(specific_clientname);
	
		break;
		
	case "Government":
		Optimize_ElementClick(Dtail_ApplyGovernment_opt());
		break;
		
	case "Corporate":
		Optimize_ElementClick(Dtail_ApplyCorporate_opt());
		break;
	
	default://for All Client
		Optimize_ElementClick(Dtail_ApplyAll_opt());
		
		break;
	}
}

}
