package pages;

import org.openqa.selenium.Keys;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;
import custom_Func.Data_Optimize;

public class Order_Insurance_section extends PageObjects{
	

	public Order_Insurance_section(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private  String Insur_Edit_btn_xpath = "//a[@id='icon-hhg-order-insurance']";
	private  String Insur_Update_btn_xpath = "//div[@id='hhg-order-insurance']//button[@data-control='update']";
	private  String Insur_Cancel_btn_xpath = "//div[@id='hhg-order-insurance']//button[@data-control='cancel-update']";
	private  String ValueInsur_Amount_txt_xpath = "//div[@org-placeholder='Value Insurance Amount']//input";
	private  String InsurAuth_Cap_txt_xpath = "//div[@org-placeholder='Insurance Authorized Cap']//input";
	private  String DteValueInven_Received_txt_xpath = "//div[@org-placeholder='Date Valued Inventory Received']//input";
	private  String Insur_Paidby_ddl_xpath = "//div[@id='hhg-order-insurance']//div[@org-placeholder='Paid by']//select";
	private  String Insur_Reimby_ddl_xpath = "//div[@id='hhg-order-insurance']//div[@org-placeholder='Reimbursed by']//select";
	private  String Insur_Terms_txt_xpath = "//div[@id='hhg-order-insurance']//div[@org-placeholder='Terms']//pre[@placeholder = 'Terms']";
	private  String Insur_SpecExcept_txt_xpath = "//div[@id='hhg-order-insurance']//div[@org-placeholder='Special Exceptions']//pre[@placeholder = 'Terms']";

	
	
	public  WebElement Insur_Edit_btn() {
	  	return GetElement(Insur_Edit_btn_xpath);
	 }
	
	public  WebElement Insur_Update_btn() {
	  	return GetElement(Insur_Update_btn_xpath);
	 }
	
	public  WebElement Insur_Cancel_btn() {
	  	return GetElement(Insur_Cancel_btn_xpath);
	 }
	
	public  WebElement ValueInsur_Amount_txt() {
	  	return GetElement(ValueInsur_Amount_txt_xpath);
	 }
	
	public  WebElement InsurAuth_Cap_txt() {
	  	return GetElement(InsurAuth_Cap_txt_xpath);
	 }
	
	public  WebElement DteValueInven_Received_txt() {
	  	return GetElement(DteValueInven_Received_txt_xpath);
	 }
	
	public  WebElement Insur_Paidby_ddl() {
	  	return GetElement(Insur_Paidby_ddl_xpath);
	 }
	
	public  WebElement Insur_Reimby_ddl() {
	  	return GetElement(Insur_Reimby_ddl_xpath);
	 }
	
	public  WebElement Insur_Terms_txt() {
	  	return GetElement(Insur_Terms_txt_xpath);
	 }
	
	public  WebElement Insur_SpecExcept_txt() {
	  	return GetElement(Insur_SpecExcept_txt_xpath);
	 }
	
	//METHODS
	
	public  void Update_Insurance_Section_func(Service_Order_Info order, String button)
	{
		if(order.ServiceType.contains("HHG") == false)
		{
			switch (order.ServiceType)
			{
			case "POV":
				Update_Insurance_section_POV_func(order, "submit");
				break;
			
			default:
				Update_Insurance_Other_sections_func(order, "submit");
				break;
			}//end switch	
		}
		
	}
	
	private  void Update_Insurance_section_POV_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Insur_Edit_btn());
		
		
		ClearThenEnterValueToField(ValueInsur_Amount_txt(), Double.toString(order.Insure_ValueInsurAmount));
			
		ClearThenEnterValueToField(InsurAuth_Cap_txt(), Double.toString(order.Insure_InsurAuthCap_Amount));
			
		SelectDropdownItem(Insur_Paidby_ddl(), order.Insure_PaidBy, "");
		
		SelectDropdownItem(Insur_Reimby_ddl(), order.Insure_ReimBy, "");
		
		ClearThenEnterValueToField(Insur_Terms_txt(), order.Insure_Terms);
			
		ClearThenEnterValueToField(Insur_SpecExcept_txt(), order.Insure_Special);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Insur_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Service Dates",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
	}
	
	private  void Update_Insurance_Other_sections_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Insur_Edit_btn());
			
		ClearThenEnterValueToField(ValueInsur_Amount_txt(), Double.toString(order.Insure_ValueInsurAmount));
			
		ClearThenEnterValueToField(InsurAuth_Cap_txt(), Double.toString(order.Insure_InsurAuthCap_Amount));
			
		Optimize_ElementSendkey(DteValueInven_Received_txt(), order.Insure_DateVlueInvenReceiv);
		
		DteValueInven_Received_txt().sendKeys(Keys.ENTER);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Insur_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Service Dates",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
	}
	
	
	//=======================================METHODS VERIFY
	
	public  void Verify_OrderDetail_Insurance_func(Service_Order_Info order)
	{
		if(order.ServiceType.contains("HHG") == false)
		{
			switch (order.ServiceType)
			{
			case "POV":
				Verify_Insurance_section_POV_func(order);
				break;
			
			default:
				Verify_Insurance_Other_sections_func(order);
				break;
			}//end switch	
		}
	}
	
	private  void Verify_Insurance_Other_sections_func(Service_Order_Info order)
	{
		String field = "";
			
		field = "Value Insurance Amount";
		String Insure_Amount="";
		if (order.Insure_InsurAuthCap_Amount!=0.00)
		{
			Insure_Amount = Data_Optimize.Format_Double_WithCurrency(order.Insure_ValueInsurAmount);
			
		}
			
		VerifyFieldValueEqual_func(field,ValueInsur_Amount_txt(), Insure_Amount,"");
		
		
	
		field = "Insurance Authorized Cap";
		String Insurecap_amount_str = "";	
		if (order.Insure_InsurAuthCap_Amount!=0.00)
		{
			Insurecap_amount_str = Data_Optimize.Format_Double_WithCurrency(order.Insure_InsurAuthCap_Amount);
			
		}
		VerifyFieldValueEqual_func(field,InsurAuth_Cap_txt(), Insurecap_amount_str,"");
			
		field = "Date Valued Inventory Received";
		
			VerifyFieldValueEqual_func(field,DteValueInven_Received_txt(), order.Insure_DateVlueInvenReceiv,"");
	}
			
		
	private  void Verify_Insurance_section_POV_func(Service_Order_Info order)
	{
		if (order.ServiceType.contains("HHG") == false)
		{
			String field = "";
			
			field = "Value Insurance Amount";
			String Insure_Amount="";
			if (order.Insure_InsurAuthCap_Amount!=0.00)
			{
				Insure_Amount = Data_Optimize.Format_Double_WithCurrency(order.Insure_ValueInsurAmount);
				
			}
			
			VerifyFieldValueEqual_func(field,ValueInsur_Amount_txt(), Insure_Amount ,"");
			
						
			field = "Insurance Authorized Cap";
			String Insurecap_amount_str = "";	
			if (order.Insure_InsurAuthCap_Amount!=0.00)
			{
				Insurecap_amount_str = Data_Optimize.Format_Double_WithCurrency(order.Insure_InsurAuthCap_Amount);
				
			}
			VerifyFieldValueEqual_func(field,InsurAuth_Cap_txt(), Insurecap_amount_str,"");
			
			field = "Paid By";
			
			VerifyFieldValueEqual_func(field,Insur_Paidby_ddl(), order.Insure_PaidBy,"");
				
			field = "Reimbursed By";
				
			VerifyFieldValueEqual_func(field,Insur_Reimby_ddl(), order.Insure_ReimBy,"");
				
			field = "Terms";
				
			VerifyFieldValueEqual_func(field,Insur_Terms_txt(), order.Insure_Terms,"");
				
			field = "Special Exception";
				
			VerifyFieldValueEqual_func(field,Insur_SpecExcept_txt(), order.Insure_Special,"");
				
		}
	}
}
