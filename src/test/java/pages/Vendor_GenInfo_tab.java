package pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class Vendor_GenInfo_tab extends PageObjects{
	
	
	public Vendor_GenInfo_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	// Vendor detail
	private  String companyNameTitle_lb_xpath = "//a[@id='company_name']";
	
	// General Info _ Vendor Information
	
	private  String Heading_GenralInfo_tab_xpath = "//a[contains(@href,'#vendor-general-info')]";
	private  String vendorEdit_btn_xpath = "//a[@id='icon-general-infomation-panel']";
	private  String vendorName_txt_xpath = "//input[@placeholder='Enter vendor name']";
	private  String vendorCode_num_xpath = "//div[@org-placeholder='Enter vendor code']//input";
	private  String vendorService_drd_xpath = "//select[@id='vendor-services']";
	
	private  String vendorOwnership_drd_xpath = "//select[@id='vendor-ownership']";
	
	private  String vendorWebSite_txt_xpath = "//div[@org-placeholder='Enter website']//input";
	private  String updateVendorInfo_btn_xpath = "//div[@class='panel panel-border general-infomation-panel']//button[@data-control='update']";
	private  String cancelVendorInfo_btn_xpath = "//div[@class='panel panel-border general-infomation-panel']//button[@data-control='cancel']";

	// General Info _ Status
	private  String updateVendorStatus_btn_xpath = "//div[@id='status-panel']//button[@data-control='update']";
	private  String editVendorStatus_btn_xpath = "//a[@id='icon-status-panel']";
	private  String active_toggle_xpath = "//input[@id='vendor_is_active']";
	private  String available_toggle_xpath = "//input[@id='vendor_is_available']";
	private  String international_toggle_xpath = "//input[@id='vendor_is_international']";

	// General Info _ Rating
	private  String updateVendorRating_btn_xpath = "//div[@id='rating-panel']//button[@data-control='update']";
	private  String editVendorRating_btn_xpath = "//a[@id='icon-rating-panel']";
	private  String whyVendorRating_txt_xpath = "//textarea[@id='vendor-why']";
	private  String whyVendorRating_view_xpath = "//pre[@placeholder='Why']";
	private  String fiveVendorRating_icon_xpath = "//div[@class='panel panel-white tab-top']//div[@class='b__components b-ios b-rating']//li[4]";
	private  String fiveVendorRating_att_xpath = "//div[@class='panel panel-white tab-top']//div[@class='b__components b-ios b-rating']//li[4][@class='active']";

	
	
	
	//DEFINE ELEMENTS:
	public  WebElement Heading_GenralInfo_tab() {
		  return GetElement(Heading_GenralInfo_tab_xpath);
		 }
	
	public  WebElement companyNameTitle_lb() {
	  return GetElement(companyNameTitle_lb_xpath);
	 }
	public  WebElement vendorEdit_btn() {
	  return GetElement(vendorEdit_btn_xpath);
	 }
	public  WebElement vendorName_txt() {
	  return GetElement(vendorName_txt_xpath);
	 }
	public  WebElement vendorCode_num() {
	  return GetElement(vendorCode_num_xpath);
	 }
	public  WebElement vendorService_drd() {
	  return GetElement(vendorService_drd_xpath);
	 }
	public  WebElement vendorOwnership_drd() {
	  return GetElement(vendorOwnership_drd_xpath);
	 }
	
	public  WebElement vendorWebSite_txt() {
	  return GetElement(vendorWebSite_txt_xpath);
	 }
	public  WebElement updateVendorInfo_btn() {
	  return GetElement(updateVendorInfo_btn_xpath);
	 }
	public  WebElement cancelVendorInfo_btn() {
	  return GetElement(cancelVendorInfo_btn_xpath);
	 }
	public  WebElement updateVendorStatus_btn() {
	  return GetElement(updateVendorStatus_btn_xpath);
	 }
	public  WebElement editVendorStatus_btn() {
	  return GetElement(editVendorStatus_btn_xpath);
	 }
	public  WebElement active_toggle() {
	  return GetElement(active_toggle_xpath);
	 }
	public  WebElement available_toggle() {
	  return GetElement(available_toggle_xpath);
	 }
	public  WebElement international_toggle() {
	  return GetElement(international_toggle_xpath);
	 }
	public  WebElement updateVendorRating_btn() {
	  return GetElement(updateVendorRating_btn_xpath);
	 }
	public  WebElement editVendorRating_btn() {
	  return GetElement(editVendorRating_btn_xpath);
	 }
	public  WebElement whyVendorRating_txt() {
	  return GetElement(whyVendorRating_txt_xpath);
	 }
	public  WebElement whyVendorRating_view() {
	  return GetElement(whyVendorRating_view_xpath);
	 }
	public  WebElement fiveVendorRating_icon() {
	  return GetElement(fiveVendorRating_icon_xpath);
	 }
	public  WebElement fiveVendorRating_att() {
	  return GetElement(fiveVendorRating_att_xpath);
	 }
	
	
	
	//======METHODS
	public  void Verify_Vendor_Info_Is_Correct_ViewMode(Vendor_Info vendor)
	{
		
		String area = "Vendor Details - ";
		String fieldname = "vendorName_txt";
		VerifyFieldValueEqual_func(area+fieldname, vendorName_txt(),  vendor.VendorName, "");
		
		fieldname = "vendorName_txt";
		VerifyFieldValueEqual_func(area+fieldname, vendorService_drd(), vendor.VendorContactType, "");
		
		fieldname = "vendorName_txt";
		VerifyFieldValueEqual_func(area+fieldname, vendorCode_num(), vendor.VendorCode, "");
		
		fieldname = "vendorName_txt";
		VerifyFieldValueEqual_func(area+fieldname, vendorWebSite_txt(), vendor.Website, "");
				
		fieldname = "Active_toggle";
		String stt_tmp = "false";
		if(vendor.Status.equals("Active")) {
			stt_tmp = "true"; 
		}
		VerifyFieldValueEqual_func(area+fieldname, active_toggle(),stt_tmp, "");
			
	}
	
	
	public  void Update_Existing_Vendor_Info_func(Vendor_Info vendor,String button)
	{
		
		Fill_Vendor_General_Info(vendor, button);
		
			
	}
	
	private  void Fill_Vendor_General_Info(Vendor_Info vendor, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(vendorEdit_btn()!=null)
			Optimize_ElementClick(vendorEdit_btn());
		
		//vendorEdit_btn().click();		
		
		ClearThenEnterValueToField(vendorName_txt(), vendor.VendorName);
		
		String note_str = "Vendor Service List";
		
		SelectDropdownItem(vendorService_drd(), vendor.VendorContactType, note_str);
		
		ClearThenEnterValueToField(vendorCode_num(), vendor.VendorCode);
		
		ClearThenEnterValueToField(vendorWebSite_txt(), vendor.Website);
		
		
		if(button.equals("Submit")||button.equals("submit") )
		{
			updateVendorInfo_btn().click();
			
		}
		
		if(Messages_Notification.notification_msg()!=null)
		{
			String msg = Messages_Notification.notification_msg().getText();
			
			String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
			
			if(msg.equals(expected_msg))
			{
				TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Client Creation.Msg: '"+msg+"'.\n";
				Assert.fail(TestConfigs.glb_TCFailedMessage);
			}
		}//end if of message
		
		
	}
	


}
