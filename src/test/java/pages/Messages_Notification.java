package pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;

import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class Messages_Notification extends PageObjects{
	public Messages_Notification(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	private  String Actived_detail_mdal_xpath = "//div[contains(@class,'modal') and contains(@style,'display: block')]";
	
	private  String ConfirmNo_btn_xpath	= "//button[contains(text(),'Cancel')]"; //Updated: 5/29/2018;
	private  String ConfirmYes_btn_xpath	= "//button[contains(text(),'Yes')| contains(text(),'OK')]"; //Updated: 5/29/2018;
	private  String modalconfirmationheader_xpath	= "//h4[@class ='modal-title']";
	private  String modalmessagecontent_xpath	= "//div[@class = 'modal-body']";
	private  String PopupMessage_xpath	= "//div[@class='modal-dialog']//div[@class ='modal-body clearfix']";// | //div[@class ='modal-body']";
	private  String OppsOk_btn_xpath	= Actived_detail_mdal_xpath+"//button[contains(text(),'OK')]";
	private  String SuccessSubmitMessage_xpath	= "//div[contains(@class,'noti-success-content')]";
	private  String Servercache_msg_xpath = "//span[contains(text(), 'No such file or directory')]";

	private  String ContLastMove_lnk_xpath ="//a[@id = 'has-last-move-plan-in-modal']";
	private  String StartNewPlan_btn_xpath = "//button[@id='submit-exactly-in-modal']";
	private  String Close_btn_xpath = "//div[@class = 'modal-content']//button[@class ='close']";

	private  String notification_msg_xpath = "//div[@class = 'toast toast-success']";

	private  String Delete_btn_xpath = Actived_detail_mdal_xpath+ "//button[contains(text(),'Remove') or contains(text(),'Delete')]";
	private  String DeleteItem_btn_xpath = "//button[@data-control='delete']";

	private  String NoResultMsg_lb_xpath = "//td[@class='text-center']";
	//private  String String Close_btn_xpath = "//div[@class='modal-dialog modal-md']//i[@class='icon-close']"


	//==================Message Type
	public  String SystemError_msg = "Server error Request. Please try again or contact IT helpdesk.";
	public  String noResutl_msg = "No matching records";
	public  String newContactToast_msg = "New Vendor Contact has been created successfully.";
	public  String updateVendor_msg = "Vendor has been updated successfully.";
	public  String updateContact_msg = "Vendor Contact has been updated successfully.";

	
	
	//DEFINE ELEMENTS:
	public  WebElement ConfirmNo_btn() {
	  return GetElement(ConfirmNo_btn_xpath);
	 }
	public  WebElement ConfirmYes_btn() {
	  return GetElement(ConfirmYes_btn_xpath);
	 }
	public  WebElement modalconfirmationheader() {
	  return GetElement(modalconfirmationheader_xpath);
	 }
	public  WebElement modalmessagecontent() {
	  return GetElement(modalmessagecontent_xpath);
	 }
	public  WebElement PopupMessage() {
	  return GetElement(PopupMessage_xpath);
	 }
	public  WebElement OppsOk_btn() {
	  return GetElement(OppsOk_btn_xpath);
	 }
	public  WebElement SuccessSubmitMessage() {
	  return GetElement(SuccessSubmitMessage_xpath);
	 }
	public  WebElement Servercache_msg () {
	  return GetElement(Servercache_msg_xpath);
	 }
	public  WebElement ContLastMove_lnk () {
	  return GetElement(ContLastMove_lnk_xpath);
	 }
	public  WebElement StartNewPlan_btn () {
	  return GetElement(StartNewPlan_btn_xpath);
	 }
	public  WebElement Close_btn () {
	  return GetElement(Close_btn_xpath);
	 }
	public  WebElement notification_msg () {
	  return GetElement(notification_msg_xpath);
	 }
	public  WebElement Delete_btn () {
	  return GetElement(Delete_btn_xpath);
	 }
	public  WebElement DeleteItem_btn () {
	  return GetElement(DeleteItem_btn_xpath);
	 }
	public  WebElement NoResultMsg_lb () {
	  return GetElement(NoResultMsg_lb_xpath);
	 }

			
			//=====================================================FUNCTION=================
			//======================================================
			//=============================

			public  void VerifyNotificationMessageDisplay_func(String Expected_msg,String note)
			{

				if(notification_msg()==null)
				{

					TestConfigs.glb_TCStatus = false;
					TestConfigs.glb_TCFailedMessage += "The Notification disappeared too fast or is not found. ";
				}
				else
				{
					if(!notification_msg().getText().contains(Expected_msg))
					{
						TestConfigs.glb_TCStatus = false;
						TestConfigs.glb_TCFailedMessage += "Incorrect Notification content.[Observed:"+notification_msg().getText()+"-Expected:"+Expected_msg+"]."+ " "+note;
					}
				}
			}

			public  void VerifyNotifyMsgNotDisplay_func(String Expected_msg,String note,boolean stoprun)
			{
			if(notification_msg()!=null)
				{
					String msg_tmp = notification_msg().getText();
					if(msg_tmp.contains(Expected_msg))
					{
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}//catch
					
						TestConfigs.glb_TCFailedMessage += "Notify Message Should NOT Display.[Observed:"+msg_tmp+"-Expected:"+Expected_msg+"]."+ " "+note+".\n";
				
					}//if gettext
						
					
					if(stoprun == true)
					{
						Assert.fail(TestConfigs.glb_TCFailedMessage);
					}
				
				}//if!null


	}//end void
			
			public  void Wait_Until_Success_Notificaiton_NotDisplay()
			{

				if(notification_msg()!=null &&notification_msg().isDisplayed())
				{
					Wait_Ultil_ElementNotDisplay(notification_msg_xpath);
				}
			}
			public  void VerifyPopUpMessageDisplay_func(String Expected_msg)
			{
				if(PopupMessage()==null)
				{
					TestConfigs.glb_TCStatus = false;
					TestConfigs.glb_TCFailedMessage += "Opps message pop-up does not display.";
					return;
				}

				if (PopupMessage().getText() != Expected_msg) {
					String result_message = ((((("The Oops messages do not match.Observed: " + PopupMessage().getText()) + "-") + "Expected: ") +
							Expected_msg) + ".");

					TestConfigs.glb_TCStatus = false;
					TestConfigs.glb_TCFailedMessage += result_message;

					if(OppsOk_btn()!=null)
					{
						OppsOk_btn().click();
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			}//and func

}
