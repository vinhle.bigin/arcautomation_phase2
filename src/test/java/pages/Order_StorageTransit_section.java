package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;

public class Order_StorageTransit_section extends PageObjects{
	

	public Order_StorageTransit_section(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Storage_Edit_btn_xpath = "//a[@id='icon-hhg-order-storage-in-transit']";
	private  String Storage_Update_btn_xpath = "//div[@id='hhg-order-storage-in-transit']//button[@data-control='update']";
	private  String Storage_Cancel_btn_xpath = "//div[@id='hhg-order-storage-in-transit']//button[@data-control='cancel-update']";
	private  String DayAuth_txt_xpath = "//div[@org-placeholder='Days Authorized']//input";
	private  String AtOrigin_txt_xpath = "//div[@org-placeholder='At Origin']//input";
	private  String AtDes_txt_xpath = "//div[@org-placeholder='At Destination']//input";
	private  String PaidBy_slect_xpath = "//select[@placeholder='Paid by']";
	private  String ReimBy_slect_xpath = "//select[@placeholder='Reimbursed by']";
	private  String Terms_txt_xpath = "//div[@org-placeholder='Terms']//textarea";
	private  String UpdatedTerms_txt_xpath = "//pre[@placeholder='Terms']";
	private  String SpecExcept_txt_xpath = "//div[@org-placeholder='Special Exceptions']//textarea";
	private  String UpdatedSpecExcept_txt_xpath = "//pre[@placeholder='Special Exceptions']";
	
	
	
	
	public  WebElement Storage_Edit_btn() {
	  	return GetElement(Storage_Edit_btn_xpath);
	 }
	
	public  WebElement Storage_Update_btn() {
	  	return GetElement(Storage_Update_btn_xpath);
	 }
	
	public  WebElement Storage_Cancel_btn() {
	  	return GetElement(Storage_Cancel_btn_xpath);
	 }
	
	public  WebElement DayAuth_txt() {
	  	return GetElement(DayAuth_txt_xpath);
	 }
	
	public  WebElement AtOrigin_txt() {
	  	return GetElement(AtOrigin_txt_xpath);
	 }
	
	public  WebElement AtDes_txt() {
	  	return GetElement(AtDes_txt_xpath);
	 }
	
	public  WebElement PaidBy_slect() {
	  	return GetElement(PaidBy_slect_xpath);
	 }
	
	public  WebElement ReimBy_slect() {
	  	return GetElement(ReimBy_slect_xpath);
	 }
	
	public  WebElement Terms_txt() {
	  	return GetElement(Terms_txt_xpath);
	 }
	
	public  WebElement UpdatedTerms_txt() {
	  	return GetElement(UpdatedTerms_txt_xpath);
	 }
	
	public  WebElement SpecExcept_txt() {
	  	return GetElement(SpecExcept_txt_xpath);
	 }
	
	public  WebElement UpdatedSpecExcept_txt() {
	  	return GetElement(UpdatedSpecExcept_txt_xpath);
	 }
	
	public  void Update_StorageTransit_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		if (order.ServiceType.contains("UAB") == true || order.ServiceType.contains("HHE") == true)
		{
			Optimize_ElementClick(Storage_Edit_btn());
				
			ClearThenEnterValueToField(DayAuth_txt(), order.StorgeTransit_DaysAuth);
				
			ClearThenEnterValueToField(AtOrigin_txt(), order.AtOrigin);
				
			ClearThenEnterValueToField(AtDes_txt(), order.AtDes);
				
			SelectDropdownItem(PaidBy_slect(), order.PaidBy, "");
				
			SelectDropdownItem(ReimBy_slect(), order.ReimbursedBy, "");
				
			ClearThenEnterValueToField(Terms_txt(), order.Terms);
				
			ClearThenEnterValueToField(SpecExcept_txt(), order.SpecialExpect);
			
			if(button.equals("Submit")||button.equals("submit"))
			{
				Optimize_ElementClick(Storage_Update_btn());
				
				Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Storage Transit",false);
				
				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			}
		}
	}
	
	//=======================================METHODS VERIFY
	
	public  void Verify_OrderDetail_StorageTransit_func(Service_Order_Info order)
	{
		if (order.ServiceType.contains("UAB") == true || order.ServiceType.contains("HHE") == true)
		{
			String field = "";
				
			field = "Days Authorized";
			
			VerifyFieldValueEqual_func(field,DayAuth_txt(), order.StorgeTransit_DaysAuth,"");
				
			field = "At Origin";
			
			VerifyFieldValueEqual_func(field,AtOrigin_txt(), order.AtOrigin,"");
				
			field = "At Destination";
			
			VerifyFieldValueEqual_func(field,AtDes_txt(), order.AtDes,"");
				
			field = "Paid By";
				
			VerifyFieldValueEqual_func(field,PaidBy_slect(), order.PaidBy,"");
				
			field = "Reimbursed By";
				
			VerifyFieldValueEqual_func(field,ReimBy_slect(), order.ReimbursedBy,"");
				
			field = "Terms";
				
			VerifyFieldValueEqual_func(field,UpdatedTerms_txt(), order.Terms,"");
				
			field = "Special Exceptions";
				
			VerifyFieldValueEqual_func(field,UpdatedSpecExcept_txt(), order.SpecialExpect,"");
		}	
	}

}
