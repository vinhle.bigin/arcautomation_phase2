package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import configuration.DriverConfig.DriverBase;

public class Transf_AddrDesRes_sect extends PageObjects{
	
	
	
	public Transf_AddrDesRes_sect(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

		///DESTINATION RESIDENCE
		private  String DesRes_Update_btn_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//button[@data-control = 'update']";
		private  String DesRes_Cancel_btn_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//button[@data-control = 'cancel-update']";
		private  String DesRes_Addrs1_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter Address 1']//input";
		private  String DesRes_Addrs2_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter Address 2']//input";
		private  String DesRes_City_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter City']//input";
		private  String DesRes_State_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter State']//select";
		private  String DesRes_Zip_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter Zip']//input";
		private  String DesRes_Country_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter Country']//select";
		private  String DesRes_IsBilling_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][1]/div[1]//input";
		private  String DesRes_IsShipping_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][1]/div[2]//input";
		private  String DesRes_IsMailing_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][1]/div[3]//input";
		private  String DesRes_IsPrimary_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][1]/div[4]//input";
		private  String DesRes_IsSendMailPayment_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][2]/div[1]//input";
		private  String DesRes_IsSendMailTaxDoc_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][2]/div[2]//input";
		private  String DesRes_IsSendMailService_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][2]/div[3]//input";
		private  String DesRes_Edit_btn_xpath= "//a[@id = 'icon-edit-transferee-destination-residence-address']";



	
	public  WebElement DesRes_Edit_btn() {
		  return GetElement(DesRes_Edit_btn_xpath);
		 }
	public  WebElement DesRes_Update_btn() {
		  return GetElement(DesRes_Update_btn_xpath);
		 }
	public  WebElement DesRes_Cancel_btn() {
		  return GetElement(DesRes_Cancel_btn_xpath);
		 }
	public  WebElement DesRes_Addrs1() {
		  return GetElement(DesRes_Addrs1_xpath);
		 }
	public  WebElement DesRes_Addrs2() {
		  return GetElement(DesRes_Addrs2_xpath);
		 }
	public  WebElement DesRes_City() {
		  return GetElement(DesRes_City_xpath);
		 }
	public  WebElement DesRes_State() {
		  return GetElement(DesRes_State_xpath);
		 }
	public  WebElement DesRes_Zip() {
		  return GetElement(DesRes_Zip_xpath);
		 }
	public  WebElement DesRes_Country() {
		  return GetElement(DesRes_Country_xpath);
		 }
	public  WebElement DesRes_IsBilling() {
		  return GetElement(DesRes_IsBilling_xpath);
		 }
	public  WebElement DesRes_IsShipping() {
		  return GetElement(DesRes_IsShipping_xpath);
		 }
	public  WebElement DesRes_IsMailing() {
		  return GetElement(DesRes_IsMailing_xpath);
		 }
	public  WebElement DesRes_IsPrimary() {
		  return GetElement(DesRes_IsPrimary_xpath);
		 }
	public  WebElement DesRes_IsSendMailPayment() {
		  return GetElement(DesRes_IsSendMailPayment_xpath);
		 }
	public  WebElement DesRes_IsSendMailTaxDoc() {
		  return GetElement(DesRes_IsSendMailTaxDoc_xpath);
		 }
	public  WebElement DesRes_IsSendMailService() {
		  return GetElement(DesRes_IsSendMailService_xpath);
		 }
	
	
	public  void Edit_TransfAddrInfo_func(Address_Info addr)
	{
		try
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";

		//	Wait_For_ElementDisplay(DesRes_Edit_btn());

			Optimize_ElementClick(DesRes_Edit_btn());

			ScrollElementtoViewPort_func(DesRes_Addrs1());
			ClearThenEnterValueToField(DesRes_Addrs1(), addr.Addr1);

			ClearThenEnterValueToField(DesRes_Addrs2(), addr.Addr2);

			ClearThenEnterValueToField(DesRes_City(), addr.City);

			ClearThenEnterValueToField(DesRes_Zip(), addr.Zip);

			String note_str = "Transferee Des Res Address Tab - State Dropdown.";
			
			SelectDropdownItem(DesRes_State(),  addr.State,note_str);
			
			note_str = "Transferee Des Res Address Tab - Country Dropdown.";

			SelectDropdownItem(DesRes_Country(), addr.Country,note_str);

			//addr.Country = "Viet Nam"
			//ClearThenEnterValueToField()

			DesRes_Update_btn().click();

			Thread.sleep(5000);

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(expected_msg,"Unsuccessfull DesRes - Info update",false);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}//end void
	
	public  void VerifyInfoViewMode_func(Address_Info addr)
	{
		String area = "";
		String field_name = "";
		String mode = "View Mode";

	

		///===================================================DESTINATION RESIDENCE
		area = "Des Res - ";
		field_name = "DesRes_Addrs1";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_Addrs1(),addr.Addr1 , mode);

		field_name = "DesRes_Addrs2";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_Addrs2(),addr.Addr2 , mode);

		field_name = "DesRes_City";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_City(),addr.City , mode);

		field_name = "DesRes_Zip";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_Zip(),addr.Zip , mode);

		field_name = "DesRes_State";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_State(),addr.State , mode);

		field_name = "DesRes_Country";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_Country(),addr.Country , mode);

		field_name = "DesRes_IsBilling";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_IsBilling(),addr.IsAtri_Billing.toString() , mode);

		field_name = "DesRes_IsShipping";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_IsShipping(),addr.IsAtri_Shipping.toString() , mode);

		field_name = "DesRes_IsMailing";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_IsMailing(),addr.IsAtri_Mailing.toString() , mode);

		field_name = "DesRes_IsPrimary";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_IsPrimary(),addr.IsAtri_Primary.toString() , mode);

		field_name = "DesRes_IsSendMailPayment";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_IsSendMailPayment(),addr.IsSendMailPayment.toString() , mode);

		field_name = "DesRes_IsSendMailService";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_IsSendMailService(),addr.IsSendMailServices.toString() , mode);

		field_name = "DesRes_IsSendMailTaxDoc";
		VerifyFieldValueEqual_func(area+field_name ,DesRes_IsSendMailTaxDoc(),addr.IsSendMailTaxDoc.toString() , mode);

	}

}
