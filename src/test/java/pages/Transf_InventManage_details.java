package pages;

import java.text.ParseException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.HSService_Info;
import businessObjects.InventManageService_Info;
import configuration.DriverConfig.DriverBase;
import custom_Func.Data_Optimize;
import custom_Func.DateTime_Manage;

public class Transf_InventManage_details extends PageObjects{
	
	
	public Transf_InventManage_details(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private  String ListSale_CopyFromHS_btn_xpath = "//button[contains(text(),'Copy from Home Sale')]";
	private  String ListSale_Update_btn_xpath = "//div[@id='loading-hbo-inventory-home-listing-panel']//button[@data-control='update']";
	private  String ListSale_Cancel_btn_xpath = "//div[@id='loading-hbo-inventory-home-listing-panel']//button[@data-control='cancel-update']";
	private  String ListSale_Edit_btn_xpath = "//a[@id='icon-hbo-inventory-home-listing-panel']";
	private  String HSMnage_CurrentListPrice_vlue_xpath = "//div[@org-placeholder='Current Listing Price']//input";
	private  String HSMnage_BOutOfferPrice_vlue_xpath = "//div[@org-placeholder='Buyout Offer Price']//input";
	private  String HSMnage_EstSellPrice_vlue_xpath = "//div[@org-placeholder='Estimated Selling Price']//input";
	private  String HSMnage_CurrentDayMarket_vlue_xpath = "//div[@org-placeholder='Current Days On Market']//input";
	private  String HSMnage_AvrgDayMarket_vlue_xpath = "//div[@org-placeholder='Average Days On Market']//input";
	
	private  String HSAnlyze_CrrentList_ddl_xpath = "//div[contains(@org-placeholder,'Currently listed')]//select";
	private  String HSAnlyze_PropertyType_ddl_xpath = "//div[contains(@org-placeholder,'Property Type')]//select";
	private  String HSAnlyze_LocationType_ddl_xpath = "//div[contains(@org-placeholder,'Location Type')]//select";
	private  String HSAnlyze_ListCompany_ddl_xpath = "//div[contains(@org-placeholder,'Listing Company')]//input";
	private  String HSAnlyze_IndicatePerson_txt_xpath = "//div[contains(@org-placeholder,'Indicate any personal')]//textarea";
	private  String HSAnlyze_ImproveMade_txt_xpath = "//div[contains(@org-placeholder,'Improvements made')]//textarea";
	private  String HSAnlyze_PropertyAddInfo_txt_xpath = "//div[contains(@org-placeholder,'additional Information/Comment')]//textarea";
	private  String HSAnlyze_PropertyGenInfo_txt_xpath = "//div[contains(@org-placeholder,'Property General Condition')]//textarea";
	private  String HSAnlyze_InterCondt_txt_xpath = "//div[contains(@org-placeholder,'Interior Condition')]//textarea";
	private  String HSAnlyze_ExterCondt_txt_xpath = "//div[contains(@org-placeholder,'Exterior Condition')]//textarea";
	private  String HSAnlyze_Inspect_txt_xpath = "//div[contains(@org-placeholder,'Inspections/Disclosures')]//textarea";
	private  String HSAnlyze_PropertyFnance_ddl_xpath = "//div[contains(@org-placeholder,'Property') and contains(@org-placeholder,'Financing')]//select";
	private  String HSAnlyze_FnanceDesr_txt_xpath = "//div[contains(@org-placeholder,'Financing Description')]//input";
	private  String HSAnlyze_IsCmmonInterest_opt_xpath = "//label[contains(text(),'Common interest development')]/..//input";
	private  String HSAnlyze_OwnersPercent_txt_xpath = "//div[contains(@org-placeholder,'Owners percentage')]//input";
	private  String HSAnlyze_InvestorsPercent_txt_xpath = "//div[contains(@org-placeholder,'Investors percentage')]//input";
	private  String HSAnlyze_HOAFee_txt_xpath = "//div[contains(@org-placeholder,'HOA Fee')]//input";
	private  String HSAnlyze_HOAFeePaid_txt_xpath = "//div[contains(@org-placeholder,'HOA Fee Paid Frequency')]//select";
	private  String HSAnlyze_PriceRangeFrom_txt_xpath = "//div[contains(@org-placeholder,'Price Range From')]//input";
	private  String HSAnlyze_PriceRangeTo_txt_xpath = "//div[contains(@org-placeholder,'Price Range To')]//input";
	private  String HSAnlyze_AverageDay_txt_xpath = "//div[@id= 'hbo-inventory-home-sale-analysis-panel']//div[contains(@org-placeholder,'Average Days')]//input";
	private  String HSAnlyze_SgestSellPrice_txt_xpath = "//div[contains(@org-placeholder,'Sugessted Selling Price')]//input";
	private  String HSAnlyze_SgestListPrice_txt_xpath = "//div[contains(@org-placeholder,'Suggested Listing Price')]//input";
	private  String HSAnlyze_MrketCondtCmment_txt_xpath = "//div[contains(@org-placeholder,'Market Conditions Comment')]//textarea";
	private  String HSAnlyze_Update_btn_xpath = "//div[contains(@id,'inventory-home-sale-analysis')]//button[@data-control='update']";
	private  String HSAnlyze_Cancel_btn_xpath = "//div[contains(@id,'inventory-home-sale-analysis')]//button[@data-control='cancel-update']";
	private  String HSAnlyze_Edit_btn_xpath = "//a[contains(@id,'inventory-home-sale-analysis')]";


	public  WebElement HSAnlyze_OwnersPercent_txt() {
		  return GetElement(HSAnlyze_OwnersPercent_txt_xpath);
		 }
	public  WebElement HSAnlyze_InvestorsPercent_txt() {
		  return GetElement(HSAnlyze_InvestorsPercent_txt_xpath);
		 }
	public  WebElement ListSale_CopyFromHS_btn() {
	  return GetElement(ListSale_CopyFromHS_btn_xpath);
	 }
	public  WebElement ListSale_Update_btn() {
		  return GetElement(ListSale_Update_btn_xpath);
		 }
	public  WebElement ListSale_Cancel_btn() {
		  return GetElement(ListSale_Cancel_btn_xpath);
		 }
	public  WebElement ListSale_Edit_btn() {
		  return GetElement(ListSale_Edit_btn_xpath);
		 }
	public  WebElement HSMnage_CurrentListPrice_vlue() {
		  return GetElement(HSMnage_CurrentListPrice_vlue_xpath);
		 }
	public  WebElement HSMnage_BOutOfferPrice_vlue() {
		  return GetElement(HSMnage_BOutOfferPrice_vlue_xpath);
		 }
	public  WebElement HSMnage_EstSellPrice_vlue() {
		  return GetElement(HSMnage_EstSellPrice_vlue_xpath);
		 }
	public  WebElement HSMnage_CurrentDayMarket_vlue() {
		  return GetElement(HSMnage_CurrentDayMarket_vlue_xpath);
		 }
	public  WebElement HSMnage_AvrgDayMarket_vlue() {
		  return GetElement(HSMnage_AvrgDayMarket_vlue_xpath);
		 }
	public  WebElement HSAnlyze_CrrentList_ddl() {
		  return GetElement(HSAnlyze_CrrentList_ddl_xpath);
		 }
	public  WebElement HSAnlyze_PropertyType_ddl() {
		  return GetElement(HSAnlyze_PropertyType_ddl_xpath);
		 }
	public  WebElement HSAnlyze_LocationType_ddl() {
		  return GetElement(HSAnlyze_LocationType_ddl_xpath);
		 }
	public  WebElement HSAnlyze_ListCompany_ddl() {
		  return GetElement(HSAnlyze_ListCompany_ddl_xpath);
		 }
	public  WebElement HSAnlyze_IndicatePerson_txt() {
		  return GetElement(HSAnlyze_IndicatePerson_txt_xpath);
		 }
	public  WebElement HSAnlyze_ImproveMade_txt() {
		  return GetElement(HSAnlyze_ImproveMade_txt_xpath);
		 }
	public  WebElement HSAnlyze_PropertyAddInfo_txt() {
		  return GetElement(HSAnlyze_PropertyAddInfo_txt_xpath);
		 }
	public  WebElement HSAnlyze_PropertyGenInfo_txt() {
		  return GetElement(HSAnlyze_PropertyGenInfo_txt_xpath);
		 }
	public  WebElement HSAnlyze_InterCondt_txt() {
		  return GetElement(HSAnlyze_InterCondt_txt_xpath);
		 }
	public  WebElement HSAnlyze_ExterCondt_txt() {
		  return GetElement(HSAnlyze_ExterCondt_txt_xpath);
		 }
	public  WebElement HSAnlyze_Inspect_txt() {
		  return GetElement(HSAnlyze_Inspect_txt_xpath);
		 }
	public  WebElement HSAnlyze_PropertyFnance_ddl() {
		  return GetElement(HSAnlyze_PropertyFnance_ddl_xpath);
		 }
	public  WebElement HSAnlyze_FnanceDesr_txt() {
		  return GetElement(HSAnlyze_FnanceDesr_txt_xpath);
		 }
	public  WebElement HSAnlyze_IsCmmonInterest_opt() {
		  return GetElement(HSAnlyze_IsCmmonInterest_opt_xpath);
		 }
	public  WebElement HSAnlyze_HOAFee_txt() {
		  return GetElement(HSAnlyze_HOAFee_txt_xpath);
		 }
	public  WebElement HSAnlyze_HOAFeePaid_txt() {
		  return GetElement(HSAnlyze_HOAFeePaid_txt_xpath);
		 }
	public  WebElement HSAnlyze_PriceRangeFrom_txt() {
		  return GetElement(HSAnlyze_PriceRangeFrom_txt_xpath);
		 }
	public  WebElement HSAnlyze_PriceRangeTo_txt() {
		  return GetElement(HSAnlyze_PriceRangeTo_txt_xpath);
		 }
	public  WebElement HSAnlyze_AverageDay_txt() {
		  return GetElement(HSAnlyze_AverageDay_txt_xpath);
		 }
	public  WebElement HSAnlyze_SgestSellPrice_txt() {
		  return GetElement(HSAnlyze_SgestSellPrice_txt_xpath);
		 }
	public  WebElement HSAnlyze_SgestListPrice_txt() {
		  return GetElement(HSAnlyze_SgestListPrice_txt_xpath);
		 }
	public  WebElement HSAnlyze_MrketCondtCmment_txt() {
		  return GetElement(HSAnlyze_MrketCondtCmment_txt_xpath);
		 }
	public  WebElement HSAnlyze_Update_btn() {
		  return GetElement(HSAnlyze_Update_btn_xpath);
		 }
	public  WebElement HSAnlyze_Cancel_btn() {
		  return GetElement(HSAnlyze_Cancel_btn_xpath);
		 }
	public  WebElement HSAnlyze_Edit_btn() {
		  return GetElement(HSAnlyze_Edit_btn_xpath);
		 }
	
	
	public  void Update_Home_Listing_Sale_section_func(HSService_Info HS_service_info, boolean iscopyHS,String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(ListSale_Edit_btn());
		
		if(iscopyHS==true)
		{
			String message_str = "You are about to override all the vendor list and the Listing and Sale information of Inventory\r\n" + 
					"\n" + "Management screen by Home Sale screen. Do you want to proceed?";
			
			ListSale_CopyFromHS_btn().click();
			
			Messages_Notification.VerifyPopUpMessageDisplay_func(message_str);
			
			Optimize_ElementClick(Messages_Notification.OppsOk_btn());
			
		}
		
		else if(HS_service_info!=null)
		{
			new Transf_HomeSale_details(driverbase).FillHSInfo_func(HS_service_info, "");
			
		}
		
		if(button.equals("Submit")||button.equals("submit"))
			Optimize_ElementClick(ListSale_Update_btn());
		
		
		else if(button.equals("Cancel"))
			Optimize_ElementClick(ListSale_Cancel_btn());
		
	
		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "when update InventManage_ListPrice_section",false);
	
	}
	
	public  void Update_Home_Sale_Analysis_section_func(InventManageService_Info IM_service_info, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(HSAnlyze_Edit_btn());
		
		SelectDropdownItem(HSAnlyze_PropertyType_ddl(), IM_service_info.HSAnal_PropertyType,"");
		
		SelectDropdownItem(HSAnlyze_LocationType_ddl(), IM_service_info.HSAnal_LocationType, "");
		
		ClearThenEnterValueToField(HSAnlyze_ListCompany_ddl(), IM_service_info.HSAnal_ListCompAgent);
		
		ClearThenEnterValueToField(HSAnlyze_IndicatePerson_txt(), IM_service_info.HSAnal_Indicate);
		
		ClearThenEnterValueToField(HSAnlyze_ImproveMade_txt(), IM_service_info.HSAnal_Improvements);
		
		ClearThenEnterValueToField(HSAnlyze_PropertyAddInfo_txt(), IM_service_info.HSAnal_PropertyInfo);
		
		ClearThenEnterValueToField(HSAnlyze_PropertyGenInfo_txt(), IM_service_info.HSAnal_ProperCondition);
		
		ClearThenEnterValueToField(HSAnlyze_InterCondt_txt(), IM_service_info.HSAnal_InteriorCondition);
		
		ClearThenEnterValueToField(HSAnlyze_ExterCondt_txt(), IM_service_info.HSAnal_ExeriorCondition);
		
		ClearThenEnterValueToField(HSAnlyze_Inspect_txt(), IM_service_info.HSAnal_Inspections);
		
		SelectDropdownItem(HSAnlyze_PropertyFnance_ddl(), IM_service_info.HSAnal_PropertyFinanceMeans, "");
		
		ClearThenEnterValueToField(HSAnlyze_FnanceDesr_txt(), IM_service_info.HSAnal_FinanceDescript);
		
		Optimize_ElementClick(HSAnlyze_IsCmmonInterest_opt());
		
		ClearThenEnterValueToField(HSAnlyze_OwnersPercent_txt(), IM_service_info.HSAnal_OwnersPercent);
		
		ClearThenEnterValueToField(HSAnlyze_InvestorsPercent_txt(), IM_service_info.HSAnal_InvestorsPercent);
		
		ClearThenEnterValueToField(HSAnlyze_HOAFee_txt(), IM_service_info.HSAnal_HOAFee);
		
		HSAnlyze_HOAFee_txt().sendKeys(Keys.TAB);
		
		SelectDropdownItem(HSAnlyze_HOAFeePaid_txt(), IM_service_info.HSAnal_HOAFeePaidFrequent, "");
		
		ClearThenEnterValueToField(HSAnlyze_PriceRangeFrom_txt(), IM_service_info.HSAnal_PriceRangeFrom);
		
		ClearThenEnterValueToField(HSAnlyze_PriceRangeTo_txt(), IM_service_info.HSAnal_PriceRangeTo);
		
		ClearThenEnterValueToField(HSAnlyze_AverageDay_txt(), IM_service_info.HSAnal_AverageDays);
		
		ClearThenEnterValueToField(HSAnlyze_SgestSellPrice_txt(), IM_service_info.HSAnal_SugesstSellPrice);
		
		ClearThenEnterValueToField(HSAnlyze_SgestListPrice_txt(), IM_service_info.HSAnal_SugesstListPrice);
		
		ClearThenEnterValueToField(HSAnlyze_MrketCondtCmment_txt(), IM_service_info.HSAnal_MarketConditionsCmt);
		
		if(button.equals("Submit")||button.equals("submit"))
			Optimize_ElementClick(HSAnlyze_Update_btn());
					
		else if(button.equals("Cancel"))
			Optimize_ElementClick(HSAnlyze_Cancel_btn());
		
	
		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "when update Home Sale Analysis section",false);
	
		
	}
	

//=======================VERIFY METHODS
public  void Verify_HSManage_Info_Is_Correct_func(InventManageService_Info InventMnage_info,double BuyOutOfferPrice_str)
{

	try {
		
		String fieldname_str = "InventMnage_info";
		
		VerifyFieldValueEqual_func(fieldname_str, HSMnage_CurrentListPrice_vlue(), InventMnage_info.HS_ListSale_section.ListPrice, "");
	
		fieldname_str = "HSMnage_BOutOfferPrice_vlue";
		
		String offer_price_str = Data_Optimize.Format_Double_WithCurrency(BuyOutOfferPrice_str);
		
		VerifyFieldValueEqual_func(fieldname_str, HSMnage_BOutOfferPrice_vlue(), offer_price_str, "");
	
		fieldname_str = "HSMnage_EstSellPrice_vlue";
		
		VerifyFieldValueEqual_func(fieldname_str, HSMnage_EstSellPrice_vlue(), InventMnage_info.HSAnal_SugesstSellPrice, "");
	
		fieldname_str = "HSMnage_CurrentDayMarket_vlue";//Today - List StartDate
				
		String days_numb;
		
			days_numb = DateTime_Manage.Calculate_Days_Numb(InventMnage_info.HS_ListSale_section.ListStartDate);
		
		
		VerifyFieldValueEqual_func(fieldname_str, HSMnage_CurrentDayMarket_vlue(), days_numb, "");
	
		
		fieldname_str = "HSMnage_EstSellPrice_vlue";
		
		VerifyFieldValueEqual_func(fieldname_str, HSMnage_AvrgDayMarket_vlue(), InventMnage_info.HSAnal_AverageDays, "");
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}


}
