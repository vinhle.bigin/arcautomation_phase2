package pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Template_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class Template_page extends PageObjects {
	

	
	
	//Landing page

		public Template_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


		private  String Search_btn_xpath= "//button[@data-control ='search']";
		private  String New_btn_xpath= "//a[@href ='#/new']";
		private  String Search_TemplName_txt_xpath= "//input[@placeholder ='Template Name']";
		private  String Templ_List_tbl_xpath= "//table[@id = 'template-table']/tbody";
		private  String Search_TemplType_ddl_xpath= "//select[@placeholder ='Type']";
		private  String Search_Target_ddl_xpath= "//select[@placeholder ='Target Record']";
		private  String Search_Status_ddl_xpath= "//select[@placeholder ='Status']";
		private  String SearchClear_btn_xpath= "//button[@data-control ='cancel-search']";
		
	


	
	
	public  WebElement Search_btn() {
	  return GetElement(Search_btn_xpath);
	 }
public  WebElement New_btn() {
	  return GetElement(New_btn_xpath);
	 }
public  WebElement Search_TemplName_txt() {
	  return GetElement(Search_TemplName_txt_xpath);
	 }
public  WebElement Templ_List_tbl() {
	  return GetElement(Templ_List_tbl_xpath);
	 }
public  WebElement Search_TemplType_ddl() {
	  return GetElement(Search_TemplType_ddl_xpath);
	 }
public  WebElement Search_Target_ddl() {
	  return GetElement(Search_Target_ddl_xpath);
	 }
public  WebElement Search_Status_ddl() {
	  return GetElement(Search_Status_ddl_xpath);
	 }
public  WebElement SearchClear_btn() {
	  return GetElement(SearchClear_btn_xpath);
	 }

		



	public  WebElement Remove_icon(String tpl_name) {
			
				String xpath_str = Templ_List_tbl_xpath +"//a[contains(text(),'"+tpl_name+"')]/././/a[@data-original-title= 'Remove']";
				WebElement temp_element = GetElement(xpath_str);
				return temp_element;
			
		}


		public  WebElement TemplName_link(String tpl_name) {
		
				String xpath_str = Templ_List_tbl_xpath +"//a[contains(text(),'"+tpl_name+"')]";
				WebElement temp_element =  GetElement(xpath_str); 
				return temp_element;
			
		}

	
		
		
/////////================================METHOD
	public void SearchTemplate_func(String SearchType, Template_Info template)
	{
		String note_str = "";
		try
		{
		
		switch(SearchType)
		{
			case "Type":
				note_str = "Template  - Template Type Dropdown.";
				
				SelectDropdownItem(Search_TemplType_ddl(), template.Type ,note_str);
				break;
			case "Target":
				note_str = "Template  - Target Type Dropdown.";
				
				SelectDropdownItem(Search_Target_ddl(), template.Target,note_str );
				
				break;
			case "Status":
				note_str = "Template  - Status Type Dropdown.";
				
				SelectDropdownItem(Search_Status_ddl(), template.IsActive.toString() ,note_str);
				break;
		}
		
		
		ClearThenEnterValueToField(Search_TemplName_txt(), template.Name);
		
		Search_btn().click();

		Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}







	public  void VeifyTemplateNotExistTable_func(Template_Info tpl)
	{
		Boolean exist = IsItemExistTable(Templ_List_tbl(), tpl.Name);

		if(exist==true)
		{
			TestConfigs.glb_TCStatus=false;
			TestConfigs.glb_TCFailedMessage += "Template["+tpl.Name+"] SHOULD NOT Show after search.\n";
		}//end if
	}//end void



	public  void VerifyTemplateExistTable_func(Template_Info tpl,String note)
	{
		Boolean exist = false;
		//Search template
		//SearchTemplate_func(tpl)

		List<String> list_tmp = GetTableRecordsPerPaging_func(Templ_List_tbl());

		int count = list_tmp.size();

		if(count==0)
		{
			TestConfigs.glb_TCStatus=false;
			TestConfigs.glb_TCFailedMessage += "No record shown after search "+note+".\n";
		}
		else
		{
			for(int i=0;i<count;i++)
			{
				if(list_tmp.get(i).contains(tpl.Name))
				{
					exist = true;
				}
			}//end for
		}//end else

		if(exist==false)
		{
			TestConfigs.glb_TCStatus=false;
			TestConfigs.glb_TCFailedMessage += "Template["+tpl.Name+"] NOT found after search "+note+".\n";
		}
	}//end void
	

	public  void GotoTemplateDetailsPage_func(String tpl_name)
	{
		
		try {
			TemplName_link(tpl_name).click();

			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

	//		Wait_For_ElementDisplay(TemplateDetails_page.Edit_ServiceType_ddl());
	
		}catch(NullPointerException e)
		{
			//TestConfigs.glb_TCStatus = false
			TestConfigs.glb_TCFailedMessage += "Not Found template["+tpl_name+"].\n";

			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
			}	
	
	
	//METHOD

			public  void RemoveTemplate_func(Template_Info template, String button)
			{
				
				Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
				
				SearchTemplate_func("Type",template);

				Boolean exist  = IsItemExistTable(Templ_List_tbl(), template.Name);

				if(exist ==true)
				{
					Remove_icon(template.Name).click();
					if(Messages_Notification.ConfirmYes_btn()!=null&&button =="Submit")
					{
						Messages_Notification.ConfirmYes_btn().click();

						Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"",false);
					}//end if
					else
						Messages_Notification.ConfirmNo_btn().click();
				}//end if exist
				else
				{
					//TestConfigs.glb_TCStatus = false
					TestConfigs.glb_TCFailedMessage += "Not Found template to remove.\n";

					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}//end else exist
			}//end void



			public  void CreateTemplate_func(Template_Info newtemplate, String button)
			{
				
				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
					
	//			Wait_For_ElementDisplay(New_btn());

				Optimize_ElementClick(New_btn());
				
				new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

	//			Wait_For_ElementDisplay(TemplateDetails_page.Edit_ServiceType_ddl());

				new TemplateDetails_page(driverbase).FillInfo_func("Create",newtemplate,button);
				
				new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();


			}//end void


			public  void UpdateTemplate_func(String oldtemplname, Template_Info newTemplate,String button)
			{
				
				GotoTemplateDetailsPage_func(oldtemplname);

				new TemplateDetails_page(driverbase).FillInfo_func("Update",newTemplate,button);

			
			}//end void
	
}
