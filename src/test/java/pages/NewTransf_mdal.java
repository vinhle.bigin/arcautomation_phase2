package pages;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import baseClasses.PageObjects;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class NewTransf_mdal extends PageObjects{
	
	public NewTransf_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Fname_txt_xpath = "//input[contains(@placeholder,'First Name')]";
	private  String Lname_txt_xpath = "//input[contains(@placeholder,'Last Name')]";
	private  String Client_slect_xpath = "//div[@class='form-group col-md-4 multi-client']//div[contains(@class,'dropdown-main')]/ul";
	private  String Coordinate_slect_xpath = "//div[@id='list-coordinators']//ul";
	private  String InitDate_txt_xpath = "//input[@id = 'create-transferee-other-info-initiation-date']";
	private  String OrigState_slect_xpath = "//h4[contains(text(),'Home Address')]/..//select[@placeholder = 'State *']";
	private  String DestState_slect_xpath = "//h4[contains(text(),'Destination Office Address')]/..//select[@placeholder = 'State *']";
	private  String Create_btn_xpath = "//button[@data-control = 'create']";

	private  String Coord_txt_xpath = "//input[@id = 'input-list-coordinators']";

	private  String Client_txt_xpath = "//input[@placeholder = 'Client *']";

	private  String CoordExpand_icon_xpath = "//div[@id='list-coordinators']//div[@class = 'control']";

	private  String PhoneType_ddl_xpath = "//div[@class ='mobile-wrap']//select[@placeholder = 'Type']";
	private  String Phone_txt_xpath = "//div[@class ='mobile-wrap']//input[@placeholder = 'Number']";

	private  String EmailType_ddl_xpath = "//div[@class ='email-wrap extension-fields']//select[@placeholder = 'Type']";
	private  String Email_txt_xpath = "//div[@class ='email-wrap extension-fields']//input[@placeholder = 'E-mail']";

	private  String ClientContact_ddl_xpath = "//div[@org-placeholder ='Client Contact']//select";
	
	private  String Role_ddl_xpath = "//select[@placeholder ='Role']";

	//This element to display the selected value on Coordinator Dropdown
	private  String Coord_vlue_xpath = "//div[@id='list-coordinators']//div[@class='item-result-combobox']/div[@class='content']";
	
	private  String File_Status_ddl_xpath = "//select[@placeholder='File Status']";
	
	
	
	
	//DEFINE ELEMENT:
	
	public  WebElement File_Status_ddl() {
		  return GetElement(File_Status_ddl_xpath);
		 }
	
	public  WebElement Role_ddl() {
		  return GetElement(Role_ddl_xpath);
		 }
	
	public  WebElement Fname_txt() {
	  return GetElement(Fname_txt_xpath);
	 }
public  WebElement Lname_txt() {
	  return GetElement(Lname_txt_xpath);
	 }
public  WebElement Client_slect() {
	  return GetElement(Client_slect_xpath);
	 }
public  WebElement Coordinate_slect() {
	  return GetElement(Coordinate_slect_xpath);
	 }
public  WebElement InitDate_txt() {
	  return GetElement(InitDate_txt_xpath);
	 }
public  Select OrigState_slect() {
	  return new Select(GetElement(OrigState_slect_xpath));
	 }
public  Select DestState_slect() {
	  return new Select(GetElement(DestState_slect_xpath));
	 }
public  WebElement Create_btn() {
	  return GetElement(Create_btn_xpath);
	 }
public  WebElement Coord_txt() {
	  return GetElement(Coord_txt_xpath);
	 }
public  WebElement Client_txt() {
	  return GetElement(Client_txt_xpath);
	 }
public  WebElement CoordExpand_icon() {
	  return GetElement(CoordExpand_icon_xpath);
	 }
public  WebElement PhoneType_ddl() {
	  return GetElement(PhoneType_ddl_xpath);
	 }
public  WebElement Phone_txt() {
	  return GetElement(Phone_txt_xpath);
	 }
public  WebElement EmailType_ddl() {
	  return GetElement(EmailType_ddl_xpath);
	 }
public  WebElement Email_txt() {
	  return GetElement(Email_txt_xpath);
	 }
public  WebElement ClientContact_ddl() {
	  return GetElement(ClientContact_ddl_xpath);
	 }


public  WebElement Coord_vlue() {
	  return GetElement(Coord_vlue_xpath);
	 }
	
	//=============================METHOD

		public  void FillInfoModal_func(Transferee_Info trf,Boolean submit)
		{
			try {
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

			ARCLoading ARCLoading = new ARCLoading(driverbase);
				
			ARCLoading.Wait_Util_FinishedPageLoading();

		//	Wait_For_ElementDisplay(Fname_txt());

			Fname_txt().sendKeys(trf.firstname);
			//Fname_txt().sendKeys(keysToSend);
			

			Thread.sleep(1000);

			Lname_txt().sendKeys(trf.lastname);

			Thread.sleep(1000);

			Client_txt().sendKeys(trf.Client.CompanyName);

			String note_str = "Transferee Details - Client Dropdown.";
			
			SelectDropdownItem(Client_slect(), trf.Client.CompanyName, note_str);
			
			ARCLoading.Wait_Util_FinishedPageLoading();

			
			if(trf.Client.Contacts.size()!=0)
			{
				//Add the client contact
				note_str = "Transferee Details - Client Contact Dropdown.";
				
				SelectDropdownItem(ClientContact_ddl(), trf.Client.Contacts.get(0).FullName,note_str);
			}
			
			
			Optimize_ElementClick(Coord_vlue());
			
		//	Wait_For_ElementDisplay(Coord_txt());
			//Thread.sleep(2000);
			
			//ClearThenEnterValueToField(Coord_txt(), trf.Coordinator);
			
			Optimize_ElementSendkey(Coord_txt(), trf.Coordinator);
			
			Thread.sleep(1000);
			
			note_str = "Transferee Details - Coordinator Dropdown.";
			
			SelectDropdownItem(Coordinate_slect(), trf.Coordinator,note_str);
			
			Thread.sleep(1000);
			
			//===========Select File Status
			note_str = "Transferee Details -File_Status_ddl.";
			SelectDropdownItem(File_Status_ddl(), trf.Status,note_str);
						
			//InitDate info
			//ClearThenEnterValueToField(InitDate_txt(), trf.FileEnterDate);

			Optimize_ElementSendkey(InitDate_txt(), trf.Other_info.InitDate);
			
			Thread.sleep(1000);

			InitDate_txt().sendKeys(Keys.TAB);

			OrigState_slect().selectByVisibleText(trf.OriResAddr_info.State);

			
			Thread.sleep(1000);

			DestState_slect().selectByVisibleText(trf.DesOffAddr_info.State);

			Thread.sleep(1000);

			//role
			if(trf.Status.equals("Active"))
			{
				note_str = "Transferee Details - Role Dropdown.";
				
				SelectDropdownItem(Role_ddl(), trf.role,note_str);

			}//end if
			
			
			//Phone info
			note_str = "Transferee Details - Phone Type Dropdown.";
			
			SelectDropdownItem(PhoneType_ddl(), trf.PhoneList.get(0).Type,note_str);

			ClearThenEnterValueToField(Phone_txt(), trf.PhoneList.get(0).Numb);

			//Email info
			note_str = "Transferee Details - Mail Type Dropdown.";
			
			ScrollElementtoViewPort_func(EmailType_ddl());
			
			SelectDropdownItem(EmailType_ddl(), trf.MailList.get(0).Type,note_str);

			ClearThenEnterValueToField(Email_txt(), trf.MailList.get(0).EmailAddr);
			

			if(submit)
			{
				Create_btn().click();
				
				if(Messages_Notification.notification_msg()!=null)
				{
					String msg = Messages_Notification.notification_msg().getText();
					
					String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
					//println(msg);
					if(msg.equals(expected_msg))
					{
						TestConfigs.glb_TCFailedMessage+= "Unsuccessfull TRANSF Creation.Msg: '"+msg+"'.\n";
						
						Assert.fail(TestConfigs.glb_TCFailedMessage);
					}
				}//end if of message
				
				ARCLoading.Wait_Util_FinishedARClogoLoading();

			}//end if of submit
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}
		}

}
