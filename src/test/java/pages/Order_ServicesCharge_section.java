package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;

public class Order_ServicesCharge_section extends PageObjects{
	
	

	public Order_ServicesCharge_section(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String ServiceCharge_Edit_btn_xpath = "//a[@id='icon-hhg-order-servicescharge-panel']";
	private  String ServiceCharge_Update_btn_xpath = "//div[@id='hhg-order-servicescharge-panel']//button[@data-control='update']";
	private  String ServiceCharge_Cancel_btn_xpath = "//div[@id='hhg-order-servicescharge-panel']//button[@data-control='cancel-update']";
	private  String ClientType_txt_xpath = "//div[@org-placeholder='Client Type']//span[@class='placeholder show']";
	private  String ClientType_ddl_xpath = "//div[@org-placeholder='Client Type']//select";
	private  String Champ_Commer_ddl_xpath = "//select[@placeholder='Champ or Commercial']";
	private  String SCAC_txt_xpath = "//div[@org-placeholder='SCAC']//input";
	private  String TariffCode_ddl_xpath = "//select[@placeholder='Tariff Code']";
	private  String BillType_ddl_xpath = "//select[@placeholder='BILL TYPE']";
	
	

	
	public  WebElement ServiceCharge_Edit_btn() {
	  	return GetElement(ServiceCharge_Edit_btn_xpath);
	 }
	
	public  WebElement ServiceCharge_Update_btn() {
	  	return GetElement(ServiceCharge_Update_btn_xpath);
	 }
	
	public  WebElement ServiceCharge_Cancel_btn() {
	  	return GetElement(ServiceCharge_Cancel_btn_xpath);
	 }
	
	public  WebElement ClientType_txt() {
	  	return GetElement(ClientType_txt_xpath);
	 }
	
	public  WebElement ClientType_ddl() {
	  	return GetElement(ClientType_ddl_xpath);
	 }
	
	public  WebElement Champ_Commer_ddl() {
	  	return GetElement(Champ_Commer_ddl_xpath);
	 }
	
	public  WebElement TariffCode_ddl() {
	  	return GetElement(TariffCode_ddl_xpath);
	 }	
	
	public  WebElement BillType_ddl() {
	  	return GetElement(BillType_ddl_xpath);
	 }
	
	public  WebElement SCAC_txt() {
	  	return GetElement(SCAC_txt_xpath);
	 }
	
	//METHODS
	
	public  void Update_ServiceCharge_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(ServiceCharge_Edit_btn());
		
		SelectDropdownItem(ClientType_ddl(), order.ClientType, "");
		
		SelectDropdownItem(Champ_Commer_ddl(), order.ChampCommercial, "");
		
		ClearThenEnterValueToField(SCAC_txt(), order.SCAC);
		
		SelectDropdownItem(BillType_ddl(), order.BillType,"");
		
		if(order.Is_Domestic == true)
		{
			SelectDropdownItem(TariffCode_ddl(), order.TariffCode, "");
		}
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(ServiceCharge_Update_btn() );
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Service Dates",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
	}
	
	//=======================================METHODS VERIFY
	
	public  void Verify_OrderDetail_ServiceCharge_func(Service_Order_Info order)
	{
		String field = "";
		
		field = "Client Type";
		
		VerifyFieldValueEqual_func(field,ClientType_ddl(), order.ClientType,"");
		
		field = "Champ or Commercial";
		
		VerifyFieldValueEqual_func(field,Champ_Commer_ddl(), order.ChampCommercial,"");
		
		field = "SCAC";
		
		VerifyFieldValueEqual_func(field,SCAC_txt(), order.SCAC,"");
		
		if (order.Is_Domestic == true)
		{
			field = "Tariff Code";
		
			VerifyFieldValueEqual_func(field,TariffCode_ddl(), order.TariffCode,"");
		}
		
		field = "BILL TYPE";
		
		VerifyFieldValueEqual_func(field,BillType_ddl(), order.BillType,"");
		
	}//end void
	
	
}
