package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Print_Info;
import businessObjects.Transferee_Info;
import configuration.DriverConfig.DriverBase;

public class Print_tab extends PageObjects{
	private Messages_Notification Messages_Notification;
	private Shortcuts_menu Shortcuts_menu;
	
	private  String actived_detail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";
	private String PrintOutType_ddl_xpath = actived_detail_mdal_xpath+"//select[@placeholder='Printout Type']";
	private String Template_ddl_xpath = actived_detail_mdal_xpath+"//select[@placeholder='Template']";
	private String FileType_PDF_radio_xpath = actived_detail_mdal_xpath+"//div[@value = 'pdf']";
	private String FileType_Word_radio_xpath = actived_detail_mdal_xpath+"//div[@value = 'word']";
	private String ContactList_PrintCurrentSearch_radio_xpath = actived_detail_mdal_xpath+"//label[contains(text(), 'Print Current Search')]/..//input";
	private String ContactList_AllTransfContact_xpath = actived_detail_mdal_xpath+"//label[contains(text(), 'All Transferee Contact')]/..//input";
	private String SelectContact_radio_xpath = actived_detail_mdal_xpath+"//div[@id='table-list-contact-print']//input[@type='checkbox']";
	private String Print_btn_xpath = actived_detail_mdal_xpath+"//button[contains(text(),'Print')]";
	private String Cancel_btn_xpath = actived_detail_mdal_xpath+"//button[@data-control='cancel-create']";
	
	
	//Search Transferee
	private String Search_Transf_txt_xpath = actived_detail_mdal_xpath+"//div[@class='VueTables__search-field']//input";
	
	public Print_tab(DriverBase driver) {
		// TODO Auto-generated constructor stub
		super(driver);
		
		Shortcuts_menu = new Shortcuts_menu(driver);
		new ARCLoading(driver);
		Messages_Notification = new Messages_Notification(driver);
	}
	
	public  WebElement Search_Transf_txt(){
		return GetElement(Search_Transf_txt_xpath);
	}
	
	public  WebElement PrintOutType_ddl(){
		return GetElement(PrintOutType_ddl_xpath);
	}
	
	public  WebElement Template_ddl(){
		return GetElement(Template_ddl_xpath);
	}
	
	public  WebElement FileType_PDF_radio(){
		return GetElement(FileType_PDF_radio_xpath);
	}
	
	public  WebElement FileType_Word_radio(){
		return GetElement(FileType_Word_radio_xpath);
	}
	
	public  WebElement ContactList_PrintCurrentSearch_radio(){
		return GetElement(ContactList_PrintCurrentSearch_radio_xpath);
	}
	
	public  WebElement ContactList_AllTransfContact(){
		return GetElement(ContactList_AllTransfContact_xpath);
	}
	
	public  WebElement SelectContact_radio(){
		return GetElement(SelectContact_radio_xpath);
	}
	
	public  WebElement Print_btn(){
		return GetElement(Print_btn_xpath);
	}
	
	public  WebElement Cancel_btn(){
		return GetElement(Cancel_btn_xpath);
	}
	
		
	public void Print_Via_ShortCut_with_Current_Search(Print_Info print_info, String button)
	{
		Optimize_ElementClick(Shortcuts_menu.Print_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
		SelectDropdownItem(PrintOutType_ddl(), print_info.PrintOutType, "");
		
		SelectDropdownItem(Template_ddl(), print_info.Template, "");
		
		if(print_info.PDF == true)
		{
			Optimize_ElementClick(FileType_PDF_radio());
		}//end if
		
		if(print_info.Word == true)
		{
			Optimize_ElementClick(FileType_Word_radio());
		}//end if
		
		Optimize_ElementClick(ContactList_PrintCurrentSearch_radio());
			
		Optimize_ElementClick(SelectContact_radio());

		
		if(button.equals("Submit"))
		{
			Optimize_ElementClick(Print_btn());
		}

		
		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "Print Transferee Address Label Template", true);
	
	}
	
	public void Print_Via_ShortCut_with_Some_Transfs(Print_Info print_info, List<Transferee_Info> transf_list, String button)
	{
		//Just for the Address Label template type
		Optimize_ElementClick(Shortcuts_menu.Print_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
		SelectDropdownItem(PrintOutType_ddl(), print_info.PrintOutType, "");
		
		SelectDropdownItem(Template_ddl(), print_info.Template, "");
		
		if(print_info.PDF == true)
		{
			Optimize_ElementClick(FileType_PDF_radio());
		}//end if
		
		if(print_info.Word == true)
		{
			Optimize_ElementClick(FileType_Word_radio());
		}//end if
		
		Optimize_ElementClick(ContactList_AllTransfContact());
			
		Optimize_ElementClick(Messages_Notification.OppsOk_btn());
		
		
		for (int i = 0; i < 2; i++)
		{
			ClearThenEnterValueToField(Search_Transf_txt(), transf_list.get(i).FullName);
			
			Optimize_ElementClick(SelectContact_radio());
		}
		
		if(button.equals("Submit"))
		{
			Optimize_ElementClick(Print_btn());
			
		}
			
		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "Print Transferee Address Label Template", true);
		
	}
}
