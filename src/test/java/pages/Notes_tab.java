package pages;

import java.util.List;


import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Mail_Info;
import businessObjects.Note_Info;

import org.junit.Assert;


import configuration.DriverConfig.DriverBase;
import configuration.TestConfigs;
import custom_Func.DateTime_Manage;

public class Notes_tab extends PageObjects{
	

	
	public Notes_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private  String Note_tab_xpath = "//ul[@role='tablist']//a[contains(@href, '-notes')]";
	private  String CreateNew_btn_xpath= "//button[contains(text(),'New Note')]";
	private  String CreateNew_ic_xpath= "//span[@class='menu-icon fas fa-sticky-note']";
	private  String Search_btn_xpath= "//button[contains(@id,'btn-search')]";
	private  String Clear_btn_xpath= "//button[contains(text(),'Clear')]";
	private  String SearchFromDate_txt_xpath= "//input[contains(@id,'from_date')]";
	private  String SearchToDate_txt_xpath= "//input[contains(@id,'to_date')]";
	private  String SearchSubject_txt_xpath = "//input[@name = 'subject']";
	private  String SearchTinyContentBox_xpath = "//input[@placeholder='Content']";
	private String Search_ServiceType_ddl_xpath = "//select[@placeholder ='Service type']";
	private  String NoteList_tbl_xpath= "//table[@id = 'table-list-notes-tab']/tbody | //div[contains(@id,'list-note-tab') or contains(@id,'table-list-note')]//table//tbody";
	private  String Remove_icon_xpath = "//a[@class='none-underline']";
	private  String CreateNewTranfInfoTop_btn_xpath = "//p[contains(text(),'Note')]";
	private  String QuickSearch_txt_xpath= "//input[@id='VueTables__search_GaRAd']";
	private  String TranfPort_MyInfo_Tab_xpath = "//ul[@class = 'menu accordion-menu']//li[contains(@class, '   clearfix')]//a[contains(@href, 'admin/transfere')]";
	private  String ContactClientNote_ddl_xpath = "//select[@placeholder = 'Contact']";
	
	
	
	
	
	public  WebElement Remove_icon(String subject) {
		//WebElement temp_element  = GetElementInTable(subject,Transf_NoteList_tbl(), Remove_icon_xpath)
		WebElement temp_element  = GetElementInTable(subject,NoteList_tbl(), Remove_icon_xpath);
		return temp_element;
	}
	
	public  WebElement Search_ServiceType_ddl() {
		  return GetElement(Search_ServiceType_ddl_xpath);
		 }
	
		
	public  WebElement NoteList_tbl() {
		  return GetElement(NoteList_tbl_xpath);
		 }
	
	public  WebElement Note_tab() {
		  return GetElement(Note_tab_xpath);
		 }
		
	public  WebElement CreateNew_btn() {
		  return GetElement(CreateNew_btn_xpath);
		 }
	
	public  WebElement CreateNew_ic() {
		  return GetElement(CreateNew_ic_xpath);
		 }
	public  WebElement Search_btn() {
		  return GetElement(Search_btn_xpath);
		 }
	
	public  WebElement Clear_btn() {
		  return GetElement(Clear_btn_xpath);
		 }
	
	public  WebElement SearchFromDate_txt() {
		  return GetElement(SearchFromDate_txt_xpath);
		 }
	
	public  WebElement SearchToDate_txt() {
		  return GetElement(SearchToDate_txt_xpath);
		 }
	public  WebElement SearchSubject_txt() {
		  return GetElement(SearchSubject_txt_xpath);
		 }
	
	public  WebElement SearchTinyContentBox() {
		  return GetElement(SearchTinyContentBox_xpath);
		 }
	
	public  WebElement CreateNewTranfInfoTop_btn() {
		  return GetElement (CreateNewTranfInfoTop_btn_xpath);
	}
	
	public  WebElement CreateNewTranList_btn() {
		  return GetElement (CreateNew_ic_xpath);
	}
	
	public  WebElement QuickSearch_txt() {
		  return GetElement(QuickSearch_txt_xpath);
		 }
	
	public  WebElement TranfPort_MyInfo_tab() {
		  return GetElement(TranfPort_MyInfo_Tab_xpath);
		 }
	
	public  WebElement noteTitle_lnk(String titleNote) {
		try{
			
			String xpath_str =  "//a//span[contains(text(),'"+titleNote+"')] | //a[contains(text(),'" + titleNote + "')]";
			WebElement temp_element = GetElement(xpath_str);
					
			/*
			 WebDriver webDriver = DriverFactory.getWebDriver()
			 String xpath_str =  ".//a//span[text()='"+titleNote+"']"
			 WebElement temp_element
			 if(Transf_NoteList_tbl()!=null)
			 {
			 temp_element = GetElementInTable(titleNote, Transf_NoteList_tbl(), xpath_str)
			 }
			 else if(Client_NoteList_tbl()!=null)
			 {
			 temp_element = GetElementInTable(titleNote, Client_NoteList_tbl(), xpath_str)
			 }
			 else
			 {
			 temp_element = webDriver.findElement(By.xpath("//a[contains(text(),'" + titleNote + "')]"));
			 }
			 */
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}
	
	public  WebElement ClientContactNote_ddl () {
		return GetElement(ContactClientNote_ddl_xpath);
	}
	
	//=================================METHOD

		public  void SearchNote_func(String subject_str)
		{
			try {
		//	Wait_For_ElementDisplay(SearchSubject_txt());

			ClearThenEnterValueToField(SearchSubject_txt(), subject_str);

			Optimize_ElementClick(Search_btn());

			//Search_btn().click()

			
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		public  void Quick_Search_Note_func(String subject_str)
		{
			try {
		//	Wait_For_ElementDisplay(QuickSearch_txt());
			
			ClearThenEnterValueToField(QuickSearch_txt(), subject_str);
			
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		public  void Search_Note_func(Note_Info note)
		{
		
			try {
				
			SelectDropdownItem(Search_ServiceType_ddl(), note.Service, "Search Service Type dropdown.");
				
			ClearThenEnterValueToField(SearchSubject_txt(), note.Title);

			Optimize_ElementClick(Search_btn());
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public  void searchNoteDate()
		{
			try {
			Thread.sleep(3000);
			String FileEnterDate = DateTime_Manage.GetCurrentLocalDate_Str();

			ClearThenEnterValueToField(SearchFromDate_txt(), FileEnterDate);
			Thread.sleep(1000);

			SearchFromDate_txt().sendKeys(Keys.TAB);

			ClearThenEnterValueToField(SearchToDate_txt(), FileEnterDate);
			Thread.sleep(1000);

			Optimize_ElementClick(Search_btn());

			Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public  void VerifyNoteInfoCorrectExistTable_func(Note_Info note,boolean StopRun)
		{
		
			Wait_For_ElementDisplay(NoteList_tbl_xpath);
			//	Optimize_ElementClick(Note_tab());
			
			Boolean exist = false;
			
			List<String> records;
			records = GetTableRecordsPerPaging_func(NoteList_tbl());

			for(String index_str: records)
			{
				if(index_str.contains(note.Title) && index_str.contains(note.Content))
				{
					exist = true;
					break;
					
				}//end if
				
				
			}//end foreach
	
			boolean isFalse=true;
			if(!exist)
			{
				isFalse = false;
				
				TestConfigs.glb_TCStatus = isFalse;
				
				TestConfigs.glb_TCFailedMessage += "Note["+note.Title+"-"+note.Content+"] NOT exist in table.\n";
				
				
			}//end if
			
			if(isFalse==false && StopRun==true)
			{
				Assert.fail(TestConfigs.glb_TCFailedMessage);
			}
			
		}
		
		public  void VerifyNoteNotExistTable_func(Note_Info note)
		{
			//SearchNote_func(note.Title)

			Boolean exist = false;
			//SearchNote_func(note.Title)
			List<String> records;

			records = GetTableRecordsPerPaging_func(NoteList_tbl());
			/*
			 if(note.TransfFullName!="")
			 records = GetTableRecordsPerPaging_func(Transf_NoteList_tbl())
			 else if(note.Contact!="")
			 records = GetTableRecordsPerPaging_func(Client_NoteList_tbl())
			 */

			if(records.size()>0)
			{
				for(String index_str: records)
				{
					if(index_str.contains(note.Title))
					{
						exist = true;
						break;
					}//end if
				}//end foreach
			}


			if(exist==true)
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Note["+note.Title+"] SHOULD NOT exist in table.\n";
			}//end if
		}

		
		public  void Create_New_Note_func(Note_Info newnote) {
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			//Go to Note Tab

			GotoNoteTab_func();

	//		Wait_For_ElementDisplay(CreateNew_btn());

			//Click on New button
			Optimize_ElementClick(CreateNew_btn());

			ARCLoading.Wait_Util_FinishedPageLoading();

			//Fill info into modal
			new NoteDetails_mdal(driverbase).Fill_Info_To_Modal_func(newnote,"Submit");

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"when Create Note.",false);

		}//end void
		
		
		public  void Create_New_Note_Transf_Info_Top_Menu (Note_Info newnote) {
			
			Messages_Notification Messages_Notification = new  Messages_Notification(driverbase);

			//Click on New button
			Optimize_ElementClick(CreateNewTranfInfoTop_btn());

			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
			
			//Fill info into modal
			new NoteDetails_mdal(driverbase).Fill_Info_To_Modal_func(newnote,"Submit");

			new Messages_Notification(driverbase).VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"when Create Note.",false);

		}
		
		public  void Create_New_Note_Transf_List (Note_Info newnote) {
			
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

			//Click on New button
			Optimize_ElementClick(CreateNewTranList_btn());

			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

			//Fill info into modal
			new NoteDetails_mdal(driverbase).Fill_Info_To_Modal_func(newnote,"Submit");

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"when Create Note.",false);

		}

		public  void Update_Note_Info_func(String oldtitle,Note_Info newnote,String button)
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			//Click on hyperlink
			Optimize_ElementClick(noteTitle_lnk(oldtitle));

			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

			//Click on Edit on modal
			//Wait_For_ElementDisplay(NoteDetails_mdal.Edit_btn());

			Optimize_ElementClick(new NoteDetails_mdal(driverbase).Edit_btn());

			new NoteDetails_mdal(driverbase).Fill_Info_To_Modal_func(newnote, button);
			//FillInfoModalClientfunc(this)

			new Messages_Notification(driverbase).VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"when Edit Client ote.",false);
		}

		
		public  void Delete_Note_func(Note_Info note)
		{			
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			try {
				//GotoNoteTab_func();
	
				//SearchNote_func(note.Title);
	
				Thread.sleep(2000);
				
				Optimize_ElementClick(Remove_icon(note.Title));
	
				//Remove_icon(note.Title).click();
	
				Thread.sleep(5000);
	
				Messages_Notification.Delete_btn().click();
	
				Thread.sleep(3000);
	
				Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"when Delete Note.",false);
				
				} catch (InterruptedException e) {
			
					e.printStackTrace();
				}
		}

			
		//METHOD: VERIFY INFO ON DETAILS MODAL
		public  void VerifyModalDetailsInfo_func(Note_Info note)
		{
			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			Optimize_ElementClick(noteTitle_lnk(note.Title));

			ARCLoading.Wait_Util_FinishedPageLoading();
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();

		//	noteTitle_lnk(note.Title).click();

			new NoteDetails_mdal(driverbase).VerifyModalDetailsInfo_func(note);
		

		}//end void
		
		
		//Only for SEnding Email flow
		public  void VerifyModalDetailsInfo_func(Mail_Info mail_info)
		{
			
			
			Note_tab().click();
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

			SearchNote_func(mail_info.Subject);


			noteTitle_lnk(mail_info.Subject).click();

			
			new NoteDetails_mdal(driverbase).VerifyModalDetailsInfo_func(mail_info);
			
		}//end void
		
		
		public  void GotoNoteTab_func()
		{
			try {
		//	Wait_For_ElementDisplay(Note_tab());

			if(CreateNew_btn()==null)
				Note_tab().click();

			Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}



}
