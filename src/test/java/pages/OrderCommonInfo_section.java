package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;
import custom_Func.Data_Optimize;

public class OrderCommonInfo_section extends PageObjects{
	
	
	
	public OrderCommonInfo_section(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String actived_detail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";
	
	private  String NewOrder_btn_xpath = "//button[contains(text(),'New Order')]";
	
	
	private  String OrderDesc_link_xpath = "//div[@class='dropdown dropdown-content']//span[contains(text(),'<order_name>')]";
	
	private  String OrderList_tbl_xpath = "//div[@id='table_hhg_order']//table//tbody";
	
	private  String mdal_Descr_txt_xpath = actived_detail_mdal_xpath+ "//input[@placeholder = 'Description']";
	
	private  String mdal_Domestic_opt_xpath = actived_detail_mdal_xpath+ "//div[contains(text(),'Domestic')]/..//input";
	
	private  String mdal_International_opt_xpath = actived_detail_mdal_xpath+ "//div[contains(text(),'International')]/..//input";
	
	private  String mdal_HHG_ServiceType_ddl_xpath = actived_detail_mdal_xpath+ "//div[@org-placeholder = 'HHG Service Type']//select";
	
	private  String mdal_DosMoveType_ddl_xpath = actived_detail_mdal_xpath+ "//div[@org-placeholder = 'Domestic Moving Type']//select";
	
	private  String mdal_OrderType_ddl_xpath = actived_detail_mdal_xpath+ "//select[@placeholder='Order Type']";
	
	private  String mdal_OtherServiceType_txt_xpath = actived_detail_mdal_xpath+ "//input[@placeholder = 'Other HHG Service Type']";
	
	private  String mdal_Submit_btn_xpath = actived_detail_mdal_xpath+ "//button[@data-control='create']";

	
	
	private  String Order_VendorSelect_txt_xpath = "//div[@id='list-vendor-selected']//input[@id ='input-list-vendor-selected']";
	
	private  String Order_SelectedContact_vlue_xpath = "//div[@id = 'list-vendor-selected']//div[@class='content']";
	
	private  String Order_VendorSelect_ddl_xpath = "//div[@id='list-vendor-selected']//ul";
	
	private  String Order_VendorSelect_Submit_btn_xpath = "//div[@id='loading-hhg-order-vendor-selected-panel']//button[@data-control='update']";
	
	private  String Order_VendorSelect_Edit_btn_xpath = "//div[@id='loading-hhg-order-vendor-selected-panel']//a[@id='icon-hhg-order-vendor-selected-panel']";

	private  String OrderName_Header_xpath = "//div[contains(text(),'Order Detail -')]/span";

	private  String Order_Copy_btn_xpath = "//i[@class='far fa-copy']";
	
	private  String Order_ConfirmCopy_btn_xpath = "//div[@id='confirm-modal-component']//button[contains(text(),'Copy')]";
	
	//Order Information section
	private  String OrderInfo_Edit_btn_xpath = "//a[@id='icon-hhg-order-information-panel']";
	private  String OrderDesc_txt_xpath = "//div[@org-placeholder='Order Description']//input";
	private  String HHGService_Type_ddl_xpath = "//select[@placeholder='HHG Service Type']";
	private  String Domestic_MoveType_ddl_xpath = "//select[@placeholder='Domestic Moving Type']";
	private  String ShipMode_Auth_ddl_xpath = "//select[@placeholder='Shipment Mode Authorized']";
	private  String OrderDte_txt_xpath = "//div[@org-placeholder='Order Date']//input";
	private  String ContactDte_txt_xpath = "//div[@org-placeholder='1ST Contact Date']//input";
	private  String AssignDte_txt_xpath = "//div[@org-placeholder='Assign Date']//input";
	private  String SurveyDte_txt_xpath = "//div[@org-placeholder='Survey Date']//input";
	private  String Other_txt_xpath = "//div[@org-placeholder='Special Note/Instructions']//textarea";
	private  String UpdateOther_txt_xpath = "//pre[@placeholder='Special Note/Instructions']";
	private  String Status_ddl_xpath = "//div[@org-placeholder='Delivery Status']//select";
	private  String DateIn_txt_xpath = "//div[@org-placeholder='Date in Storage']//input";
	private  String DateOut_txt_xpath = "//div[@org-placeholder='Date out of Storage']//input";
	private  String OrderInfo_Update_btn_xpath = "//div[@id='loading-hhg-order-information-panel']//button[@data-control='update']";
	private  String OrderInfo_Cancel_btn_xpath = "//div[@id='loading-hhg-order-information-panel']//button[@data-control='cancel-update']";

	//Billing and Invoice
	private  String Bill_Invoice_Edit_btn_xpath = "//a[@id='icon-hhg-order-billing-invoice-panel']";
	private  String Bill_Invoice_Update_btn_xpath = "//div[@id='hhg-order-billing-invoice-panel']//button[@data-control='update']";
	private  String Bill_Invoice_Cancel_btn_xpath = "//div[@id='hhg-order-billing-invoice-panel']//button[@data-control='cancel-update']";
			//Total Services Charge
	private  String Total_ServiceCharge_txt_xpath = "//div[@org-placeholder='Total Services Charge']//input";
	private  String Override_chbox_xpath = "//h4[contains(text(),'Total Services Charge')]/..//span[@class='checkbox__checkmark']";
			//Escrow Fee
	private  String EscrowPer_txt_xpath = "//div[@org-placeholder='Escrow Percentage']//input";
	private  String Escrow_Amount_txt_xpath = "//div[@org-placeholder='Escrow Amount']//input";
	private  String Fee_Received_chbox_xpath = "//h4[contains(text(),'Escrow Fee')]/..//span[@class='checkbox__checkmark']";
			//Billing
	private  String Audit_Fee1_txt_xpath = "//div[@org-placeholder='Audit Fee 1']//input";
	private  String Audit_Fee2_txt_xpath = "//div[@org-placeholder='Audit Fee 2']//input";
	private  String Audit_Fee3_txt_xpath = "//div[@org-placeholder='Audit Fee 3']//input";
	private  String TotalInvoice_txt_xpath = "//div[@org-placeholder='Total Invoice to Client']//input";
	private  String TotalBill_Mover_txt_xpath = "//div[@org-placeholder='Total Bill to Mover']//input";
	private  String TotalARC_Profit_txt_xpath = "//div[@org-placeholder='Total ARC Profit']//input";
	private  String DteBill_txt_xpath = "//div[@org-placeholder='Date Billed Client']//input";
	private  String DteReceivePayment_txt_xpath = "//div[@org-placeholder='Date Received Payment from Client']//input";
	private  String DteInvoice_Submit_txt_xpath = "//div[@org-placeholder='Date Invoice Submitted']//input";
	
	//Services Rating
	private  String Rating_Edit_btn_xpath = "//a[@id='icon-hhg-order-servicesrating-panel']";
	private  String Rating_Update_btn_xpath = "//div[@id='hhg-order-servicesrating-panel']//button[@data-control='update']";
	private  String Rating_Cancel_btn_xpath = "//div[@id='hhg-order-servicesrating-panel']//button[@data-control='cancel-update']";
	private  String Rating_Note_txt_xpath = "//div[@org-placeholder='Rating Note']//textarea";
	
	
	public  WebElement OrderDesc_link(String order_name) {
		
		WebElement e_temp = GetElement(OrderDesc_link_xpath);
		
		//Catch xpath for order name
		if(e_temp==null)
		{
			String a = OrderDesc_link_xpath.replace("<order_name>", order_name);
			
			e_temp = GetElement(a);
		}
		
		return e_temp;
	}
	
	public  WebElement Order_VendorSelect_txt() {
		 return GetElement(Order_VendorSelect_txt_xpath);
	}
		
	public  WebElement Order_VendorSelect_ddl() {
		return GetElement(Order_VendorSelect_ddl_xpath);
	}
		
	public  WebElement Order_VendorSelect_Submit_btn() {
		 return GetElement(Order_VendorSelect_Submit_btn_xpath);
	}
		
	public  WebElement Order_VendorSelect_Edit_btn() {
		return GetElement(Order_VendorSelect_Edit_btn_xpath);
	}

	public  WebElement NewOrder_btn() {
	  return GetElement(NewOrder_btn_xpath);
	 }
	
	public  WebElement Order_SelectedContact_vlue() {
		  return GetElement(Order_SelectedContact_vlue_xpath);
		 }
	
	public  WebElement OrderList_tbl() {
		  return GetElement(OrderList_tbl_xpath);
		 }
	
	public  WebElement mdal_Descr_txt() {
		  return GetElement(mdal_Descr_txt_xpath);
		 }

	public  WebElement mdal_Domestic_opt() {
		  return GetElement(mdal_Domestic_opt_xpath);
		 }
	
	public  WebElement mdal_International_opt() {
		  return GetElement(mdal_International_opt_xpath);
		 }
	
	public  WebElement mdal_HHG_ServiceType_ddl() {
		  return GetElement(mdal_HHG_ServiceType_ddl_xpath);
		 }
	
	public  WebElement mdal_DosMoveType_ddl() {
		  return GetElement(mdal_DosMoveType_ddl_xpath);
		 }
	
	public  WebElement mdal_OrderType_ddl() {
		  return GetElement(mdal_OrderType_ddl_xpath);
		 }
	
	public  WebElement mdal_OtherServiceType_txt() {
		  return GetElement(mdal_OtherServiceType_txt_xpath);
		 }
	
	public  WebElement mdal_Submit_btn() {
		  return GetElement(mdal_Submit_btn_xpath);
		 }
	
	public  WebElement OrderName_Header() {
		return GetElement(OrderName_Header_xpath);
	}
	
	public  WebElement Order_Copy_btn(String order_descr) {
		
		return GetElementInTable(order_descr, OrderList_tbl(), Order_Copy_btn_xpath);

	}
	
	public  WebElement Order_ConfirmCopy_btn() {
		return GetElement(Order_ConfirmCopy_btn_xpath);
	}

	
	public  WebElement OrderInfo_Edit_btn() {
		return GetElement(OrderInfo_Edit_btn_xpath);
	 }
	
	public  WebElement OrderDesc_txt() {
		return GetElement(OrderDesc_txt_xpath);
	 }
	
	public  WebElement HHGService_Type_ddl() {
		return GetElement(HHGService_Type_ddl_xpath);
	 }
	
	public  WebElement Domestic_MoveType_ddl() {
		return GetElement(Domestic_MoveType_ddl_xpath);
	 }
	
	public  WebElement ShipMode_Auth_ddl() {
		return GetElement(ShipMode_Auth_ddl_xpath);
	 }
	
	public  WebElement OrderDte_txt() {
		return GetElement(OrderDte_txt_xpath);
	 }
	
	public  WebElement ContactDte_txt() {
		return GetElement(ContactDte_txt_xpath);
	 }
	
	public  WebElement AssignDte_txt() {
		return GetElement(AssignDte_txt_xpath);
	 }
	
	public  WebElement SurveyDte_txt() {
		return GetElement(SurveyDte_txt_xpath);
	 }
	
	public  WebElement Other_txt() {
		return GetElement(Other_txt_xpath);
	 }
	
	public  WebElement Status_ddl() {
		return GetElement(Status_ddl_xpath);
	 }
	
	public  WebElement DateIn_txt() {
		return GetElement(DateIn_txt_xpath);
	 }
	
	public  WebElement DateOut_txt() {
		return GetElement(DateOut_txt_xpath);
	 }
	
	public  WebElement OrderInfo_Update_btn() {
		return GetElement(OrderInfo_Update_btn_xpath);
	 }
	
	public  WebElement OrderInfo_Cancel_btn() {
		return GetElement(OrderInfo_Cancel_btn_xpath);
	 }
	

	public  WebElement Bill_Invoice_Edit_btn() {
	  	return GetElement(Bill_Invoice_Edit_btn_xpath);
	 }
	
	public  WebElement Bill_Invoice_Update_btn() {
	  	return GetElement(Bill_Invoice_Update_btn_xpath);
	 }
	
	public  WebElement Bill_Invoice_Cancel_btn() {
	  	return GetElement(Bill_Invoice_Cancel_btn_xpath);
	 }
	
	public  WebElement Total_ServiceCharge_txt() {
	  	return GetElement(Total_ServiceCharge_txt_xpath);
	 }
	
	public  WebElement Override_chbox() {
	  	return GetElement(Override_chbox_xpath);
	 }
	
	public  WebElement EscrowPer_txt() {
	  	return GetElement(EscrowPer_txt_xpath);
	 }
	
	public  WebElement Escrow_Amount_txt() {
	  	return GetElement(Escrow_Amount_txt_xpath);
	 }
	
	public  WebElement Fee_Received_chbox() {
	  	return GetElement(Fee_Received_chbox_xpath);
	 }
	
	public  WebElement Audit_Fee1_txt() {
	  	return GetElement(Audit_Fee1_txt_xpath);
	 }
	
	public  WebElement Audit_Fee2_txt() {
	  	return GetElement(Audit_Fee2_txt_xpath);
	 }
	
	public  WebElement Audit_Fee3_txt() {
	  	return GetElement(Audit_Fee3_txt_xpath);
	 }
	
	public  WebElement TotalInvoice_txt() {
	  	return GetElement(TotalInvoice_txt_xpath);
	 }
	
	public  WebElement TotalBill_Mover_txt() {
	  	return GetElement(TotalBill_Mover_txt_xpath);
	 }
	
	public  WebElement TotalARC_Profit_txt() {
	  	return GetElement(TotalARC_Profit_txt_xpath);
	 }
	
	public  WebElement DteBill_txt() {
	  	return GetElement(DteBill_txt_xpath);
	 }
	
	public  WebElement DteReceivePayment_txt() {
	  	return GetElement(DteReceivePayment_txt_xpath);
	 }
	
	public  WebElement DteInvoice_Submit_txt() {
	  	return GetElement(DteInvoice_Submit_txt_xpath);
	 }
	
	public  WebElement Rating_Edit_btn() {
	  	return GetElement(Rating_Edit_btn_xpath);
	 }
	
	public  WebElement Rating_Update_btn() {
	  	return GetElement(Rating_Update_btn_xpath);
	 }
	
	public  WebElement Rating_Cancel_btn() {
	  	return GetElement(Rating_Cancel_btn_xpath);
	 }
	
	public  WebElement Rating_Note_txt() {
	  	return GetElement(Rating_Note_txt_xpath);
	 }
	
	public  WebElement UpdateOther_txt() {
	  	return GetElement(UpdateOther_txt_xpath);
	 }
	
	
	//=========================================METHOD
	public  void Fill_Modal_Value_func(Service_Order_Info order,String button)
	{	
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		//	Wait_For_ElementDisplay(mdal_Descr_txt());
		
		ClearThenEnterValueToField(mdal_Descr_txt(), order.Description);
		
		if(order.Is_Domestic==true)
		{
			Optimize_ElementClick(mdal_Domestic_opt());
			
			SelectDropdownItem(mdal_DosMoveType_ddl(), order.DosmeticMoveType,"mdal_DosMoveType_ddl");
		}
			
		else
			Optimize_ElementClick(mdal_International_opt());
		
		String note_str = "mdal_HHG_ServiceType_ddl";
		
		SelectDropdownItem(mdal_HHG_ServiceType_ddl(), order.ServiceType, note_str);
		
		note_str = "mdal_OrderType_ddl";
		
		SelectDropdownItem(mdal_OrderType_ddl(), order.OrderType, note_str);
		
		
		if(order.OrderType.equals("Other"))
		{
			note_str = "mdal_OtherServiceType_txt";
			
			SelectDropdownItem(mdal_OtherServiceType_txt(), order.OtherServiceType, note_str);
	
		}
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(mdal_Submit_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Creating New Order",false);
		}
		
	}//end void
	
	public  void Copy_Order_func(String order_descr)
	{
		Optimize_ElementClick(Order_Copy_btn(order_descr));
			
		Optimize_ElementClick(Order_ConfirmCopy_btn());
	}
	

	public  void Select_Vendor_To_Order_func(Service_Order_Info order,String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Order_VendorSelect_Edit_btn());
			
		Optimize_ElementClick(Order_SelectedContact_vlue());
		
		Optimize_ElementSendkey(Order_VendorSelect_txt(), order.VendorName);
		
		SelectDropdownItem(Order_VendorSelect_ddl(), order.VendorName, "Vendor Select dropdown");
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Order_VendorSelect_Submit_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Creating New Order",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
	}
	
	public  void Open_Order_Details_func(String order_description)
	{
		if(OrderName_Header() == null || !OrderName_Header().getText().equals(order_description))
		
			Optimize_ElementClick(OrderDesc_link(order_description));
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	
	public  void Update_OrderInfo_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Optimize_ElementClick(OrderInfo_Edit_btn());
		
		ClearThenEnterValueToField(OrderDesc_txt(), order.Description);
		
		if (order.Is_Domestic == true)
		{
			SelectDropdownItem(Domestic_MoveType_ddl(), order.DosmeticMoveType, "Domestic Moving Type dropdown");
		}
		else
		{
			SelectDropdownItem(ShipMode_Auth_ddl(), order.ShipModeAuth, "Shipment Mode Authorized dropdown");
		}
		
		Optimize_ElementSendkey(OrderDte_txt(), order.OrderDate);
		
		OrderDte_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(ContactDte_txt(), order.ContactDate);
		
		ContactDte_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(AssignDte_txt(), order.AssignDate);
		
		AssignDte_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(SurveyDte_txt(), order.SurveyDate);
		
		SurveyDte_txt().sendKeys(Keys.ENTER);
		
		ClearThenEnterValueToField(Other_txt(), order.Other);
		
		switch(order.ServiceType)
		{
			case "SIT" : case "STG" : case " EXT" :
				SelectDropdownItem(Status_ddl(), order.OrderInfo_Status, "Delivery Status dropdown");
				
				Optimize_ElementSendkey(DateIn_txt(), order.OrderInfo_DateIn);
				
				OrderDte_txt().sendKeys(Keys.ENTER);
				
				Optimize_ElementSendkey(DateOut_txt(), order.OrderInfo_DateOut);
				
				ContactDte_txt().sendKeys(Keys.ENTER);
			break;
		}
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(OrderInfo_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in OrderInfo",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
		
	}
	

	public  void Update_BillingInvoice_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Bill_Invoice_Edit_btn());
		
		ClearThenEnterValueToField(Total_ServiceCharge_txt(), String.format("%.2f", order.TotalServiceCharge));
		
		ClearThenEnterValueToField(EscrowPer_txt(), order.EscrowPercentage);			
				
		ClearThenEnterValueToField(Escrow_Amount_txt(), String.format("%.2f", order.EscrowAmount) );
		
		ScrollElementtoViewPort_func(Escrow_Amount_txt());
		
		Optimize_ElementSendkey(DteBill_txt(), order.DateBilledCient);
		
		DteBill_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(DteReceivePayment_txt(), order.DateReceviedPayment);
		
		DteReceivePayment_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(DteInvoice_Submit_txt(), order.DateInvoiceSubmitted);
		
		DteInvoice_Submit_txt().sendKeys(Keys.ENTER);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Bill_Invoice_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Billing Invoice section",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
	}
	
	//=======================================METHODS VERIFY

	public  void Verify_OrderDetail_VendorSelected_func(String vendor_name)
	{
		
		String field = "";
		
		field = "Vendor Selected";
		
		VerifyFieldValueEqual_func(field,Order_VendorSelect_txt(), vendor_name,"");
		
	}//end void
	
	public  void Verify_OrderDetail_OrderInfo_func(Service_Order_Info order)
	{
		String field = "";
		
		field = "Order Description";
		
		VerifyFieldValueEqual_func(field,OrderDesc_txt(), order.Description,"");
		
		field = "HHG Service Type";
		
		VerifyFieldValueEqual_func(field,HHGService_Type_ddl(), order.ServiceType,"");
	
		if (order.Is_Domestic == true)
		{
			field = "Domestic Moving Type";
		
			VerifyFieldValueEqual_func(field,Domestic_MoveType_ddl(), order.DosmeticMoveType,"");
		}
		else
		{
			field = "Shipment Mode Authorized";
			
			VerifyFieldValueEqual_func(field,ShipMode_Auth_ddl(), order.ShipModeAuth,"");
		}
		field = "Order Date";
		
		VerifyFieldValueEqual_func(field,OrderDte_txt(), order.OrderDate,"");
		
		field = "1ST Contact Date";
		
		VerifyFieldValueEqual_func(field,ContactDte_txt(), order.ContactDate,"");
		
		field = "Assign Date";
		
		VerifyFieldValueEqual_func(field,AssignDte_txt(), order.AssignDate,"");
		
		field = "Survey Date";
		
		VerifyFieldValueEqual_func(field,SurveyDte_txt(), order.SurveyDate,"");
		
		field = "Other";
		
		VerifyFieldValueEqual_func(field,UpdateOther_txt(), order.Other,"");
		
		switch(order.ServiceType)
		{
			case "SIT" : case "STG" : case " EXT" :
				
				field = "Delivery Status";
				
				VerifyFieldValueEqual_func(field, Status_ddl(), order.OrderInfo_Status, "");
				
				field = "Date In Storage";
				
				VerifyFieldValueEqual_func(field, DateIn_txt(), order.OrderInfo_DateIn, "");
				
				field = "Date Out Of Storage";
				
				VerifyFieldValueEqual_func(field, DateOut_txt(), order.OrderInfo_DateOut, "");
				
			break;
		}
		
	}//end void
	

	
	public  void Verify_OrderDetail_BillInvoice_func(Service_Order_Info order)
	{
		String field = "";
		
		field = "Total Services Charge";
		
		if (order.TotalServiceCharge==0)
		{
			VerifyFieldValueEqual_func(field,Total_ServiceCharge_txt(), "","");
		}
		else 
		{
		
		VerifyFieldValueEqual_func(field,Total_ServiceCharge_txt(), Data_Optimize.Format_Double_WithCurrency(order.TotalServiceCharge) ,"");
		}
		
		field = "Escrow Percentage";
		
		VerifyFieldValueEqual_func(field,EscrowPer_txt(), order.EscrowPercentage,"");
		
		field = "Escrow Amount";
		
		if (order.EscrowAmount==0)
		{
			VerifyFieldValueEqual_func(field,Escrow_Amount_txt(), "","");
		}
		else 
		{
			
		VerifyFieldValueEqual_func(field,Escrow_Amount_txt(), Data_Optimize.Format_Double_WithCurrency(order.EscrowAmount),"");
		}
		
		field = "Date Billed Client";
		
		VerifyFieldValueEqual_func(field,DteBill_txt(), order.DateBilledCient,"");
		
		field = "Date Received Payment From Client";
		
		VerifyFieldValueEqual_func(field,DteReceivePayment_txt(), order.DateReceviedPayment,"");
		
		field = "Date Invoice Submitted";
		
		VerifyFieldValueEqual_func(field,DteInvoice_Submit_txt(), order.DateInvoiceSubmitted,"");
		
	}//end void
		
	public  void Verify_OrderDetail_BillInvoice_in_ClientPortal_func(Service_Order_Info order)
	{
		String field = "";
		
		field = "Total Services Charge";
		
		if (order.TotalServiceCharge==0.00)
		{
			VerifyFieldValueEqual_func(field,Total_ServiceCharge_txt(), "","");
		}
		else 
		{
			VerifyFieldValueEqual_func(field,Total_ServiceCharge_txt(), Data_Optimize.Format_Double_WithCurrency(order.TotalServiceCharge),"");
		}
		
		field = "Date Billed Client";
		
		VerifyFieldValueEqual_func(field,DteBill_txt(), order.DateBilledCient,"");
		
		field = "Date Received Payment From Client";
		
		VerifyFieldValueEqual_func(field,DteReceivePayment_txt(), order.DateReceviedPayment,"");
		
	}//end void
	
}
