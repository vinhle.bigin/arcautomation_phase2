package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Vendor_Info;
import configuration.DriverConfig.DriverBase;

public class Vendor_SearchOption_tab extends PageObjects{
	
	
	
	public Vendor_SearchOption_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}



	private  String SearchOpt_tab_xpath ="//a[contains(@href,'#vendor-search-option')]";
	private  String Edit_btn_xpath ="//div[@id ='search-infomation-panel']//a[@data-original-title='Edit']";
	private  String Update_btn_xpath ="//div[@id ='search-infomation-panel']//button[@data-control='update']";
	private  String Cancel_btn_xpath ="//div[@id ='search-infomation-panel']//button[@data-control='cancel-update']";
	private  String IsPreferVendor_opt_xpath ="//div[@id ='search-infomation-panel']//input[@type='checkbox']";
	private  String IsUseZip_opt_xpath ="//div[@id ='search-infomation-panel']//input[@name ='check-mile']";
	private  String IsUSA_opt_xpath ="//div[@id ='search-infomation-panel']//input[@name ='in-usa']";
	private  String IsInternation_opt_xpath ="//div[@id ='search-infomation-panel']//input[@name ='international']";
	private  String ExcludeClient_ddl_xpath ="//div[@id ='search-infomation-panel']//div[@class ='b__components b__multi__select']";
	private  String IncludeZip_txt_xpath ="//input[@id ='vendor-detail-zip-code']";
	private  String IncludeCity_txt_xpath = "//input[@id='vendor-detail-city']";
	private  String IncludeState_txt_xpath = "//input[@placeholder='States']";
	private  String IncludeState_arrow_xpath = "//label[contains(text(),'States')]/..//div[contains(@class,'iconC')]";
	private  String IncludeState_ddl_xpath = "//label[contains(text(),'States')]/..//ul";
	


	
	public  WebElement IncludeCity_txt(){
		
			 return GetElement(IncludeCity_txt_xpath);
			
		
	}
		
	public  WebElement IncludeState_txt(){
		
		 return GetElement(IncludeState_txt_xpath);
		
		}
	
	public  WebElement IncludeState_arrow(){
		
		 return GetElement(IncludeState_arrow_xpath);
		
	
	}
	
	public  WebElement IncludeState_ddl(){
		
		 return GetElement(IncludeState_ddl_xpath);
		
	
	}
	
	
	public  WebElement SearchOpt_tab(){
		try{
			
			 return GetElement(SearchOpt_tab_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}
	
	
	public  WebElement Edit_btn(){
		try{
			
			 return GetElement(Edit_btn_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}
	public  WebElement Update_btn(){
		try{
			
			 return GetElement(Update_btn_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}
	public  WebElement Cancel_btn(){
		try{
			
			 return GetElement(Cancel_btn_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}
	public  WebElement IsPreferVendor_opt(){
		try{
			
			 return GetElement(IsPreferVendor_opt_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}
	public  WebElement IsUseZip_opt(){
		try{
			
			 return GetElement(IsUseZip_opt_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}
	public  WebElement IsUSA_opt(){
		try{
			
			 return GetElement(IsUSA_opt_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}
	public  WebElement IsInternation_opt(){
		try{
			
			 return GetElement(IsInternation_opt_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}
	public  WebElement ExcludeClient_ddl(){
		try{
			
			 return GetElement(ExcludeClient_ddl_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}


	public  WebElement IncludeZip_txt(){
		try{
			
			 return GetElement(IncludeZip_txt_xpath);
			
		}
		catch(NoSuchElementException e){
			return null;
		}
	}



	//===========================================METHOD

	public  void UpdateSearchOption_fun(Vendor_Info vendor ,String button)
	{
		Optimize_ElementClick(SearchOpt_tab());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
				
		Optimize_ElementClick(Edit_btn());
		
		String isprefer = GetFieldText_func(IsPreferVendor_opt());

		if(!Boolean.toString(vendor.SearchOpt_IsPrefered).equals(isprefer))
		{
			Optimize_ElementClick(IsPreferVendor_opt());
		}

		if(vendor.SearchOpt_IsZip==true)
		{Optimize_ElementClick(IsUseZip_opt());

			int count = vendor.SearchOpt_ZipList.size();
			for(int i =0;i<count;i++)
			{
				IncludeZip_txt().sendKeys(vendor.SearchOpt_ZipList.get(i));
				if(i!=count-1)
				{
					IncludeZip_txt().sendKeys(",");
				}
			}//end for
			
		
		//========State	
		count = vendor.SearchOpt_StateList.size();
		
		for(int i =0;i<count;i++)
		{
			Optimize_ElementClick(IncludeState_arrow());
			
			
		//	Wait_For_ElementDisplay(IncludeState_txt());
			
			ClearThenEnterValueToField(IncludeState_txt(), vendor.SearchOpt_StateList.get(i));
			
			
			ScrollElementtoViewPort_func(SearchOpt_tab());
					
			SelectDropdownItem(IncludeState_ddl(), vendor.SearchOpt_StateList.get(i), "");
						
			Optimize_ElementClick(IncludeState_arrow());
			
			
		}//end for
		
		//=============City
		count = vendor.SearchOpt_CityList.size();
		
		for(int i =0;i<count;i++)
		{
			IncludeCity_txt().sendKeys(vendor.SearchOpt_CityList.get(i));
			if(i!=count-1)
			{
				IncludeCity_txt().sendKeys(",");
			}
		}//end for
			
			
		}//end if

		else if(vendor.SearchOpt_IsUSA==true)
			Optimize_ElementClick(IsUSA_opt());

		else if(vendor.SearchOpt_IsInter==true)
			Optimize_ElementClick(IsInternation_opt());

		if(button.equals("Submit")||button.equals("submit"))
			Optimize_ElementClick(Update_btn());

		else if(button.equals("Cancel")||button.equals("Cancel"))
			Optimize_ElementClick(Cancel_btn());

	}	//end void

}
