package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import businessObjects.Transferee_Info;
import configuration.DriverConfig.DriverBase;



public class Transf_Address_tab extends PageObjects{

	
	
public Transf_Address_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


private  String Address_Tab_xpath = "//a[@href = '#transferee-address']";
	
	


	
	public  WebElement Address_Tab() {
	  return GetElement(Address_Tab_xpath);
	 }


	
/////////================================METHOD
	public  void Verify_DesResViewMode_func(Address_Info addr_info)
	{
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		Optimize_ElementClick(Address_Tab());
		
		ARCLoading.Wait_Util_FinishedARClogoLoading();

		ARCLoading.Wait_Util_FinishedPageLoading();

		///ORG RESIDENCE
		new Transf_AddrDesRes_sect(driverbase).VerifyInfoViewMode_func(addr_info);
		
	}	
	
public  void Verify_OrgResViewMode_func(Address_Info addr_info)
{
	ARCLoading ARCLoading = new ARCLoading(driverbase);
	
	Optimize_ElementClick(Address_Tab());
	
	ARCLoading.Wait_Util_FinishedARClogoLoading();

	ARCLoading.Wait_Util_FinishedPageLoading();

	///ORG RESIDENCE
	new Transf_AddrOriRes_sect(driverbase).VerifyInfoViewMode_func(addr_info);

}
	
public  void VerifyInfoViewMode_func(Transferee_Info tf)
{
	try
	{
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		ARCLoading.Wait_Util_FinishedPageLoading();

		Optimize_ElementClick(Address_Tab());

		ARCLoading.Wait_Util_FinishedPageLoading();

	//	Wait_For_ElementDisplay(Transf_AddrOriRes_sect.OrgRes_Edit_btn());

		Thread.sleep(10000);

		///ORG RESIDENCE
		new Transf_AddrOriRes_sect(driverbase).VerifyInfoViewMode_func(tf.OriResAddr_info);

		//====================================ORG OFF
		new Transf_AddOriOff_sect(driverbase).VerifyInfoViewMode_func(tf.OriOffAddr_info);

		///===================================DESTINATION RESIDENCE
		new Transf_AddrDesRes_sect(driverbase).VerifyInfoViewMode_func(tf.DesResAddr_info);
		
		///DES OFF
		new Transf_AddrDesOff_sect(driverbase).VerifyInfoViewMode_func(tf.DesOffAddr_info);
	}catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
}


//EDIT ADDRESS TAB
		public  void EditAddressInfo_func(Transferee_Info tf)
		{
		

			Optimize_ElementClick(Address_Tab());

			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

			new Transf_AddrOriRes_sect(driverbase).Edit_TransfAddrInfo_func(tf.OriResAddr_info);

			new Transf_AddOriOff_sect(driverbase).Edit_TransfAddrInfo_func(tf.OriOffAddr_info);
			new Transf_AddrDesRes_sect(driverbase).Edit_TransfAddrInfo_func(tf.DesResAddr_info);

			new Transf_AddrDesOff_sect(driverbase).Edit_TransfAddrInfo_func(tf.DesOffAddr_info);
		}

		public void Goto_Address_tab()
		{
			Optimize_ElementClick(Address_Tab());
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
}
