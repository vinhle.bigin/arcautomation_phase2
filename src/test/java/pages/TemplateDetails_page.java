package pages;


import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Template_Info;
import configuration.DriverConfig.DriverBase;
import custom_Func.FileManage;

public class TemplateDetails_page extends PageObjects{
	

	
	public TemplateDetails_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Edit_Type_ddl_xpath= "//select[@placeholder = 'Type']";
	private  String Edit_Target_ddl_xpath= "//select[@placeholder = 'Target Record']";
	private  String Edit_EmailFrom_ddl_xpath= "//ul[@class ='b__multi__select__list']";
	private  String Edit_ServiceType_ddl_xpath= "//select[@placeholder = 'Service Type']";
	private  String Edit_EmailFrom_txt_xpath= "//input[@placeholder = 'Email From']";
	private  String Edit_EmailFrom_vlue_xpath = "//span[@class='thumb']";
	private  String Edit_Name_txt_xpath= "//input[@placeholder = 'Template Name']";
	private  String Edit_Descr_txt_xpath= "//input[@placeholder = 'Description']";
	private  String Edit_Subject_txt_xpath= "//input[@placeholder = 'Subject']";
	private  String Edit_Search_txt_xpath= "//input[@placeholder = 'Search box']";
	private  String Edit_Attach_lnk_xpath= "//div[@class = 'content dz-clickable']";
	private  String Edit_Submit_btn_xpath= "//button[@id = 'submit-template']";
	private  String Edit_Cancel_btn_xpath= "//button[@cancel = 'cancel-template']";

	private  String New_IsDocProcess_xpath = "//span[contains(text(),'doc_name_arg')]/..//input[contains(@name,'is-process')]";

	private  String New_DocRemove_icon_xpath = "//span[contains(text(),'doc_name_arg')]/..//a[@class='remove-archive']";

	private  String New_ProcessDocX_opt_xpath = "//span[contains(text(),'doc_name_arg')]/..//input[@value='docx']";

	private  String New_ProcessPDF_opt_xpath = "//span[contains(text(),'doc_name_arg')]/..//input[@value='pdf']";
	
	private  String Edit_ProcessDocX_opt_xpath = "//span[contains(text(),'doc_name_arg')]/../../..//div[@value='docx']/input";

	private  String Edit_ProcessPDF_opt_xpath = "//span[contains(text(),'doc_name_arg')]/../../..//div[@value='pdf']/input";


	//	Type = Address Label
	private  String Edit_CtentAddr1_txt_xpath = "//input[@placeholder = 'Address 1']";
	private  String Edit_CtentAddr2_txt_xpath = "//input[@placeholder = 'Address 2']";
	private  String Edit_CtentAddr3_txt_xpath = "//input[@placeholder = 'Address 3']";
	private  String Edit_CtentAddr4_txt_xpath = "//input[@placeholder = 'Address 4']";
	private  String Edit_CtentAddr5_txt_xpath = "//input[@placeholder = 'Address 5']";

	private  String Edit_IsActive_chk_xpath = "//input[@type = 'checkbox']";


	
	public  WebElement Edit_Type_ddl() {
	  return GetElement(Edit_Type_ddl_xpath);
	 }
	public  WebElement Edit_Target_ddl() {
		  return GetElement(Edit_Target_ddl_xpath);
		 }
	public  WebElement Edit_EmailFrom_ddl() {
		  return GetElement(Edit_EmailFrom_ddl_xpath);
		 }
	public  WebElement Edit_ServiceType_ddl() {
		  return GetElement(Edit_ServiceType_ddl_xpath);
		 }
	public  WebElement Edit_EmailFrom_txt() {
		  return GetElement(Edit_EmailFrom_txt_xpath);
		 }
	public  WebElement Edit_EmailFrom_vlue() {
		  return GetElement(Edit_EmailFrom_vlue_xpath);
		 }
	public  WebElement Edit_Name_txt() {
		  return GetElement(Edit_Name_txt_xpath);
		 }
	public  WebElement Edit_Descr_txt() {
		  return GetElement(Edit_Descr_txt_xpath);
		 }
	public  WebElement Edit_Subject_txt() {
		  return GetElement(Edit_Subject_txt_xpath);
		 }
	public  WebElement Edit_Search_txt() {
		  return GetElement(Edit_Search_txt_xpath);
		 }
	public  WebElement Edit_Attach_lnk() {
		  return GetElement(Edit_Attach_lnk_xpath);
		 }
	public  WebElement Edit_Submit_btn() {
		  return GetElement(Edit_Submit_btn_xpath);
		 }
	public  WebElement Edit_Cancel_btn() {
		  return GetElement(Edit_Cancel_btn_xpath);
		 }

	public  WebElement Edit_CtentAddr1_txt() {
		  return GetElement(Edit_CtentAddr1_txt_xpath);
		 }
	public  WebElement Edit_CtentAddr2_txt() {
		  return GetElement(Edit_CtentAddr2_txt_xpath);
		 }
	public  WebElement Edit_CtentAddr3_txt() {
		  return GetElement(Edit_CtentAddr3_txt_xpath);
		 }
	public  WebElement Edit_CtentAddr4_txt() {
		  return GetElement(Edit_CtentAddr4_txt_xpath);
		 }
	public  WebElement Edit_CtentAddr5_txt() {
		  return GetElement(Edit_CtentAddr5_txt_xpath);
		 }
	public  WebElement Edit_IsActive_chk() {
		  return GetElement(Edit_IsActive_chk_xpath);
		 }

	public  WebElement Edit_ProcessDocX_opt(String doc_name) {
		
			String xpath_str = Edit_ProcessDocX_opt_xpath.replace("doc_name_arg", doc_name);
			
			WebElement temp_element =  GetElement(xpath_str);
					
			return temp_element;
		
	}


	public  WebElement Edit_ProcessPDF_opt(String doc_name) {
	
			String xpath_str = Edit_ProcessPDF_opt_xpath.replace("doc_name_arg", doc_name);
			
			WebElement temp_element =  GetElement(xpath_str);
				

			return temp_element;
		
	}

	public  WebElement New_ProcessDocX_opt(String doc_name) {
		
			String xpath_str = New_ProcessDocX_opt_xpath.replace("doc_name_arg", doc_name);
			
			WebElement temp_element =  GetElement(xpath_str);
			

			return temp_element;
	
	}


	public  WebElement New_ProcessPDF_opt(String doc_name) {
		
			String xpath_str = New_ProcessPDF_opt_xpath.replace("doc_name_arg", doc_name);
			
			WebElement temp_element =  GetElement(xpath_str);
				

			return temp_element;
		
	}


	public  WebElement New_IsDocProcess(String doc_name) {
		
			String xpath_str = New_IsDocProcess_xpath.replace("doc_name_arg", doc_name);
			
			WebElement temp_element =  GetElement(xpath_str);
			

			return temp_element;
	
	}
	
	

	public  WebElement New_DocRemove_icon(String doc_name) {
		

			String xpath_str = New_DocRemove_icon_xpath.replace("doc_name_arg", doc_name);
			
			WebElement temp_element =  GetElement(xpath_str);
					

			return temp_element;
	
	}
			
		
			
	//FILL INFO INTO CREATE NEW TEMPLATE
	public  void FillInfo_func(String action_str,Template_Info tpl,String button_str)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		//NOTE:
		//action_str = "Create" | "Update"
		//button_str = "Submit" | "Cancel" | ""
		String note_str="";
		try {
			if(action_str.equals("Create"))
			{
				ScrollElementtoViewPort_func(Edit_Type_ddl());

				note_str = "Template Details - Type Type Dropdown.";
				
				SelectDropdownItem(Edit_Type_ddl(), tpl.Type,note_str);

				note_str = "Template Details - Target Type Dropdown.";
				
				SelectDropdownItem(Edit_Target_ddl(),tpl.Target,note_str);
			}

			ClearThenEnterValueToField(Edit_Name_txt(),tpl.Name);

			if(tpl.IsActive!=Edit_IsActive_chk().isSelected())
				Optimize_ElementClick(Edit_IsActive_chk());
			

			switch(tpl.Type)
			{
				case "Address Label":
					ClearThenEnterValueToField(Edit_CtentAddr1_txt(), tpl.Content);

					ClearThenEnterValueToField(Edit_CtentAddr2_txt(), tpl.Content);

					ClearThenEnterValueToField(Edit_CtentAddr3_txt(), tpl.Content);

					ClearThenEnterValueToField(Edit_CtentAddr4_txt(), tpl.Content);
					
					ClearThenEnterValueToField(Edit_CtentAddr5_txt(), tpl.Content);

					break;

				case "Email":
					
					note_str = "Template Details - Service Type Dropdown.";
					
					SelectDropdownItem( Edit_ServiceType_ddl(),tpl.ServiceType,note_str);

					Edit_EmailFrom_vlue().click();

					Thread.sleep(1000);

					if(!tpl.EmailFrom.equals(""))
						ClearThenEnterValueToField(Edit_EmailFrom_txt(), tpl.EmailFrom);

					note_str = "Template Details - Email From Dropdown.";
					
					SelectDropdownItem(Edit_EmailFrom_ddl(),tpl.EmailFrom,note_str);

					ClearThenEnterValueToField(Edit_Subject_txt(),tpl.Subject);

					break;

			}
			
			//Fill Content Box
			
			if(tpl.Type.equals("Email")||tpl.Type.equals("Letter"))
			{
				//ScrollElementtoViewPort_func(Page_TinyContentBox());
				
				//Optimize_ElementClick(Page_TinyContentBox());
				
				//ClearThenEnterValueToField(Page_TinyContentBox(), tpl.Content);
				
				Optimize_ElementSendkey(Page_TinyContentBox(), tpl.Content);
			}
			
		

			//UPLOAD DOCUMENT / PROCESS DOCUMENT
			if(tpl.DocName!="")
			{
				new FileManage().UploadFile_func(Edit_Attach_lnk(), tpl.DocName);



				if(tpl.DocProcessType.equals("DOCX"))
				{
					Optimize_ElementClick(New_IsDocProcess(tpl.DocName));

					Optimize_ElementClick(New_ProcessDocX_opt(tpl.DocName));

				}//end if IsProcess

				else if(tpl.DocProcessType.equals("PDF"))
				{
					Optimize_ElementClick(New_IsDocProcess(tpl.DocName));

					Optimize_ElementClick(New_ProcessPDF_opt(tpl.DocName));
				}

			}//end if Docment!=""

			if(button_str.equals("Submit"))
				Edit_Submit_btn().click();
			else if(button_str.equals("Cancel"))
				Edit_Cancel_btn().click();

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"",false);
		}	
	 catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}//end void
	
	public  void VerifyDetails_func(Template_Info tpl)
	{
		
		String area = "Template Details-";
		String field_name = "";
		String mode = "";

		//WaitForElementNotDisplay(LandingPage.LoadingArc())

		
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

	//	Wait_For_ElementDisplay(Edit_ServiceType_ddl());

		//WebUI.delay(5)
		field_name = "Edit_Type_ddl";
		VerifyFieldValueEqual_func(area+field_name , Edit_Type_ddl(),tpl.Type, mode);
		//VerifyFieldValueEqual_func(area+field_name , Edit_Type_ddl(),tf.OriResAddr_info.Addr1, mode)

		field_name = "Edit_Target_ddl";
		VerifyFieldValueEqual_func(area+field_name , Edit_Target_ddl(),tpl.Target, mode);

		switch(tpl.Type)
		{
			case "Address Label":

				field_name = "Edit_CtentAddr1_txt";
				VerifyFieldValueEqual_func(area+field_name , Edit_CtentAddr1_txt(), tpl.Content , mode);

				field_name = "Edit_CtentAddr2_txt";
				VerifyFieldValueEqual_func(area+field_name , Edit_CtentAddr2_txt(), tpl.Content , mode);

				field_name = "Edit_CtentAddr3_txt";
				VerifyFieldValueEqual_func(area+field_name , Edit_CtentAddr3_txt(), tpl.Content , mode);

				field_name = "Edit_CtentAddr4_txt";
				VerifyFieldValueEqual_func(area+field_name , Edit_CtentAddr4_txt(), tpl.Content , mode);

				field_name = "Edit_CtentAddr5_txt";
				VerifyFieldValueEqual_func(area+field_name , Edit_CtentAddr5_txt(), tpl.Content , mode);

				break;

			case "Email":
				field_name = "Edit_ServiceType_ddl";
				VerifyFieldValueEqual_func(area+field_name , Edit_ServiceType_ddl(),tpl.ServiceType, mode);


				field_name = "Edit_EmailFrom_txt";
				VerifyFieldValueEqual_func(area+field_name , Edit_EmailFrom_vlue(),tpl.EmailFrom, mode);

				field_name = "Edit_Subject_txt";
				VerifyFieldValueEqual_func(area+field_name , Edit_Subject_txt(),tpl.Subject , mode);

				break;

		}

		if(tpl.Type !="Address Label")
		{
			String field = "Content_txt";
			//VerifyTinyContentEqual_func(area+field,act_info.Content);
			VerifyFieldValueEqual_func(area+field,Page_TinyContentBox() , tpl.Content,"");

		}//end if

		if(tpl.DocName!="")
		{

			mode = "When Document set as Proccess";
			if(tpl.DocProcessType.equals("DOCX"))

			{
				field_name = "Edit_ProcessDocX_opt";
				VerifyFieldValueEqual_func(area+field_name , Edit_ProcessDocX_opt(tpl.DocName), "true", mode);
			}
			else if(tpl.DocProcessType.equals("PDF"))
			{
				field_name = "Edit_ProcessPDF_opt";
				VerifyFieldValueEqual_func(area+field_name , Edit_ProcessPDF_opt(tpl.DocName), "true", mode);

			}//else
			else
			{
				mode = "When Document not set as Proccess";
				field_name = "Edit_ProcessDocX_opt";
				VerifyFieldValueEqual_func(area+field_name , Edit_ProcessDocX_opt(tpl.DocName), "false", mode);

				field_name = "Edit_ProcessPDF_opt";
				VerifyFieldValueEqual_func(area+field_name , Edit_ProcessPDF_opt(tpl.DocName), "false", mode);
			}//end final else

		}//if Docname!=""


	}//end void

}
