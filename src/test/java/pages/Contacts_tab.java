package pages;

import org.junit.Assert;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Contact_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class Contacts_tab extends PageObjects{
	private ARCLoading ARCLoading;
	private Messages_Notification Messages_Notification;

	private  String actived_detail_mdal_xpath = "//div[contains(@class,'modal') and contains(@style,'display: block')]";
	
	private  String Contact_Tab_xpath = "//a[contains(@href,'-contacts')]";
	private  String ModalEdit_btn_xpath= actived_detail_mdal_xpath+ "//a[@data-original-title='Edit']";
	private  String ModalSubmit_btn_xpath= actived_detail_mdal_xpath+ "//button[@type = 'submit']";
	private  String ModalCancel_btn_xpath= "//div[@class = 'modal-footer']//button[contains(@data-control,'cancel')]";
	private  String New_btn_xpath = "//button[contains(text(),'New Contact')]";
	private  String ContactList_tbl_xpath = "//div[contains(@id,'contact')]//table//tbody";
	private  String Search_txt_xpath = "//input[@type='search']";
	private  String UserName_txt_xpath = actived_detail_mdal_xpath+"//input[@placeholder ='Username']";
	private  String Role_ddl_xpath = actived_detail_mdal_xpath+"//select[@placeholder ='Role']";

	private  String Modal_Client_Company_Name_xpath = actived_detail_mdal_xpath+ "//select[@placeholder='Client Name']";
	private  String Salutation_ddl_xpath= "//div[@org-placeholder ='Salutation']//select";
	private  String ModalFName_txt_xpath= actived_detail_mdal_xpath + "//input[@placeholder ='First Name' or @placeholder ='First name']"; //div[@org-placeholder ='First Name']//input |
	private  String ModalLName_txt_xpath= actived_detail_mdal_xpath+ "//input[@placeholder ='Last Name' or @placeholder ='Last name']" ;//div[@org-placeholder ='Last Name']//input  |
	private  String ModalCtactType_ddl_xpath= actived_detail_mdal_xpath+ "//select[@placeholder ='Contact Type']" ;//div[@org-placeholder ='Contact Type']//select |
	private  String ModalisDefault_chk_xpath= actived_detail_mdal_xpath+ "//label[text() ='Default' or contains(text(),'Main Contact')]/../input";
	private  String ModalAddr1_txt_xpath= actived_detail_mdal_xpath+ "//input[@placeholder ='Address1' or @placeholder ='Address 1']";
	private  String ModalCity_txt_xpath= actived_detail_mdal_xpath+ "//input[@placeholder ='City']";
	private  String ModalState_ddl_xpath= actived_detail_mdal_xpath+ "//select[@placeholder ='State']";
	private  String ModalZip_txt_xpath= actived_detail_mdal_xpath+ "//input[contains(@placeholder,'Zip')]";
	private  String ModalCountry_ddl_xpath= actived_detail_mdal_xpath+ "//select[@placeholder ='Country']"+" | "+actived_detail_mdal_xpath+ "//div[@org-placeholder ='Country']//select";
	private  String ModalEmailType_ddl_xpath= actived_detail_mdal_xpath+ "//div[@class='email-wrap extension-fields']//div[@class='row']//div[@org-placeholder='Type']//select";
	private  String ModalEmailAddr_txt_xpath= actived_detail_mdal_xpath+ "//div[@class='email-wrap extension-fields']//div[@class='row']//div[@org-placeholder='E-mail']//input";
	private  String FullName_lnk_xpath = "//a[text() ='fullname'] | //a/span[text() ='fullname']";
	
	private  String IsDefault_icon_xpath = "/.//div[@class='check-items-default checked']//i";
	private  String SetDefault_lnk_xpath = "//span[text() = 'Set as Default']";
	//private  String SetDefault_lnk_xpath = "/.//a[contains(@href,'contact/setDefault')]/span";
	private  String Remove_icon_xpath = "/.//span[@data-original-title ='Remove Contact' or @data-original-title = 'Remove Vendor Contact']";

	private  String ModalPhoneType_ddl_xpath= actived_detail_mdal_xpath+"//div[@class='mobile-wrap']//div[@class='row']//div[@org-placeholder='Type']//select";
	private  String ModalPhoneNumb_txt_xpath= actived_detail_mdal_xpath+"//div[@class='mobile-wrap']//div[@class='row']//div[@org-placeholder='Number']//input";

	private  String ModalServiceType_ddl_xpath = actived_detail_mdal_xpath+ "//select[@placeholder ='Vendor Contact Type']";
			
	private  String Modal_Close_icon_xpath = actived_detail_mdal_xpath+"//i[@class='icon-close']";
	
	private  String Edit_Profile_icon_xpath = "//span[@class = 'icon-note default-color']";
	private  String Contact_Profile_mdal = "//div[@id = 'loading-client-contact-profile']";
	private  String Edit_Contact_Profile_mdal_btn_xpath = actived_detail_mdal_xpath +"//div[@class = 'panel-control pull-right']//a[contains(text(),'Edit')]";
	private  String Username_Contact_Profile_txt_xpath = actived_detail_mdal_xpath +"//div[@class = 'form-group col-md-4']//input[@placeholder = 'Username']";
	private  String Password_Contact_Profile_txt_xpath = actived_detail_mdal_xpath +"//div[@class = 'form-group col-md-4']//input[@placeholder = 'Password']";
	private  String Submit_Contact_Profile_btn_xpath = actived_detail_mdal_xpath +"//div[@class = 'btn-group-action']//button[contains(text(),'Submit')]";
	private  String Close_Profile_icon_xpath = actived_detail_mdal_xpath +"//i[@class = 'icon-close']";
	
	public Contacts_tab(DriverBase driverbase) {
		// TODO Auto-generated constructor stub
		super(driverbase);
		ARCLoading = new ARCLoading(driverbase);
		Messages_Notification = new Messages_Notification(driverbase);
		
	}
	
	public  WebElement Modal_Close_icon() {
		  return GetElement(Modal_Close_icon_xpath );
		 }
	
	public  WebElement Role_ddl() {
		  return GetElement(Role_ddl_xpath );
		 }
	
	public  WebElement UserName_txt() {
		  return GetElement(UserName_txt_xpath );
		 }
	
	public  WebElement Modal_Client_Company_vlue() {
		  return GetElement(Modal_Client_Company_Name_xpath );
		 }
	
	public  WebElement Search_txt() {
		  return GetElement(Search_txt_xpath );
		 }
	
	
	public  WebElement Contact_Tab () {
	  return GetElement(Contact_Tab_xpath );
	 }
	
	public  WebElement ModalEdit_btn() {
	  return GetElement(ModalEdit_btn_xpath);
	 }
	
	public  WebElement ModalSubmit_btn() {
	  return GetElement(ModalSubmit_btn_xpath);
	 }
	
	public  WebElement ModalCancel_btn() {
	  return GetElement(ModalCancel_btn_xpath);
	 }
	
	public  WebElement New_btn () {
	  return GetElement(New_btn_xpath );
	 }
	
	public  WebElement Salutation_ddl() {
	  return GetElement(Salutation_ddl_xpath);
	 }
	
	public  WebElement ModalFName_txt() {
	  return GetElement(ModalFName_txt_xpath);
	 }
	
	public  WebElement ModalLName_txt() {
	  return GetElement(ModalLName_txt_xpath);
	 }
	
	public  WebElement ModalCtactType_ddl() {
	  return GetElement(ModalCtactType_ddl_xpath);
	 }
	
	public  WebElement ModalisDefault_chk() {
	  return GetElement(ModalisDefault_chk_xpath);
	 }
	
	public  WebElement ModalAddr1_txt() {
	  return GetElement(ModalAddr1_txt_xpath);
	 }
	
	public  WebElement ModalCity_txt() {
	  return GetElement(ModalCity_txt_xpath);
	 }
	
	public  WebElement ModalState_ddl() {
	  return GetElement(ModalState_ddl_xpath);
	 }
	
	public  WebElement ModalZip_txt() {
	  return GetElement(ModalZip_txt_xpath);
	 }
	
	public  WebElement ModalCountry_ddl() {
	  return GetElement(ModalCountry_ddl_xpath);
	 }
	
	public  WebElement ModalEmailType_ddl() {
	  return GetElement(ModalEmailType_ddl_xpath);
	 }
	
	public  WebElement ModalEmailAddr_txt() {
	  return GetElement(ModalEmailAddr_txt_xpath);
	 }


	public  WebElement ModalPhoneType_ddl() {
	  return GetElement(ModalPhoneType_ddl_xpath);
	 }
	
	public  WebElement ModalPhoneNumb_txt() {
	  return GetElement(ModalPhoneNumb_txt_xpath);
	 }
	
	public  WebElement ModalServiceType_ddl() {
	  return GetElement(ModalServiceType_ddl_xpath );
	 }
	
	public  WebElement ContactList_tbl() {
		  return GetElement(ContactList_tbl_xpath );
		 }


	public  WebElement FullName_lnk(String fullname_str) {
		
		
		String xpath_str = FullName_lnk_xpath.replace("fullname", fullname_str);
		return GetElement(xpath_str);
	
	}


	public  WebElement IsDefault_icon(String fullname_str) {
		return GetElementInTable(fullname_str, ContactList_tbl(), IsDefault_icon_xpath);
		
	}

	public  WebElement SetDefault_lnk(String fullname_str) {
		return GetElementInTable(fullname_str, ContactList_tbl(), SetDefault_lnk_xpath);
			
	}

	public  WebElement Remove_icon(String fullname_str) {
		return GetElementInTable(fullname_str, ContactList_tbl(), Remove_icon_xpath);
			
	}
	
	public  WebElement Edit_Profile_icon() {
		  return GetElement(Edit_Profile_icon_xpath);
	}
	
	public  WebElement Contact_Profile_mdal() {
		  return GetElement(Contact_Profile_mdal);
	}
	
	public  WebElement Edit_Contact_Profile_mdal_btn() {

		WebElement e_temp = GetElement(Edit_Contact_Profile_mdal_btn_xpath);
			
			//Catch xpath for Vendor
		if(e_temp==null)
		{
			String a = Edit_Contact_Profile_mdal_btn_xpath.replace("loading-client-contact-profile", "loading-vendor-contact-profile");
				
			e_temp = GetElement(a);
		}
		
		return e_temp;
	}
	
	public  WebElement Username_Contact_Profile_txt() {
		
		WebElement e_temp = GetElement(Username_Contact_Profile_txt_xpath);
		
		//Catch xpath for Vendor
		if(e_temp==null)
		{
			String a = Username_Contact_Profile_txt_xpath.replace("loading-client-contact-profile", "loading-vendor-contact-profile");
			
			e_temp = GetElement(a);
		}	
	
		return e_temp;
	}
	
	public  WebElement Password_Contact_Profile_txt() {
		
		WebElement e_temp = GetElement(Password_Contact_Profile_txt_xpath);
		
		//Catch xpath for Vendor
		if(e_temp==null)
		{
			String a = Password_Contact_Profile_txt_xpath.replace("loading-client-contact-profile", "loading-vendor-contact-profile");
			
			e_temp = GetElement(a);
		}	
	
		return e_temp;
	}
	
	public  WebElement Submit_Contact_Profile_btn() {
		  return GetElement(Submit_Contact_Profile_btn_xpath);
	}
	
	public  WebElement Close_Profile_icon() {
		
		WebElement e_temp = GetElement(Close_Profile_icon_xpath);
		
		//Catch xpath for Vendor
		if(e_temp==null)
		{
			String a = Close_Profile_icon_xpath.replace("loading-client-contact-profile", "loading-vendor-contact-profile");
			
			e_temp = GetElement(a);
		}	
	
		return e_temp;
	}
	
	//=========================METHODS
	//METHOD: REMOVE CONTACT
	//METHOD: REMOVE THE CLIENTCONTACT
	public  void RemoveContact_func(String fullName)
	{
		try {
	
			Messages_Notification = new Messages_Notification (driverbase);

		if(ContactList_tbl()==null)
			Optimize_ElementClick(Contact_Tab());
		
		
		Remove_icon(fullName).click();;
		
		Messages_Notification.Delete_btn().click();

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When removing the Contact",false);


		
			Thread.sleep(1000);
		} catch (InterruptedException | JavascriptException e) {
			
			TestConfigs.glb_TCFailedMessage+="Can't Remove the contact["+fullName+"] due to:"+e.getMessage();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		


	}//end void
	
	
	//METHOD: Update contact
	public  void UpdateContact_func(String old_fullname_str,Contact_Info new_ctact, String button)
	{
		
		ARCLoading = new ARCLoading(driverbase);
		
		Optimize_ElementClick(Contact_Tab());

		Optimize_ElementClick(FullName_lnk(old_fullname_str));

		ARCLoading.Wait_Util_FinishedPageLoading();


		//Wait_For_ElementDisplay(ModalEdit_btn());

		Optimize_ElementClick(ModalEdit_btn());


		FillModalInfo(new_ctact, button);
		
		
		if(Modal_Close_icon()!=null)
		{
			Modal_Close_icon().click();
		}

	}//end void
	
	
	//METHOD: Create Contact
	public  void CreateContact_func(Contact_Info ctact, String button)
	{
		
		
		Optimize_ElementClick(Contact_Tab());

		Optimize_ElementClick(New_btn());

		FillModalInfo(ctact, button);

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}//end void
	
		private  void Fill_Account_Login_func(String role, String username)
		{
			
			try {
				//Get the first Role item
				String role_item = role;
				
				
				if(role.equals(""))
				{
					
					Optimize_ElementClick(Role_ddl());
					
					WebElement e = Get_Dropdown_ItemsElement_func(Role_ddl()).get(0);
					
					role_item = e.getText();
						
				}
				
				String note_str = "Contact Modal - Role_ddl";
				
				SelectDropdownItem(Role_ddl(), role_item, note_str);
				
				ClearThenEnterValueToField(UserName_txt(), username);
			}
			catch(NullPointerException e)
			{
				TestConfigs.glb_TCFailedMessage+="Can't find Login fields:"+e.getMessage();
				Assert.fail(TestConfigs.glb_TCFailedMessage);
			}
			
			
		
		}
	
		public  void FillModalInfo(Contact_Info ctact_info, String button)
		{
			try {
				
			Messages_Notification = new Messages_Notification(driverbase);
			
			new ARCLoading(driverbase);
		
			
	//	    Wait_For_ElementDisplay(ModalFName_txt());

			Optimize_ElementSendkey(ModalFName_txt(), ctact_info.firstname);
			
			ClearThenEnterValueToField(ModalLName_txt(),ctact_info.lastname);
			
			
			String status = GetFieldText_func(ModalisDefault_chk());
			
			if(!status.equals(Boolean.toString(ctact_info.IsDefault)))
				Optimize_ElementClick(ModalisDefault_chk());

			String note_str = "";
			//this is handle for creating contact for Vendor
			if(!ctact_info.ServiceType.equals(""))
			{
				note_str = "Contact Details - Service Type Dropdown.";
				
				SelectDropdownItem(ModalServiceType_ddl(), ctact_info.ServiceType,note_str);
				
				
				Thread.sleep(2000);
			}
			
			//Fill_account_login
				if(UserName_txt()!=null)
				Fill_Account_Login_func(ctact_info.role,ctact_info.username);
	
			ClearThenEnterValueToField(ModalAddr1_txt(),ctact_info.AddrInfo.Addr1);
						
			ClearThenEnterValueToField(ModalCity_txt(),ctact_info.AddrInfo.City);
		
			ClearThenEnterValueToField(ModalZip_txt(),ctact_info.AddrInfo.Zip);
			if(ctact_info.AddrInfo.State!="")
			{
				note_str = "Contact Details - State Dropdown.";
				SelectDropdownItem(ModalState_ddl(),  ctact_info.AddrInfo.State,note_str);
			}
				

			if(ctact_info.AddrInfo.Country!="")
			{
				note_str = "Contact Details - Country Dropdown.";
				
				SelectDropdownItem(ModalCountry_ddl(), ctact_info.AddrInfo.Country,note_str);
			}
				

			note_str = "Contact Details - Phone Type Dropdown.";
			SelectDropdownItem(ModalPhoneType_ddl(), ctact_info.PhoneList.get(0).Type,note_str);

			
			ClearThenEnterValueToField(ModalPhoneNumb_txt(),ctact_info.PhoneList.get(0).Numb);
			
			note_str = "Contact Details - MailList Dropdown.";
			
			SelectDropdownItem(ModalEmailType_ddl(), ctact_info.email_info.Type,note_str);

			ClearThenEnterValueToField(ModalEmailAddr_txt(),ctact_info.email_info.EmailAddr);
			
			
			if(button.equals("Submit")||button.equals("submit"))
				Optimize_ElementClick(ModalSubmit_btn());
			else if(button.equals("Cancel"))
				Optimize_ElementClick(ModalCancel_btn());

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Submit Edit Contact Info",false);

			} //end else
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}//end void

		public  void VerifyModalViewMode_func(Contact_Info ctact_info)
		{
			String area = "";
			String field_name = "";
			String mode = "View Mode";


			ARCLoading.Wait_Util_FinishedPageLoading();

			//WaitForElementNotDisplay(LandingPage.LoadingArc())
			// CLICK ON FullName link
			FullName_lnk(ctact_info.FullName).click();

			ARCLoading.Wait_Util_FinishedARClogoLoading();

		//	Wait_For_ElementDisplay(ModalEdit_btn());

			
			area = "";
			
			if(!ctact_info.ClientName.equals(""))
			{

				field_name = "Modal_Client_Company_vlue";
				VerifyFieldValueEqual_func(area+field_name , Modal_Client_Company_vlue(),ctact_info.ClientName, mode);
			}

			
			else
			{
				//For Vendor will update for test case of vendor
			}
			
			
			field_name = "ModalFirstName";
			VerifyFieldValueEqual_func(area+field_name , ModalFName_txt(),ctact_info.firstname, mode);


			field_name = "ModalLastName";
			VerifyFieldValueEqual_func(area+field_name , ModalLName_txt(),ctact_info.lastname, mode);


			field_name = "ModalisDefault";
			VerifyFieldValueEqual_func(area+field_name , ModalisDefault_chk(),Boolean.toString(ctact_info.IsDefault), mode);

			field_name = "ModalAddrs1";
			VerifyFieldValueEqual_func(area+field_name , ModalAddr1_txt(),ctact_info.AddrInfo.Addr1, mode);

			field_name = "ModalCity";
			VerifyFieldValueEqual_func(area+field_name , ModalCity_txt(),ctact_info.AddrInfo.City , mode);


			field_name = "ModalZip";
			VerifyFieldValueEqual_func(area+field_name , ModalZip_txt(),ctact_info.AddrInfo.Zip , mode);


			field_name = "ModalState";
			VerifyFieldValueEqual_func(area+field_name , ModalState_ddl(),ctact_info.AddrInfo.State , mode);

			field_name = "ModalCountry";
			VerifyFieldValueEqual_func(area+field_name , ModalCountry_ddl(),ctact_info.AddrInfo.Country , mode);

		}
		
		public  void Update_AccountInfo_func(Contact_Info contact, String button)
		{
			try {
				
				Messages_Notification = new Messages_Notification(driverbase);

				ARCLoading = new ARCLoading(driverbase);
			
				ARCLoading.Wait_Util_FinishedARClogoLoading();
				
				Optimize_ElementClick(Edit_Profile_icon());
				
				Wait_For_ElementEnable(Contact_Profile_mdal());
				
				Thread.sleep(3000);
				
				Optimize_ElementClick(Edit_Contact_Profile_mdal_btn());
				//User Name
				ClearThenEnterValueToField(Username_Contact_Profile_txt(), contact.username);
				
				//Password
				ClearThenEnterValueToField(Password_Contact_Profile_txt(), contact.password);
				
				if(button.equals("submit")||button.equals("Submit"))
					{
						
						Optimize_ElementClick(Submit_Contact_Profile_btn());
						//Update_btn().click();
						
						Thread.sleep(5000);
						
	
						Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"Unsuccessfull OrgOff - Info update",false);
					}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
			
			Optimize_ElementClick(Close_Profile_icon());
			
			//Optimize_ElementClick(Close_Profile_icon_xpath);
			
		}//end void
		
		public  void Search_Contact(String fullname)
		{
			ScrollElementtoViewPort_func(Search_txt());
			
			ClearThenEnterValueToField(Search_txt(), fullname);
		}
		
		public  void Set_Contact_As_Default(String fullname)
		{
			Optimize_ElementClick(SetDefault_lnk(fullname));
		}
		
		public  void Verify_Contact_Is_Not_Set_As_Default(String fullname)
		{
			VerifyFieldDisplayed_func("SetDefault_link - Contact ["+fullname+"]", SetDefault_lnk(fullname), "");
		}
		
		public  void Verify_Contact_Is_Currently_Set_As_Default(String fullname)
		{
			VerifyFieldDisplayed_func("IsDefault_icon - Contact ["+fullname+"]", IsDefault_icon(fullname), "");
		}


}
