package pages;

import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Policy_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class Policy_page extends PageObjects {

	public Policy_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String actived_detail_mdal_xpath = "//div[contains(@id,'document') and contains(@style,'display: block')]";
	private String NewClient_btn_xpath = "//button[contains(text(),'New Client')]";
	private String NewPolicy_btn_xpath = "//button[contains(text(), 'New Policy')]";
	private String ClientList_tbl_xpath = "//table[@id = 'table-list-client']/tbody";
	private String PolicyList_tbl_xpath = "//div[@id = 'table-list-policy']//table/tbody | //table[@id='table-list-policy']/tbody";
	private String SearchSubject_txt_xpath = "//input[@placeholder= 'Policy name']";
	private String Search_btn_xpath = "//button[contains(text(), 'Search')]";
	private String MdalPolicy_txt_xpath = actived_detail_mdal_xpath
			+ "//div[contains(@org-placeholder,'Enter policy name')]/input | //input[@placeholder ='Enter policy name']";
	private String MdalClient_ddl_xpath = actived_detail_mdal_xpath + "//select[@placeholder='Client']";
	private String Edit_btn_xpath = "//a[contains(text(),'Edit')]";

	// DEFINE ELEMENTS
	public WebElement NewClient_btn() {
		return GetElement(NewClient_btn_xpath);
	}

	public WebElement NewPolicy_btn() {
		return GetElement(NewPolicy_btn_xpath);
	}

	public WebElement ClientList_tbl() {
		return GetElement(ClientList_tbl_xpath);
	}

	public WebElement PolicyList_tbl() {
		return GetElement(PolicyList_tbl_xpath);
	}

	public WebElement SearchSubject_txt() {
		return GetElement(SearchSubject_txt_xpath);
	}

	public WebElement Search_btn() {
		return GetElement(Search_btn_xpath);
	}

	public WebElement policyTitle_lnk(String titlePolicy) {
		try {

			String xpath_str = "//a//span[contains(text(),'" + titlePolicy + "')] | //a[contains(text(),'" + titlePolicy
					+ "')]";
			// WebElement temp_element = TestConfigs.driverbase.findElement(By.xpath());
			return GetElement(xpath_str);

		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public WebElement Modal_PolicyName_txt() {
		return GetElement(MdalPolicy_txt_xpath);
	}

	public WebElement Modal_ClientCompany_ddl() {
		return GetElement(MdalClient_ddl_xpath);
	}

	public WebElement Edit_btn() {
		return GetElement(Edit_btn_xpath);
	}

	// METHODS

	public void CreateRelocationPolicy_func(String mode,Policy_Info policy, String submit) {

		NewPolicy_btn().click();

		NewPolicy_mdal NewPolicy_mdal = new NewPolicy_mdal(driverbase);

		NewPolicy_mdal.FillInfoModal_func(mode, policy, submit);

		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

		if (Messages_Notification.notification_msg() != null) {
			String msg = Messages_Notification.notification_msg().getText();

			String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";

			if (msg.equals(expected_msg)) {
				TestConfigs.glb_TCFailedMessage += "Unsuccessfull Client Creation.Msg: '" + msg + "'.\n";
				Assert.fail(TestConfigs.glb_TCFailedMessage);
			}
		} // end if of message

	}

	public void VerifyPolicyExistTable_func(Policy_Info policy) {
		boolean exist = false;
		
		List<String> records = GetTableRecordsPerPaging_func(PolicyList_tbl());

		for (String index_str : records) {
			if (index_str.contains(policy.PolicyName)) {
				exist = true;
				break;
			} // end if
		} // end foreach

		if (!exist) {
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "Policy[" + policy.PolicyName + "] NOT exist in table.\n";
		} // end if
	}// endvoid

	public void SearchPolicy_func(String policy_str) {

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ClearThenEnterValueToField(SearchSubject_txt(), policy_str);

		Optimize_ElementClick(Search_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
	}

	public void VerifyModalPolicyInfoViewMode_func(Policy_Info p_info) {

		String area = "";
		String field_name = "";
		String mode = "View Mode";

		// Edit_icon(doc_info.file_name).click();

		try {

			Optimize_ElementClick(policyTitle_lnk(p_info.PolicyName));

		} catch (StaleElementReferenceException e) {

			Optimize_ElementClick(policyTitle_lnk(p_info.PolicyName));
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}

		field_name = "Modal_PolicyName_txt";
		VerifyFieldValueEqual_func(area + field_name, Modal_PolicyName_txt(), p_info.PolicyName, mode);

		if (!p_info.client.CompanyName.equals("") && Modal_ClientCompany_ddl() != null) {
			field_name = "Modal_ClientCompany_ddl";
			VerifyFieldValueEqual_func(area + field_name, Modal_ClientCompany_ddl(), p_info.client.CompanyName, mode);
		}
	}

	public void UpdateThePolicy(Policy_Info p_info, String submit) {

		Edit_btn().click();

		NewPolicy_mdal NewPolicy_mdal = new NewPolicy_mdal(driverbase);

		NewPolicy_mdal.FillInfoModal_func("Update",p_info, submit);

	}

}
