package pages;


import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;

public class Order_FacilityInfo_section extends PageObjects{
	

	
	public Order_FacilityInfo_section(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String FacilityInfo_Edit_btn_xpath = "//a[@id='icon-hhg-order-storage-facility-information']";
	private  String FacilityInfo_Update_btn_xpath = "//div[@id='hhg-order-storage-facility-information']//button[@data-control='update']";
	private  String FacilityInfo_Cancel_btn_xpath = "//div[@id='hhg-order-storage-facility-information']//button[@data-control='cancel-update']";

	private  String FacilityName_txt_xpath = "//div[@org-placeholder='Facility Name']//input";
	
	private  String ContactName_txt_xpath = "//div[@org-placeholder='Contact Name']//input";
	
	private  String ContactEmail_txt_xpath = "//div[@org-placeholder='Contact Email']//input";
	
	private  String FaciAddress_txt_xpath = "//div[@org-placeholder='Facility Address']//input";
	
	private  String City_txt_xpath = "//div[@org-placeholder='City']//input";
	
	private  String State_ddl_xpath = "//div[@org-placeholder='State']//select";
	
	private  String Zipcode_txt_xpath = "//div[@org-placeholder='Zipcode']//input";
	
	private  String Country_ddl_xpath = "//div[@org-placeholder='Country']//select";
	
	private  String Paidby_ddl_xpath = "//div[@org-placeholder='Paid by']//select";
	
	private  String Reimby_ddl_xpath = "//div[@org-placeholder='Reimbursed by']//select";
	
	private  String Terms_txt_xpath = "//div[@org-placeholder='Terms']/textarea";
	
	private  String UpdatedTerms_txt_xpath = "//pre[@placeholder = 'Terms']";
	
	private  String SpecExcept_txt_xpath = "//div[@org-placeholder='Special Exceptions']/textarea";
	
	private  String UpdatedExcept_txt_xpath = "//pre[@placeholder = 'Special Exceptions']";
	
	
	
	
	public  WebElement FacilityInfo_Edit_btn() {
	  	return GetElement(FacilityInfo_Edit_btn_xpath);
	 }
	
	public  WebElement FacilityInfo_Update_btn() {
	  	return GetElement(FacilityInfo_Update_btn_xpath);
	 }
	
	public  WebElement FacilityInfo_Cancel_btn() {
	  	return GetElement(FacilityInfo_Cancel_btn_xpath);
	 }
	
	public  WebElement FacilityName_txt() {
	  	return GetElement(FacilityName_txt_xpath);
	 }
	
	public  WebElement ContactName_txt() {
	  	return GetElement(ContactName_txt_xpath);
	 }
	
	public  WebElement ContactEmail_txt() {
	  	return GetElement(ContactEmail_txt_xpath);
	 }
	
	public  WebElement FaciAddress_txt() {
	  	return GetElement(FaciAddress_txt_xpath);
	 }
	
	public  WebElement City_txt() {
	  	return GetElement(City_txt_xpath);
	 }
	
	public  WebElement State_ddl() {
	  	return GetElement(State_ddl_xpath);
	 }
	
	public  WebElement Zipcode_txt() {
	  	return GetElement(Zipcode_txt_xpath);
	 }
	
	public  WebElement Country_ddl() {
	  	return GetElement(Country_ddl_xpath);
	 }
	
	public  WebElement Paidby_ddl() {
	  	return GetElement(Paidby_ddl_xpath);
	 }
	
	public  WebElement Reimby_ddl() {
	  	return GetElement(Reimby_ddl_xpath);
	 }
	
	public  WebElement Terms_txt() {
	  	return GetElement(Terms_txt_xpath);
	 }
	
	public  WebElement SpecExcept_txt() {
	  	return GetElement(SpecExcept_txt_xpath);
	 }
	
	public  WebElement UpdatedTerms_txt() {
	  	return GetElement(UpdatedTerms_txt_xpath);
	 }
	
	public  WebElement UpdatedSpecExcept_txt() {
	  	return GetElement(UpdatedExcept_txt_xpath);
	 }
	
	//==========================================METHODS
	
	public  void Update_FacilityInfo_Section_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		switch(order.ServiceType)
		{
			case "SIT" : case "EXT" : case "STG" :
				Optimize_ElementClick(FacilityInfo_Edit_btn());
				
				ClearThenEnterValueToField(FacilityName_txt(), order.Faci_FacilityName);
				
				ClearThenEnterValueToField(ContactName_txt(), order.Faci_ContactName);
				
				ClearThenEnterValueToField(ContactEmail_txt(), order.email_info.EmailAddr);
				
				ClearThenEnterValueToField(FaciAddress_txt(), order.AddrInfo.Addr1);
				
				ClearThenEnterValueToField(City_txt(), order.AddrInfo.City);
				
				ClearThenEnterValueToField(Zipcode_txt(), order.AddrInfo.Zip);
				
				SelectDropdownItem(Country_ddl(), order.AddrInfo.Country, "");
				
				SelectDropdownItem(State_ddl(), order.AddrInfo.State, "");
				
				SelectDropdownItem(Paidby_ddl(), order.Faci_Paidby, "");
				
				SelectDropdownItem(Reimby_ddl(), order.Faci_Reimby, "");
				
				ClearThenEnterValueToField(Terms_txt(), order.Faci_Terms);
				
				ClearThenEnterValueToField(SpecExcept_txt(), order.Faci_Special);
				
				if(button.equals("Submit")||button.equals("submit"))
				{
					Optimize_ElementClick(FacilityInfo_Update_btn());
					
					Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Shipment Info",false);
					
					new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
				}
			break;
		}
	}
	
	//==================================================METHOD VERIFY
	
	public  void Verify_FacilityInfo_section_func(Service_Order_Info order)
	{
		switch(order.ServiceType)
		{
			case "SIT" : case "EXT" : case "STG" :
				String field = "";
				
				field = "Facility Name";
				
				VerifyFieldValueEqual_func(field, FacilityName_txt(), order.Faci_FacilityName,"");
				
				field = "Contact Name";
				
				VerifyFieldValueEqual_func(field, ContactName_txt(), order.Faci_ContactName,"");
				
				field = "Contact Email";
				
				VerifyFieldValueEqual_func(field, ContactEmail_txt(), order.email_info.EmailAddr,"");
				
				field = "Facility Address";
				
				VerifyFieldValueEqual_func(field, FaciAddress_txt(), order.AddrInfo.Addr1,"");
				
				field = "City";
				
				VerifyFieldValueEqual_func(field, City_txt(), order.AddrInfo.City,"");
				
				field = "State";
				
				VerifyFieldValueEqual_func(field, Zipcode_txt(), order.AddrInfo.Zip,"");
				
				field = "Zipcode";
				
				VerifyFieldValueEqual_func(field, Country_ddl(), order.AddrInfo.Country,"");
				
				field = "Country";
				
				VerifyFieldValueEqual_func(field, State_ddl(), order.AddrInfo.State,"");
				
				field = "Paid by";
				
				VerifyFieldValueEqual_func(field, Paidby_ddl(), order.Faci_Paidby,"");
				
				field = "Reimbursed by";
				
				VerifyFieldValueEqual_func(field, Reimby_ddl(), order.Faci_Reimby,"");
				
				field = "Terms";
				
				VerifyFieldValueEqual_func(field, UpdatedTerms_txt(), order.Faci_Terms,"");
				
				field = "Special Exceptions";
				
				VerifyFieldValueEqual_func(field, UpdatedSpecExcept_txt(), order.Faci_Special,"");
				
			break;
		}
		
	}
}
