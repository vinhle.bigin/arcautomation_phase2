package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import configuration.DriverConfig.DriverBase;

public class Transf_AddrDesOff_sect extends PageObjects{
	
	
	
	public Transf_AddrDesOff_sect(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

		//DESTINATION OFF
		private  String DesOff_Edit_btn_xpath= "//a[@id = 'icon-edit-transferee-destination-office-address']";
		private  String DesOff_Update_btn_xpath= "//div[@id = 'edit-transferee-destination-office-address']//button[@data-control = 'update']";
		private  String DesOff_Cancel_btn_xpath= "//div[@id = 'edit-transferee-destination-office-address']//button[@data-control = 'cancel-update']";
		private  String DesOff_Addrs1_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter Address 1']//input";
		private  String DesOff_Addrs2_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter Address 2']//input";
		private  String DesOff_City_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter City']//input";
		private  String DesOff_State_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter State']//select";
		private  String DesOff_Zip_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter Zip']//input";
		private  String DesOff_Country_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter Country']//select";
		private  String DesOff_IsBilling_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][1]/div[1]//input";
		private  String DesOff_IsShipping_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][1]/div[2]//input";
		private  String DesOff_IsMailing_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][1]/div[3]//input";
		private  String DesOff_IsPrimary_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][1]/div[4]//input";
		private  String DesOff_IsSendMailPayment_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][2]/div[1]//input";
		private  String DesOff_IsSendMailTaxDoc_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][2]/div[2]//input";
		private  String DesOff_IsSendMailService_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][2]/div[3]//input";



	
	public  WebElement DesOff_Edit_btn() {
		  return GetElement(DesOff_Edit_btn_xpath);
		 }
	public  WebElement DesOff_Update_btn() {
		  return GetElement(DesOff_Update_btn_xpath);
		 }
	public  WebElement DesOff_Cancel_btn() {
		  return GetElement(DesOff_Cancel_btn_xpath);
		 }
	public  WebElement DesOff_Addrs1() {
		  return GetElement(DesOff_Addrs1_xpath);
		 }
	public  WebElement DesOff_Addrs2() {
		  return GetElement(DesOff_Addrs2_xpath);
		 }
	public  WebElement DesOff_City() {
		  return GetElement(DesOff_City_xpath);
		 }
	public  WebElement DesOff_State() {
		  return GetElement(DesOff_State_xpath);
		 }
	public  WebElement DesOff_Zip() {
		  return GetElement(DesOff_Zip_xpath);
		 }
	public  WebElement DesOff_Country() {
		  return GetElement(DesOff_Country_xpath);
		 }
	public  WebElement DesOff_IsBilling() {
		  return GetElement(DesOff_IsBilling_xpath);
		 }
	public  WebElement DesOff_IsShipping() {
		  return GetElement(DesOff_IsShipping_xpath);
		 }
	public  WebElement DesOff_IsMailing() {
		  return GetElement(DesOff_IsMailing_xpath);
		 }
	public  WebElement DesOff_IsPrimary() {
		  return GetElement(DesOff_IsPrimary_xpath);
		 }
	public  WebElement DesOff_IsSendMailPayment() {
		  return GetElement(DesOff_IsSendMailPayment_xpath);
		 }
	public  WebElement DesOff_IsSendMailTaxDoc() {
		  return GetElement(DesOff_IsSendMailTaxDoc_xpath);
		 }
	public  WebElement DesOff_IsSendMailService() {
		  return GetElement(DesOff_IsSendMailService_xpath);
		 }
	
	
	public  void Edit_TransfAddrInfo_func(Address_Info addr)
	{
		try
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
		String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";

	//	Wait_For_ElementDisplay(DesOff_Edit_btn());

		Optimize_ElementClick(DesOff_Edit_btn());

		ScrollElementtoViewPort_func(DesOff_Addrs1());

		ClearThenEnterValueToField(DesOff_Addrs1(), addr.Addr1);

		ClearThenEnterValueToField(DesOff_Addrs2(), addr.Addr2);

		ClearThenEnterValueToField(DesOff_City(), addr.City);

		ClearThenEnterValueToField(DesOff_Zip(), addr.Zip);
		
		String note_str = "Transferee Des Off Address Tab - State Dropdown.";

		SelectDropdownItem(DesOff_State(),  addr.State,note_str);
		
		note_str = "Transferee Des Off Address Tab - Country Dropdown.";

		SelectDropdownItem(DesOff_Country(), addr.Country,note_str);

		//addr.Country = "Viet Nam"
		//ClearThenEnterValueToField()

		DesOff_Update_btn().click();

		Thread.sleep(5000);

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(expected_msg,"Unsuccessfull DesOff - Info update",false);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//end void
	
	public  void VerifyInfoViewMode_func(Address_Info addr)
	{
		String area = "";
		String field_name = "";
		String mode = "View Mode";

	
		///===================================================DESTINATION OFF
		area = "Des Off - ";
		field_name = "DesOff_Addrs1";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_Addrs1(),addr.Addr1 , mode);

		field_name = "DesOff_Addrs2";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_Addrs2(),addr.Addr2 , mode);

		field_name = "DesOff_City";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_City(),addr.City , mode);

		field_name = "DesOff_Zip";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_Zip(),addr.Zip , mode);

		field_name = "DesOff_State";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_State(),addr.State , mode);

		field_name = "DesOff_Country";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_Country(),addr.Country , mode);

		field_name = "DesOff_IsBilling";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_IsBilling(),addr.IsAtri_Billing.toString() , mode);

		field_name = "DesOff_IsShipping";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_IsShipping(),addr.IsAtri_Shipping.toString() , mode);

		field_name = "DesOff_IsMailing";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_IsMailing(),addr.IsAtri_Mailing.toString() , mode);

		field_name = "DesOff_IsPrimary";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_IsPrimary(),addr.IsAtri_Primary.toString() , mode);

		field_name = "DesOff_IsSendMailPayment";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_IsSendMailPayment(),addr.IsSendMailPayment.toString() , mode);

		field_name = "DesOff_IsSendMailService";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_IsSendMailService(),addr.IsSendMailServices.toString() , mode);

		field_name = "DesOff_IsSendMailTaxDoc";
		VerifyFieldValueEqual_func(area+field_name ,DesOff_IsSendMailTaxDoc(),addr.IsSendMailTaxDoc.toString() , mode);


		
	}

}
