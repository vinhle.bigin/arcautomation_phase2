package pages;

import java.util.List;

import org.junit.Assert;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Activity_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class TaskList_page extends PageObjects{
	
	
	
	
	public TaskList_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Search_btn_xpath= "//button[@id='task-search-activity']";
	private  String Clear_btn_xpath= "//button[contains(text(),'Clear')]";
	private  String SearchFromDate_txt_xpath = "//input[@id='search_from_date']";
	private  String SearchToDate_txt_xpath= "//input[@id='search_to_date']";
	private  String SearchPriority_slect_xpath = "//select[contains(@placeholder,'Priorities')]";
	private  String SearchStatus_slect_xpath = "//select[contains(@placeholder,'Status')]";
	private  String SearchTitle_txt_xpath = "//div[@class='form-group col-md-2']//input[@placeholder='Title']";
	private  String SearchOverdueOnly_swit_xpath = "//div[@class='switch__touch']";
	private  String SearchType_slect_xpath = "//select[contains(@placeholder,'Type')]";
	private  String SearchOwner_slect_xpath = "//select[contains(@placeholder,'Owner')]";
	//private  String ActivityType_slect_xpath= "//div[@class = 'modal-body clearfix']//select[@placeholder = 'Activity Type']"
	//private  String ServiceType_slect_xpath= "//div[@class = 'modal-body clearfix']//select[@placeholder = 'Service type']"

	private  String ActivityList_tbl_xpath= "//div[@id='activities']//table//tbody";
	
	private  String Delete_icon_xpath = "//div[@class='table-responsive']//i[@class='icon-close']";
	
	private  String Table_Entry_Limit_slect = "//div[@id='table-activities']//select[contains(@id,'limit')]";

	//private  String ActivityName_lnk_xpath = ActivityList_tbl_xpath;
	

	
	public  WebElement Search_btn() {
	  return GetElement(Search_btn_xpath);
	 }
public  WebElement Clear_btn() {
	  return GetElement(Clear_btn_xpath);
	 }
public  WebElement SearchFromDate_txt() {
	  return GetElement(SearchFromDate_txt_xpath);
	 }
public  WebElement SearchToDate_txt() {
	  return GetElement(SearchToDate_txt_xpath);
	 }
public  WebElement SearchPriority_slect() {
	  return GetElement(SearchPriority_slect_xpath);
	 }
public  WebElement SearchStatus_slect() {
	  return GetElement(SearchStatus_slect_xpath);
	 }
public  WebElement SearchTitle_txt() {
	  return GetElement(SearchTitle_txt_xpath);
	 }
public  WebElement SearchType_slect() {
	return GetElement(SearchType_slect_xpath);
}
public  WebElement SearchOverdueOnly_swit() {
	  return GetElement(SearchOverdueOnly_swit_xpath);
	 }
public  WebElement SearchOwner_slect() {
	return GetElement(SearchOwner_slect_xpath);
}

public  WebElement Entry_Limit() {
	return GetElement(Table_Entry_Limit_slect);
}

public  WebElement ActivityList_tbl() {
	  return GetElement(ActivityList_tbl_xpath);
	 }

public  WebElement Delete_icon(String title) {
	
		if(!title.equals(""))
		{
			return GetElementInTable(title, ActivityList_tbl(), Delete_icon_xpath);
		}
		else
		{
			return GetElement(Delete_icon_xpath);	
		}
}
	 //end void


	public  WebElement ActivityName_lnk(String title) {
		try{
			
			String tag_a_xpath = ActivityList_tbl_xpath + "//a[contains(text(),'"+title+"')]";
			//String tag_span_xpath = ActivityList_tbl_xpath + "//span[text() ='"+title+"']";
			
			//String xpath_str = tag_a_xpath+" | "+tag_span_xpath;
			
			//println(xpath_str)
			WebElement temp_element = GetElement(tag_a_xpath);
						
			return temp_element;
		}
		catch(NoSuchElementException e) {
			
			return null;
		}
	}

	
	//=================================METHOD

		public  void Search_Activity_Date_func(String StartDate_str, String EndDate_str)
		{
			try
			{
				if(StartDate_str!="")
				{

					Optimize_ElementSendkey(SearchFromDate_txt(), StartDate_str);
					
				}
				if(EndDate_str!="")
				{
					Optimize_ElementSendkey(SearchToDate_txt(), EndDate_str);
				}

				Optimize_ElementClick(Search_btn());

				Thread.sleep(10000);

				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

		//		Wait_For_ElementDisplay(ActivityList_tbl());
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}
		
		public  void Search_Activity_Type_func(String activity_type) 
		{
			try
			{
				
				Optimize_ElementSendkey(SearchType_slect(), activity_type);
				
				Optimize_ElementClick(Search_btn());

				Thread.sleep(10000);

				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

		//		Wait_For_ElementDisplay(ActivityList_tbl());
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		public  void Search_Activity_Title_func(String activity_title)
		{
			try
			{
				
				Optimize_ElementSendkey(SearchTitle_txt(), activity_title);
				
				Optimize_ElementClick(Search_btn());

				Thread.sleep(10000);

				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

		//		Wait_For_ElementDisplay(ActivityList_tbl());
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public  void VerifyActivityExistTable_func(Activity_Info Act)
		{
			Boolean exist = false;;
			
			List<String> records = GetTableRecordsPerPaging_func(ActivityList_tbl());

			for(String index_str: records)
			{
				System.out.println(index_str+"\n");
				if(index_str.contains(Act.Title))
				{
					exist = true;
					break;
				}//end if
			}//end foreach

			if(!exist)
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Activity["+Act.Title+"] NOT exist in table.\n";
			}//end if
		}
		
		public  void VerifyActivityNotExistTable_func(Activity_Info Act)
		{
			Boolean exist = false;;
			Search_Activity_Title_func(Act.Title);
			List<String> records = GetTableRecordsPerPaging_func(ActivityList_tbl());

			for(String index_str: records)
			{
				if(index_str.contains(Act.Title))
				{
					exist = true;
					break;
				}//end if
			}//end foreach

			if(!exist)
			{
				TestConfigs.glb_TCStatus = true;
				TestConfigs.glb_TCFailedMessage += "Activity["+Act.Title+"] Still Exist in table.\n";
			}//end if
		}

		public  void OpenDetailsModal_func(Activity_Info Act)
		{
			Search_Activity_Date_func(Act.StartDate,"");
		//	Wait_For_ElementDisplay(ActivityList_tbl());
			//println(ActivityName_lnk_xpath + "span[text()='"+Act.Title+"']");
			ActivityName_lnk(Act.Title).click();


		}


		public  void VerifyModalDetailsViewMode_func(Activity_Info act_info)
		{
		//	ActivityName_lnk(act_info.Title).click();

			new ActivityDetails_mdal(driverbase).VerifyModalDetailsViewMode_func(act_info);
			
		}


		public  void CreateActivity_func(Activity_Info act)
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
			try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			switch(act.Type)
			{
			case "Follow Up":
				 new Shortcuts_menu(driverbase).NewFollow_btn().click();
				break;
				
			case "Task":
				new Shortcuts_menu(driverbase).NewTask_btn().click();
				break;
			}
			
			String note_str = "Task List - Heading Entity Dropdown.";
			
			SelectDropdownItem(new Shortcuts_menu(driverbase).Entity_ddl(), act.AssigneeType,note_str);
						
		//	Wait_For_ElementDisplay(new ActivityDetails_mdal(driverbase).Title_txt());
			
			new ActivityDetails_mdal(driverbase).FillModalDetails(act);
			
			new ActivityDetails_mdal(driverbase).Submit_btn().click();
			
			
			if(Messages_Notification.notification_msg()!=null)
			{
				String msg = Messages_Notification.notification_msg().getText();

				String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
				
				System.out.println(msg);
				
				if(msg ==expected_msg)
				{
					TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Activity Creation.Msg: '"+msg+"'.\n";
					
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
			}
			
			
			
				
		}//end void
			
		public  void UpdateActivity_func(Activity_Info New_info)
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			new ActivityDetails_mdal(driverbase).FillModalDetails(New_info);
			
			new ActivityDetails_mdal(driverbase).Submit_btn().click();
			
			String msg = Messages_Notification.notification_msg().getText();

			String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
			
			System.out.println(msg);
			
			if(msg ==expected_msg)
			{
				TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Activity Update.Msg: '"+msg+"'.\n";
				
				Assert.fail(TestConfigs.glb_TCFailedMessage);
			}
		}
		
		public  void RemoveActivity_func(String activity_title)
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			Delete_icon(activity_title);
			
			Messages_Notification.ConfirmYes_btn().click();
			
			String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
			
			String msg = Messages_Notification.notification_msg().getText();	
						
			if(msg ==expected_msg)
			{
				TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Activity Update.Msg: '"+msg+"'.\n";
				
				Assert.fail(TestConfigs.glb_TCFailedMessage);
			}
		}
	
		public  void SearchTaskList_func(String SearchType, String Search_vlue)
		{
			String note_str = "";
			try
			{
			
			switch(SearchType)
			{
				case "From Date":
					
					ClearThenEnterValueToField(SearchFromDate_txt(), Search_vlue);
					
					break;
					
				case "To Date":
					
					ClearThenEnterValueToField(SearchToDate_txt(), Search_vlue);
					
					break;
					
				case "Title":
					
					ClearThenEnterValueToField(SearchTitle_txt(), Search_vlue);
					
					break;	
					
				case "Type":
					note_str = "Task List  - Type Type Dropdown.";
					
					SelectDropdownItem(SearchType_slect(), Search_vlue ,note_str);
					
					break;
					
				case "Priorities":
					
					note_str = "Task List  - Priorities Type Dropdown.";
					
					SelectDropdownItem(SearchPriority_slect(), Search_vlue,note_str );
					
					break;
					
				case "Status":
					
					note_str = "Task List  - Status Type Dropdown.";
					
					SelectDropdownItem(SearchStatus_slect(), Search_vlue ,note_str);
					
					break;
					
				case "Owner":
					
					note_str = "Task List  - Owner Type Dropdown.";
					
					SelectDropdownItem(SearchOwner_slect(), Search_vlue ,note_str);
					
					break;
			}

			Search_btn().click();

			Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public  void EditTaskList_func(String old_title,Activity_Info new_activity_arg)
		{
			System.out.println(old_title);
			new Activities_tab(driverbase).ActivityName_lnk(old_title).click();

		
			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
	//		Wait_For_ElementDisplay(new ActivityDetails_mdal(driverbase).Edit_btn());

			new ActivityDetails_mdal(driverbase).Edit_btn().click();
			
			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
	//		Wait_For_ElementDisplay(new ActivityDetails_mdal(driverbase).Title_txt());

			new ActivityDetails_mdal(driverbase).FillModalDetails(new_activity_arg);
			
			
			new ActivityDetails_mdal(driverbase).Submit_btn().click();
				
			try {
			
				Thread.sleep(4000);
			
				Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
				
				String msg = Messages_Notification.notification_msg().getText();

				String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
				
				System.out.println(msg);
				
				if(msg ==expected_msg)
				{
					TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Activity Update.Msg: '"+msg+"'.\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}//end if msg		

				if(new ActivityDetails_mdal(driverbase).ModalClose_btn()!=null)
				{
					Optimize_ElementClick(new ActivityDetails_mdal(driverbase).ModalClose_btn());
					Thread.sleep(1000);
					
				}//end if

			} //end try
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}//end void
		
		public  void Search_Activity_Status_func(String activity_status)
		{
			try
			{
				
				Optimize_ElementSendkey(SearchStatus_slect(), activity_status);
				
				Optimize_ElementClick(Search_btn());

				Thread.sleep(3000);

				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

		//		Wait_For_ElementDisplay(ActivityList_tbl());
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public  void Search_Activity_AllConditions_func(String fromdate, String todate, String title, String type, String priority, String status)
		{
			if(!fromdate.equals(""))
			{
				//fill to the text field
				
				Optimize_ElementSendkey(SearchFromDate_txt(), fromdate);
			}
			
			if(!todate.equals(""))
			{
				//fill to the text field
				
				Optimize_ElementSendkey(SearchToDate_txt(), todate);
			}
			
			if(!title.equals(""))
			{
				//fill to the text field
				
				Optimize_ElementSendkey(SearchTitle_txt(), title);
			}
			
			
			String note_str = "Search Activity section - Type";
			if(type.equals(""))
			{
				SelectDropdownItem(SearchType_slect(), "All", note_str);
			}
			
			else
			{
				SelectDropdownItem(SearchType_slect(), type, note_str);
			}
			
			note_str = "Search Activity section - priority";
			if(priority.equals(""))
			{
				SelectDropdownItem(SearchPriority_slect(), "All", note_str);
			}
			
			else
			{
				SelectDropdownItem(SearchPriority_slect(), priority, note_str);
			}
			

			note_str = "Search Activity section - status";
			if(status.equals(""))
			{
				SelectDropdownItem(SearchStatus_slect(), "All", note_str);
			}
			
			else
			{
				SelectDropdownItem(SearchStatus_slect(), status, note_str);
			}
			
			
			try
			{		
				Optimize_ElementClick(Search_btn());

				Thread.sleep(3000);

				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

		//		Wait_For_ElementDisplay(ActivityList_tbl());
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
		}//end void

}
