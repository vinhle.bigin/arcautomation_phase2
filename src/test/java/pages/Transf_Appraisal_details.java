package pages;



import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import businessObjects.AppraisalService_Info;
import businessObjects.Contact_Info;
import configuration.DriverConfig.DriverBase;
import custom_Func.Data_Optimize;

public class Transf_Appraisal_details extends PageObjects{
	
	
	
	
	public Transf_Appraisal_details(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private  String actived_detail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";
	private  String PropertyInfo_Edit_btn_xpath = "//a[@id='icon-hbo-property-info-address-panel']";
	private  String PropertyInfo_Address1_txt_xpath = "//div[@id = 'loading-hbo-property-info-address-panel']//div[@org-placeholder = 'Enter Address 1']//input";
	private  String PropertyInfo_Address2_txt_xpath = "//div[@id = 'loading-hbo-property-info-address-panel']//div[@org-placeholder = 'Enter Address 2']//input";
	private  String PropertyInfo_EnterCity_txt_xpath = "//div[@id = 'loading-hbo-property-info-address-panel']//div[@org-placeholder = 'Enter City']//input";
	private  String PropertyInfo_State_txt_xpath = "//div[@id = 'loading-hbo-property-info-address-panel']//Select[@placeholder = 'State']";
	private  String PropertyInfo_Zip_txt_xpath = "//div[@id = 'loading-hbo-property-info-address-panel']//div[@org-placeholder = 'Enter Zip']//input";
	private  String PropertyInfo_Country_txt_xpath = "//div[@id = 'loading-hbo-property-info-address-panel']//Select[@placeholder = 'Country']";
	private  String PropertyInfo_Update_btn_xpath = "//div[@id = 'loading-hbo-property-info-address-panel']//button[@data-control = 'update']";
	private  String PropertyInfo_Cancel_btn_xpath = "//div[@id = 'loading-hbo-property-info-address-panel']//button[@data-control = 'cancel-update']";
	
	private  String Address_Tab_xpath = "//a[@href = '#transferee-address']";
	
	private  String Analysis_compare_tbl_xpath = "//div[@id='loading-appraisal-analysis-detail']//table";
	private  String Analysis_Edit_btn_xpath = "//a[@id='icon-appraisal-analysis-detail']";
	private  String Analysis_Price_txt_xpath = "//div[@org-placeholder = 'Price']//input";
	private  String Analysis_Price_chk_xpath = "//div[@class='b__components b-checkbox']//input[@type ='checkbox']";
	
	private  String Analysis_FromPercent_txt_xpath = "//div[@org-placeholder = 'Enter From Percent']//input";
	private  String Analysis_ToPercent_txt_xpath = "//div[@org-placeholder = 'Enter To Percent']//input";
	private  String Analysis_Calculate_btn_xpath = "//button[contains(text(),'Calculate')]";
	private  String Analysis_URL_txt_xpath = "//div[@org-placeholder = 'Enter Listing URL']//input";
	private  String Analysis_Update_btn_xpath = "//div[@id = 'loading-appraisal-analysis-detail']//button[@data-control = 'update']";
	private  String Analysis_Cancel_btn_xpath = "//div[@id = 'loading-appraisal-analysis-detail']//button[@data-control = 'cancel-update']";
	
	private  String Specialist_Edit_btn_xpath = "//a[@id='icon-home-inspection-specialist']";
	private  String Specialist_UserList_xpath = "//div[@org-placeholder = 'User List' ]//select";
	private  String Specialist_AssistantList_xpath = "//div[@org-placeholder = 'Assistant' ]//select";
	private  String Specialist_Update_btn_xpath = "//div[@id = 'loading-home-inspection-specialist']//button[@data-control = 'update']";
	private  String Specialist_Cancel_btn_xpath = "//div[@id = 'loading-home-inspection-specialist']//button[@data-control = 'cancel-update']";
	
	//MODAL
	private  String CtactMdal_OrderDate_txt_xpath = actived_detail_mdal_xpath+"//div[contains(@org-placeholder,'Order Date')]//input";
	private  String CtactMdal_WalkThru_txt_xpath = actived_detail_mdal_xpath+"//div[contains(@org-placeholder,'Appraisal Walk Thru')]//input";
	private  String CtactMdal_ApprReceive_txt_xpath = actived_detail_mdal_xpath+"//div[contains(@org-placeholder,'Appraisal Received')]//input";
	private  String CtactMdal_VerbalValue_txt_xpath = actived_detail_mdal_xpath+"//div[contains(@org-placeholder,'Verbal Value')]//input";
	private  String CtactMdal_Specialinstruct_txt_xpath = actived_detail_mdal_xpath+"//div[contains(@org-placeholder,'Special Instructions')]//textarea";
	private  String CtactMdal_Edit_btn_xpath = actived_detail_mdal_xpath+"//a[contains(text(),'Edit')]";
	private  String CtactMdal_Update_btn_xpath = actived_detail_mdal_xpath+"//button[@type = 'submit']";
	private  String CtactMdal_Cancel_btn_xpath = actived_detail_mdal_xpath+"//button[@data-control = 'cancel-create']";
	private  String CtactMdal_Close_btn_xpath = actived_detail_mdal_xpath+"//a[@data-dismiss='modal']";
	

	
	
	public  WebElement CtactMdal_Close_btn() {
		  return GetElement(CtactMdal_Close_btn_xpath);
		 }
	
	public  WebElement CtactMdal_OrderDate_txt() {
	  return GetElement(CtactMdal_OrderDate_txt_xpath);
	 }
	
	public  WebElement CtactMdal_WalkThru_txt() {
		  return GetElement(CtactMdal_WalkThru_txt_xpath);
		 }
	
	public  WebElement CtactMdal_ApprReceive_txt() {
		  return GetElement(CtactMdal_ApprReceive_txt_xpath);
		 }
	
	public  WebElement CtactMdal_VerbalValue_txt() {
		  return GetElement(CtactMdal_VerbalValue_txt_xpath);
		 }
	
	public  WebElement CtactMdal_Specialinstruct_txt() {
		  return GetElement(CtactMdal_Specialinstruct_txt_xpath);
		 }
	
	public  WebElement CtactMdal_Edit_btn() {
		  return GetElement(CtactMdal_Edit_btn_xpath);
		 }
	
	public  WebElement CtactMdal_Update_btn() {
		  return GetElement(CtactMdal_Update_btn_xpath);
		 }
	
	public  WebElement CtactMdal_Cancel_btn() {
		  return GetElement(CtactMdal_Cancel_btn_xpath);
		 }
	
	public  WebElement PropertyInfo_Edit_btn() {
		  return GetElement(PropertyInfo_Edit_btn_xpath);
		 }
	
	public  WebElement PropertyInfo_Address1_txt() {
		  return GetElement(PropertyInfo_Address1_txt_xpath);
		 }
	
	public  WebElement PropertyInfo_Address2_txt() {
		  return GetElement(PropertyInfo_Address2_txt_xpath);
		 }
	
	public  WebElement PropertyInfo_EnterCity_txt() {
		  return GetElement(PropertyInfo_EnterCity_txt_xpath);
		 }
	
	public  WebElement PropertyInfo_State_txt() {
		  return GetElement(PropertyInfo_State_txt_xpath);
		 }
	
	public  WebElement PropertyInfo_Zip_txt() {
		  return GetElement(PropertyInfo_Zip_txt_xpath);
		 }
	
	public  WebElement PropertyInfo_Country_txt() {
		  return GetElement(PropertyInfo_Country_txt_xpath);
		 }
	
	public  WebElement PropertyInfo_Update_btn() {
		  return GetElement(PropertyInfo_Update_btn_xpath);
		 }
	
	public  WebElement PropertyInfo_Cancel_btn() {
		  return GetElement(PropertyInfo_Cancel_btn_xpath);
		 }
	
	public  WebElement Address_Tab() {
		  return GetElement(Address_Tab_xpath);
		 }
	
	public  WebElement Analysis_Edit_btn() {
		  return GetElement(Analysis_Edit_btn_xpath);
		 }
	
	public  WebElement Analysis_Price_txt(String ctact_fullname) 
	{
		List<WebElement> ctact_name_e = GetElements(Analysis_compare_tbl_xpath+"/thead//th");
		
		WebElement e_temp = null;	
		
		int count = ctact_name_e.size();
		for(int i = 0;i<count;i++)
		{
			String extract_name = ctact_name_e.get(i).getText(); 
			System.out.println(extract_name);
			
			if(ctact_name_e.get(i).getText().contains(ctact_fullname))
			{
				
				e_temp = GetElement(Analysis_compare_tbl_xpath+"//td["+(i+1)+"]"+Analysis_Price_txt_xpath);
				
				break;
			}//end if
		
		}//end for
		
		return e_temp;
	}//end void
	
	public  WebElement SelectEst_opt(String ctact_fullname) 
	{
				
			List<WebElement> ctact_name_e = GetElements(Analysis_compare_tbl_xpath+"/thead//th");
			
			WebElement e_temp = null;	
			
			int count = ctact_name_e.size();
			
			for(int i = 0;i<count;i++)
			{
				String extract_name = ctact_name_e.get(i).getText(); 
			
				System.out.println(extract_name);
				
				if(ctact_name_e.get(i).getText().contains(ctact_fullname))
				{
					e_temp = GetElement(Analysis_compare_tbl_xpath+"//td["+(i+1)+"]"+Analysis_Price_chk_xpath);
					
					break;
				}//end if
			
			}//end for
			
			return e_temp;
		 }//end void
	
	public  WebElement Analysis_FromPercent_txt() {
		  return GetElement(Analysis_FromPercent_txt_xpath);
		 }
	
	public  WebElement Analysis_ToPercent_txt() {
		  return GetElement(Analysis_ToPercent_txt_xpath);
		 }
	
	public  WebElement Analysis_Calculate_btn() {
		  return GetElement(Analysis_Calculate_btn_xpath);
		 }
	
	public  WebElement Analysis_URL_txt() {
		  return GetElement(Analysis_URL_txt_xpath);
		 }
	
	public  WebElement Analysis_Update_btn() {
		  return GetElement(Analysis_Update_btn_xpath);
		 }
	
	public  WebElement Analysis_Cancel_btn() {
		  return GetElement(Analysis_Cancel_btn_xpath);
		 }
	
	public  WebElement Specialist_Edit_btn() {
		  return GetElement(Specialist_Edit_btn_xpath);
		 }
	
	public  WebElement Specialist_UserList() {
		  return GetElement(Specialist_UserList_xpath);
		 }
	
	public  WebElement Specialist_AssistantList() {
		  return GetElement(Specialist_AssistantList_xpath);
		 }
	
	public  WebElement Specialist_Update_btn() {
		  return GetElement(Specialist_Update_btn_xpath);
		 }
	
	public  WebElement Specialist_Cancel_btn() {
		  return GetElement(Specialist_Cancel_btn_xpath);
		 }
	
	////====== STEPS METHODS
	
	public  void UpdatePropertyInfo_func(Address_Info Addr_info,String button)
	{
		Optimize_ElementClick(PropertyInfo_Edit_btn());
	
		ClearThenEnterValueToField(PropertyInfo_Address1_txt(), Addr_info.Addr1);
		
		ClearThenEnterValueToField(PropertyInfo_Address2_txt(), Addr_info.Addr2);
		
		ClearThenEnterValueToField(PropertyInfo_EnterCity_txt(), Addr_info.City);
		
		ClearThenEnterValueToField(PropertyInfo_Zip_txt(), Addr_info.Zip);
		
		SelectDropdownItem(PropertyInfo_State_txt(), Addr_info.State, "State Dropdown");
		
		if(button.equals("Submit")||button.equals("submit"))
			Optimize_ElementClick(PropertyInfo_Update_btn());
	}//end void
	
	
	public  void FillAnalysisDetail_func(AppraisalService_Info service, String button)
	{
		Optimize_ElementClick(Analysis_Edit_btn());
		
		
		for(String name : service.SelectEst_List.keySet())
		{
			Optimize_ElementClick(SelectEst_opt(name));
			
			ClearThenEnterValueToField(Analysis_Price_txt(name), service.SelectEst_List.get(name).toString());
		}
		
		ClearThenEnterValueToField(Analysis_FromPercent_txt(), Double.toString(service.ListPercent_From));
				
		ClearThenEnterValueToField(Analysis_ToPercent_txt(), Double.toString(service.ListPercent_To));
					
		Optimize_ElementClick(Analysis_Calculate_btn());

		ClearThenEnterValueToField(Analysis_URL_txt(), service.URL);
		
		Optimize_ElementClick(Analysis_Update_btn());
	}//end void
	
	
	
	public  void UpdateSpecialist_func(AppraisalService_Info service)
	{
		Optimize_ElementClick(Specialist_Edit_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		
		//Select User List
		String note_str = "Update UserList - UserList Dropdown.";
		
		SelectDropdownItem(Specialist_UserList(), service.User,note_str);
		
		//Select Assistant
		note_str = "Update Assistant - Assistant Dropdown.";
		
		SelectDropdownItem(Specialist_AssistantList(), service.Assistant,note_str);
		
		//Click on Update
		Optimize_ElementClick(Specialist_Update_btn());
	}//end void
	
	
	public  void Update_Appraisal_CtactModal_Info_func(Contact_Info contact_info,String button)
	{
		
		WebElement ctact_edit_icon = GetElementInTable(contact_info.FullName, new Transf_Domestic_tab(driverbase).Vendor_Contact_tbl(), "//a[@data-original-title='Edit']");
		
		Optimize_ElementClick(ctact_edit_icon);
		
		Optimize_ElementClick(CtactMdal_Edit_btn());
		
		Optimize_ElementSendkey(CtactMdal_OrderDate_txt(), contact_info.OrderDate);
		
		CtactMdal_OrderDate_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(CtactMdal_WalkThru_txt(), contact_info.WalkThru);
		CtactMdal_WalkThru_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(CtactMdal_ApprReceive_txt(), contact_info.ApprsaiReceive);
		CtactMdal_ApprReceive_txt().sendKeys(Keys.ENTER);
		
		ClearThenEnterValueToField(CtactMdal_VerbalValue_txt(), contact_info.VerbalValue);
		
		ClearThenEnterValueToField(CtactMdal_Specialinstruct_txt(), contact_info.SpecialInstruct);
					
		if(button.equals("submit")||button.equals("Submit"))
		{
			CtactMdal_Update_btn().click();
			
			Wait_Ultil_ElementNotDisplay(CtactMdal_Update_btn_xpath);
			
			Optimize_ElementClick(CtactMdal_Close_btn());
			
			
		}//end void
		
		
	}//end void
	
	
	
	
	//=========================VERIFICATION METHODS
		
	public  void Verify_PropertyAddress_Detail_Is_Correct_func(String serviceName,Address_Info addr)
	{
		String field_name = "";

		field_name = serviceName+" - " + "PropertyInfo_Address1_txt";
		VerifyFieldValueEqual_func(field_name,PropertyInfo_Address1_txt(), addr.Addr1,"");

		field_name = serviceName+" - " + "PropertyInfo_Address2_txt";
		VerifyFieldValueEqual_func(field_name,PropertyInfo_Address2_txt(), addr.Addr2,"");

		field_name = serviceName+" - " + "PropertyInfo_EnterCity_txt";
		VerifyFieldValueEqual_func(field_name,PropertyInfo_EnterCity_txt(), addr.City,"");

		field_name = serviceName+" - " + "PropertyInfo_EnterZip_txt";
		VerifyFieldValueEqual_func(field_name,PropertyInfo_Zip_txt(), addr.Zip,"");

	}//end void
	
	
	public  void Verify_AppraisalSpecialist_func(AppraisalService_Info service)
	{
		String field_name = "";

		try {	
		field_name = "Specialist_UserList_xpath";
		VerifyFieldValueEqual_func(field_name,Specialist_UserList(), service.User,"");
		}
		catch (StaleElementReferenceException e )
		{
			VerifyFieldValueEqual_func(field_name,Specialist_UserList(), service.User,"");
		}
		field_name = "Specialist_AssistantList";
		VerifyFieldValueEqual_func(field_name,Specialist_AssistantList(), service.Assistant ,"");

	}//end void
	
	public  void Verify_Appraisal_CtactModal_Info_func(Contact_Info contact_info)
	{
		String field_name = "";

		WebElement ctact_edit_icon = GetElementInTable(contact_info.FullName, new Transf_Domestic_tab(driverbase).Vendor_Contact_tbl(), "//a[data-original-title='Edit']");
		
		Optimize_ElementClick(ctact_edit_icon);
				
		field_name = "CtactMdal_OrderDate_txt";
		VerifyFieldValueEqual_func(field_name,CtactMdal_OrderDate_txt(), contact_info.OrderDate,"");
		
		field_name = "CtactMdal_WalkThru_txt";
		VerifyFieldValueEqual_func(field_name,CtactMdal_WalkThru_txt(), contact_info.WalkThru,"");
	
		field_name = "CtactMdal_ApprReceive_txt";
		VerifyFieldValueEqual_func(field_name,CtactMdal_ApprReceive_txt(), contact_info.ApprsaiReceive,"");
		
		field_name = "CtactMdal_ApprReceive_txt";
		VerifyFieldValueEqual_func(field_name,CtactMdal_VerbalValue_txt(), contact_info.VerbalValue,"");
		
		field_name = "CtactMdal_Specialinstruct_txt";
		VerifyFieldValueEqual_func(field_name,CtactMdal_Specialinstruct_txt(), contact_info.SpecialInstruct,"");
	}
	
	
	public  void Verify_AppraisalAnalysis_section_ViewMode_func(AppraisalService_Info service)
	{
		service.Calculate_Appraisal_Analysis();
		
		String fieldname_str="";
		String note_str="";
		for(String name : service.SelectEst_List.keySet())
		{
			fieldname_str = "Analysis_Price_txt["+name+"]"; 
			
			VerifyFieldValueEqual_func(fieldname_str, Analysis_Price_txt(name), Data_Optimize.Format_Double_WithCurrency(service.SelectEst_List.get(name))  , note_str);
			
		}
		
		fieldname_str = "Analysis_FromPercent_txt"; 
		VerifyFieldValueEqual_func(fieldname_str, Analysis_FromPercent_txt(), Data_Optimize.Convert_Double_To_String_func(service.ListPercent_From, "%.0f")+"%", note_str);
	
		fieldname_str = "Analysis_ToPercent_txt"; 
		VerifyFieldValueEqual_func(fieldname_str, Analysis_ToPercent_txt(), Data_Optimize.Convert_Double_To_String_func(service.ListPercent_To, "%.0f")+"%" , note_str);		
	
					
		fieldname_str = "Analysis_URL_txt"; 
		VerifyFieldValueEqual_func(fieldname_str, Analysis_URL_txt(), service.URL , note_str);
		
	}//end void
	
}
