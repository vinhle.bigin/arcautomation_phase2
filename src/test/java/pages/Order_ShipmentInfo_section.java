package pages;


import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;
import custom_Func.Data_Optimize;

public class Order_ShipmentInfo_section extends PageObjects{
	
	
	
	public Order_ShipmentInfo_section(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String ShipInfo_Edit_btn_xpath = "//a[@id='icon-hhg-order-shipmentinformation-panel']";
	private  String ShipInfo_Update_btn_xpath = "//div[@id='hhg-order-shipmentinformation-panel']//button[@data-control='update']";
	private  String ShipInfo_Cancel_btn_xpath = "//div[@id='hhg-order-shipmentinformation-panel']//button[@data-control='cancel-update']";
	
	private  String NewContainer_btn_xpath = "//div[@id= 'loading-hhg-shipment-information']//button[@class='btn btn-custom btn-rounded']";
	private  String ShipmentInfo_tbl_xpath = "//div[@id = 'table_hhg_shipment_information']//table//tbody";
	private  String Descript_txt_xpath = "//div[@org-placeholder='Description']//input";
	private  String NewContainer_Submit_btn_xpath = "//button[@data-control='create']";
	private  String NewContainer_Cancel_btn_xpath = "//button[@data-control='cancel-create']";
	private  String Newcontainer_Edit_btn_xpath = "//i[@class='icon-note default-color']";
	private  String Model_Close_btn_xpath = "//div[@id= 'loading-modal-hhg-update-shipment-information']//i[@class = 'icon-close']";
		//Weight
	private  String Weight_Auth_txt_xpath = "//h4[contains(text(),'Weight')]/..//div[@org-placeholder='Authorized']//input";
	private  String Weight_Est_txt_xpath = "//h4[contains(text(),'Weight')]/..//div[@org-placeholder='Estimated']//input";
	private  String Weight_Act_txt_xpath = "//h4[contains(text(),'Weight')]/..//div[@org-placeholder='Actual']//input";
		//Container Size
	private  String ContainerSize_Auth_txt_xpath = "//h4[contains(text(),'Container Size')]/..//div[@org-placeholder='Authorized']//input";
	private  String ContainerSize_Est_txt_xpath = "//h4[contains(text(),'Container Size')]/..//div[@org-placeholder='Estimated']//input";
	private  String ContainerSize_Act_txt_xpath = "//h4[contains(text(),'Container Size')]/..//div[@org-placeholder='Actual']//input";
		//Cube
	private  String Cube_Auth_txt_xpath = "//h4[contains(text(),'Cube')]/..//div[@org-placeholder='Authorized']//input";
	private  String Cube_Est_txt_xpath = "//h4[contains(text(),'Cube')]/..//div[@org-placeholder='Estimated']//input";
	private  String Cube_Act_txt_xpath = "//h4[contains(text(),'Cube')]/..//div[@org-placeholder='Actual']//input";
		//Auto w/HHGs
	private  String AutoHHG_Auth_txt_xpath = "//h4[contains(text(),'Auto w/HHGs')]/..//div[@org-placeholder='Authorized']//input";
	private  String AutoHHG_Est_txt_xpath = "//h4[contains(text(),'Auto w/HHGs')]/..//div[@org-placeholder='Estimated']//input";
	private  String AutoHHG_Act_txt_xpath = "//h4[contains(text(),'Auto w/HHGs')]/..//div[@org-placeholder='Actual']//input";
		//Air Container Size
	private  String AirSize_Auth_ddl_xpath = "//h4[contains(text(),'Air Container Size')]/..//div[@org-placeholder='Authorized']//select";
	private  String AirSize_Est_ddl_xpath = "//h4[contains(text(),'Air Container Size')]/..//div[@org-placeholder='Estimated']//select";
	private  String AirSize_Act_ddl_xpath = "//h4[contains(text(),'Air Container Size')]/..//div[@org-placeholder='Actual']//select";
	
	//#Auto
	private  String Auto_Auth_txt_xpath = "//h4[contains(text(),'#Auto')]/..//div[@org-placeholder='Authorized']//input";
	private  String Auto_Est_txt_xpath = "//h4[contains(text(),'#Auto')]/..//div[@org-placeholder='Estimated']//input";
	private  String Auto_Act_txt_xpath = "//h4[contains(text(),'#Auto')]/..//div[@org-placeholder='Actual']//input";
	
	//Distance
		private  String Distance_Auth_txt_xpath = "//h4[contains(text(),'Distance')]/..//div[@org-placeholder='Authorized']//input";
		private  String Distance_Est_txt_xpath = "//h4[contains(text(),'Distance')]/..//div[@org-placeholder='Estimated']//input";
		private  String Distance_Act_txt_xpath = "//h4[contains(text(),'Distance')]/..//div[@org-placeholder='Actual']//input";
	
		
	
	public  WebElement NewContainer_btn() {
		 return GetElement(NewContainer_btn_xpath);
	}
	
	public  WebElement ShipmentInfo_tbl() {
		 return GetElement(ShipmentInfo_tbl_xpath);
	}
	
	public  WebElement Descript_txt() {
		 return GetElement(Descript_txt_xpath);
	}
	
	public  WebElement Model_Close_btn() {
		 return GetElement(Model_Close_btn_xpath);
	}
	
	
	public  WebElement NewContainer_Submit_btn() {
		 return GetElement(NewContainer_Submit_btn_xpath);
	}
	
	public  WebElement NewContainer_Cancel_btn() {
		 return GetElement(NewContainer_Cancel_btn_xpath);
	}
	
	public  WebElement Newcontainer_Edit_btn() {
		 return GetElement(Newcontainer_Edit_btn_xpath);
	}
	
	public  WebElement AirSize_Auth_ddl() {
		 return GetElement(AirSize_Auth_ddl_xpath);
	}
	
	public  WebElement AirSize_Est_ddl() {
		 return GetElement(AirSize_Est_ddl_xpath);
	}
	
	public  WebElement AirSize_Act_ddl() {
		 return GetElement(AirSize_Act_ddl_xpath);
	}
	
	public  WebElement ShipInfo_Edit_btn() {
		return GetElement(ShipInfo_Edit_btn_xpath);
	 }
	
	public  WebElement ShipInfo_Update_btn() {
		return GetElement(ShipInfo_Update_btn_xpath);
	 }
	
	public  WebElement ShipInfo_Cancel_btn() {
		return GetElement(ShipInfo_Cancel_btn_xpath);
	 }
	
	public  WebElement Weight_Auth_txt() {
		return GetElement(Weight_Auth_txt_xpath);
	 }
	
	public  WebElement Weight_Est_txt() {
		return GetElement(Weight_Est_txt_xpath);
	 }
	
	public  WebElement Weight_Act_txt() {
		return GetElement(Weight_Act_txt_xpath);
	 }
	
	public  WebElement Auto_Auth_txt() {
		return GetElement(Auto_Auth_txt_xpath);
	 }
	
	public  WebElement Auto_Est_txt() {
		return GetElement(Auto_Est_txt_xpath);
	 }
	
	public  WebElement Auto_Act_txt() {
		return GetElement(Auto_Act_txt_xpath);
	 }
	
	public  WebElement ContainerSize_Auth_txt() {
	  	return GetElement(ContainerSize_Auth_txt_xpath);
	 }
	
	public  WebElement ContainerSize_Est_txt() {
	  	return GetElement(ContainerSize_Est_txt_xpath);
	 }
	
	public  WebElement ContainerSize_Act_txt() {
	  	return GetElement(ContainerSize_Act_txt_xpath);
	 }
	
	public  WebElement Cube_Auth_txt() {
	  	return GetElement(Cube_Auth_txt_xpath);
	 }
	
	public  WebElement Cube_Est_txt() {
	  	return GetElement(Cube_Est_txt_xpath);
	 }
	
	public  WebElement Cube_Act_txt() {
	  	return GetElement(Cube_Act_txt_xpath);
	 }
	
	public  WebElement AutoHHG_Auth_txt() {
		
		return GetElement(AutoHHG_Auth_txt_xpath);
				
	}
	
	public  WebElement AutoHHG_Est_txt() {
		
		return  GetElement(AutoHHG_Est_txt_xpath);
		
		 }
	
	public  WebElement AutoHHG_Act_txt() {
		
		return GetElement(AutoHHG_Act_txt_xpath);
		
	 }
	
public  WebElement HHG_Distance_Auth_txt() {
		
		return GetElement(Distance_Auth_txt_xpath);
				
	}
	
	public  WebElement HHG_Distance_Est_txt() {
		
		return  GetElement(Distance_Est_txt_xpath);
		
		 }
	
	public  WebElement HHG_Distance_Act_txt() {
		
		return GetElement(Distance_Act_txt_xpath);
		
	 }
	
	//================Methods
	
	public  void Update_ShipmentInfo_func(Service_Order_Info order, String button)
	{
		switch (order.ServiceType)
		{
		case "HHG":
			Update_ShipInfo_section_HHG_func(order, "submit");
			break;
		 case "HHE":
				Update_ShipInfo_section_HHE_func(order, "submit");
				break;
		case "POV":
			Update_ShipInfo_section_POV_func(order, "submit");
			break; 
		case "UAB":
			Update_ShipInfo_section_UAB_func(order, "submit");
			break;
		}//end switch	
	}
	
	private  void Update_ShipInfo_section_HHG_func(Service_Order_Info order, String button)
	{			
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(ShipInfo_Edit_btn());
		
		ClearThenEnterValueToField(Weight_Auth_txt(), order.Ship_WeightAuth);
		
		ClearThenEnterValueToField(Weight_Est_txt(), order.Ship_WeightEst);
		
		ClearThenEnterValueToField(Weight_Act_txt(), Double.toString(order.Ship_WeightAct));
		
		
		
		ClearThenEnterValueToField(HHG_Distance_Auth_txt(), order.Ship_DistanceAuth);
			
		ClearThenEnterValueToField(HHG_Distance_Est_txt(), order.Ship_DistanceEst);
					
		ClearThenEnterValueToField(HHG_Distance_Act_txt(), order.Ship_DistanceAct);
		
			
		ClearThenEnterValueToField(ContainerSize_Auth_txt(), order.Ship_ContainerAuth);
			
		ClearThenEnterValueToField(ContainerSize_Est_txt(), order.Ship_ContainerEst);
			
		ClearThenEnterValueToField(ContainerSize_Act_txt(), order.Ship_ContainerAct);

		
		ClearThenEnterValueToField(Cube_Auth_txt(), order.Ship_CubeAuth);
		
		ClearThenEnterValueToField(Cube_Est_txt(), order.Ship_CubeEst);
		
		ClearThenEnterValueToField(Cube_Act_txt(), order.Ship_CubeAct);

		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(ShipInfo_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Shipment Info",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
		
	}
	
	
	private  void Update_ShipInfo_section_HHE_func(Service_Order_Info order, String button)
	{			
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(ShipInfo_Edit_btn());
		
		ClearThenEnterValueToField(Weight_Auth_txt(), order.Ship_WeightAuth);
		
		ClearThenEnterValueToField(Weight_Est_txt(), order.Ship_WeightEst);
		
		ClearThenEnterValueToField(Weight_Act_txt(), Double.toString(order.Ship_WeightAct));
		
				
		ClearThenEnterValueToField(AutoHHG_Auth_txt(), order.Ship_AutoHHGAuth);
		
		ClearThenEnterValueToField(AutoHHG_Est_txt(), order.Ship_AutoHHGEst);
				
		ClearThenEnterValueToField(AutoHHG_Act_txt(), order.Ship_AutoHHGAct);
	
					
		ClearThenEnterValueToField(ContainerSize_Auth_txt(), order.Ship_ContainerAuth);
			
		ClearThenEnterValueToField(ContainerSize_Est_txt(), order.Ship_ContainerEst);
			
		ClearThenEnterValueToField(ContainerSize_Act_txt(), order.Ship_ContainerAct);

		
		ClearThenEnterValueToField(Cube_Auth_txt(), order.Ship_CubeAuth);
		
		ClearThenEnterValueToField(Cube_Est_txt(), order.Ship_CubeEst);
		
		ClearThenEnterValueToField(Cube_Act_txt(), order.Ship_CubeAct);

		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(ShipInfo_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Shipment Info",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
		
	}
	
	
	
	
	private  void Update_ShipInfo_section_POV_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(ShipInfo_Edit_btn());
		
		ClearThenEnterValueToField(ContainerSize_Auth_txt(), order.Ship_ContainerAuth);
		
		ClearThenEnterValueToField(ContainerSize_Est_txt(), order.Ship_ContainerEst);
		
		ClearThenEnterValueToField(ContainerSize_Act_txt(), order.Ship_ContainerAct);
		
		ClearThenEnterValueToField(Auto_Auth_txt(), order.Ship_AutoAuth);
		
		ClearThenEnterValueToField(Auto_Est_txt(), order.Ship_AutoEst);
		
		ClearThenEnterValueToField(Auto_Act_txt(), order.Ship_AutoAct);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(ShipInfo_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Shipment Info",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
		
	}
	
	private  void Update_ShipInfo_section_UAB_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(NewContainer_btn());
		
		ClearThenEnterValueToField(Descript_txt(), order.Ship_Descript);
		
		ClearThenEnterValueToField(Weight_Auth_txt(), order.Ship_WeightAuth);
		
		ClearThenEnterValueToField(Weight_Est_txt(), order.Ship_WeightEst);
		
		ClearThenEnterValueToField(Weight_Act_txt(), Double.toString(order.Ship_WeightAct));
		
		ClearThenEnterValueToField(Cube_Auth_txt(), order.Ship_CubeAuth);
		
		ClearThenEnterValueToField(Cube_Est_txt(), order.Ship_CubeEst);
	
		ClearThenEnterValueToField(Cube_Act_txt(), order.Ship_CubeAct);
		
		SelectDropdownItem(AirSize_Auth_ddl(), order.Ship_AirSize_Auth, "");
		
		SelectDropdownItem(AirSize_Est_ddl(), order.Ship_AirSize_Est, "");
			
		SelectDropdownItem(AirSize_Act_ddl(), order.Ship_AirSize_Act, "");
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(NewContainer_Submit_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Shipment Info",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
		
	}
	
	//=======================================METHODS VERIFY
	
	public  void Verify_OrderDetail_ShipInfo_func(Service_Order_Info order)
	{
		if(order.ServiceType.contains("SIT") == false || order.ServiceType.contains("EXT") == false || order.ServiceType.contains("STG") == false)
		{
			switch (order.ServiceType)
			{
			case "HHG": 
				Verify_ShipInfo_section_HHG_func(order);
				break;
				
			case "HHE":
				Verify_ShipInfo_section_HHE_func(order);
				break;
				
			case "POV":
				Verify_ShipInfo_section_POV_func(order);
				break; 
				
			default:
				Verify_ShipInfo_section_UAB_func(order);
				break;
			}//end switch	
		}
	}	
	
	private  void Verify_ShipInfo_section_HHG_func(Service_Order_Info order)
	{
		String area = "";
		String field = "";
		
		area = "Shipment Info - ";
		
		field = "Weight Authorized";
		
		if(order.Ship_WeightAuth.equals("0.00"))
		{
			order.Ship_WeightAuth="";
		}
		
		VerifyFieldValueEqual_func(area+ field,Weight_Auth_txt(), order.Ship_WeightAuth,"");
		
		field = "Weight Estimated";
		
		VerifyFieldValueEqual_func(area+ field,Weight_Est_txt(), order.Ship_WeightEst,"");
		
		field = "Weight Actual";
		
		String Ship_WeightAct_str = Data_Optimize.Convert_Double_To_String_func(order.Ship_WeightAct,"");
		
		VerifyFieldValueEqual_func(area+ field,Weight_Act_txt(), Ship_WeightAct_str,"");

		field = "Distance - Authorized";
				
		VerifyFieldValueEqual_func(area+ field,HHG_Distance_Auth_txt(), order.Ship_DistanceAuth,"");
				
		field = "Distance - Estimated";
				
		VerifyFieldValueEqual_func(area+ field,HHG_Distance_Est_txt(), order.Ship_DistanceEst,"");
				
		field = "Distance - Actual";	
					
		VerifyFieldValueEqual_func(area+ field,HHG_Distance_Act_txt(), order.Ship_DistanceAct,"");
	
		field = "Cube Authorized";
			
		VerifyFieldValueEqual_func(area+ field,Cube_Auth_txt(), order.Ship_CubeAuth,"");
			
		field = "Cube Estimated";
			
		VerifyFieldValueEqual_func(area+ field,Cube_Est_txt(), order.Ship_CubeEst,"");
			
		field = "Cube Actual";	
				
		VerifyFieldValueEqual_func(area+ field,Cube_Act_txt(), order.Ship_CubeAct,"");

	}
	
	
	private  void Verify_ShipInfo_section_HHE_func(Service_Order_Info order)
	{
		String area = "";
		String field = "";
		
		area = "Shipment Info - ";
		
		field = "Weight Authorized";
		
		VerifyFieldValueEqual_func(area+ field,Weight_Auth_txt(), order.Ship_WeightAuth,"");
		
		field = "Weight Estimated";
		
		VerifyFieldValueEqual_func(area+ field,Weight_Est_txt(), order.Ship_WeightEst,"");
		
		field = "Weight Actual";
		String Ship_WeightAct_str = Data_Optimize.Convert_Double_To_String_func(order.Ship_WeightAct,"");
				
		VerifyFieldValueEqual_func(area+ field,Weight_Act_txt(), Ship_WeightAct_str,"");

		field = "Auto w/HHGs - Authorized";
				
		VerifyFieldValueEqual_func(area+ field,AutoHHG_Auth_txt(), order.Ship_AutoHHGAuth,"");
				
		field = "Auto w/HHGs - Estimated";
				
		VerifyFieldValueEqual_func(area+ field,AutoHHG_Est_txt(), order.Ship_AutoHHGEst,"");
				
		field = "Auto w/HHGs - Actual";	
					
		VerifyFieldValueEqual_func(area+ field,AutoHHG_Act_txt(), order.Ship_AutoHHGAct,"");
	
		field = "Cube Authorized";
			
		VerifyFieldValueEqual_func(area+ field,Cube_Auth_txt(), order.Ship_CubeAuth,"");
			
		field = "Cube Estimated";
			
		VerifyFieldValueEqual_func(area+ field,Cube_Est_txt(), order.Ship_CubeEst,"");
			
		field = "Cube Actual";	
				
		VerifyFieldValueEqual_func(area+ field,Cube_Act_txt(), order.Ship_CubeAct,"");

	}
	
	
	private  void Verify_ShipInfo_section_POV_func(Service_Order_Info order)
	{

		String area = "";
		String field = "";
		
		area = "Ship Info - Container Size";
		
		field = "Authorized";
		
		VerifyFieldValueEqual_func(area+ field,ContainerSize_Auth_txt(), order.Ship_ContainerAuth,"");
		
		field = "Estimated";
		
		VerifyFieldValueEqual_func(area+ field,ContainerSize_Est_txt(), order.Ship_ContainerEst,"");
		
		field = "Actual";	
			
		VerifyFieldValueEqual_func(area+ field,ContainerSize_Act_txt(), order.Ship_ContainerAct,"");
		
		area = "Ship Info - #AutoSize";
		
		field = "Authorized";
		
		VerifyFieldValueEqual_func(area+ field,Auto_Auth_txt(), order.Ship_AutoAuth,"");
		
		field = "Estimated";
		
		VerifyFieldValueEqual_func(area+ field,Auto_Est_txt(), order.Ship_AutoEst,"");
		
		field = "Actual";
		
		VerifyFieldValueEqual_func(area+ field,Auto_Act_txt(), order.Ship_AutoAct,"");
	}
	
	private  void Verify_ShipInfo_section_UAB_func(Service_Order_Info order)
	{
		String area = "";
		String field = "";
		
		Optimize_ElementClick(Newcontainer_Edit_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		
		area = "Ship Info - Description - ";
		
			
		VerifyFieldValueEqual_func(area, Descript_txt(), order.Ship_Descript,"");
		
		area = "Ship Info - Weight - ";
		
		field = "Authorized";
		
		VerifyFieldValueEqual_func(area+ field,Weight_Auth_txt(), order.Ship_WeightAuth,"");
		
		field = "Estimated";
		
		VerifyFieldValueEqual_func(area+ field,Weight_Est_txt(), order.Ship_WeightEst,"");
		
		field = "Actual";
		
		String Ship_WeightAct_str = Data_Optimize.Convert_Double_To_String_func(order.Ship_WeightAct,"");
		
		VerifyFieldValueEqual_func(area+ field,Weight_Act_txt(), Ship_WeightAct_str,"");
		
		area = "Ship Info - Cube - ";
		
		field = "Authorized";
		
		VerifyFieldValueEqual_func(area+ field,Cube_Auth_txt(), order.Ship_CubeAuth,"");
		
		field = "Estimated";
		
		VerifyFieldValueEqual_func(area+ field,Cube_Est_txt(), order.Ship_CubeEst,"");
		
		field = "Actual";	
			
		VerifyFieldValueEqual_func(area+ field,Cube_Act_txt(), order.Ship_CubeAct,"");
		
		area = "Ship Info - Air Container Size - ";
			
		field = "Estimated";
			
		VerifyFieldValueEqual_func(area+ field, AirSize_Est_ddl(), order.Ship_AirSize_Est,"");
			
		field = "Authorized";
			
		VerifyFieldValueEqual_func(area+ field, AirSize_Auth_ddl(), order.Ship_AirSize_Auth,"");
			
		field = "Actual";
			
		VerifyFieldValueEqual_func(area+ field, AirSize_Act_ddl(), order.Ship_AirSize_Act,"");
		
		Optimize_ElementClick(Model_Close_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
	}
	
}
