package pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class Transferees_page extends PageObjects {

	public Transferees_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String NewTransf_btn_xpath = "//button[contains(text(),'New Transferee')]";
	private String TransfList_tbl_xpath = "//div[@id = 'table-list-transferee']//table/tbody";
	private String MyTranfOnly_chk_xpath = "//div[@class = 'b__components b-checkbox m-b-md']/span";
	private String SearchTranf_FirstName_txt_xpath = "//input[@name ='first_name']";
	private String Search_btn_xpath = "//button[contains(text(),'Search')]";

	// DEFINE ELEMENTS
	public WebElement NewTransf_btn() {

		return GetElement(NewTransf_btn_xpath);

	}

	public WebElement TransfList_tbl() {

		return GetElement(TransfList_tbl_xpath);

	}

	public WebElement MyTranfOnly_chk() {
		return GetElement(MyTranfOnly_chk_xpath);
	}

	public WebElement SearchTranf_FirstName_txt() {
		return GetElement(SearchTranf_FirstName_txt_xpath);
	}

	public WebElement Search_btn() {
		return GetElement(Search_btn_xpath);
	}

	public WebElement TransfName_link(String tranffullname_str) {

		String TransfName_lnk_xapth = "//a/span[contains(text(),'" + tranffullname_str + "')]";
		return GetElement(TransfName_lnk_xapth);

	}

	public void SearchTransferee_with_Name_func(String FirstName) {
		try {
			// Wait_For_ElementDisplay(SearchTranf_FirstName_txt());

			Thread.sleep(10000);

			ClearThenEnterValueToField(SearchTranf_FirstName_txt(), FirstName);

			Optimize_ElementClick(Search_btn());

			// new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

			Thread.sleep(1000);
			//
			// Wait_For_ElementDisplay(Transferees_page.TransfList_tbl());

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void VerifyTranfExistTable_func(Transferee_Info tf) {
		Boolean exist = false;
		List<String> records = GetTableRecordsPerPaging_func(TransfList_tbl());

		for (String index_str : records) {
			if (index_str.contains(tf.firstname)) {
				exist = true;
				break;
			} // end if
		} // end foreach

		if (!exist) {
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "Transferee[" + tf.FullName + "] NOT exist in table.\n";
		} // end if

	}

	public void CreateTransf_func(Transferee_Info tf, Boolean submit) {
		Optimize_ElementClick(NewTransf_btn());

		new NewTransf_mdal(driverbase).FillInfoModal_func(tf, submit);

		// Wait_For_ElementDisplay(Transf_GenInfo_tab.Generall_tab());

	}

	public void CreateTransf_via_Shortcut_func(Transferee_Info tf, Boolean submit) {
		// Wait_For_ElementDisplay(Shortcuts_menu.NewTrans_btn());

		Optimize_ElementClick(new Shortcuts_menu(driverbase).NewTrans_btn());

		new NewTransf_mdal(driverbase).FillInfoModal_func(tf, submit);
//		Wait_For_ElementDisplay(Transf_GenInfo_tab.Generall_tab());

	}

	public void GotoTransDetailsPage(String fullname_str) {

		try {

			if (new Transf_GenInfo_tab(driverbase).Generall_tab() == null)

				Optimize_ElementClick(TransfName_link(fullname_str));

			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

			GoToLatestBrowserTab();

		} catch (Exception e) {
			TestConfigs.glb_TCFailedMessage += "CANNOT NAVIGATE TO TRANFEREE DETAILS PAGE:" + e.getMessage() + ".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

}
