package pages;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.HSService_Info;
import configuration.DriverConfig.DriverBase;



public class Transf_HomeSale_details extends PageObjects{

	public Transf_HomeSale_details(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private  String HS_ListPrice_txt_xpath= "//div[@org-placeholder = 'List Price']//input";
	private  String HS_SalePrice_txt_xpath= "//div[@org-placeholder = 'Sale Price']//input[@placeholder ='Sale Price']";
	private  String HS_CmsionPercnt_txt_xpath= "//div[@org-placeholder = 'Commission Percent']//input[@placeholder ='Commission Percent']";
	private  String HS_ListStartDate_txt_xpath= "//div[@org-placeholder = 'Listing Start Date' or @data-field='listing_start_date']//input";
	private  String HS_ListEndDate_txt_xpath= "//div[@org-placeholder = 'Listing End Date'or @data-field='listing_end_date']//input";
	private  String HS_CloseDate_txt_xpath= "//div[@org-placeholder = 'Closing Date']//input";
	private  String HS_RebateAmount_txt_xpath= "//input[@placeholder='Rebate Amount']";
	private  String HS_BuyOutDate_txt_xpath= "//div[@org-placeholder='Buy-Out Accepted Date']//input";
	private  String HS_EscrowPercent_txt_xpath= "//div[@org-placeholder='Escrow Percentage']//input";
	private  String HS_EscrowFeeAmount_txt_xpath= "//div[@org-placeholder='Escrow Fee Amount']//input";
	
	
	private  String HS_IsRebateSent_chk_xpath= "//label[contains(text(),'Rebate Sent')]/..//input";
	private  String HS_IsTookBOut_chk_xpath= "//label[contains(text(),'Took Buy-Out')]/..//input";
	private  String HS_IsInvoiceCreate_chk_xpath= "//label[contains(text(),'Invoice Created')]/..//input";
	private  String HS_IsInvoiceClose_chk_xpath= "//label[contains(text(),'Invoice Closed Out')]/..//input";
	private  String HS_IsEscrReceive_chk_xpath= "//label[contains(text(),'Escrow Fee Received')]/..//input";
	private  String HS_IsSignRefAgree_chk_xpath= "//label[contains(text(),'Signed Referral Fee Agreement')]/..//input";
	
	//Price
	private  String HS_OriListPrice_txt_xpath = "//div[@org-placeholder = 'Original Listing Price']//input";
	private  String HS_CurrentPrice_txt_xpath = "//div[@org-placeholder = 'Current Price']//input";
	private  String HS_1st_PriceReduce_txt_xpath = "//div[@org-placeholder = '1ST Price Reduction']//input";
	private  String HS_2st_PriceReduce_txt_xpath = "//div[@org-placeholder = '2ND Price Reduction']//input";
	private  String HS_3st_PriceReduce_txt_xpath = "//div[@org-placeholder = '3RD Price Reduction']//input";
	private  String HS_4st_PriceReduce_txt_xpath = "//div[@org-placeholder = '4TH Price Reduction']//input";
	private  String HS_5st_PriceReduce_txt_xpath = "//div[@org-placeholder = '5TH Price Reduction']//input";
	private  String HS_6st_PriceReduce_txt_xpath = "//div[@org-placeholder = '6TH Price Reduction']//input";
	
	//Fees and Invoice
	private  String HS_IsRefFeeReceive_opt_xpath = "//label[contains(text(),'Referral Fee Received')]/..//input";
	private  String HS_IsNotAutoCal_opt_xpath = "//label[contains(text(),'Not Auto-Calculate')]/..//input";
	private  String HS_RefPercent_txt_xpath = "//div[@org-placeholder = 'Referral Percentage']//input";
	private  String HS_RefFeeAmount_txt_xpath = "//label[contains(text(),'Referral Fee Amount')]/..//input";

	private  String Update_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='update']";
	private  String Cancel_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='cancel-update']";
	private  String AddedVendor_tbl_xpath= "//div[@id='transferee-domestic-table']//table/tbody";

	
	private  String Edit_btn_xpath= "//a[@data-original-title ='Edit']";
	
	public  WebElement HS_OriListPrice_txt() {
	  return GetElement(HS_OriListPrice_txt_xpath);
	 }
public  WebElement HS_CurrentPrice_txt() {
	  return GetElement(HS_CurrentPrice_txt_xpath);
	 }
public  WebElement HS_1st_PriceReduce_txt() {
	  return GetElement(HS_1st_PriceReduce_txt_xpath);
	 }
public  WebElement HS_2st_PriceReduce_txt() {
	  return GetElement(HS_2st_PriceReduce_txt_xpath);
	 }
public  WebElement HS_3st_PriceReduce_txt() {
	  return GetElement(HS_3st_PriceReduce_txt_xpath);
	 }
public  WebElement HS_4st_PriceReduce_txt() {
	  return GetElement(HS_4st_PriceReduce_txt_xpath);
	 }
public  WebElement HS_5st_PriceReduce_txt() {
	  return GetElement(HS_5st_PriceReduce_txt_xpath);
	 }
public  WebElement HS_6st_PriceReduce_txt() {
	  return GetElement(HS_6st_PriceReduce_txt_xpath);
	 }
public  WebElement HS_IsRefFeeReceive_opt() {
	  return GetElement(HS_IsRefFeeReceive_opt_xpath);
	 }
public  WebElement HS_IsNotAutoCal_opt() {
	  return GetElement(HS_IsNotAutoCal_opt_xpath);
	 }
public  WebElement HS_RefPercent_txt() {
	  return GetElement(HS_RefPercent_txt_xpath);
	 }
public  WebElement HS_RefFeeAmount_txt() {
	  return GetElement(HS_RefFeeAmount_txt_xpath);
	 }

	
	
	public  WebElement HS_ListPrice_txt() {
	  return GetElement(HS_ListPrice_txt_xpath);
	 }
	public  WebElement HS_SalePrice_txt() {
	  return GetElement(HS_SalePrice_txt_xpath);
	 }
	public  WebElement HS_CmsionPercnt_txt() {
	  return GetElement(HS_CmsionPercnt_txt_xpath);
	 }
	public  WebElement HS_ListStartDate_txt() {
	  return GetElement(HS_ListStartDate_txt_xpath);
	 }
	public  WebElement HS_ListEndDate_txt() {
	  return GetElement(HS_ListEndDate_txt_xpath);
	 }
	public  WebElement HS_CloseDate_txt() {
	  return GetElement(HS_CloseDate_txt_xpath);
	 }
	public  WebElement HS_RebateAmount_txt() {
	  return GetElement(HS_RebateAmount_txt_xpath);
	 }
	public  WebElement HS_BuyOutDate_txt() {
	  return GetElement(HS_BuyOutDate_txt_xpath);
	 }
	public  WebElement HS_EscrowPercent_txt() {
	  return GetElement(HS_EscrowPercent_txt_xpath);
	 }
	public  WebElement HS_EscrowFeeAmount_txt() {
	  return GetElement(HS_EscrowFeeAmount_txt_xpath);
	 }
	public  WebElement HS_IsRebateSent_chk() {
	  return GetElement(HS_IsRebateSent_chk_xpath);
	 }
	public  WebElement HS_IsTookBOut_chk() {
	  return GetElement(HS_IsTookBOut_chk_xpath);
	 }
	public  WebElement HS_IsInvoiceCreate_chk() {
	  return GetElement(HS_IsInvoiceCreate_chk_xpath);
	 }
	public  WebElement HS_IsInvoiceClose_chk() {
	  return GetElement(HS_IsInvoiceClose_chk_xpath);
	 }
	public  WebElement HS_IsEscrReceive_chk() {
	  return GetElement(HS_IsEscrReceive_chk_xpath);
	 }
	public  WebElement HS_IsSignRefAgree_chk() {
	  return GetElement(HS_IsSignRefAgree_chk_xpath);
	 }
	public  WebElement Update_btn() {
	  return GetElement(Update_btn_xpath);
	 }
	public  WebElement Cancel_btn() {
	  return GetElement(Cancel_btn_xpath);
	 }
	public  WebElement AddedVendor_tbl() {
	  return GetElement(AddedVendor_tbl_xpath);
	 }
	
	
	
	
	
	
	public  WebElement Edit_btn() {
		  return GetElement(Edit_btn_xpath);
		 }

	
	
	public  void FillHSInfo_func(HSService_Info service,String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
		//Optimize_ElementClick(Edit_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();


	//	Wait_For_ElementDisplay(HS_BuyOutDate_txt());


		ClearThenEnterValueToField(HS_ListPrice_txt(), service.ListPrice);

		ClearThenEnterValueToField(HS_SalePrice_txt(), service.SalePrice);

		ClearThenEnterValueToField(HS_CmsionPercnt_txt(), service.CommissionPercent);

		Optimize_ElementSendkey(HS_ListStartDate_txt(), service.ListStartDate);

		Optimize_ElementSendkey(HS_ListEndDate_txt(), service.ListEndDate);

		Optimize_ElementSendkey(HS_CloseDate_txt(), service.CloseDate);

		//ClearThenEnterValueToField(HS_RebateAmount_txt(), service.RebateAmount);

		Optimize_ElementSendkey(HS_BuyOutDate_txt(), service.BuyOutAcceptDate);

	
		Optimize_ElementClick(HS_IsRebateSent_chk());

		Optimize_ElementClick(HS_IsTookBOut_chk());
		
		
		//Please make sure the Address State must be Whashinton DC
		if(!service.RebateAmount.equals("$0.00"))
		{
			ClearThenEnterValueToField(HS_RebateAmount_txt(),service.RebateAmount);
		}		
		
		
		ClearThenEnterValueToField(HS_OriListPrice_txt(),service.OriListPrice);
		
		ClearThenEnterValueToField(HS_CurrentPrice_txt(),service.CurrentPrice);
		
		ClearThenEnterValueToField(HS_1st_PriceReduce_txt(),service.HS_1st_PriceReduce);
		
		ClearThenEnterValueToField(HS_2st_PriceReduce_txt(),service.HS_2st_PriceReduce);
		
		ClearThenEnterValueToField(HS_3st_PriceReduce_txt(),service.HS_3st_PriceReduce);
		
		ClearThenEnterValueToField(HS_4st_PriceReduce_txt(),service.HS_4st_PriceReduce);
		
		ClearThenEnterValueToField(HS_5st_PriceReduce_txt(),service.HS_5st_PriceReduce);
		
		ClearThenEnterValueToField(HS_6st_PriceReduce_txt(),service.HS_6st_PriceReduce);
		
		ClearThenEnterValueToField(HS_RefPercent_txt(),service.RefPercent);
		
		String status = GetFieldText_func(HS_IsRefFeeReceive_opt());
		
		if(status!=Boolean.toString(service.IsRefFeeReceive))
		{
			Optimize_ElementClick(HS_IsRefFeeReceive_opt());
		}
	
		
		status = GetFieldText_func(HS_IsNotAutoCal_opt());
		if(!status.equals(Boolean.toString(service.IsNotAutoCal)))
		{
			Optimize_ElementClick(HS_IsNotAutoCal_opt());
						
			if(service.IsNotAutoCal==true)
			{
				ClearThenEnterValueToField(HS_RefFeeAmount_txt(),service.RefFeeAmount_vlue);
			}
			
			
		}
		
	//	Optimize_ElementClick(HS_IsInvoiceCreate_chk());

	//	Optimize_ElementClick(HS_IsInvoiceClose_chk());

//		Optimize_ElementClick(HS_IsEscrReceive_chk());

		Optimize_ElementClick(HS_IsSignRefAgree_chk());

		if(button.equals("Submit")||button.equals("submit"))
			Update_btn().click();
		else if(button.equals("Cancel"))
			Cancel_btn().click();

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "when update HS_ServiceInfo",false);


	}//end void
	
	public  void Verify_HSServiceDetail_func(HSService_Info service)
	{
		String field_name = "";

		field_name = "HS_ListPrice_txt";

        //======================CALCULATE THE REFERRAL AMOUNT BEFORE VERIFY
		service.Caculate_Referral_Amount();
		
		VerifyFieldValueEqual_func(field_name,HS_ListPrice_txt(), service.ListPrice,"");

		field_name = "HS_SalePrice_txt";

		VerifyFieldValueEqual_func(field_name,HS_SalePrice_txt(), service.SalePrice,"");

		field_name = "HS_CmsionPercnt_txt";
		VerifyFieldValueEqual_func(field_name,HS_CmsionPercnt_txt(), service.CommissionPercent,"");

		field_name = "HS_ListStartDate_txt";
		VerifyFieldValueEqual_func(field_name,HS_ListStartDate_txt(), service.ListStartDate,"");

		field_name = "HS_ListEndDate_txt";
		VerifyFieldValueEqual_func(field_name,HS_ListEndDate_txt(), service.ListEndDate,"");

		field_name = "HS_CloseDate_txt";
		VerifyFieldValueEqual_func(field_name,HS_CloseDate_txt(), service.CloseDate,"");

		field_name = "HS_RebateAmount_txt";

		VerifyFieldValueEqual_func(field_name,HS_RebateAmount_txt(), service.RebateAmount,"");
	
		
		field_name = "HS_BuyOutDate_txt";
		VerifyFieldValueEqual_func(field_name,HS_BuyOutDate_txt(), service.BuyOutAcceptDate,"");

//		field_name = "HS_EscrowPercent_txt";
//		VerifyFieldValueEqual_func(field_name,HS_EscrowPercent_txt(), service.EscrowFeePercent,"");

//		field_name = "HS_EscrowFeeAmount_txt";

//		VerifyFieldValueEqual_func(field_name,HS_EscrowFeeAmount_txt(), service.EscrowFeeAmount,"");

		field_name = "HS_IsRebateSent_chk";
		VerifyFieldValueEqual_func(field_name,HS_IsRebateSent_chk(),Boolean.toString(service.IsRebaseSent),"");

		field_name = "HS_IsTookBOut_chk";
		VerifyFieldValueEqual_func(field_name,HS_IsTookBOut_chk(),Boolean.toString(service.IsTookBuyOut),"");

//		field_name = "HS_IsInvoiceCreate_chk";
//		VerifyFieldValueEqual_func(field_name,HS_IsInvoiceCreate_chk(),service.IsInvoiceCreate.toString(),"");

//		field_name = "HS_IsInvoiceClose_chk";
//		VerifyFieldValueEqual_func(field_name,HS_IsInvoiceClose_chk(),service.IsInvoiceClose.toString(),"");

//		field_name = "HS_IsEscrReceive_chk";
//		VerifyFieldValueEqual_func(field_name,HS_IsEscrReceive_chk(),service.IsEscrowFeeReceive.toString(),"");

		field_name = "HS_IsSignRefAgree_chk";
		VerifyFieldValueEqual_func(field_name,HS_IsSignRefAgree_chk(),Boolean.toString(service.IsSignRefFeeAgree),"");
		
		//PRICE
		
		
		field_name = "HS_OriListPrice_txt";
		VerifyFieldValueEqual_func(field_name,HS_OriListPrice_txt(),service.OriListPrice,"");
		
		field_name = "HS_CurrentPrice_txt";
		VerifyFieldValueEqual_func(field_name,HS_CurrentPrice_txt(),service.CurrentPrice,"");
		
		field_name = "HS_1st_PriceReduce_txt";
		VerifyFieldValueEqual_func(field_name,HS_1st_PriceReduce_txt(),service.HS_1st_PriceReduce,"");
		
		field_name = "HS_2st_PriceReduce_txt";
		VerifyFieldValueEqual_func(field_name,HS_2st_PriceReduce_txt(),service.HS_2st_PriceReduce,"");
		
		field_name = "HS_3st_PriceReduce_txt";
		VerifyFieldValueEqual_func(field_name,HS_3st_PriceReduce_txt(),service.HS_3st_PriceReduce,"");
		
		field_name = "HS_4st_PriceReduce_txt";
		VerifyFieldValueEqual_func(field_name,HS_4st_PriceReduce_txt(),service.HS_4st_PriceReduce,"");
		
		field_name = "HS_5st_PriceReduce_txt";
		VerifyFieldValueEqual_func(field_name,HS_5st_PriceReduce_txt(),service.HS_5st_PriceReduce,"");
		
		field_name = "HS_6st_PriceReduce_txt";
		VerifyFieldValueEqual_func(field_name,HS_6st_PriceReduce_txt(),service.HS_6st_PriceReduce,"");
		
		//FEE AND INVOICE
		field_name = "HS_IsRefFeeReceive_opt";
		VerifyFieldValueEqual_func(field_name,HS_IsRefFeeReceive_opt(),Boolean.toString(service.IsRefFeeReceive),"");
		
		field_name = "HS_IsNotAutoCal_opt";
		VerifyFieldValueEqual_func(field_name,HS_IsNotAutoCal_opt(),Boolean.toString(service.IsNotAutoCal),"");
		
		field_name = "HS_RefPercent_txt";
		VerifyFieldValueEqual_func(field_name,HS_RefPercent_txt(),service.RefPercent,"");
		
		field_name = "HS_RefFeeAmount_vlue";
		VerifyFieldValueEqual_func(field_name,HS_RefFeeAmount_txt(),service.RefFeeAmount_vlue,"");
			
		
	}//end void
	
	
	public  void Verify_RebateAmount_logic_func(String state)
	{
		
		String[] state_lists = {"Alabama", "Alaska", "Kansas", "Louisiana", "Mississippi", "Missouri", "Oklahoma", "Oregon", "Tennessee"} ;
		
		List<String> lists = Arrays.asList(state_lists);
		
		String field_name = "HS_RebateAmount_txt";
		
		if(lists.contains(state))
		{
			String value_str = "$0.50";
			
			Verify_Field_Not_Editable_func(field_name, HS_RebateAmount_txt(), value_str,  "when State["+state+"]");
			
			VerifyFieldValueEqual_func(field_name,HS_RebateAmount_txt(), "$0.00","");
		}
		else
		{
			String value_str = "$0.50";
			
			Verify_Field_Editable_func(field_name, HS_RebateAmount_txt(), value_str, "when State["+state+"]");
			
			VerifyFieldValueEqual_func(field_name, HS_RebateAmount_txt(), value_str, "when State["+state+"]");
			
		}
		
		
	}

}
