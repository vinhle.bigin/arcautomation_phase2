package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.CustomGroup_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class CF_ManageGroup_tab extends PageObjects{
	public CF_ManageGroup_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	LeftMenu_page LeftMenu_page;
	
	private  String Detail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";
	private String ManageGroup_tab_xpath = "//label[@id= 'manage-group-option']";
	private String NewGroup_btn_xpath = "//button[contains(text() ,'New Group')]";
	
	private String CFGroup_FieldType_ddl_xpath = Detail_mdal_xpath + "//select[@placeholder ='Field Type']";
	private String CFGroup_Tab_ddl_xpath = Detail_mdal_xpath + "//select[@placeholder='Tab']";
	private String CFGroup_GroupName_txt_xpath = Detail_mdal_xpath + "//input[@placeholder='Group Name']";
	private String CFGroup_Submit_button_xpath = "//button[@data-control='create']";
	private String CFGroup_Cancel_button_xpath = "//button[@data-control='cancel-create']";
	
	private String Search_FieldType_ddl_xpath = "//div[@id='custom-field-group-search']//select[@placeholder='Field Type']";
	private String Search_Tab_ddl_xpath = "//div[@id='custom-field-group-search']//select[@placeholder='Tab']";
	private String Search_btn_xpath = "//div[@id='custom-field-group-search']//button[@data-control='search']";
	private String Search_GroupName_txt_xpath = "//div[@id='table-list-ctf-group']//input";
	private String CFGroupList_tbl_xpath = "//div[@id= 'table-list-ctf-group']";
	private String Edit_GroupName_btn_xpath = "//i[@class='icon-note default-color']";
	
	
	
	public  WebElement Search_btn() {
		  return GetElement(Search_btn_xpath);
		 }
	
	public  WebElement Search_FieldType_ddl() {
		  return GetElement(Search_FieldType_ddl_xpath);
		 }
	
	public  WebElement Search_Tab_ddl() {
		  return GetElement(Search_Tab_ddl_xpath);
		 }
	
	public  WebElement CFGroupList_tbl() {
		  return GetElement(CFGroupList_tbl_xpath);
		 }
	
	public  WebElement Edit_GroupName_btn() {
		  return GetElement(Edit_GroupName_btn_xpath);
		 }
	
	public  WebElement Search_GroupName_txt() {
		  return GetElement(Search_GroupName_txt_xpath);
		 }
	
	public  WebElement ManageGroup_tab() {
		  return GetElement(ManageGroup_tab_xpath);
		 }
	
	public  WebElement NewGroup_btn() {
		  return GetElement(NewGroup_btn_xpath);
		 }
	
	public  WebElement CFGroup_FieldType_ddl() {
		  return GetElement(CFGroup_FieldType_ddl_xpath);
		 }
	
	public  WebElement CFGroup_Tab_ddl() {
		  return GetElement(CFGroup_Tab_ddl_xpath);
		 }
	
	public  WebElement CFGroup_GroupName_txt() {
		  return GetElement(CFGroup_GroupName_txt_xpath);
		 }
	
	public  WebElement CFGroup_Submit_button() {
		  return GetElement(CFGroup_Submit_button_xpath);
		 }
	
	public  WebElement CFGroup_Cancel_button() {
		  return GetElement(CFGroup_Cancel_button_xpath);
		 }
	
	public void Create_CFGroup_func(CustomGroup_Info group_info, String button)
	{
		Optimize_ElementClick(ManageGroup_tab());

		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		ARCLoading.Wait_Util_FinishedPageLoading();
		
		ARCLoading.Wait_Util_FinishedARClogoLoading();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Optimize_ElementClick(NewGroup_btn());
		
		SelectDropdownItem(CFGroup_FieldType_ddl(), group_info.FieldType, "");
		
		SelectDropdownItem(CFGroup_Tab_ddl(), group_info.Tab, "");
		
		ClearThenEnterValueToField(CFGroup_GroupName_txt(), group_info.Name);
		
		if(button=="Submit")
		{
			CFGroup_Submit_button().click();
		}
		else if(button=="Cancel")
		{
			CFGroup_Cancel_button().click();
		}
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Search_New_CFGroup_func(CustomGroup_Info group_info, String GroupName)
	{
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		ARCLoading.Wait_Util_FinishedPageLoading();
		
		SelectDropdownItem(Search_FieldType_ddl(), group_info.FieldType, "");
		
		SelectDropdownItem(Search_Tab_ddl(), group_info.Tab, "");
		
		Optimize_ElementClick(Search_btn());
		
		if(NewGroup_btn().equals(null))
		{
			Optimize_ElementClick(ManageGroup_tab());
		}
		try {
			Thread.sleep(10000);
			
			ClearThenEnterValueToField(Search_GroupName_txt(), GroupName);
			
			Thread.sleep(1000);

		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

////////////////////////////////VERIFY
	public void Verify_CFGourp_ExistTable_func(CustomGroup_Info group_info)
	{
		Boolean exist = false;
		List<String> records = GetTableRecordsPerPaging_func(CFGroupList_tbl());

		for(String index_str: records)
		{
			if(index_str.contains(group_info.Name))
			{
				exist = true;
				break;
			}//end if
		}//end foreach

		if(!exist)
		{
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "Transferee["+group_info.Name+"] NOT exist in table.\n";
		}//end if
	}

	public void Verify_New_CFGroup(CustomGroup_Info group_info)
	{
		Optimize_ElementClick(Edit_GroupName_btn());
		
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		ARCLoading.Wait_Util_FinishedPageLoading();
		
		ARCLoading.Wait_Util_FinishedARClogoLoading();
		
		String field = "";
		
		field = "Field Type";
		
		VerifyFieldValueEqual_func(field, CFGroup_FieldType_ddl(), group_info.FieldType, "");
		
		field = "Tab";
		
		VerifyFieldValueEqual_func(field, CFGroup_Tab_ddl(), group_info.Tab, "");
		
		field = "Group Name";
		
		VerifyFieldValueEqual_func(field, CFGroup_GroupName_txt(), group_info.Name, "");
	}
}
