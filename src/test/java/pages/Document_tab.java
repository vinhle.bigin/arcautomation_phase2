package pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Document_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;
import custom_Func.FileManage;

public class Document_tab extends PageObjects{
	
	public Document_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private ARCLoading ARCLoading;
	private Messages_Notification Messages_Notification;
	private FileManage FileManage = new FileManage();
	
	private ActivityDetails_mdal ActivityDetails_mdal;
	
	private  String actived_detail_mdal_xpath = "//div[contains(@id,'document') and contains(@style,'display: block')]";
	
	private  String Tab_xpath = "//a[contains(@href, 'documents') and @data-toggle = 'tab']";

	private  String CreateNew_btn_xpath= "//button[@data-target = '#modal-new-documents' or contains(text(),'New Document')]";

	private  String Search_btn_xpath= "//button[contains(@id,'btn-search')]";

	private  String Modal_Edit_btn_xpath = actived_detail_mdal_xpath + "//a[@data-original-title = 'Edit']";

	private  String SearchSubject_txt_xpath = "//input[@placeholder = 'Document name']";

	private  String SearchDescription_txt_xpath = "//input[@placeholder='Description']";

	private  String Modal_ServiceType_ddl_xpath= actived_detail_mdal_xpath + "//select[@placeholder = 'Service']";

	private  String Modal_DocName_txt_xpath= actived_detail_mdal_xpath+ "//div[contains(@org-placeholder,'Document name')]/input | //input[@placeholder ='Document name']";

	private  String Submit_btn_xpath= actived_detail_mdal_xpath+ "//button[@type = 'submit']";

	private  String Cancel_btn_xpath= "//div[@class = 'modal-footer']//button[contains(@data-control, 'cancel')]";

	private  String Attach_lnk_xpath= "//div[@id = 'email-process-attachment']//div[@class = 'content dz-clickable']";

	private  String DocList_tbl_xpath= "//table[@id = 'table-list-document']//tbody | //div[contains(@id,'list-document-tab') or contains(@id,'table-list-document')]//table//tbody";

	private  String Edit_icon_xpath = "//span[contains(@data-original-title,'Edit')]";

	//private  String Remove_icon_xpath = ".//a[contains(@href,'document/delete')]"

	private  String Remove_icon_xpath = "//span[@data-toggle='tooltip']//i[@class='icon-close']";

	private  String DownLoad_icon_xpath = "//a[contains(@class,'b-download-media')]";

	private  String UpdateDescriptionDoc_txt_xpath = "//div[@org-placeholder='Description']//input";

	private  String Close_icon_xpath = "//div[@id='loading-modal-document-edit']//i[@class='icon-close']";
	
	private  String Close_modal_xpath = actived_detail_mdal_xpath+"//i[@class='icon-close']";
	
	private  String ClientView_btn_xpath = actived_detail_mdal_xpath+"//div[@class='b__components b-checkbox']//label[contains(text(),'Client View')]";
	
	private  String VendorView_btn_xpath = actived_detail_mdal_xpath+"//div[@class='b__components b-checkbox']//label[contains(text(),'Vendor View')]";
	
	private  String TransfereeView_btn_xpath = actived_detail_mdal_xpath+"//div[@class='b__components b-checkbox']//label[contains(text(),'Transferee View')]";

	
	public WebElement Navigate_Tab()
	{
		return GetElement(Tab_xpath);
	}
	
	public  WebElement ClientView_btn() {
		return GetElement(ClientView_btn_xpath);
	}
	
	public  WebElement VendorView_btn() {
		return GetElement(VendorView_btn_xpath);
	}
	
	public  WebElement TransfereeView_btn() {
		return GetElement(TransfereeView_btn_xpath);
	}
			
	public  WebElement SearchDescription_txt() {
	  return GetElement(SearchDescription_txt_xpath);
	 }
	
	public  WebElement Modal_ServiceType_ddl() {
	  return GetElement(Modal_ServiceType_ddl_xpath);
	 }

	public  WebElement Modal_DocName_txt() {
	  return GetElement(Modal_DocName_txt_xpath);
	 }


	public  WebElement Submit_btn() {
	  return GetElement(Submit_btn_xpath);
	 }

	public  WebElement Cancel_btn() {
	  return GetElement(Cancel_btn_xpath);
	 }

	public  WebElement Attach_lnk() {
	  return GetElement(Attach_lnk_xpath);
	 }

	public  WebElement DocList_tbl() {
	  return GetElement(DocList_tbl_xpath);
	 }

	public  WebElement UpdateDescriptionDoc_txt() {
	  return GetElement(UpdateDescriptionDoc_txt_xpath);
	 }

	public  WebElement Close_icon() {
	  return GetElement(Close_icon_xpath);
	 }

	public  WebElement SearchSubject_txt() {
	  return GetElement(SearchSubject_txt_xpath);
	 }

	public  WebElement Search_btn() {
	  return GetElement(Search_btn_xpath);
	 }

	public  WebElement CreateNew_btn() {
	  return GetElement(CreateNew_btn_xpath);
	 }

	public  WebElement CloseModal_btn() {
		return GetElement(Close_modal_xpath);
	}
	
	public  WebElement DownLoad_icon(String docname_str) {
		WebElement temp_element  = GetElementInTable(docname_str,DocList_tbl(), DownLoad_icon_xpath);
		return temp_element;
	}

	public  WebElement Remove_icon(String docname_str) {
		WebElement temp_element  = GetElementInTable(docname_str,DocList_tbl(), Remove_icon_xpath);

		return temp_element;
	}

	public  WebElement Edit_icon(String docname_str) {
		WebElement temp_element  = GetElementInTable(docname_str,DocList_tbl(), Edit_icon_xpath);
		return temp_element;
	}
	

	
	public  WebElement Modal_Edit_btn() {
		  return GetElement(Modal_Edit_btn_xpath);
		 }
	
	
	
	
	//=================================METHOD

		public  void SearchDoc_func(String docname_str)
		{
			//Wait_For_ElementDisplay(SearchSubject_txt());
	
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			ClearThenEnterValueToField(SearchSubject_txt(),docname_str);
			
			Optimize_ElementClick(Search_btn());
			
		}

		public  void VerifyDocExistTable_func(Document_Info doc)
		{
			boolean exist = false;
			//SearchDoc_func(doc.file_name);
			List<String> records = GetTableRecordsPerPaging_func(DocList_tbl());

			for(String index_str: records)
			{
				if(index_str.contains(doc.file_name))
				{
					exist = true;
					break;
				}//end if;
			}//end foreach

			if(!exist)
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Doc["+doc.file_name+"] NOT exist in table.\n";
			}//end if
		}

		public  void VerifyDocNotExistTable_func(Document_Info doc)
		{
			Boolean exist = false;
			
			List<String> records = GetTableRecordsPerPaging_func(DocList_tbl());


			for(String index_str: records)
			{
				if(index_str.contains(doc.file_name))
				{
					exist = true;
					break;
				}//end if
			}//end foreach

			if(exist==true)
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Doc["+doc.file_name+"] SHOULD NOT exist in table.\n";
			}//end if
		}

		public  void VerifyModalInfoViewMode_func(Document_Info doc_info)
		{
			String area = "";
			String field_name = "";
			String mode = "View Mode";

			//Edit_icon(doc_info.file_name).click();
			
			try {
				
				Optimize_ElementClick(Edit_icon(doc_info.file_name));
				
			} catch(StaleElementReferenceException e) {
				
				Optimize_ElementClick(Edit_icon(doc_info.file_name));
			}
			

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			field_name = "Modal_DocName_txt";
			VerifyFieldValueEqual_func(area+field_name , Modal_DocName_txt(),doc_info.file_name, mode);

			if(!doc_info.ServiceType.equals("")&&Modal_ServiceType_ddl()!=null)
			{
				field_name = "Modal_ServiceType_ddl";
				VerifyFieldValueEqual_func(area+field_name , Modal_ServiceType_ddl(),doc_info.ServiceType, mode);
			}


		}//end void

			
		public  void CreateDoc_func(Document_Info newdoc) {
			try {
			
			Messages_Notification = new Messages_Notification(driverbase);

			new LeftMenu_page(driverbase).GotoDocumentTab_func();

			CreateNew_btn().click();
			
			wait_For_PageLoad();

			//Upload file
			FileManage.UploadFile_func(Attach_lnk(), newdoc.file_name);

			Thread.sleep(5000);
			
			if(newdoc.ClientView == true)
			{
				Optimize_ElementClick(ActivityDetails_mdal.ClientView_btn());
			}//end if
			
			if(newdoc.VendorView == true)
			{
				Optimize_ElementClick(ActivityDetails_mdal.VendorView_btn());
			}//end if
			
			if(newdoc.TransView == true)
			{
				Optimize_ElementClick(ActivityDetails_mdal.TransfereeView_btn());
			}//end if

			//this.Name = Modal_DocName_txt().getAttribute("value")

			Submit_btn().click();
			
			Thread.sleep(5000);
			
			//wait_For_PageLoad();
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"when Edit Document.",false);
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		public  void EditDoc_func(Document_Info existingdoc)//This function no needs to return a DocumentInfo object
		{
			ARCLoading = new ARCLoading(driverbase);
			Messages_Notification = new Messages_Notification(driverbase);
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
			
			try {
				
				try {
					
					Edit_icon(existingdoc.file_name).click();
				
				} catch(StaleElementReferenceException e) {
				
					Edit_icon(existingdoc.file_name).click();
				}
				
			//Edit_icon(existingdoc.file_name).click();

			Thread.sleep(2000);

			Modal_Edit_btn().click();

			Thread.sleep(2000);
			
			//Description
			ClearThenEnterValueToField(UpdateDescriptionDoc_txt(), existingdoc.description);
			
			//String note_str = "Document Details - ServiceType Dropdown.";
			
			//SelectDropdownItem(Modal_ServiceType_ddl(), existingdoc.ServiceType,note_str);

			Submit_btn().click();
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"when Edit Document.",false);

			if (CloseModal_btn() != null) {
				Optimize_ElementClick(CloseModal_btn());
				
			}
			
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}//end void

		public  void DeleteDoc_func(String file_name)
		{
			
			try {
				Messages_Notification = new Messages_Notification(driverbase);
				
				new LeftMenu_page(driverbase).GotoDocumentTab_func();
	
				SearchDoc_func(file_name);
				
					try {
					
						Remove_icon(file_name).click();
				
					} catch(StaleElementReferenceException e) {
				
						Remove_icon(file_name).click();
					}
	
				//Remove_icon(file_name).click();
	
				Thread.sleep(2000);
	
				Messages_Notification.Delete_btn().click();
	
				Thread.sleep(3000);
	
				Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"when Edit Document.",false);
	
				} catch (InterruptedException e) {
				// TODO Auto-generated catch block
							
				Assert.fail(TestConfigs.glb_TCFailedMessage+".\n"+e.getMessage());
			}
		}//end void
		
		public  void Verify_Document_Modal_Not_Editable(String note)
		{
			Document_Info new_Document = new Document_Info();
			
			String fieldname_str = "Doc Modal - Description txt";
			
			Verify_Field_Not_Editable_func(fieldname_str, UpdateDescriptionDoc_txt(), new_Document.description, note);
			
			fieldname_str = "Doc Modal - Modal_ServiceType_ddl";
			
			Verify_Field_Not_Editable_func(fieldname_str, Modal_ServiceType_ddl(), new_Document.ServiceType, note);
			
			
		}

		
		
		

}
