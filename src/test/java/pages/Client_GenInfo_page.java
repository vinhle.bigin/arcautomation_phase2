package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Client_Info;
import configuration.DriverConfig.DriverBase;

public class Client_GenInfo_page extends PageObjects{
	private ARCLoading ARCLoading ;
	
	private  String Generall_tab_xpath = "//a[contains(@href,'general-info')]";
	private  String Address_tab_xpath = "//a[contains(@href,'#client-address')]";

	private  String Custom_tab_xpath = "//a[contains(@href,'#custom-fields')]";


	//CLIENT INFO
	private  String CompName_txt_xpath = "//input[@placeholder = 'Company name']";
	private  String Prof_Edit_btn_xpath= "//div[@id = 'edit-client-profile-panel-general-info']//a[contains(@id,'icon-edit')]";
	private  String Prof_Submit_btn_xpath= "//div[@id = 'edit-client-profile-panel-general-info']//button[@data-control = 'update']";
	private  String Prof_Cancel_btn_xpath= "//div[@id = 'edit-client-profile-panel-general-info']//button[@data-control = 'cancel-update']";

	//STATUS
	private  String Stt_Edit_btn_xpath= "//div[contains(@id,'edit-client-status')]//a[contains(@id,'icon-edit')]";
	private  String Stt_Submit_btn_xpath= "//div[contains(@id,'edit-client-status')]//button[@data-control = 'update']";
	private  String Stt_Cancel_btn_xpath= "//div[contains(@id,'edit-client-status')]//button[@data-control = 'cancel-update']";
	private  String Status_ddl_xpath= "//div[contains(@id, 'edit-client-status')]//select[@placeholder = 'Status']";


	//HEADING ELEMENT
	private  String HeadingNewClient_btn_xpath = "//div[@class ='page-menu']//a[contains(@href,'create')]";
	private  String HeadingNewNote_btn_xpath = "//div[@class ='page-menu']//a/p[text() ='Note']";
	private  String HeadingPrint_btn_xpath = "//div[@class ='page-menu']//a/p[text() ='Print']";

	public Client_GenInfo_page(DriverBase driver) {
		// TODO Auto-generated constructor stub
		super(driver);
		ARCLoading = new ARCLoading(driver);
	}
	
	//DEFINE ELEMENTS
	public  WebElement Generall_tab() {
		
	return GetElement(Generall_tab_xpath);
	}
	
	public  WebElement Custom_tab() {
	  return GetElement(Custom_tab_xpath);
	 }
	public  WebElement Address_tab() {
	  return GetElement(Address_tab_xpath);
	 }
	public  WebElement CompName_txt () {
	  return GetElement(CompName_txt_xpath);
	 }
	public  WebElement Prof_Edit_btn() {
	  return GetElement(Prof_Edit_btn_xpath);
	 }
	public  WebElement Prof_Submit_btn() {
	  return GetElement(Prof_Submit_btn_xpath);
	 }
	public  WebElement Prof_Cancel_btn() {
	  return GetElement(Prof_Cancel_btn_xpath);
	 }
	public  WebElement Stt_Edit_btn() {
	  return GetElement(Stt_Edit_btn_xpath);
	 }
	public  WebElement Stt_Submit_btn() {
	  return GetElement(Stt_Submit_btn_xpath);
	 }
	public  WebElement Stt_Cancel_btn() {
	  return GetElement(Stt_Cancel_btn_xpath);
	 }
	public  WebElement Status_ddl() {
	  return GetElement(Status_ddl_xpath);
	 }
	public  WebElement HeadingNewClient_btn () {
	  return GetElement(HeadingNewClient_btn_xpath);
	 }
	public  WebElement HeadingNewNote_btn () {
	  return GetElement(HeadingNewNote_btn_xpath);
	 }
	public  WebElement HeadingPrint_btn () {
	  return GetElement(HeadingPrint_btn_xpath);
	 }

	
	
	
	
/////////================================METHOD
public  void VerifyGeneralInfoViewMode_func(Client_Info client)
{

	ARCLoading.Wait_Util_FinishedPageLoading();

	if(Status_ddl()==null)
	{
		Generall_tab().click();
	
	//	Wait_For_ElementDisplay(Status_ddl());
	}
		

	String result_str;
	result_str = CompName_txt().getAttribute("value");

	VerifyFieldValueEqual_func("CompanyName",CompName_txt(),client.CompanyName," - ViewMode");

	System.out.println("1." + result_str);

	result_str = Status_ddl().getText();
	System.out.println("2." + result_str);
	VerifyFieldValueEqual_func("Status",Status_ddl(),client.Status," - ViewMode");
	
	
}//end void

}
