package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Auth_Info;
import businessObjects.Mail_Info;
import businessObjects.Transferee_Info;
import configuration.DriverConfig.DriverBase;

public class Transf_GenInfo_tab extends PageObjects {

	public Transf_GenInfo_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String Generall_tab_xpath = "//a[@href = '#transferee-general-info']";

	private String Bank_tab_xpath = "//a[@href = '#transferee-dependents']";

	private String Custom_tab_xpath = "//a[@href = '#transferee-custom-fields']";

	// TRANSFERE INFO
	private String Salulation_slect_xpath = "//select[@placeholder = 'Salutation']";
	private String Fname_txt_xpath = "//input[@placeholder = 'First Name']";

	private String Lname_txt_xpath = "//input[@placeholder = 'Last Name']";
	private String Trf_Mid_txt_xpath = "//input[@placeholder = 'Mid']";
	private String Trf_Pronu_txt_xpath = "//div[@org-placeholder='Pronunciation']//input";
	private String Jobtle_txt_xpath = "//input[@placeholder = 'Job Title']";
	private String Trf_Nickname_txt_xpath = "//input[@placeholder = 'Nickname']";
	private String Trf_IsExecOff_opt_xpath = "//label[contains(text(),'Executive Officer')]/..//input[@type = 'checkbox']";
	private String Trf_Coord_txt_xpath = "//input[@id = 'input-list-coordinators']";
	private String Trf_Coord_ddl_xpath = "//div[@id = 'list-coordinators']//ul";
	private String Trf_CoordAss_txt_xpath = "//input[@id = 'input-list-coordinator-assistants']";
	private String Trf_CoordAss_ddl_xpath = "//div[@id = 'list-coordinator-assistants']//ul";
	private String Trf_TeamLead_txt_xpath = "//input[@id = 'input-list-team-leads']";
	private String Trf_TeamLead_ddl_xpath = "//div[@id = 'list-team-leads']//ul";
	private String Trf_AdditleNote_txt_xpath = "//div[@org-placeholder='Additional Notes']//textarea";
	private String Trf_UrgentInstr_txt_xpath = "//div[@org-placeholder='Urgent Instructions']//textarea";
	private String TransInfoEdit_btn_xpath = "//a[@id ='icon-edit-transferee-infomation-panel']";
	private String TransInfoUpdate_btn_xpath = "//div[@id ='edit-transferee-infomation-panel']//button[@data-control = 'update']";
	private String TransInfoCancel_btn_xpath = "//div[@id ='edit-transferee-infomation-panel']//button[@data-control = 'cancel-update']";

	// CLIENT
	private String Client_txt_xpath = "//input[@id = 'list-clients']";
	private String Client_ddl_xpath = "//input[@id = 'input-list-clients']";
	private String ClientContact_ddl_xpath = "//select[@placeholder = 'Client Contact']";
	private String ClientEdit_btn_xpath = "//a[@id = 'icon-edit-client-panel']";
	private String ClientUpdate_btn_xpath = "//div[@id = 'edit-client-panel']//button[@data-control = 'update']";
	private String ClientCancel_btn_xpath = "//div[@id = 'edit-client-panel']//button[@data-control = 'cancel-update']";

	// STATUS
	private String StatusEdit_btn_xpath = "//a[@id = 'icon-edit-status-infomation-panel']";
	private String Status_ddl_xpath = "//div[@org-placeholder='Status']//*[@placeholder = 'Status']";
	private String Stt_ChangeDate_vlue_xpath = "//input[@placeholder = 'Status Change Date']";
	private String Stt_IsOverride_opt_xpath = "//label[contains(text(),'Override')]/../input";
	private String Stt_IsQualified_opt_xpath = "//label[contains(text(),'Qualified')]/../input";
	private String Stt_FileEnterDate_txt_xpath = "";
	private String Stt_IsRecvSgnABAD_opt_xpath = "//label[contains(text(),'Received Signed ABAD')]/../input";
	private String Stt_IsRefLstSide_opt_xpath = "//label[contains(text(),'Referred List Side')]/../input";

	private String Stt_Update_btn_xpath = "//div[contains(@id,'edit-status')]//button[@data-control = 'update']";
	private String Stt_Cancel_btn_xpath = "//div[contains(@id,'edit-status')]//button[@data-control = 'cancel-update']";

	private String PhoneList_xpath = "//div[@class = 'mobile-wrap']";

	private String EMailList_xpath = "//div[@class='email-wrap extension-fields']";

	// VIEW MODE
	private String Fname_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'First Name']/input";

	private String Lname_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Last Name']/input";

	private String Trf_Mid_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Mid']";

	private String Trf_Pronu_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Pronunciation']//input";

	private String Jobtle_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Job Title']//input";// "//div[@org-placeholder
																																		// =
																																		// 'Job
																																		// Title']/..";

	private String Trf_Nickname_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Nickname']//input";

	private String Trf_Coord_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//div[@id = 'list-coordinators']//div[@class = 'result']//div[@class = 'content']//div[@class = 'name']";

	private String Trf_CoordAss_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//div[@id = 'list-coordinator-assistants']/..";/// div[@class
																																			/// =
																																			/// 'result']//div[@class
																																			/// =
																																			/// 'content']//div[@class
																																			/// =
																																			/// 'name']"

	private String Trf_TeamLead_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//div[@id = 'list-team-leads']//input";// div[@class
																																	// =
																																	// 'result']//div[@class
																																	// =
																																	// 'content']//div[@class
																																	// =
																																	// 'name']"

	private String Trf_AdditleNote_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//pre[@placeholder = 'Additional Notes']";

	private String Trf_UrgentInstr_view_xpath = "//div[@id = 'edit-transferee-infomation-panel']//pre[@placeholder = 'Urgent Instructions']";

	private String Client_view_xpath = "//div[@org-placeholder='Client']//input";
	// private String ClientContact_view_xpath= "//div[@org-placeholder ='Client
	// Contact']" ; //"//select[@placeholder = 'Client Contact']"

	private String Stt_FileEnterDate_view_xpath = "//div[@org-placeholder ='File Entered Date']//input";

	private String TransInfo_heading_lbl_xpath = "//div[@id ='edit-transferee-infomation-panel']//div[@class ='panel-heading']";

	private String ClientInfo_heading_lbl_xpath = "//div[@id ='edit-client-panel']//div[@class ='panel-heading']";
	// private String ClientInfo_heading_lbl= "//div[@id
	// ='edit-client-panel']//div[@class ='panel-heading']"
	private String StatusInfo_heading_lbl_xpath = "//div[@id ='edit-status-infomation-panel']//div[@class ='panel-heading']";

	// PHONE
	private String Phone_Edit_btn_xpath = "//a[@id ='icon-edit-phones-panel']";

	private String Phone_Update_btn_xpath = "//div[@id='edit-phones-panel']//button[@data-control ='update']";

	private String Phone_Cancel_btn_xpath = "//div[@id='edit-phones-panel']//button[@data-control ='cancel-update']";

	// EMAIL

	private String Email_Edit_btn_xpath = "//a[@id ='icon-edit-emails-panel']";

	private String Email_Update_btn_xpath = "//div[@id='edit-emails-panel']//button[@data-control ='update']";

	private String Email_Cancel_btn_xpath = "//div[@id='edit-emails-panel']//button[@data-control ='cancel-update']";

	public WebElement Generall_tab() {
		return GetElement(Generall_tab_xpath);
	}

	public WebElement Bank_tab() {
		return GetElement(Bank_tab_xpath);
	}

	public WebElement Custom_tab() {
		return GetElement(Custom_tab_xpath);
	}

	public WebElement Salulation_slect() {
		return GetElement(Salulation_slect_xpath);
	}

	public WebElement Fname_txt() {
		return GetElement(Fname_txt_xpath);
	}

	public WebElement Lname_txt() {
		return GetElement(Lname_txt_xpath);
	}

	public WebElement Trf_Mid_txt() {
		return GetElement(Trf_Mid_txt_xpath);
	}

	public WebElement Trf_Pronu_txt() {
		return GetElement(Trf_Pronu_txt_xpath);
	}

	public WebElement Jobtle_txt() {
		return GetElement(Jobtle_txt_xpath);
	}

	public WebElement Trf_Nickname_txt() {
		return GetElement(Trf_Nickname_txt_xpath);
	}

	public WebElement Trf_IsExecOff_opt() {
		return GetElement(Trf_IsExecOff_opt_xpath);
	}

	public WebElement Trf_Coord_txt() {
		return GetElement(Trf_Coord_txt_xpath);
	}

	public WebElement Trf_Coord_ddl() {
		return GetElement(Trf_Coord_ddl_xpath);
	}

	public WebElement Trf_CoordAss_txt() {
		return GetElement(Trf_CoordAss_txt_xpath);
	}

	public WebElement Trf_CoordAss_ddl() {
		return GetElement(Trf_CoordAss_ddl_xpath);
	}

	public WebElement Trf_TeamLead_txt() {
		return GetElement(Trf_TeamLead_txt_xpath);
	}

	public WebElement Trf_TeamLead_ddl() {
		return GetElement(Trf_TeamLead_ddl_xpath);
	}

	public WebElement Trf_AdditleNote_txt() {
		return GetElement(Trf_AdditleNote_txt_xpath);
	}

	public WebElement Trf_UrgentInstr_txt() {
		return GetElement(Trf_UrgentInstr_txt_xpath);
	}

	public WebElement TransInfoEdit_btn() {
		return GetElement(TransInfoEdit_btn_xpath);
	}

	public WebElement TransInfoUpdate_btn() {
		return GetElement(TransInfoUpdate_btn_xpath);
	}

	public WebElement TransInfoCancel_btn() {
		return GetElement(TransInfoCancel_btn_xpath);
	}

	public WebElement Client_txt() {
		return GetElement(Client_txt_xpath);
	}

	public WebElement Client_ddl() {
		return GetElement(Client_ddl_xpath);
	}

	public WebElement ClientContact_ddl() {
		return GetElement(ClientContact_ddl_xpath);
	}

	public WebElement ClientEdit_btn() {
		return GetElement(ClientEdit_btn_xpath);
	}

	public WebElement ClientUpdate_btn() {
		return GetElement(ClientUpdate_btn_xpath);
	}

	public WebElement ClientCancel_btn() {
		return GetElement(ClientCancel_btn_xpath);
	}

	public WebElement StatusEdit_btn() {
		return GetElement(StatusEdit_btn_xpath);
	}

	public WebElement Status_ddl() {
		return GetElement(Status_ddl_xpath);
	}

	public WebElement Stt_ChangeDate_vlue() {
		return GetElement(Stt_ChangeDate_vlue_xpath);
	}

	public WebElement Stt_IsOverride_opt() {
		return GetElement(Stt_IsOverride_opt_xpath);
	}

	public WebElement Stt_IsQualified_opt() {
		return GetElement(Stt_IsQualified_opt_xpath);
	}

	public WebElement Stt_FileEnterDate_txt() {
		return GetElement(Stt_FileEnterDate_txt_xpath);
	}

	public WebElement Stt_IsRecvSgnABAD_opt() {
		return GetElement(Stt_IsRecvSgnABAD_opt_xpath);
	}

	public WebElement Stt_IsRefLstSide_opt() {
		return GetElement(Stt_IsRefLstSide_opt_xpath);
	}

	public WebElement Stt_Update_btn() {
		return GetElement(Stt_Update_btn_xpath);
	}

	public WebElement Stt_Cancel_btn() {
		return GetElement(Stt_Cancel_btn_xpath);
	}

	public WebElement PhoneList() {
		return GetElement(PhoneList_xpath);
	}

	public WebElement EMailList() {
		return GetElement(EMailList_xpath);
	}

	public WebElement Fname_view() {
		return GetElement(Fname_view_xpath);
	}

	public WebElement Lname_view() {
		return GetElement(Lname_view_xpath);
	}

	public WebElement Trf_Mid_view() {
		return GetElement(Trf_Mid_view_xpath);
	}

	public WebElement Trf_Pronu_view() {
		return GetElement(Trf_Pronu_view_xpath);
	}

	public WebElement Jobtle_view() {
		return GetElement(Jobtle_view_xpath);
	}

	public WebElement Trf_Nickname_view() {
		return GetElement(Trf_Nickname_view_xpath);
	}

	public WebElement Trf_Coord_view() {
		return GetElement(Trf_Coord_view_xpath);
	}

	public WebElement Trf_CoordAss_view() {
		return GetElement(Trf_CoordAss_view_xpath);
	}

	public WebElement Trf_TeamLead_view() {
		return GetElement(Trf_TeamLead_view_xpath);
	}

	public WebElement Trf_AdditleNote_view() {
		return GetElement(Trf_AdditleNote_view_xpath);
	}

	public WebElement Trf_UrgentInstr_view() {
		return GetElement(Trf_UrgentInstr_view_xpath);
	}

	public WebElement Client_view() {
		return GetElement(Client_view_xpath);
	}

	public WebElement Stt_FileEnterDate_view() {
		return GetElement(Stt_FileEnterDate_view_xpath);
	}

	public WebElement TransInfo_heading_lbl() {
		return GetElement(TransInfo_heading_lbl_xpath);
	}

	public WebElement ClientInfo_heading_lbl() {
		return GetElement(ClientInfo_heading_lbl_xpath);
	}

	public WebElement StatusInfo_heading_lbl() {
		return GetElement(StatusInfo_heading_lbl_xpath);
	}

	public WebElement Phone_Edit_btn() {
		return GetElement(Phone_Edit_btn_xpath);
	}

	public WebElement Phone_Update_btn() {
		return GetElement(Phone_Update_btn_xpath);
	}

	public WebElement Phone_Cancel_btn() {
		return GetElement(Phone_Cancel_btn_xpath);
	}

	public WebElement Email_Edit_btn() {
		return GetElement(Email_Edit_btn_xpath);
	}

	public WebElement Email_Update_btn() {
		return GetElement(Email_Update_btn_xpath);
	}

	public WebElement Email_Cancel_btn() {
		return GetElement(Email_Cancel_btn_xpath);
	}

	public WebElement PhoneType_ddl(int index) {
		try {
			String e_xpath = ".//select[@placeholder = 'Type']";

			List<WebElement> temp_element = PhoneList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public WebElement Phone_txt(int index) {
		try {
			String e_xpath = ".//input[@type = 'tel']";

			List<WebElement> temp_element = PhoneList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public WebElement PhonePrefer_opt(int index) {
		try {
			String e_xpath = ".//input[@type = 'checkbox']";

			List<WebElement> temp_element = PhoneList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public WebElement EmailType_ddl(int index) {
		try {
			String e_xpath = ".//select[@placeholder = 'Type']";

			List<WebElement> temp_element = EMailList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public WebElement EmailAddr_txt(int index) {
		try {
			String e_xpath = ".//div[@org-placeholder='E-mail']//input";

			List<WebElement> temp_element = EMailList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		} catch (NoSuchElementException e) {
			return null;
		}
	}

//=============================================================================
/////////======================================================================METHOD

	private void Verify_Transferee_Information_section_ViewMode(Transferee_Info tf) {
		// TRANSFEREE INFORMATION

		VerifyFieldValueEqual_func("FirstName", Fname_view(), tf.firstname, " - ViewMode");

		VerifyFieldValueEqual_func("LastName", Lname_view(), tf.lastname, " - ViewMode");

		VerifyFieldValueEqual_func("Trf_Pronu_view", Trf_Pronu_txt(), tf.Pronunciation, " - ViewMode");

		VerifyFieldValueEqual_func("Jobtitle", Jobtle_view(), tf.Jobtitle, " - ViewMode");

		VerifyFieldValueEqual_func("IsExecutiveOfficer", Stt_IsRefLstSide_opt(),
				Boolean.toString(tf.IsExecutiveOfficer), "View Mode");

		// Scroll down to identify the element
		ScrollElementtoViewPort_func(Trf_Coord_view());

		VerifyFieldValueEqual_func("Coordinator", Trf_Coord_view(), tf.Coordinator, " - ViewMode");

		VerifyFieldValueEqual_func("Coordinator text field", Trf_Coord_txt(), tf.Coordinator, " - ViewMode");

		VerifyFieldValueEqual_func("CoordinatorAss", Trf_CoordAss_ddl(), tf.CoordinatorAssistant, " - ViewMode");

		VerifyFieldValueEqual_func("Trf_TeamLead_ddl", Trf_TeamLead_ddl(), tf.TeamLeader, " - ViewMode");

		VerifyFieldValueEqual_func("Trf_UrgentInstr_txt", Trf_UrgentInstr_txt(), tf.UrgentInstructions, " - ViewMode");
	}// end void

	private void Verify_Client_Section_ViewMode(Transferee_Info tf) {
		// Client COMPANY
		VerifyFieldValueEqual_func("Client_view", Client_view(), tf.Client.CompanyName, " - ViewMode");

		
		if(tf.Client.Contacts.size()>0)
			VerifyFieldValueEqual_func("Client Contact", ClientContact_ddl(), tf.Client.Contacts.get(0).FullName, "View Mode");
		
	}// end void

	private void Verify_Status_Section_ViewMode(boolean IsARCPortal, Transferee_Info tf) {
		// STATUS SECTION
		VerifyFieldValueEqual_func("Status", Status_ddl(), tf.Status, " - ViewMode");

		VerifyFieldValueEqual_func("FileEnterDate", Stt_FileEnterDate_view(), tf.FileEnterDate, " - ViewMode");

		if (IsARCPortal == true) {
			VerifyFieldValueEqual_func("Stt_IsOverride_opt", Stt_IsOverride_opt(), Boolean.toString(tf.IsOverRide),
					"View Mode");

			VerifyFieldValueEqual_func("Stt_IsRecvSgnABAD_opt", Stt_IsRecvSgnABAD_opt(),
					Boolean.toString(tf.IsReceivedSignedABAD), "View Mode");

			VerifyFieldValueEqual_func("Stt_IsRefLstSide_opt", Stt_IsRefLstSide_opt(),
					Boolean.toString(tf.ReferedListSide), "View Mode");

		}
	}// end void

	public void VerifyGeneralInfoViewMode_func(boolean IsARCPortal, Transferee_Info tf) {
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

		if (Status_ddl() == null)
			// Wait_For_ElementDisplay(Status_ddl());

			Verify_Transferee_Information_section_ViewMode(tf);

		Verify_Client_Section_ViewMode(tf);

		Verify_Status_Section_ViewMode(IsARCPortal, tf);
		
		if(!tf.PhoneList.get(0).Type.equals(""))
			VerifyFieldValueEqual_func("PhoneType_ddl", PhoneType_ddl(0), tf.PhoneList.get(0).Type, " - ViewMode");

		if(!tf.PhoneList.get(0).Numb.equals(""))
		VerifyFieldValueEqual_func("Phone_txt", Phone_txt(0), tf.PhoneList.get(0).Numb, " - ViewMode");

		if(!tf.MailList.get(0).Type.equals(""))
			VerifyFieldValueEqual_func("EmailType_ddl", EmailType_ddl(0), tf.MailList.get(0).Type, " - ViewMode");

		if(!tf.MailList.get(0).EmailAddr.equals(""))
			VerifyFieldValueEqual_func("EmailAddr_txt", EmailAddr_txt(0), tf.MailList.get(0).EmailAddr, " - ViewMode");
		
	}

//EDIT GENERAL INFO
	public void EditGenInfo_func(Transferee_Info tf) {

		try {
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

			// TRANSF INFO

			// Optimize_ElementClick(Generall_tab())

			Optimize_ElementClick(TransInfoEdit_btn());

			ScrollElementtoViewPort_func(Salulation_slect());

			Optimize_ElementClick(Salulation_slect());
			// WebUI.delay(2)
			String note_str = "Transferee General Details - Salutation Dropdown.";

			SelectDropdownItem(Salulation_slect(), tf.Salutation, note_str);

			ClearThenEnterValueToField(Fname_txt(), tf.firstname);

			ClearThenEnterValueToField(Lname_txt(), tf.lastname);

			note_str = "Transferee General Details - Assistant Dropdown.";

			SelectDropdownItem(Trf_CoordAss_ddl(), tf.CoordinatorAssistant, note_str);

			if (Trf_IsExecOff_opt().getText().equals(tf.IsExecutiveOfficer.toString()))
				;
			Trf_IsExecOff_opt().click();

			ClearThenEnterValueToField(Trf_Pronu_txt(), tf.Pronunciation);

			note_str = "Transferee General Details - TeamLeader Dropdown.";

			SelectDropdownItem(Trf_TeamLead_ddl(), tf.TeamLeader, note_str);

			ClearThenEnterValueToField(Trf_UrgentInstr_txt(), tf.UrgentInstructions);

			TransInfoUpdate_btn().click();

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,
					"Update Transferee Section.\n", false);

			Thread.sleep(5000);
			// CLIENT INFO

			Optimize_ElementClick(ClientEdit_btn());

			// ClientContact_ddl().selectByVisibleText(Client_info.Contacts.get(0).FullName)

			Optimize_ElementClick(ClientUpdate_btn());

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,
					"Update Client Section.\n", false);

			// STATUS INFO
			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

			Optimize_ElementClick(StatusEdit_btn());

			String current_stt = GetFieldText_func(Status_ddl());

			if (tf.Status != current_stt) {
				ScrollElementtoViewPort_func(Status_ddl());

				note_str = "Transferee General Details - Status Dropdown.";

				SelectDropdownItem(Status_ddl(), tf.Status, note_str);
			}

			String Stt_IsOverride_opt_str = GetFieldText_func(Stt_IsOverride_opt());

			if (!tf.IsOverRide.toString().equals(Stt_IsOverride_opt_str)) {
				Optimize_ElementClick(Stt_IsOverride_opt());
			}

			String Stt_IsRecvSgnABAD_str = GetFieldText_func(Stt_IsRecvSgnABAD_opt());

			if (!tf.IsReceivedSignedABAD.toString().equals(Stt_IsRecvSgnABAD_str)) {
				Optimize_ElementClick(Stt_IsRecvSgnABAD_opt());
			}

			String Stt_IsRefLstSide_str = GetFieldText_func(Stt_IsRefLstSide_opt());

			if (!tf.IsReceivedSignedABAD.toString().equals(Stt_IsRefLstSide_str)) {
				Optimize_ElementClick(Stt_IsRefLstSide_opt());
			}

			Optimize_ElementClick(Stt_Update_btn());

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,
					"Update Transferee Status Info", false);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}// end void

	public void Update_Transferee_Email(Mail_Info mail) {
		Optimize_ElementClick(Email_Edit_btn());

//	SelectDropdownItem(EmailType_ddl(0), mail.Type, "EmailType dropdown.");

		Optimize_ElementSendkey(EmailAddr_txt(0), mail.EmailAddr);

		Optimize_ElementClick(Email_Update_btn());

		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,
				"Update Transferee Email Info", false);

	}


}
