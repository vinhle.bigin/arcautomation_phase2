package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import configuration.DriverConfig.DriverBase;

public class Transf_AddOriOff_sect extends PageObjects{
	
	
	
	public Transf_AddOriOff_sect(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

		///ORIGINAL OFF
		private  String OrgOff_Edit_btn_xpath= "//a[@id = 'icon-edit-transferee-origin-office-address']";
		private  String OrgOff_Update_btn_xpath= "//div[@id = 'edit-transferee-origin-office-address']//button[@data-control = 'update']";
		private  String OrgOff_Cancel_btn_xpath= "//div[@id = 'edit-transferee-origin-office-address']//button[@data-control = 'cancel-update']";
		private  String OrgOff_Addrs1_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter Address 1']//input";
		private  String OrgOff_Addrs2_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter Address 2']//input";
		private  String OrgOff_City_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter City']//input";
		private  String OrgOff_State_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter State']//select";
		private  String OrgOff_Zip_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter Zip']//input";
		private  String OrgOff_Country_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter Country']//select";
		private  String OrgOff_IsBilling_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][1]/div[1]//input";
		private  String OrgOff_IsShipping_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][1]/div[2]//input";
		private  String OrgOff_IsMailing_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][1]/div[3]//input";
		private  String OrgOff_IsPrimary_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][1]/div[4]//input";
		private  String OrgOff_IsSendMailPayment_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][2]/div[1]//input";
		private  String OrgOff_IsSendMailTaxDoc_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][2]/div[2]//input";
		private  String OrgOff_IsSendMailService_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][2]/div[3]//input";


	
	public  WebElement OrgOff_Edit_btn() {
		  return GetElement(OrgOff_Edit_btn_xpath);
		 }
	public  WebElement OrgOff_Update_btn() {
		  return GetElement(OrgOff_Update_btn_xpath);
		 }
	public  WebElement OrgOff_Cancel_btn() {
		  return GetElement(OrgOff_Cancel_btn_xpath);
		 }
	public  WebElement OrgOff_Addrs1() {
		  return GetElement(OrgOff_Addrs1_xpath);
		 }

	public  WebElement OrgOff_Addrs2() {
		  return GetElement(OrgOff_Addrs2_xpath);
		 }
	public  WebElement OrgOff_City() {
		  return GetElement(OrgOff_City_xpath);
		 }
	public  WebElement OrgOff_State() {
		  return GetElement(OrgOff_State_xpath);
		 }
	public  WebElement OrgOff_Zip() {
		  return GetElement(OrgOff_Zip_xpath);
		 }
	public  WebElement OrgOff_Country() {
		  return GetElement(OrgOff_Country_xpath);
		 }
	public  WebElement OrgOff_IsBilling() {
		  return GetElement(OrgOff_IsBilling_xpath);
		 }
	public  WebElement OrgOff_IsShipping() {
		  return GetElement(OrgOff_IsShipping_xpath);
		 }
	public  WebElement OrgOff_IsMailing() {
		  return GetElement(OrgOff_IsMailing_xpath);
		 }
	public  WebElement OrgOff_IsPrimary() {
		  return GetElement(OrgOff_IsPrimary_xpath);
		 }
	public  WebElement OrgOff_IsSendMailPayment() {
		  return GetElement(OrgOff_IsSendMailPayment_xpath);
		 }
	public  WebElement OrgOff_IsSendMailTaxDoc() {
		  return GetElement(OrgOff_IsSendMailTaxDoc_xpath);
		 }
	public  WebElement OrgOff_IsSendMailService() {
		  return GetElement(OrgOff_IsSendMailService_xpath);
		 }
	

	public  void Edit_TransfAddrInfo_func(Address_Info addr)
	{
		try
		
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";

		//	Wait_For_ElementDisplay(OrgOff_Edit_btn());

			Optimize_ElementClick(OrgOff_Edit_btn());

			ScrollElementtoViewPort_func(OrgOff_Addrs1());
			ClearThenEnterValueToField(OrgOff_Addrs1(), addr.Addr1);

			ClearThenEnterValueToField(OrgOff_Addrs2(), addr.Addr2);

			ClearThenEnterValueToField(OrgOff_City(), addr.City);

			ClearThenEnterValueToField(OrgOff_Zip(), addr.Zip);

			String note_str = "Transferee Orgi Off Address Tab - State Dropdown.";
			
			SelectDropdownItem(OrgOff_State(),  addr.State,note_str);

			note_str = "Transferee Orgi Off Address Tab - Country Dropdown.";
			
			SelectDropdownItem(OrgOff_Country(), addr.Country,note_str);

			//addr.Country = "Viet Nam"
			//ClearThenEnterValueToField()

			OrgOff_Update_btn().click();

			Thread.sleep(5000);

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(expected_msg,"Unsuccessfull OrgOff - Info update",false);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}//end void
	
	public  void VerifyInfoViewMode_func(Address_Info addr)
	{
		String area = "";
		String field_name = "";
		String mode = "View Mode";

		//==========================================ORG OFF
		area = "Org Off - ";

		field_name = "OrgOff_Addrs1";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_Addrs1(),addr.Addr1 , mode);

		field_name = "OrgOff_Addrs2";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_Addrs2(),addr.Addr2 , mode);

		field_name = "OrgOff_City";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_City(),addr.City , mode);

		field_name = "OrgOff_Zip";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_Zip(),addr.Zip , mode);

		field_name = "OrgOff_State";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_State(),addr.State , mode);

		field_name = "OrgOff_Country";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_Country(),addr.Country , mode);

		field_name = "OrgOff_IsBilling";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_IsBilling(),addr.IsAtri_Billing.toString() , mode);

		field_name = "OrgOff_IsShipping";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_IsShipping(),addr.IsAtri_Shipping.toString() , mode);

		field_name = "OrgOff_IsMailing";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_IsMailing(),addr.IsAtri_Mailing.toString() , mode);

		field_name = "OrgOff_IsPrimary";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_IsPrimary(),addr.IsAtri_Primary.toString() , mode);

		field_name = "OrgOff_IsSendMailPayment";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_IsSendMailPayment(),addr.IsSendMailPayment.toString() , mode);

		field_name = "OrgOff_IsSendMailService";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_IsSendMailService(),addr.IsSendMailServices.toString() , mode);

		field_name = "OrgOff_IsSendMailTaxDoc";
		VerifyFieldValueEqual_func(area+field_name ,OrgOff_IsSendMailTaxDoc(),addr.IsSendMailTaxDoc.toString() , mode);


		
	}

}
