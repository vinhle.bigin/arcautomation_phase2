package pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.History_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class History_tab extends PageObjects{
	public History_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}



	private  String History_tab_xpath = "//a[@href='#tab-vendor-history']";
	private  String Search_btn_xpath= "//button[contains(@id,'btn-search')]";
	private  String Clear_btn_xpath= "//button[contains(text(),'Clear')]";
	private  String SearchFromDate_txt_xpath= "//input[contains(@id,'from_date')]";
	private  String SearchToDate_txt_xpath= "//input[contains(@id,'to_date')]";
	private  String SearchType_slect_xpath = "//select[contains(@placeholder,'Types')]";
	private  String SearchDetail_txt_xpath= "//input[@placeholder='Detail']";
	private  String SearchUser_txt_xpath= "//input[@placeholder='User']";
	private  String HistoryList_tbl_xpath= "//table[@class = 'VueTables__table btable table dataTable table-striped table-hover']//tbody";
	private  String Log_icon_xpath = "//span[@data-original-title='Log']";
	private  String UserName_lb_xpath = "//span[@id='nav-fullname']";

	
	
	//DEFINE ELEMENTS:
	public  WebElement Navigation_tab() {
	  return GetElement(History_tab_xpath);
	 }
	public  WebElement Search_btn() {
	  return GetElement(Search_btn_xpath);
	 }
	public  WebElement Clear_btn() {
	  return GetElement(Clear_btn_xpath);
	 }
	public  WebElement SearchFromDate_txt() {
	  return GetElement(SearchFromDate_txt_xpath);
	 }
	public  WebElement SearchToDate_txt() {
	  return GetElement(SearchToDate_txt_xpath);
	 }
	public  WebElement SearchType_slect() {
	  return GetElement(SearchType_slect_xpath);
	 }
	public  WebElement SearchDetail_txt() {
	  return GetElement(SearchDetail_txt_xpath);
	 }
	public  WebElement SearchUser_txt() {
	  return GetElement(SearchUser_txt_xpath);
	 }
	public  WebElement HistoryList_tbl() {
	  return GetElement(HistoryList_tbl_xpath);
	 }
	public  WebElement Log_icon() {
	  return GetElement(Log_icon_xpath);
	 }
	public  WebElement UserName_lb() {
	  return GetElement(UserName_lb_xpath);
	 }

	
	
	
	public void searchHistoryDate(History_Info info) {
		
		try{
			Thread.sleep(3000);

			Optimize_ElementSendkey(SearchFromDate_txt(), info.StartDate);
			Thread.sleep(1000);

			SearchFromDate_txt().sendKeys(Keys.TAB);

			Optimize_ElementSendkey(SearchToDate_txt(), info.EndDate);
			Thread.sleep(1000);

			Optimize_ElementClick(Search_btn());

			Thread.sleep(3000);

		} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		
	}

	public void searchHistory(String searchField, WebElement element, String subject_str)
	{
		try{
			ClearThenEnterValueToField(element, subject_str);

			Optimize_ElementClick(Search_btn());

			Thread.sleep(3000);

		} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
			}
		
	}



	public  void VerifyHistoryVendorExistTable_func(String searchField , String searchName)
	{
		Boolean exist = false;
		List<String> records = GetTableRecordsPerPaging_func(HistoryList_tbl());

		for(String index_str: records)
		{
			if(index_str.contains(searchName))
			{
				exist = true;
				break;
			}//end if
		}//end for each

		if(exist == false)
		{
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "["+searchName+"] NOT exist in table.\n";
		}//end if
		System.out.println ("History Vendor exists?: " + exist);
	}

}
