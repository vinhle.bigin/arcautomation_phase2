package pages;




import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;

import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;


public class login_page extends PageObjects {
	

	public login_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Username_xpath ="//*[@name = 'email' or @name ='username']";

	private  String Password_xpath ="//*[@name = 'password']";
	
	private  String Signin_xpath = "//button[@id ='login-btn']";
	
	private  String lock_msg_xpath = "//div[contains(@class,'alert')]/span";
	
	private  String email_error_msg_xpath = "//ul[@data-field='email']/li";
	
	private  String pass_error_msg_xpath = "//ul[@data-field='password']/li";
	
	
	private String Forgotpass_lnk_xpath = "//a[text()='Forgot Password?']";
	
	private String ForgotPass_email_txt_xpath = "//div[@style='position: relative;']//input[@type= 'email']";
	
	private  String ResetPass_btn_xpath = "//button[contains(text(),'Reset Password')]";
	//Validation message
	private  String lock_validation = "Your account has been temporarily locked. please contact ARC for more details.";
	
	
	private String login_form_xpath = "//div[@id='form-login']";
	
	public  WebElement login_form()
	{
		return GetElement(login_form_xpath);
	
	}
	
	public  WebElement email_error_msg()
	{
		return GetElement(email_error_msg_xpath);
	
	}
	
	public  WebElement pass_error_msg()
	{
		return GetElement(pass_error_msg_xpath);
	
	}

	
	//public WebElement Username_txt = Getelement_sub(Username_xpath);
	public  WebElement lock_msg()
	{
		return GetElement(lock_msg_xpath);
	
	}
	
	
	public  WebElement Username_txt()
	{
		return GetElement(Username_xpath);
	

	}
	
	public  WebElement Password_txt()
	{
		return GetElement(Password_xpath);

	}

	public  WebElement SignIn_btn()
	{
		return GetElement(Signin_xpath);
	}
	
	public  WebElement Forgotpass_lnk()
	{
		return GetElement(Forgotpass_lnk_xpath);
	}
	
	public  WebElement ForgotPass_email_txt()
	{
		return GetElement(ForgotPass_email_txt_xpath);
	}
	
	public  WebElement ResetPass_btn()
	{
		return GetElement(ResetPass_btn_xpath);
	}
	
	//METHOD: To Login to an portal
	public  void Login_func(String portal_url,String username, String password)
	{
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		driverbase.GetDriver().navigate().to(portal_url);
		
	//	Wait_For_ElementDisplay(Username_txt());
		
		wait_For_PageLoad();
		
		
		try {
			Thread.sleep(TestConfigs.glb_WaitTime*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("username: "+username);
		
		System.out.println("password: "+password);
		
		Username_txt().sendKeys(username);
	
		Password_txt().sendKeys(password);
		
		System.out.println("Username after filled: "+GetFieldText_func(Username_txt()));
		
		System.out.println("Password after filled: "+GetFieldText_func(Password_txt()));
	
		SignIn_btn().click();		
		
		//Optimize_ElementClick(SignIn_btn());
		
		wait_For_PageLoad();
		
		ARCLoading.Wait_Util_FinishedARClogoLoading();
        
        ARCLoading.Wait_Util_FinishedPageLoading();
        
       Wait_Ultil_ElementNotDisplay(Signin_xpath);
       
       if(email_error_msg()!=null)
       {
    	   Assert.fail("LOGIN PAGE STILL REMAINED DUE TO: '"+GetFieldText_func(email_error_msg())+"'");
           
       }
       
       else if(pass_error_msg()!=null)
       {
    	   Assert.fail("LOGIN PAGE STILL REMAINED DUE TO: '"+GetFieldText_func(pass_error_msg())+"'");
           
       }
       
       else if(SignIn_btn()!=null)
        {
        	
    	   System.out.println("Login form innerHTML: "+login_form().getAttribute("innerHTML"));
           
           System.out.println("Login form outerHTML: "+login_form().getAttribute("outerHTML"));
    	   
    	   System.out.println(driverbase.GetDriver().getCurrentUrl());
    	   
    	   driverbase.printBrowserLog();
    	   
    	   Assert.fail("LOGIN PAGE STILL REMAINED.");
        }
       
       else
       {
    	   System.out.println("SUCCESS LOGIN with current URL: "+driverbase.GetDriver().getCurrentUrl());
    	   
       }//end else
  	}
	
	public  void Verify_Account_is_Locked_func(String note_str)
	{
		String fieldname_str = "Lock Account Validation";
		
		VerifyFieldValueEqual_func(fieldname_str, lock_msg(), lock_validation, note_str);
		
		fieldname_str = "Username";
		
		VerifyFieldDisplayed_func(fieldname_str, Username_txt(), note_str);
	}
	
	
	public  void Verify_Account_Can_Login_func(String note_str)
	{
		String fieldname_str = "DashBoard - SideBar FullName";
		VerifyFieldDisplayed_func(fieldname_str, new Dashboard_page(driverbase).Left_FullName(), note_str);
		
	}
	
	public void Reset_Password_func(String portal_url,String email_str)
	{
		driverbase.GetDriver().navigate().to(portal_url);
		
		Optimize_ElementClick(Forgotpass_lnk());
		
		ForgotPass_email_txt().sendKeys(email_str);
		
		Optimize_ElementClick(ResetPass_btn());
		
	}

}
