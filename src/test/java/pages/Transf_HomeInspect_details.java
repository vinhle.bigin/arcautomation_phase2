package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.HomeInspectService_Info;
import configuration.DriverConfig.DriverBase;

public class Transf_HomeInspect_details extends PageObjects{
	
	
	public Transf_HomeInspect_details(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String mdal_actived_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";
	private  String mdal_Inspect_Type_ddl_xpath = mdal_actived_xpath+"//div[@org-placeholder ='Type']//select";
	private  String mdal_Inspect_AssignAgent_ddl_xpath = mdal_actived_xpath+"//div[@org-placeholder ='Assigned Agent']//select";
	private  String mdal_Inspect_OrderDate_txt_xpath = mdal_actived_xpath+"//div[@org-placeholder ='Order Date']//input";
	private  String mdal_Inspect_InspectDate_txt_xpath = mdal_actived_xpath+"//div[@org-placeholder ='Inspection Date']//input";
	private  String mdal_Inspect_ComplteDate_txt_xpath =mdal_actived_xpath+ "//div[@org-placeholder ='Completed Date']//input";
	private  String mdal_Inspect_IsFollow_opt_xpath = mdal_actived_xpath+"//label[contains(text(),'Follow-up Needed')]/..//input";
	private  String mdal_Inspect_CanclDate_txt_xpath = mdal_actived_xpath+"//div[@org-placeholder ='Cancelled Date']//input";
	private  String mdal_Inspect_Submit_btn_xpath = mdal_actived_xpath+"//button[@type ='submit']";
	private  String mdal_Inspect_Cancel_btn_xpath = mdal_actived_xpath+"//button[@data-dismiss ='modal']";
	private  String mdal_Inspect_Edit_btn_xpath = mdal_actived_xpath+"//a[@data-original-title='Edit']";
	private  String InspectNeed_InspectList_tbl_xpath = "//div[@id ='inspection-table-home-inspection-need']//table/tbody";
	private  String InspectNeed_New_btn_xpath = "//button[contains(text(),'New Inspection')]";
	private  String InspectNeed_Edit_icon_xpath = "//a[@data-original-title='Edit']";
	private  String InspectNeed_Remove_icon_xpath = "//a[@data-original-title='Remove']";
	
	private  String InspecResult_Edit_btn_xpath = "//h3[contains(text(),'Home Inspection Result')]/..//a[@data-original-title='Edit']";
	private  String InspecResult_IsClear_opt_xpath = "//label[contains(text(),'Inspection CLear')]/..//input";
	private  String InspecResult_Comment_txt_xpath = "//div[@org-placeholder='Comment']//textarea";
	private  String InspecResult_Update_btn_xpath = "//h3[contains(text(),'Home Inspection Result')]/../..//button[@data-control='update']";
	private  String InspecResult_Cancel_btn_xpath = "//h3[contains(text(),'Home Inspection Result')]/../..//button[@data-control='cancel-update']";
	private  String InspectSpecial_Edit_btn_xpath = "//h3[contains(text(),'Home Inspection Specialist')]/../..//a[@data-original-title='Edit']";
	private  String InspecSpecial_UserList_ddl_xpath = "//div[@org-placeholder ='User List']//select";
	private  String InspecSpecial_Assistant_ddl_xpath = "//div[@org-placeholder ='Assistant']//select";
	private  String InspecSpecial_Update_btn_xpath = "//h3[contains(text(),'Home Inspection Specialist')]/../..//button[@data-control='update']";
	private  String InspecSpecial_Cancel_btn_xpath = "//h3[contains(text(),'Home Inspection Specialist')]/../..//button[@data-control='cancel-update']";

	private  String InspecResult_Comment_vlue_xpath = "//div[@org-placeholder='Comment']//pre";
	
	
	
	public  WebElement InspectNeed_Remove_icon(String InspectType) {
		 
		return GetElementInTable(InspectType, InspectNeed_InspectList_tbl(), InspectNeed_Remove_icon_xpath);
		
		 }	
	
	public  WebElement InspecResult_Comment_vlue() {
		  return GetElement(InspecResult_Comment_vlue_xpath);
		 }
	
	public  WebElement mdal_Inspect_Type_ddl() {
	  return GetElement(mdal_Inspect_Type_ddl_xpath);
	 }
	
public  WebElement mdal_Inspect_AssignAgent_ddl() {
	  return GetElement(mdal_Inspect_AssignAgent_ddl_xpath);
	 }
public  WebElement mdal_Inspect_OrderDate_txt() {
	  return GetElement(mdal_Inspect_OrderDate_txt_xpath);
	 }
public  WebElement mdal_Inspect_InspectDate_txt() {
	  return GetElement(mdal_Inspect_InspectDate_txt_xpath);
	 }
public  WebElement mdal_Inspect_ComplteDate_txt() {
	  return GetElement(mdal_Inspect_ComplteDate_txt_xpath);
	 }
public  WebElement mdal_Inspect_IsFollow_opt() {
	  return GetElement(mdal_Inspect_IsFollow_opt_xpath);
	 }
public  WebElement mdal_Inspect_CanclDate_txt() {
	  return GetElement(mdal_Inspect_CanclDate_txt_xpath);
	 }
public  WebElement mdal_Inspect_Submit_btn() {
	  return GetElement(mdal_Inspect_Submit_btn_xpath);
	 }
public  WebElement mdal_Inspect_Cancel_btn() {
	  return GetElement(mdal_Inspect_Cancel_btn_xpath);
	 }
public  WebElement InspectNeed_InspectList_tbl() {
	  return GetElement(InspectNeed_InspectList_tbl_xpath);
	 }
public  WebElement InspectNeed_New_btn() {
	  return GetElement(InspectNeed_New_btn_xpath);
	 }

public  WebElement InspectNeed_Edit_icon(String InspectType) {
	  	
		return GetElementInTable(InspectType, InspectNeed_InspectList_tbl(), InspectNeed_Edit_icon_xpath);
	 }

public  WebElement InspecResult_Edit_btn() {
	  return GetElement(InspecResult_Edit_btn_xpath);
	 }
public  WebElement InspecResult_IsClear_opt() {
	  return GetElement(InspecResult_IsClear_opt_xpath);
	 }
public  WebElement InspecResult_Comment_txt() {
	  return GetElement(InspecResult_Comment_txt_xpath);
	 }
public  WebElement InspecResult_Update_btn() {
	  return GetElement(InspecResult_Update_btn_xpath);
	 }
public  WebElement InspecResult_Cancel_btn() {
	  return GetElement(InspecResult_Cancel_btn_xpath);
	 }
public  WebElement InspectSpecial_Edit_btn() {
	  return GetElement(InspectSpecial_Edit_btn_xpath);
	 }
public  WebElement InspecSpecial_UserList_ddl() {
	  return GetElement(InspecSpecial_UserList_ddl_xpath);
	 }
public  WebElement InspecSpecial_Assistant_ddl() {
	  return GetElement(InspecSpecial_Assistant_ddl_xpath);
	 }
public  WebElement InspecSpecial_Update_btn() {
	  return GetElement(InspecSpecial_Update_btn_xpath);
	 }
public  WebElement InspecSpecial_Cancel_btn() {
	  return GetElement(InspecSpecial_Cancel_btn_xpath);
	 }
public  WebElement mdal_Inspect_Edit_btn() {
	  return GetElement(mdal_Inspect_Edit_btn_xpath);
	 }

	

	
	private  void Fill_Modal_InspectNeed_Info_func(HomeInspectService_Info HomeInspect_info,String button)
	{
		String note_str= "InspectType";
		
		SelectDropdownItem(mdal_Inspect_Type_ddl(), HomeInspect_info.InspectType, note_str);
		
		
		note_str= "AssignedAgent";
		SelectDropdownItem(mdal_Inspect_AssignAgent_ddl(), HomeInspect_info.AssignedAgent, note_str);
		
		boolean isfollow = Boolean.parseBoolean(GetFieldText_func(mdal_Inspect_IsFollow_opt())) ;
		if(HomeInspect_info.IsFollowNeed!=isfollow)
			Optimize_ElementClick(mdal_Inspect_IsFollow_opt());
		
		Optimize_ElementSendkey(mdal_Inspect_OrderDate_txt(), HomeInspect_info.OrderDate);
		
		mdal_Inspect_OrderDate_txt().sendKeys(Keys.ENTER);
		
		
		Optimize_ElementSendkey(mdal_Inspect_InspectDate_txt(), HomeInspect_info.InspectDate);
		
		mdal_Inspect_InspectDate_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(mdal_Inspect_ComplteDate_txt(), HomeInspect_info.CompleteDate);
		
		mdal_Inspect_ComplteDate_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(mdal_Inspect_CanclDate_txt(), HomeInspect_info.CancelDate);
		
		mdal_Inspect_CanclDate_txt().sendKeys(Keys.ENTER);
		
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(mdal_Inspect_Submit_btn());
			
			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
			
		}//end if
		else if(button.equals("Cancel")||button.equals("cancel"))
		{
			mdal_Inspect_Cancel_btn().click();
		}//end if

		
	}
	
	public  void Update_Inspection_Result_func(HomeInspectService_Info Inspec_info,String button)
	{
	
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(InspecResult_Edit_btn());
		
		ClearThenEnterValueToField(InspecResult_Comment_txt(), Inspec_info.Comment);
		
		
		Optimize_ElementClick(InspecResult_IsClear_opt());
		
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			InspecResult_Update_btn().click();
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "when update InspectResult section.\n",false);
			
		}//end if
		else if(button.equals("Cancel")||button.equals("cancel"))
		{
			InspecResult_Cancel_btn().click();
		}//end if
		
		
		Wait_Ultil_ElementNotDisplay(InspecResult_Update_btn_xpath);
		
	
	}//end void
	
	public  void Create_New_InspectNeed_func(HomeInspectService_Info HomeInspect_info,String button)
	{
		InspectNeed_New_btn().click();
		
		Fill_Modal_InspectNeed_Info_func(HomeInspect_info,button);
	}
	
	public  void Update_Existing_InspectNeed_func(String Old_InspectType, HomeInspectService_Info HomeInspect_info,String button)
	{
		Optimize_ElementClick(InspectNeed_Edit_icon(Old_InspectType));
		
		Optimize_ElementClick(mdal_Inspect_Edit_btn());
		
		Fill_Modal_InspectNeed_Info_func(HomeInspect_info,button);
	}
	
	
	public  void Update_Inspection_Specialist_func(HomeInspectService_Info HomeInspect_info,String button)
	{
		Optimize_ElementClick(InspectSpecial_Edit_btn());
		
		String note_str = "User";
		
		SelectDropdownItem(InspecSpecial_UserList_ddl(), HomeInspect_info.User, note_str);
		
		note_str = "Assistant";
		SelectDropdownItem(InspecSpecial_Assistant_ddl(), HomeInspect_info.Assistant, note_str);
		
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(InspecSpecial_Update_btn());
			
		}//end if
		else if(button.equals("Cancel")||button.equals("cancel"))
		{
			Optimize_ElementClick(InspecSpecial_Cancel_btn());
		
		}//end if
	}
	
	public  void Remove_InspectNeed_func(String Inspecttype)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(InspectNeed_Remove_icon(Inspecttype));
		
		Optimize_ElementClick(Messages_Notification.Delete_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
	
	}
	
	
	//===========================VERIFY METHODS
	public  void Verify_Modal_InspectNeed_Info_ViewMode_func(HomeInspectService_Info HomeInspect_info,String note_str)
	{

		Optimize_ElementClick(InspectNeed_Edit_icon(HomeInspect_info.InspectType));
		
		String field_name = "";

		field_name = "InspectType";
		VerifyFieldValueEqual_func(field_name,mdal_Inspect_Type_ddl(), HomeInspect_info.InspectType,note_str);
		
		field_name = "AssignedAgent";
		VerifyFieldValueEqual_func(field_name,mdal_Inspect_AssignAgent_ddl(), HomeInspect_info.AssignedAgent,note_str);
		
		field_name = "OrderDate";
		VerifyFieldValueEqual_func(field_name,mdal_Inspect_OrderDate_txt(), HomeInspect_info.OrderDate,note_str);
		
		field_name = "InspectDate";
		VerifyFieldValueEqual_func(field_name,mdal_Inspect_InspectDate_txt(), HomeInspect_info.InspectDate,note_str);
		
		field_name = "CompleteDate";
		VerifyFieldValueEqual_func(field_name,mdal_Inspect_ComplteDate_txt(), HomeInspect_info.CompleteDate,note_str);
		
		field_name = "CancelDate";
		VerifyFieldValueEqual_func(field_name,mdal_Inspect_CanclDate_txt(), HomeInspect_info.CancelDate,note_str);
		
		field_name = "IsFollowNeed";
		VerifyFieldValueEqual_func(field_name,mdal_Inspect_IsFollow_opt(),Boolean.toString(HomeInspect_info.IsFollowNeed),note_str);
			
	}//end void
	
	public  void Verify_Inspection_Specialist_ViewMode_func(HomeInspectService_Info HomeInspect_info)
	{

		String field_name = "Assistant";

		VerifyFieldValueEqual_func(field_name,InspecSpecial_UserList_ddl(), HomeInspect_info.User,"");
			
		field_name = "User";
		VerifyFieldValueEqual_func(field_name,InspecSpecial_Assistant_ddl(), HomeInspect_info.Assistant,"");
		
	}//end void
	
	public  void Verify_Inspection_Result_ViewMode_func(HomeInspectService_Info HomeInspect_info)
	{

		String field_name = "";

		field_name = "Comment";
		VerifyFieldValueEqual_func(field_name,InspecResult_Comment_vlue(), HomeInspect_info.Comment,"");
		
		field_name = "IsClear";
		VerifyFieldValueEqual_func(field_name,InspecResult_IsClear_opt(), Boolean.toString(HomeInspect_info.IsClear),"");
		
	}//end void
	
	public  void Verify_InspectNeed_Shown_On_Table(HomeInspectService_Info HomeInspect_info)
	{
	
		
		String Is_followNeed = "";
		if(HomeInspect_info.IsFollowNeed==true)
		{
			Is_followNeed = "Yes";
		}
		else
			Is_followNeed = "No";	
		
		String expected_result = HomeInspect_info.InspectType+"|"+HomeInspect_info.AssignedAgent+"|"+HomeInspect_info.OrderDate
								+"|"+HomeInspect_info.InspectDate+"|"+Is_followNeed+"|"+HomeInspect_info.CompleteDate+"|"
								+HomeInspect_info.CancelDate;
		
				
		VerifyDataExistTable_func(InspectNeed_InspectList_tbl(), expected_result, "");
	}

}
