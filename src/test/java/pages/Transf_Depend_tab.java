package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Dependent_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;



public class Transf_Depend_tab extends PageObjects{
	
	
	
	public Transf_Depend_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Dependent_tab_xpath = "//a[@href = '#transferee-dependents']";

	private  String New_btn_xpath= "//button[@data-toggle = 'modal']";
	private  String Modal_Fname_txt_xpath= "//div[@org-placeholder = 'Enter First Name']//input";
	private  String Modal_Lname_txt_xpath= "//div[@org-placeholder = 'Enter Last Name']//input";
	private  String Modal_State_txt_xpath= "//div[@org-placeholder = 'State']//select";
	private  String Modal_EmailType_ddl_xpath= "//div[@class = 'email-wrap extension-fields']//div[@org-placeholder = 'Type']//select";
	private  String Modal_Email_txt_xpath= "//div[@org-placeholder = 'E-mail']//input";
	private  String Modal_Submit_btn_xpath= "//div[@class = 'modal-footer']//button[@type = 'submit']";
	private  String Modal_Cancel_btn_xpath= "//div[@class = 'modal-footer']//button[contains(@data-control, 'cancel')]";
	private  String Dependent_List_xpath= "//div[@id = 'table-list-transferee-dependent']//table/tbody";

	private  String Modal_Edit_btn_xpath= "//a[@data-original-title = 'Edit']";

	
	
	public  WebElement Dependent_tab() {
	  return GetElement(Dependent_tab_xpath);
	 }
	public  WebElement New_btn() {
		  return GetElement(New_btn_xpath);
		 }
	public  WebElement Modal_Fname_txt() {
		  return GetElement(Modal_Fname_txt_xpath);
		 }
	public  WebElement Modal_Lname_txt() {
		  return GetElement(Modal_Lname_txt_xpath);
		 }
	public  WebElement Modal_State_txt() {
		  return GetElement(Modal_State_txt_xpath);
		 }
	public  WebElement Modal_EmailType_ddl() {
		  return GetElement(Modal_EmailType_ddl_xpath);
		 }
	public  WebElement Modal_Email_txt() {
		  return GetElement(Modal_Email_txt_xpath);
		 }
	public  WebElement Modal_Submit_btn() {
		  return GetElement(Modal_Submit_btn_xpath);
		 }
	public  WebElement Modal_Cancel_btn() {
		  return GetElement(Modal_Cancel_btn_xpath);
		 }
	public  WebElement Dependent_List() {
		  return GetElement(Dependent_List_xpath);
		 }
	public  WebElement Modal_Edit_btn() {
		  return GetElement(Modal_Edit_btn_xpath);
		 }

	
	public  WebElement DependentName_lnk(String fullname_str) {
		
		String DependentName_lnk_xpath= "//a[contains(text(),'"+fullname_str+"')]";
		
		return GetElement(DependentName_lnk_xpath);
		
	}
	
	
/////////================================METHOD
public  void VerifyModalDetailsViewMode_func(Dependent_Info D_info)
{
	String area = "";
	String field_name = "";
	String mode = "View Mode";

	new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

	DependentName_lnk(D_info.firstname).click();

	new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

	//Wait_For_ElementDisplay(Modal_Edit_btn());

	field_name = "Modal_Fname_txt";
	VerifyFieldValueEqual_func(area+field_name , Modal_Fname_txt(),D_info.firstname, mode);

	field_name = "Modal_Lname_txt";
	VerifyFieldValueEqual_func(area+field_name , Modal_Lname_txt(),D_info.lastname, mode);

	field_name = "Modal_State_txt";
	VerifyFieldValueEqual_func(area+field_name , Modal_State_txt(),D_info.AddrInfo.State, mode);

	field_name = "Modal_Email_txt";
	VerifyFieldValueEqual_func(area+field_name , Modal_Email_txt(),D_info.email_info.EmailAddr, mode);


}//end void

public  void VerifyItemDisplayInTable_func(Dependent_Info D_info)
{
	List<String> tble_list = GetTableRecordsPerPaging_func(Dependent_List());

	int size_int = tble_list.size();
	Boolean exist = false;
	for(int i=0;i<size_int;i++)
	{
		String item_text = tble_list.get(i);
		if(item_text.contains(D_info.firstname))
		{
			exist = true;
			return;
		}//end if

	}//end for

	if(exist==false)
	{
		TestConfigs.glb_TCStatus=false;
		TestConfigs.glb_TCFailedMessage += "Dependency["+D_info.FullName+"] does not exist on the Table.Observed: "+tble_list+".\n";
	}

}//end void

//EDIT DEPENDET TAB
public  void CreateDepedent_func(Dependent_Info D_info)
{
	//Wait_For_ElementDisplay(Dependent_tab());

	Optimize_ElementClick(Dependent_tab());
	
	new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

	//Wait_For_ElementDisplay(New_btn());
	
	Optimize_ElementClick(New_btn());

	Edit_TransfDependent_func(D_info);


}



public  void UpdateDependent_func(String old_dependFname, Dependent_Info new_dependent)
{
//	Wait_For_ElementDisplay(Dependent_tab());
	Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
	
	Optimize_ElementClick(Dependent_tab());

//	Wait_For_ElementDisplay(Dependent_List());
		
	ScrollElementtoViewPort_func(Dependent_List());

	Optimize_ElementClick(DependentName_lnk(old_dependFname));
	
	Messages_Notification.Wait_Until_Success_Notificaiton_NotDisplay();
	
//	Wait_For_ElementDisplay(Modal_Edit_btn());
	
	ScrollElementtoViewPort_func(Modal_Edit_btn());
	
	Optimize_ElementClick(Modal_Edit_btn());
	
	Edit_TransfDependent_func(new_dependent);

}

public  void Edit_TransfDependent_func(Dependent_Info depend_info) {
	
	Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
	
	new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

///	Wait_For_ElementDisplay(Modal_State_txt());

	ClearThenEnterValueToField(Modal_Fname_txt(), depend_info.firstname);

	ClearThenEnterValueToField(Modal_Lname_txt(), depend_info.lastname);

	new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
	
//	Wait_For_ElementDisplay(Modal_State_txt());
	String note_str = "DependencyTab Details - State Dropdown.";
	
	SelectDropdownItem(Modal_State_txt(), depend_info.AddrInfo.State,note_str);

	 note_str = "DependencyTab Details - Email Dropdown.";
	SelectDropdownItem(Modal_EmailType_ddl(), depend_info.email_info.Type,note_str);

	ClearThenEnterValueToField(Modal_Email_txt(), depend_info.email_info.EmailAddr);

	Modal_Submit_btn().click();

	Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Create Dependent-Info",false);


}//end void

}
