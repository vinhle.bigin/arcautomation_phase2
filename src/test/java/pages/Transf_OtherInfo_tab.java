package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Other_Info;
import configuration.DriverConfig.DriverBase;

public class Transf_OtherInfo_tab extends PageObjects{
	
	
	
	public Transf_OtherInfo_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private  String Other_tab_xpath = "//a[@href = '#transferee-other-info']";

	private  String PropertyType_ddl_xpath= "//select[@placeholder = 'Property Type']";
	private  String MoveReason_ddl_xpath= "//select[@placeholder = 'Move Reason']";
	private  String FileType_ddl_xpath= "//select[@placeholder = 'Type of File']";
	private  String SaleType_ddl_xpath= "//select[@placeholder = 'Type of Sale']";
	private  String MoveType_ddl_xpath= "//select[@placeholder = 'Move Type']";
	private  String ProgramType_ddl_xpath= "//select[@placeholder = 'Type of Program']";
	private  String Move_Edit_btn_xpath= "//a[@id ='icon-edit-transferee-moving-panel-other-info']";
	private  String Move_Update_btn_xpath= "//div[@id ='edit-transferee-moving-panel-other-info']//button[@data-control = 'update']";
	private  String Move_Cancel_btn_xpath= "//div[@id ='edit-transferee-moving-panel-other-info']//button[@data-control = 'cancel-update']";
	private  String Date_Edit_btn_xpath= "//a[@id ='icon-edit-transferee-dates-panel-other-info']";
	private  String Date_Update_btn_xpath= "//div[@id ='edit-transferee-dates-panel-other-info']//button[@data-control = 'update']";
	private  String Date_Cancel_btn_xpath= "//div[@id ='edit-transferee-dates-panel-other-info']//button[@data-control = 'cancel-update']";
	private  String InitDate_txt_xpath= "//input[@id ='initiationDate']";
	private  String EffectDate_txt_xpath= "//input[@id ='effectiveDate']";
	private  String InterMoveInDate_txt_xpath= "//input[@id ='interimMoveInDate']";
	private  String InterMoveOutDate_txt_xpath= "//input[@id ='interimMoveOutDate']";
	private  String FullConvrsDate_txt_xpath= "//input[@id ='fullConversationDate']";
	private  String ExpectDate_txt_xpath= "//input[@id ='expectedMoveDate']";
	private  String MoveEndDate_txt_xpath= "//input[@id ='moveEndDate']";
	private  String JobStart_txt_xpath= "//input[@id ='jobStartDate']";
	private  String EmplNumb_txt_xpath= "//div[@org-placeholder = 'Employee Number']//input";
	private  String VendorNumb_txt_xpath= "//div[@org-placeholder = 'Vendor Number']//input";
	private  String FileNumb_txt_xpath= "//div[@org-placeholder = 'File Number']//input";
	private  String GeneralLedger_txt_xpath= "//div[@org-placeholder = 'General Ledger']//input";
	private  String Empl_Edit_btn_xpath= "//a[@id ='icon-edit-transferee-employee-panel-other-info']";
	private  String Empl_Update_btn_xpath= "//div[@id ='edit-transferee-employee-panel-other-info']//button[@data-control = 'update']";
	private  String Empl_Cancel_btn_xpath= "//div[@id ='edit-transferee-employee-panel-other-info']//button[@data-control = 'cancel-update']";
	private  String Curcy_Edit_xpath= "//a[@id ='icon-edit-transferee-currency-and-countries-panel-other-info']";
	private  String Curcy_Update_btn_xpath= "//div[@id ='edit-transferee-currency-and-countries-panel-other-info']//button[@data-control = 'update']";
	private  String Curcy_Cancel_btn_xpath= "//div[@id ='edit-transferee-currency-and-countries-panel-other-info']//button[@data-control = 'cancel-update']";
	private  String PrefCurrency_dll_xpath= "//select[@placeholder = 'Pref Currency']";
	private  String HomeCurrency_dll_xpath= "//select[@placeholder = 'Home Currency']";
	private  String HostCurrency_dll_xpath= "//select[@placeholder = 'Host Currency']";
	private  String PriCountry_dll_xpath= "//select[@placeholder = 'Primary Country']";
	private  String SecCountry_dll_xpath= "//select[@placeholder = 'Secondary Country']";
	private  String HomeCountry_dll_xpath= "//select[@placeholder = 'Home Country']";
	private  String HostCountry_dll_xpath= "//select[@placeholder = 'Host Country']";
	private  String OldCountry_dll_xpath= "//select[@placeholder = 'Old Country']";
	private  String NewCountry_dll_xpath= "//select[@placeholder = 'New Country']";
	private  String Other_Edit_xpath= "//a[@id ='icon-edit-transferee-other-panel']";
	private  String Other_Update_btn_xpath= "//div[@id ='edit-transferee-other-panel']//button[@data-control = 'update']";
	private  String Other_Cancel_btn_xpath= "//div[@id ='edit-transferee-other-panel']//button[@data-control = 'cancel-update']";
	private  String SSN_txt_xpath= "//input[@type ='password']";
	private  String POBox_txt_xpath= "//div[@org-placeholder = 'Po Box']//input";
	private  String BirthDate_txt_xpath= "//div[@org-placeholder = 'Date of Birth']//input";
	private  String Nation_txt_xpath= "//div[@org-placeholder = 'Nationality']//select";


	
	//DEFINE ELEMENTS
	public  WebElement Other_tab() {
		  return GetElement(Other_tab_xpath);
		 }
	
	public  WebElement PropertyType_ddl() {
	  return GetElement(PropertyType_ddl_xpath);
	 }
	public  WebElement MoveReason_ddl() {
	  return GetElement(MoveReason_ddl_xpath);
	 }
	public  WebElement FileType_ddl() {
	  return GetElement(FileType_ddl_xpath);
	 }
	public  WebElement SaleType_ddl() {
	  return GetElement(SaleType_ddl_xpath);
	 }
	public  WebElement MoveType_ddl() {
	  return GetElement(MoveType_ddl_xpath);
	 }
	public  WebElement ProgramType_ddl() {
	  return GetElement(ProgramType_ddl_xpath);
	 }
	public  WebElement Move_Edit_btn() {
	  return GetElement(Move_Edit_btn_xpath);
	 }
	public  WebElement Move_Update_btn() {
	  return GetElement(Move_Update_btn_xpath);
	 }
	public  WebElement Move_Cancel_btn() {
	  return GetElement(Move_Cancel_btn_xpath);
	 }
	public  WebElement Date_Edit_btn() {
	  return GetElement(Date_Edit_btn_xpath);
	 }
	public  WebElement Date_Update_btn() {
	  return GetElement(Date_Update_btn_xpath);
	 }
	public  WebElement Date_Cancel_btn() {
	  return GetElement(Date_Cancel_btn_xpath);
	 }
	public  WebElement InitDate_txt() {
	  return GetElement(InitDate_txt_xpath);
	 }
	public  WebElement EffectDate_txt() {
	  return GetElement(EffectDate_txt_xpath);
	 }
	public  WebElement InterMoveInDate_txt() {
	  return GetElement(InterMoveInDate_txt_xpath);
	 }
	public  WebElement InterMoveOutDate_txt() {
	  return GetElement(InterMoveOutDate_txt_xpath);
	 }
	public  WebElement FullConvrsDate_txt() {
	  return GetElement(FullConvrsDate_txt_xpath);
	 }
	public  WebElement ExpectDate_txt() {
	  return GetElement(ExpectDate_txt_xpath);
	 }
	public  WebElement MoveEndDate_txt() {
	  return GetElement(MoveEndDate_txt_xpath);
	 }
	public  WebElement JobStart_txt() {
	  return GetElement(JobStart_txt_xpath);
	 }
	public  WebElement EmplNumb_txt() {
	  return GetElement(EmplNumb_txt_xpath);
	 }
	public  WebElement VendorNumb_txt() {
	  return GetElement(VendorNumb_txt_xpath);
	 }
	public  WebElement FileNumb_txt() {
	  return GetElement(FileNumb_txt_xpath);
	 }
	public  WebElement GeneralLedger_txt() {
	  return GetElement(GeneralLedger_txt_xpath);
	 }
	public  WebElement Empl_Edit_btn() {
	  return GetElement(Empl_Edit_btn_xpath);
	 }
	public  WebElement Empl_Update_btn() {
	  return GetElement(Empl_Update_btn_xpath);
	 }
	public  WebElement Empl_Cancel_btn() {
	  return GetElement(Empl_Cancel_btn_xpath);
	 }
	public  WebElement Curcy_Edit() {
	  return GetElement(Curcy_Edit_xpath);
	 }
	public  WebElement Curcy_Update_btn() {
	  return GetElement(Curcy_Update_btn_xpath);
	 }
	public  WebElement Curcy_Cancel_btn() {
	  return GetElement(Curcy_Cancel_btn_xpath);
	 }
	public  WebElement PrefCurrency_dll() {
	  return GetElement(PrefCurrency_dll_xpath);
	 }
	public  WebElement HomeCurrency_dll() {
	  return GetElement(HomeCurrency_dll_xpath);
	 }
	public  WebElement HostCurrency_dll() {
	  return GetElement(HostCurrency_dll_xpath);
	 }
	public  WebElement PriCountry_dll() {
	  return GetElement(PriCountry_dll_xpath);
	 }
	public  WebElement SecCountry_dll() {
	  return GetElement(SecCountry_dll_xpath);
	 }
	public  WebElement HomeCountry_dll() {
	  return GetElement(HomeCountry_dll_xpath);
	 }
	public  WebElement HostCountry_dll() {
	  return GetElement(HostCountry_dll_xpath);
	 }
	public  WebElement OldCountry_dll() {
	  return GetElement(OldCountry_dll_xpath);
	 }
	public  WebElement NewCountry_dll() {
	  return GetElement(NewCountry_dll_xpath);
	 }
	public  WebElement Other_Edit() {
	  return GetElement(Other_Edit_xpath);
	 }
	public  WebElement Other_Update_btn() {
	  return GetElement(Other_Update_btn_xpath);
	 }
	public  WebElement Other_Cancel_btn() {
	  return GetElement(Other_Cancel_btn_xpath);
	 }
	public  WebElement SSN_txt() {
	  return GetElement(SSN_txt_xpath);
	 }
	public  WebElement POBox_txt() {
	  return GetElement(POBox_txt_xpath);
	 }
	public  WebElement BirthDate_txt() {
	  return GetElement(BirthDate_txt_xpath);
	 }
	public  WebElement Nation_txt() {
	  return GetElement(Nation_txt_xpath);
	 }

	
	
	
/////////================================METHOD
public  void VerifyInfoViewMode_func(Other_Info otherinfo)
{

	try {
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
		Optimize_ElementClick(Other_tab());

		//Wait_For_ElementDisplay(Move_Edit_btn())

		//Wait_For_ElementDisplay(PropertyType_ddl());

		Thread.sleep(3000);

		//VerifyFieldValueEqual_func("PropertyType_ddl", PropertyType_ddl(), otherinfo.PropertyType, " - Moves Info - View Mode.")

		VerifyFieldValueEqual_func("PropertyType_ddl", PropertyType_ddl(), otherinfo.PropertyType , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("MoveReason", MoveReason_ddl(), otherinfo.MoveReason, " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("FileType", FileType_ddl(), otherinfo.FileType, " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("MoveType", MoveType_ddl(), otherinfo.MoveType, " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("SaleType", SaleType_ddl(), otherinfo.SaleType, " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("ProgramType", ProgramType_ddl(), otherinfo.ProgramType , " - Moves Info - View Mode.");


		//================DATES INFO
		VerifyFieldValueEqual_func("InitDate", InitDate_txt(), otherinfo.InitDate , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("EffectDate", EffectDate_txt(), otherinfo.EffectDate , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("InterMoveInDate", InterMoveInDate_txt(), otherinfo.InterMoveInDate , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("InterMoveOutDate", InterMoveOutDate_txt(), otherinfo.InterMoveOutDate , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("FullConvers", FullConvrsDate_txt(), otherinfo.FullConvers , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("ExpectedMoveByDate", ExpectDate_txt(), otherinfo.ExpectedMoveByDate , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("MoveEnd", MoveEndDate_txt(), otherinfo.MoveEnd , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("JobStart", JobStart_txt(), otherinfo.JobStart , " - Moves Info - View Mode.");


		//==================EMPLOYEE INFO

		VerifyFieldValueEqual_func("EmployeeNumb", EmplNumb_txt(), otherinfo.EmployeeNumb , " - Moves Info - View Mode.");

		//VerifyFieldValueEqual_func("VendorNumb", VendorNumb_txt(), otherinfo.VendorNumb , " - Moves Info - View Mode.")

		//VerifyFieldValueEqual_func("FileNumb",FileNumb_txt(), otherinfo.FileNumb , " - Moves Info - View Mode.")

		//VerifyFieldValueEqual_func("GeneralLedger", GeneralLedger_txt(), otherinfo.GeneralLedger , " - Moves Info - View Mode.")

		//VerifyFieldValueEqual_func("JobStart", JobStart_txt(), otherinfo.JobStart , " - Moves Info - View Mode.")


		//===========Currency and Countries
		VerifyFieldValueEqual_func("PrefCurrency",PrefCurrency_dll(), otherinfo.PrefCurrency , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("HomeCurrency", HomeCurrency_dll(),otherinfo.HomeCurrency , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("HostCurrency",HostCurrency_dll(),otherinfo.HostCurrency , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("PriCountry", PriCountry_dll(),otherinfo.PriCountry , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("SecCountry", SecCountry_dll(),otherinfo.SecCountry , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("HomeCountry", HomeCountry_dll(),otherinfo.HomeCountry , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("HostCountry",HostCountry_dll(),otherinfo.HostCountry , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("OldCountry", OldCountry_dll(),otherinfo.OldCountry , " - Moves Info - View Mode.");
		
		VerifyFieldValueEqual_func("NewCountry", NewCountry_dll(),otherinfo.NewCountry , " - Moves Info - View Mode.");


		//======================Transferee Other Info
	//	Wait_For_ElementDisplay(SSN_txt());

		VerifyFieldValueEqual_func("SSN", SSN_txt(),otherinfo.SSN , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("PoBox", POBox_txt(),otherinfo.PoBox , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("BirthDate", BirthDate_txt(),otherinfo.BirthDate , " - Moves Info - View Mode.");

		VerifyFieldValueEqual_func("Nation", Nation_txt(),otherinfo.Nation , " - Moves Info - View Mode.");
		
	}catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

}//end void


public  void Edit_TransfOtherInfo_func(Other_Info info)
{

	try{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		String expected_msg = "Transferee has been updated successfully.";

		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
			

		Optimize_ElementClick(Other_tab());

		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();;
			

		//Wait_For_ElementDisplay(Move_Edit_btn());

		Optimize_ElementClick(Move_Edit_btn());

		ScrollElementtoViewPort_func(PropertyType_ddl());

		String note_str = "Transferee Other Details - PropertyType Dropdown.";
		
		SelectDropdownItem(PropertyType_ddl(), info.PropertyType,note_str);

		note_str = "Transferee Other Details - MoveReason Dropdown.";
		
		SelectDropdownItem(MoveReason_ddl(),  info.MoveReason,note_str);

		note_str = "Transferee Other Details - FileType Dropdown.";
		
		SelectDropdownItem(FileType_ddl(),  info.FileType,note_str);

		note_str = "Transferee Other Details - MoveType Dropdown.";
		
		SelectDropdownItem(MoveType_ddl(),  info.MoveType,note_str);

		note_str = "Transferee Other Details - SaleType Dropdown.";
		
		SelectDropdownItem(SaleType_ddl(),  info.SaleType,note_str);

		note_str = "Transferee Other Details - ProgramType Dropdown.";
		
		SelectDropdownItem(ProgramType_ddl(),  info.ProgramType,note_str);

		Move_Update_btn().click();

		Messages_Notification.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update");

		//================DATES INFO

		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
			

		//Wait_For_ElementDisplay(Date_Edit_btn());

		Optimize_ElementClick(Date_Edit_btn());
		Thread.sleep(2000);
		ScrollElementtoViewPort_func(InitDate_txt());
		Optimize_ElementSendkey(InitDate_txt(),  info.InitDate);

		Optimize_ElementSendkey(EffectDate_txt(),  info.EffectDate);

		//Optimize_ElementClick(InterMoveInDate_txt())
		Optimize_ElementSendkey(InterMoveInDate_txt(),  info.InterMoveInDate);

		//Optimize_ElementClick(InterMoveOutDate_txt())
		Optimize_ElementSendkey(InterMoveOutDate_txt(),  info.InterMoveOutDate);

		//Optimize_ElementClick(FullConvrsDate_txt())
		Optimize_ElementSendkey(FullConvrsDate_txt(),  info.FullConvers);

		//Optimize_ElementClick(ExpectDate_txt())
		Optimize_ElementSendkey(ExpectDate_txt(),  info.ExpectedMoveByDate);

		//Optimize_ElementClick(MoveEndDate_txt())
		Optimize_ElementSendkey(MoveEndDate_txt(),  info.MoveEnd);

		//Optimize_ElementClick(JobStart_txt());
		Optimize_ElementSendkey(JobStart_txt(),  info.JobStart);


		Date_Update_btn().click();

		Messages_Notification.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update");


		//==================EMPLOYEE INFO
		Optimize_ElementClick(Empl_Edit_btn());


		ScrollElementtoViewPort_func(EmplNumb_txt());
		ClearThenEnterValueToField(EmplNumb_txt(),  info.EmployeeNumb);


		//ClearThenEnterValueToField(VendorNumb_txt(),  info.VendorNumb) --Obsoleted

		//ClearThenEnterValueToField(FileNumb_txt(),  info.FileNumb)

		//ClearThenEnterValueToField(GeneralLedger_txt(),  info.GeneralLedger)

		Optimize_ElementClick(Empl_Update_btn());

		Messages_Notification.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update");


		//===========Currency and Countries
		Optimize_ElementClick(Curcy_Edit());

		ScrollElementtoViewPort_func(PrefCurrency_dll());
		
		note_str = "Transferee Other Details - PrefCurrency Dropdown.";
		
		SelectDropdownItem(PrefCurrency_dll(),  info.PrefCurrency,note_str);

		note_str = "Transferee Other Details - HomeCurrency Dropdown.";
		ScrollElementtoViewPort_func(HomeCurrency_dll());
		SelectDropdownItem(HomeCurrency_dll(), info.HomeCurrency,note_str);

		note_str = "Transferee Other Details - HostCurrency Dropdown.";
		ScrollElementtoViewPort_func(PrefCurrency_dll());
		SelectDropdownItem(HostCurrency_dll(), info.HostCurrency,note_str);

		note_str = "Transferee Other Details - PriCountry Dropdown.";
		ScrollElementtoViewPort_func(PriCountry_dll());
		SelectDropdownItem(PriCountry_dll(), info.PriCountry,note_str);

		note_str = "Transferee Other Details - SecCountry Dropdown.";
		ScrollElementtoViewPort_func(SecCountry_dll());
		SelectDropdownItem(SecCountry_dll(), info.SecCountry,note_str);

		note_str = "Transferee Other Details - HomeCountry Dropdown.";
		ScrollElementtoViewPort_func(HomeCountry_dll());
		SelectDropdownItem(HomeCountry_dll(), info.HomeCountry,note_str);

		note_str = "Transferee Other Details - HostCountry Dropdown.";
		ScrollElementtoViewPort_func(HostCountry_dll());
		SelectDropdownItem(HostCountry_dll(), info.HostCountry,note_str);

		note_str = "Transferee Other Details - OldCountry Dropdown.";
		ScrollElementtoViewPort_func(OldCountry_dll());
		SelectDropdownItem(OldCountry_dll(), info.OldCountry,note_str);

		note_str = "Transferee Other Details - NewCountry Dropdown.";
		ScrollElementtoViewPort_func(NewCountry_dll());
		SelectDropdownItem(NewCountry_dll(), info.NewCountry,note_str);

		Optimize_ElementClick(Curcy_Update_btn());

		Messages_Notification.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update");

		
		//======================Transferee Other Info
		Optimize_ElementClick(Other_Edit());
		
		ScrollElementtoViewPort_func(SSN_txt());
		
		ClearThenEnterValueToField(SSN_txt(), info.SSN);
		
		ClearThenEnterValueToField(POBox_txt(), info.PoBox);
		
		Optimize_ElementSendkey(BirthDate_txt(), info.BirthDate);
		
		note_str = "Transferee Other Details - Nation Dropdown.";
		SelectDropdownItem(Nation_txt(), info.Nation,note_str);

		Optimize_ElementClick(Other_Update_btn());

		Messages_Notification.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update");


	} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	

}//end void

}
