package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import configuration.DriverConfig.DriverBase;

public class ClientAddrDetails_mdal extends PageObjects{
	
	public ClientAddrDetails_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}



	private Messages_Notification Messages_Notification;
	
	private  String ModalEdit_btn_xpath= "//a[@data-original-title='Edit']";
	private  String ModalUpdate_btn_xpath= "//div[@class = 'modal-footer']//button[@type = 'submit']";
	private  String ModalCancel_btn_xpath= "//div[@class = 'modal-footer']//button[contains(@data-control,'cancel')]";
	private  String ModalAddrs1_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Address 1']//input";
	private  String ModalAddrs2_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Address 2']//input";
	private  String ModalCity_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'City']//input";
	private  String ModalState_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'State']//select";
	private  String ModalZip_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Zip']//input";
	private  String ModalCountry_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Country']//select";

	private  String ModalCode_txt_xpath = "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Code']//input";
	private  String ModalDescrt_txt_xpath = "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Description']//textarea";


	
	//DEFINE ELEMENTS
	

	public  WebElement ModalEdit_btn() {
	  return GetElement(ModalEdit_btn_xpath);
	 }
	public  WebElement ModalUpdate_btn() {
	  return GetElement(ModalUpdate_btn_xpath);
	 }
	public  WebElement ModalCancel_btn() {
	  return GetElement(ModalCancel_btn_xpath);
	 }
	public  WebElement ModalAddrs1_txt() {
	  return GetElement(ModalAddrs1_xpath);
	 }
	public  WebElement ModalAddrs2_txt() {
	  return GetElement(ModalAddrs2_xpath);
	 }
	public  WebElement ModalCity_txt() {
	  return GetElement(ModalCity_xpath);
	 }
	public  WebElement ModalState_ddl() {
	  return GetElement(ModalState_xpath);
	 }
	public  WebElement ModalZip_txt() {
	  return GetElement(ModalZip_xpath);
	 }
	public  WebElement ModalCountry_ddl() {
	  return GetElement(ModalCountry_xpath);
	 }
	
	public  WebElement ModalCode_txt() {
	  return GetElement(ModalCode_txt_xpath );
	 }
	public  WebElement ModalDescrt_txt() {
	  return GetElement(ModalDescrt_txt_xpath );
	 }

	
	
	/////////================================METHOD


	public  void FillModalInfo_func(Address_Info addr,String button)
	{
		ScrollElementtoViewPort_func(ModalCode_txt());

		
		ModalDescrt_txt().sendKeys(addr.Description);

		
		ModalCode_txt().sendKeys(addr.Code);
		
		ModalAddrs1_txt().sendKeys(addr.Addr1);
		
		ModalAddrs2_txt().sendKeys(addr.Addr2);
		
		ModalDescrt_txt().sendKeys(addr.City);

		ModalDescrt_txt().sendKeys(addr.Zip);
		
		String note_str = "Address Details - State Dropdown.";
		
		SelectDropdownItem(ModalState_ddl(),  addr.State,note_str);
		
		note_str = "Address Details - Country Dropdown.";

		SelectDropdownItem(ModalCountry_ddl(), addr.Country,note_str);
		
		if(button=="Submit")
		{
			//ScrollElementtoViewPort_func(ModalUpdate_btn())
			Optimize_ElementClick(ModalUpdate_btn());

		}

		else if (button == "Cancel")
			ModalCancel_btn().click();

		new Messages_Notification(driverbase).VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"Unsuccessfull Submit Create/Edit Address.",false);
	}

}
