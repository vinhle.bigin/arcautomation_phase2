package pages;

import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Mail_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;
import custom_Func.FileManage;

public class EmailProcess_mdal extends PageObjects{
	
	public EmailProcess_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private ARCLoading ARCLoading;
	private Messages_Notification Messages_Notification;
	
	private  String Service_ddl_xpath= "//div[@org-placeholder='Service']//select";
	private  String Recipient_ddl_xpath= "//div[@org-placeholder='Recipient']//select";
	private  String Template_ddl_xpath= "//div[@org-placeholder='Template']//select";
	
	private  String OrderDescr_ddl_xpath= "//div[@org-placeholder='Order Description']//select";
	private  String SelectedVendorOnly_opt_xpath= "//label[contains(text(),'Selected Vendor Only')]/..//input";
	
	private  String Contact_tbl_xpath= "//div[@id ='table-list-contact-print']//table/tbody";
	private  String From_ddl_xpath= "//label[contains(text(),'From')]/..//ul[@class ='b__multi__select__list']";
	private  String To_txt_xpath= "//input[@placeholder ='To']";
	private  String CC_txt_xpath= "//input[@placeholder ='CC']";
	private  String Bcc_txt_xpath= "//input[@placeholder ='BCC']";
	private  String Subject_txt_xpath= "//input[@placeholder ='Subject']";

	private  String Attach_lnk_xpath= "//div[@class ='content dz-clickable']";
	private  String Next_btn_xpath= "//div[@class='modal-footer']//button[@type ='submit']";
	private  String Send_btn_xpath= "//div[@class='modal-footer']//button[@type='submit' and contains(text(),'Send')]";
	private  String Cancel_btn_xpath= "//button[@data-dismiss='modal']";
	private  String From_txt_xpath= "//input[@placeholder ='From']";
	private  String From_vlue_xpath= "//span[@class='thumb']";
	private  String ContactSelect_chk_xpath= "//input[@type ='radio']";

	private  String DocReview_lnk_xpath = "//a[contains(text(),'doc_name_arg')]";

	
	
	
	public  WebElement OrderDescr_ddl() {
		  return GetElement(OrderDescr_ddl_xpath);
		 }
	
	public  WebElement SelectedVendorOnly_opt() {
		  return GetElement(SelectedVendorOnly_opt_xpath);
		 }
	
	public  WebElement Service_ddl() {
	  return GetElement(Service_ddl_xpath);
	 }
public  WebElement Recipient_ddl() {
	  return GetElement(Recipient_ddl_xpath);
	 }
public  WebElement Template_ddl() {
	  return GetElement(Template_ddl_xpath);
	 }
public  WebElement Contact_tbl() {
	  return GetElement(Contact_tbl_xpath);
	 }
public  WebElement From_ddl() {
	  return GetElement(From_ddl_xpath);
	 }
public  WebElement To_txt() {
	  return GetElement(To_txt_xpath);
	 }
public  WebElement CC_txt() {
	  return GetElement(CC_txt_xpath);
	 }
public  WebElement Bcc_txt() {
	  return GetElement(Bcc_txt_xpath);
	 }
public  WebElement Subject_txt() {
	  return GetElement(Subject_txt_xpath);
	 }

public  WebElement Attach_lnk() {
	  return GetElement(Attach_lnk_xpath);
	 }
public  WebElement Next_btn() {
	  return GetElement(Next_btn_xpath);
	 }
public  WebElement Send_btn() {
	  return GetElement(Send_btn_xpath);
	 }
public  WebElement Cancel_btn() {
	  return GetElement(Cancel_btn_xpath);
	 }
public  WebElement From_txt() {
	  return GetElement(From_txt_xpath);
	 }
public  WebElement From_vlue() {
	  return GetElement(From_vlue_xpath);
	 }
public  WebElement ContactSelect_chk() {
	  return GetElement(ContactSelect_chk_xpath);
	 }

	
	public  WebElement DocReview_lnk(String doc_name) {
		try{
			String xpath_str = DocReview_lnk_xpath.replace("doc_name_arg", doc_name);
		
			WebElement temp_element = GetElement(xpath_str);
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}
	
	public  WebElement ContactSelect_chk(String name_str) {
		try{
			
			return GetElementInTable(name_str,Contact_tbl(),ContactSelect_chk_xpath);
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}
	
	//METHOD

	public  void FillInfoModal_func(Mail_Info mail_info,String button ,Boolean customcontent)
		{

		try {
			
			GoToLatestBrowserTab();
			
			Messages_Notification = new Messages_Notification(driverbase);
			
			ARCLoading = new ARCLoading(driverbase);
				
			ARCLoading.Wait_Util_FinishedPageLoading();
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
			
			

			//Wait_For_ElementDisplay(Service_ddl());

			String note_str = "";
			//Select Recipienttype
			note_str = "Email Process Modal - Recepicient Dropdown.";
			SelectDropdownItem( Recipient_ddl(),mail_info.RecipientType,note_str);

			Thread.sleep(3000);

			//Select Service type
			note_str = "Email Process Modal - Service Dropdown.";
			SelectDropdownItem( Service_ddl(),mail_info.ServiceType,note_str);

			ARCLoading.Wait_Util_FinishedARClogoLoading();
			
			//Handle to include Householdgood ORDER Info INTO THE EMAIL CONTENT
			if(!mail_info.Order_Descr.equals("")&&mail_info.ServiceType.contains("Household"))
			{
				note_str = "Email Process Modal - Order Description Dropdown";
				SelectDropdownItem(OrderDescr_ddl(),mail_info.Order_Descr,note_str);
			}

			//Wait_For_ElementDisplay(Template_ddl());

			//Select Template Name
			note_str = "Email Process Modal - Template Dropdown.";
			SelectDropdownItem(Template_ddl(),mail_info.TemplateName,note_str);
			
			//Select Contact
			Optimize_ElementClick(ContactSelect_chk(mail_info.EmailAddr));
			
			
			//ContactSelect_chk(mail_info.EmailAddr).click();

			//Click Next button
			Next_btn().click();

			Thread.sleep(2000);

			//Select From
			Optimize_ElementClick(From_vlue());

			Optimize_ElementSendkey(From_txt(), mail_info.From);

			note_str = "Email Process Modal - From Dropdown.";
			
			SelectDropdownItem(From_ddl(),mail_info.From,"");
			
			//Fill "To" field
			
			Optimize_ElementSendkey(To_txt(), mail_info.To_list.get(0));
			
			if(mail_info.CC_list.size()!=0)
				ClearThenEnterValueToField(CC_txt(), mail_info.CC_list.get(0));
								
			if(mail_info.BCC_list.size()!=0)
				ClearThenEnterValueToField(Bcc_txt(), mail_info.BCC_list.get(0));
			
			if(customcontent==true)
			{
				ClearThenEnterValueToField(Modal_TinyContentBox(), mail_info.Content);
				//Optimize_ElementSendkey(Modal_TinyContentBox(), mail_info.Content);
				
				
				//attachment
				if(mail_info.Attach_List.size()>0)
				{
					if(!mail_info.Attach_List.get(0).equals(""))
					{
						String file_name = mail_info.Attach_List.get(0);
						new FileManage().UploadFile_func(Attach_lnk(), file_name);
					}
				}


			}//end if boolean = true

			//Click Button
			if(button.equals("Submit")||button.equals("submit"))
			{
				Optimize_ElementClick(Send_btn());
				
				Thread.sleep(2000);
				
				GoToLatestBrowserTab();
				
				if(Messages_Notification.notification_msg()!=null)
				{
					String msg = Messages_Notification.notification_msg().getText();
					String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
					if(msg ==expected_msg)
					{
						TestConfigs.glb_TCFailedMessage+= "Unsuccessfull TRANSF Creation.Msg: '"+msg+"'.\n";
						Assert.fail(TestConfigs.glb_TCFailedMessage);
					}
				}//end if of message

				else if(button.equals("Cancel")||button.equals("cancel"))
				{
					Cancel_btn().click();
				}
				
				
			
			}//end if of submit
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		}//end void

		//METHOD VERIFY SEND EMAIL MODAL
		public  void VerifyModalDetails_func(Mail_Info mail_info)
		{
			String area = "Email modal - ";
			String field = "Modal_TinyContentBox";
		
			VerifyFieldValueEqual_func(area+field, Modal_TinyContentBox(), mail_info.Content, "");
		

		}//end voids

}
