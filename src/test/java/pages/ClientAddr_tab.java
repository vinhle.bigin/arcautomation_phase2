package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;


public class ClientAddr_tab extends PageObjects{
	
	public ClientAddr_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	ARCLoading ARCLoading;
	Messages_Notification Messages_Notification ;
	
	private  String Address_Tab_xpath = "//a[@href = '#client-address']";
	private  String New_btn_xpath = "//div[@slot = 'modal-button-create form-group']/a";
	private  String AddressList_tbl_xpath = "//table[@id ='address-table']/tbody";
	private  String AddrEdit_icon_xpath = ".//span[@data-original-title='Edit Address']";
	private  String AddrRemove_icon_xpath = ".//span[@class ='btn-remove-address']";
	
	
	
	//DEFINE ELEMENTS
	
	public  WebElement Address_Tab () {
	  return GetElement(Address_Tab_xpath );
	 }
	
	public  WebElement New_btn() {
	  return GetElement(New_btn_xpath );
	 }
	public  WebElement AddressList_tbl() {
	  return GetElement(AddressList_tbl_xpath );
	 }

	
	
	public  WebElement AddrEdit_icon(String addr_code) {
		return GetElementInTable(addr_code, AddressList_tbl(), AddrEdit_icon_xpath);

	
	}

	public  WebElement AddrRemove_icon(String addr_code) {
		
			return GetElementInTable(addr_code, AddressList_tbl(), AddrRemove_icon_xpath);

		
	}

	
	/////////================================METHOD
	public  void VerifyInfoViewMode_func(Address_Info addr)
	{
		String area = "";
		String field_name = "";
		String mode = "View Mode";
		

		ARCLoading ARCLoading = new ARCLoading(driverbase);

		ARCLoading.Wait_Util_FinishedPageLoading();

		//Click on ADDRESS TAB
		Optimize_ElementClick(Address_Tab());

		ARCLoading.Wait_Util_FinishedPageLoading();

		//WaitForElementNotDisplay(LandingPage.LoadingArc())
		// CLICK ON EDIT ICON
		AddrEdit_icon(addr.Code).click();
		
		ClientAddrDetails_mdal modal = new ClientAddrDetails_mdal(driverbase);

		
		Wait_For_ElementEnable(modal.ModalCode_txt());

		field_name = "ModalCode";
		VerifyFieldValueEqual_func(area+field_name , modal.ModalCode_txt(),addr.Code, mode);

		field_name = "ModalAddrs1";
		VerifyFieldValueEqual_func(area+field_name , modal.ModalAddrs1_txt(),addr.Addr1, mode);

		field_name = "ModalAddrs2";
		VerifyFieldValueEqual_func(area+field_name , modal.ModalAddrs2_txt(),addr.Addr2 , mode);

		field_name = "ModalCity";
		VerifyFieldValueEqual_func(area+field_name , modal.ModalCity_txt(),addr.City , mode);

		field_name = "ModalZip";
		VerifyFieldValueEqual_func(area+field_name ,modal.ModalZip_txt(),addr.Zip , mode);

		field_name = "ModalState";
		VerifyFieldValueEqual_func(area+field_name , modal.ModalState_ddl(),addr.State , mode);

		field_name = "ModalCountry";
		VerifyFieldValueEqual_func(area+field_name , modal.ModalCountry_ddl(),addr.Country , mode);

	}
	
	//CREATE NEW CLIENT ADDRESS
			public  void CreateClientAddress_func(Address_Info address, String button)
			{
				 //button = "Submit" | "Cancel"
				ClientAddrDetails_mdal modal = new ClientAddrDetails_mdal(driverbase);

				Optimize_ElementClick(Address_Tab());

				ARCLoading.Wait_Util_FinishedPageLoading();

				New_btn().click();

				//ScrollElementtoViewPort_func(null)
				modal.FillModalInfo_func(address, button);

			}

			public  void UpdateClientAddress_func(String old_code,Address_Info newaddress, String button)
			{
				try {
					ClientAddrDetails_mdal modal = new ClientAddrDetails_mdal(driverbase);
					Address_Tab().click();
					ARCLoading.Wait_Util_FinishedPageLoading();

					AddrEdit_icon(old_code).click();

					Thread.sleep(3000);

					Optimize_ElementClick(modal.ModalEdit_btn());

					//ScrollElementtoViewPort_func(null)
					modal.FillModalInfo_func(newaddress, button);
				}
				catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//button = "Submit" | "Cancel"
				
			

			}

			public  void RemoveClientAddress_func(Address_Info address, String button)
			{
				try{
					Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

					ARCLoading ARCLoading = new ARCLoading(driverbase);
					
					Address_Tab().click();
					ARCLoading.Wait_Util_FinishedPageLoading();

					AddrRemove_icon(address.Code).click();
					Thread.sleep(1000);

					Messages_Notification.Delete_btn().click();

					Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When removing the Client Address.",false);

				
				} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			}//end void
			
			public void Verify_Address_Auto_Generated_After_Client_Created()
			{
				ARCLoading arc_loading = new ARCLoading(driverbase);
				Optimize_ElementClick(Address_Tab());
				arc_loading.Wait_Util_FinishedPageLoading();
					
				List<String> list_str = GetTableRecordsPerPaging_func(AddressList_tbl());

				if(list_str.size()==0)
				{
					TestConfigs.glb_TCStatus= false;
					TestConfigs.glb_TCFailedMessage+="There's should be a Address item shown after client is created.\n";

				}
			}	

}
