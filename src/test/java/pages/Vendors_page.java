package pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Contact_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class Vendors_page extends PageObjects{
	

	public Vendors_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private  String NewVendor_btn_xpath = "//button[contains(text(),'New Vendor')]";
	
	private  String NoteTop_btn_xpath = "//span[@class='menu-icon fas fa-sticky-note']";
	private  String PrintTop_btn_xpath = "//span[@class='menu-icon fas fa-print']";
	private  String Search_btn_xpath = "//button[contains(text(),'Search')]";
	private  String Clear_btn_xpath = "//button[contains(text(),'Clear')]";
	private  String OnlyMainContact_ck_xpath = "//input[contains(@name,'is_main_contact')]";
	private  String SearchName_txt_xpath = "//input[@placeholder='Vendor name']";
	private  String SearchContact_txt_xpath = "//input[@placeholder='Contact Name']";
	private  String SearchAddress1_txt_xpath = "//input[@placeholder='Address Line 1']";
	private  String SearchAddress2_txt_xpath = "//input[@placeholder='Address Line 2']";
	private  String SearchCity_txt_xpath = "//input[@placeholder='City']";
	private  String SearchZipcode_num_xpath = "//input[@placeholder='Zip Code']";
	private  String Status_drd_xpath = "//select[@id='search-vendor-status']";
	private  String EditStatus_btn_xpath = "//a[@id ='icon-status-panel']";
	private  String Status_toggle_xpath = "//input[@id = 'vendor_is_active']";
	private  String UpdateStatus_btn_xpath= "//div[@id = 'loading-status-panel']//button[@data-control='update']";
	
	private  String ServiceType_drd_xpath = "//select[@placeholder='Service Type']";

	private  String Country_drd_xpath = "//select[@name='country']";
	
	private  String State_drd_xpath = "//select[@placeholder='State']";
	
	private  String OnlyMainVendor_icon_xpath = "//i[@class='glyphicon glyphicon-ok']";
	private  String Noresults_xpath ="//td[contains(@class,'text-center')]";
	private  String VendorList_tbl_xpath = "//div[@id='table-list-vendor']//table//tbody";
	
	private  String Email_txt_xpath = "//input[@placeholder='Preferred Email Address']";

	private  String Update_btn_xpath = "//div[@class='modal-footer']//button[@class='btn btn-primary btn-rounded']";
	
	private  String Status_ddl_xpath = "//select[@id = 'search-vendor-status']";

	
	
	
	//DEFINE ELEMENTS:
	public  WebElement NewVendor_btn() {
	  return GetElement(NewVendor_btn_xpath);
	 }
	public  WebElement NoteTop_btn() {
	  return GetElement(NoteTop_btn_xpath);
	 }
	public  WebElement PrintTop_btn() {
	  return GetElement(PrintTop_btn_xpath);
	 }
	public  WebElement Search_btn() {
	  return GetElement(Search_btn_xpath);
	 }
	public  WebElement Clear_btn() {
	  return GetElement(Clear_btn_xpath);
	 }
	public  WebElement OnlyMainContact_ck() {
	  return GetElement(OnlyMainContact_ck_xpath);
	 }
	public  WebElement SearchName_txt() {
	  return GetElement(SearchName_txt_xpath);
	 }
	public  WebElement SearchContact_txt() {
	  return GetElement(SearchContact_txt_xpath);
	 }
	public  WebElement SearchAddress1_txt() {
	  return GetElement(SearchAddress1_txt_xpath);
	 }
	public  WebElement SearchAddress2_txt() {
	  return GetElement(SearchAddress2_txt_xpath);
	 }
	public  WebElement SearchCity_txt() {
	  return GetElement(SearchCity_txt_xpath);
	 }
	public  WebElement SearchZipcode_num() {
	  return GetElement(SearchZipcode_num_xpath);
	 }
	public  WebElement Status_drd() {
	  return GetElement(Status_drd_xpath);
	 }

	
	public  WebElement ServiceType_drd() {
	  return GetElement(ServiceType_drd_xpath);
	 }
	
	public  WebElement Country_drd() {
	  return GetElement(Country_drd_xpath);
	 }
	
	public  WebElement State_drd() {
	  return GetElement(State_drd_xpath);
	 }
	
	public  WebElement OnlyMainVendor_icon() {
	  return GetElement(OnlyMainVendor_icon_xpath);
	 }
	public  WebElement Noresults() {
	  return GetElement(Noresults_xpath);
	 }
	
	public  WebElement VendorList_tbl() {
	  return GetElement(VendorList_tbl_xpath);
	 }
	
	public  WebElement EditStatus_btn() {
		return GetElement(EditStatus_btn_xpath);
	}
	
	public  WebElement Status_toggle() {
		return GetElement(Status_toggle_xpath);
	}
	
	public  WebElement UpdateStatus_btn() {
		return GetElement(UpdateStatus_btn_xpath);
	}
	
	public  WebElement Status_ddl() {
		return GetElement(Status_ddl_xpath);
	}
	
	public  WebElement vendorName_link(String vendorName) {
		
		String vendorName_lnk_xapth = "//a/span[contains(text(), '"+vendorName+"')]";
		
		return GetElement(vendorName_lnk_xapth); 
	}
	
	
	public  WebElement mdal_Email_txt() {
		return GetElement(Email_txt_xpath);
	}
	
	public  WebElement mdal_Update_btn() {
		return GetElement(Update_btn_xpath);
	}
	
	///==============METHODS
	
	
	public  void Search_For_VendorName_func(Vendor_Info vendor)
	{
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ClearThenEnterValueToField(SearchName_txt(), vendor.VendorName);
		
		SelectDropdownItem(Status_ddl(), vendor.Status, "status_field");
		
		Optimize_ElementClick(OnlyMainContact_ck());
		
		Optimize_ElementClick(Search_btn());
		
		
	}
	
	public  void CreateVendor_Via_Top_Shortcut_func(Vendor_Info vendor,String submit)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
			//if(NewClient_mdal.CompanyName_txt().isDisplayed()==false)
		//	Wait_For_ElementDisplay(Shortcuts_menu.NewVendor_btn());
			Optimize_ElementClick(new Shortcuts_menu(driverbase).NewVendor_btn());
			
			new NewVendor_mdal(driverbase).FillInfoModal_func(vendor, submit);
			
			if(Messages_Notification.notification_msg()!=null)
			{
				String msg = Messages_Notification.notification_msg().getText();
				
				String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
				
				//println(msg)
				
				if(msg.equals(expected_msg))
				{
					TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Client Creation.Msg: '"+msg+"'.\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
			}//end if of message
			
			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
			
	}
	
	
	public  void Create_Vendor_Via_New_Button_func(Vendor_Info vendor,String submit)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
	

			//if(NewClient_mdal.CompanyName_txt().isDisplayed()==false)
			Optimize_ElementClick(NewVendor_btn());
			
			new NewVendor_mdal(driverbase).FillInfoModal_func(vendor, submit);
			
		//(Vendor_GenInfo_tab.Heading_GenralInfo_tab());
			
			if(Messages_Notification.notification_msg()!=null)
			{
				String msg = Messages_Notification.notification_msg().getText();
				
				String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
				
				//println(msg)
				
				if(msg.equals(expected_msg))
				{
					TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Client Creation.Msg: '"+msg+"'.\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
			}//end if of message
			
			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
	}
	
	//==================Verify
	public  void Verify_Vendor_Exist_Table_func(Vendor_Info vendor)
	{
		boolean exist = false;;
		List<String> records = GetTableRecordsPerPaging_func(VendorList_tbl());

		for(String index_str: records)
		{
			if(index_str.contains(vendor.VendorName))
			{
				exist = true;
				break;
			}//end if
		}//end foreach

		if(!exist)
		{
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "Vendor["+vendor.VendorName+"] NOT exist in table.\n";
		}//end if
	}//end void
	
	public  void Register_ContactEnail_func(Contact_Info ctact_info)
	{
		if (mdal_Email_txt() != null) 
		{
			ClearThenEnterValueToField(mdal_Email_txt(), ctact_info.email_info.EmailAddr);
		
			Optimize_ElementClick(mdal_Update_btn());
		}
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
	}


	public  void Update_Status_Vendor(Vendor_Info vendor_info) {
		// TODO Auto-generated method stub
		EditStatus_btn().click();
		
		Wait_For_ElementEnable(Status_toggle());
		
		String is_active = GetFieldText_func(Status_toggle());
		
		if(!is_active.equals(vendor_info.Status))
		{
			Optimize_ElementClick(Status_toggle());
			//Active_toggle().click();
		}
		
		Optimize_ElementClick(UpdateStatus_btn());
		
	}

}
