package pages;


import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;

import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;
import pages.expenseFeature.ExpenseReim_page;


public class LeftMenu_page extends PageObjects{
	
	public LeftMenu_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	//==========ARC page========
	
	private  String Transferee_mnuitem_xpath = "//ul[@class = 'sub-menu']//a[contains(@href, 'transferee')] | //li[contains(@class, 'active   clearfix')]//a[contains(@href, 'admin/transferee')]";	
	private  String Client_mnuitem_xpath = "//ul[@class = 'sub-menu']//a[contains(@href, 'backend/client/clients')]";
	private  String Vendor_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/vendors/vendors')]";
	private  String TaskList_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/task/task')]";
	private  String Template_mnuitem_xpath = "//ul[@class = 'sub-menu']//a[contains(@href, 'backend/template')]";
	private  String SmartTask_mnuitem_xpath = "//ul[@class = 'sub-menu']//a[contains(@href, 'backend/smart-task/tasks')]";
	private  String Custom_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/customization')]";
	private  String Role_mnuitem_xpath = "//ul[@class = 'sub-menu']//a[contains(@href, 'backend/arc-employee/role')]";
	private  String User_mnuitem_xpath = "//ul[@class = 'sub-menu']//a[contains(@href, 'backend/arc-employee/user')]";
	private  String Admin_mnu_xpath = "//ul[@class = 'menu accordion-menu']//a/p[contains(text(), 'Admin')]";
	private  String Policies_mnuitem_xpath = "//ul[@class = 'sub-menu']//a[contains(@href, 'backend/client/policies')]";
	
	//==========CLient page========
	
	private String Auth_mnuitem_xpath = "//ul[@class = 'sub-menu']//a[contains(@href, '/authorization')]"; 
	
	private String Expenses_mnuitem_xpath = "//a[contains(@href, 'admin/expense-reimbursement')] | //a[contains(@href, 'backend/expense-reimbursement')]";
	

	
	public WebElement Expenses_mnuitem() {

        return GetElement(Expenses_mnuitem_xpath);
        
    }
	
	public WebElement Policies_mnuitem() {

        return GetElement(Policies_mnuitem_xpath);
        
    }
	
	 public  WebElement Auth_mnu() {
			return GetElement(Auth_mnuitem_xpath);			
	 }
	
	public  WebElement Transferee_mnuitem() {

		WebElement e_temp = GetElement(Transferee_mnuitem_xpath);
		
		//Catch xpath for Transferee Portal
		if(e_temp==null)
		{
			String a = Transferee_mnuitem_xpath.replace("sub-menu", "menu accordion-menu").replace("@href, 'transferee'", "@href, 'admin/transferee'");
			
			e_temp = GetElement(a);
		}
		
		return e_temp;
	}

	public  WebElement Client_mnuitem() {

		return GetElement(Client_mnuitem_xpath);
	
	}

	public  WebElement Vendor_mnuitem() {

		return GetElement(Vendor_mnuitem_xpath);
		}

	public  WebElement TaskList_mnuitem() {

		return GetElement(TaskList_mnuitem_xpath);
	
	}

	public  WebElement Template_mnuitem() {

		return GetElement(Template_mnuitem_xpath);
		
	}

	public  WebElement SmartTask_mnuitem() {

		return GetElement(SmartTask_mnuitem_xpath);
	
	}

	public  WebElement Custom_mnuitem() {

		return GetElement(Custom_mnuitem_xpath);
	
	}

	public  WebElement Role_mnuitem() {

		return GetElement(Role_mnuitem_xpath);
		
	}

	public  WebElement User_mnuitem() {

		return GetElement(User_mnuitem_xpath);
		
	}

	public  WebElement Admin_mnu() {

		return GetElement(Admin_mnu_xpath);
		
	}

	
	
	///==================METHODS
	
	//1. ARC PORTAL =======================================
	
	/*
	public  void GotoUserProfile_func(){
		Usersetting_menu.UserSetting_dropdown().click();
		
		Thread.sleep(1000);
		
		Usersetting_menu.Profile_mnuitem().click();
		
		Thread.sleep(5000);
	}

*/

	public  void GotoForgotPass(String portal_url) {

		/*
		TestConfigs.OpenUrl_func(portal_url);

		Login_lnk().click();

		LoginPage.ForgotPass_lnk().click();
		Thread.sleep(5000);
		 *
		 */
	}
	
	public void GotoAuthPage_ClienPortal() {

		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		try{
			
			if(new Authorization_page(driverbase).AuthList_tbl()==null) {
				//CloseBrowserTab();
			
				Optimize_ElementClick(Auth_mnu());

				ARCLoading.Wait_Util_FinishedARClogoLoading();
				
				ARCLoading.Wait_Util_FinishedPageLoading();

			}
	
	}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT PAGE due to:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
		
	}
 
	
	public  void GotoYourTransfereePage() {
		
		Optimize_ElementClick (Transferee_mnuitem());
		
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
	//	Wait_For_ElementDisplay(Clients_page.Search_btn());
	}

	public  void GotoTransfereePage() {
		try{
			
			ARCLoading ARCLoading = new ARCLoading(driverbase);

			wait_For_PageLoad();
			
			if(new Transferees_page(driverbase).TransfList_tbl()==null) {
				CloseBrowserTab();
				
				Optimize_ElementClick(Transferee_mnuitem());
				
				ARCLoading.Wait_Util_FinishedARClogoLoading();

				ARCLoading.Wait_Util_FinishedPageLoading();

			//	Wait_For_ElementDisplay(Transferees_page.NewTransf_btn());
			}//end if
	
			}catch(Exception e) {
					TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO TRANSFEREE PAGE:"+e.getMessage()+".\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
	
	}
	
	public  void GotoVendorPage() {

		try{
			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			if(new Vendors_page(driverbase).VendorList_tbl()==null) {
				CloseBrowserTab();
//				Vendor_mnuitem().click();
//				Thread.sleep(5000);

				Optimize_ElementClick(Vendor_mnuitem());
				
				ARCLoading.Wait_Util_FinishedARClogoLoading();

				ARCLoading.Wait_Util_FinishedPageLoading();

			//	Wait_For_ElementDisplay(Vendors_page.VendorList_tbl());
			}
		}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO VENDOR PAGE:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

	public void GotoClientPage() {

		try{
			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			if(new Clients_page(driverbase).ClientList_tbl()==null) {
				CloseBrowserTab();
			
				Optimize_ElementClick(Client_mnuitem());
				
				//Thread.sleep(5000);;

				ARCLoading.Wait_Util_FinishedARClogoLoading();
				
				ARCLoading.Wait_Util_FinishedPageLoading();

			//	Wait_For_ElementDisplay(Clients_page.ClientList_tbl());
			}
	
	}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT PAGE due to:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
		
	}

	public  void GotoRolePage() {
		try{
			CloseBrowserTab();
			Role_mnuitem().click();
			Thread.sleep(5000);;
		}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO ROLE PAGE:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

	public  void GotoUserPage() {
		try{
			CloseBrowserTab();
			User_mnuitem().click();
			Thread.sleep(5000);
		}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO USER PAGE:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

	public  void GotoTaskListPage() {
		try{
			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
			
			if(new Shortcuts_menu(driverbase).NewFollow_btn()==null)
			{
								
				CloseBrowserTab();
				
				Optimize_ElementClick(TaskList_mnuitem());
				
				ARCLoading.Wait_Util_FinishedARClogoLoading();
				
				ARCLoading.Wait_Util_FinishedPageLoading();
		
			}//end if
				
		
		}
	
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO TASKLIST PAGE:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
		
	}

	public  void GotoSmartTaskPage() {

		try{

			if(new SmartTask_page(driverbase).SmartTaskList_tbl()==null) {
				Admin_mnu().click();
				Thread.sleep(1000);

				ARCLoading ARCLoading = new ARCLoading(driverbase);
				
				Optimize_ElementClick(SmartTask_mnuitem());
				
				ARCLoading.Wait_Util_FinishedARClogoLoading();
				
				ARCLoading.Wait_Util_FinishedPageLoading();
			}
		}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO SMARTTASK PAGE:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

	public  void GotoTemplatePage() {

		try{

			if(new Template_page(driverbase).Templ_List_tbl()==null) {

				ARCLoading ARCLoading = new ARCLoading(driverbase);
				
				Optimize_ElementClick(Admin_mnu());
					
				Optimize_ElementClick(Template_mnuitem());
				
				ARCLoading.Wait_Util_FinishedPageLoading();

			//	Wait_For_ElementDisplay(Template_page.Search_btn());
			}//end if


		}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO TEMPLATE PAGE:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

	public  void GotoCustomFieldPage() {
		try{
			Admin_mnu().click();
			
			Thread.sleep(1000);
			
			Optimize_ElementClick(Custom_mnuitem());
			
			Thread.sleep(5000);
			
			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();

			ARCLoading.Wait_Util_FinishedPageLoading();
		}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO CUSTOMFIELD PAGE:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

	public  void GotoClientDetailsPage(String fullname_str) {
		try{
			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			new Clients_page(driverbase).ClientName_link(fullname_str).click();
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();

			ARCLoading.Wait_Util_FinishedPageLoading();
			
			GoToLatestBrowserTab();
		}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT DETAILS PAGE:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

	public  void gotoVendorDetailsPage(String vendorName) {
		try{
			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			if(new Vendor_SearchOption_tab(driverbase).SearchOpt_tab()==null)
			
				new Vendors_page(driverbase).vendorName_link(vendorName).click();
		
			ARCLoading.Wait_Util_FinishedARClogoLoading();

			ARCLoading.Wait_Util_FinishedPageLoading();
			
			GoToLatestBrowserTab();
			
		}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO VENDOR DETAILS PAGE:"+e.getMessage()+".\n";
			
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

	public  void GotoActivityTab_func()
	{
		try {
	//	Wait_For_ElementDisplay(Activities_tab.activity_tab());
			Activities_tab Activities_tab = new Activities_tab(driverbase);
			if(Activities_tab.CreateNew_btn() == null)
				Activities_tab.activity_tab().click();
	
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//WaitForElementNotDisplay(Loading_icon_xpath)
	}//end void


/*
	public  void GotoHistoryTab_func()
	{
		try {
		Wait_For_ElementDisplay(History_tab.History_tab());

		History_tab.History_tab().click();

		Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//WaitForElementNotDisplay(Loading_icon_xpath)
	}//end void
*/
	public  void GotoDocumentTab_func()
	{
		try {
			Document_tab tab = new Document_tab(driverbase);
		//	Wait_For_ElementDisplay(tab.Document_tab());
	
			if(tab.CreateNew_btn()==null)
	
				Optimize_ElementClick(tab.Navigate_Tab());
	
			Thread.sleep(3000);;
	
			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}//end void

	public void GotoPoliciesPage() {
		
		
           try{
        	
        	ARCLoading ARCLoading = new ARCLoading(driverbase);
        	
            if(new Clients_page(driverbase).ClientList_tbl()==null) {
            	
                CloseBrowserTab();
            
                Optimize_ElementClick(Policies_mnuitem());
                
                //Thread.sleep(5000);;

                ARCLoading.Wait_Util_FinishedARClogoLoading();
                
                ARCLoading.Wait_Util_FinishedPageLoading();

            //    Wait_For_ElementDisplay(Clients_page.ClientList_tbl());
            }
    
    }
        catch(Exception e) {
            TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO POLICIES PAGE due to:"+e.getMessage()+".\n";
            Assert.fail(TestConfigs.glb_TCFailedMessage);
        }
	}
	
	public void GotoExpReimPage() {
		try{
			
			ARCLoading ARCLoading = new ARCLoading(driverbase);

			wait_For_PageLoad();
			
			if(new ExpenseReim_page(driverbase).ExpenseReim_tbl()==null) {
				CloseBrowserTab();
				
				Optimize_ElementClick(Expenses_mnuitem());
				
				ARCLoading.Wait_Util_FinishedARClogoLoading();

				ARCLoading.Wait_Util_FinishedPageLoading();
			}//end if
	
			}catch(Exception e) {
					TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO EXPENSE REIMBURSEMNT PAGE:"+e.getMessage()+".\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
	
	}
		
}
