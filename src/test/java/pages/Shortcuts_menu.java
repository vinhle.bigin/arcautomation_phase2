package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Mail_Info;
import configuration.DriverConfig.DriverBase;

public class Shortcuts_menu extends PageObjects{
	
	public Shortcuts_menu(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String NewTrans_btn_xpath= "//div[@class='page-menu']//a/p[contains(text(),'New')]";
	private  String NewVendor_btn_xpath= "//div[@class='page-menu']//a/p[contains(text(),'Vendor')]";
	private  String NewClient_btn_xpath= "//div[@class='page-menu']//a/p[contains(text(),'Client')]";
	//private  String NewActivity_btn_xpath= "//div[@class='page-menu']//a/p[text()='Activity']" - Obsolete
	private  String NewNote_btn_xpath= "//div[@class='page-menu']//a/p[contains(text(),'Note')]";
	private  String Print_btn_xpath= "//div[@class='page-menu']//a/p[contains(text(),'Print')]";
	private  String Email_btn_xpath= "//div[@class='page-menu']//a/p[contains(text(),'Email')]";
	private  String NewTask_btn_xpath = "//div[@class='page-menu']//button//span[contains(text(),'Task')] | //div[@class='page-menu']//p[contains(text(),'Task')]";
	private  String NewFollow_btn_xpath = "//div[@class='page-menu']//button//span[text()='Follow Up']";

	private  String Entity_ddl_xpath ="//div[@class='btn-group open']//ul[@class='list-unstyled']";
	

	
	public  WebElement NewTask_btn() {
		
		 return GetElement(NewTask_btn_xpath);
	}

	public  WebElement NewFollow_btn() {
		
		 return GetElement(NewFollow_btn_xpath);
	}

	public  WebElement Entity_ddl() {
		
		return GetElement(Entity_ddl_xpath);
	}

	public  WebElement NewTrans_btn() {
		
		 return GetElement(NewTrans_btn_xpath);
	
	}

	public  WebElement NewVendor_btn() {
		
		 return GetElement(NewVendor_btn_xpath);
	}

	public  WebElement NewClient_btn() {
		
		 return GetElement(NewClient_btn_xpath);
	}


	public  WebElement NewNote_btn() {
		
		return GetElement(NewNote_btn_xpath);
	
	}

	public  WebElement Print_btn() {
		
			
		return GetElement(Print_btn_xpath);
	
	}

	public  WebElement Email_btn() {
		
		return GetElement(Email_btn_xpath);

	}
	
	public  void SendEmail_With_Customed_Content_func(Mail_Info mail_info, String button)//button = "Submit"/"Cancel"/""
	{
		Optimize_ElementClick(Email_btn());

	//	Wait_For_ElementDisplay(EmailProcess_mdal.Service_ddl());

		new EmailProcess_mdal(driverbase).FillInfoModal_func(mail_info, button,true);


	}//end void
	
	public  void Send_Email_With_Template_Content_func(Mail_Info mail_info, String button)//button = "Submit"/"Cancel"/""
	{
		
		Optimize_ElementClick(Email_btn());

	//	Wait_For_ElementDisplay(EmailProcess_mdal.Service_ddl());

		new EmailProcess_mdal(driverbase).FillInfoModal_func(mail_info, button,false);


	}//end void
	
	
	

}
