package pages;


import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;

public class Order_StorageInfo_section extends PageObjects{
	
	
	
	public Order_StorageInfo_section(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String StorageInfo_Edit_btn_xpath = "//a[@id='icon-hhg-order-storage-information']";
	private  String StorageInfo_Update_btn_xpath = "//div[@id='hhg-order-storage-information']//button[@data-control='update']";
	private  String StorageInfo_Cancel_btn_xpath = "//div[@id='hhg-order-storage-information']//button[@data-control='cancel-update']";

	private  String AuthWeight_txt_xpath = "//div[@org-placeholder='Authorized Weight']//input";
	
	private  String EstWeight_txt_xpath = "//div[@org-placeholder='Estimated Weight']//input";
	
	private  String ActWeight_txt_xpath = "//div[@org-placeholder='Actual Weight']//input";
	
	private  String AuthCube_txt_xpath = "//div[@org-placeholder='Authorized Cube']//input";
	
	private  String EstCube_txt_xpath = "//div[@org-placeholder='Estimated Cube']//input";
	
	private  String ActCube_txt_xpath = "//div[@org-placeholder='Actual Cube']//input";
	
	private  String Make_txt_xpath = "//div[@org-placeholder='Make']//input";
	
	private  String Model_txt_xpath = "//div[@org-placeholder='Model']//input";
	
	private  String Year_txt_xpath = "//div[@org-placeholder='Year']//input";
	
	
	
	public  WebElement StorageInfo_Edit_btn() {
	  	return GetElement(StorageInfo_Edit_btn_xpath);
	 }
	
	public  WebElement StorageInfo_Update_btn() {
	  	return GetElement(StorageInfo_Update_btn_xpath);
	 }
	
	public  WebElement StorageInfo_Cancel_btn() {
	  	return GetElement(StorageInfo_Cancel_btn_xpath);
	 }
	
	public  WebElement AuthWeight_txt() {
	  	return GetElement(AuthWeight_txt_xpath);
	 }
	
	public  WebElement EstWeight_txt() {
	  	return GetElement(EstWeight_txt_xpath);
	 }
	
	public  WebElement ActWeight_txt() {
	  	return GetElement(ActWeight_txt_xpath);
	 }
	
	public  WebElement AuthCube_txt() {
	  	return GetElement(AuthCube_txt_xpath);
	 }
	
	public  WebElement EstCube_txt() {
	  	return GetElement(EstCube_txt_xpath);
	 }
	
	public  WebElement ActCube_txt() {
	  	return GetElement(ActCube_txt_xpath);
	 }
	
	public  WebElement Make_txt() {
	  	return GetElement(Make_txt_xpath);
	 }
	
	public  WebElement Model_txt() {
	  	return GetElement(Model_txt_xpath);
	 }
	
	public  WebElement Year_txt() {
	  	return GetElement(Year_txt_xpath);
	 }
	
	public  void Update_StorageInfo_Section_func(Service_Order_Info order, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		switch(order.ServiceType)
		{
		case "SIT" : case "EXT" : case "STG" :
			Optimize_ElementClick(StorageInfo_Edit_btn());
			
			ClearThenEnterValueToField(AuthWeight_txt(), order.StorageInfo_AuthWeight);
			
			ClearThenEnterValueToField(EstWeight_txt(), order.StorageInfo_EstWeight);
			
			ClearThenEnterValueToField(ActWeight_txt(), order.StorageInfo_ActWeight);
			
			ClearThenEnterValueToField(AuthCube_txt(), order.StorageInfo_AuthCube);
			
			ClearThenEnterValueToField(EstCube_txt(), order.StorageInfo_EstCube);
			
			ClearThenEnterValueToField(ActCube_txt(), order.StorageInfo_ActCube);
			
			ClearThenEnterValueToField(Make_txt(), order.StorageInfo_Make);
			
			ClearThenEnterValueToField(Model_txt(), order.StorageInfo_Model);
			
			ClearThenEnterValueToField(Year_txt(), order.StorageInfo_Year);
			
			if(button.equals("Submit")||button.equals("submit"))
			{
				Optimize_ElementClick(StorageInfo_Update_btn());
				
				Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Shipment Info",false);
				
				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			}
			break;
		}
	}
	
	//=======================================METHODS VERIFY
	
	public  void Verify_StorageInfo_section_func(Service_Order_Info order)
	{
		switch(order.ServiceType)
		{
		case "SIT" : case "EXT" : case "STG" :
			String area = "";
			String field = "";
			
			area = "Weight";
			
			field = "Authorized";
			
			VerifyFieldValueEqual_func(field+ area,AuthWeight_txt(), order.StorageInfo_AuthWeight,"");
			
			field = "Estimated";
			
			VerifyFieldValueEqual_func(field+ area,EstWeight_txt(), order.StorageInfo_EstWeight,"");
			
			field = "Actual";
			
			VerifyFieldValueEqual_func(field+ area,ActWeight_txt(), order.StorageInfo_ActWeight,"");
			
			area = "Cube";
			
			field = "Authorized";
			
			VerifyFieldValueEqual_func(field+ area,AuthCube_txt(), order.StorageInfo_AuthCube,"");
			
			field = "Estimated";
			
			VerifyFieldValueEqual_func(field+ area,EstCube_txt(), order.StorageInfo_EstCube,"");
			
			field = "Actual";
			
			VerifyFieldValueEqual_func(field+ area,ActCube_txt(), order.StorageInfo_ActCube,"");
			
			field = "Make";
			
			VerifyFieldValueEqual_func(field,Make_txt(), order.StorageInfo_Make,"");
			
			field = "Model";
			
			VerifyFieldValueEqual_func(field,Model_txt(), order.StorageInfo_Model,"");
			
			field = "Year";
			
			VerifyFieldValueEqual_func(field,Year_txt(), order.StorageInfo_Year,"");
			
			break;
		}
	}
}
