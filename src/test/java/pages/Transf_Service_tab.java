package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Services_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;


public class Transf_Service_tab extends PageObjects{
	
	
	
	public Transf_Service_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Srv_tab_xpath = "//a[contains(@href, 'transferee-services')]";
	private  String Edit_btn_xpath= "//a[contains(@id, 'icon-panel-edit')]";
	private  String HHG_opt_xpath= "//h3[contains(text(),'Moving services')]/../..//label[contains(text(),'Household goods')]/..//input[@type = 'checkbox']";
	private  String Morgt_opt_xpath= "//h3[contains(text(),'Destination services')]/../..//label[contains(text(),'Mortgage')]/..//input[@type = 'checkbox']";
	private  String HP_opt_xpath= "//h3[contains(text(),'Destination services')]/../..//label[contains(text(),'Home purchase')]/..//input[@type = 'checkbox']";
	private  String HS_opt_xpath= "//h3[contains(text(),'Origin services')]/../..//label[contains(text(),'Home sale')]/..//input[@type = 'checkbox']";
	private  String TQ_opt_xpath= "//h3[contains(text(),'Destination services')]/../..//label[contains(text(),'Temporary quarter')]/..//input[@type = 'checkbox']";
	
	private  String Appraisal_opt_xpath= "//h3[contains(text(),'Destination services')]/../..//label[contains(text(),'Appraisal')]/..//input[@type = 'checkbox']";
	private  String HomeBuyout_opt_xpath= "//h3[contains(text(),'Destination services')]/../..//label[contains(text(),'Home buyout')]/..//input[@type = 'checkbox']";
	private  String InventoryManage_opt_xpath= "//h3[contains(text(),'Destination services')]/../..//label[contains(text(),'Inventory management')]/..//input[@type = 'checkbox']";
	private  String HomeInspect_opt_xpath= "//h3[contains(text(),'Destination services')]/../..//label[contains(text(),'Home inspection')]/..//input[@type = 'checkbox']";
	
	private  String ExpManage_opt_xpath= "//h3[contains(text(),'Expense services')]/../..//label[contains(text(),'Expense management')]/..//input[@type = 'checkbox']";
	
	private  String Update_btn_xpath= "//button[@data-control = 'update']";
	private  String Cancel_btn_xpath= "//button[@data-control = 'cancel-update']";


	
	
	//DEFINE ELEMENTS:
	
	public  WebElement Appraisal_opt() {
		  return GetElement(Appraisal_opt_xpath);
		 }
	
	public  WebElement HomeBuyout_opt() {
		  return GetElement(HomeBuyout_opt_xpath);
		 }
	
	public  WebElement InventoryManage_opt() {
		  return GetElement(InventoryManage_opt_xpath);
		 }
	
	public  WebElement HomeInspect_opt() {
		  return GetElement(HomeInspect_opt_xpath);
		 }
	public  WebElement Srv_tab() {
	  return GetElement(Srv_tab_xpath);
	 }
	public  WebElement Edit_btn() {
	  return GetElement(Edit_btn_xpath);
	 }
	public  WebElement HHG_opt() {
	  return GetElement(HHG_opt_xpath);
	 }
	public  WebElement Morgt_opt() {
	  return GetElement(Morgt_opt_xpath);
	 }
	public  WebElement HP_opt() {
	  return GetElement(HP_opt_xpath);
	 }
	public  WebElement HS_opt() {
	  return GetElement(HS_opt_xpath);
	 }
	public  WebElement TQ_opt() {
	  return GetElement(TQ_opt_xpath);
	 }
	
	public  WebElement ExpManage_opt() {
		  return GetElement(ExpManage_opt_xpath);
	}
	
	public  WebElement Update_btn() {
	  return GetElement(Update_btn_xpath);
	 }
	public  WebElement Cancel_btn() {
	  return GetElement(Cancel_btn_xpath);
	 }

	
/////////================================METHOD
public  void VerifyServiceInfoViewMode_func(List<Services_Info> serviceslist)
{
	new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

	//Wait_For_ElementDisplay(Edit_btn());

/*
	String result_str_1 = Boolean.toString(HHG_opt().isSelected());
	System.out.println("result_str_1: "+result_str_1);
	String result_str_2 = Boolean.toString(Morgt_opt().isSelected());
	System.out.println("result_str_2:"+result_str_2);
	String result_str_3 = Boolean.toString(HP_opt().isSelected());
	System.out.println("result_str_3: "+result_str_3);
	String result_str_4 = Boolean.toString(HS_opt().isSelected());
	System.out.println("result_str_4: "+result_str_4);
	String result_str_5 = Boolean.toString(TQ_opt().isSelected());
	System.out.println("result_str_5: "+result_str_5);
*/
	int count = serviceslist.size();
	for(int i =0;i<count;i++)
	{
		String serivce_tmp =serviceslist.get(i).Type;
		String status = Boolean.toString(serviceslist.get(i).Status);

		switch(serivce_tmp)
		{
			case "HouseHoldGood":
				VerifyFieldValueEqual_func(serivce_tmp,HHG_opt(),status,"View Mode");
				break;
			case "Mortgage":
				VerifyFieldValueEqual_func(serivce_tmp,Morgt_opt(),status,"View Mode");
				break;

			case "HomePurchase":
				VerifyFieldValueEqual_func(serivce_tmp,HP_opt(),status,"View Mode");
				break;

			case "HomeSale":
				VerifyFieldValueEqual_func(serivce_tmp, HS_opt(),status,"View Mode");
				break;

			case "TemporaryQuarter":
				VerifyFieldValueEqual_func(serivce_tmp,TQ_opt(),status,"View Mode");
				break;
		}//end switch
	}//end for

	}
	
	//EDIT SERVICE TAB
	public  void EditService_func(List<Services_Info> serviceslist)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
				
		//	Wait_For_ElementDisplay(Srv_tab());
			
			Optimize_ElementClick(Srv_tab());

			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

		//	Wait_For_ElementDisplay(Edit_btn());

			Optimize_ElementClick(Edit_btn());
			
			ScrollElementtoViewPort_func(HS_opt());
			
			int count = serviceslist.size();
			
			
			for(int i = 0;i<count;i++)
			{
				Select_Service_Option(serviceslist.get(i).Type);
				
			}//end for
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Optimize_ElementClick(Update_btn());

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When update Service Tab.\n",false);
			

		
	}//end void
	private  void Select_Service_Option(String type)
	{
		switch(type)
		{
			case "Household goods":
				
				if(HHG_opt().isSelected()==false)
					Optimize_ElementClick(HHG_opt());
				
				break;
				
			case "Mortgage":
				if(Morgt_opt().isSelected()==false)
					Optimize_ElementClick(Morgt_opt());
				
				break;

			case "Home purchase":
				if(HP_opt().isSelected()==false)
					Optimize_ElementClick(HP_opt());
				
				break;

			case "Home sale":
				if(HS_opt().isSelected()==false)
					Optimize_ElementClick(HS_opt());
				
				break;

			case "Temporary quarter":
				if(TQ_opt().isSelected()==false)
					Optimize_ElementClick(TQ_opt());
				
				break;
				
			case "Home inspection":
				if(HomeInspect_opt().isSelected()==false)
					Optimize_ElementClick(HomeInspect_opt());
				
				break;
				
			case "Appraisal":
				if(Appraisal_opt().isSelected()==false)
					Optimize_ElementClick(Appraisal_opt());
				
				break;
				
			case "Home buyout":
				if(HomeBuyout_opt().isSelected()==false)
					Optimize_ElementClick(HomeBuyout_opt());
				
				break;
				
			case "Inventory management":
				if(InventoryManage_opt().isSelected()==false)
					Optimize_ElementClick(InventoryManage_opt());
				
				break;
				
			case "Expense management":
				if(ExpManage_opt().isSelected()==false)
					Optimize_ElementClick(ExpManage_opt());
				
				break;
		}//end switch

	}

	public  void Turn_Off_Service_func(String service_type)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Srv_tab());

		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

	//	Wait_For_ElementDisplay(Edit_btn());

		Optimize_ElementClick(Edit_btn());
		
		ScrollElementtoViewPort_func(HS_opt());
		
		Select_Service_Option(service_type);
	
	Update_btn().click();

	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	String msg = Messages_Notification.notification_msg().getText();

	String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
	System.out.println(msg);
	if(msg ==expected_msg)
	{
		TestConfigs.glb_TCStatus = false;

		TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Service - Info update.Msg: '"+msg+"'.\n";
		//Assert.fail(TestConfigs.glb_TCFailedMessage)
	}
	}

}
