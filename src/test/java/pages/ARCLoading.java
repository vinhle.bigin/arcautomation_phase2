package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import configuration.DriverConfig.DriverBase;


public class ARCLoading extends PageObjects{
	public ARCLoading(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Loading_icon_xpath = "//div[@class ='page-loader arc-auto arc-loading-plugin']";
	private  String LoadingArc_xpath = "div[contains(@class, 'arc-loading-page-loader loader-fullpage')]";

	public  WebElement Loading_icon() {
		return GetElement(Loading_icon_xpath);
	
	}
	
	
	
	public  WebElement LoadingArcLogo() {
		return GetElement(LoadingArc_xpath);
	}

	public  void Wait_Util_FinishedARClogoLoading()
	{
		
		//wait_For_PageLoad();
		//	Wait_For_ElementDisplay(LoadingArcLogo());
		
		
			if(LoadingArcLogo()!=null)
			{
				System.out.println("Wait For the ARC Logo is disappeared.\n");
				Wait_Ultil_ElementNotDisplay(LoadingArc_xpath);
			}
	}
	
	public  void Wait_Util_FinishedPageLoading()
	{
		
		wait_For_PageLoad();
		//Wait_For_ElementDisplay(Loading_icon());
		
		
			if(Loading_icon()!=null)
			{
				System.out.println("Wait For the Data Loading is finished.\n");
				Wait_Ultil_ElementNotDisplay(Loading_icon_xpath);
			}
	}

}
