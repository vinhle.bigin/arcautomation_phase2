package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Transferee_Info;
import businessObjects.UserAccount_Info;
import configuration.DriverConfig.DriverBase;

public class Transf_Account_tab extends PageObjects{
	

	
	public Transf_Account_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private  String Heading_tab_xpath = "//a[@href='#transferee-account']";
	
	private  String Role_ddl_xpath = "//select[@placeholder='Role']";
	private  String UserName_txt_xpath = "//div[@org-placeholder='Username']//input";
	private  String Password_txt_xpath = "//div[@org-placeholder='Password']//input";
	private  String Active_toggle_xpath = "//div[@placeholder='Active']//input";//div[@class='switch__touch']";
	private  String IsAllow_DocCreate_opt_xpath = "//label[contains(text(),'Create Documents')]/..//label[contains(text(),'Allow')]/../input";
	private  String IsDeny_DocCreate_opt_xpath = "//label[contains(text(),'Create Documents')]/..//label[contains(text(),'Deny')]/../input";
	private  String IsRole_DocCreate_opt_xpath = "//label[contains(text(),'Create Documents')]/..//label[contains(text(),'As Role Defined')]/../input";
	private  String IsAllow_DocEdit_opt_xpath = "//label[contains(text(),'Edit Documents')]/..//label[contains(text(),'Allow')]/../input";
	private  String IsDeny_DocEdit_opt_xpath = "//label[contains(text(),'Edit Documents')]/..//label[contains(text(),'Deny')]/../input";
	private  String IsRole_DocEdit_opt_xpath = "//label[contains(text(),'Edit Documents')]/..//label[contains(text(),'As Role Defined')]/../input";
	
	private  String Edit_btn_xpath= "//a[@id = 'icon-panel-transferee-account']";
	private  String Update_btn_xpath = "//button[@data-control='update']";
	private  String Cancel_btn_xpath = "//button[@data-control='cancel-update']";
	private  String Permission_arrow_xpath = "//div[@class='permission-wrapper m-b-xxl']//i[@class='icon-arrow-down']";
	
	private  String Active_toggle_value_xpath = "//div[@placeholder='Active']/../span";
	
	

	public  WebElement Active_toggle_value() {
		  return GetElement(Active_toggle_value_xpath);
		 }
	
	public  WebElement Permission_arrow() {
		  return GetElement(Permission_arrow_xpath);
		 }
	
	public  WebElement Heading_tab() {
		  return GetElement(Heading_tab_xpath);
		 }
	
	public  WebElement Role_ddl() {
	  return GetElement(Role_ddl_xpath);
	 }
	
public  WebElement UserName_txt() {
	  return GetElement(UserName_txt_xpath);
	 }

public  WebElement Password_txt() {
	  return GetElement(Password_txt_xpath);
	 }

public  WebElement Active_toggle() {
	  return GetElement(Active_toggle_xpath);
	 }

public  WebElement IsAllow_DocCreate_opt() {
	  return GetElement(IsAllow_DocCreate_opt_xpath);
	 }

public  WebElement IsDeny_DocCreate_opt() {
	  return GetElement(IsDeny_DocCreate_opt_xpath);
	 }

public  WebElement IsRole_DocCreate_opt() {
	  return GetElement(IsRole_DocCreate_opt_xpath);
	 }

public  WebElement IsAllow_DocEdit_opt() {
	  return GetElement(IsAllow_DocEdit_opt_xpath);
	 }

public  WebElement IsDeny_DocEdit_opt() {
	  return GetElement(IsDeny_DocEdit_opt_xpath);
	 }

public  WebElement IsRole_DocEdit_opt() {
	  return GetElement(IsRole_DocEdit_opt_xpath);
	 }


public  WebElement Edit_btn() {
	  return GetElement(Edit_btn_xpath);
	 }

public  WebElement Update_btn() {
	  return GetElement(Update_btn_xpath);
	 }
public  WebElement Cancel_btn() {
	  return GetElement(Cancel_btn_xpath);
	 }



//=====================METHODS

public  void Update_AccountInfo_func(UserAccount_Info account_info, String button)
{
	
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		ARCLoading ARCLoading = new ARCLoading(driverbase);
	
		ARCLoading.Wait_Util_FinishedARClogoLoading();
		
		ARCLoading.Wait_Util_FinishedPageLoading();
				
		new Transferees_page(driverbase).GotoTransDetailsPage(account_info.FullName);
		
		Optimize_ElementClick(Heading_tab());
			
	//	Wait_For_ElementDisplay(Edit_btn());
		
		ARCLoading.Wait_Util_FinishedARClogoLoading();
		
		Optimize_ElementClick(Edit_btn());
	
	//	Wait_For_ElementDisplay(Update_btn());
		
		//Role Dropdown
	String note_str = "Transf Account Tab - Role field";
	SelectDropdownItem(Role_ddl(), account_info.role, note_str);
	
	//User Name
	ClearThenEnterValueToField(UserName_txt(),account_info.username);
	
	//Password
	ClearThenEnterValueToField(Password_txt(),account_info.password);
	
	//Active Toggle
	String is_active = GetFieldText_func(Active_toggle_value());
	
	if(!is_active.equals(account_info.acc_Active_Status))
	{
		Optimize_ElementClick(Active_toggle());
		//Active_toggle().click();
	}
	
	//Create Doc permission
	switch (account_info.permision_CreateDoc) {
	case "Allow":
		
		Optimize_ElementClick(IsAllow_DocCreate_opt());
		//IsAllow_DocCreate_opt().click();
		
		break;
		
	case "Deny":
		Optimize_ElementClick(IsDeny_DocCreate_opt());
		
	//	IsDeny_DocCreate_opt().click();
		
		break;
	
	default:
		
		Optimize_ElementClick(IsRole_DocCreate_opt());
	//	IsRole_DocCreate_opt().click();
		
		break;
	}//end switch
	
	
	//Edit Doc permission
	switch (account_info.permision_EditDoc) {
		case "Allow":
			Optimize_ElementClick(IsAllow_DocEdit_opt());
		//	IsAllow_DocEdit_opt().click();
			
			break;
			
		case "Deny":
			Optimize_ElementClick(IsDeny_DocEdit_opt());
	//		IsDeny_DocEdit_opt().click();
			
			break;
		
		default:
			Optimize_ElementClick(IsRole_DocEdit_opt());
	//		IsRole_DocEdit_opt().click();
			
			break;
		}//end switch
	
		
		if(button.equals("submit")||button.equals("Submit"))
		{
			
			Optimize_ElementClick(Update_btn());
			//Update_btn().click();
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,"Unsuccessfull OrgOff - Info update",false);
		}
		
	
	
}//end void


public  void Verify_AccountInfo_ViewMode_func(Transferee_Info transf,String note)
{
	
	new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
	
	Wait_For_ElementEnable(Role_ddl());
	
	//Role Dropdown
	String fieldname_str = "Role_ddl";
	
	
	VerifyFieldValueEqual_func(fieldname_str, Role_ddl(), transf.role, note);
	
	//User Name
	fieldname_str = "UserName_txt";

	VerifyFieldValueEqual_func(fieldname_str, UserName_txt(), transf.username, note);
	
	//Password
	
	fieldname_str = "Active_toggle";
	VerifyFieldValueEqual_func(fieldname_str, Active_toggle_value(),transf.acc_Active_Status, note);
	
	
	if(IsDeny_DocCreate_opt()==null)
	{
		Permission_arrow().click();
	}
	
	//Create Doc permission
	switch (transf.permision_CreateDoc) {
	case "Allow":
		
	fieldname_str = "IsAllow_DocCreate_opt";
	
	VerifyFieldValueEqual_func(fieldname_str, IsAllow_DocCreate_opt(), "true", note);
		
		break;
		
	case "Deny":
			
		fieldname_str = "IsDeny_DocCreate_opt";
		
		VerifyFieldValueEqual_func(fieldname_str, IsDeny_DocCreate_opt(), "true", note);
		
		break;
	
	default:
		fieldname_str = "IsRole_DocCreate_opt";
		
		VerifyFieldValueEqual_func(fieldname_str, IsRole_DocCreate_opt(), "true", note);	
		
		break;
	}//end switch
	
	
	//Edit Doc permission
		switch (transf.permision_EditDoc) {
		case "Allow":
			
		fieldname_str = "IsAllow_DocEdit_opt";
		
		VerifyFieldValueEqual_func(fieldname_str, IsAllow_DocEdit_opt(), "true", note);	
			
		
			
			break;
			
		case "Deny":
			
			fieldname_str = "IsDeny_DocEdit_opt";
			
			VerifyFieldValueEqual_func(fieldname_str, IsDeny_DocEdit_opt(), "true", note);	
			
			
			break;
		
		default:
			fieldname_str = "IsRole_DocEdit_opt";
			
			VerifyFieldValueEqual_func(fieldname_str, IsRole_DocEdit_opt(), "true", note);	
			
			
			break;
		}//end switch
	
	
}//end void


}
