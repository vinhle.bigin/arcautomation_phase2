package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;

public class Order_BillStorage_section extends PageObjects{
	
	public Order_BillStorage_section(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String BillStorage_Edit_btn_xpath = "//a[@id='icon-hhg-order-billingstorage-panel']";
	private  String BillStorage_Update_btn_xpath = "//div[@id='hhg-order-billingstorage-panel']//button[@data-control='update']";
	private  String BillStorage_Cancel_btn_xpath = "//div[@id='hhg-order-billingstorage-panel']//button[@data-control='cancel-update']";

	private  String ActualDate_txt_xpath = "//div[@org-placeholder='Actual Delivery Date']//input";
	
	private  String StartDate_txt_xpath = "//div[@org-placeholder='Start Billing Period']//input";
	
	private  String EndDate_txt_xpath = "//div[@org-placeholder='End Billing Period']//input";
	
	private  String SupplierDays_txt_xpath = "//div[@org-placeholder='# of days supplier']//input";
	
	private  String TotalDays_txt_xpath = "//div[@org-placeholder='Total # of days']//input";
	
	private  String TotalMonths_txt_xpath = "//div[@org-placeholder='Total # of months']//input";
	
	private  String ZipCode_txt_xpath = "//div[@org-placeholder='Facility Zip Code']//input";
	
	
	
	public  WebElement BillStorage_Edit_btn() {
	  	return GetElement(BillStorage_Edit_btn_xpath);
	 }
	
	public  WebElement BillStorage_Update_btn() {
	  	return GetElement(BillStorage_Update_btn_xpath);
	 }
	
	public  WebElement BillStorage_Cancel_btn() {
	  	return GetElement(BillStorage_Cancel_btn_xpath);
	 }
	
	public  WebElement StartDate_txt() {
	  	return GetElement(StartDate_txt_xpath);
	 }
	
	public  WebElement ActualDate_txt() {
	  	return GetElement(ActualDate_txt_xpath);
	 }
	
	public  WebElement EndDate_txt() {
	  	return GetElement(EndDate_txt_xpath);
	 }
	
	public  WebElement SupplierDays_txt() {
	  	return GetElement(SupplierDays_txt_xpath);
	 }
	
	public  WebElement TotalDays_txt() {
	  	return GetElement(TotalDays_txt_xpath);
	 }
	
	public  WebElement TotalMonths_txt() {
	  	return GetElement(TotalMonths_txt_xpath);
	 }
	
	public  WebElement ZipCode_txt() {
	  	return GetElement(ZipCode_txt_xpath);
	 }
	
	//==================================================METHOD
	
	public  void Update_BillStorage_Section_func(Service_Order_Info order, String button)
	{
		 Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		 
		if (order.ServiceType.contains("EXT") == true || order.ServiceType.contains("SIT") == true)
		{
			Optimize_ElementClick(BillStorage_Edit_btn());
			
			Optimize_ElementSendkey(ActualDate_txt(), order.BillStorage_ActualDate);
			
			ActualDate_txt().sendKeys(Keys.ENTER);
			
			Optimize_ElementSendkey(StartDate_txt(), order.BillStorage_StartDate);
			
			StartDate_txt().sendKeys(Keys.ENTER);
			
			Optimize_ElementSendkey(EndDate_txt(), order.BillStorage_EndDate);
			
			EndDate_txt().sendKeys(Keys.ENTER);
			
			ClearThenEnterValueToField(ZipCode_txt(), order.BillStorage_ZipCode);
			
			switch(order.ServiceType)
			{
			case "SIT" : 

				ClearThenEnterValueToField(SupplierDays_txt(), order.BillStorage_SupplierDays);
				
				ClearThenEnterValueToField(TotalDays_txt(), order.BillStorage_TotalDays);
				break;

			default : 
				ClearThenEnterValueToField(TotalMonths_txt(), order.BillStorage_SupplierDays);
				break;
			}
			
			if(button.equals("Submit")||button.equals("submit"))
			{
				Optimize_ElementClick(BillStorage_Update_btn());
				
				Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Shipment Info",false);
				
				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			}
		}
	}
	
	//==================================================METHOD VERIFY
	
	public  void Verify_BillStorage_section_func(Service_Order_Info order)
	{
		if (order.ServiceType.contains("EXT") == true || order.ServiceType.contains("SIT") == true)
		{
			String field = "";
			
			field = "Actual Delivery Date";
			
			VerifyFieldValueEqual_func(field,ActualDate_txt(), order.BillStorage_ActualDate,"");
			
			field = "Start Billing Period";
			
			VerifyFieldValueEqual_func(field, StartDate_txt(), order.BillStorage_StartDate,"");
			
			field = "End Billing Period";
			
			VerifyFieldValueEqual_func(field, EndDate_txt(), order.BillStorage_EndDate,"");
			
			field = "Facility Zip code";
			
			VerifyFieldValueEqual_func(field, ZipCode_txt(), order.BillStorage_ZipCode,"");
			
			switch(order.ServiceType)
			{
			case "SIT" : 
			
				field = "# Of Days Supplier Is Billing ARC";
				
				VerifyFieldValueEqual_func(field, SupplierDays_txt(), order.BillStorage_SupplierDays,"");
				
				field = "Total # Of Days";
				
				VerifyFieldValueEqual_func(field, TotalDays_txt(), order.BillStorage_TotalDays,"");
			
			break;

			default : 
				
				field = "Total # Of Months";
				
				VerifyFieldValueEqual_func(field, TotalMonths_txt(), order.BillStorage_SupplierDays,"");
				
				break;
			
			
			}
		}
		
	}
}
