package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Policy_Info;
import configuration.DriverConfig.DriverBase;

public class NewPolicy_mdal extends PageObjects {

	public NewPolicy_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String actived_detail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";

	private String PolicyName_txt_xpath = actived_detail_mdal_xpath + "//input[@placeholder = 'Enter policy name']";

	private String Descr_txt_xpath = actived_detail_mdal_xpath + "//textarea[@placeholder = 'Description']";

	private String Client_ddl_xpath = actived_detail_mdal_xpath
			+ "//div[@class='col-md-6']//div[contains(@class,'dropdown-main')]/ul";

	private String ClientInput_ddl_xpath = actived_detail_mdal_xpath
			+ "//div[@class='col-md-6']//input[@placeholder='Select client']";

	private String Submit_btn_xpath = actived_detail_mdal_xpath + "//button[@type='submit']";

	private String Cancel_btn_xpath = actived_detail_mdal_xpath + "//button[@data-dismiss='modal']";

	private String ActiveDate_txt_xpath = actived_detail_mdal_xpath + "//input[@id = 'create-policy-active-date']";

	private String ExpirationDate_txt_xpath = actived_detail_mdal_xpath
			+ "//input[@id = 'create-policy-expiration-date']";

	private String Clientvlue_txt_xpath = actived_detail_mdal_xpath + "//span[@class='thumb']";

	private String Clientslect_txt_xpath = actived_detail_mdal_xpath + "//select[@placeholder='Client']";

	public WebElement PolicyName_txt() {
		return GetElement(PolicyName_txt_xpath);
	}

	public WebElement Descr_txt() {
		return GetElement(Descr_txt_xpath);
	}

	public WebElement Client_ddl() {
		return GetElement(Client_ddl_xpath);
	}

	public WebElement Submit_btn() {
		return GetElement(Submit_btn_xpath);
	}

	public WebElement Cancel_btn() {
		return GetElement(Cancel_btn_xpath);
	}

	public WebElement ActiveDate() {
		return GetElement(ActiveDate_txt_xpath);
	}

	public WebElement ExpirationDate() {
		return GetElement(ExpirationDate_txt_xpath);
	}

	public WebElement Client_txt() {
		return GetElement(ClientInput_ddl_xpath);
	}

	public WebElement Clientvlue() {
		return GetElement(Clientvlue_txt_xpath);
	}

	public WebElement Clientslect() {
		return GetElement(Clientslect_txt_xpath);
	}

	// METHOD

	public void FillInfoModal_func(String mode, Policy_Info newpolicy, String submit_button) {

		ARCLoading ARCLoading = new ARCLoading(driverbase);

		//ARCLoading.Wait_Util_FinishedPageLoading();

		// Wait_For_ElementDisplay(CompanyName_txt());

		// Company Name
		ClearThenEnterValueToField(PolicyName_txt(), newpolicy.PolicyName);

		// Client Type
		String note_str = "Create Client - Client Dropdown.";
		
		//On Update Mode , the field is disabled
		if(mode.equals("Create"))

		{
			Clientvlue().click();

			ClearThenEnterValueToField(Client_txt(), newpolicy.client.CompanyName);

			SelectDropdownItem(Client_ddl(), newpolicy.client.CompanyName,"when select the clientcompany");
			
			Optimize_ElementSendkey(ExpirationDate(), newpolicy.EnterDate);
		}
		
		// Description
		ClearThenEnterValueToField(Descr_txt(), newpolicy.Description);

		if (submit_button.equals("Submit") || submit_button.equals("submit")) {

			Submit_btn().click();

			wait_For_PageLoad();
		}

	}

}
