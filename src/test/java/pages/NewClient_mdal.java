package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Client_Info;
import configuration.DriverConfig.DriverBase;

public class NewClient_mdal extends PageObjects{
	


	public NewClient_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}




	private  String actived_detail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";
	
	private  String CompanyName_txt_xpath = actived_detail_mdal_xpath+ "//input[@placeholder = 'Company Name']";

	private  String Descr_txt_xpath= actived_detail_mdal_xpath + "//div[@org-placeholder='Description']//textarea";
	
	private  String Client_type_ddl_xpath = actived_detail_mdal_xpath + "//select[@placeholder ='Client Type']";
	
	private  String State_ddl_xpath= actived_detail_mdal_xpath + "//select[@placeholder ='State']";
	
	private  String Zip_txt_xpath= actived_detail_mdal_xpath+ "//input[@placeholder = 'Zip']";
	
	private  String City_txt_xpath= actived_detail_mdal_xpath + "//input[@placeholder = 'City']";
	
	private  String Submit_btn_xpath= actived_detail_mdal_xpath + "//button[@type='submit']";
	
	private  String Cancel_btn_xpath= actived_detail_mdal_xpath+ "//button[@data-dismiss='modal']";
	
	
	
	
	 public  WebElement CompanyName_txt() {
		  return GetElement(CompanyName_txt_xpath);
		 }
	 
	 public  WebElement Descr_txt() {
		  return GetElement(Descr_txt_xpath);
		 }
	 
	 public  WebElement Client_type_ddl() {
		  return GetElement(Client_type_ddl_xpath);
		 }
	
	
	 public  WebElement Zip_txt() {
		  return GetElement(Zip_txt_xpath);
		 }
	 
	public  WebElement City_txt() {
		  return GetElement(City_txt_xpath);
		 }
	
	 public  WebElement Submit_btn() {
		  return GetElement(Submit_btn_xpath);
		 }
	
	 public  WebElement Cancel_btn() {
		  return GetElement(Cancel_btn_xpath);
		 }
	
	public  WebElement State_slect() {
	  return GetElement(State_ddl_xpath);
	 }



	
	//METHOD

		public  void FillInfoModal_func(Client_Info newclient,String submit_button)
		{
		
			

			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			ARCLoading.Wait_Util_FinishedPageLoading();

			//Wait_For_ElementDisplay(CompanyName_txt());

			//Company Name	
			ClearThenEnterValueToField(CompanyName_txt(), newclient.CompanyName);

			//Client Type
			String note_str = "Create Client - Client Type Dropdown.";
			
			SelectDropdownItem(Client_type_ddl(), newclient.Type,note_str);
			
			//Description
			ClearThenEnterValueToField(Descr_txt(), newclient.Description);

			note_str = "Create Client - State Dropdown.";
			SelectDropdownItem(State_slect(), newclient.AddrInfo.get(0).State,note_str);

			ClearThenEnterValueToField(City_txt(), newclient.AddrInfo.get(0).City);

			if(submit_button.equals("Submit")||submit_button.equals("submit") )
			{
				//Optimize_ElementClick(null)

				Submit_btn().click();
				
//					Wait_For_ElementDisplay(Client_GenInfo_page.CompName_txt());
	
				wait_For_PageLoad();
				
			}
		
		}//end void

}
