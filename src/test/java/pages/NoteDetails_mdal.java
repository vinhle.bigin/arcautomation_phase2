package pages;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import baseClasses.PageObjects;
import businessObjects.Mail_Info;
import businessObjects.Note_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class NoteDetails_mdal extends PageObjects{
	
	
	public NoteDetails_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}



	private  String Detail_mdal_xpath = "//div[(contains(@id,'modal') or contains(@id,'client-note-edit')) and contains(@style,'display: block')]";
	private  String ServiceType_box_xpath= Detail_mdal_xpath + "//div[@org-placeholder ='Services']";
	private  String ServiceType_txt_xpath = Detail_mdal_xpath +"//input[@placeholder ='services']";
	private  String ServiceType_slect_xpath= ServiceType_box_xpath +"//ul[@class='b__multi__select__list']";
	private  String ServiceType_arrow_xpath = ServiceType_box_xpath + "//div[contains(@class,'iconC')]";
	
	private  String Contact_slect_xpath= Detail_mdal_xpath + "//div[@org-placeholder = 'Contact']//select";
	private  String Start_txt_xpath= Detail_mdal_xpath + "//input[@id = 'clientActivityCreate_from_date']";
	private  String End_txt_xpath= Detail_mdal_xpath + "//input[@id = 'clientActivityCreate_to_date']";
	private  String Title_txt_xpath= Detail_mdal_xpath + "//input[contains(@id,'subject')] | //h5[text()='Subject:']/..//div";
	
	private  String ClientName_box_xpath = Detail_mdal_xpath+"//label[contains(text(),'Company Name')]/..//div[@class='selected single']";
	private  String ClientName_arrow_xpath = Detail_mdal_xpath+"//label[contains(text(),'Company Name')]/..//div[@class='iconC']";
	
	private  String VendorName_box_xpath = Detail_mdal_xpath+"//label[contains(text(),'Vendor Name')]/..//div[@class='selected single']";
	private  String VendorName_arrow_xpath = Detail_mdal_xpath+"//label[contains(text(),'Vendor Name')]//div[@class='iconC']";
	
	private  String ClientName_txt_xpath = Detail_mdal_xpath+ "//input[@placeholder='Company Name']";

	private  String VendorName_txt_xpath = Detail_mdal_xpath+ "//input[contains(@placeholder,'Vendor Name')]";

	private  String TransfName_txt_xpath = Detail_mdal_xpath+ "//input[contains(@placeholder,'Transferee')]";

	private  String ClientList_ddl_xpath = Detail_mdal_xpath+"//label[contains(text(),'Company Name')]/..//ul";

	private  String VendorList_ddl_xpath = Detail_mdal_xpath+"//label[contains(text(),'Contact')]/..//ul";

	private  String TransfList_ddl_xpath =  Detail_mdal_xpath+ "//div[@id = 'b-combobox-transferee']//ul";
	
	private  String TransfView_chkbox_xpath = Detail_mdal_xpath+ "//label[contains(text(),'Transferee View')]/..//input";
	
	private  String ClientView_chkbox_xpath = Detail_mdal_xpath+ "//label[contains(text(),'Client View')]/..//input";
	
	private  String VendorView_chkbox_xpath = Detail_mdal_xpath+ "//label[contains(text(),'Vendor View')]/..//input";

	//===============VIEWMODE for special element:
	private  String ServiceName_vlue_xpath = Detail_mdal_xpath+ "//select[@placeholder='Service']";
	
	private  String ClientName_vlue_xpath = Detail_mdal_xpath+ "//select[@placeholder='Company Name']";
	
	private  String VendorName_vlue_xpath = Detail_mdal_xpath+ "//select[@placeholder='Vendor Name']";
	
	private  String Transferee_vlue_xpath = Detail_mdal_xpath+ "//select[@placeholder='Transferee']";
	
	
	
	
	private  String Submit_btn_xpath= Detail_mdal_xpath + "//div[@class = 'modal-footer']//button[@type = 'submit']";
	private  String Cancel_btn_xpath= Detail_mdal_xpath + "//div[@class = 'modal-footer']//button[contains(@data-control, 'cancel')]";
	private  String Attach_lnk_xpath= Detail_mdal_xpath + "//div[@id = 'new-activity-attachment']//div[@class = 'content dz-clickable']";
	
	private  String Edit_btn_xpath = "//div[@class ='modal-header']//a[@data-original-title = 'Edit']";

	
	
	public  WebElement ServiceName_vlue(){

		return GetElement(ServiceName_vlue_xpath);
	}
	
	public  WebElement Transferee_vlue(){

		return GetElement(Transferee_vlue_xpath);
	}
	
	public  WebElement ClientName_box(){

		return GetElement(ClientName_box_xpath);
	}
	
	public  WebElement VendorName_box(){

		return GetElement(VendorName_box_xpath);
	}
	
	public  WebElement ClientName_arrow(){

		return GetElement(ClientName_arrow_xpath);
	}
	
	public  WebElement VendorName_arrow(){

		return GetElement(VendorName_arrow_xpath);
	}
	
	public  WebElement VendorName_vlue(){

		return GetElement(VendorName_vlue_xpath);
	}

	public  WebElement ClientName_vlue(){
		return GetElement(ClientName_vlue_xpath);
		
	}

	public  WebElement TransfName_txt(){
		return GetElement(TransfName_txt_xpath);
		
	}

	public  WebElement TransfList_ddl(){
		return GetElement(TransfList_ddl_xpath);
	}

	public  WebElement ClientName_txt(){
		return GetElement(ClientName_txt_xpath);
		
	}

	public  WebElement VendorName_txt(){
		return GetElement(VendorName_txt_xpath);
		
	}

	public  WebElement VendorList_ddl(){
		return GetElement(VendorList_ddl_xpath);
		}

	public  WebElement ClientList_ddl(){
		return GetElement(ClientList_ddl_xpath);
		
	}
	
	public  WebElement ServiceType_txt() {
		  return GetElement(ServiceType_txt_xpath);
		 }
	public  WebElement ServiceType_box() {
		  return GetElement(ServiceType_box_xpath);
		 }

	public  WebElement ServiceType_slect() {
		  return GetElement(ServiceType_slect_xpath);
		 }
	
	public  WebElement ServiceType_arrow() {
		  return GetElement(ServiceType_arrow_xpath);
		 }
	
	public  WebElement Contact_slect() {
		  return GetElement(Contact_slect_xpath);
		 }
	public  WebElement Start_txt() {
		  return GetElement(Start_txt_xpath);
		 }
	
	public  WebElement End_txt() {
		  return GetElement(End_txt_xpath);
		 }
	
	public  WebElement Title_txt() {
		  return GetElement(Title_txt_xpath);
		 }
	
	public  WebElement Submit_btn() {
		  return GetElement(Submit_btn_xpath);
		 }
	
	public  WebElement Cancel_btn() {
		  return GetElement(Cancel_btn_xpath);
		 }
	
	public  WebElement Attach_lnk() {
		  return GetElement(Attach_lnk_xpath);
		 }

	
	public  WebElement Edit_btn() {
		  return GetElement(Edit_btn_xpath);
		 }
	
	public  WebElement TransfView_chkbox() {
		  return GetElement(TransfView_chkbox_xpath);
		 }
	
	public  WebElement ClientView_chkbox() {
		  return GetElement(ClientView_chkbox_xpath);
		 }
	
	public  WebElement VendorView_chkbox() {
		  return GetElement(VendorView_chkbox_xpath);
		 }
	
	public  void Verify_Note_Modal_Not_Editable(Note_Info note_info, String note_str)
	{
		
		//=================INPUT NOTE INFO
		String fieldname_str = "";
		
		
		switch(note_info.AssignType)
		{
		case "Transferee":
			
			
			if(Is_Field_Display(TransfName_txt())==true)
			{
							
				fieldname_str = "Note Detail - TransfList Dropdown.";
			
				Verify_Field_Not_Editable_func(fieldname_str,TransfList_ddl(), note_info.Assignee, note_str);
			}
			
			break;
			
		default:
			
			if(note_info.AssignType.equals("Client"))
			{
				fieldname_str = "Note Detail - ClientList Dropdown.";
				
				Verify_Field_Not_Editable_func(fieldname_str,ClientList_ddl(), note_info.Assignee, note_str);
				
			}
			else
			{
				fieldname_str = "Note Detail - VendorList Dropdown.";
				
				Verify_Field_Not_Editable_func(fieldname_str,VendorList_ddl(), note_info.Assignee, note_str);
			}
			
			//Contact dropdown
			fieldname_str = "Note Details - Contact Dropdown.";
			
			Verify_Field_Not_Editable_func(fieldname_str,Contact_slect(), note_info.Contact,note_str);
			
            break;
			
		}//end switch

		
		//Title
		
		Verify_Field_Not_Editable_func(fieldname_str, Title_txt(), note_info.Title, note_str);
		//=======INPUT FOR CONTENT
		Optimize_ElementSendkey(Modal_TinyContentBox(),note_info.Content);
		
		VerifyFieldValueNotEqual_func(fieldname_str, Modal_TinyContentBox(), note_info.Content, "When verify the field not editable.\n");

		
		
	}//end void
	
	public  void Fill_Info_To_Modal_func(Note_Info note,String button)
	{
		try {
			Messages_Notification Messages_Notification = new Messages_Notification(this.driverbase);

			
			
			//=================INPUT NOTE INFO
			String note_str="";
			
			switch(note.AssignType) {
			case "Transferee":
				
				
				if(Is_Field_Display(TransfName_txt())==true)
					
				{
								
					note_str = "Activity Detail - TransfList Dropdown.";
					
					TransfName_txt().sendKeys(note.Assignee);

					SelectDropdownItem(TransfList_ddl(), note.Assignee, note_str);
					
					Title_txt().sendKeys(Keys.TAB);
				}
				
				Select_Service_On_Service_Field_func(note.Service);
				
				break;
				
			default:
				
				/*if(note.AssignType.equals("Client"))
				{
					note_str = "Activity Detail - ClientList Dropdown.";
					
					//Optimize_ElementClick(ClientName_box());
					
					//Wait_For_ElementDisplay(ClientName_txt());
					
					//ClearThenEnterValueToField(ClientName_txt(), note.Assignee);
					 
					//SelectDropdownItem(ClientList_ddl(), note.Assignee, note_str);
					
				}
				
				else
				{
					note_str = "Activity Detail - VendorList Dropdown.";
					
					//VendorName_box().click();
					
					//Wait_For_ElementDisplay(VendorName_txt());
					
					//ClearThenEnterValueToField(VendorName_txt(), note.Assignee);
					
					//SelectDropdownItem(VendorList_ddl(), note.Assignee, note_str);
				}*/
				
				//Contact dropdown
				note_str = "Note Details - Contact Dropdown.";
				
				SelectDropdownItem(Contact_slect(), note.Contact,note_str);
				
	            break;
				
			}//end switch

			//Check on checkbox transferee View
			if(TransfView_chkbox()!=null)
				Optimize_ElementClick(TransfView_chkbox());
			
			//Check on checkbox client View
			if(ClientView_chkbox()!=null)
			Optimize_ElementClick(ClientView_chkbox());
			
			//Check on checkbox client View
			if(VendorView_chkbox()!=null)
			Optimize_ElementClick(VendorView_chkbox());
			
			//Title
			ClearThenEnterValueToField(Title_txt(), note.Title);

			//=======INPUT FOR CONTENT

			Actions actions = new Actions(driverbase.GetDriver());

			Action action_key = actions.sendKeys(Title_txt(), Keys.TAB).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(note.Content).build();

			action_key.perform();

			Thread.sleep(2000);

			if(button=="Submit")
				Optimize_ElementClick(Submit_btn());
			
			Thread.sleep(1000);

			if( Messages_Notification.notification_msg()!=null)
			{
				String msg = Messages_Notification.notification_msg().getText();

				String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
				//println(msg)
				if(msg ==expected_msg)
				{
					TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Note Creation.Msg: '"+msg+"'.\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}//end void
	
	private  void Select_Service_On_Service_Field_func(String service_name)
	{
		//check the service is selected before
		boolean is_selected = IsSelected_service_func(service_name);
		if(is_selected==false)
		{
			ServiceType_box().click();
		
			ClearThenEnterValueToField(ServiceType_txt(), service_name);
			
			String note_str = "Note modal - Service Type Dropdown";
			
			SelectDropdownItem(ServiceType_slect(), service_name, note_str);
		}
	}//end void
	
	private  boolean IsSelected_service_func(String service_name)
	{
		List<WebElement> selected_items = null;
		try {
			selected_items = ServiceType_box().findElements(By.xpath("//div[@class ='selected']/span[@class='thumb']"));
		}
		catch(NullPointerException e)
		{
			TestConfigs.glb_TCFailedMessage+="Bug: Service field must be multiple dropdown on Edit mode.\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
		
		int count = selected_items.size();
		for(int i=0;i<count;i++)
		{
			String temp_str = selected_items.get(i).getText();
			if(temp_str.equals(service_name))
			{
				return true;
			}
				
		}//end for
		
		return false;
	}//end void
	
	
	public  void VerifyModalDetailsInfo_func(Note_Info note)
	{
		String field_str = "";
		String area  = "Note";
	
		Wait_For_ElementDisplay(Detail_mdal_xpath);
		
		if(note.AssignType.equals("Transferee")&&!note.Service.equals("General"))
		{
			field_str = "Service_ddl";
			
			VerifyFieldValueEqual_func(area+field_str, ServiceName_vlue(), note.Service,"");
			
			
	}//end if
		else if(!note.Contact.equals(""))
		{
			field_str = "Contact_ddl";
			VerifyFieldValueEqual_func(area+field_str, Contact_slect(), note.Contact,"");
		}//end else if

		field_str = "Title_txt";
		VerifyFieldValueEqual_func(area+field_str, Title_txt(), note.Title,"");

		field_str = "Note Content";
		VerifyFieldValueEqual_func(area+field_str, Modal_TinyContentBox(), note.Content, "");
	

	}//end void



	public  void VerifyModalDetailsInfo_func(Mail_Info mail_info)
	{
		
		
		String field_str = "";
		String area  = "Note";

		Wait_For_ElementEnable(Edit_btn());
		

		if(mail_info.RecipientType.equals("Transferee")&&mail_info.ServiceType.equals("General"))
		{
			field_str = "Service_ddl";
			VerifyFieldValueEqual_func(area+field_str, ServiceType_slect(), mail_info.ServiceType,"");
		}//end if

		else if(mail_info.RecipientType.equals("Client"))
		{
			field_str = "Contact_ddl";
			VerifyFieldValueEqual_func(area+field_str, Contact_slect(), mail_info.EmailAddr,"");
		}//end else if

		//	field_str = "To_txt"
		//	VerifyFieldValueEqual_func(area+field_str, Title_txt(), mail_info.Subject)

		field_str = "Title_txt";
		VerifyFieldValueEqual_func(area+field_str, Title_txt(), mail_info.Subject,"");


		field_str = "Note Content";
		VerifyFieldValueEqual_func(area+field_str, Modal_TinyContentBox(), mail_info.Content, "");
		
		
	}//end void
	
}
