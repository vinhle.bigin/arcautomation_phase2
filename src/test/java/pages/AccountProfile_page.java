package pages;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Transferee_Info;
import configuration.DriverConfig.DriverBase;


public class AccountProfile_page extends PageObjects{
	
	public AccountProfile_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Topmenu_UserName_xpath = "//div[@class='top-menu']//span[@id='nav-fullname']";
	
	private  String Topmenu_Profile_xpath = "//div[@class='top-menu']//a[contains(@href,'profile')]";
	
	private  String Topmenu_SignOut_xpath = "//div[@class='top-menu']//a[contains(@href,'logout')]";
	
	//User Profile
	private  String UserProfile_Edit_btn_xpath= "//a[@id ='icon-user-edit-profile']";
	private  String LinkedProfile_txt_xpath= "//div[@org-placeholder = 'LinkedIn Profile']//input";
	private  String FacebookProfile_txt_xpath= "//div[@org-placeholder = 'Facebook Profile']//input";
	private  String UserProfile_Update_btn_xpath= "//div[@id ='user-edit-profile']//button[@data-control = 'update']";
	private  String UserProfile_Cancel_btn_xpath= "//div[@id ='user-edit-profile']//button[@data-control = 'cancel-update']";
	private  String Fname_txt_xpath = "//div[@org-placeholder = 'First Name']//input";
	private  String Lname_txt_xpath = "//div[@org-placeholder = 'Last Name']//input";
	private  String Jobtle_txt_xpath= "//div[@org-placeholder = 'Job Title']//input";
	private  String Fname_Sidebar_txt_xpath = "//span[@id = 'sidebar-profile-fullname']";
	
	//Time Zone Setting
	private  String TimeZone_Edit_btn_xpath = "//a[@id = 'icon-user-edit-timezone']";
	private  String TimeZone_Country_ddl_xpath = "//div[@org-placeholder = 'Old Password']//input";
	private  String TimeZone_Update_btn_xpath= "//div[@id ='user-edit-timezone']//button[@data-control = 'update']";
	private  String TimeZone_Cancel_btn_xpath= "//div[@id ='user-edit-timezone']//button[@data-control = 'cancel-update']";
	
	//Password
	private  String Password_Edit_btn_xpath = "//a[@id = 'icon-user-edit-password']";
	private  String Old_Password_txt_xpath = "//div[@org-placeholder = 'Old Password']//input";
	private  String New_Password_txt_xpath = "//div[@org-placeholder = 'New Password']//input";
	private  String Retype_Password_txt_xpath = "//div[@org-placeholder = 'Retype Password']//input";
	private  String Password_Update_btn_xpath= "//div[@id ='user-edit-password']//button[@data-control = 'update']";
	private  String Password_Cancel_btn_xpath= "//div[@id ='user-edit-password']//button[@data-control = 'cancel-update']";

	//Phones & Emails
	private  String PhoneEmail_Edit_btn_xpath = "//a[@id = 'icon-user-email-phone']";
	private  String PhoneType_ddl_xpath = "//div[@class ='mobile-wrap']//select[@placeholder = 'Type']";
	private  String Phone_txt_xpath = "//div[@class ='mobile-wrap']//input[@placeholder = 'Number']";
	private  String EmailType_ddl_xpath = "//div[@class ='email-wrap extension-fields']//select[@placeholder = 'Type']";
	private  String Email_txt_xpath = "//div[@class ='email-wrap extension-fields']//input[@placeholder = 'E-mail']";
	private  String PhoneEmail_Update_btn_xpath= "//div[@id ='user-email-phone']//button[@data-control = 'update']";
	private  String PhoneEmail_Cancel_btn_xpath= "//div[@id ='user-email-phone']//button[@data-control = 'cancel-update']";
	private  String PhoneList_xpath = "//div[@class = 'mobile-wrap']";
	private  String EMailList_xpath = "//div[@class='email-wrap extension-fields']";
	
	
	public  WebElement PhoneEmail_Edit_btn() {
		  return GetElement(PhoneEmail_Edit_btn_xpath);
		 }
	
	public  WebElement PhoneEmail_Update_btn() {
		  return GetElement(PhoneEmail_Update_btn_xpath);
		 }
	
	public  WebElement PhoneEmail_Cancel_btn() {
		  return GetElement(PhoneEmail_Cancel_btn_xpath);
		 }
	
	public  WebElement PhoneList() {
		  return GetElement(PhoneList_xpath);
		 }
	
	public  WebElement TimeZone_Edit_btn() {
		  return GetElement(TimeZone_Edit_btn_xpath);
		 }
	
	public  WebElement TimeZone_Country_ddl() {
		  return GetElement(TimeZone_Country_ddl_xpath);
		 }
	
	public  WebElement TimeZone_Update_btn() {
		  return GetElement(TimeZone_Update_btn_xpath);
		 }
	
	public  WebElement TimeZone_Cancel_btn() {
		  return GetElement(TimeZone_Cancel_btn_xpath);
		 }
	
	public  WebElement Password_Edit_btn() {
		  return GetElement(Password_Edit_btn_xpath);
		 }
	
	public  WebElement Old_Password_txt() {
		  return GetElement(Old_Password_txt_xpath);
		 }
	
	public  WebElement New_Password_txt() {
		  return GetElement(New_Password_txt_xpath);
		 }
	
	public  WebElement Retype_Password_txt() {
		  return GetElement(Retype_Password_txt_xpath);
		 }
	
	public  WebElement Password_Update_btn() {
		  return GetElement(Password_Update_btn_xpath);
		 }
	
	public  WebElement Password_Cancel_btn() {
		  return GetElement(Password_Cancel_btn_xpath);
		 }

	public  WebElement Fname_Sidebar_txt() {
		  return GetElement(Fname_Sidebar_txt_xpath);
		 }
	
	public  WebElement UserProfile_Edit_btn() {
		  return GetElement(UserProfile_Edit_btn_xpath);
		 }
	
	public  WebElement UserProfile_Update_btn() {
		  return GetElement(UserProfile_Update_btn_xpath);
		 }
	
	public  WebElement UserProfile_Cancel_btn() {
		  return GetElement(UserProfile_Cancel_btn_xpath);
		 }
	
	public  WebElement LinkedProfile_txt() {
		  return GetElement(LinkedProfile_txt_xpath);
		 }
	
	public  WebElement FacebookProfile_txt() {
		  return GetElement(FacebookProfile_txt_xpath);
		 }
	
	public  WebElement Fname_txt() {
		  return GetElement(Fname_txt_xpath);
		 }
	
	public  WebElement Lname_txt() {
		  return GetElement(Lname_txt_xpath);
		 }
	
	public  WebElement Jobtle_txt() {
		  return GetElement(Jobtle_txt_xpath);
		 }
	
	public  WebElement Topmenu_UserName(){
		return GetElement(Topmenu_UserName_xpath);
}
	
	public  WebElement Topmenu_Profile(){
		return GetElement(Topmenu_Profile_xpath);
}
	
	public  WebElement Topmenu_SignOut(){
		return GetElement(Topmenu_SignOut_xpath);
}
	
	public  WebElement PhoneType_ddl() {
		  return GetElement(PhoneType_ddl_xpath);
		 }
	
	public  WebElement Phone_txt() {
		  return GetElement(Phone_txt_xpath);
		 }
	
	public  WebElement EmailType_ddl() {
		  return GetElement(EmailType_ddl_xpath);
		 }
	
	public  WebElement Email_txt() {
		  return GetElement(Email_txt_xpath);
		 }
	
	public  WebElement EMailList() {
		  return GetElement(EMailList_xpath);
		 }
	
	public  WebElement PhoneType_ddl(int index) {
		try{
			String e_xpath = ".//select[@placeholder = 'Type']";
			
			List<WebElement> temp_element = PhoneList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null;}
	}

	public  WebElement Phone_txt(int index) {
		try{
			String e_xpath = ".//input[@type = 'tel']";
			
			List<WebElement> temp_element = PhoneList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null;}
	}

	public  WebElement PhonePrefer_opt(int index) {
		try{
			String e_xpath = ".//input[@type = 'checkbox']";
		
			List<WebElement> temp_element = PhoneList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null;}
	}
	
	public  WebElement EmailType_ddl(int index) {
		try{
			String e_xpath = ".//select[@placeholder = 'Type']";
		
			List<WebElement> temp_element = EMailList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null;}
	}

	public  WebElement EmailAddr_txt(int index) {
		try{
			String e_xpath = ".//div[@org-placeholder='E-mail']//input";
			
			List<WebElement> temp_element = EMailList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index);
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null;}
	}
	
	
	
	
	public  void log_out_from_Portal_func()
	{
		CloseBrowserTab();
		
		Optimize_ElementClick(Topmenu_UserName());
		
		Optimize_ElementClick(Topmenu_SignOut());
		
	}
	
	public  void Go_To_Profile_func()
	{
		Optimize_ElementClick(Topmenu_Profile());
	}
	
	//============================================================UPDATE METHODS

	public  void Update_User_Profile_Section(Transferee_Info user, String button)
	{
		Optimize_ElementClick(UserProfile_Edit_btn());

		ClearThenEnterValueToField(Fname_txt(), user.firstname);

		ClearThenEnterValueToField(Lname_txt(), user.lastname);
		
		ClearThenEnterValueToField(Jobtle_txt(), user.Jobtitle);

//		ClearThenEnterValueToField(LinkedProfile_txt(), user.Linkedin_Profile);
//		
//		ClearThenEnterValueToField(FacebookProfile_txt(), user.FB_Profile);
		
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(UserProfile_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in User Profile",false);
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
		}
	}
	
	public  void Update_TimeZone_Section(Transferee_Info user, String button)
	{
		Optimize_ElementClick(TimeZone_Edit_btn());

		ClearThenEnterValueToField(TimeZone_Country_ddl(), user.TimeZone_country);
		
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(TimeZone_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Time Zone.\n",false);
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
			
			
		}
	}
	
	public  void Update_Password_Section(Transferee_Info user, String button)
	{
		Optimize_ElementClick(Password_Edit_btn());

		ClearThenEnterValueToField(Old_Password_txt(), user.password);

		ClearThenEnterValueToField(New_Password_txt(), user.new_password);
		
		ClearThenEnterValueToField(Retype_Password_txt(), user.retype_password);
		
		
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Password_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Password",false);
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
		}
		else if(button.equals("Cancel")||button.equals("cancel"))
		{
			Optimize_ElementClick(Password_Cancel_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Canceling new info in Password",false);
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
		}
	}
	
	public  void Update_Phones_Emails_Section(Transferee_Info user, String button)
	{
		Optimize_ElementClick(PhoneEmail_Edit_btn());
		
		String note_str = "";
	
		note_str = "Transferee Details - Phone Type Dropdown.";
	
		SelectDropdownItem(PhoneType_ddl(), user.PhoneList.get(0).Type,note_str);
	
		ClearThenEnterValueToField(Phone_txt(), user.PhoneList.get(0).Numb);
	
		//Email info
		note_str = "Transferee Details - Mail Type Dropdown.";
		
		ScrollElementtoViewPort_func(EmailType_ddl());
		
		SelectDropdownItem(EmailType_ddl(), user.MailList.get(0).Type,note_str);
	
		ClearThenEnterValueToField(Email_txt(), user.MailList.get(0).EmailAddr);
		
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(PhoneEmail_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Phones Emails",false);
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
		}
	}
	
	//============================================================CANCEL METHOD
	
	public  void Cancel_Password_Updates(Transferee_Info user, String button)
	{
		Optimize_ElementClick(Password_Edit_btn());

		ClearThenEnterValueToField(Old_Password_txt(), user.password);

		ClearThenEnterValueToField(New_Password_txt(), user.new_password);
		
		ClearThenEnterValueToField(Retype_Password_txt(), user.retype_password);
		
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		if(button.equals("Cancel")||button.equals("cancel"))
		{
			Optimize_ElementClick(Password_Cancel_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Canceling new info in Password",false);
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();
		}
	}
	
	//===============================================================VERIFY METHODS
	
	public  void Verify_User_Profile(Transferee_Info user)
	{
		String field = "";
		
		field = "First Name";
		
		VerifyFieldValueEqual_func(field, Fname_txt(), user.firstname, "");
		
		field = "Last Name";
		
		VerifyFieldValueEqual_func(field, Lname_txt(), user.lastname, "");
		
		field = "Job Title";
		
		VerifyFieldValueEqual_func(field, Jobtle_txt(), user.Jobtitle, "");
		
//		field = "Linkedin Profile";
//		
//		VerifyFieldValueEqual_func(field, LinkedProfile_txt(), user.Linkedin_Profile, "");
//		
//		field = "Facebook Profile";
//		
//		VerifyFieldValueEqual_func(field, FacebookProfile_txt(), user.FB_Profile, "");
		
	}
	
	public  void Verify_Fullname_in_Sidebar(Transferee_Info user)
	{
		String field = "";
		
		field = "Full Name in Sidebar";
		
		VerifyFieldValueEqual_func(field, Fname_Sidebar_txt(), user.FullName, "");
	}
	
	public  void Verify_Fullname_in_TopMenu(Transferee_Info user)
	{
		String field = "";
		
		field = "Full Name in Top Menu";
		
		VerifyFieldValueEqual_func(field, Topmenu_UserName(), user.FullName, "");
	}

	public  void Verify_TimeZone_Section(Transferee_Info user)
	{
		String field = "";
		
		field = "Time Zone";
		
		VerifyFieldValueEqual_func(field, TimeZone_Country_ddl(), user.TimeZone_country, "");
	}
	
	public  void Verify_PhoneEmail_Section(Transferee_Info user)
	{
		VerifyFieldValueEqual_func("PhoneType_ddl",PhoneType_ddl(0),user.PhoneList.get(0).Type," - ViewMode");
	
		VerifyFieldValueEqual_func("Phone_txt",Phone_txt(0),user.PhoneList.get(0).Numb," - ViewMode");
	
		VerifyFieldValueEqual_func("EmailType_ddl",EmailType_ddl(0),user.MailList.get(0).Type," - ViewMode");
	
		VerifyFieldValueEqual_func("EmailAddr_txt",EmailAddr_txt(0),user.MailList.get(0).EmailAddr," - ViewMode");
		
	}

}
