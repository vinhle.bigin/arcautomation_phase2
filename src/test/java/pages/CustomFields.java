package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.CustomField_Info;
import configuration.DriverConfig.DriverBase;


public class CustomFields extends PageObjects{
	
	public CustomFields(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	//public List<CustomField> CustomFields_list;

	public WebElement Group_Edit_btn(String group_name)
	{
		String xpath = "//h3[text()='Custom Field - "+group_name+"']/..//a[@data-original-title='Edit']";
		
	  return GetElement(xpath );
	}
	
	public WebElement Group_Update_btn(String group_name)
	{
		String xpath = "//h3[text()='Custom Field - "+group_name+"']/../..//button[@data-control='update']";
		
	  return GetElement(xpath );
	}
	
	
	public  WebElement CustomField(CustomField_Info field) {
	
		String xpath = "//div[@org-placeholder='"+field.FieldName+"']//";
					
		switch (field.ControlType)
		{
		case "Textbox": case "Checkbox": case "Radio":
			xpath = xpath+ "input";
			break;
			
		case "Dropdown":
			xpath = xpath+ "select";
			break;
			
		
		}
	
	  return GetElement(xpath );
	 }
		
	
	
	
	
	//private void Verify_Single_CustomField(WebEleme)
	
	
	public void Update_Custom_Field_func(List<CustomField_Info> customfields_list,String button)
	{
		WebElement web_element;
		
		for(CustomField_Info current_field_info: customfields_list)
		{
			WebElement group_element = Group_Edit_btn(current_field_info.custom_group.Name);
			
			if(group_element!=null)
			{
				group_element.click();
			}//end if
			
			web_element = CustomField(current_field_info);
			
			for(String cur_expec_value:current_field_info.ValueList)
				{
					switch(current_field_info.ControlType)
					{
						case "Dropdown":
							SelectDropdownItem(web_element, cur_expec_value, current_field_info.FieldName);
							break;
	
						case "Multiselect":
							
							break;
	
						case "Checkbox": case "Radio":
							
							if(!GetFieldText_func(web_element).equals(cur_expec_value))
								Optimize_ElementClick(web_element);
							break;
							
						
						default: 
							ClearThenEnterValueToField(web_element, cur_expec_value);
							break;
							
						}//and switch
								
				}//end for cur_expec_value
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Group_Update_btn(current_field_info.custom_group.Name));
			
		}
		
		
		}//end for CustomField_Info
	}
	
	public void VerifyCustomFields(List<CustomField_Info> customfields_list)
	{
		String group_area = "";
		
		String field_name = "";
		
		String note = "- View Mode";
		
		
		WebElement web_element;
				
		for(CustomField_Info current_field_info: customfields_list)
		{
			
			group_area = current_field_info.custom_group.Name;
			
			field_name = current_field_info.FieldName;
			
			web_element = CustomField(current_field_info);
			
			for(String cur_expec_value:current_field_info.ValueList)
			{
				VerifyFieldValueEqual_func(group_area+field_name, web_element, cur_expec_value,note);
				
			}//end for cur_expec_value
						
		}//end for CustomField_Info
					
	}

}
