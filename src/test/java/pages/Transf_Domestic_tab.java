package pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import businessObjects.Contact_Info;
import businessObjects.HPService_Info;
import businessObjects.HSService_Info;
import businessObjects.MGService_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Services_Info;
import businessObjects.TQService_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;


public class Transf_Domestic_tab extends PageObjects{

	
	public Transf_Domestic_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String actived_detail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";
	private  String Service_Name_title_xpath = "//h2[@class='title-service-tab']";
	private  String Domestic_tab_xpath = "//li[contains(@class,'dropdown domestic-tab')]//a[@role='button']";
	private  String VendorType_ddl_xpath= "//div[@org-placeholder ='Select Type']//select";
	
	private  String VendorList_ddl_xpath= "//div[@class='filter-heading form-inline default-form']//ul";
	private  String Vendor_txt_xpath= "//div[@class='filter-heading form-inline default-form']//div[@class='input-control-wrap']//input";
	private  String Add_btn_xpath= "//div[@class='filter-heading form-inline default-form']//button[contains(@id,'add-vendor')]";
	private  String CustomSearch_btn_xpath= "//div[@class='filter-heading form-inline default-form']//button[contains(@data-toggle,'modal')]";
	private  String Edit_btn_xpath= "//a[@data-original-title ='Edit']";
	
	private  String Menu_List_xpath = "//ul[@class = 'dropdown-menu services-dropdown-menu']";
	
	private  String Vendor_Contact_tbl_xpath = "//div[@id='transferee-domestic-table']//table/tbody";
	
	private  String Select_Provider_option_xpath = "//div[@class='check-items-primary-ban checked']";
	
	private  String Remove_btn_xpath = "//i[@class='icon-close']";
	
	private  String ModalConfirm_Delete_btn_xpath = "//div[@id='confirm-modal']//button[contains(text(),'Delete')]";
	
	private  String CTomSearch_CtactType_ddl_xpath = actived_detail_mdal_xpath+"//select[@placeholder='Vendor Contact Type']";
	
	private  String CTomSearch_VendorName_txt_xpath = actived_detail_mdal_xpath+"//input[@placeholder ='Vendor Name']";
	
	private  String CTomSearch_FirstName_txt_xpath = actived_detail_mdal_xpath+"//input[@placeholder ='First Name']";
	
	private  String CTomSearch_Search_btn_xpath = actived_detail_mdal_xpath+"//button[@type='submit']";
	
	private  String CTomSearch_ContactList_tbl_xpath = actived_detail_mdal_xpath+"//div[contains(@id,'table-vendor-custom-search-service')]//table";
	
	private  String CTomSearch_AddContact_btn_xpath = "//button[contains(text(),'Add')]";
	
	private  String ServiceStatus_List_tbl_xpath = "//a[@id='service-status']";
	
	private  String ServiceStatus_slect_xpath = "//select[@class='form-control input-sm']//option[contains(text(),'<Status_name>')]";
	
	

	
	public  WebElement CTomSearch_CtactType_ddl() {
		return GetElement(CTomSearch_CtactType_ddl_xpath);
	 }
	
	public  WebElement CTomSearch_FirstName_txt() {
		return GetElement(CTomSearch_FirstName_txt_xpath);
	 }
	
	public  WebElement CTomSearch_VendorName_txt() {
		return GetElement(CTomSearch_VendorName_txt_xpath);
	 }
	
	public  WebElement CTomSearch_Search_btn() {
		return GetElement(CTomSearch_Search_btn_xpath);
	 }
	
	public  WebElement CTomSearch_ContactList_tbl() {
		return GetElement(CTomSearch_ContactList_tbl_xpath);
	 }
	
	public  WebElement CTomSearch_AddContact_btn(String ctactFullName) {
		
		return GetElementInTable(ctactFullName, CTomSearch_ContactList_tbl(), CTomSearch_AddContact_btn_xpath);
		
	 }
	
		
	public  WebElement VendorContact_Remove_btn(String vendor_name) {
		
		return GetElementInTable(vendor_name, Vendor_Contact_tbl(), Remove_btn_xpath);

	}
	

	public  WebElement Service_Name_title() {
		return GetElement(Service_Name_title_xpath);
	 }
	public  WebElement ServiceStatus_List_tbl() {
		return GetElement(ServiceStatus_List_tbl_xpath);
	}
	
	public  WebElement ServiceStatus_slect() {
		return GetElement(ServiceStatus_slect_xpath);
	}
	
	
	public  WebElement Remove_btn() {
		return GetElement(Remove_btn_xpath);
	 }
	
	public  WebElement ModalConfirm_Delete_btn() {
		return GetElement(ModalConfirm_Delete_btn_xpath);
	 }
	
	public  WebElement Vendor_Contact_tbl() {
		  return GetElement(Vendor_Contact_tbl_xpath);
		 }
	
	public  WebElement Select_Provider_option(String ctact_FirstName) {
		
		return GetElementInTable(ctact_FirstName, Vendor_Contact_tbl(), Select_Provider_option_xpath);
		 
		 }
	
	public  WebElement Domestic_tab() {
	  return GetElement(Domestic_tab_xpath);
	 }
	
	public  WebElement VendorType_ddl() {
	  return GetElement(VendorType_ddl_xpath);
	 }
	public  WebElement VendorList_ddl() {
	  return GetElement(VendorList_ddl_xpath);
	 }
	public  WebElement Vendor_txt() {
	  return GetElement(Vendor_txt_xpath);
	 }
	public  WebElement Add_btn() {
	  return GetElement(Add_btn_xpath);
	 }
	public  WebElement CustomSearch_btn() {
	  return GetElement(CustomSearch_btn_xpath);
	 }
	public  WebElement Edit_btn() {
	  return GetElement(Edit_btn_xpath);
	 }
	public  WebElement Menu_List() {
	  return GetElement(Menu_List_xpath);
	 }

	
/////////================================METHOD
	
	
private  void Select_Vendor_Type_on_ServiceDetails_func(Contact_Info contact)
{
		// Van Lines, Realtor - Broker, Title and closing management company, Lender, Interim Housing, Realtor - Agent
		String service_type = "";
		switch (contact.ServiceType) {
		
			case "Title and closing management company":
				service_type =  "Title/Closing";
				break;
				
			case " Realtor-Broker":
				service_type =  "Relo Director";
				break;
					
			case "Realtor - Agent":
				service_type =  "Agent";
				break;
				
			case "Appraisal Management":
				service_type =  "Appraisal Management";
				break;
			
			case "Appraiser":
				service_type =  "Appraiser";
				break;
				
				
		}//end switch
		if(!service_type.equals(""))
			
		SelectDropdownItem(VendorType_ddl(), service_type, "");
		
		
}


public  void Select_VendorContact_On_CustomSearch_func(Contact_Info ctact_info)
{
	Optimize_ElementClick(CustomSearch_btn());
	
	String note_str = "CTomSearch_CtactType_ddl";
	
	SelectDropdownItem(CTomSearch_CtactType_ddl(), ctact_info.ServiceType, note_str);
	
	ClearThenEnterValueToField(CTomSearch_FirstName_txt(), ctact_info.firstname);
	
	Optimize_ElementClick(CTomSearch_Search_btn());
	
	Optimize_ElementClick(CTomSearch_AddContact_btn(ctact_info.FullName));
	
	Wait_Ultil_ElementNotDisplay(CTomSearch_CtactType_ddl_xpath);
		
}

public  void Set_VendorContact_As_Provider_func(String ctact_FirstName)
{
	try {
	
	Select_Provider_option(ctact_FirstName).click();
	
	Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public  void Select_VendorContact_On_VendorList_func(Contact_Info contact)
{
	
	try {
		
		Select_Vendor_Type_on_ServiceDetails_func(contact);
		
		String item_str = contact.VendorName+" - "+ contact.FullName;
		
		ClearThenEnterValueToField(Vendor_txt(), item_str);
		
		SelectDropdownItem(VendorList_ddl(), item_str, "");
		
		Optimize_ElementClick(Add_btn());
		
		Thread.sleep(6000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}





//METHOD FILL HS INFO
public  void UpdateHHGService_func(Service_Order_Info service,String button) {
	
	Optimize_ElementClick(Domestic_tab());
	
	new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		
	String note_str = "DomesticTab Details - SerVice List Dropdown.";
	
	SelectDropdownItem(Menu_List(),service.Type,note_str);
	
	//Transf_HHG_details.EditHHGInfo_func(service,"Submit");
}

//METHOD FILL HS INFO
public  void UpdateHSService_func(HSService_Info service,String button) {
	
	Transf_HomeSale_details Transf_HomeSale_details = new Transf_HomeSale_details(driverbase);
	
	Optimize_ElementClick(Domestic_tab());
	
	new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
	String note_str = "DomesticTab Details - SerVice List Dropdown.";
	
	SelectDropdownItem(Menu_List(),service.Type,note_str);
	
	Optimize_ElementClick(Transf_HomeSale_details.Edit_btn());
	
	Transf_HomeSale_details.FillHSInfo_func(service,"Submit");
}



//METHOD FILL HP INFO
public  void UpdateHPService_func(HPService_Info service,String button) {
	Optimize_ElementClick(Domestic_tab());
	
	new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		
	String note_str = "DomesticTab Details - SerVice List Dropdown.";
	SelectDropdownItem(Menu_List(),service.Type,note_str);
	
	new Transf_HomePurchase_details(driverbase).FillHPInfo_func(service,"Submit");
}

//METHOD FILL APPRAISAL PROPERTY INFO
public  void Update_ServiceDetails_Property_section_func(Address_Info Addr_info,String button) {
	
	new Transf_Appraisal_details(driverbase).UpdatePropertyInfo_func(Addr_info,"submit");
}



//METHOD FILL MG INFO
public  void UpdateMGService_func(MGService_Info service,String button) {
	Optimize_ElementClick(Domestic_tab());
	
	new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		
	String note_str = "DomesticTab Details - SerVice List Dropdown.";
	SelectDropdownItem(Menu_List(),service.Type,note_str);
	new Transf_Mortgage_details(driverbase).FillMortgageInfo_func(service,"Submit");
}



//METHOD FILL HGG INFO
public  void UpdateTQService_func(TQService_Info service,String button) {
	
	Optimize_ElementClick(Domestic_tab());
	
	new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		
	String note_str = "DomesticTab Details - SerVice List Dropdown.";
	
	SelectDropdownItem(Menu_List(),service.Type,note_str);
	
	new Transf_TQ_details(driverbase).FillTQInfo_func(service,"Submit");
}


public  void Go_To_Service_Details(String service_name)
{
	String 	upper_service_name = service_name.toUpperCase();
	
	if(Service_Name_title()==null || (Service_Name_title()!=null && !Service_Name_title().getText().equals(upper_service_name)))
	{
		Optimize_ElementClick(Domestic_tab());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		
		SelectDropdownItem(Menu_List(),service_name,"");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
//	Wait_For_ElementDisplay(VendorList_ddl());
}

//METHOD FILL TQ INFO
public  void Create_New_HHG_Service_Order(Service_Order_Info order)
{
	OrderCommonInfo_section OrderCommonInfo_section = new OrderCommonInfo_section(driverbase);
	
	if(OrderCommonInfo_section.NewOrder_btn()==null)
	
		Go_To_Service_Details(order.ServiceName);
	
	Optimize_ElementClick(OrderCommonInfo_section.NewOrder_btn());
	
	OrderCommonInfo_section.Fill_Modal_Value_func(order, "submit");
	
}

public  void Update_Existing_HHG_Order(Service_Order_Info order)
{
	if(!order.VendorName.equals(""))
	{
		
	}
}


public  void Remove_VendorContact_On_VendorList_func(String vendor_ctact)
{
	Optimize_ElementClick(VendorContact_Remove_btn(vendor_ctact));

	Optimize_ElementClick(ModalConfirm_Delete_btn());
}

//====================VERIFY METHODS
public  void Verify_Vendor_Selection_List_Is_Correct_func(String service_name, Contact_Info contact)
{
	Go_To_Service_Details(service_name);
	
	String item_str = contact.VendorName+" - "+ contact.FullName;
	
	Select_Vendor_Type_on_ServiceDetails_func(contact);
	
	ClearThenEnterValueToField(Vendor_txt(), item_str);
	
	VerifyItemExistDropdown(VendorList_ddl(), item_str);
}//end void


public  void Verify_Vendor_Not_Shown_Selection_List_func(String service_name, Contact_Info contact)
{
	Go_To_Service_Details(service_name);
	
	String item_str = contact.VendorName+" - "+ contact.FullName;
	
	Select_Vendor_Type_on_ServiceDetails_func(contact);
	
	ClearThenEnterValueToField(Vendor_txt(), item_str);
	
	VerifyItemNotExistDropdown(VendorList_ddl(), item_str);
}//END void

public  void VerifyServiceDisplayMenu_func(List<Services_Info> serviceslist)
{
	int size_tmp = serviceslist.size();
	for(int i=0;i<size_tmp;i++)
	{
		VerifyItemExistDropdown(Menu_List(), serviceslist.get(i).Type);
	}

}//end void


public  void Verify_Vendor_Contact_Shown_Correctly_AssignedList_func(Contact_Info ctact_info)
{

	boolean exist = false;
	
	List<String> list_str = GetTableRecordsPerPaging_func(Vendor_Contact_tbl());
	
	String expect_str = ctact_info.FullName+","+ctact_info.FullName+","+ctact_info.VendorName+","+ctact_info.PhoneList.get(0).Numb+","+ctact_info.email_info.EmailAddr+","+ctact_info.ServiceType+",";
	
	for(int i = 0;i <list_str.size();i++)
		if(		list_str.get(i).contains(ctact_info.FullName) && 
				list_str.get(i).contains(ctact_info.VendorName)&& 
				list_str.get(i).contains(ctact_info.PhoneList.get(0).Numb) &&
				list_str.get(i).contains(ctact_info.email_info.EmailAddr) && 
				list_str.get(i).contains(ctact_info.ServiceType)
			)
	{
		exist = true;
		break;
	}//end if

	if(exist==false)
	{
		TestConfigs.glb_TCStatus= false;
		TestConfigs.glb_TCFailedMessage+="Missing info in table - record.[Observed: "+list_str+" - Expected: "+expect_str+"] "+".]\n";
	}
	
}

public  void Verify_Vendor_Contact_Not_Shown_AssignedList_func(Contact_Info ctact_info)
{
	
	VerifyDataNotExistTable_func(Vendor_Contact_tbl(), ctact_info.FullName, "");
	
	
}


}
