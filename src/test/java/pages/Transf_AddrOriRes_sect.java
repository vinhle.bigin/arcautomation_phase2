package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import configuration.DriverConfig.DriverBase;


public class Transf_AddrOriRes_sect extends PageObjects{
	
	
	
	public Transf_AddrOriRes_sect(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String OrgRes_Edit_btn_xpath= "//a[@id = 'icon-edit-transferee-origin-residence-address']";
	private  String OrgRes_Update_btn_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//button[@data-control = 'update']";
	private  String OrgRes_Cancel_btn_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//button[@data-control = 'cancel-update']";
	private  String OrgRes_Addrs1_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter Address 1']//input";
	private  String OrgRes_Addrs2_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter Address 2']//input";
	private  String OrgRes_City_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter City']//input";
	private  String OrgRes_State_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter State']//select";
	private  String OrgRes_Zip_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter Zip']//input";
	private  String OrgRes_Country_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter Country']//select";
	private  String OrgRes_IsBilling_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][1]/div[1]//input";
	private  String OrgRes_IsShipping_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][1]/div[2]//input";
	private  String OrgRes_IsMailing_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][1]/div[3]//input";
	private  String OrgRes_IsPrimary_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][1]/div[4]//input";
	private  String OrgRes_IsSendMailPayment_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][2]/div[1]//input";
	private  String OrgRes_IsSendMailTaxDoc_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][2]/div[2]//input";
	private  String OrgRes_IsSendMailService_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][2]/div[3]//input";
	

	
	
	public  WebElement OrgRes_Edit_btn() {
		  return GetElement(OrgRes_Edit_btn_xpath);
		 }
	public  WebElement OrgRes_Update_btn() {
		  return GetElement(OrgRes_Update_btn_xpath);
		 }
	public  WebElement OrgRes_Cancel_btn() {
		  return GetElement(OrgRes_Cancel_btn_xpath);
		 }
	public  WebElement OrgRes_Addrs1() {
		  return GetElement(OrgRes_Addrs1_xpath);
		 }
	public  WebElement OrgRes_Addrs2() {
		  return GetElement(OrgRes_Addrs2_xpath);
		 }
	public  WebElement OrgRes_City() {
		  return GetElement(OrgRes_City_xpath);
		 }
	public  WebElement OrgRes_State() {
		  return GetElement(OrgRes_State_xpath);
		 }
	public  WebElement OrgRes_Zip() {
		  return GetElement(OrgRes_Zip_xpath);
		 }
	public  WebElement OrgRes_Country() {
		  return GetElement(OrgRes_Country_xpath);
		 }
	public  WebElement OrgRes_IsBilling() {
		  return GetElement(OrgRes_IsBilling_xpath);
		 }
	public  WebElement OrgRes_IsShipping() {
		  return GetElement(OrgRes_IsShipping_xpath);
		 }
	public  WebElement OrgRes_IsMailing() {
		  return GetElement(OrgRes_IsMailing_xpath);
		 }
	public  WebElement OrgRes_IsPrimary() {
		  return GetElement(OrgRes_IsPrimary_xpath);
		 }
	public  WebElement OrgRes_IsSendMailPayment() {
		  return GetElement(OrgRes_IsSendMailPayment_xpath);
		 }
	public  WebElement OrgRes_IsSendMailTaxDoc() {
		  return GetElement(OrgRes_IsSendMailTaxDoc_xpath);
		 }
	public  WebElement OrgRes_IsSendMailService() {
		  return GetElement(OrgRes_IsSendMailService_xpath);
		 }
	
	
	
	public  void Edit_TransfAddrInfo_func(Address_Info addr)
	{
		try
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
			
			Thread.sleep(3000);
		//	Wait_For_ElementDisplay(OrgRes_Edit_btn());

			Optimize_ElementClick(OrgRes_Edit_btn());

			ScrollElementtoViewPort_func(OrgRes_Addrs1());
			ClearThenEnterValueToField(OrgRes_Addrs1(), addr.Addr1);

			ClearThenEnterValueToField(OrgRes_Addrs2(), addr.Addr2);

			ClearThenEnterValueToField(OrgRes_City(), addr.City);

			ClearThenEnterValueToField(OrgRes_Zip(), addr.Zip);

			String note_str = "Transferee Org Res Address Tab - State Dropdown.";
			
			SelectDropdownItem(OrgRes_State(),  addr.State,note_str);

			 note_str = "Transferee Org Res Address Tab - State Dropdown.";
			
			SelectDropdownItem(OrgRes_Country(), addr.Country,note_str);

			//addr.Country = "Viet Nam"
			//ClearThenEnterValueToField()

			OrgRes_Update_btn().click();

			Thread.sleep(5000);

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(expected_msg,"Unsuccessfull OrgRes - Info update",false);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}//end void
	
	public  void VerifyInfoViewMode_func(Address_Info addr)
	{
		String area = "";
		String field_name = "";
		String mode = "View Mode";

		//WaitForElementNotDisplay(LandingPage.LoadingArc())

	

		///ORG RESIDENCE
		area = "Org Res - ";

		field_name = "OrgRes_Addrs1";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_Addrs1(),addr.Addr1, mode);

		field_name = "OrgRes_Addrs2";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_Addrs2(),addr.Addr2 , mode);

		field_name = "OrgRes_City";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_City(),addr.City , mode);

		field_name = "OrgRes_Zip";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_Zip(),addr.Zip , mode);

		field_name = "OrgRes_State";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_State(),addr.State , mode);

		field_name = "OrgRes_Country";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_Country(),addr.Country , mode);

		field_name = "OrgRes_IsBilling";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_IsBilling(),addr.IsAtri_Billing.toString() , mode);

		field_name = "OrgRes_IsShipping";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_IsShipping(),addr.IsAtri_Shipping.toString() , mode);

		field_name = "OrgRes_IsMailing";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_IsMailing(),addr.IsAtri_Mailing.toString() , mode);

		field_name = "OrgRes_IsPrimary";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_IsPrimary(),addr.IsAtri_Primary.toString() , mode);

		field_name = "OrgRes_IsSendMailPayment";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_IsSendMailPayment(),addr.IsSendMailPayment.toString() , mode);

		field_name = "OrgRes_IsSendMailService";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_IsSendMailService(),addr.IsSendMailServices.toString() , mode);

		field_name = "OrgRes_IsSendMailTaxDoc";
		VerifyFieldValueEqual_func(area+field_name ,OrgRes_IsSendMailTaxDoc(),addr.IsSendMailTaxDoc.toString() , mode);



		
	}

}
