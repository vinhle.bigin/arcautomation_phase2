package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Vendor_Info;
import configuration.DriverConfig.DriverBase;

public class NewVendor_mdal extends PageObjects{
	
	
	
	public NewVendor_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Name_txt_xpath = "//input[@placeholder = 'Enter vendor name']";
	
	private  String Code_txt_xpath = "//input[@placeholder = 'Enter vendor code']";
	
	private  String ContactType_slect_xpath = "//select[@placeholder = 'Vendor Contact Type']";
	
	private  String Website_txt_xpath = "//input[@placeholder = 'Enter website']";
	
	private  String Active_toggle_xpath = "//input[@id = 'new-vendor-is-active']";
	
	private  String Available_toggle_xpath = "//input[@id = 'new-vendor-is-available']";
	
	private  String International_toggle_xpath = "//input[@id = 'new-vendor-is-international']";
	
	private  String Submit_btn_xpath = "//button[@data-control = 'create']";
	
	private  String Cancel_btn_xpath = "//button[@data-dismiss='modal']";

	
	
	public  WebElement Name_txt() {
		  return GetElement(Name_txt_xpath);
		 }
	
	public  WebElement Code_txt() {
		  return GetElement(Code_txt_xpath);
		 }
	
	public  WebElement ContactType_slect() {
		  return GetElement(ContactType_slect_xpath);
		 }
	
	public  WebElement Website_txt() {
		  return GetElement(Website_txt_xpath);
		 }
	
	public  WebElement Active_toggle() {
		  return GetElement(Active_toggle_xpath);
		 }
	
	public  WebElement Available_toggle() {
		  return GetElement(Available_toggle_xpath);
		 }
	
	public  WebElement International_toggle() {
		  return GetElement(International_toggle_xpath);
		 }
	
	public  WebElement Submit_btn() {
		  return GetElement(Submit_btn_xpath);
		 }
	
	public  WebElement Cancel_btn() {
		  return GetElement(Cancel_btn_xpath);
		 }

	//METHOD

	public  void FillInfoModal_func(Vendor_Info newvendor,String submit_btn)
	{
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		ARCLoading.Wait_Util_FinishedPageLoading();

		//Wait_For_ElementDisplay(CompanyName_txt());

		//VEndor Name	
		ClearThenEnterValueToField(Name_txt(), newvendor.VendorName);

		//Contact Type
		String note_str = "Create Vendor - Vendor Contact Type Dropdown.";
		
		SelectDropdownItem(ContactType_slect(), newvendor.VendorContactType,note_str);
		
		//Vendor Code
		ClearThenEnterValueToField(Code_txt(), newvendor.VendorCode);

		ClearThenEnterValueToField(Website_txt(), newvendor.Website);

		if(submit_btn.equals("Submit")||submit_btn.equals("submit") )
		{
			//Optimize_ElementClick(null)

			Submit_btn().click();

		}
	}
}

