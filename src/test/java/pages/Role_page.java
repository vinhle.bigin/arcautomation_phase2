package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import configuration.DriverConfig.DriverBase;

public class Role_page extends PageObjects{

	public Role_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String Modules_ddl_xpath = "//select[@placeholder = 'Modules']";
	private  String RoleName_txt_xpath = "//input[@placeholder = 'Role Name']";
	private  String Description_txt_xpath = "//input[@placeholder = 'Description']";

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public  WebElement Modules_ddl() {
	  return GetElement(Modules_ddl_xpath);
	 }
	
	public  WebElement RoleName_txt() {
	  return GetElement(RoleName_txt_xpath);
	 }

	public  WebElement Description_txt() {
	  return GetElement(Description_txt_xpath);
	 }

	
}
