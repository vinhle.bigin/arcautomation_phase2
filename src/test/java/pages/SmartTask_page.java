package pages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import baseClasses.PageObjects;
import businessObjects.SmartTask_Action;
import businessObjects.SmartTask_Info;
import configuration.DriverConfig.DriverBase;

public class SmartTask_page extends PageObjects{
	
	public SmartTask_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private  String Search_Name_txt_xpath= "//input[@placeholder ='Smart Task Name']";
	private  String Search_btn_xpath= "//button[@id='search-smart-task-btn']";
	private  String New_btn_xpath= "//button[contains(text(),'New Smart Task')]";
	private  String SmartTaskList_tbl_xpath= "//table[@id='smart-task-table']/tbody";
	private  String Dtail_Name_txt_xpath= "//input[@placeholder='Name']";
	private  String Dtail_ServiceType_ddl_xpath= "//select[@placeholder='Service Type']";
	private  String Dtail_FieldChange_opt_xpath= "//label[contains(text(),'Field Changed')]/..//input";
	private  String Dtail_MailProcess_opt_xpath= "//label[contains(text(),'Email Processed')]/..//input";
	private  String Dtail_ActivityComplete_opt_xpath= "//label[contains(text(),'Activity Completed')]/..//input";
	private  String Dtail_FileCreate_opt_xpath= "//label[contains(text(),'File Created')]/..//input";
	private  String Dtail_EmailTemplate_ddl_xpath= "//form//select[contains(@placeholder,'Email Template')]";
	private  String Dtail_ActivityName_txt_xpath= "//form//input[contains(@placeholder,'Activity Name')]";
	private  String Dtail_CompareValue_txt_xpath= "//form//*[@placeholder='Value']";

	private  String Dtail_TriggerType_ddl_xpath= "//label[contains(text(),'Trigger Type')]/..//select[@placeholder ='Trigger Type']";
	private  String Dtail_Fieldname_ddl_xpath= "//form//select[contains(@placeholder,'Field Name')]";
	private  String Dtail_Operator_ddl_xpath= "//div[@class='form-group col-md-3 operator']//select[@placeholder ='Operator']";
	private  String Dtail_ConditionList_tbl_xpath= "";
	private  String Dtail_ActionType_ddl_xpath= "//select[@placeholder='Action Type']";
	private  String Dtail_ConditionAdd_btn_xpath= "//div[@class='col-md-2']/button";
	private  String Dtail_ActionAdd_btn_xpath= "//div[@class='add-action d-flex']/button";
	private  String Dtail_Submit_btn_xpath= "//div[@class='col-md-12']//button[contains(@type,'submit')]";
	private  String Dtail_Cancel_btn_xpath= "//div[@class='col-md-12']/button[text() = 'Cancel')]";
	private  String Dtail_ActEmailTemplate_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Using Template']";
	private  String Dtail_ActFieldList_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Update Value Of']";
	private  String Dtail_ActFieldValueTo_txt_xpath= "//div[@class='task-content']//input[@placeholder ='To']";
	private  String Dtail_ActActivityType_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Activity Type']";
	private  String Dtail_ActAssignTo_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Assigned To']";
	private  String Dtail_ActPriority_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Priority']";
	private  String Dtail_ActDays_txt_xpath= "//div[@class='task-content']//input[@placeholder ='Days']";
	private  String Dtail_ActDate_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Date']";
	private  String Dtail_ActActivityTitle_txt_xpath= "//div[@class='task-content']//input[@placeholder ='Title']";
	private  String Dtail_ActUpdate_btn_xpath= "//div[@class='task-btn-group']//button[@type='button'][contains(text(),'Update')]";
	private  String Dtail_ActCancel_btn_xpath= "//div[@class='task-content']//button[contains(text(),'Cancel')]";
	private  String Dtail_Act_EmailReceive_ddl_xpath = "//div[@class='task-content']//select[@placeholder ='Recipient']";
	
	
	private  String Dtail_ApplyGovernment_opt_xpath = "//div[@org-placeholder='All Government Clients']//input";
	
	private  String Dtail_ApplyCorporate_opt_xpath = "//div[@org-placeholder='All Corporate Clients']//input";
	
	private  String Dtail_ApplyAll_opt_xpath = "//div[@org-placeholder='All Clients']//input";
	
	private  String Dtail_ApplySpecific_opt_xpath = "//div[@org-placeholder='Specific Clients']//input";
	
	private  String Dtail_ApplySpcefic_arrow_xpath = "//div[@class='form-group form-group-client']//div[contains(@class,'iconC')]";
	
	private  String Dtail_ApplySpcefic_Client_txt_xpath = "//div[@class='form-group form-group-client']//input";
	
	private  String Dtail_ApplySpcefic_ClientList_ddl_xpath = "//div[@class='form-group form-group-client']//ul[@class='b__multi__select__list']";
	
	private  String TaskName_lnk_xpath = "//a[contains(text(),'name_argument')]";

	private  String Dtail_ActionDescr_value_xpath = "//div[@class='task-content']";

	private  String Dtail_ActionEdit_btn_xpath = "//div[@class='task-btn-group']//button[contains(text(),'Edit Action')]";

	

	
	
	public  WebElement Dtail_ApplyGovernment_opt() {
		  return GetElement(Dtail_ApplyGovernment_opt_xpath);
		 }
	
	public  WebElement Dtail_ApplyCorporate_opt() {
		  return GetElement(Dtail_ApplyCorporate_opt_xpath);
		 }
	
	public  WebElement Dtail_ApplyAll_opt() {
		  return GetElement(Dtail_ApplyAll_opt_xpath);
		 }
	
	public  WebElement Dtail_ApplySpecific_opt() {
		  return GetElement(Dtail_ApplySpecific_opt_xpath);
		 }
	
	public  WebElement Dtail_ApplySpcefic_arrow() {
		  return GetElement(Dtail_ApplySpcefic_arrow_xpath);
		 }
	
	public  WebElement Dtail_ApplySpcefic_Client_txt() {
		  return GetElement(Dtail_ApplySpcefic_Client_txt_xpath);
		 }
	
	public  WebElement Dtail_ApplySpcefic_ClientList_ddl() {
		  return GetElement(Dtail_ApplySpcefic_ClientList_ddl_xpath);
		 }
	
	
	public  WebElement Dtail_Act_EmailReceive_ddl() {
		  return GetElement(Dtail_Act_EmailReceive_ddl_xpath);
		 }
	
	
	public  WebElement Search_Name_txt() {
	  return GetElement(Search_Name_txt_xpath);
	 }
	
	public  WebElement Search_btn() {
		  return GetElement(Search_btn_xpath);
		 }
	public  WebElement New_btn() {
		  return GetElement(New_btn_xpath);
		 }
	public  WebElement SmartTaskList_tbl() {
		  return GetElement(SmartTaskList_tbl_xpath);
		 }
	public  WebElement Dtail_Name_txt() {
		  return GetElement(Dtail_Name_txt_xpath);
		 }
	public  WebElement Dtail_ServiceType_ddl() {
		  return GetElement(Dtail_ServiceType_ddl_xpath);
		 }
	public  WebElement Dtail_FieldChange_opt() {
		  return GetElement(Dtail_FieldChange_opt_xpath);
		 }
	public  WebElement Dtail_MailProcess_opt() {
		  return GetElement(Dtail_MailProcess_opt_xpath);
		 }
	public  WebElement Dtail_ActivityComplete_opt() {
		  return GetElement(Dtail_ActivityComplete_opt_xpath);
		 }
	public  WebElement Dtail_FileCreate_opt() {
		  return GetElement(Dtail_FileCreate_opt_xpath);
		 }
	public  WebElement Dtail_EmailTemplate_ddl() {
		  return GetElement(Dtail_EmailTemplate_ddl_xpath);
		 }
	public  WebElement Dtail_ActivityName_txt() {
		  return GetElement(Dtail_ActivityName_txt_xpath);
		 }
	public  WebElement Dtail_CompareValue_txt() {
		  return GetElement(Dtail_CompareValue_txt_xpath);
		 }
	public  WebElement Dtail_TriggerType_ddl() {
		  return GetElement(Dtail_TriggerType_ddl_xpath);
		 }
	public  WebElement Dtail_Fieldname_ddl() {
		  return GetElement(Dtail_Fieldname_ddl_xpath);
		 }
	public  WebElement Dtail_Operator_ddl() {
		  return GetElement(Dtail_Operator_ddl_xpath);
		 }
	public  WebElement Dtail_ConditionList_tbl() {
		  return GetElement(Dtail_ConditionList_tbl_xpath);
		 }
	public  WebElement Dtail_ActionType_ddl() {
		  return GetElement(Dtail_ActionType_ddl_xpath);
		 }
	public  WebElement Dtail_ConditionAdd_btn() {
		  return GetElement(Dtail_ConditionAdd_btn_xpath);
		 }
	public  WebElement Dtail_ActionAdd_btn() {
		  return GetElement(Dtail_ActionAdd_btn_xpath);
		 }
	public  WebElement Dtail_Submit_btn() {
		  return GetElement(Dtail_Submit_btn_xpath);
		 }
	public  WebElement Dtail_Cancel_btn() {
		  return GetElement(Dtail_Cancel_btn_xpath);
		 }
	public  WebElement Dtail_ActEmailTemplate_ddl() {
		  return GetElement(Dtail_ActEmailTemplate_ddl_xpath);
		 }
	public  WebElement Dtail_ActFieldList_ddl() {
		  return GetElement(Dtail_ActFieldList_ddl_xpath);
		 }
	public  WebElement Dtail_ActFieldValueTo_txt() {
		  return GetElement(Dtail_ActFieldValueTo_txt_xpath);
		 }
	public  WebElement Dtail_ActActivityType_ddl() {
		  return GetElement(Dtail_ActActivityType_ddl_xpath);
		 }
	public  WebElement Dtail_ActAssignTo_ddl() {
		  return GetElement(Dtail_ActAssignTo_ddl_xpath);
		 }
	public  WebElement Dtail_ActPriority_ddl() {
		  return GetElement(Dtail_ActPriority_ddl_xpath);
		 }
	public  WebElement Dtail_ActDays_txt() {
		  return GetElement(Dtail_ActDays_txt_xpath);
		 }
	public  WebElement Dtail_ActDate_ddl() {
		  return GetElement(Dtail_ActDate_ddl_xpath);
		 }
	public  WebElement Dtail_ActActivityTitle_txt() {
		  return GetElement(Dtail_ActActivityTitle_txt_xpath);
		 }
	
	
	public  WebElement Dtail_ActUpdate_btn() {
		  return GetElement(Dtail_ActUpdate_btn_xpath);
		 }
	public  WebElement Dtail_ActCancel_btn() {
		  return GetElement(Dtail_ActCancel_btn_xpath);
		 }
	
	public  WebElement Dtail_ActionDescr_value() {
		  return GetElement(Dtail_ActionDescr_value_xpath);
		 }
	public  WebElement Dtail_ActionEdit_btn() {
		  return GetElement(Dtail_ActionEdit_btn_xpath);
		 }

	

	public  WebElement TaskName_lnk(String name_str) {
		try{
			String xpath_str = TaskName_lnk_xpath.replace("name_argument", name_str);
			
			WebElement temp_element = GetElement(xpath_str);
					
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}
	
	
	
	
	///=====METHOD: Search Button
	
	
	public  void GotoSmartTaskDetails_func(String SM_name)
	{
		SearchSmartTask_func(SM_name);
		
		TaskName_lnk(SM_name).click();
		
	//	Wait_For_ElementDisplay(Dtail_Name_txt());
	}

		public  void SearchSmartTask_func(String search_str)
		{
			try {
				ClearThenEnterValueToField(Search_Name_txt(), search_str);

				Search_btn().click();

				Thread.sleep(2000);
			}
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			//String get_str = GetFieldText_func(SmartTaskList_tbl())

		}



		//==METHOD: CREATE NEW
		public  void FillDetailsInfo_func(SmartTask_Info task,String button)
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			ClearThenEnterValueToField(Dtail_Name_txt(), task.Name);

			String note_str = "SmartTask Details - Service Dropdown.";
			
			SelectDropdownItem(Dtail_ServiceType_ddl(), task.Service,note_str);
			
			//Setting for Apply Client
			SetUp_ClientApplyType_func(task.Apply_ClientType,task.Apply_SpecificClientName);


			if(task.IsFieldchange_opt ==true)
			{
				Optimize_ElementClick(Dtail_FieldChange_opt());
				
				note_str = "SmartTask Details - TriggerType Dropdown.";
				
				SelectDropdownItem(Dtail_TriggerType_ddl(), task.TriggerType,note_str);

				note_str = "SmartTask Details - Condition FieldName Dropdown.";
				
				SelectDropdownItem(Dtail_Fieldname_ddl(), task.Condt_FieldName,note_str);

				note_str = "SmartTask Details - Operator Dropdown.";
				
				SelectDropdownItem(Dtail_Operator_ddl(), task.Condt_Operator,note_str);

				if(!task.Condt_Operator.equals("Has any change"))
				{
					Wait_For_ElementDisplay(Dtail_CompareValue_txt_xpath);
					
					String field_type = Dtail_CompareValue_txt().getTagName();
					
					if(field_type.equals("input"))
						ClearThenEnterValueToField(Dtail_CompareValue_txt(), task.Condt_Value);
					else
						SelectDropdownItem(Dtail_CompareValue_txt(), task.Condt_Value, "Dtail_CompareValue_txt");
				}

				Dtail_ConditionAdd_btn().click();

			}//end if


			//
			else if(task.IsEmailProcess_opt==true)
			{
				Dtail_MailProcess_opt().click();
				SelectDropdownItem(Dtail_EmailTemplate_ddl(), task.Condt_EMailTemplName,note_str);

			}

			else if(task.IsActivityComplete_opt==true)
			{
				Dtail_ActivityComplete_opt().click();

				ClearThenEnterValueToField(Dtail_ActivityName_txt(), task.Condt_ActivityName);
			}
			else if(task.IsFileCreate_opt==true)
				Dtail_FileCreate_opt().click();



			//============================ADD ACTIONS
			if(!task.action.Type.equals(""))
				AddAction_func(task.action,"Submit");

			if(button.equals("Submit")||button.equals("submit"))
			{
				Optimize_ElementClick(Dtail_Submit_btn());
				
				

				Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When adding new SmartTask",false);
			}
			else if(button.equals("Cancel"))
				Dtail_Cancel_btn().click();
		}//end void
		
		
		
		private  void Assign_SpecificClientList_func(String clientname)
		{
			Optimize_ElementClick(Dtail_ApplySpcefic_arrow());
			
						
			Optimize_ElementSendkey(Dtail_ApplySpcefic_Client_txt(), clientname);
			
			String note_str = "SmartTask Detail - Client List";
			
			SelectDropdownItem(Dtail_ApplySpcefic_ClientList_ddl(), clientname, note_str);
			
			Optimize_ElementClick(Dtail_ApplySpcefic_arrow());
			
		}
		
		
		private  void SetUp_ClientApplyType_func(String appy_type,String specific_clientname)
		{
			switch(appy_type)
			{
			case "Specific Clients":
				Optimize_ElementClick(Dtail_ApplySpecific_opt());
				
				Assign_SpecificClientList_func(specific_clientname);
			
				break;
				
			case "Government":
				Optimize_ElementClick(Dtail_ApplyGovernment_opt());
				break;
				
			case "Corporate":
				Optimize_ElementClick(Dtail_ApplyCorporate_opt());
				break;
			
			default://for All Client
				Optimize_ElementClick(Dtail_ApplyAll_opt());
				
				break;
			}
		}

		public  void AddAction_func(SmartTask_Action act,String button)
		{
			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
			
			String note_str = "SmartTask Details - ActionType Dropdown.";
			
			SelectDropdownItem(Dtail_ActionType_ddl(), act.Type,note_str);

			Dtail_ActionAdd_btn().click();

			switch(act.Type)
			{

				case "Sending Email Action":
					AddSendEmailAction_func(act);
					break;

				case "Updating Field Action":
					AddFieldUpdateAction_func(act);
					break;

				case "Creating Activity Action":
					AddCreateActivityAction_func(act);
					break;

			}//end switch

			
			
			if(button.equals("Submit")||button.equals("submit"))
			{
				Dtail_ActUpdate_btn().click();
				
				Wait_Ultil_ElementNotDisplay(Dtail_ActUpdate_btn_xpath);

				Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When adding new Action",false);
			}
			else if(button.equals("Cancel"))
				Dtail_ActCancel_btn().click();
		}


		//ADD ACTION TYPE SEND EMAIL TEMPLATE
		private  void AddSendEmailAction_func(SmartTask_Action act)
		{

			String note_str = "SmartTask Details - Template Name Dropdown.";
			
			SelectDropdownItem(Dtail_Act_EmailReceive_ddl(),act.Email_Recipient,"");

			SelectDropdownItem(Dtail_ActEmailTemplate_ddl(), act.Email_Template,note_str);




		}//END VOID

		//ADD ACTION TYPE SEND EMAIL TEMPLATE
		private  void AddCreateActivityAction_func(SmartTask_Action act)
		{
		try {
			
		//	Wait_For_ElementDisplay(Dtail_ActActivityType_ddl());
			
			String note_str = "SmartTask Details - Action Activity Type Dropdown.";
			
			SelectDropdownItem(Dtail_ActActivityType_ddl(), act.Act_Activity.Type,note_str);

			note_str = "SmartTask Details - Action Assign To Dropdown.";
			
			SelectDropdownItem(Dtail_ActAssignTo_ddl(), act.Act_Activity.SM_Assignee,note_str);

			ClearThenEnterValueToField(Dtail_ActDays_txt(), Integer.toString(act.Act_Activity.SM_Days));

			note_str = "SmartTask Details - Action Activity Date Dropdown.";
			
			SelectDropdownItem(Dtail_ActDate_ddl(), act.Act_Activity.Date,note_str);


			ClearThenEnterValueToField(Dtail_ActActivityTitle_txt(), act.Act_Activity.Title);

			
			Actions actions = new Actions(driverbase.GetDriver());

			Action action = actions.sendKeys(Dtail_ActActivityTitle_txt(), Keys.TAB).sendKeys(act.Act_Activity.Content).build();

			action.perform();

			Thread.sleep(2000);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		}//END VOID


		private  void AddFieldUpdateAction_func(SmartTask_Action act)
		{

			String note_str = "SmartTask Details - Action Field List Dropdown.";
			
			SelectDropdownItem(Dtail_ActFieldList_ddl(), act.FieldUpdate_FieldName,note_str);

			ClearThenEnterValueToField(Dtail_ActFieldValueTo_txt(), act.FieldUpdate_ValueTo);
		}


		public  void VerifyDetailsInfo(SmartTask_Info smarttask)
		{

			try {
				String fieldname = "";
				String Area = "SmartTask Dtails-";
	
				new LeftMenu_page(driverbase).GotoSmartTaskPage();
	
				SearchSmartTask_func(smarttask.Name);
	
				TaskName_lnk(smarttask.Name).click();
	
				Thread.sleep(3000);
	
				fieldname = "Dtail_Name_txt";
				VerifyFieldValueEqual_func(Area+fieldname, Dtail_Name_txt(), smarttask.Name,"");
	
				fieldname = "Dtail_ServiceType_ddl";
				VerifyFieldValueEqual_func(Area+fieldname, Dtail_ServiceType_ddl(), smarttask.Service,"");
	
				fieldname = "Dtail_FieldChange_opt";
				VerifyFieldValueEqual_func(Area+fieldname, Dtail_FieldChange_opt(), smarttask.IsFieldchange_opt.toString(),"");
	
				fieldname = "Dtail_ActivityComplete_opt";
				VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActivityComplete_opt(), smarttask.IsActivityComplete_opt.toString(),"");
	
				fieldname = "Dtail_FileCreate_opt";
				VerifyFieldValueEqual_func(Area+fieldname, Dtail_FileCreate_opt(), smarttask.IsFileCreate_opt.toString(),"");
	
				fieldname = "Dtail_MailProcess_opt";
				VerifyFieldValueEqual_func(Area+fieldname, Dtail_MailProcess_opt(), smarttask.IsEmailProcess_opt.toString(),"");
	
				//fieldname = "Dtail_ActionType_ddl"
				//VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActionType_ddl(), smarttask.action.Type)
	
				//fieldname = "Dtail_ActionDescr_value"
				//VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActionDescr_value(), smarttask.action.)
	
				Optimize_ElementClick(Dtail_ActionEdit_btn());
	
				Thread.sleep(1000);
	
				switch(smarttask.action.Type)
				{
					case "Sending Email Action":
						fieldname = "Dtail_ActEmailTemplate_ddl";
						VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActEmailTemplate_ddl(), smarttask.action.Email_Template,"");
						break;
	
					case "Updating Field Action":
						fieldname = "Dtail_ActFieldList_ddl";
						VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActFieldList_ddl(), smarttask.action.FieldUpdate_FieldName,"");
	
						fieldname = "Dtail_ActFieldValueTo_txt";
						VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActFieldValueTo_txt(), smarttask.action.FieldUpdate_ValueTo,"");
						break;
	
	
					case "Creating Activity Action":
	
						fieldname = "Dtail_ActActivityType_ddl";
						VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActActivityType_ddl(), smarttask.action.Act_Activity.Type,"");
	
						fieldname = "Dtail_ActAssignTo_ddl";
						VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActAssignTo_ddl(), smarttask.action.Act_Activity.Assignee,"");
	
						fieldname = "Dtail_ActDays_txt";
						VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActDays_txt(), Integer.toString(smarttask.action.Act_Activity.SM_Days),"");
	
						fieldname = "Dtail_ActDate_ddl";
						VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActDate_ddl(), smarttask.action.Act_Activity.Date,"");
	
						fieldname = "Dtail_ActActivityTitle_txt";
						VerifyFieldValueEqual_func(Area+fieldname, Dtail_ActActivityTitle_txt(), smarttask.action.Act_Activity.Title,"");
	
						fieldname = "Activity Content";
						VerifyFieldValueEqual_func(Area+fieldname, Page_TinyContentBox(), smarttask.action.Act_Activity.Content,"");

						break;
				}//switch
			}
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		
		public  void CreateSmartTask_func(SmartTask_Info sm, String button) {
			
			new LeftMenu_page(driverbase).GotoSmartTaskPage();

			New_btn().click();

			new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

		//	Wait_For_ElementDisplay(Dtail_Name_txt());

			FillDetailsInfo_func(sm, button);
			
			System.out.println("=====STeps - Smart Task is created: ["+sm.Name+"]");
		}

}
