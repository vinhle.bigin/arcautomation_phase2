package pages.expenseFeature;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import configuration.DriverConfig.DriverBase;
import custom_Func.FileManage;
import businessObjects.Activity_Info;
import businessObjects.ReimBurs_Info;
import businessObjects.SmartTask_Info;
import businessObjects.SubExpense_Info;
import businessObjects.Transferee_Info;
import pages.ARCLoading;
import pages.LeftMenu_page;
import pages.Messages_Notification;


public class NewSubExpense_modal extends PageObjects{
	
	public NewSubExpense_modal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	//New SubExpen modal
	private String SubExpense_mdal_xpath = "//div[@id='loading-modal-expense-reibursement-expense-create'] || //div[@id='loading-modal-expense-reibursement-expense-edit']";
	private String ExpDate_txt_xpath = SubExpense_mdal_xpath+ "//input[@placeholder='Expense Date *']";
	private String Purpose_ddl_xpath = SubExpense_mdal_xpath+ "//select[@placeholder='Purpose *']";
	private String ServiceType_ddl_xpath = SubExpense_mdal_xpath+ "//select[@placeholder='Service Type *']";
	private String ClaimAmount_txt_xpath = SubExpense_mdal_xpath+ "//input[@placeholder='Claim Amount *']";
	private String AttachExp_lnk_xpath = SubExpense_mdal_xpath+ "//div[@id='create-expense-attachments']";
	private String Description_txt_xpath = SubExpense_mdal_xpath+ "textarea[@placeholder='Description']";
	private String SubmitExp_btn_xpath = SubExpense_mdal_xpath+ "//button[@data-control='create']";
	private String CancelExp_btn_xpath = SubExpense_mdal_xpath+ "//button[@data-control='cancel-create']";
	private String FileName_title_xpath = SubExpense_mdal_xpath+"//div[@class='preview']//span[contains(text() ,'<file_name>')]";

	
	// DEFINE ELEMENTS
	public WebElement ExpDate_txt() {
		return GetElement(ExpDate_txt_xpath);
	}
	
	public WebElement Purpose_ddl() {
		return GetElement(Purpose_ddl_xpath);
	}
	
	public WebElement ServiceType_ddl() {
		return GetElement(ServiceType_ddl_xpath);
	}
	
	public WebElement ClaimAmount_txt() {
		return GetElement(ClaimAmount_txt_xpath);
	}
	
	public WebElement AttachmentExp() {
		return GetElement(AttachExp_lnk_xpath);
	}
	
	public WebElement Description_txt() {
		return GetElement(Description_txt_xpath);
	}
	
	public WebElement SubmitExp_btn() {
		return GetElement(SubmitExp_btn_xpath);
	}
	
	public WebElement CancelExp_btn() {
		return GetElement(CancelExp_btn_xpath);
	}
	
	public  WebElement FileName_title(String file_name){

		WebElement filename_title = GetElement(FileName_title_xpath.replace("<file_name>", file_name));
		
		if (filename_title == null) {
			
			String a = FileName_title_xpath.replace("<file_name>",file_name.replace(file_name,file_name.toLowerCase().replace('_', '-')));
			
			filename_title = GetElement(a);
		}
		
		return filename_title;
	}

	
	//=============================METHOD
	public void CreateSubExp_func(SubExpense_Info subexp, String button) 
	{
		FillSubExp(subexp);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(SubmitExp_btn());
		}
		else if(button.equals("Cancel"))
			CancelExp_btn().click();
	}
	
	public void FillSubExp(SubExpense_Info subexp) 
	{	
		Optimize_ElementSendkey(ExpDate_txt(), subexp.Exp_Date.toString());
		
		String note_str = "";
		
		note_str = "Purpose";
		SelectDropdownItem(Purpose_ddl(), subexp.Purpose, note_str);
		
		note_str = "Service Type";
		SelectDropdownItem(ServiceType_ddl(), subexp.ServiceType, note_str);
		
		Optimize_ElementSendkey(ClaimAmount_txt(), subexp.Amount.toString());

		//Attachment SubExpense
		int doc_size = subexp.FilesName.size();
		
		if(doc_size>0)
		{
			for(int i =0;i<doc_size;i++)
			{
				new FileManage().UploadFile_func(AttachmentExp(), subexp.FilesName.get(i));
				
			}//end for
			
		}//end if doc_size
		
		Optimize_ElementSendkey(Description_txt(), subexp.Descr);

	}//end void
	
	public void VerifySubExpDetail(SubExpense_Info subexp)
	{
		String field_name = "";
		String note = "- View Mode";
		
		field_name = "Expense Date";
		VerifyFieldValueEqual_func(field_name, ExpDate_txt(), subexp.Exp_Date.toString(), note);
		
		field_name = "Purpose";
		VerifyFieldValueEqual_func(field_name, Purpose_ddl(), subexp.Purpose, note);
		
		field_name = "Service Type";
		VerifyFieldValueEqual_func(field_name, ServiceType_ddl(), subexp.ServiceType, note);
		
		field_name = "Claim Amount";
		VerifyFieldValueEqual_func(field_name, ClaimAmount_txt(), subexp.Amount.toString(), note);
		
		int doc_size = subexp.FilesName.size();
		
		if(doc_size>0)
		{
			for(int i =0;i<doc_size;i++) 
			{
				field_name = "Attached File";
				
				VerifyFieldDisplayed_func(field_name, FileName_title(subexp.FilesName.get(i)), "");
			
			}//end for
			
		}//end if doc_size
		
		field_name = "Description";
		VerifyFieldValueEqual_func(field_name, Description_txt(), subexp.Descr, note);
	}
		
	public void Verify_SubExp_Modal_Not_Editable(SubExpense_Info subexp)
	{
		String field_name = "";		
		String note = "- View Mode";
		
		field_name = "Expense Date";
		Verify_Field_Not_Editable_func(field_name, ExpDate_txt(), subexp.Exp_Date.toString(), note);
		
		field_name = "Purpose";
		Verify_Field_Not_Editable_func(field_name, Purpose_ddl(), subexp.Purpose, note);
		
		field_name = "Service Type";
		Verify_Field_Not_Editable_func(field_name, ServiceType_ddl(), subexp.ServiceType, note);
		
		field_name = "Amount";
		Verify_Field_Not_Editable_func(field_name, ClaimAmount_txt(), subexp.Amount.toString(), note);
		
		field_name = "Description";
		Verify_Field_Not_Editable_func(field_name, Description_txt(), subexp.Descr, note);
		
	}

}
