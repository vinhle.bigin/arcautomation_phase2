package pages.expenseFeature;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import Enums.Approval;
import configuration.DriverConfig.DriverBase;
import businessObjects.BankAccount_Info;
import businessObjects.ReimBurs_Info;
import businessObjects.SubExpense_Info;
import pages.Messages_Notification;


public class ExpenseReimDetail_page extends ExpenseReim_modal{
	
	public ExpenseReimDetail_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String GeneralInfo_section_xpath = "//div[@id='expense-reimbursement-general-info']";
	private String EditGeneralInfo_btn_xpath = GeneralInfo_section_xpath + "//a[@data-original-title='Edit']";
	private String TransfName_txt_xpath = "//div[@org-placeholder='Transferee Name']//input"; 
	private String AssignedAnalyst_slect_xpath = "//div[@org-placeholder='Assigned Analyst']//select";
	private String Acknowdged_txt_xpath = "//input[@id='acknowledgement_accepted_date']";
	private String UpdateGeneralInfo_btn_xpath = GeneralInfo_section_xpath + "//button[@data-control='update']";
	private String CancelGeneralInfo_btn_xpath = GeneralInfo_section_xpath + "//button[@data-control='cancel-update']";
	
	private String Payment_section_xpath = "//div[@id='expense-reimbursement-payment']";
	private String EditPayment_btn_xpath = Payment_section_xpath + "//a[@data-original-title='Edit']";
	private String UpdatePayment_btn_xpath = Payment_section_xpath + "//button[@data-control='update']";
	private String CancelPayment_btn_xpath = Payment_section_xpath + "//button[@data-control='cancel-update']";
	
	private String AddExpense_btn_xpath = "//button[contains(text(),'Add Expense')]";
	private String UnprocessedExpense_tbl_xpath = "//div[@id='table-expense-reibursement']";
	private String SelectSubExpense_chbox_xpath = UnprocessedExpense_tbl_xpath + "//input[@type='checkbox']";
	private String ProcessCheckExpense_btn_xpath = "//button[contains(text(),'Process Checked Expense(s)')]";
	private String EditSubExpense_btn_xpath = UnprocessedExpense_tbl_xpath + "//a[@data-original-title='Edit']";
	private String RemoveSubExpense_btn_xpath = UnprocessedExpense_tbl_xpath + "//a[@data-original-title='Remove']";
	
	private String Approved_tbl_xpath = "//div[@id='expense-approved-list-0']";
	private String ViewExpenseDetail_btn_xpath = "//button[contains(text(),'View Expense Detail')]";
	private String ViewApproveSubExpense_btn_xpath = Approved_tbl_xpath + "//a[@data-original-title='View']";
	private String UndoApproveSubExpense_btn_xpath = Approved_tbl_xpath + "//a[@data-original-title='Undo']";
	private String Rejected_tbl_xpath = "//div[@id='expense-rejected-reibursement']";
	private String ViewRejectSubExpense_btn_xpath = Rejected_tbl_xpath + "//a[@data-original-title='View']";
	private String UndoRejectSubExpense_btn_xpath = Rejected_tbl_xpath + "//a[@data-original-title='Undo']";
	private String Approve_btn_xpath = "//div[@class='floating-action-wrapper']//button[contains(text(),'Approve')]";
	private String Reject_btn_xpath = "//div[@class='floating-action-wrapper']//button[contains(text(),'Reject')]";
	private String Confirm_mdal_xpath = "//div[@id='confirm-modal']";
	private String Ok_btn_xpath = Confirm_mdal_xpath + "//button[contains(text(),'OK')]";

	
	// DEFINE ELEMENTS
	public WebElement GeneralInfo_section() {
		return GetElement(GeneralInfo_section_xpath);
	}
	
	public WebElement EditGeneralInfo_btn() {
		return GetElement(EditGeneralInfo_btn_xpath);
	}
	
	public WebElement TransfName_txt() {
		return GetElement(TransfName_txt_xpath);
	}
	
	public WebElement AssignedAnalyst_slect() {
		return GetElement(AssignedAnalyst_slect_xpath);
	}
	
	public WebElement Acknowdged_txt() {
		return GetElement(Acknowdged_txt_xpath);
	}
	
	public WebElement UpdateGeneralInfo_btn() {
		return GetElement(UpdateGeneralInfo_btn_xpath);
	}
	
	public WebElement CancelGeneralInfo_btn() {
		return GetElement(CancelGeneralInfo_btn_xpath);
	}
	
	public WebElement Payment_section() {
		return GetElement(Payment_section_xpath);
	}
	
	public WebElement EditPayment_btn() {
		return GetElement(EditPayment_btn_xpath);
	}
	
	public WebElement UpdatePayment_btn() {
		return GetElement(UpdatePayment_btn_xpath);
	}
	
	public WebElement CancelPayment_btn() {
		return GetElement(CancelPayment_btn_xpath);
	}
	
	public WebElement AddExpense_btn() {
		return GetElement(AddExpense_btn_xpath);
	}
	
	public WebElement UnprocessedExpense_tbl() {
		return GetElement(UnprocessedExpense_tbl_xpath);
	}
	
	public WebElement SelectSubExpense_chbox() {
		return GetElement(SelectSubExpense_chbox_xpath);
	}
	
	public WebElement ProcessCheckExpense_btn() {
		return GetElement(ProcessCheckExpense_btn_xpath);
	}
	
	public WebElement EditSubExpense_btn() {
		return GetElement(EditSubExpense_btn_xpath);
	}
	
	public WebElement RemoveSubExpense_btn() {
		return GetElement(RemoveSubExpense_btn_xpath);
	}
	
	public WebElement Approved_tbl() {
		return GetElement(Approved_tbl_xpath);
	}
	
	public WebElement ViewExpenseDetail_btn() {
		return GetElement(ViewExpenseDetail_btn_xpath);
	}
	
	public WebElement ViewApproveSubExpense_btn() {
		return GetElement(ViewApproveSubExpense_btn_xpath);
	}
	
	public WebElement UndoApproveSubExpense_btn() {
		return GetElement(UndoApproveSubExpense_btn_xpath);
	}
	
	public WebElement Rejected_tbl() {
		return GetElement(Rejected_tbl_xpath);
	}
	
	public WebElement ViewRejectSubExpense_btn() {
		return GetElement(ViewRejectSubExpense_btn_xpath);
	}
	
	public WebElement UndoRejectSubExpense_btn() {
		return GetElement(UndoRejectSubExpense_btn_xpath);
	}
	
	public WebElement Approve_btn() {
		return GetElement(Approve_btn_xpath);
	}
	
	public WebElement Reject_btn() {
		return GetElement(Reject_btn_xpath);
	}
	
	public WebElement Confirm_mdal() {
		return GetElement(Confirm_mdal_xpath);
	}
	
	public WebElement Ok_btn() {
		return GetElement(Ok_btn_xpath);
	}
	
//=====================================METHOD

	public void EditExpReimInfo(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment, String button)
	{
		EditGeneralInfo(reim, button);
		
		EditPayment(payment, button);
		
		EditSubExp(subexp, button);
	}
	
	public void EditGeneralInfo(ReimBurs_Info reim, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(EditGeneralInfo_btn());
		
		this.FillGeneralInfo(reim);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(UpdateGeneralInfo_btn());

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When updating the general info",false);
		}
		else if(button.equals("Cancel")||button.equals("cancel"))
		{
			Optimize_ElementClick(CancelGeneralInfo_btn());
		}
	}

	@Override
	public void FillGeneralInfo(ReimBurs_Info reim)
	{
		super.FillGeneralInfo(reim);
		
		ClearThenEnterValueToField(TransfName_txt(), reim.transf.FullName);

		ClearThenEnterValueToField(AssignedAnalyst_slect(), reim.Assigned_Analyst.toString());

		ClearThenEnterValueToField(Acknowdged_txt(), reim.Acknowdged.toString());
	}
	
	public void EditPayment(BankAccount_Info payment, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(EditPayment_btn());
		
		this.FillPayment(payment);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(UpdatePayment_btn());

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When updating the payment",false);
		}
		else if(button.equals("Cancel")||button.equals("cancel"))
		{
			Optimize_ElementClick(CancelPayment_btn());
		}
	}
	
	@Override
	public void FillPayment(BankAccount_Info payment)
	{
		super.FillPayment(payment);
	}
	
	public void EditSubExp(SubExpense_Info subexp, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(EditSubExpense_btn());
		
		new NewSubExpense_modal(driverbase).CreateSubExp_func(subexp, button);
	}
	
	public void ProcessExp_func(Approval approval,SubExpense_Info subexp, ReimBurs_Info reim, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(SelectSubExpense_chbox());
		
		Optimize_ElementClick(ProcessCheckExpense_btn());
		
		new NewExpense_modal(driverbase).ProcessExp_func(approval, subexp);
		
		reim.ReimApproval = approval;
		
		if(approval==Approval.Approve)
		{
			Optimize_ElementClick(Approve_btn());
		}
			
		else if(approval==Approval.Reject)
	
			Optimize_ElementClick(Reject_btn());
	}
	
	
	
	
	
	
//=======================VERIFY

	public void VerifyExpReimDetail(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment)
	{	
		VerifyGeneralInfo(reim);
		
		this.VerifyPayment(payment);
	}
	
@Override
	public void VerifyGeneralInfo(ReimBurs_Info reim)
	{
		String area = "";
		String field_name = "";
		String note = "- View Mode";
		
		super.VerifyGeneralInfo(reim);
		
		area = "General Info";
		field_name = "Transferee Name";
		VerifyFieldValueEqual_func(area+field_name, TransfName_txt(), reim.transf.FullName, note);
		
		field_name = "Assigned Analyst";
		VerifyFieldValueEqual_func(area+field_name, AssignedAnalyst_slect(), reim.Assigned_Analyst.toString(), note);
		
		field_name = "Acknowledgement Accepted At";
		VerifyFieldValueEqual_func(area+field_name, Acknowdged_txt(), reim.Acknowdged.toString(), note);
	}

@Override
	public void VerifyPayment(BankAccount_Info payment)
	{
		super.VerifyPayment(payment);
	}
}
