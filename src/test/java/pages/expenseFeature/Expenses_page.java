package pages.expenseFeature;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Transferee_Info;
import configuration.DriverConfig.DriverBase;


public class Expenses_page extends PageObjects{
	
	public Expenses_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String ReportedFrom_txt_xpath = "//input[@id='search_from_date']";
	private String ReportedTo_txt_xpath = "//input[@id='search_to_date']";
	private String TransfName_txt_xpath = "//input[@id='transferee_name']";
	private String Client_ddl_xpath = "//input[@placeholder='Client']";
	private String Coordinator_ddl_xpath ="//input[@placeholder='Coordinator']";
	private String AssignedAnalyst_ddl_xpath = "//input[@placeholder='Assigned Analyst']";
	private String Search_btn_xpath = "//button[@data-control='search']";
	private String Cancel_btn_xpath = "//button[contains(text(),'Clear')]";
	private String ViewExpense_btn_xpath = "//a[@data-original-title='View']";
	private String EditExpense_btn_xpath = "//a[@data-original-title='Edit']";
	
	
	// DEFINE ELEMENTS
	
	public WebElement ReportedFrom_txt() {
		return GetElement(ReportedFrom_txt_xpath);
	}
	
	public WebElement ReportedTo_txt() {
		return GetElement(ReportedTo_txt_xpath);
	}
	
	public WebElement TransfName_txt() {
		return GetElement(TransfName_txt_xpath);
	}
	
	public WebElement Client_ddl() {
		return GetElement(Client_ddl_xpath);
	}
	
	public WebElement Coordinator_ddl() {
		return GetElement(Coordinator_ddl_xpath);
	}
	
	public WebElement AssignedAnalyst_ddl() {
		return GetElement(AssignedAnalyst_ddl_xpath);
	}
	
	public WebElement Search_btn() {
		return GetElement(Search_btn_xpath);
	}
	
	public WebElement Cancel_btn() {
		return GetElement(Cancel_btn_xpath);
	}
	
	public  WebElement Status_tab(String Status) {
		
		String Status_tab_xpath = "//a[contains(text(), '"+Status+"')]";
		
		return GetElement(Status_tab_xpath); 
	}
	
	public WebElement ViewExpense_btn() {
		return GetElement(ViewExpense_btn_xpath);
	}
	
	public WebElement EditExpense_btn() {
		return GetElement(EditExpense_btn_xpath);
	}
	
	

}
