package pages.expenseFeature;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;
import businessObjects.Activity_Info;
import businessObjects.BankAccount_Info;
import businessObjects.ReimBurs_Info;
import businessObjects.SubExpense_Info;
import businessObjects.Transferee_Info;
import pages.ARCLoading;
import pages.Messages_Notification;


public class ExpenseReim_modal extends NewSubExpense_modal{
	
	public ExpenseReim_modal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String ReimDetail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]";
	private String EditExpReim_btn_xpath = ReimDetail_mdal_xpath + "//div[@class='modal-header']//a[@data-original-title='Edit']";
	private String Name_txt_xpath = ReimDetail_mdal_xpath+ "//input[@placeholder='Name *']";
	private String Desc_txt_xpath = ReimDetail_mdal_xpath+ "//textarea[@placeholder='Description']";
	//Payment
	private String AddPaymentInfo_btn_xpath = "//button[contains(text(),'Add payment info')]";
	private String WireAccount_opt_xpath = "//label[contains(text(),'Wire Account')]/../input";
	private String USDDirect_opt_xpath = "//label[contains(text(),'USD Direct Deposit/ACH')]/../input";
	private String Check_opt_xpath = "//label[contains(text(),'Check')]/../input";
	private String SwiftCode_txt_xpath = "//div[@org-placeholder='SWIFT Code']//input";
	private String BankName_txt_xpath = "//div[@org-placeholder='Bank Name']//input";
	private String AccNumber_txt_xpath = "//div[@org-placeholder='Account Number']//input";
	private String AccHolderName_txt_xpath = "//div[@org-placeholder='Account Holder Name']//input";
	//Expense
	private String AddExp_btn_xpath = "//button[contains(text(),'Add Expense')]";
	private String Search_txt_xpath = ReimDetail_mdal_xpath+ "//input[@class='form-control ']";
	private String SubExp_tbl_xpath = ReimDetail_mdal_xpath+ "//div[@id = 'table-expense-reibursement']//table/tbody";
	private String View_btn_xpath = ReimDetail_mdal_xpath+ "//a[@data-original-title='View']";
	private String Remove_btn_xpath = ReimDetail_mdal_xpath+ "//a[@data-original-title='Remove']";

	private String CheckCertify_chbox_xpath = "//input[@class=' checkbox__input']";
	private String SubmitExpClaim_btn_xpath = ReimDetail_mdal_xpath + "//button[contains(text(),'Submit Expense Claim')]";
	private String SaveDraft_btn_xpath = ReimDetail_mdal_xpath + "//button[contains(text(),'Save Draft')]";
	private String Cancel_btn_xpath = ReimDetail_mdal_xpath + "//button[contains(text(),'Cancel')]";
	private String ModalClose_btn_xpath = ReimDetail_mdal_xpath + "//div[@class='modal-header']//i[@class='icon-close']";




	
	// DEFINE ELEMENTS
	public WebElement ReimDetail_mdal() {
		return GetElement(ReimDetail_mdal_xpath);
	}
	
	public WebElement EditExpReim_btn() {
		return GetElement(EditExpReim_btn_xpath);
	}
	
	public WebElement Name_txt() {
		return GetElement(Name_txt_xpath);
	}
	
	public WebElement Desc_txt() {
		return GetElement(Desc_txt_xpath);
	}
	
	public WebElement AddPaymentInfo_btn() {
		return GetElement(AddPaymentInfo_btn_xpath);
	}
	
	public WebElement WireAccount_opt() {
		return GetElement(WireAccount_opt_xpath);
	}
	
	public WebElement USDDirect_opt() {
		return GetElement(USDDirect_opt_xpath);
	}
	
	public WebElement Check_opt() {
		return GetElement(Check_opt_xpath);
	}
	
	public WebElement SwiftCode_txt() {
		return GetElement(SwiftCode_txt_xpath);
	}
	
	public WebElement BankName_txt() {
		return GetElement(BankName_txt_xpath);
	}
	
	public WebElement Bank_txt(String bankname)
	{
		String create_parent_xpath = "//div[contains(@class, 'bank-block last-bank')]";
		if(!bankname.equals(""))
		{
			BankName_txt_xpath = "//div[contains(@class,'bank block') and inntertext()='"+bankname+"']"+BankName_txt_xpath;
			
			
		}//if
		else
		BankName_txt_xpath = create_parent_xpath+BankName_txt_xpath.replace("input", "input[not(disabled='disabled')]");
		
		return GetElement(BankName_txt_xpath);
	}
	
	public WebElement AccNumber_txt() {
		return GetElement(AccNumber_txt_xpath);
	}
	
	public WebElement AccHolderName_txt() {
		return GetElement(AccHolderName_txt_xpath);
	}
	
	public WebElement SubExp_tbl() {
		return GetElement(SubExp_tbl_xpath);
	}

	public WebElement AddExp_btn() {
		return GetElement(AddExp_btn_xpath);
	}
	
	public WebElement Search_txt() {
		return GetElement(Search_txt_xpath);
	}
	
	public WebElement CheckCertify_chbox() {
		return GetElement(CheckCertify_chbox_xpath);
	}
	
	public WebElement SubmitExpClaim_btn() {
		return GetElement(SubmitExpClaim_btn_xpath);
	}
	
	public WebElement SaveDraft_btn() {
		return GetElement(SaveDraft_btn_xpath);
	}
	
	public WebElement View_btn() {
		return GetElement(View_btn_xpath);
	}
	
	public WebElement Remove_btn() {
		return GetElement(Remove_btn_xpath);
	}
	
	public WebElement Cancel_btn() {
		return GetElement(Cancel_btn_xpath);
	}
	
	public WebElement ModalClose_btn() {
		return GetElement(ModalClose_btn_xpath);
	}
	

//==================METHOD
@Override
	public void CreateSubExp_func(SubExpense_Info subexp, String button) 
	{
		AddExp_btn().click();

		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

		super.CreateSubExp_func(subexp, button);
		
		System.out.println("=====STeps - SubExpense is added: ["+subexp.Purpose+"]");
	}
	
	public void CreateExpReim_func(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

		FillGeneralInfo(reim);

		FillPayment(payment);
		
		CreateSubExp_func(subexp, button);
		
		Optimize_ElementClick(CheckCertify_chbox());

		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(SubmitExpClaim_btn());

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When creating new expense reim",false);
		}
		else if(button.equals("SaveDraft"))
			SaveDraft_btn().click();
	}
	
	public void EditExpReim_func(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment, String button) 
	{
		ARCLoading ARCLoading = new ARCLoading(driverbase);
		
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		ARCLoading.Wait_Util_FinishedPageLoading();

		ARCLoading.Wait_Util_FinishedPageLoading();
		
		Optimize_ElementClick(EditExpReim_btn());

		FillGeneralInfo(reim);

		FillPayment(payment);
		
		CreateSubExp_func(subexp, button);

		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(SubmitExpClaim_btn());

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When submitting expense reim",false);
		}
		else if(button.equals("SaveDraft"))
			SaveDraft_btn().click();

		try {

			Thread.sleep(4000);

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,
					"When editing expense reimbursement.\n", false);

			if (ModalClose_btn() != null) {
				Optimize_ElementClick(ModalClose_btn());
				Thread.sleep(1000);

			} // end if

		} // end try
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}// end void
	
	protected void FillGeneralInfo(ReimBurs_Info reim)
	{
		Optimize_ElementSendkey(Name_txt(), reim.Name);
		
		Optimize_ElementSendkey(Desc_txt(), reim.Descr);
	}
	
	protected void FillPayment(BankAccount_Info payment)
	{
		String bankName_tmp = "";
		
		if(Bank_txt(payment.BankName)!=null)
		{
			bankName_tmp = payment.BankName;
		}//
		
		Optimize_ElementClick(AddPaymentInfo_btn());
		
		switch(payment.TransactionType)
		{
		case "USD DIRECT DEPOSIT/ACH" : 
			
			Optimize_ElementClick(USDDirect_opt());
			
			if(!BankName_txt().equals(""))
			{	
				ClearThenEnterValueToField(BankName_txt(), payment.BankName);
			
				ClearThenEnterValueToField(AccNumber_txt(), payment.AccountNumber);
				
				ClearThenEnterValueToField(AccHolderName_txt(), payment.HolderName);
			}
			break;
		
		case "CHECK" :
			
			Optimize_ElementClick(Check_opt());

		default : 
			if(!SwiftCode_txt().equals(""))
			{	
				ClearThenEnterValueToField(SwiftCode_txt(), payment.SwiftCode);
				
				ClearThenEnterValueToField(BankName_txt(), payment.BankName);
			
				ClearThenEnterValueToField(AccNumber_txt(), payment.AccountNumber);
				
				ClearThenEnterValueToField(AccHolderName_txt(), payment.HolderName);
			}
			break;
		}
	}
	
	protected void VerifyExpReimDetail(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment)
	{
		VerifyGeneralInfo(reim);
		
		VerifyPayment(payment);
		
		Optimize_ElementClick(View_btn());
		
		new NewSubExpense_modal(driverbase).VerifySubExpDetail(subexp);

	}
	
	protected void VerifySubReimExistTable_func(SubExpense_Info subexp)
	{
		Boolean exist = false;
		List<String> records = GetTableRecordsPerPaging_func(SubExp_tbl());

		for (String index_str : records) {
			if (index_str.contains(subexp.Purpose)) {
				exist = true;
				break;
			} // end if
		} // end foreach

		if (!exist) {
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "Sub Expense[" + subexp.Purpose + "] NOT exist in table.\n";
		} // end if
	}
	
	protected void VerifyGeneralInfo(ReimBurs_Info reim) 
	{
		String field_name = "";
		String note = "- View Mode";
		
		field_name = "Name";
		VerifyFieldValueEqual_func(field_name, Name_txt(), reim.Name, note);
		
		field_name = "Description";
		VerifyFieldValueEqual_func(field_name, Desc_txt(), reim.Descr, note);
		
	}
	
	protected void VerifyPayment(BankAccount_Info payment)
	{
		String field_name = "";
		String note = "- View Mode";
		
		switch(payment.TransactionType)
		{
		case "USD DIRECT DEPOSIT/ACH" : 
			
			field_name = "Bank Name";
			VerifyFieldValueEqual_func(field_name, BankName_txt(), payment.BankName, note);
			
			field_name = "Account Number";
			VerifyFieldValueEqual_func(field_name, AccNumber_txt(), payment.AccountNumber, note);
				
			field_name = "Account Holder Name";
			VerifyFieldValueEqual_func(field_name, AccHolderName_txt(), payment.HolderName, note);
			
			break;
		
		case "CHECK" :
			
			//will update later

		default : 
			
			field_name = "Swift Code";
			VerifyFieldValueEqual_func(field_name, SwiftCode_txt(), payment.SwiftCode, note);
				
			field_name = "Bank Name";
			VerifyFieldValueEqual_func(field_name, BankName_txt(), payment.BankName, note);
			
			field_name = "Account Number";
			VerifyFieldValueEqual_func(field_name, AccNumber_txt(), payment.AccountNumber, note);
				
			field_name = "Account Holder Name";
			VerifyFieldValueEqual_func(field_name, AccHolderName_txt(), payment.HolderName, note);
			
			break;
		}
	}

	public void Verify_ExpReim_Modal_Not_Editable(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment)
	{
		String field_name = "";		
		String note = "- View Mode";
		
		field_name = "Name";
		Verify_Field_Not_Editable_func(field_name, Name_txt(), reim.Name, note);
		
		field_name = "Description";
		Verify_Field_Not_Editable_func(field_name, Desc_txt(), reim.Descr, note);
		
		//Verify payment is not editted
		
		Optimize_ElementClick(View_btn());
		
		new NewSubExpense_modal(driverbase).Verify_SubExp_Modal_Not_Editable(subexp);

	}//end void
	
	
}
