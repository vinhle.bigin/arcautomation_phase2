package pages.expenseFeature;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Transferee_Info;
import configuration.DriverConfig.DriverBase;


public class Transf_ExpenseReim_tab extends PageObjects{
	
	public Transf_ExpenseReim_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	private String ExpenseReim_tbl_xpath = "//div[@id='table-list-reimbursement']";
	private String Edit_btn_xpath = "//a[@data-original-title='Edit']";
	private String View_btn_xpath = "//a[@data-original-title='View']";
	private String Download_btn_xpath = "//a[@data-original-title='Download']";
	
	// DEFINE ELEMENTS
	public WebElement ExpenseReim_tbl() {
		return GetElement(ExpenseReim_tbl_xpath);
	}
	
	public WebElement Edit_btn() {
		return GetElement(Edit_btn_xpath);
	}
	
	public WebElement View_btn() {
		return GetElement(View_btn_xpath);
	}
	
	public WebElement Download_btn() {
		return GetElement(Download_btn_xpath);
	}

}
