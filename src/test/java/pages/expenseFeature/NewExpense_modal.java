package pages.expenseFeature;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import Enums.Approval;
import baseClasses.PageObjects;
import configuration.DriverConfig.DriverBase;
import businessObjects.SubExpense_Info;
import businessObjects.Transferee_Info;
import pages.Messages_Notification;


public class NewExpense_modal extends PageObjects{
	
	public NewExpense_modal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String Expense_mdal_xpath = "//div[@id='loading-modal-expense-create' or @id='loading-modal-expense-reibursement-process-create']";
	private String TransactionID_txt_xpath = Expense_mdal_xpath + "//div[@org-placeholder='Transaction ID']//input";
	private String FinalAmount_txt_xpath = Expense_mdal_xpath + "//div[@org-placeholder='Final Amount *']//input";
	private String ExpenseCode_txt_xpath = Expense_mdal_xpath + "//input[@placeholder='Expense Code *']";
	private String ExpenseDate_txt_xpath = Expense_mdal_xpath + "//input[@placeholder='Expense Date *']";
	private String ReportDate_txt_xpath = Expense_mdal_xpath + "//input[@placeholder='Report Date *']";
	private String InvoiceNo_txt_xpath = Expense_mdal_xpath + "//div[@org-placeholder='Invoice No']//input";
	private String InvoiceDate_txt_xpath = Expense_mdal_xpath + "//div[@org-placeholder='Invoice Date']//input";
	private String ClientView_opt_xpath = Expense_mdal_xpath + "//label[contains(text(),'Client')]/../input[@type = 'checkbox']";
	private String TransfView_opt_xpath = Expense_mdal_xpath + "//label[contains(text(),'Transferee')]/../input[@type = 'checkbox']";
	private String Reject_btn_xpath = Expense_mdal_xpath + "//button[contains(text(),'Reject')]";
	private String Approve_btn_xpath = Expense_mdal_xpath + "//button[contains(text(),'Approve')]";
	private String Submit_btn_xpath = Expense_mdal_xpath + "//button[contains(text(),'Submit')]";
	private String Cancel_btn_xpath = Expense_mdal_xpath + "//button[contains(text(),'Cancel')]";

	// DEFINE ELEMENTS
	public WebElement Expense_mdal() {
		return GetElement(Expense_mdal_xpath);
	}
	
	public WebElement TransactionID_txt() {
		return GetElement(TransactionID_txt_xpath);
	}
	
	public WebElement FinalAmount_txt() {
		return GetElement(FinalAmount_txt_xpath);
	}
	
	public WebElement ExpenseCode_txt() {
		return GetElement(ExpenseCode_txt_xpath);
	}
	
	public WebElement ExpenseDate_txt() {
		return GetElement(ExpenseDate_txt_xpath);
	}
	
	public WebElement ReportDate_txt() {
		return GetElement(ReportDate_txt_xpath);
	}
	
	public WebElement InvoiceNo_txt() {
		return GetElement(InvoiceNo_txt_xpath);
	}
	
	public WebElement InvoiceDate_txt() {
		return GetElement(InvoiceDate_txt_xpath);
	}
	
	public WebElement ClientView_opt() {
		return GetElement(ClientView_opt_xpath);
	}
	
	public WebElement TransfView_opt() {
		return GetElement(TransfView_opt_xpath);
	}
	
	public WebElement Reject_btn() {
		return GetElement(Reject_btn_xpath);
	}
	
	public WebElement Approve_btn() {
		return GetElement(Approve_btn_xpath);
	}
	
	public WebElement Submit_btn() {
		return GetElement(Submit_btn_xpath);
	}
	
	public WebElement Cancel_btn() {
		return GetElement(Cancel_btn_xpath);
	}
	
//====================================METHOD
	
	public void ProcessExp_func(Approval approval, SubExpense_Info subexp)
	{
		Optimize_ElementSendkey(TransactionID_txt(),subexp.Transaction);
		
		Optimize_ElementSendkey(ExpenseCode_txt(), subexp.ExpenseCode);
		
		Optimize_ElementSendkey(ExpenseDate_txt(), subexp.Exp_Date.toString());
		
		Optimize_ElementSendkey(InvoiceNo_txt(), subexp.InvoiceNo);
		
		Optimize_ElementSendkey(InvoiceDate_txt(), subexp.InvoiceDate.toString());
	
		subexp.SubExApproval = approval;
			
		if(approval==Approval.Approve)
		{
			Optimize_ElementClick(Approve_btn());
		}
			
		else if(approval==Approval.Reject)
	
			Optimize_ElementClick(Reject_btn());
	}
	
	public void EditExpense_func(SubExpense_Info subexp, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementSendkey(TransactionID_txt(),subexp.Transaction);
		
		Optimize_ElementSendkey(ExpenseCode_txt(), subexp.ExpenseCode);
		
		Optimize_ElementSendkey(ExpenseDate_txt(), subexp.Exp_Date.toString());
		
		Optimize_ElementSendkey(InvoiceNo_txt(), subexp.InvoiceNo);
		
		Optimize_ElementSendkey(InvoiceDate_txt(), subexp.InvoiceDate.toString());
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Submit_btn());

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When updating the general info",false);
		}
		else if(button.equals("Cancel")||button.equals("cancel"))
		{
			Optimize_ElementClick(Cancel_btn());
		}
	}
	
	public void VerifyExpDetail(SubExpense_Info subexp)
	{
		String area = "";
		String field_name = "";
		String note = "- View Mode";
		
		area = "General Information";
		field_name = "Transaction ID";
		VerifyFieldValueEqual_func(area+field_name, TransactionID_txt(), subexp.Transaction, note);
		
		field_name = "Final Amount";
		VerifyFieldValueEqual_func(area+field_name, FinalAmount_txt(), subexp.Amount.toString(), note);
		
		field_name = "Expense Code";
		VerifyFieldValueEqual_func(area+field_name, ExpenseCode_txt(), subexp.ExpenseCode, note);	
		
		//Verify split expense code
		
		field_name = "Expense Date";
		VerifyFieldValueEqual_func(area+field_name, ExpenseDate_txt(), subexp.Exp_Date.toString(), note);
		
		field_name = "Invoice No";
		VerifyFieldValueEqual_func(area+field_name, InvoiceNo_txt(), subexp.InvoiceNo, note);
		
		field_name = "Invoice Date";
		VerifyFieldValueEqual_func(area+field_name, InvoiceDate_txt(), subexp.InvoiceDate.toString(), note);
		
		//Verify payment info
		area = "General Information";
		field_name = "Client View";
		VerifyFieldValueEqual_func(area+field_name, ClientView_opt(), Boolean.toString(subexp.ClientView),"");
		
		field_name = "Transferee View";
		VerifyFieldValueEqual_func(area+field_name, TransfView_opt(), Boolean.toString(subexp.TransfView),"");
	}
}
