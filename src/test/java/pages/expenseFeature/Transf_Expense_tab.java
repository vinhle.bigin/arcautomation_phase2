package pages.expenseFeature;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Transferee_Info;
import configuration.DriverConfig.DriverBase;


public class Transf_Expense_tab extends PageObjects{
	
	public Transf_Expense_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String Expense_tab_xpath = "//a[@href = '#expense-services']";
	private String NewExpense_btn_xpath = "//div[@class='form-group action-container']//button[contains(text(),'New Expense')]";
	private String Search_txt_xpath = "//div[@id='table-list-expense']//input";
	private String Finance_tbl_xpath = "//div[@id='table-list-expense']";
	private String Edit_btn_xpath = Expense_tab_xpath + "//a[@data-original-title='Edit']";
	private String Remove_btn_xpath = Expense_tab_xpath + "//a[@data-original-title='Remove']";
	private String Download_btn_xpath = Expense_tab_xpath + "//a[@data-original-title='Download']";

	public WebElement Expense_tab() {
		return GetElement(Expense_tab_xpath);
	}
	
	public WebElement NewExpense_btn() {
		return GetElement(NewExpense_btn_xpath);
	}
	
	public WebElement Search_txt() {
		return GetElement(Search_txt_xpath);
	}
	
	public WebElement Finance_tbl() {
		return GetElement(Finance_tbl_xpath);
	}
	
	public WebElement Edit_btn() {
		return GetElement(Edit_btn_xpath);
	}
	
	public WebElement Remove_btn() {
		return GetElement(Remove_btn_xpath);
	}

	public WebElement Download_btn() {
		return GetElement(Download_btn_xpath);
	}

}
