package pages.expenseFeature;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;
import businessObjects.Activity_Info;
import businessObjects.BankAccount_Info;
import businessObjects.ReimBurs_Info;
import businessObjects.SubExpense_Info;
import businessObjects.Transferee_Info;
import pages.ARCLoading;
import pages.Messages_Notification;
import pages.NewTransf_mdal;
import pages.Shortcuts_menu;
import pages.Transf_GenInfo_tab;
import baseClasses.PageObjects;

public class ExpenseReim_page extends ExpenseReim_modal{
	
	public ExpenseReim_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String ExpenseReim_tbl_xpath = "//div[@id = 'table-expense-reibursement']//table/tbody";
	private String ClaimExpense_btn_xpath = "//button[contains(text(),'Claim Expense')]";
	private String Search_txt_xpath = "//div[@id='table-expense-reibursement']//input[@class='form-control ']";
	private String ViewExpReim_btn_xpath = "//a[@data-original-title='View']";
	private String EditExpReim_btn_xpath = ExpenseReim_tbl_xpath + "//a[@data-original-title='Edit']";
	private String RemoveExpReim_btn_xpath = ExpenseReim_tbl_xpath + "//a[@data-original-title='Remove']";
	
	
	// DEFINE ELEMENTS
	public WebElement ExpenseReim_tbl() {
		return GetElement(ExpenseReim_tbl_xpath);
	}
	
	public WebElement ClaimExpense_btn() {
		return GetElement(ClaimExpense_btn_xpath);
	}
	
	public WebElement Search_txt() {
		return GetElement(Search_txt_xpath);
	}
	
	public WebElement ViewExpReim_btn() {
		return GetElement(ViewExpReim_btn_xpath);
	}
	
	public WebElement EditExpReim_btn() {
		return GetElement(EditExpReim_btn_xpath);
	}
	
	public WebElement RemoveExpReim_btn() {
		return GetElement(RemoveExpReim_btn_xpath);
	}
	

//==================METHOD
	public void SearchExpReim_func(String ReportName) 
	{
		try 
		{
			Thread.sleep(10000);

			ClearThenEnterValueToField(Search_txt(), ReportName);

			Search_txt().sendKeys(Keys.ENTER);

			Thread.sleep(1000);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

@Override
	public void CreateExpReim_func(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment, String button) 
	{
		Optimize_ElementClick(ClaimExpense_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		
		super.CreateExpReim_func(reim, subexp, payment, button);
		
		System.out.println("=====STeps - Expense Reimbursement is added: ["+reim.Name+"]");
	}

@Override
	public void EditExpReim_func(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment, String button) 
	{
		Optimize_ElementClick(this.EditExpReim_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		
		super.EditExpReim_func(reim, subexp, payment, button);
		
		System.out.println("=====STeps - Expense Reimbursement is added: ["+reim.Name+"]");
	}

	public void GotoExpReimDetail() 
	{
		try {
			Optimize_ElementClick(ViewExpReim_btn());

			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

		} catch (Exception e) {
			TestConfigs.glb_TCFailedMessage += "CANNOT NAVIGATE TO EXPENSES REIM DETAIL:" + e.getMessage() + ".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}
	
	public void DeleteExpReim_func(String reportname) 
	{
		RemoveExpReim_btn().click();

		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

		if (Messages_Notification.OppsOk_btn() != null)
			Messages_Notification.OppsOk_btn().click();
		else
			Messages_Notification.Delete_btn().click();

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,
				"when Delete Expense Reimbursement.", false);

	}// end void
	
	
//===============================VERIFY METHODS
	
	public void VerifyExpReimExistTable_func(ReimBurs_Info reim) 
	{
		Boolean exist = false;
		List<String> records = GetTableRecordsPerPaging_func(ExpenseReim_tbl());

		for (String index_str : records) {
			if (index_str.contains(reim.Name)) {
				exist = true;
				break;
			} // end if
		} // end foreach

		if (!exist) {
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "Expense Reimbursement[" + reim.Name + "] NOT exist in table.\n";
		} // end if

	}
	
	public void VerifyExpReimDetail(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment)
	{
		GotoExpReimDetail();
		
		new ExpenseReim_modal(driverbase).VerifyExpReimDetail(reim, subexp, payment);
	}
	
	public void Verify_ExpReim_Not_Exist_Table_func(ReimBurs_Info reim, String note) 
	{
		VerifyDataNotExistTable_func(ExpenseReim_tbl(), reim.Name, note);
	}

}
