package pages.expenseFeature;

import java.util.List;

import org.assertj.core.internal.Objects;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Transferee_Info;
import configuration.DriverConfig.DriverBase;


public class NewTaxYear_modal extends PageObjects{
	
	public NewTaxYear_modal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String AddTaxYear_mdal_xpath = "//div[@id='loading-modal-finance-create']";
	private String TaxYear_slect_xpath = AddTaxYear_mdal_xpath + "//div[@org-placeholder='Tax Year *']//select";
	private String GrossupPolicy_slect_xpath = AddTaxYear_mdal_xpath + "//div[@org-placeholder='Gross-up Policy']//select";
	private String WHPolicy_slect_xpath = AddTaxYear_mdal_xpath + "//div[@org-placeholder='WH Policy']//select";
	private String MarriageStatus_slect_xpath = AddTaxYear_mdal_xpath + "//div[@org-placeholder='Marriage Status *']//select";
	private String TaxCountry_slect_xpath = AddTaxYear_mdal_xpath + "//div[@org-placeholder='Tax Country *']//select";
	private String TaxState_slect_xpath = AddTaxYear_mdal_xpath + "//div[@org-placeholder='Tax State *']//select";
	private String Submit_btn_xpath = "//button[@data-control='create']";
	private String Cancel_btn_xpath = "//button[@data-control='cancel-create']";
	//Edit
	private String EditTaxYear_mdal_xpath = "//div[@id='loading-modal-finance-edit']";
	private String Edit_btn_xpath = EditTaxYear_mdal_xpath + "//a[@data-original-title='Edit']";
	private String MovedFromState_slect_xpath = EditTaxYear_mdal_xpath + "//div[@org-placeholder='Moved From State *']//select";
	private String YTD_txt_xpath = EditTaxYear_mdal_xpath + "//div[@org-placeholder='YTD FICA Wage For Gross-up/Withholding *']//input";
	private String FICA_txt_xpath = EditTaxYear_mdal_xpath + "//div[@org-placeholder='FICA Wage for NetCheck Withholding *']//input";
	private String GrossWages_txt_xpath = EditTaxYear_mdal_xpath + "//div[@org-placeholder='Gross-up Tax State Wages *']//input";
	private String AnnualWages_txt_xpath = EditTaxYear_mdal_xpath + "//div[@org-placeholder='Annual Wages *']//input";
	private String OtherWages_txt_xpath = EditTaxYear_mdal_xpath + "//div[@org-placeholder='Other Wages *']//input";
	
	// DEFINE ELEMENTS
	public WebElement AddTaxYear_mdal() {
		return GetElement(AddTaxYear_mdal_xpath);
	}
	
	public WebElement TaxYear_slect() {
		return GetElement(TaxYear_slect_xpath);
	}
	
	public WebElement GrossupPolicy_slect() {
		return GetElement(GrossupPolicy_slect_xpath);
	}
	
	public WebElement WHPolicy_slect() {
		return GetElement(WHPolicy_slect_xpath);
	}
	
	public WebElement MarriageStatus_slect() {
		return GetElement(MarriageStatus_slect_xpath);
	}
	
	public WebElement TaxCountry_slect() {
		return GetElement(TaxCountry_slect_xpath);
	}
	
	public WebElement TaxState_slect() {
		return GetElement(TaxState_slect_xpath);
	}
	
	public WebElement Submit_btn() {
		return GetElement(Submit_btn_xpath);
	}
	
	public WebElement Cancel_btn() {
		return GetElement(Cancel_btn_xpath);
	}
	
	public WebElement EditTaxYear_mdal() {
		return GetElement(EditTaxYear_mdal_xpath);
	}
	
	public WebElement Edit_btn() {
		return GetElement(Edit_btn_xpath);
	}
	
	public WebElement MovedFromState_slect() {
		return GetElement(MovedFromState_slect_xpath);
	}
	
	public WebElement YTD_txt() {
		return GetElement(YTD_txt_xpath);
	}
	
	public WebElement FICA_txt() {
		return GetElement(FICA_txt_xpath);
	}
	
	public WebElement GrossWages_txt() {
		return GetElement(GrossWages_txt_xpath);
	}
	
	public WebElement AnnualWages_txt() {
		return GetElement(AnnualWages_txt_xpath);
	}
	
	public WebElement OtherWages_txt() {
		return GetElement(OtherWages_txt_xpath);
	}
}
