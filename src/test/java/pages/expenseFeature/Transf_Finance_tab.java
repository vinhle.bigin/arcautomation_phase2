package pages.expenseFeature;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Transferee_Info;
import configuration.DriverConfig.DriverBase;


public class Transf_Finance_tab extends PageObjects{
	
	public Transf_Finance_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private String Finance_tab_xpath = "//a[@href = '#finance-services']";
	private String NewTaxYear_btn_xpath = "//button[contains(text(),'New Tax Year')]";
	private String Search_txt_xpath = "//div[@id='table-list-finance']//input";
	private String Finance_tbl_xpath = "//div[@id='table-list-finance']";
	private String Edit_btn_xpath = Finance_tbl_xpath + "//a[@data-original-title='Edit']";
	private String Remove_btn_xpath = Finance_tbl_xpath + "//a[@data-original-title='Remove']";

	public WebElement Finance_tab() {
		return GetElement(Finance_tab_xpath);
	}
	
	public WebElement NewTaxYear_btn() {
		return GetElement(NewTaxYear_btn_xpath);
	}
	
	public WebElement Search_txt() {
		return GetElement(Search_txt_xpath);
	}
	
	public WebElement Finance_tbl() {
		return GetElement(Finance_tbl_xpath);
	}
	
	public WebElement Edit_btn() {
		return GetElement(Edit_btn_xpath);
	}
	
	public WebElement Remove_btn() {
		return GetElement(Remove_btn_xpath);
	}

}
