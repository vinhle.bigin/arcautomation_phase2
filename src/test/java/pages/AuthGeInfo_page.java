package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Auth_Info;
import configuration.DriverConfig.DriverBase;

public class AuthGeInfo_page extends PageObjects {

	private ARCLoading ARCLoading;
	
	private String Policy_slect_xpath = "//div[@org-placeholder='Policy']//select";
	private String FirstName_txt_xpath = "//div[@org-placeholder='First Name']//input";
	private String Mid_txt_xpath = "//div[@org-placeholder='Mid']//input";
	private String LastName_txt_xpath = "//div[@org-placeholder='Last Name']//input";
	private String Status_lnk_xpath = "//a[contains(text(),'status_progress')]";
	private String Auth_Details_section_xpath = "//div[@id='loading-details-authorization']";
	
	
	//DEFINE ELEMENTS

	public  WebElement Auth_Details_section(){
		return GetElement(Auth_Details_section_xpath);
}
	
	public  WebElement FirstName_txt(){
		return GetElement(FirstName_txt_xpath);
}
	public  WebElement Mid_txt(){
		return GetElement(Mid_txt_xpath);
}
	public  WebElement LastName_txt(){
		return GetElement(LastName_txt_xpath);
	}
	public  WebElement Policy_slect(){
		return GetElement(Policy_slect_xpath);
}
	public  WebElement Status_Progress(String status_str) {
		try{
			String xpath_str = Status_lnk_xpath.replace("status_progress", status_str);
			
			WebElement temp_element = GetElement(xpath_str);
					
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}
	
	
	//METHOD
	
	public AuthGeInfo_page(DriverBase driverbase) {
		super(driverbase);
		
		ARCLoading = new ARCLoading(driverbase);
	}


	public void VerifyAuthInfoViewMode_func(Auth_Info auth_info) {
		
		if(Auth_Details_section() == null)
		{
			GoToLatestBrowserTab();
		}
		
		ARCLoading.Wait_Util_FinishedPageLoading();
		
		String fieldname = "";
		String Area = "Authorizarion -";
		
		fieldname = "FirstName_txt";
		VerifyFieldValueEqual_func(Area+fieldname, FirstName_txt(), auth_info.firstname,"- ViewMode");

		fieldname = "LastName_txt";
		VerifyFieldValueEqual_func(Area+fieldname, LastName_txt(), auth_info.lastname,"- ViewMode");
	
		fieldname = "Policy_slect";
		VerifyFieldValueEqual_func(Area+fieldname, Policy_slect(), auth_info.Policy,"- ViewMode");
		
		fieldname = "Policy_slect";
		VerifyFieldValueEqual_func(Area+fieldname, Status_Progress(auth_info.AuthStatus), auth_info.AuthStatus,"- ViewMode");

	}
	
}
