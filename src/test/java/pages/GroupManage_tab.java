package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.CustomGroup_Info;
import configuration.DriverConfig.DriverBase;

public class GroupManage_tab extends PageObjects{
	
	public GroupManage_tab(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	public ARCLoading ARCLoading;
	public Messages_Notification Messages_Notification;
	
	private  String ManageGroup_tab_xpath= "//label[@id='manage-group-option']";
	private  String FieldType_ddl_xpath= "//select[@placeholder ='Field Type']";
	private  String Tab_ddl_xpath= "//select[@placeholder ='Tab']";
	private  String Filter_btn_xpath= "//button[@id ='filter-layout']";
	private  String NewGroup_btn_xpath= "//button[@id ='create-group']";
	private  String Search_txt_xpath= "//input[@type='search']";
	private  String GroupList_tbl_xpath= "//table[@id='tb-list-group']/tbody";
	private  String ModalFieldType_ddl_xpath= "//div[@class='modal-body clearfix']//select[@placeholder ='Field Type']";
	private  String ModalTab_ddl_xpath= "//div[@class='modal-body clearfix']//select[@placeholder ='Tab']";
	private  String ModalGroupName_txt_xpath= "//div[@class='modal-body clearfix']//input[@placeholder ='Group Name']";
	private  String ModalCreate_btn_xpath= "//div[@id='group-custom-field-form']//button[@data-control ='create']";
	private  String ModalCancel_btn_xpath= "//div[@id='group-custom-field-form']//button[@data-dismiss ='modal'][1]";
	private  String Edit_icon_xpath= ".//span[@data-toggle='modal']";
	private  String Remove_icon_xpath= ".//span[@title='Remove group']";
	private  String ModalUpdate_btn_xpath= "//div[@id='group-custom-field-form']//button[@data-control ='update']";
			
			

	
	
	
	public  WebElement ManageGroup_tab() {
	  return GetElement(ManageGroup_tab_xpath);
	 }
public  WebElement FieldType_ddl() {
	  return GetElement(FieldType_ddl_xpath);
	 }
public  WebElement Tab_ddl() {
	  return GetElement(Tab_ddl_xpath);
	 }
public  WebElement Filter_btn() {
	  return GetElement(Filter_btn_xpath);
	 }
public  WebElement NewGroup_btn() {
	  return GetElement(NewGroup_btn_xpath);
	 }
public  WebElement Search_txt() {
	  return GetElement(Search_txt_xpath);
	 }
public  WebElement GroupList_tbl() {
	  return GetElement(GroupList_tbl_xpath);
	 }
public  WebElement ModalFieldType_ddl() {
	  return GetElement(ModalFieldType_ddl_xpath);
	 }
public  WebElement ModalTab_ddl() {
	  return GetElement(ModalTab_ddl_xpath);
	 }
public  WebElement ModalGroupName_txt() {
	  return GetElement(ModalGroupName_txt_xpath);
	 }
public  WebElement ModalCreate_btn() {
	  return GetElement(ModalCreate_btn_xpath);
	 }
public  WebElement ModalCancel_btn() {
	  return GetElement(ModalCancel_btn_xpath);
	 }


public  WebElement ModalUpdate_btn() {
	  return GetElement(ModalUpdate_btn_xpath);
	 }

	
	public  WebElement Edit_icon(String groupname) {
		try{

			WebElement temp_element = GetElementInTable(groupname, GroupList_tbl(), Edit_icon_xpath);
			//webDriver.findElement(By.xpath());
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}

	public  WebElement Remove_icon(String groupname) {
		try{

			WebElement temp_element = GetElementInTable(groupname, GroupList_tbl(), Remove_icon_xpath);
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null;
		}
	}
	
	/////////================================METHOD
	public  void VerifyModalViewMode_func(CustomGroup_Info group)
	{
		String area = "";
		String field_name = "";
		String mode = "View Mode";
		
		ARCLoading ARCLoading = new ARCLoading(driverbase);

		//WaitForElementNotDisplay(LandingPage.LoadingArc())

		ARCLoading.Wait_Util_FinishedPageLoading();

		Optimize_ElementClick(Edit_icon(group.Name));

		ARCLoading.Wait_Util_FinishedPageLoading();

		//Wait_For_ElementDisplay(ModalGroupName_txt());

		///ORG RESIDENCE
		area = "Custom Group - ";

		field_name = "ModalFieldType_ddl";
		VerifyFieldValueEqual_func(area+field_name , ModalFieldType_ddl(),group.FieldType, mode);

		field_name = "OrgRes_Addrs2";
		VerifyFieldValueEqual_func(area+field_name , ModalTab_ddl(),group.Tab , mode);

		field_name = "ModalGroupName_txt";
		VerifyFieldValueEqual_func(area+field_name , ModalGroupName_txt(),group.Name , mode);


	}

	public  void FillModalInfo_func(CustomGroup_Info group,String Action)
	{

		//Wait_For_ElementDisplay(ModalGroupName_txt());
		String note_str = "";
		if(Action=="Create")
		{
			
			note_str = "Custom Group Details - Field Type Dropdown.";
			
			SelectDropdownItem(ModalFieldType_ddl(), group.FieldType,note_str);

			note_str = "Custom Group Details - Tab Dropdown.";
			
			SelectDropdownItem(ModalTab_ddl(), group.Tab,note_str);
		}

		ClearThenEnterValueToField(ModalGroupName_txt(),group.Name);
	}
	
	
	public void CreateGroup_func(CustomGroup_Info group_info) {
		
		try {
			ARCLoading = new ARCLoading(driverbase);
			Messages_Notification = new Messages_Notification(driverbase);
			
		
			//Wait_For_ElementDisplay(ManageGroup_tab());
	
			ManageGroup_tab().click();
	
			ARCLoading.Wait_Util_FinishedPageLoading();
	
			//Wait_For_ElementDisplay(GroupList_tbl());
	
			NewGroup_btn().click();
	
			FillModalInfo_func(group_info, "Create");
	
			ModalCreate_btn().click();
	
			Thread.sleep(2000);
	
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "create new custom group",false);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}//end void
	
	

	public void UpdateGroup_func(String oldgroupname_str,CustomGroup_Info newgroupinfo) {
		
		try {
			ARCLoading = new ARCLoading(driverbase);

			Messages_Notification = new Messages_Notification(driverbase);
			
			ManageGroup_tab().click();
	
			ARCLoading.Wait_Util_FinishedPageLoading();
			
		//	Wait_For_ElementDisplay(GroupList_tbl());
	
			Edit_icon(oldgroupname_str).click();
	
			FillModalInfo_func(newgroupinfo,"Update");
	
			ModalUpdate_btn().click();
	
			Thread.sleep(2000);
	
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "update new custom group",false);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


public void RemoveGroup_func(CustomGroup_Info group_info) {
	try {
		Messages_Notification = new Messages_Notification(driverbase);
			

		Remove_icon(group_info.Name);

		Messages_Notification.ConfirmYes_btn().click();

		Thread.sleep(2000);

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "remove custom group",false);
	
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}

}
