package pages;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;

import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class NewAccount_mdal extends PageObjects{
	
	public NewAccount_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String actived_detail_mdal_xpath = "//div[contains(@class,'modal') and contains(@style,'display: block')]";
	
	private  String TempPass_text_xpath = "//label[contains(text(),'Temporary Password')]/..//p";
	
	private  String UserName_text_xpath = "//div[contains(text(),'Username:')]/b";
	
	private  String Login_Info_text_xpath = "//div[@class='m-b-3']/div[1]";
	
	
	private  String Close_btn_xpath = actived_detail_mdal_xpath + "//button[@data-dismiss='modal']";
	

	public  WebElement Login_Info_text(){
		return GetElement(Login_Info_text_xpath);
	
	}
	
	public  WebElement TempPass_text(){
		return GetElement(TempPass_text_xpath);
	
}
	
	public  WebElement UserName_text(){
		return GetElement(UserName_text_xpath);
	
}
	
	public  WebElement Close_btn(){
		return GetElement(Close_btn_xpath);
	
}


	
	
	public  void Close_Modal_func()
	{
		try {
		
	//	Wait_For_ElementDisplay(Close_btn());
		
		
			try {
				
				if(Close_btn()!=null)
				Optimize_ElementClick(Close_btn());
				
			} catch(StaleElementReferenceException e1)
			
			{
				Optimize_ElementClick(Close_btn());
			}
		
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Wait_Ultil_ElementNotDisplay(Close_btn_xpath);
		
		
	}
	
	public  String Get_UserName_func()
	{
		return UserName_text().getText();
	}
	
	public  String Get_Temp_Password_func()
	{
		return TempPass_text().getText();
	}
	
	public  void Verify_Login_Info_Text_Correct_func(String full_name)
	{
		String expected_text = "The Login information of "+full_name +" is:";
		String fieldname_str = "Login Account Info Text";
		VerifyFieldValueEqual_func(fieldname_str, Login_Info_text(), expected_text, "");
	}
	
	public  void Verify_Modal_Account_Info_Correct_func(String full_name)
	{
		Verify_Login_Info_Text_Correct_func(full_name);
		
		//Verify username is not empty
		String user_name = Get_UserName_func();
		String temp_pass = Get_Temp_Password_func();
		
		if(user_name.equals(""))
		{
			TestConfigs.glb_TCStatus=false;
			TestConfigs.glb_TCFailedMessage+="UserName is not generated[Observed: "+user_name+"]";
		}
		
		//Verify temp pass is generated
		if(temp_pass.equals(""))
		{
			TestConfigs.glb_TCStatus=false;
			TestConfigs.glb_TCFailedMessage+="Temp Pass is not generated[Observed: "+temp_pass+"]";
		
		}
		
		
		
	}

}
