package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.HPService_Info;
import configuration.DriverConfig.DriverBase;
import custom_Func.Data_Optimize;



public class Transf_HomePurchase_details extends PageObjects{

	public Transf_HomePurchase_details(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String HP_LoanAmount_txt_xpath= "//div[@org-placeholder='Loan Amount']//input";

	private  String HP_IsTransfRent_chk_xpath= "//label[contains(text(),'Transferee Renting')]/..//input";
	private  String HP_SalesPrice_txt_xpath= "//input[@placeholder ='Sales Price']";
	
	private  String HP_ExactHouseTripDate_txt_xpath= "//div[@org-placeholder='Exact Househunting trip Date']//input";
	private  String HP_ExactCloseDate_txt_xpath= "//div[@org-placeholder ='Exact Close Date']//input";
	private  String HP_EscrowPercent_txt_xpath= "//div[@org-placeholder='Escrow Fee Percent']//input";
	private  String HP_BuyOutDate_txt_xpath= "//div[@org-placeholder='Buy-Out Accepted Date']//input";
	private  String HP_CmsionPercnt_txt_xpath= "//input[@placeholder ='Commission Percent']";
	private  String HP_RebateAmount_txt_xpath= "//div[@org-placeholder='Rebate Amount']//input";
	private  String HP_EscrowFeeAmount_txt_xpath= "//div[@org-placeholder='Escrow Fee Amount']//input";
	private  String HP_ApproCloseDate_txt_xpath = "//div[@org-placeholder = 'Approximate Close Date']//input";
	private  String HP_ApproHuntTripDate_txt_xpath = "//div[@org-placeholder = 'Approximate Househunting trip Date']//input";

	//Fee and Invoice
	private  String HP_IsRebateSent_chk_xpath= "//label[contains(text(),'Rebate Sent')]/..//input";
	private  String HP_IsInvoiceCreate_chk_xpath= "//label[contains(text(),'Invoice Created')]/..//input";
	private  String HP_IsInvoiceClose_chk_xpath= "//label[contains(text(),'Invoice Closed Out')]/..//input";
	private  String HP_IsEscrReceive_chk_xpath= "//label[contains(text(),'Escrow Fee Received')]/..//input";
	private  String HP_IsSignRefAgree_chk_xpath= "//label[contains(text(),'Signed Referral Fee Agreement')]/..//input";
	
	
	private  String Update_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='update']";
	private  String Cancel_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='cancel-update']";
	private  String Edit_btn_xpath= "//a[@id='icon-edit-home-purchase-info-panel']";
	
	
	
	public  WebElement HP_LoanAmount_txt() {
	  return GetElement(HP_LoanAmount_txt_xpath);
	 }
	public  WebElement HP_ExactHouseTripDate_txt() {
	  return GetElement(HP_ExactHouseTripDate_txt_xpath);
	 }
	public  WebElement HP_IsTransfRent_chk() {
	  return GetElement(HP_IsTransfRent_chk_xpath);
	 }
	public  WebElement HP_SalesPrice_txt() {
	  return GetElement(HP_SalesPrice_txt_xpath);
	 }
	public  WebElement HP_ExactCloseDate_txt() {
	  return GetElement(HP_ExactCloseDate_txt_xpath);
	 }
	public  WebElement HP_EscrowPercent_txt() {
	  return GetElement(HP_EscrowPercent_txt_xpath);
	 }
	
	public  WebElement Update_btn() {
		  return GetElement(Update_btn_xpath);
		 }
		public  WebElement Cancel_btn() {
		  return GetElement(Cancel_btn_xpath);
		 }
		
		public  WebElement Edit_btn() {
			  return GetElement(Edit_btn_xpath);
			 }

		
		public  WebElement HP_BuyOutDate_txt() {
		  return GetElement(HP_BuyOutDate_txt_xpath);
		 }
		public  WebElement HP_CmsionPercnt_txt() {
		  return GetElement(HP_CmsionPercnt_txt_xpath);
		 }
		public  WebElement HP_RebateAmount_txt() {
		  return GetElement(HP_RebateAmount_txt_xpath);
		 }
		public  WebElement HP_EscrowFeeAmount_txt() {
		  return GetElement(HP_EscrowFeeAmount_txt_xpath);
		 }
		public  WebElement HP_IsRebateSent_chk() {
		  return GetElement(HP_IsRebateSent_chk_xpath);
		 }
		public  WebElement HP_IsInvoiceCreate_chk() {
		  return GetElement(HP_IsInvoiceCreate_chk_xpath);
		 }
		public  WebElement HP_IsInvoiceClose_chk() {
		  return GetElement(HP_IsInvoiceClose_chk_xpath);
		 }
		public  WebElement HP_IsEscrReceive_chk() {
		  return GetElement(HP_IsEscrReceive_chk_xpath);
		 }
		public  WebElement HP_IsSignRefAgree_chk() {
		  return GetElement(HP_IsSignRefAgree_chk_xpath);
		 }
		
		public  WebElement HP_ApproCloseDate_txt() {
		  return GetElement(HP_ApproCloseDate_txt_xpath);
		 }
	public  WebElement HP_ApproHuntTripDate_txt() {
		  return GetElement(HP_ApproHuntTripDate_txt_xpath);
		 }


	
	public  void FillHPInfo_func(HPService_Info service,String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Edit_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();


//	Wait_For_ElementDisplay(HP_BuyOutDate_txt());

		ClearThenEnterValueToField(HP_SalesPrice_txt(), service.SalePrice);

		ClearThenEnterValueToField(HP_LoanAmount_txt(), service.LoanAmount);

		ClearThenEnterValueToField(HP_CmsionPercnt_txt(), service.CommissionPercent);

		Optimize_ElementSendkey(HP_ExactCloseDate_txt(), service.CloseDate);
		
		HP_ExactCloseDate_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(HP_ExactHouseTripDate_txt(), service.HouseHuntTripDate);

		HP_ExactHouseTripDate_txt().sendKeys(Keys.ENTER);
		
		String RebateAmount = Data_Optimize.Format_Double_WithCurrency(service.RebateAmount);
				
		Optimize_ElementSendkey(HP_RebateAmount_txt(), RebateAmount);

		//FEE AND INVOICE
		//HS_RefPercent_txt
		
		Optimize_ElementSendkey(HP_ApproHuntTripDate_txt(),service.ApproHuntTripDate);

		HP_ApproHuntTripDate_txt().sendKeys(Keys.TAB);
		
		
		//HP_ApproCloseDate_txt
		Optimize_ElementSendkey(HP_ApproCloseDate_txt(),service.ApproCloseDate);

		HP_ApproCloseDate_txt().sendKeys(Keys.TAB);

		Optimize_ElementClick(HP_IsRebateSent_chk());

		Optimize_ElementClick(HP_IsInvoiceCreate_chk());

		Optimize_ElementClick(HP_IsInvoiceClose_chk());

		Optimize_ElementClick(HP_IsSignRefAgree_chk());

		Optimize_ElementClick(HP_IsTransfRent_chk());

		if(button=="Submit")
			Update_btn().click();
		else if(button=="Cancel")
			Cancel_btn().click();

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "when update HP_ServiceInfo",false);
		
		
	}//end void
	
	public  void Verify_HPServiceDetail_func(HPService_Info service)
	{
		String field_name = "";

		field_name = "HP_SalePrice_txt";

		VerifyFieldValueEqual_func(field_name,HP_SalesPrice_txt(), service.SalePrice,"");

		field_name = "HP_LoanAmount_txt";
		VerifyFieldValueEqual_func(field_name,HP_LoanAmount_txt(), service.LoanAmount,"");

		field_name = "HP_CmsionPercnt_txt";
		VerifyFieldValueEqual_func(field_name,HP_CmsionPercnt_txt(), service.CommissionPercent,"");

		field_name = "HP_ExactCloseDate_txt";
		VerifyFieldValueEqual_func(field_name,HP_ExactCloseDate_txt(), service.CloseDate,"");

		field_name = "HP_ExactHouseTripDate_txt";
		VerifyFieldValueEqual_func(field_name,HP_ExactHouseTripDate_txt(), service.HouseHuntTripDate,"");

		field_name = "HP_RebateAmount_txt";
		String RebateAmount = Data_Optimize.Format_Double_WithCurrency(service.RebateAmount);
		
		VerifyFieldValueEqual_func(field_name,HP_RebateAmount_txt(),RebateAmount ,"");

	//	field_name = "HP_EscrowPercent_txt";
	//	VerifyFieldValueEqual_func(field_name,HP_EscrowPercent_txt(), service.EscrowFeePercent,"");

	//	field_name = "HP_EscrowFeeAmount_txt";
	//	VerifyFieldValueEqual_func(field_name,HP_EscrowFeeAmount_txt(), service.EscrowFeeAmount,"");

		field_name = "HP_IsRebateSent_chk";
		VerifyFieldValueEqual_func(field_name,HP_IsRebateSent_chk(),service.IsRebateSent.toString(),"");

		field_name = "HP_IsInvoiceCreate_chk";
		VerifyFieldValueEqual_func(field_name,HP_IsInvoiceCreate_chk(), service.IsInvoiceCreate.toString(),"");

		field_name = "HP_IsInvoiceClose_chk";
		VerifyFieldValueEqual_func(field_name,HP_IsInvoiceClose_chk(), service.IsInvoiceClose.toString(),"");

//		field_name = "HP_IsEscrReceive_chk";
//		VerifyFieldValueEqual_func(field_name,HP_IsEscrReceive_chk(), service.IsEscrowFeeReceive.toString(),"");

		field_name = "HP_IsSignRefAgree_chk";
		VerifyFieldValueEqual_func(field_name,HP_IsSignRefAgree_chk(), service.IsSignRefFeeAgree.toString(),"");

		field_name = "HP_IsTransfRent_chk";
		VerifyFieldValueEqual_func(field_name,HP_IsTransfRent_chk(), service.IsTransfRent.toString(),"");
		
		field_name = "HP_ApproHuntTripDate_txt";
		VerifyFieldValueEqual_func(field_name,HP_ApproHuntTripDate_txt(), service.ApproHuntTripDate,"");
		
		field_name = "HP_ApproCloseDate_txt";
		VerifyFieldValueEqual_func(field_name,HP_ApproCloseDate_txt(), service.ApproCloseDate,"");
		
		
	}//end void

}
