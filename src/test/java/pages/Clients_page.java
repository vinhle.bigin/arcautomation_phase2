package pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;


public class Clients_page extends PageObjects{
	public Clients_page(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	/*private ARCLoading ARCLoading;
	private Messages_Notification Messages_Notification;
	private NewClient_mdal NewClient_mdal ;
	private Shortcuts_menu Shortcuts_menu ;*/
	
	
	private String NewClient_btn_xpath = "//button[contains(text(),'New Client')]";
	private String ClientList_tbl_xpath = "//table[@id = 'table-list-client']/tbody";
	private  String SearchClientName_txt_xpath = "//input[@name ='company_name']";
	private String Search_btn_xpath = "//button[contains(text(),'Search')]";
	private String EditClientProfile_btn_xpath = "//a[@id ='icon-edit-client-profile-panel-general-info']";
	private String EditClientStatus_btn_xpath = "//a[@id = 'icon-edit-client-status-panel-general-info']";
	private String CompanyName_input_xpath = "//input[@placeholder ='Company name']";
	private String ClientInfoUpdate_btn_xpath= "//div[@id ='edit-client-profile-panel-general-info']//button[@data-control = 'update']";
	private String ClientType_ddl_xpath = "//select[@placeholder = 'Client Type']";
	private String StatusClient_ddl_xpath = "//select[@placeholder = 'Status']";
	private String ClientStatusUpdate_btn_xpath = "//div[@id ='edit-client-status-panel-general-info']//button[@data-control = 'update']";
	private String Region_input_xpath = "//input[@placeholder ='Region']";
	private String Territory_ddl_xpath = "//select[@placeholder ='Territory']";
	private String Revenue_input_xpath = "//input[@placeholder ='Revenue']";
	private String OfEmployees_input_xpath = "//input[@placeholder= '# of Employees']";
	private String ReferredBy_ddl_xpath = "//select[@placeholder = 'Referred By']";
	private String Industry_ddl_xpath = "//select[@placeholder = 'Industry']";
	private String AccountManager_ddl_xpath = "//select[@placeholder = 'Account Manager']";
	
	//Go to Auth page

	
	
	//Preferences tab
	private String Preferences_tab_xpath = "//a[contains(text(),'Preferences')]";
	private String EditPrefer_btn_xpath = "//a[@id = 'icon-panel-edit-preference']";
	private String NoApproval_opt_xpath = "//label[contains(text(),'No Approval required')]/../input";
	private String AnyContact_opt_xpath = "//label[contains(text(),'Any contact can approve')]/../input";
	private String OneOfSelected_opt_xpath = "//label[contains(text(),'One of selected must approve')]/../input";
	private String EnteringContact_opt_xpath = "//label[contains(text(),'Entering contact must approve')]/../input";
	private String ClientContact_ddl_xpath ="//div[@class='area-one-approve']//ul[@class='b__multi__select__list']";
	private String ClientContact_arrow_xpath = "//div[@class='area-one-approve']//div[contains(@class,'iconC')]";
	private String ClientContact_txt_xpath = "//div[@class='area-one-approve']//input";

	private String Update_btn_xpath ="//button[contains(text(),'Update')]";
	
	
	
	//DEFINE ELEMENTS
	 public  WebElement NewClient_btn () {
	  return GetElement(NewClient_btn_xpath );
	 }
	 public  WebElement ClientList_tbl () {
	  return GetElement(ClientList_tbl_xpath );
	 }
	 public  WebElement SearchClientName_txt () {
	  return GetElement(SearchClientName_txt_xpath );
	 }
	 public  WebElement Search_btn () {
	  return GetElement(Search_btn_xpath );
	 }
	 public  WebElement EditClientProfile_btn () {
		  return GetElement(EditClientProfile_btn_xpath );
	 }
	 public  WebElement CompanyName_input () {
		  return GetElement(CompanyName_input_xpath );
	 }
	 public  WebElement ClientInfoUpdate_btn () {
		 return GetElement(ClientInfoUpdate_btn_xpath);
	 }
	 public  WebElement ClientType_ddl() {
		 return GetElement(ClientType_ddl_xpath);
	 }
	 public  WebElement EditClientStatus_btn () {
		 return GetElement(EditClientStatus_btn_xpath);
	 }
	 public  WebElement StatusClient_ddl() {
		 return GetElement(StatusClient_ddl_xpath);
	 }
	 public  WebElement ClientStatusUpdate_btn() {
		 return GetElement(ClientStatusUpdate_btn_xpath);
	 }
	 public  WebElement Region_input() {
		 return GetElement(Region_input_xpath);
	 }	 
	 public  WebElement Territory_ddl() {
		 return GetElement(Territory_ddl_xpath);
	 }	 
	 public  WebElement Revenue_txt() {
		 return GetElement(Revenue_input_xpath);
	 }
	 public  WebElement OfEmployees_txt(){
		 return GetElement(OfEmployees_input_xpath);
	 }
	 public  WebElement ReferredBy_ddl() {
		 return GetElement(ReferredBy_ddl_xpath);
	 }
	 public  WebElement Industry_ddl() {
		 return GetElement(Industry_ddl_xpath);
	 }
	 public  WebElement AccountManager_ddl() {
		 return GetElement(AccountManager_ddl_xpath);
	 }
	 public  WebElement ClientName_link(String clientfullname_str) {
			
			String TransfName_lnk_xapth = "//a[contains(text(),'"+clientfullname_str+"')]";
				
			return GetElement(TransfName_lnk_xapth);
		}
	
	 //Client Portal

	 
	//Preferences tab
	 public  WebElement Preferences_tab() {
		 return GetElement(Preferences_tab_xpath);
	 }
	 public  WebElement EditPrefer_btn() {
		 return GetElement(EditPrefer_btn_xpath);
	 }
	 public  WebElement NoApproval_radio() {
		 return GetElement(NoApproval_opt_xpath);
	 }
	 public  WebElement AnyContact_radio() {
		 return GetElement(AnyContact_opt_xpath);
	 }
	 public  WebElement OneOfSelected_radio() {
		 return GetElement(OneOfSelected_opt_xpath);
	 }
	 public  WebElement EnteringContact_radio() {
		 return GetElement(EnteringContact_opt_xpath);
	 }	 
	 public  WebElement ClientContact_ddl() {
		 return GetElement(ClientContact_ddl_xpath);
	 }
	 public  WebElement ClientContact_arrow() {
		 return GetElement(ClientContact_arrow_xpath);
	 }
	 public  WebElement ClientContact_txt() {
		 return GetElement(ClientContact_txt_xpath);
	 }	
	 public  WebElement Update_btn() {
		 return GetElement(Update_btn_xpath);
	 }
	 
	 
	 
	 
	 //METHODS
	 
	 
	 public void GotoAuthDetailatCP(String authname) {
			try {
				
				ARCLoading ARCLoading = new ARCLoading(driverbase);
				
				new Authorization_page(driverbase).AuthName_lnk(authname).click();
				
				ARCLoading.Wait_Util_FinishedARClogoLoading();

				ARCLoading.Wait_Util_FinishedPageLoading();
				
				GoToLatestBrowserTab();
			}
			catch(Exception e) {
				TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT DETAILS PAGE:"+e.getMessage()+".\n";
				Assert.fail(TestConfigs.glb_TCFailedMessage);
			}
	 	}
	 
	 public  void SearchClient_func(Client_Info client) 
		{
		//	Wait_For_ElementDisplay(SearchClientName_txt());

			SearchClientName_txt().sendKeys(client.CompanyName);
			
			Optimize_ElementClick(Search_btn());
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
		
		}

	 public  void VerifyClientExistTable_func(Client_Info client) 
		{
			boolean exist = false;;
			List<String> records = GetTableRecordsPerPaging_func(ClientList_tbl());

			for(String index_str: records)
			{
				if(index_str.contains(client.CompanyName))
				{
					exist = true;
					break;
				}//end if
			}//end foreach

			if(!exist)
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Client["+client.CompanyName+"] NOT exist in table.\n";
			}//end if
		}//endvoid
		
	 public  void CreateClient_func(Client_Info client,String submit)
		{
		 Messages_Notification Messages_Notification = new Messages_Notification(driverbase);	
			
		 NewClient_mdal	NewClient_mdal = new NewClient_mdal(driverbase);

				//if(NewClient_mdal.CompanyName_txt().isDisplayed()==false)
				
		//		Wait_For_ElementDisplay(NewClient_btn());

				
				Optimize_ElementClick(NewClient_btn());
				
				NewClient_mdal.FillInfoModal_func(client, submit);
				
				if(Messages_Notification.notification_msg()!=null)
				{
					String msg = Messages_Notification.notification_msg().getText();
					
					String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
					
					//println(msg)
					
					if(msg.equals(expected_msg))
					{
						TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Client Creation.Msg: '"+msg+"'.\n";
						Assert.fail(TestConfigs.glb_TCFailedMessage);
					}
				}//end if of message
		}//end void
				
	 public  void CreateClient_via_Shortcut_func(Client_Info client, String submit)
			{
		//		Wait_For_ElementDisplay(Shortcuts_menu.NewClient_btn());
		 NewClient_mdal NewClient_mdal = new NewClient_mdal(driverbase);
		 Messages_Notification 		Messages_Notification = new Messages_Notification (driverbase);
				
				Optimize_ElementClick(new Shortcuts_menu(driverbase).NewClient_btn());

				NewClient_mdal.FillInfoModal_func(client, submit);
				
				if(Messages_Notification.notification_msg()!=null)
				{
					String msg = Messages_Notification.notification_msg().getText();
					
					String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";
					
					//println(msg)
					
					if(msg.equals(expected_msg))
					{
						TestConfigs.glb_TCFailedMessage+= "Unsuccessfull Client Creation.Msg: '"+msg+"'.\n";
						Assert.fail(TestConfigs.glb_TCFailedMessage);
					}
				}//end if of message


			}		
	
	 public  void Update_ClientCompanyProfile(Client_Info cf)
		{	
			
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			
				EditClientProfile_btn().click();
				//Optimize_ElementClick(EditClientProfile_btn());
				
				Wait_For_ElementEnable(CompanyName_input());
				
				ClearThenEnterValueToField(CompanyName_input(), cf.CompanyName);
				
				String note_str = "Client Details - Client Type Dropdown.";
				
				SelectDropdownItem(ClientType_ddl(), cf.Type, note_str);
				
				ClearThenEnterValueToField(Region_input(), cf.Region);
				
				ClearThenEnterValueToField(Revenue_txt(), cf.Revenue);
				
				note_str = "Client Details - Territory Dropdown ";
				
				SelectDropdownItem(Territory_ddl(), cf.Territory, note_str);
				
				ClearThenEnterValueToField(OfEmployees_txt(), cf.OfEmployees);
				
				ClientInfoUpdate_btn().click(); 
			
				
		}//end void

	 public  void Update_ClientCompanyStatus(Client_Info cf) {
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				EditClientStatus_btn().click();
				
				Wait_For_ElementEnable(StatusClient_ddl());
				
				String note_str = "Client Details - Client Status Dropdown.";
				
				SelectDropdownItem(StatusClient_ddl(), cf.Status, note_str );
				
				note_str = "Client Details - Client Reffered By Dropdown.";
				
				SelectDropdownItem(ReferredBy_ddl(), cf.ReferredBy, note_str);
				
				note_str = "Client Details - Client Industry Dropdown";
				
				SelectDropdownItem(Industry_ddl(), cf.Industry, note_str);
				
				note_str = "Client Detail - Account Manager Dropdown";
				
				SelectDropdownItem(AccountManager_ddl(), cf.AccountManager, note_str);
				
				ClientStatusUpdate_btn().click();
				
		}
	
	 public  void Preferences_Option(Client_Info client) {
		 switch (client.Approval_option){
			
			case "Specified Contact":
				
				if(GetFieldText_func(OneOfSelected_radio()).equals("false")) {
					
					Optimize_ElementClick(OneOfSelected_radio());
				
					Optimize_ElementClick(ClientContact_arrow());
					
					Optimize_ElementSendkey(ClientContact_txt(), "Su 1");
				
					String note_str = "Client Contact List";
				
				    SelectDropdownItem(ClientContact_ddl(), "Su 1", note_str);
				    
				    Optimize_ElementClick(ClientContact_arrow());
				}
				
				break;
				
			case "Any Contact":
				
					if(GetFieldText_func(AnyContact_radio()).equals("false")) {
					
					Optimize_ElementClick(OneOfSelected_radio());
				
					}
				
				break;
				
			case "Entering Contact":
				
					if(GetFieldText_func(EnteringContact_radio()).equals("false")) {
					
					Optimize_ElementClick(OneOfSelected_radio());
				
				   }
			break;
				
			default:
				
					Optimize_ElementClick(NoApproval_radio());
					
				break;
			}
	 }
	 
	 public void ChangePreferences_NoApprove_func(Client_Info client, Contact_Info ctact_info) {
			//SearchClientName_txt().sendKeys(client.FullName);
			
			//Optimize_ElementClick(Search_btn());
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
			try{
				
				if(new Clients_page(driverbase).Preferences_tab()==null)
			
					Optimize_ElementClick(ClientName_link(client.FullName));
						
					ARCLoading ARCLoading = new ARCLoading(driverbase);
					
					ARCLoading.Wait_Util_FinishedARClogoLoading();
					
					ARCLoading.Wait_Util_FinishedPageLoading();
								
					GoToLatestBrowserTab();

				}
				catch(Exception e) {
					TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT DETAILS PAGE:"+e.getMessage()+".\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
				
			Optimize_ElementClick(Preferences_tab());
			
			Optimize_ElementClick(EditPrefer_btn());
			
			Preferences_Option(client);
			
			Optimize_ElementClick(Update_btn());
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
		}

	 public void ChangePreferences_AnyContact_func(Client_Info client) {
			SearchClientName_txt().sendKeys("QA Su");
			
			Optimize_ElementClick(Search_btn());
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
			try{
				
				if(new Clients_page(driverbase).Preferences_tab()==null)
			
					Optimize_ElementClick(ClientName_link("QA Su"));
						
					ARCLoading ARCLoading = new ARCLoading(driverbase);
					
					ARCLoading.Wait_Util_FinishedARClogoLoading();
					
					ARCLoading.Wait_Util_FinishedPageLoading();
								
					GoToLatestBrowserTab();

				}
				catch(Exception e) {
					TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT DETAILS PAGE:"+e.getMessage()+".\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
				
			Optimize_ElementClick(Preferences_tab());
			
			Optimize_ElementClick(EditPrefer_btn());
			
			Preferences_Option(client);
			
			Optimize_ElementClick(Update_btn());
			
		}	
			
	 public void ChangePreferences_Oneofselected(Client_Info client) {
			
			SearchClientName_txt().sendKeys("QA Su");
			
			Optimize_ElementClick(Search_btn());
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
			Optimize_ElementClick(ClientName_link("QA Su"));
					
			ARCLoading ARCLoading = new ARCLoading(driverbase);
					
			ARCLoading.Wait_Util_FinishedARClogoLoading();
					
			ARCLoading.Wait_Util_FinishedPageLoading();
								
			GoToLatestBrowserTab();
				
			Optimize_ElementClick(Preferences_tab());
			
			Optimize_ElementClick(EditPrefer_btn());
				
			Preferences_Option(client);
				
			Optimize_ElementClick(Update_btn());
		}	

	 public void ChangePreferences_Entering_func(Client_Info client) {
			SearchClientName_txt().sendKeys("QA Su");
			
			Optimize_ElementClick(Search_btn());
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
			try{
				
				if(new Clients_page(driverbase).Preferences_tab()==null)
			
					Optimize_ElementClick(ClientName_link("QA Su"));
						
					ARCLoading ARCLoading = new ARCLoading(driverbase);
					
					ARCLoading.Wait_Util_FinishedARClogoLoading();
					
					ARCLoading.Wait_Util_FinishedPageLoading();
								
					GoToLatestBrowserTab();

				}
				catch(Exception e) {
					TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT DETAILS PAGE:"+e.getMessage()+".\n";
					Assert.fail(TestConfigs.glb_TCFailedMessage);
				}
				
			Optimize_ElementClick(Preferences_tab());
			
			Optimize_ElementClick(EditPrefer_btn());
			
			Preferences_Option(client);
			
			Optimize_ElementClick(Update_btn());
			
		}	
		 
}
