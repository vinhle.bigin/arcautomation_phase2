package pages;

import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.TQService_Info;
import configuration.DriverConfig.DriverBase;

public class Transf_TQ_details extends PageObjects{
	
	
	
	public Transf_TQ_details(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String TQ_ServiceDesrc_txt_xpath= "//div[@org-placeholder ='Service Description']//input";
	private  String TQ_Currency_ddl_xpath= "//div[@org-placeholder ='Currency']//select";
	private  String TQ_AccountPurpose_ddl_xpath= "//div[@org-placeholder ='Account Purpose']//select";
	private  String TQ_NotifyDate_txt_xpath= "//div[@org-placeholder ='Notification Date']//input";
	private  String TQ_ServiceDate_txt_xpath= "//div[@org-placeholder ='Service Date']//input";
	private  String TQ_MoveInDate_txt_xpath= "//div[@org-placeholder ='Move In Date']//input";
	private  String TQ_MoveOutDate_txt_xpath= "//div[@org-placeholder ='Move Out Date']//input";
	private  String TQ_DateAuth_txt_xpath= "//div[@org-placeholder ='Days Authorized']//input";
	private  String TQ_ApartmentType_ddl_xpath= "//div[@org-placeholder ='Appartment Type']//select";
	private  String TQ_ApartmentFinish_ddl_xpath= "//div[@org-placeholder ='Appartment Finish']//select";
	private  String TQ_Bath_txt_xpath= "//div[@org-placeholder ='Baths']//input";
	private  String TQ_Budget_txt_xpath= "//div[@org-placeholder ='Budget']//input";
	private  String TQ_Pet_txt_xpath= "//div[@org-placeholder ='Pets']//textarea";
	private  String TQ_SpecicialInstruct_txt_xpath= "//div[@org-placeholder ='Special Instructions']//textarea";
	private  String TQ_WhoPay_ddl_xpath= "//div[contains(@org-placeholder,'s Paying')]//select";
	private  String TQ_PayType_txt_xpath= "//div[contains(@org-placeholder,'Payment Type')]//input";
	private  String TQ_IsBillFirstDay_chk_xpath= "//label[contains(text(),'Bill First Day')]/..//input";
	private  String HS_IsInvoiceCreate_chk_xpath= "//label[contains(text(),'Invoice Created')]/..//input";
	private  String HS_IsInvoiceClose_chk_xpath= "//label[contains(text(),'Invoice Closed Out')]/..//input";
	private  String HS_IsEscrReceive_chk_xpath= "//label[contains(text(),'Escrow Fee Received')]/..//input";
	
	private  String Edit_btn_xpath= "//a[@data-original-title ='Edit']";
	private  String Update_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='update']";
	private  String Cancel_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='cancel-update']";
	
	//Value Field
	private  String TQ_Pet_vlue_xpath= "//pre[@placeholder ='Pets']";
	private  String TQ_SpecicialInstruct_vlue_xpath= "//pre[@placeholder ='Special Instructions']";
	
	
	public  WebElement TQ_Pet_vlue() {
		  return GetElement(TQ_Pet_vlue_xpath);
		 }
	
	public  WebElement TQ_SpecicialInstruct_vlue() {
		  return GetElement(TQ_SpecicialInstruct_vlue_xpath);
		 }
	
	public  WebElement TQ_ServiceDesrc_txt() {
	  return GetElement(TQ_ServiceDesrc_txt_xpath);
	 }
	public  WebElement TQ_Currency_ddl() {
	  return GetElement(TQ_Currency_ddl_xpath);
	 }
	public  WebElement TQ_AccountPurpose_ddl() {
	  return GetElement(TQ_AccountPurpose_ddl_xpath);
	 }
	public  WebElement TQ_NotifyDate_txt() {
	  return GetElement(TQ_NotifyDate_txt_xpath);
	 }
	public  WebElement TQ_ServiceDate_txt() {
	  return GetElement(TQ_ServiceDate_txt_xpath);
	 }
	public  WebElement TQ_MoveInDate_txt() {
	  return GetElement(TQ_MoveInDate_txt_xpath);
	 }
	public  WebElement TQ_MoveOutDate_txt() {
	  return GetElement(TQ_MoveOutDate_txt_xpath);
	 }
	public  WebElement TQ_DateAuth_txt() {
	  return GetElement(TQ_DateAuth_txt_xpath);
	 }
	public  WebElement TQ_ApartmentType_ddl() {
	  return GetElement(TQ_ApartmentType_ddl_xpath);
	 }
	public  WebElement TQ_ApartmentFinish_ddl() {
	  return GetElement(TQ_ApartmentFinish_ddl_xpath);
	 }
	public  WebElement TQ_Bath_txt() {
	  return GetElement(TQ_Bath_txt_xpath);
	 }
	public  WebElement TQ_Budget_txt() {
	  return GetElement(TQ_Budget_txt_xpath);
	 }
	public  WebElement TQ_Pet_txt() {
	  return GetElement(TQ_Pet_txt_xpath);
	 }
	public  WebElement TQ_SpecicialInstruct_txt() {
	  return GetElement(TQ_SpecicialInstruct_txt_xpath);
	 }
	public  WebElement TQ_WhoPay_ddl() {
	  return GetElement(TQ_WhoPay_ddl_xpath);
	 }
	public  WebElement TQ_PayType_txt() {
	  return GetElement(TQ_PayType_txt_xpath);
	 }
	public  WebElement TQ_IsBillFirstDay_chk() {
	  return GetElement(TQ_IsBillFirstDay_chk_xpath);
	 }
	
	public  WebElement Update_btn() {
		  return GetElement(Update_btn_xpath);
		 }
		public  WebElement Cancel_btn() {
		  return GetElement(Cancel_btn_xpath);
		 }
		
		public  WebElement Edit_btn() {
			  return GetElement(Edit_btn_xpath);
			 }
		
		
		public  WebElement HS_IsInvoiceCreate_chk() {
			  return GetElement(HS_IsInvoiceCreate_chk_xpath);
			 }
		
		public  WebElement HS_IsInvoiceClose_chk() {
			  return GetElement(HS_IsInvoiceClose_chk_xpath);
			 }
		
		public  WebElement HS_IsEscrReceive_chk() {
			  return GetElement(HS_IsEscrReceive_chk_xpath);
			 }

	
	public  void FillTQInfo_func(TQService_Info service,String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Edit_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();	


		//WaitForElementDisplay(HS_BuyOutDate_txt())


		ClearThenEnterValueToField(TQ_ServiceDesrc_txt(),service.ServiceDesr);

		ScrollElementtoViewPort_func(TQ_Currency_ddl());

		String note_str = "Transferee Other Details - Nation Dropdown.";
		SelectDropdownItem(TQ_Currency_ddl(),service.Currency,note_str);

		ScrollElementtoViewPort_func(TQ_AccountPurpose_ddl());

		note_str = "Transferee Other Details - AccountPurpose Dropdown.";
		SelectDropdownItem(TQ_AccountPurpose_ddl(),service.AccountPurpose,note_str);

		Optimize_ElementSendkey(TQ_NotifyDate_txt(),service.NotiDate);

		Optimize_ElementSendkey(TQ_ServiceDate_txt(),service.ServiceDate);

		Optimize_ElementSendkey(TQ_MoveInDate_txt(),service.MoveInDate);

		Optimize_ElementSendkey(TQ_MoveOutDate_txt(),service.MoveOutDate);

		ClearThenEnterValueToField(TQ_DateAuth_txt(),service.DayAuth);

		note_str = "Transferee Other Details - ApartmentType Dropdown.";
		ScrollElementtoViewPort_func(TQ_ApartmentType_ddl());
		SelectDropdownItem(TQ_ApartmentType_ddl(),service.ApartmentType,note_str);

		note_str = "Transferee Other Details - ApartmentFinish Dropdown.";
		ScrollElementtoViewPort_func(TQ_ApartmentFinish_ddl());
		SelectDropdownItem(TQ_ApartmentFinish_ddl(),service.ApartmentFinish,note_str);

		ClearThenEnterValueToField(TQ_Bath_txt(),service.Baths);

		ClearThenEnterValueToField(TQ_Budget_txt(),service.Budget);

		Optimize_ElementSendkey(TQ_Pet_txt(),service.Pets);

		Optimize_ElementSendkey(TQ_SpecicialInstruct_txt(),service.SpecialInstruct);

		note_str = "Transferee Other Details - WhoPay Dropdown.";
		ScrollElementtoViewPort_func(TQ_WhoPay_ddl());
		SelectDropdownItem(TQ_WhoPay_ddl(),service.WhoPay,note_str);

		ClearThenEnterValueToField(TQ_PayType_txt(),service.PaymentType);


		Optimize_ElementClick(TQ_IsBillFirstDay_chk());

		Optimize_ElementClick(HS_IsInvoiceCreate_chk());

		Optimize_ElementClick(HS_IsInvoiceClose_chk());

		Optimize_ElementClick(HS_IsEscrReceive_chk());

		if(button=="Submit")
			Update_btn().click();
		else if(button=="Cancel")
			Cancel_btn().click();

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "when update TQ_ServiceInfo",false);


	}//end void
	
	public  void Verify_TQServiceDetail_func(TQService_Info service)
	{

		String field_name = "";

		field_name = "TQ_ServiceDesrc_txt";
		VerifyFieldValueEqual_func(field_name,TQ_ServiceDesrc_txt(),service.ServiceDesr,"");

		field_name = "TQ_Currency_ddl";
		VerifyFieldValueEqual_func(field_name,TQ_Currency_ddl(),service.Currency,"");

		field_name = "TQ_AccountPurpose_ddl";
		VerifyFieldValueEqual_func(field_name,TQ_AccountPurpose_ddl(),service.AccountPurpose,"");

		field_name = "TQ_NotifyDate_txt";
		VerifyFieldValueEqual_func(field_name,TQ_NotifyDate_txt(),service.NotiDate,"");

		field_name = "TQ_ServiceDate_txt";
		VerifyFieldValueEqual_func(field_name,TQ_ServiceDate_txt(),service.ServiceDate,"");

		field_name = "TQ_MoveInDate_txt";
		VerifyFieldValueEqual_func(field_name,TQ_MoveInDate_txt(),service.MoveInDate,"");

		field_name = "TQ_MoveOutDate_txt";
		VerifyFieldValueEqual_func(field_name,TQ_MoveOutDate_txt(),service.MoveOutDate,"");

		field_name = "TQ_DateAuth_txt";
		VerifyFieldValueEqual_func(field_name,TQ_DateAuth_txt(),service.DayAuth,"");

		field_name = "TQ_ApartmentType_ddl";
		VerifyFieldValueEqual_func(field_name,TQ_ApartmentType_ddl(),service.ApartmentType,"");

		field_name = "TQ_ApartmentFinish_ddl";
		VerifyFieldValueEqual_func(field_name,TQ_ApartmentFinish_ddl(),service.ApartmentFinish,"");

		field_name = "TQ_Bath_txt";
		VerifyFieldValueEqual_func(field_name,TQ_Bath_txt(),service.Baths,"");

		field_name = "TQ_Budget_txt";
		VerifyFieldValueEqual_func(field_name,TQ_Budget_txt(),service.Budget,"");

		field_name = "TQ_Pet_txt";
		VerifyFieldValueEqual_func(field_name,TQ_Pet_vlue(),service.Pets,"");

		field_name = "TQ_SpecicialInstruct_txt";
		VerifyFieldValueEqual_func(field_name,TQ_SpecicialInstruct_vlue(),service.SpecialInstruct,"");

		field_name = "TQ_WhoPay_ddl";
		VerifyFieldValueEqual_func(field_name,TQ_WhoPay_ddl(),service.WhoPay,"");

		field_name = "TQ_PayType_txt";
		VerifyFieldValueEqual_func(field_name,TQ_PayType_txt(),service.PaymentType,"");

		field_name = "TQ_IsBillFirstDay_chk";
		VerifyFieldValueEqual_func(field_name,TQ_IsBillFirstDay_chk(),service.IsBillFirstDay.toString(),"");

		field_name = "TQ_IsInvoiceCreate_chk";
		VerifyFieldValueEqual_func(field_name,HS_IsInvoiceCreate_chk(),service.IsInvoiceCreate.toString(),"");

		field_name = "TQ_IsInvoiceClose_chk";
		VerifyFieldValueEqual_func(field_name,HS_IsInvoiceClose_chk(),service.IsInvoiceClose.toString(),"");

		field_name = "TQ_IsEscrReceive_chk";
		VerifyFieldValueEqual_func(field_name,HS_IsEscrReceive_chk(),service.IsEscrowFeeReceive.toString(),"");
	}//end void

}
