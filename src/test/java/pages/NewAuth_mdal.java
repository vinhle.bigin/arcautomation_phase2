package pages;

import org.openqa.selenium.WebElement;

import businessObjects.Auth_Info;
import configuration.DriverConfig.DriverBase;

public class NewAuth_mdal extends NewTransf_mdal{
	
	
	
	
	
	//DEFINE ELEMENT:

	public NewAuth_mdal(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	private String Salutation_slect_xpath = "//div[@org-placeholder='Salutation']//select";
	private String FirstName_txt_xpath = "//div[@org-placeholder='First Name']//input";
	private String Mid_txt_xpath = "//div[@org-placeholder='Mid']//input";
	private String LastName_txt_xpath = "//div[@org-placeholder='Last Name']//input";
	private String Client_txt_xpath = "//div[@org-placeholder='Client']";
	private String ClientContact_txt_xpath = "//div[@org-placeholder='Client Contact']";
	private String Policy_slect_xpath = "//div[@org-placeholder='Policy']//select";
	private String InitiationDate_txt_xpath = "//input[@id= 'create-transferee-other-info-initiation-date']";
	private String EffectiveDate_txt_xpath = "//input[@id= 'create-transferee-other-info-effective-date']";
	private String HomeAddress_ClientLocation_slect_xpath = "//h4[contains(text(),'Home/Depature Address')]//..//div[@org-placeholder='Client Location']//select";
	private String HomeAddress_Address1_slect_xpath = "//h4[contains(text(),'Home/Depature Address')]//..//div[@org-placeholder='Address 1']//input";
	private String HomeAddress_Address2_slect_xpath = "//h4[contains(text(),'Home/Depature Address')]//..//div[@org-placeholder='Address 2']//input";
	private String HomeAddress_City_slect_xpath = "//h4[contains(text(),'Home/Depature Address')]//..//div[@org-placeholder='City']//input";
	private String HomeAddress_State_slect_xpath = "//h4[contains(text(),'Home/Depature Address')]//..//div[@org-placeholder='State']//input";
	private String HomeAddress_ZipCode_slect_xpath = "//h4[contains(text(),'Home/Depature Address')]//..//div[@org-placeholder='Zip Code']//input";
	private String HomeAddress_Country_slect_xpath = "//h4[contains(text(),'Home/Depature Address')]//..//div[@org-placeholder='Country']//select";
	private String DesAddress_ClientLocation_slect_xpath = "//h4[contains(text(),'Destination Office Address')]//..//div[@org-placeholder='Client Location']//select";
	private String DesAddress_Address1_slect_xpath = "//h4[contains(text(),'Destination Office Address')]//..//div[@org-placeholder='Address 1']//input";
	private String DesAddress_Address2_slect_xpath = "//h4[contains(text(),'Destination Office Address')]//..//div[@org-placeholder='Address 2']//input";
	private String DesAddress_City_slect_xpath = "//h4[contains(text(),'Destination Office Address')]//..//div[@org-placeholder='City']//input";
	private String DesAddress_State_slect_xpath = "//h4[contains(text(),'Destination Office Address')]//..//div[@org-placeholder='State']//input";
	private String DesAddress_ZipCode_slect_xpath = "//h4[contains(text(),'Destination Office Address')]//..//div[@org-placeholder='Zip Code']//input";
	private String DesAddress_Country_slect_xpath = "//h4[contains(text(),'Destination Office Address')]//..//div[@org-placeholder='Country']//select";
	private String PhoneNumber_Type_slect_xpath = "//h4[contains(text(),'Phone Number')]//..//div[@org-placeholder='Type']//select";
	private String Number_txt_xpath = "//div[@org-placeholder='Number']//input";
	private String Ext_txt_xpath = "//div[@org-placeholder='Ext.']//input";
	private String Email_Type_slect_xpath = "//h4[contains(text(),'Email')]//..//div[@org-placeholder='Type']//select";
	private String Email_txt_xpath = "//div[@org-placeholder='E-mail']/input";
	private String ClientNote_txt_xpath = "//div[@org-placeholder='Client Note']";
	private String Submit_btn_xpath = "//button[@data-control = 'create']";

	

	
	public  WebElement Salutation_slect(){
		return GetElement(Salutation_slect_xpath);
}
	public  WebElement FirstName_txt(){
		return GetElement(FirstName_txt_xpath);
}
	public  WebElement Mid_txt(){
		return GetElement(Mid_txt_xpath);
}
	public  WebElement LastName_txt(){
		return GetElement(LastName_txt_xpath);
	}
	public  WebElement Client_txt(){
		return GetElement(Client_txt_xpath);
}
	public  WebElement ClientContact_txt(){
		return GetElement(ClientContact_txt_xpath);
}
	public  WebElement Policy_slect(){
		return GetElement(Policy_slect_xpath);
}
	public  WebElement InitiationDate_txt(){
		return GetElement(InitiationDate_txt_xpath);
}
	public  WebElement EffectiveDate_txt(){
		return GetElement(EffectiveDate_txt_xpath);
}
	public  WebElement HomeAddress_ClientLocation_slect(){
		return GetElement(HomeAddress_ClientLocation_slect_xpath);
}
	public  WebElement HomeAddress_Address1_slect(){
		return GetElement(HomeAddress_Address1_slect_xpath);
}
	public  WebElement HomeAddress_Address2_slect(){
		return GetElement(HomeAddress_Address2_slect_xpath);
}
	public  WebElement HomeAddress_City_slect(){
		return GetElement(HomeAddress_City_slect_xpath);
}
	public  WebElement HomeAddress_State_slect(){
		return GetElement(HomeAddress_State_slect_xpath);
}
	public  WebElement HomeAddress_ZipCode_slect(){
		return GetElement(HomeAddress_ZipCode_slect_xpath);
}
	public  WebElement HomeAddress_Country_slect(){
		return GetElement(HomeAddress_Country_slect_xpath);
}
	public  WebElement DesAddress_ClientLocation_slect(){
		return GetElement(DesAddress_ClientLocation_slect_xpath);
}
	public  WebElement DesAddress_Address1_slect(){
		return GetElement(DesAddress_Address1_slect_xpath);
}
	public  WebElement DesAddress_Address2_slect(){
		return GetElement(DesAddress_Address2_slect_xpath);
}
	public  WebElement DesAddress_City_slect(){
		return GetElement(DesAddress_City_slect_xpath);
}
	public  WebElement DesAddress_State_slect(){
		return GetElement(DesAddress_State_slect_xpath);
}
	public  WebElement DesAddress_ZipCode_slect(){
		return GetElement(DesAddress_ZipCode_slect_xpath);
}
	public  WebElement DesAddress_Country_slect(){
		return GetElement(DesAddress_Country_slect_xpath);
}
	public  WebElement PhoneNumber_Type_slect(){
		return GetElement(PhoneNumber_Type_slect_xpath);
}
	public  WebElement Number_txt(){
		return GetElement(Number_txt_xpath);
}
	public  WebElement Ext_txt(){
		return GetElement(Ext_txt_xpath);
}
	public  WebElement Email_Type_slect(){
		return GetElement(Email_Type_slect_xpath);
}
	public  WebElement Email_txt(){
		return GetElement(Email_txt_xpath);
}
	public  WebElement ClientNote_txt(){
		return GetElement(ClientNote_txt_xpath);
}
	public	WebElement Submit_btn() {
		return GetElement(Submit_btn_xpath);
	}

	
	//=============================METHOD
	
	public void FillAuthModal_func(Auth_Info auth,String submit) {
	
			new Messages_Notification(driverbase);

			ARCLoading ARCLoading = new ARCLoading(driverbase);
				
			ARCLoading.Wait_Util_FinishedPageLoading();
						
			//select Policy
			String note_str = "New Authorization - Policy Dropdown.";
			SelectDropdownItem(Policy_slect(), auth.Policy, note_str);
			
			//Input First name and Last name
			FirstName_txt().sendKeys(auth.firstname);			
			
			LastName_txt().sendKeys(auth.lastname);
			
			//Phone info
			note_str = "Auth Details - Phone Type Dropdown.";
			
			SelectDropdownItem(PhoneType_ddl(), auth.PhoneList.get(0).Type,note_str);

			ClearThenEnterValueToField(Phone_txt(), auth.PhoneList.get(0).Numb);

			//Email info
			note_str = "Auth Details - Mail Type Dropdown.";
			
			ScrollElementtoViewPort_func(EmailType_ddl());
			
			SelectDropdownItem(EmailType_ddl(), auth.MailList.get(0).Type,note_str);

			ClearThenEnterValueToField(Email_txt(), auth.MailList.get(0).EmailAddr);
			

			if(submit.equals("Submit")||submit.equals("submit") )
			{
				//Optimize_ElementClick(null)

				Submit_btn().click();
				
//					Wait_For_ElementDisplay(Client_GenInfo_page.CompName_txt());
	
				wait_For_PageLoad();
				
			}
			
	}

}
