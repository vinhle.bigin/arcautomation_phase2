package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Service_Order_Info;
import configuration.DriverConfig.DriverBase;

public class Order_AutoTransport_section extends PageObjects{
	
	
	
	private  String NewVehicle_btn_xpath = "//div[@id= 'hhg-order-panel-auto-transport']//button[@class='btn btn-custom btn-rounded']";
	
	private  String Model_Close_btn_xpath = "//div[@id= 'loading-modal-hhg-update-auto-transport']//i[@class='icon-close']";
	
	private  String Model_Submit_btn_xpath = "//div[@id= 'hhg-order-panel-auto-transport']//button[@data-control='create']";
	
	private  String Model_Cancel_btn_xpath = "//div[@id= 'hhg-order-panel-auto-transport']//button[@data-control='cancel-create']";
	
	private  String Vehicle_Edit_btn_xpath = "//i[@class='icon-note default-color']";
	
	private  String Vehicle_Remove_btn_xpath = "//div[@id= 'hhg-order-panel-auto-transport']//i[@class='icon-close']";
	
	private  String Vehicles_List_tbl_xpath = "//div[@id='hhg-order-panel-auto-transport']//table//tbody";
	
	private  String Make_txt_xpath = "//div[@org-placeholder = 'Make']//input";
	
	private  String Year_txt_xpath = "//div[@org-placeholder = 'Year']//input";
	
	private  String Model_txt_xpath = "//div[@org-placeholder = 'Model']//input";
	
	private  String VIN_txt_xpath = "//div[@org-placeholder = 'VIN']//input";
	
	private  String Plate_txt_xpath = "//div[@org-placeholder = 'Plate']//input";
	
	private  String EstLoadDate_txt_xpath = "//div[@org-placeholder = 'Est Load Date']//input";
	
	private  String ActLoadDate_txt_xpath = "//div[@org-placeholder = 'Act Load Date']//input";
	
	private  String EstDeliDate_txt_xpath = "//div[@org-placeholder = 'Est Delivery date']//input";
	
	private  String ActDeliDate_txt_xpath = "//div[@org-placeholder = 'Act Delivery Date']//input";
	
	private  String SpecialNote_txt_xpath = "//div[@org-placeholder = 'Special Note']//pre[@placeholder='Special Note']";
	
	public Order_AutoTransport_section(DriverBase driverbase)
	{
		super(driverbase);
	}
	
	public  WebElement NewVehicle_btn() {
		 return GetElement(NewVehicle_btn_xpath);
	}
	
	public  WebElement Vehicles_List_tbl() {
		 return GetElement(Vehicles_List_tbl_xpath);
	}
	
	public  WebElement Vehicle_Edit_btn(String auto_make) {
		
		return GetElementInTable(auto_make, Vehicles_List_tbl(), Vehicle_Edit_btn_xpath);
	}
	
	public  WebElement Vehicle_Remove_btn(String auto_make) {
		
		return GetElementInTable(auto_make, Vehicles_List_tbl(), Vehicle_Remove_btn_xpath);
	}
	
	public  WebElement Model_Close_btn() {
		 return GetElement(Model_Close_btn_xpath);
	}
	
	public  WebElement Model_Submit_btn() {
		 return GetElement(Model_Submit_btn_xpath);
	}
	
	public  WebElement Model_Cancel_btn() {
		 return GetElement(Model_Cancel_btn_xpath);
	}
	
	public  WebElement Make_txt() {
		 return GetElement(Make_txt_xpath);
	}
	
	public  WebElement Year_txt() {
		 return GetElement(Year_txt_xpath);
	}
	
	public  WebElement Model_txt() {
		 return GetElement(Model_txt_xpath);
	}
	
	public  WebElement VIN_txt() {
		 return GetElement(VIN_txt_xpath);
	}
	
	public  WebElement Plate_txt() {
		 return GetElement(Plate_txt_xpath);
	}
	
	public  WebElement EstLoadDate_txt() {
		 return GetElement(EstLoadDate_txt_xpath);
	}
	
	public  WebElement ActLoadDate_txt() {
		 return GetElement(ActLoadDate_txt_xpath);
	}
	
	public  WebElement EstDeliDate_txt() {
		 return GetElement(EstDeliDate_txt_xpath);
	}
	
	public  WebElement ActDeliDate_txt() {
		 return GetElement(ActDeliDate_txt_xpath);
	}
	
	public  WebElement SpecialNote_txt() {
		 return GetElement(SpecialNote_txt_xpath);
	}
	
	//==================================METHODS
	
	public  void Update_AutoTransport_section_func(Service_Order_Info order, String button)
	{
		 Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		 if (order.ServiceType.contains("POV") == true)
		{
			Optimize_ElementClick(NewVehicle_btn());
			
			ClearThenEnterValueToField(Make_txt(), order.AutoTrans_Make);
			
			ClearThenEnterValueToField(Year_txt(), order.AutoTrans_Year);

			ClearThenEnterValueToField(Model_txt(), order.AutoTrans_Model);

			ClearThenEnterValueToField(VIN_txt(), order.AutoTrans_VIN);

			ClearThenEnterValueToField(Plate_txt(), order.AutoTrans_Plate);

			Optimize_ElementSendkey(EstLoadDate_txt(), order.AutoTrans_EstLoadDate);
			
			EstLoadDate_txt().sendKeys(Keys.ENTER);

			Optimize_ElementSendkey(ActLoadDate_txt(), order.AutoTrans_ActLoadDate);
			
			ActLoadDate_txt().sendKeys(Keys.ENTER);

			Optimize_ElementSendkey(EstDeliDate_txt(), order.AutoTrans_EstDeliDate);
			
			EstDeliDate_txt().sendKeys(Keys.ENTER);

			Optimize_ElementSendkey(ActDeliDate_txt(), order.AutoTrans_ActDeliDate);
			
			ActDeliDate_txt().sendKeys(Keys.ENTER);

			ClearThenEnterValueToField(SpecialNote_txt(), order.AutoTrans_SpecNote);
			
			if(button.equals("Submit")||button.equals("submit"))
			{
				Optimize_ElementClick(Model_Submit_btn());
				
				Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating new info in Auto Transport",false);
				
				new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			}
			
		}
	}
	
	//==================================================METHOD VERIFY
	
	public  void Verify_AutoTransport_section_func(Service_Order_Info order)
	{
		if (order.ServiceType.contains("POV") == true)
		{
			Optimize_ElementClick(Vehicle_Edit_btn(order.AutoTrans_Make));
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
			String field = "";
			
			field = "Make";
			
			VerifyFieldValueEqual_func(field,Make_txt(), order.AutoTrans_Make,"");
			
			field = "Year";
			
			VerifyFieldValueEqual_func(field,Year_txt(), order.AutoTrans_Year,"");
			
			field = "Model";
			
			VerifyFieldValueEqual_func(field,Model_txt(), order.AutoTrans_Model,"");
			
			field = "VIN";
			
			VerifyFieldValueEqual_func(field,VIN_txt(), order.AutoTrans_VIN,"");
			
			field = "Plate";
			
			VerifyFieldValueEqual_func(field,Plate_txt(), order.AutoTrans_Plate,"");
			
			field = "Est Load Date";
			
			VerifyFieldValueEqual_func(field,EstLoadDate_txt(), order.AutoTrans_EstLoadDate,"");
			
			field = "Act Load Date";
			
			VerifyFieldValueEqual_func(field,ActLoadDate_txt(), order.AutoTrans_ActLoadDate,"");
			
			field = "Est Delivery Date";
			
			VerifyFieldValueEqual_func(field,EstDeliDate_txt(), order.AutoTrans_EstDeliDate,"");
			
			field = "Act Delivery Date";
			
			VerifyFieldValueEqual_func(field,ActDeliDate_txt(), order.AutoTrans_ActDeliDate,"");
			
			field = "Special Note";
			
			VerifyFieldValueEqual_func(field,SpecialNote_txt(), order.AutoTrans_SpecNote,"");
			
			Optimize_ElementClick(Model_Close_btn());
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
		}
	}

}
