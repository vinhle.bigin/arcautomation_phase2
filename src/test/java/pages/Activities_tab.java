package pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Activity_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;

public class Activities_tab extends PageObjects {
	private ARCLoading ARCLoading;
	private Messages_Notification Messages_Notification;
	private ActivityDetails_mdal ActivityDetails_mdal;
	private Shortcuts_menu Shortcuts_menu;

	private String activity_tab_xpath = "//a[contains(text(),'Activities')]";
	private String SearchFromDate_txt_xpath = "//input[contains(@id,'from_date')]";
	private String SearchToDate_txt_xpath = "//input[contains(@id,'to_date')]";
	private String SearchType_slect_xpath = "//select[contains(@placeholder,'Types')]";
	private String SearchPriority_slect_xpath = "//select[contains(@placeholder,'Priorities')]";
	private String SearchStatus_slect_xpath = "//select[contains(@placeholder,'Status')]";

	public String ActivityName_lnk_xpath = "//a[contains(@href,'activity/edit')]";

	private String Task_icon_xpath = "//i[@data-original-title='Task']";
	private String FollowUp_icon_xpath = "//i[@data-original-title='Follow Up']";

	private String Delete_icon_xpath = "//div[@class='table-responsive']//i[@class='icon-close']";
	private String CreateNew_btn_xpath = "//button[contains(text(),'New Activity')]";
	private String Search_btn_xpath = "//button[contains(@id,'btn-search')]";
	private String Clear_btn_xpath = "//button[contains(text(),'Clear')]";
	private String ActivityList_tbl_xpath = "//div[@id='transferee-list-activity-tab' or @id = 'table-list-activity']//table/tbody | //table[@id='table-list-activities']/tbody";

	public Activities_tab(DriverBase driverbase) {
		// TODO Auto-generated constructor stub
		super(driverbase);
		ActivityDetails_mdal = new ActivityDetails_mdal(driverbase);
		Shortcuts_menu = new Shortcuts_menu(driverbase);
		ARCLoading = new ARCLoading(driverbase);
		Messages_Notification = new Messages_Notification(driverbase);
	}

	public WebElement ActivityList_tbl() {
		return GetElement(ActivityList_tbl_xpath);

	}

	public WebElement activity_tab() {
		return GetElement(activity_tab_xpath);

	}

	public WebElement ActivityName_lnk(String title) {
		String xpath_str = ActivityName_lnk_xpath + "//span[text() ='" + title + "'] | //a[text() ='" + title + "']";
		return GetElement(xpath_str);
	}

	public WebElement Task_icon() {
		return GetElement(Task_icon_xpath);

	}

	public WebElement FollowUp_icon() {
		return GetElement(FollowUp_icon_xpath);

	}

	public WebElement Delete_icon() {
		return GetElement(Delete_icon_xpath);

	}

	public WebElement CreateNew_btn() {
		return GetElement(CreateNew_btn_xpath);

	}

	public WebElement Search_btn() {
		return GetElement(Search_btn_xpath);

	}

	public WebElement Clear_btn() {
		return GetElement(Clear_btn_xpath);

	}

	public WebElement SearchFromDate_txt() {
		return GetElement(SearchFromDate_txt_xpath);

	}

	public WebElement SearchToDate_txt() {
		return GetElement(SearchToDate_txt_xpath);

	}

	public WebElement SearchType_slect() {
		return GetElement(SearchType_slect_xpath);

	}

	public WebElement SearchPriority_slect() {
		return GetElement(SearchPriority_slect_xpath);

	}

	public WebElement SearchStatus_slect() {
		return GetElement(SearchStatus_slect_xpath);

	}

	// =================================METHOD

	public void SearchActivity_func(String StartDate_str, String EndDate_str) {
		try {
			if (StartDate_str != "") {

				SearchFromDate_txt().sendKeys(StartDate_str);

			}
			if (EndDate_str != "") {
				SearchFromDate_txt().sendKeys(EndDate_str);

			}

			Optimize_ElementClick(Search_btn());

			Thread.sleep(3000);
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void VerifyActivityExistTable_func(Activity_Info Act) {
		boolean exist = false;
		List<String> records = GetTableRecordsPerPaging_func(ActivityList_tbl());

		for (String index_str : records) {
			if (index_str.contains(Act.Title)) {
				exist = true;
				break;
			} // end if
		} // end foreach

		if (!exist) {
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "Activity[" + Act.Title + "] NOT exist in table.\n";
		} // end if

	}

	public void Verify_Activity_Not_Exist_Table_func(Activity_Info Act, String note) {
		VerifyDataNotExistTable_func(ActivityList_tbl(), Act.Title, note);
	}

	public void CreateActivity_func(Activity_Info activity_arg) {

		if (activity_tab() == null) {
			Wait_For_ElementDisplay(activity_tab_xpath);
		}

		Optimize_ElementClick(activity_tab());

		Optimize_ElementClick(CreateNew_btn());

		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();

		// Fill info on modal
		ActivityDetails_mdal.FillModalDetails(activity_arg);

		// Submit OR Cancel creating

		Optimize_ElementClick(ActivityDetails_mdal.Submit_btn());

		try {

			Thread.sleep(4000);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,
				"When creating Activity.\n", false);

	}// end void

	public void EditActivity_func(String old_title, Activity_Info new_activity_arg) {
		System.out.println(old_title);
		ActivityName_lnk(old_title).click();

		ARCLoading.Wait_Util_FinishedPageLoading();

		Optimize_ElementClick(ActivityDetails_mdal.Edit_btn());

		ARCLoading.Wait_Util_FinishedPageLoading();

		ActivityDetails_mdal.FillModalDetails(new_activity_arg);

		ActivityDetails_mdal.Submit_btn().click();

		try {

			Thread.sleep(4000);

			Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,
					"When creating Activity.\n", false);

			if (ActivityDetails_mdal.ModalClose_btn() != null) {
				Optimize_ElementClick(ActivityDetails_mdal.ModalClose_btn());
				Thread.sleep(1000);

			} // end if

		} // end try
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}// end void

	// METHOD: DeleteActivity
	public void DeleteActivity_func(String activity_title) {
		Delete_icon().click();

		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);

		if (Messages_Notification.OppsOk_btn() != null)
			Messages_Notification.OppsOk_btn().click();
		else
			Messages_Notification.Delete_btn().click();

		Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg,
				"when Delete Activity.", false);

	}// end void

	public void searchType(String type) {
		try {
			Optimize_ElementClick(activity_tab());

			String note_str = "Search Activity with Type.";

			SelectDropdownItem(SearchType_slect(), type, note_str);

			Optimize_ElementClick(Search_btn());

			Thread.sleep(3000);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}// end void

	public void searchPriority(String priority) throws InterruptedException {
		Thread.sleep(3000);
		String note_str = "Search Activity with Priority.";
		SelectDropdownItem(SearchPriority_slect(), priority, note_str);

		Search_btn();

		Thread.sleep(3000);
	}// end void

	public void Search_Activity_With_Status_func(String status) {
		try {

			Thread.sleep(3000);
			String note_str = "Search Activity with Status.";

			SelectDropdownItem(SearchStatus_slect(), status, note_str);

			Search_btn();

			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}// end void

	public void Create_Activity_Via_ShortCut(Activity_Info new_activity) {

		Wait_For_ElementEnable(Shortcuts_menu.NewTask_btn());

		Shortcuts_menu.NewTask_btn().click();

		String note_str = "Details - Heading Entity Dropdown.";

		// Handle for shortcut on Task List Page
		if (Shortcuts_menu.Entity_ddl() != null) {
			SelectDropdownItem(Shortcuts_menu.Entity_ddl(), new_activity.AssigneeType, note_str);
		}

		// PageObjects.Wait_For_ElementDisplay(ActivityDetails_mdal.ActivityType_slect());

		// Fill info on modal
		ActivityDetails_mdal.FillModalDetails(new_activity);

		// Submit OR Cancel creating

		ActivityDetails_mdal.Submit_btn().click();

		try {

			Thread.sleep(4000);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String msg = Messages_Notification.notification_msg().getText();

		String expected_msg = "Server error Request. Please try again or contact IT helpdesk.";

		System.out.println(msg);

		if (msg == expected_msg) {
			TestConfigs.glb_TCFailedMessage += "Unsuccessfull Activity Creation.Msg: '" + msg + "'.\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}

}
