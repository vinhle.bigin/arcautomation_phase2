package pages;

import java.text.ParseException;

import java.util.List;
import java.util.Date;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.HBOService_Info;
import businessObjects.HBO_Offer_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;
import custom_Func.Data_Optimize;
import custom_Func.DateTime_Manage;




public class Transf_HBO_details  extends PageObjects{

	
	public Transf_HBO_details(DriverBase driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	private  String actived_detail_mdal_xpath = "//div[contains(@id,'modal') and contains(@style,'display: block')]"; 
	
	private  String CurrentListPrice_txt_xpath = "//div[@org-placeholder = 'Current Listing Price']//input";
	private  String BuyoutOfferPrice_txt_xpath = "//div[@org-placeholder = 'Buyout Offer Price']//input";
	private  String EstSellPrice_txt_xpath = "//div[@org-placeholder = 'Estimated Selling Price']//input";
	private  String CurrentDays_txt_xpath = "//div[@org-placeholder = 'Current Days On Market']//input";
	private  String AverageDays_txt_xpath = "//div[@org-placeholder = 'Average Days On Market']//input";
	
	//Transaction Detail
	private  String TransDetail_Edit_btn_xpath = "//a[@id = 'icon-transaction-detail']";
	private  String TransDetail_Update_btn_xpath = "//div[@id = 'transaction-detail']//button[@data-control = 'update']";
	private  String TransDetail_Cancel_btn_xpath = "//div[@id = 'transaction-detail']//button[@data-control = 'cancel-update']";
		//HBO Specialist
	private  String UserList_ddl_xpath = "//div[@org-placeholder = 'User List']//select";
	private  String Assistant_ddl_xpath = "//div[@org-placeholder = 'Assistant']//select";
		//Acquisition/Resale Details
	private  String AcquistionType_ddl_xpath = "//div[@org-placeholder = 'Acquisition Type']//select";
	private  String AcquisitionAmount_txt_xpath = "//div[@org-placeholder = 'Acquisition Amount']//input";
	private  String AcquisitionDate_txt_xpath = "//div[@org-placeholder = 'Acquisition Date']//input";
	private  String GuaOfferType_ddl_xpath = "//div[@org-placeholder = 'Guaranteed Offer Type']//select";
	private  String ReleaseAmount_txt_xpath = "//div[@org-placeholder = 'Resale Amount']//input";
	private  String ReleaseDate_txt_xpath = "//div[@org-placeholder = 'Resale Date']//input";
	private  String SpecInstruc_txt_xpath = "//div[@org-placeholder = 'Special Instructions']//textarea";
		//Forecasting
	private  String AntiEquityAmount_txt_xpath = "//div[@org-placeholder = 'Anticipated Equity Amount']//input";
	private  String AntiEquityDate_txt_xpath = "//div[@org-placeholder = 'Anticipated Equity Date']//input";
	private  String AntiEquityProceedsReturnedAmount_txt_xpath = "//div[@org-placeholder = 'Anticipated Equity Proceeds Returned Amount']//input";
	private  String AntiProceedReturnDate_txt_xpath = "//div[@org-placeholder = 'Anticipated Proceed Return Date']//input";
		//Actual
	private  String ActualEquitySenttoEE_txt_xpath = "//div[@org-placeholder = 'Actual Equity Sent to EE']//input";
	private  String ActualEquityProceedsReturnedAmount_txt_xpath = "//div[@org-placeholder = 'Actual Equity Proceeds Returned Amount']//input";
	private  String ActualDateEquitySenttoEE_txt_xpath = "//div[@org-placeholder = 'Actual Date Equity Sent to EE']//input";
	private  String ActualDateEquityProceedsReturned_txt_xpath = "//div[@org-placeholder = 'Actual Date Equity Proceeds Returned']//input";
	private  String Note_txt_xpath = "//div[@org-placeholder = 'Note']//textarea";
	
	//Inspections Need
	private  String NewIns_btn_xpath = "//div[@id = 'loading-inspection-panel-inspection-need']//button[@class= 'btn btn-custom btn-rounded']";
	private  String InsType_ddl_xpath = "//div[@org-placeholder = 'Type']//select";
	private  String Ins_OrderDate_txt_xpath = "//div[@org-placeholder = 'Order Date']//input";
	private  String InsDate_txt_xpath = "//div[@org-placeholder = 'Inspection Date']//input";
	private  String Ins_CompletedDate_txt_xpath = "//div[@org-placeholder = 'Completed Date']//input";
	private  String Ins_CanceledDate_txt_xpath = "//div[@org-placeholder = 'Cancelled Date']//input";
	private  String FollowUp_chbox_xpath = actived_detail_mdal_xpath+"//input[@type = 'checkbox']";
	private  String Ins_Submit_btn_xpath = actived_detail_mdal_xpath+"//button[@data-control = 'create']";
	private  String Ins_Cancel_btn_xpath = actived_detail_mdal_xpath+"//button[@data-control = 'cancel-create']";
	private  String Ins_Close_btn_xpath = actived_detail_mdal_xpath+"//a[@data-original-title = 'Close']";
	private  String Ins_Edit_icon_xpath = "//a[@data-original-title = 'Edit']";
	private  String Ins_Remove_btn_xpath = "//i[@class='icon-close']";
	private  String Ins_tbl_xpath = "//div[@id='inspection-table-inspection-need']//table/tbody";
	private  String Ins_ModalConfirm_Delete_btn_xpath = "//div[@id='confirm-modal-component']//button[contains(text(),'Delete')]";
	private  String Ins_Edit_btn_xpath = "//div[@id = 'loading-modal-update-inspection-need']//a[@data-original-title = 'Edit']";
	private  String Ins_Update_btn_xpath = "//div[@id = 'loading-modal-update-inspection-need']//button[@data-control = 'update']";
	
	//Required Repair
	private  String NewRepair_btn_xpath = "//div[@id = 'loading-inspection-panel-repair-inspections']//button[@class= 'btn btn-custom btn-rounded']";
	private  String RepairType_txt_xpath = "//div[@org-placeholder = 'Repair Type']//input";
	private  String Repair_Amount_txt_xpath = "//div[@id= 'loading-modal-repair-inspections']//div[@org-placeholder = 'Amount']//input";
	private  String Option_Selected_ddl_xpath = "//div[@org-placeholder = 'Option Selected']//select";
	private  String Completed_chbox_xpath = "//div[@id = 'loading-modal-repair-inspections']//span[@class = 'checkbox__checkmark']";
	private  String Repair_Submit_btn_xpath = "//div[@id = 'loading-modal-repair-inspections']//button[@data-control = 'create']";
	private  String Repair_Cancel_btn_xpath = "//div[@id = 'loading-modal-repair-inspections']//button[@data-control = 'cancel-create']";
	private  String Repair_Close_btn_xpath = "//div[@id = 'loading-modal-repair-inspections']//a[@data-original-title = 'Close']";
	private  String Repair_Edit_icon_xpath = "//div[@id = 'inspection-table-repair-inspections']//a[@data-original-title = 'Edit']";
	private  String Repair_Remove_btn_xpath = "//i[@class='icon-close']";
	private  String Repair_tbl_xpath = "//div[@id='inspection-table-repair-inspections']//table/tbody";
	private  String Repair_ModalConfirm_Delete_btn_xpath = "//div[@id='confirm-modal-component']//button[contains(text(),'Delete')]";
	private  String Repair_Edit_btn_xpath = "//div[@id = 'loading-modal-update-repair-inspections']//a[@data-original-title = 'Edit']";
	private  String Repair_Update_btn_xpath = "//div[@id = 'loading-modal-update-repair-inspections']//button[@data-control = 'update']";
	
	//Seller Concessions
	private  String NewConcession_btn_xpath = "//div[@id = 'loading-inspection-panel-edit-concession']//button[@class= 'btn btn-custom btn-rounded']";
	private  String AdjustExplain_txt_xpath = "//div[@org-placeholder = 'Adjustment Explaination']//input";
	private  String Concession_Amount_txt_xpath = "//div[@id= 'loading-modal-edit-concession']//div[@org-placeholder = 'Amount']//input";
	private  String Concession_Submit_btn_xpath = "//div[@id = 'loading-modal-edit-concession']//button[@data-control = 'create']";
	private  String Concession_Cancel_btn_xpath = "//div[@id = 'loading-modal-edit-concession']//button[@data-control = 'cancel-update']";
	private  String Concession_Close_btn_xpath = "//div[@id = 'loading-modal-edit-concession']//a[@data-original-title = 'Close']";
	private  String Concession_Edit_icon_xpath = "//div[@id = 'inspection-table-edit-concession']//a[@data-original-title = 'Edit']";
	private  String Concession_Remove_btn_xpath = "//i[@class='icon-close']";
	private  String Concessions_tbl_xpath = "//div[@id='inspection-table-edit-concession']//table/tbody";
	private  String Concession_ModalConfirm_Delete_btn_xpath = "//div[@id='inspection-panel-edit-concession']//button[contains(text(),'Delete')]";
	private  String Concession_Edit_btn_xpath = "//div[@id = 'loading-modal-update-edit-concession']//a[@data-original-title = 'Edit']";
	private  String Concession_Update_btn_xpath = "//div[@id = 'loading-modal-update-edit-concession']//button[@data-control = 'update']";
	
	//OFFER INFO
	private  String Offer_AddOffer_btn_xpath = "//button[@id='dropdown-offer']";
	private  String Offer_HeaderTitle_vlue_xpath = "//div[contains(text(),'offer_type')]";
	private  String Offer_Amount_txt_xpath = "//div[@org-placeholder='Amount']//input";
	private  String Offer_OfferedDate_txt_xpath = "//div[@org-placeholder='Offered date']//input";
	private  String Offer_ExpiredDate_txt_xpath = "//div[@org-placeholder='Expired date']//input";
	private  String Offer_IsSelectOffer_opt_xpath = "//label[contains(text(),'Selected offer')]/..//input[@type ='checkbox']";
	private  String Offer_ContractDate_txt_xpath = "//div[@org-placeholder='Contract date']//input";
	private  String Offer_AcceptDate_txt_xpath = "//div[@org-placeholder='Acceptance date']//input";
	private  String Offer_VacatePeriod_txt_xpath = "//div[@org-placeholder='Vacate period']//input";
	private  String OfferConting_IsTitle_opt_xpath = "//div[@id='offer-detail']//label[contains(text(),'Title')]/..//input";
	private  String OfferConting_TitleNote_txt_xpath = "//div[@id='offer-detail']//label[contains(text(),'Title')]/../..//textarea";
	private  String OfferConting_IsInspect_opt_xpath = "//div[@id='offer-detail']//label[contains(text(),'Inspection')]/..//input";
	private  String OfferConting_InspectNote_txt_xpath = "//div[@id='offer-detail']//label[contains(text(),'Inspection')]/../..//textarea";
	private  String OfferConting_IsPersonProp_opt_xpath = "//div[@id='offer-detail']//label[contains(text(),'Personal Property')]/..//input";
	private  String OfferConting_PersonPropNote_txt_xpath = "//div[@id='offer-detail']//label[contains(text(),'Personal Property')]/../..//textarea";
	private  String OfferConting_IsInspectClear_opt_xpath = "//div[@id='offer-detail']//label[contains(text(),'Inspection Clear')]/..//input";
	private  String OfferConting_InspectClearNote_txt_xpath = "//div[@id='offer-detail']//label[contains(text(),'Inspection Clear')]/../..//textarea";
	private  String Offer_Edit_btn_xpath = "//div[@id='offer-detail']//a[contains(text(),'Edit')]";
	private  String Offer_Update_btn_xpath = "//div[@id='offer-detail']//button[@data-control='update']";
	private  String Offer_Cancel_btn_xpath = "//div[@id='offer-detail']//button[@data-control='cancel-update']";
	private  String Offer_TypeList_ddl_xpath = "//div[@id='offer-detail']//div[@class='dropdown open']//ul";

	
	public  WebElement Offer_AddOffer_btn() {
	  return GetElement(Offer_AddOffer_btn_xpath);
	 }
	
public  WebElement Offer_HeaderTitle_vlue(String offertype_str) {
	
	String tmp_Offer_HeaderTitle_vlue_xpath = Offer_HeaderTitle_vlue_xpath.replace("offer_type", offertype_str);
	
	return GetElement(tmp_Offer_HeaderTitle_vlue_xpath);
	 }

public  WebElement Offer_Amount_txt(String offertype_str) {
	
	String tmp_Offer_Amount_txt_xpath = Offer_HeaderTitle_vlue_xpath.replace("offer_type", offertype_str) +"/../.."+Offer_Amount_txt_xpath;
	
	  return GetElement(tmp_Offer_Amount_txt_xpath);
	 }

public  WebElement Offer_OfferedDate_txt(String offertype_str) {
	
	String tmp_OfferedDate_txt_xpath = Offer_HeaderTitle_vlue_xpath.replace("offer_type", offertype_str) +"/../.."+Offer_OfferedDate_txt_xpath;
	
	  return GetElement(tmp_OfferedDate_txt_xpath);
	 }

public  WebElement Offer_ExpiredDate_txt(String offertype_str) {
	
	String tmp_Offer_ExpiredDate_txt_xpath = Offer_HeaderTitle_vlue_xpath.replace("offer_type", offertype_str) +"/../.."+Offer_ExpiredDate_txt_xpath;
	
	  return GetElement(tmp_Offer_ExpiredDate_txt_xpath);
	 }

public  WebElement Offer_IsSelectOffer_opt(String offertype_str) {
	
	String tmp_Offer_IsSelectOffer_opt_xpath = Offer_HeaderTitle_vlue_xpath.replace("offer_type", offertype_str) +"/../.."+Offer_IsSelectOffer_opt_xpath;
	
	  return GetElement(tmp_Offer_IsSelectOffer_opt_xpath);
	 }

public  WebElement Offer_ContractDate_txt(String offertype_str) {
	
	String tmp_Offer_ContractDate_txt_xpath = Offer_HeaderTitle_vlue_xpath.replace("offer_type", offertype_str) +"/../.."+Offer_ContractDate_txt_xpath;
	
	  return GetElement(tmp_Offer_ContractDate_txt_xpath);
	 }

public  WebElement Offer_AcceptDate_txt(String offertype_str) {
	
	String tmp_Offer_AcceptDate_txt_xpath = Offer_HeaderTitle_vlue_xpath.replace("offer_type", offertype_str) +"/../.."+Offer_AcceptDate_txt_xpath;
	
	  return GetElement(tmp_Offer_AcceptDate_txt_xpath);
	 }

public  WebElement Offer_VacatePeriod_txt(String offertype_str) {
	
	String tmp_Offer_VacatePeriod_txt_xpath = Offer_HeaderTitle_vlue_xpath.replace("offer_type", offertype_str) +"/../.."+Offer_VacatePeriod_txt_xpath;
	
	  return GetElement(tmp_Offer_VacatePeriod_txt_xpath);
	 }


public  WebElement OfferConting_IsTitle_opt() {
	  return GetElement(OfferConting_IsTitle_opt_xpath);
	 }
public  WebElement OfferConting_TitleNote_txt() {
	  return GetElement(OfferConting_TitleNote_txt_xpath);
	 }
public  WebElement OfferConting_IsInspect_opt() {
	  return GetElement(OfferConting_IsInspect_opt_xpath);
	 }
public  WebElement OfferConting_InspectNote_txt() {
	  return GetElement(OfferConting_InspectNote_txt_xpath);
	 }
public  WebElement OfferConting_IsPersonProp_opt() {
	  return GetElement(OfferConting_IsPersonProp_opt_xpath);
	 }
public  WebElement OfferConting_PersonPropNote_txt() {
	  return GetElement(OfferConting_PersonPropNote_txt_xpath);
	 }
public  WebElement OfferConting_IsInspectClear_opt() {
	  return GetElement(OfferConting_IsInspectClear_opt_xpath);
	 }
public  WebElement OfferConting_InspectClearNote_txt() {
	  return GetElement(OfferConting_InspectClearNote_txt_xpath);
	 }
public  WebElement Offer_Edit_btn() {
	  return GetElement(Offer_Edit_btn_xpath);
	 }
public  WebElement Offer_Update_btn() {
	  return GetElement(Offer_Update_btn_xpath);
	 }
public  WebElement Offer_Cancel_btn() {
	  return GetElement(Offer_Cancel_btn_xpath);
	 }
public  WebElement Offer_TypeList_ddl() {
	  return GetElement(Offer_TypeList_ddl_xpath);
	 }

	 
	
	public  WebElement Ins_ModalConfirm_Delete_btn() {
		  return GetElement(Ins_ModalConfirm_Delete_btn_xpath);
	}
	
	public  WebElement Ins_tbl() {
		  return GetElement(Ins_tbl_xpath);
	}
	
	public  WebElement Ins_Update_btn() {
		  return GetElement(Ins_Update_btn_xpath);
	}
	
	public  WebElement NewIns_btn() {
		  return GetElement(NewIns_btn_xpath);
	}
	
	public  WebElement InsType_ddl() {
		  return GetElement(InsType_ddl_xpath);
	}
	
	public  WebElement Ins_OrderDate_txt() {
		  return GetElement(Ins_OrderDate_txt_xpath);
	}
	
	public  WebElement InsDate_txt() {
		  return GetElement(InsDate_txt_xpath);
	}
	
	public  WebElement Ins_CompletedDate_txt() {
		  return GetElement(Ins_CompletedDate_txt_xpath);
	}
	
	public  WebElement Ins_CanceledDate_txt() {
		  return GetElement(Ins_CanceledDate_txt_xpath);
	}
	
	public  WebElement FollowUp_chbox() {
		  return GetElement(FollowUp_chbox_xpath);
	}
	
	public  WebElement Ins_Submit_btn() {
		  return GetElement(Ins_Submit_btn_xpath);
	}
	
	public  WebElement Ins_Cancel_btn() {
		  return GetElement(Ins_Cancel_btn_xpath);
	}
	
	public  WebElement Ins_Close_btn() {
		  return GetElement(Ins_Close_btn_xpath);
	}
	
	public  WebElement Ins_Remove_btn(String inspection) {
		  return GetElementInTable(inspection, Ins_tbl(), Ins_Remove_btn_xpath);
	}
	
	public  WebElement Ins_Edit_btn() {
		  return GetElement(Ins_Edit_btn_xpath);
	}
	
	public  WebElement Ins_Edit_icon(String inspection) {
		  return GetElementInTable(inspection, Ins_tbl(), Ins_Edit_icon_xpath);
	}
	
	public  WebElement Repair_ModalConfirm_Delete_btn() {
		  return GetElement(Repair_ModalConfirm_Delete_btn_xpath);
	}
	
	public  WebElement Repair_tbl() {
		  return GetElement(Repair_tbl_xpath);
	}
	
	public  WebElement Repair_Update_btn() {
		  return GetElement(Repair_Update_btn_xpath);
	}
	
	public  WebElement NewRepair_btn() {
		  return GetElement(NewRepair_btn_xpath);
	}
	
	public  WebElement RepairType_txt() {
		  return GetElement(RepairType_txt_xpath);
	}
	
	public  WebElement Option_Selected_ddl() {
		  return GetElement(Option_Selected_ddl_xpath);
	}
	
	public  WebElement Completed_chbox() {
		  return GetElement(Completed_chbox_xpath);
	}
	
	public  WebElement Repair_Amount_txt() {
		  return GetElement(Repair_Amount_txt_xpath);
	}
	
	public  WebElement Repair_Submit_btn() {
		  return GetElement(Repair_Submit_btn_xpath);
	}
	
	public  WebElement Repair_Cancel_btn() {
		  return GetElement(Repair_Cancel_btn_xpath);
	}
	
	public  WebElement Repair_Close_btn() {
		  return GetElement(Repair_Close_btn_xpath);
	}
	
	public  WebElement Repair_Remove_btn(String repair) {
		  return GetElementInTable(repair, Repair_tbl(), Repair_Remove_btn_xpath);
	}
	
	public  WebElement Repair_Edit_btn() {
		  return GetElement(Repair_Edit_btn_xpath);
	}
	
	public  WebElement Repair_Edit_icon(String repair) {
		  return GetElementInTable(repair, Repair_tbl(), Repair_Edit_icon_xpath);
	}
	
	public  WebElement Concession_ModalConfirm_Delete_btn() {
		  return GetElement(Concession_ModalConfirm_Delete_btn_xpath);
	}
	
	public  WebElement Concessions_tbl() {
		  return GetElement(Concessions_tbl_xpath);
	}
	
	public  WebElement Concession_Update_btn() {
		  return GetElement(Concession_Update_btn_xpath);
	}
	
	public  WebElement NewConcession_btn() {
		  return GetElement(NewConcession_btn_xpath);
	}
	
	public  WebElement AdjustExplain_txt() {
		  return GetElement(AdjustExplain_txt_xpath);
	}
	
	public  WebElement Concession_Amount_txt() {
		  return GetElement(Concession_Amount_txt_xpath);
	}
	
	public  WebElement Concession_Submit_btn() {
		  return GetElement(Concession_Submit_btn_xpath);
	}
	
	public  WebElement Concession_Cancel_btn() {
		  return GetElement(Concession_Cancel_btn_xpath);
	}
	
	public  WebElement Concession_Close_btn() {
		  return GetElement(Concession_Close_btn_xpath);
	}
	
	public  WebElement Concession_Remove_btn(String concession) {
		  return GetElementInTable(concession, Concessions_tbl(), Concession_Remove_btn_xpath);
	}
	
	public  WebElement Concession_Edit_btn() {
		  return GetElement(Concession_Edit_btn_xpath);
	}
	
	public  WebElement Concession_Edit_icon(String concession) {
		  return GetElementInTable(concession, Concessions_tbl(), Concession_Edit_icon_xpath);
	}
	
	public  WebElement TransDetail_Edit_btn() {
		  return GetElement(TransDetail_Edit_btn_xpath);
	}
	
	public  WebElement TransDetail_Update_btn() {
		  return GetElement(TransDetail_Update_btn_xpath);
	}
	
	public  WebElement TransDetail_Cancel_btn() {
		  return GetElement(TransDetail_Cancel_btn_xpath);
	}
	
	public  WebElement SpecInstruc_txt() {
		  return GetElement(SpecInstruc_txt_xpath);
	}
	
	public  WebElement AntiEquityAmount_txt() {
		  return GetElement(AntiEquityAmount_txt_xpath);
	}
	
	public  WebElement AntiEquityDate_txt() {
		  return GetElement(AntiEquityDate_txt_xpath);
	}
	
	public  WebElement AntiEquityProceedsReturnedAmount_txt() {
		  return GetElement(AntiEquityProceedsReturnedAmount_txt_xpath);
	}
	
	public  WebElement AntiProceedReturnDate_txt() {
		  return GetElement(AntiProceedReturnDate_txt_xpath);
	}
	
	public  WebElement ActualEquitySenttoEE_txt() {
		  return GetElement(ActualEquitySenttoEE_txt_xpath);
	}
	
	public  WebElement ActualEquityProceedsReturnedAmount_txt() {
		  return GetElement(ActualEquityProceedsReturnedAmount_txt_xpath);
	}
	
	public  WebElement ActualDateEquitySenttoEE_txt() {
		  return GetElement(ActualDateEquitySenttoEE_txt_xpath);
	}
	
	public  WebElement ActualDateEquityProceedsReturned_txt() {
		  return GetElement(ActualDateEquityProceedsReturned_txt_xpath);
	}
	
	public  WebElement Note_txt() {
		  return GetElement(Note_txt_xpath);
	}
	
	public  WebElement Assistant_ddl() {
		  return GetElement(Assistant_ddl_xpath);
	}
	
	public  WebElement CurrentListPrice_txt() {
		  return GetElement(CurrentListPrice_txt_xpath);
	}
	
	public  WebElement BuyoutOfferPrice_txt() {
		  return GetElement(BuyoutOfferPrice_txt_xpath);
	}
	
	public  WebElement EstSellPrice_txt() {
		  return GetElement(EstSellPrice_txt_xpath);
	}
	
	public  WebElement CurrentDays_txt() {
		  return GetElement(CurrentDays_txt_xpath);
	}
	
	public  WebElement AverageDays_txt() {
		  return GetElement(AverageDays_txt_xpath);
	}
	
	public  WebElement UserList_ddl() {
		  return GetElement(UserList_ddl_xpath);
	}
	
	public  WebElement AcquistionType_ddl() {
		  return GetElement(AcquistionType_ddl_xpath);
	}
	
	public  WebElement AcquisitionAmount_txt() {
		  return GetElement(AcquisitionAmount_txt_xpath);
	}
	
	public  WebElement AcquisitionDate_txt() {
		  return GetElement(AcquisitionDate_txt_xpath);
	}
	
	public  WebElement GuaOfferType_ddl() {
		  return GetElement(GuaOfferType_ddl_xpath);
	}
	
	public  WebElement ReleaseAmount_txt() {
		  return GetElement(ReleaseAmount_txt_xpath);
	}
	
	public  WebElement ReleaseDate_txt() {
		  return GetElement(ReleaseDate_txt_xpath);
	}
	
	//========================================================METHODS
	
	public  void Add_New_Offer_func(HBO_Offer_Info offerinfo, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Offer_Edit_btn());
				
		Optimize_ElementClick(Offer_AddOffer_btn());
		
		SelectDropdownItem(Offer_TypeList_ddl(), offerinfo.Type, "Offer_TypeList_ddl");
		
		String tmp_Offer_Amount_txt_xpath = Offer_HeaderTitle_vlue_xpath.replace("offer_type", offerinfo.Type) +"/../.."+Offer_Amount_txt_xpath;
		
		Wait_For_ElementDisplay(tmp_Offer_Amount_txt_xpath);
		
		ClearThenEnterValueToField(Offer_Amount_txt(offerinfo.Type), Double.toString(offerinfo.Amount));
		
		Optimize_ElementSendkey(Offer_OfferedDate_txt(offerinfo.Type), offerinfo.Date);
		
		Offer_OfferedDate_txt(offerinfo.Type).sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Offer_ExpiredDate_txt(offerinfo.Type), offerinfo.ExpireDate);
		
		Offer_ExpiredDate_txt(offerinfo.Type).sendKeys(Keys.TAB);
		
		if(offerinfo.IsSelcted==true)
		{
			Optimize_ElementClick(Offer_IsSelectOffer_opt(offerinfo.Type));
			
			ScrollElementtoViewPort_func(Offer_ContractDate_txt(offerinfo.Type));
			
			Optimize_ElementSendkey(Offer_ContractDate_txt(offerinfo.Type), offerinfo.ContractDate);
			
			Offer_ContractDate_txt(offerinfo.Type).sendKeys(Keys.TAB);
			
			Optimize_ElementSendkey(Offer_AcceptDate_txt(offerinfo.Type), offerinfo.AcceptDate);
			
			Offer_AcceptDate_txt(offerinfo.Type).sendKeys(Keys.TAB);
			
			Optimize_ElementSendkey(Offer_VacatePeriod_txt(offerinfo.Type), offerinfo.VacatePeriod);
			
			Offer_VacatePeriod_txt(offerinfo.Type).sendKeys(Keys.TAB);
		}
		
		if(button.equals("submit")||button.equals("Submit"))
		{
			Optimize_ElementClick(Offer_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating Offer Details",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
			
		}//end if
				
	}
	
	public  void Edit_TransactionDetails_func(HBOService_Info service, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(TransDetail_Edit_btn());
		
		SelectDropdownItem(UserList_ddl(), service.Transt_UserList, "");
		
		SelectDropdownItem(Assistant_ddl(), service.Transt_Assistant, "");
		
		SelectDropdownItem(AcquistionType_ddl(), service.Transt_AcquisType, "AcquistionType_ddl");
		
		ScrollElementtoViewPort_func(AcquisitionAmount_txt());
		
		ClearThenEnterValueToField(AcquisitionAmount_txt(), service.Transt_AcquisAmount);
		
		Optimize_ElementSendkey(AcquisitionDate_txt(), service.Transt_AcquisDate);
		
		AcquisitionDate_txt().sendKeys(Keys.ENTER);
		
		SelectDropdownItem(GuaOfferType_ddl(), service.Transt_GuarantOfferType, "GuaOfferType_ddl");
		
		
		//Home Buyout Cash Flow section
		if(button.equals("submit")||button.equals("Submit"))
		{
			Optimize_ElementClick(TransDetail_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Updating Transaction Details",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}//end if
	}
	
	public  void Create_New_Concessions_func(HBOService_Info service, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(NewConcession_btn());
		
		ClearThenEnterValueToField(AdjustExplain_txt(), service.Seller_AdjustExplain);
		
		ClearThenEnterValueToField(Concession_Amount_txt(), service.Seller_Amount);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Concession_Submit_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Creating New Concession",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
	}
	
	public  void Edit_Concessions_func(String adjust, HBOService_Info service, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Concession_Edit_icon(adjust));
		
		Optimize_ElementClick(Concession_Edit_btn());
		
		service.Seller_AdjustExplain = "Update_Adjustment Explaination";
		
		service.Seller_Amount = "555";
		
		
		ClearThenEnterValueToField(AdjustExplain_txt(), service.Seller_AdjustExplain);
		
		ClearThenEnterValueToField(Concession_Amount_txt(), service.Seller_Amount);
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Concession_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Editing New Concession",false);
			
			new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
		}
	}
	
	public  void Remove_Concession_In_The_Table_func(String concession)
	{
		Optimize_ElementClick(Concession_Remove_btn(concession));

		Optimize_ElementClick(Concession_ModalConfirm_Delete_btn());
	}
	
	public  void Create_New_Repair_func(HBOService_Info service, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(NewRepair_btn());
		
		ClearThenEnterValueToField(RepairType_txt(), service.Repair_RepairType);
		
		ClearThenEnterValueToField(Repair_Amount_txt(), service.Repair_Amount);
		
		SelectDropdownItem(Option_Selected_ddl(), service.Repair_OptionSlected,"");
		
		Optimize_ElementClick(Completed_chbox());
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Repair_Submit_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Creating New Concession",false);
		}
	}
	
	public  void Edit_Repair_func(HBOService_Info service, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Repair_Edit_icon(service.Repair_RepairType));
		
		Optimize_ElementClick(Repair_Edit_btn());
		
		service.Repair_RepairType = "Update Repair Type";
		
		service.Repair_Amount = "555";
		
		ClearThenEnterValueToField(RepairType_txt(), service.Repair_RepairType);
		
		ClearThenEnterValueToField(Repair_Amount_txt(), service.Repair_Amount);
		
		ClearThenEnterValueToField(Option_Selected_ddl(), service.Repair_OptionSlected);
		
		Optimize_ElementClick(Completed_chbox());
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Repair_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Creating New Repair",false);
		}
	}
	
	public  void Remove_Repair_In_The_Table_func(String repair)
	{
		Optimize_ElementClick(Repair_Remove_btn(repair));

		Optimize_ElementClick(Repair_ModalConfirm_Delete_btn());
	}
	
	public  void Create_New_Inspection_func(HBOService_Info service, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(NewIns_btn());
		
		SelectDropdownItem(InsType_ddl(), service.Ins_Type,"");
		
		Optimize_ElementSendkey(Ins_OrderDate_txt(), service.Ins_OrderDate);
		
		Ins_OrderDate_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(InsDate_txt(), service.Ins_InsDate);
		
		InsDate_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Ins_CompletedDate_txt(), service.Ins_CompletedDate);
		
		Ins_CompletedDate_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Ins_CanceledDate_txt(), service.Ins_CancelledDate);
		
		Ins_CanceledDate_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementClick(FollowUp_chbox());
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Ins_Submit_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Creating New Concession",false);
		}
		
		new ARCLoading(driverbase).Wait_Util_FinishedARClogoLoading();
	}
	
	public  void Edit_Inspection_func(HBOService_Info service, String button)
	{
		Messages_Notification Messages_Notification = new Messages_Notification(driverbase);
		
		Optimize_ElementClick(Ins_Edit_icon(service.Ins_Type));
		
		Optimize_ElementClick(Ins_Edit_btn());
		
		service.Ins_Type = "Mold";
		
		Date Date = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),1);
		
		service.Ins_OrderDate = DateTime_Manage.ConvertDatetoString(Date);
		
		service.Ins_InsDate = DateTime_Manage.ConvertDatetoString(Date);
		
		service.Ins_CompletedDate = DateTime_Manage.ConvertDatetoString(Date);
		
		service.Ins_CancelledDate = DateTime_Manage.ConvertDatetoString(Date);
		
		SelectDropdownItem(InsType_ddl(), service.Ins_Type,"Inspection Type Dropdown");
		
		ClearThenEnterValueToField(Ins_OrderDate_txt(), service.Ins_OrderDate);
		
		Ins_OrderDate_txt().sendKeys(Keys.ENTER);
		
		ClearThenEnterValueToField(InsDate_txt(), service.Ins_InsDate);
		
		InsDate_txt().sendKeys(Keys.ENTER);
		
		ClearThenEnterValueToField(Ins_CompletedDate_txt(), service.Ins_CompletedDate);
		
		Ins_CompletedDate_txt().sendKeys(Keys.ENTER);
		
		Optimize_ElementSendkey(Ins_CanceledDate_txt(), service.Ins_CancelledDate);
		
		Ins_CanceledDate_txt().sendKeys(Keys.ENTER);
		
		
		String Is_follow_up = GetFieldText_func(FollowUp_chbox());
		if(!service.Ins_FollowUp.toString().equals(Is_follow_up))
			Optimize_ElementClick(FollowUp_chbox());
		
		if(button.equals("Submit")||button.equals("submit"))
		{
			Optimize_ElementClick(Ins_Update_btn());
			
			Messages_Notification.VerifyNotifyMsgNotDisplay_func(Messages_Notification.SystemError_msg, "When Creating New Repair",false);
		}
	}
	
	public  void Remove_Inspection_In_The_Table_func(String inpsection)
	{
		Optimize_ElementClick(Ins_Remove_btn(inpsection));

		Optimize_ElementClick(Ins_ModalConfirm_Delete_btn());
	}
	
	//=============================================METHODS VERIFY
	
	public  void Verify_Offer_Detail_func(HBO_Offer_Info offerinfo)
	{
		String field = "";
		
		field = "Amount";
		
		String offer_price_str = Data_Optimize.Format_Double_WithCurrency(offerinfo.Amount);
		
		VerifyFieldValueEqual_func(field, Offer_Amount_txt(offerinfo.Type), offer_price_str, "");
		
		field = "Offered Date";
		
		VerifyFieldValueEqual_func(field, Offer_OfferedDate_txt(offerinfo.Type), offerinfo.Date, "");
		
		field = "Expired Date";
		
		VerifyFieldValueEqual_func(field, Offer_ExpiredDate_txt(offerinfo.Type), offerinfo.ExpireDate, "");
		
		if(offerinfo.IsSelcted==true)
		{
			field = "Contract Date";
			
			VerifyFieldValueEqual_func(field, Offer_ContractDate_txt(offerinfo.Type), offerinfo.ContractDate, "");
			
			field = "Acceptance Date";
			
			VerifyFieldValueEqual_func(field, Offer_AcceptDate_txt(offerinfo.Type), offerinfo.AcceptDate, "");
			
			field = "Vacate Period";
			
			VerifyFieldValueEqual_func(field, Offer_VacatePeriod_txt(offerinfo.Type), offerinfo.VacatePeriod, "");
		}
	}
	
	public  void Verify_Transaction_Detail_func(HBOService_Info service)
	{
		String field = "";
		
		field = "Acquisition Type";
		
		VerifyFieldValueEqual_func(field, AcquistionType_ddl(), service.Transt_AcquisType, "");
		
		field = "Acquisition Amount";
		
		VerifyFieldValueEqual_func(field, AcquisitionAmount_txt(), service.Transt_AcquisAmount, "");
		
		field = "Acquisition Date";
		
		VerifyFieldValueEqual_func(field, AcquisitionDate_txt(), service.Transt_AcquisDate, "");
		
		field = "Guatanteed Offer Type";
		
		VerifyFieldValueEqual_func(field, GuaOfferType_ddl(), service.Transt_GuarantOfferType, "");
		
		
	}
	
	public  void Verify_HBO_Summary_Section_func(double HBO_OfferPrice_str, String HS_ListPrice, String HS_ListStartDate_str, String Invent_AnlysAvrDay_str,String Invent_EstSellPrice_str)
	{
		try {
			String field = "";
			
			field = "Buyout Offer Price";
			
			String offer_price_str = Data_Optimize.Format_Double_WithCurrency(HBO_OfferPrice_str);
			
			if(offer_price_str.equals("$0.00"))
			{
				offer_price_str = "";
				
			}
			
			VerifyFieldValueEqual_func(field, BuyoutOfferPrice_txt(), offer_price_str, "");
			
			//sync HS service
			field = "Current Listing Price";
			
			if(HS_ListPrice.equals("$0.00"))
			{
				HS_ListPrice = "";
				
			}
			
			VerifyFieldValueEqual_func(field, CurrentListPrice_txt(), HS_ListPrice, "");
			
			field = "Current Day Market";
			
			String crrent_daymarket_str = "";
			if(!HS_ListStartDate_str.equals(""))
				crrent_daymarket_str = DateTime_Manage.Calculate_Days_Numb(HS_ListStartDate_str);
			
			VerifyFieldValueEqual_func(field, CurrentDays_txt(), crrent_daymarket_str, "");
			
			//Sync Inventory Manage
			
			field = "Estimated Selling Price";
			
			if(Invent_EstSellPrice_str.equals("$0.00"))
			{
				Invent_EstSellPrice_str = "";
				
			}
			VerifyFieldValueEqual_func(field, EstSellPrice_txt(), Invent_EstSellPrice_str, "");
			
			
			
			field = "Average Days On Market";
			
			VerifyFieldValueEqual_func(field, AverageDays_txt(), Invent_AnlysAvrDay_str, "");
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  void Verify_Concessions_In_Table_func(HBOService_Info service)
	{
		boolean exist = false;
		
		List<String> list_str = GetTableRecordsPerPaging_func(Concessions_tbl());
		
		String expect_str = service.Seller_AdjustExplain + "," + service.Seller_Amount;
		
		for(int i = 0;i <list_str.size();i++)
			if(		list_str.get(i).contains(service.Seller_AdjustExplain) && 
					list_str.get(i).contains(service.Seller_Amount)
				)
		{
			exist = true;
			break;
		}//end if

		if(exist==false)
		{
			TestConfigs.glb_TCStatus= false;
			TestConfigs.glb_TCFailedMessage+="Missing info in table - record.[Observed: "+list_str+" - Expected: "+expect_str+"] "+".]\n";
		}
	}
	
	public  void Verify_Concession_Not_Show_IntheTable_func(HBOService_Info service)
	{
		
		VerifyDataNotExistTable_func(Concessions_tbl(), service.Seller_AdjustExplain, "");
		
	}
	
	public  void Verify_Repairs_In_Table_func(HBOService_Info service)
	{
		boolean exist = false;
		
		List<String> list_str = GetTableRecordsPerPaging_func(Repair_tbl());
		
		String checkbox;
		
		String expect_str;
		
		if (service.Repair_Completed = true)
		{
			checkbox = "Yes";

			expect_str = service.Repair_RepairType + "|" + service.Repair_Amount + "|" + service.Repair_OptionSlected + "|" + checkbox;
		}
		else
		{
			checkbox = "No";

			expect_str = service.Repair_RepairType + "|" + service.Repair_Amount + "|" + service.Repair_OptionSlected + "|" + checkbox;
		}
		
		for(int i = 0;i <list_str.size();i++)
			if(		list_str.get(i).contains(service.Repair_RepairType) && 
					list_str.get(i).contains(service.Repair_Amount) &&
					list_str.get(i).contains(service.Repair_OptionSlected) &&
					list_str.get(i).contains(checkbox)
				)
		{
			exist = true;
			break;
		}//end if

		if(exist==false)
		{
			TestConfigs.glb_TCStatus= false;
			TestConfigs.glb_TCFailedMessage+="Missing info in table - record.[Observed: "+list_str+" - Expected: "+expect_str+"] "+".]\n";
		}
	}

	public  void Verify_Repair_Not_Show_IntheTable_func(HBOService_Info service)
	{
		VerifyDataNotExistTable_func(Repair_tbl(), service.Repair_RepairType, "");	
	}

	public  void Verify_Ins_In_Table_func(HBOService_Info service)
	{
		boolean exist = false;
		
		List<String> list_str = GetTableRecordsPerPaging_func(Ins_tbl());
		
		String checkbox;
		
		String expect_str;
		
		if (service.Ins_FollowUp = true)
		{
			checkbox = "Yes";

			expect_str = service.Ins_Type + "|" + service.Ins_OrderDate + "|" + service.Ins_InsDate + "|" + checkbox + "|" + service.Ins_CompletedDate + "|" + service.Ins_CancelledDate;
		}
		else
		{
			checkbox = "No";

			expect_str = service.Ins_Type + "|" + service.Ins_OrderDate + "|" + service.Ins_InsDate + "|" + checkbox + "|" + service.Ins_CompletedDate + "|" + service.Ins_CancelledDate;
		}
		
		for(int i = 0;i <list_str.size();i++)
			if(		list_str.get(i).contains(service.Ins_Type) && 
					list_str.get(i).contains(service.Ins_OrderDate) &&
					list_str.get(i).contains(service.Ins_InsDate) &&
					list_str.get(i).contains(checkbox) &&
					list_str.get(i).contains(service.Ins_CompletedDate) &&
					list_str.get(i).contains(service.Ins_CancelledDate)
				)
		{
			exist = true;
			break;
		}//end if

		if(exist==false)
		{
			TestConfigs.glb_TCStatus= false;
			TestConfigs.glb_TCFailedMessage+="Missing info in table - record.[Observed: "+list_str+" - Expected: "+expect_str+"] "+".]\n";
		}
	}
	
	public  void Verify_Ins_Not_Show_IntheTable_func(HBOService_Info service)
	{
		VerifyDataNotExistTable_func(Ins_tbl(), service.Ins_Type, "");	
	}

}
