package pages;


import org.junit.Assert;
import org.openqa.selenium.WebElement;

import baseClasses.PageObjects;
import businessObjects.Address_Info;
import businessObjects.Auth_Info;
import businessObjects.Client_Info;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;


public class AuthDetails_page extends PageObjects{
	
	NewAuth_mdal auth_details;
	
	public AuthDetails_page(DriverBase driver) {
		
		super(driver);
		// TODO Auto-generated constructor stub
		 auth_details = new NewAuth_mdal(driverbase);
	}

	//DEFINE ELEMENT:
	private String Edit_button_xpath = "//a[@data-original-title='Edit']";
	private String Update_btn_xpath = "//div[@class='panel-body']//button[@data-control='update']";
	private String Cancel_btn_xpath = "//div[@class='panel-body']//button[@data-control='cancel-update']";
	private String Approve_btn_xpath = "//div[@class='floating-action-group']//button[contains(text(), 'Approve')]";
	private String Reject_btn_xpath = "//div[@class='floating-action-group']//button[contains(text(), 'Approve')]";
	private String Assign_btn_xpath = "//div[@class='floating-action-group']//button[contains(text(), 'Assign')]";
	private String Delete_btn_xpath = "//div[@class='floating-action-group']//button[contains(text(), 'Delete')]";
	private String Confirm_DeleteAuth_btn_xpath = "//div[@id ='confirm-modal-component']//button[contains(text(),'OK')]";
	private String Cancel_DeleteAuth_btn_xpath = "//div[@id ='confirm-modal-component']//button[contains(text(),'Cancel')]";
	private String Coordinator_txt_xpath = "//div[@id='list-coordinators']//input[@id='input-list-coordinators']";
	private String Coordinator_vlue_xpath = "//div[@id='list-coordinators']//div[@class='content']";
	private String Coordinator_ddl_xpath = "//div[@id='list-coordinators']//ul";
	private String Assistant_vlue_xpath = "//div[@id='list-coordinators-assistants']//div[@class='content']";
	private String Assistant_txt_xpath = "//div[@id = 'list-coordinator-assistants']//input[@id='input-list-coordinator-assistants']";
	private String Assistant_ddl_xpath = "//div[@id = 'list-coordinator-assistants']//ul";
	private String TeamLeader_vlue_xpath = "//div[@id='list-team-leaders']//div[@class='content']";
	private String TeamLeader_txt_xpath = "//div[@id = 'list-team-leaders']//input[@id='input-list-team-leaders']";
	private String TeamLeader_ddl_xpath = "//div[@id = 'list-team-leaders']//ul";
	private String Auth_Assign_btn_xpath = "//button[@id = 'add-row']";
	private String Create_TransFile_btn_xpath = "//button[contains(text(),'Create Transferee File')]";
	private String Confirm_RejectAuth_btn_xpath = "//div[@id ='reject-modal-component']//button[contains(text(),'OK')]";
	private String Cancel_RejectAuth_btn_xpath = "//div[@id ='reject-modal-component']//button[contains(text(),'Cancel')]";
	
	
	public  WebElement Edit_button(){
		return GetElement(Edit_button_xpath);
}
	public  WebElement Update_btn(){
		return GetElement(Update_btn_xpath);
}
	public  WebElement Cancel_btn(){
		return GetElement(Cancel_btn_xpath);
}
	public  WebElement Approve_btn(){
		return GetElement(Approve_btn_xpath);
}
	public  WebElement Reject_btn(){
		return GetElement(Reject_btn_xpath);
}
	public  WebElement Assign_btn(){
		return GetElement(Assign_btn_xpath);
}
	public  WebElement Delete_btn(){
		return GetElement(Delete_btn_xpath);
}
	public  WebElement Confirm_DeleteAuth_btn(){
		return GetElement(Confirm_DeleteAuth_btn_xpath);
}
	public  WebElement Cancel_DeleteAuth_btn(){
		return GetElement(Cancel_DeleteAuth_btn_xpath);
}
	public  WebElement Coordinator_vlue(){
		return GetElement(Coordinator_vlue_xpath);
}
	public  WebElement Coordinator_txt(){
		return GetElement(Coordinator_txt_xpath);
}
	public  WebElement Coordinator_ddl(){
		return GetElement(Coordinator_ddl_xpath);
}
	public  WebElement Assistant_vlue(){
		return GetElement(Assistant_vlue_xpath);
}
	public  WebElement Assistant_txt(){
		return GetElement(Assistant_txt_xpath);
}
	public  WebElement Assistant_ddl(){
		return GetElement(Assistant_ddl_xpath);
}
	public  WebElement TeamLeader_vlue(){
		return GetElement(TeamLeader_vlue_xpath);
}
	public  WebElement TeamLeader_txt(){
		return GetElement(TeamLeader_txt_xpath);
}
	public  WebElement TeamLeader_ddl(){
		return GetElement(TeamLeader_ddl_xpath);
}
	public  WebElement Auth_Assign_btn(){
		return GetElement(Auth_Assign_btn_xpath);
}
	public  WebElement Create_TransFile_btn(){
		return GetElement(Create_TransFile_btn_xpath);
}
	public  WebElement Confirm_RejectAuth_btn(){
		return GetElement(Confirm_RejectAuth_btn_xpath);
}
	public  WebElement Cancel_RejectAuth_btn(){
		return GetElement(Cancel_RejectAuth_btn_xpath);
}
	
	//=============================METHOD
	
	public void GotoAuthDetail(String authname) {
		try {
			
			ARCLoading ARCLoading = new ARCLoading(driverbase);
			
			new Authorization_page(driverbase).AuthName_lnk(authname).click();
			
			ARCLoading.Wait_Util_FinishedARClogoLoading();

			ARCLoading.Wait_Util_FinishedPageLoading();
			
			GoToLatestBrowserTab();
		}
		catch(Exception e) {
			TestConfigs.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT DETAILS PAGE:"+e.getMessage()+".\n";
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
	}
	
	public void EditAuth_func(Auth_Info new_authinfo)
	{
		//As the result of:
		//the Address can be update based on selecting ClientLocation, this method should be return Auth_info
		Optimize_ElementClick(Edit_button());
		
		SelectDropdownItem(auth_details.Salutation_slect(), new_authinfo.Salutation, "");
		
		ClearThenEnterValueToField(auth_details.FirstName_txt(), new_authinfo.firstname);
		
		ClearThenEnterValueToField(auth_details.LastName_txt(), new_authinfo.lastname);
		
		SelectDropdownItem(auth_details.Policy_slect(), new_authinfo.Policy, "");
		
		ClearThenEnterValueToField(auth_details.InitiationDate_txt(), new_authinfo.FileEnterDate);
		
		ClearThenEnterValueToField(auth_details.EffectiveDate_txt(), new_authinfo.effectDate);
		
		SelectDropdownItem(auth_details.HomeAddress_ClientLocation_slect(), new_authinfo.Client.AddrInfo.get(0).ClientLocation, "");
		
		//This is commentted due to the flow is not needed:
		//+ When select the ClientLocation, the under fields are auto-populated. 
		//--This can be stop at this unless we want to update the address info of current ClientLocation
		SelectDropdownItem(auth_details.PhoneNumber_Type_slect(), new_authinfo.PhoneList.get(0).Type, "");
		
		ClearThenEnterValueToField(auth_details.Number_txt(), new_authinfo.PhoneList.get(0).Numb);
		
		SelectDropdownItem(auth_details.Email_Type_slect(), new_authinfo.MailList.get(0).Type, "");
		
		ClearThenEnterValueToField(auth_details.Email_txt(), new_authinfo.MailList.get(0).EmailAddr);
		
		ClearThenEnterValueToField(auth_details.ClientNote_txt(), new_authinfo.ClientNote);
		
		//Update Address with select Client Location by default flow
		Update_Auth_HomeAddress(new_authinfo.OriResAddr_info);
		
		//Update Destination Address WITHOUT getting from Client Location
		Update_Auth_HomeAddress(new_authinfo.DesOffAddr_info);
				
		Optimize_ElementClick(Update_btn());
		
	}
	
	
	public void Update_Auth_HomeAddress(Address_Info home_addr)
	{
		//Need to confirm that after creating the transferee from the Auth, the Transferee Original Res address will be pulled from ClientLocation
		if(!home_addr.ClientLocation.equals(""))
		{
			SelectDropdownItem(auth_details.HomeAddress_ClientLocation_slect(), home_addr.ClientLocation, "");
			
		}
				
		else
		{
			//Original Address
			ClearThenEnterValueToField(auth_details.HomeAddress_Address1_slect(), home_addr.Addr1);
			
			ClearThenEnterValueToField(auth_details.HomeAddress_Address2_slect(), home_addr.Addr2);
			
			ClearThenEnterValueToField(auth_details.HomeAddress_City_slect(),home_addr.City);
			
			SelectDropdownItem(auth_details.HomeAddress_State_slect(), home_addr.State, "");
			
			ClearThenEnterValueToField(auth_details.HomeAddress_ZipCode_slect(), home_addr.Zip);
					
			SelectDropdownItem(auth_details.HomeAddress_Country_slect(), home_addr.Country, "");
		}
		
		
			
	}
	
	public void Update_Auth_DestinationAddress(Address_Info des_addr)
	{
		//Need to confirm that after creating the transferee from the Auth, the Transferee Des Office address will be pulled from ClientLocation
		
		if(!des_addr.ClientLocation.equals(""))
			SelectDropdownItem(auth_details.HomeAddress_ClientLocation_slect(), des_addr.ClientLocation, "");
		
		else
		{		
			//Destination Address 
			ClearThenEnterValueToField(auth_details.DesAddress_Address1_slect(), des_addr.Addr1);
			
			ClearThenEnterValueToField(auth_details.DesAddress_Address2_slect(), des_addr.Addr2);
			
			ClearThenEnterValueToField(auth_details.DesAddress_City_slect(), des_addr.City);
			
			SelectDropdownItem(auth_details.DesAddress_State_slect(), des_addr.State, "");
			
			ClearThenEnterValueToField(auth_details.DesAddress_ZipCode_slect(), des_addr.Zip);
			
			SelectDropdownItem(auth_details.DesAddress_Country_slect(), des_addr.Country, "");
		}
		
		
	}
	
	public void Assign_Auth_func(Auth_Info auth_info)
	{
		Optimize_ElementClick(Assign_btn());
		
		new ARCLoading(driverbase).Wait_Util_FinishedPageLoading();
		
		Optimize_ElementClick(Coordinator_vlue());
		
		Optimize_ElementSendkey(Coordinator_txt(), auth_info.Coordinator);
		
		SelectDropdownItem(Coordinator_ddl(), auth_info.Coordinator,"");
		
		Optimize_ElementSendkey(Assistant_txt(), auth_info.CoordinatorAssistant);
		
		SelectDropdownItem(Assistant_ddl(), auth_info.CoordinatorAssistant,"");
		
		Optimize_ElementSendkey(TeamLeader_txt(), auth_info.TeamLeader);
		
		SelectDropdownItem(TeamLeader_ddl(), auth_info.TeamLeader,"");
		
		Optimize_ElementClick(Auth_Assign_btn());
	}
	
	public void Delete_Auth_func(Auth_Info auth_info)
	{
		Optimize_ElementClick(Delete_btn());
		
		Optimize_ElementClick(Confirm_DeleteAuth_btn());
	}
	
	public void Approve_Auth_func(Auth_Info auth_info)
	{
		Optimize_ElementClick(Approve_btn());
	}
	
	public void Create_TransfFile_func(Auth_Info auth_info)
	{
		Optimize_ElementClick(Create_TransFile_btn());
	}
	
	public void Reject_Auth_func(Auth_Info auth_info)
	{
		Optimize_ElementClick(Reject_btn());
		
		Optimize_ElementClick(Confirm_RejectAuth_btn());
	}
	
	public void VerifyAuth_func(Auth_Info auth_info)
	{
		Optimize_ElementClick(new Authorization_page(driverbase).AuthName_lnk(auth_info.FullName));
		
		String field = "";
		
		field = "Salutation";
		
		VerifyFieldValueEqual_func(field, auth_details.Salutation_slect(), auth_info.Salutation, "");
		
		field = "First Name";
		
		VerifyFieldValueEqual_func(field, auth_details.FirstName_txt(), auth_info.firstname, "");
		
		field = "Last Name";
		
		VerifyFieldValueEqual_func(field, auth_details.LastName_txt(), auth_info.lastname, "");
		
		field = "Policy";
		
		VerifyFieldValueEqual_func(field, auth_details.Policy_slect(), auth_info.Policy, "");
		
		field = "Initiation Date";
		
		VerifyFieldValueEqual_func(field, auth_details.InitiationDate_txt(), auth_info.FileEnterDate, "");
		
		field = "Effective Date";
		
		VerifyFieldValueEqual_func(field, auth_details.EffectiveDate_txt(), auth_info.effectDate, "");
		
		field = "Home Address Client Location";
		
		VerifyFieldValueEqual_func(field, auth_details.HomeAddress_ClientLocation_slect(), auth_info.OriResAddr_info.ClientLocation, "");
		
		field = "Home Address Address 1";
		
		VerifyFieldValueEqual_func(field, auth_details.HomeAddress_Address1_slect(), auth_info.OriResAddr_info.Addr1, "");
		
		field = "Home Address Address 2";
		
		VerifyFieldValueEqual_func(field, auth_details.HomeAddress_Address2_slect(), auth_info.OriResAddr_info.Addr2, "");
		
		field = "Home Address City";
		
		VerifyFieldValueEqual_func(field, auth_details.HomeAddress_City_slect(), auth_info.OriResAddr_info.City, "");
		
		field = "Home Address State";
		
		VerifyFieldValueEqual_func(field, auth_details.HomeAddress_State_slect(), auth_info.OriResAddr_info.State, "");
		
		field = "Home Address ZipCode";
		
		VerifyFieldValueEqual_func(field, auth_details.HomeAddress_ZipCode_slect(), auth_info.OriResAddr_info.Zip, "");
		
		field = "Des Address Country";
		
		VerifyFieldValueEqual_func(field, auth_details.DesAddress_Country_slect(), auth_info.OriResAddr_info.Country, "");
		
		field = "Des Address Client Location";
		
		VerifyFieldValueEqual_func(field, auth_details.DesAddress_ClientLocation_slect(), auth_info.DesOffAddr_info.ClientLocation, "");
		
		field = "Des Address Address 1";
		
		VerifyFieldValueEqual_func(field, auth_details.DesAddress_Address1_slect(), auth_info.DesOffAddr_info.Addr1, "");
		
		field = "Des Address Address 2";
		
		VerifyFieldValueEqual_func(field, auth_details.DesAddress_Address2_slect(), auth_info.DesOffAddr_info.Addr2, "");
		
		field = "Des Address City";
		
		VerifyFieldValueEqual_func(field, auth_details.DesAddress_City_slect(), auth_info.DesOffAddr_info.City, "");
		
		field = "Des Address State";
		
		VerifyFieldValueEqual_func(field, auth_details.DesAddress_State_slect(), auth_info.DesOffAddr_info.State, "");
		
		field = "Des Address ZipCode";
		
		VerifyFieldValueEqual_func(field, auth_details.DesAddress_ZipCode_slect(), auth_info.DesOffAddr_info.Zip, "");
		
		field = "Des Address Country";
		
		VerifyFieldValueEqual_func(field, auth_details.DesAddress_Country_slect(), auth_info.DesOffAddr_info.Country, "");
		
		field = "Phone Number Type";
		
		VerifyFieldValueEqual_func(field, auth_details.PhoneNumber_Type_slect(), auth_info.PhoneList.get(0).Type, "");
		
		field = "Number";
		
		VerifyFieldValueEqual_func(field, auth_details.Number_txt(), auth_info.PhoneList.get(0).Numb, "");
		
		field = "Email Type";
		
		VerifyFieldValueEqual_func(field, auth_details.Email_Type_slect(), auth_info.MailList.get(0).Type, "");
		
		field = "Email";
		
		VerifyFieldValueEqual_func(field, auth_details.Email_txt(),auth_info.MailList.get(0).EmailAddr, "");
		
		field = "Client Note";
		
		VerifyFieldValueEqual_func(field, auth_details.ClientNote_txt(), auth_info.ClientNote, "");
		
	}
	

}
