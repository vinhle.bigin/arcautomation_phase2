package configuration.DriverConfig;


import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ThreadGuard;

import configuration.TestConfigs;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Web_Driver extends DriverBase{
	
	
	
	public Web_Driver(DesiredCapabilities capacities) {
		Capacities = capacities;
		
	}//init
	
	
	@Override
	public void CreateDriver() throws Exception{
		System.out.println("All capacibilities:"+Capacities.toString());
		
		System.out.println("===========Config Info:"
							+ "\nIsRemote: "+TestConfigs.isRemote
							+ "\nRemoteMode: "+TestConfigs.RemoteMode
							+ "\nRemoteURL: "+TestConfigs.RemoteUrl
							+ "\nEnvName: "+TestConfigs.EnvName);
		
		
		
			//If run on local machine
			if (TestConfigs.isRemote==false)
			{	System.out.println("START TO CREATE LOCAL WEB DRIVER");
				
				String browserName = "";
			
				//==============Set driver location as System.Property
	
				switch(browserName)
				{
				case "FireFox":
					WebDriverManager.firefoxdriver().setup();
					
					FirefoxOptions option_ff = new FirefoxOptions();
				
					option_ff.merge(Capacities);
					
					option_ff.setHeadless(TestConfigs.glb_silentMode);
					
					driver = ThreadGuard.protect(new FirefoxDriver(option_ff));
					
					break;
					
				case "MsEdge":
					WebDriverManager.edgedriver().setup();
					EdgeOptions edge_options = new EdgeOptions();
					edge_options.merge(Capacities);
					driver = ThreadGuard.protect(new EdgeDriver(edge_options));
					break;
					
				default:
					WebDriverManager.chromedriver().setup();
										
					ChromeOptions chrome_options = new ChromeOptions();
										
					chrome_options.setHeadless(TestConfigs.glb_silentMode);
					
					chrome_options.merge(Capacities);
					
					driver = ThreadGuard.protect(new ChromeDriver(chrome_options));
					
					break;
					
				}
				
				if(TestConfigs.glb_silentMode!=true)
					driver.manage().window().maximize();
							
						
			}//end if of Local Run
			//-------Setup for Remote on Cloud or SEleniuGrid
			else
				
				if(TestConfigs.RemoteMode.equals("Cloud"))
				{
					String remote_access_str = "https://"+TestConfigs.RemoteUserName+":"+TestConfigs.RemoteAccessKey+"@"+TestConfigs.RemoteUrl;
					
					try {
						driver =  new RemoteWebDriver(new URL(remote_access_str), Capacities);
						System.out.println("SUCCESSFULL Remote CONNECTED");
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
										
						
				}//else if #1
				
				else if(TestConfigs.RemoteMode.contains("SeleniumGrid"))
				{
					System.out.println("START TO CREATE SELENIUM WEB DRIVER");
					try {
						driver =  new RemoteWebDriver(new URL(TestConfigs.RemoteUrl), Capacities);
						System.out.println("SUCCESSFULL Remote CONNECTED");
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}//else if #2
			
			System.out.println("=======SUCCESS CREATED WEB DRIVER");
				
	}
	
	
	
	

	

	
	
}
