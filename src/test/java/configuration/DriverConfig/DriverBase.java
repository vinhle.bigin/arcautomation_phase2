package configuration.DriverConfig;




import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.DesiredCapabilities;

import configuration.TestConfigs;


public abstract class DriverBase {
	
	public WebDriver driver;
	
	DesiredCapabilities Capacities;
	
	abstract public void CreateDriver () throws Exception;
	
	public WebDriver GetDriver()
	{
		return this.driver;
	}
	
	
	public void CloseDriver() {
		
		if(TestConfigs.glb_CloseBrowser==true)
		{
			this.driver.quit();
		}
	}
	
	public void printBrowserLog()
	{
		LogEntries entry = driver.manage().logs().get(LogType.BROWSER);
        // Retrieving all log 
        List<LogEntry> logs= entry.getAll();
        // Print one by one
        for(LogEntry e: logs)
        {
        	System.out.println(e);
        }
        
        // Printing details separately 
        for(LogEntry e: logs)
        {
        	System.out.println("Message is: " +e.getMessage());
        	System.out.println("Level is: " +e.getLevel());
        }
	}


	
	
	
}
