package businessObjects;

import java.util.Random;

import custom_Func.DateTime_Manage;
import dataDriven.glb_RefData;

public class Note_Info {
	public String Service;

	public String Title;

	public String Content;

	public String Contact;

	public String Assignee;
	
	public String AssignType; //"Transferee" "Client" "Vendor"
	
	public String NoteDate;
	
	public String CreatedBy;

	public Note_Info() {
		// TODO Auto-generated constructor stub
		Service = "General";

		Random r = new Random();
		int ret2 = r.nextInt(100)+1;
		Title = "Note-Title for Testing"+DateTime_Manage.getCurrentLocalTime()+ret2;

		Content = "Note-Content for Testing"+DateTime_Manage.getCurrentLocalTime();
		Contact = "";
		Assignee="";
		
		AssignType="Transferee";//"Client" | "Vendor"
		
		
		NoteDate = DateTime_Manage.FormatDateTime_WithTimeZone(glb_RefData.glb_ARCUser_TimeZone, DateTime_Manage.GetCurrentLocalDate_Date(), "MM/dd/yyyy");
				
		
		
		NoteDate = DateTime_Manage.GetCurrentLocalDate_Str();
		CreatedBy = "";
	}
	
	public Note_Info(String assigntype, Contact_Info ctact) {
		
		//AssignType="Client";
		
		
		if (assigntype.equals("Client")) {
			Contact = ctact.FullName;
			Assignee = ctact.ClientName;
			AssignType="Client";
		}
		else if (assigntype.equals("Vendor")) {
			Contact = ctact.FullName;
			Assignee = ctact.VendorName;
			AssignType="Vendor";
		}
		
		Service = "General";

		//Random r = new Random();
		//int ret2 = r.nextInt(100)+1;
		Title = "Note-Title for Testing"+DateTime_Manage.getCurrentLocalTime();

		Content = "Note-Content for Testing"+DateTime_Manage.getCurrentLocalTime();
		
		
	}
	
	public void ReGenerateNoteInfo() {

		Title = "Update Note-Title for Testing"+DateTime_Manage.getCurrentLocalTime();
		Content = "Update Note-Content for Testing"+DateTime_Manage.getCurrentLocalTime();
	}
	
	public void Note_For_Printing(String template_name) {

		Title = "Address label printed " + template_name + " Label";
		Content = "Address label printed " + template_name + " Label";
	}
	
	

}
