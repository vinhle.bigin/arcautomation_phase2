package businessObjects;

import java.util.Date;
import java.util.List;

import custom_Func.DateTime_Manage;
import dataDriven.glb_RefData;
import Enums.Approval;

public class ReimBurs_Info {
	
	public String Name;
	public UserAccount_Info Assigned_Analyst;
	public String Descr;
	public Date Acknowdged;
	public Approval ReimApproval;
	public Transferee_Info transf;
	
	public List<SubExpense_Info> SubExpense;
	
	public ReimBurs_Info() {
		
		DateTime_Manage dt = new DateTime_Manage();
		
		Name = "ExpenseReim_" + dt.getCurrentLocalTime();
		
		Assigned_Analyst = glb_RefData.User;
		
		Descr = "This is testing !";
		
		Acknowdged = dt.GetLocalDatetime();
		
		//transf.FullName = this.transf.FullName;
		
	}
	

}
