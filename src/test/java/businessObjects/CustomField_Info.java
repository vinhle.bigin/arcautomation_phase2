package businessObjects;

import java.util.ArrayList;
import java.util.List;

import custom_Func.DateTime_Manage;

public class CustomField_Info {
	public String FieldType;

	public String FieldName;

	public String ControlType;

	public Boolean IsActive;

	public Boolean IsRequire;

	public List<String> OptionList;

	public List<String> ValueList;

	public String FieldID;

	public CustomGroup_Info custom_group;
	
	public Boolean IsIncludeSmartTask;
	
	public Boolean IsSpecificClients;
	
	public Client_Info client;
	
	public String Apply_ClientType;
	
	public CustomField_Info()
	{
		
	}
	
	public CustomField_Info(String control_type,CustomGroup_Info group) {
		//NOTE: fieldtype_str value = "Textbox"
		DateTime_Manage dt = new DateTime_Manage();
		
		FieldName = "Field_"+dt.getCurrentLocalTime();

		FieldType = "Transferee";
		ControlType = control_type ;// Checkbox, Date Picker, Dropdown, Multiselect, Numeric Field, Radio Button, TextArea, Textbox
		IsActive = true;
		IsRequire = false;
		OptionList = new ArrayList<>();
		
		String pre_fix = "";
		
		FieldID = "";
		custom_group = group;
		
		IsIncludeSmartTask = false;
		
		Apply_ClientType = "Specific Clients";
		
		IsSpecificClients = false;
		
		client = new Client_Info();
		
		switch(ControlType)
		{
			case "Dropdown":
				pre_fix = "ddl_";

				break;

			case "Multiselect":
				pre_fix = "mddl_";
				break;

			case "Radio Button":
				pre_fix = "radi_";
				break;
				
			}

		dt = new DateTime_Manage();

		String time_str = dt.getCurrentLocalTime();

		for(int i =0;i<3;i++)
		{
			String tmp_str = pre_fix+time_str+"_"+i;
			OptionList.add(tmp_str);

		}//end for

	}//end void


	//ASSIGN GROUP TO CUSTOM FIELD
	public void AssignGroup_func(CustomGroup_Info group)
	{
		FieldType = group.FieldType;

		custom_group = group;
	}
	
	//These method is used on Test Case Class for initiate the value before action filling it the field:
	public void Init_TextField_Value_func(String input_str)
	{
		ValueList = new ArrayList<>();
		ValueList.add(input_str);
	}
	
		
	public void Init_CheckBox_Selected_Value_func(boolean status)
	{
		ValueList = new ArrayList<>();
		ValueList.add(Boolean.toString(status));
	}
	
	
	public void Init_Radio_Selected_Value_func(String option_name)
	{
		ValueList = new ArrayList<>();
		ValueList.add(option_name);
	}
	
	
	public void Init_Multiple_Selected_Value_func(List<String> options_name)
	{
		ValueList = new ArrayList<>();
		ValueList = options_name;
		
	}
	
	public void Init_Dropdown_Selected_Value_func(String option_name)
	{
		ValueList = new ArrayList<>();
		ValueList.add(option_name);
		
	}
	
}
