package businessObjects;

import custom_Func.DateTime_Manage;

public class AddressTemplate_Info extends Template_Info{

	public AddressTemplate_Info() {
		// TODO Auto-generated constructor stub
		super();
		Type = "Address Label";
		DateTime_Manage dt = new DateTime_Manage();
		Name = Type+ "-Template_" + dt.getCurrentLocalTime();
		Subject = "QA_Testing"+ Name;
	}

}
