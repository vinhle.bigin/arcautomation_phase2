package businessObjects;

import java.util.Date;

import custom_Func.DateTime_Manage;
import dataDriven.glb_RefData;

public class Other_Info {
	//MOVING INFO
		public String PropertyType;
		public String MoveReason;
		public String MoveType;
		public String FileType;
		public String SaleType;
		public String ProgramType;

		//Dates
		public String InitDate;
		public String EffectDate;
		public String InterMoveInDate;
		public String InterMoveOutDate;
		public String FullConvers;
		public String ExpectedMoveByDate;
		public String MoveEnd;
		public String JobStart;

		//EMPLOYEE INFO
		public String EmployeeNumb;
		public String VendorNumb;
		public String FileNumb;
		public String GeneralLedger;

		//Currency and Countries
		public String PrefCurrency;
		public String HomeCurrency;
		public String HostCurrency;
		public String PriCountry;
		public String SecCountry;
		public String HomeCountry;
		public String HostCountry;
		public String OldCountry;
		public String NewCountry;

		//Transferee Other info
		public String SSN;
		public String BirthDate;
		public String PoBox;
		public String Nation;

	public Other_Info() {
		// TODO Auto-generated constructor stub
		PropertyType = "Rental";
		MoveReason = "Group";
		MoveType = "Mandated";
		FileType = "International";
		SaleType = "Inventory";
		ProgramType = "Lump Sum";

		Date currentDate = DateTime_Manage.GetCurrentLocalDate_Date();

		InitDate = DateTime_Manage.FormatDateTime_WithTimeZone(glb_RefData.glb_ARCUser_TimeZone,currentDate,"MM/dd/yyyy");
		

		Date dt_tmp = DateTime_Manage.IncreaseDay(currentDate, 1);

		EffectDate = DateTime_Manage.ConvertDatetoString(dt_tmp);

		dt_tmp = DateTime_Manage.IncreaseDay(currentDate, 2);

		InterMoveInDate = DateTime_Manage.ConvertDatetoString(dt_tmp);

		dt_tmp = DateTime_Manage.IncreaseDay(currentDate, 3);
		InterMoveOutDate = DateTime_Manage.ConvertDatetoString(dt_tmp);

		//dt_tmp = DateTime_Manage.IncreaseDay(currentDate, 3)
		FullConvers = DateTime_Manage.ConvertDatetoString(dt_tmp);

		dt_tmp = DateTime_Manage.IncreaseDay(currentDate, 4);
		ExpectedMoveByDate = DateTime_Manage.ConvertDatetoString(dt_tmp);

		dt_tmp = DateTime_Manage.IncreaseDay(currentDate, 5);
		MoveEnd = DateTime_Manage.ConvertDatetoString(dt_tmp);

		dt_tmp = DateTime_Manage.IncreaseDay(currentDate, 6);
		JobStart = DateTime_Manage.ConvertDatetoString(dt_tmp);

		//EMPLOYEE INFO
		String t_tmp = DateTime_Manage.getCurrentLocalTime();
		EmployeeNumb = "EM_"+ t_tmp;
		VendorNumb = "VN_" + t_tmp;
		FileNumb = "FN_" + t_tmp;
		GeneralLedger = "GL For testing.";

		//Currency and Countries
		PrefCurrency = "USD - United States of America";
		HomeCurrency = "BRL - Brazil";
		HostCurrency = "ALL - Albania";
		PriCountry = "Algeria";
		SecCountry = "Viet Nam";
		HomeCountry = "Aruba";
		HostCountry = "Bolivia";
		OldCountry = "Belize";
		NewCountry = "Sweden";

		//Transferee Other info
		SSN = t_tmp;
		BirthDate = "07/15/1987";
		PoBox = "PO_001";
		Nation = "United States";
	}
		
	

}
