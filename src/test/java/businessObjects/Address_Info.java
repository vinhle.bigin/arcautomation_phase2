package businessObjects;

import custom_Func.DateTime_Manage;

public class Address_Info {
	public String ClientLocation;
	public String Addr1;
	public String Addr2;
	public String City;
	public String State;
	public String Zip;
	public String Country;
	public Boolean IsAtri_Billing;
	public Boolean IsAtri_Shipping;
	public Boolean IsAtri_Mailing;
	public Boolean IsAtri_Primary;
	public Boolean IsSendMailPayment;
	public Boolean IsSendMailTaxDoc;
	public Boolean IsSendMailServices;
	public String Description;
	public String Code;

	public Address_Info() {
		// TODO Auto-generated constructor stub
		DateTime_Manage dt = new DateTime_Manage();
		Code = "AddrCode_"+ dt.getCurrentLocalTime();
		Description = "Descr for testing";
		ClientLocation = "";
		Addr1 = "20 Wall Street";
		Addr2 = "50 High Road";
		City = "New York";
		State  = "Alabama";
		Country  = "United States of America";
		Zip = "12345";
		IsAtri_Billing = false;
		IsAtri_Shipping = false;
		IsAtri_Mailing = false;
		IsAtri_Primary = false;
		IsSendMailPayment = false;
		IsSendMailTaxDoc = false;
		IsSendMailServices = false;
	}
	
	




}
