package businessObjects;

import custom_Func.DateTime_Manage;

public class SmartTask_Info {
	public String Name;
	public String Service;
	public Boolean IsFieldchange_opt;
	public Boolean IsEmailProcess_opt;
	public Boolean IsActivityComplete_opt;
	public Boolean IsFileCreate_opt;
	public String TriggerType;
	public String Condt_FieldName;
	public String Condt_Operator;
	public String Condt_Value;
	public SmartTask_Action action;
	public String Condt_EMailTemplName;
	public String Condt_ActivityName;
	public String Apply_ClientType;
	public String Apply_SpecificClientName;

	public SmartTask_Info() {
		// TODO Auto-generated constructor stub
		Name = "SmartTask_"+DateTime_Manage.getCurrentLocalTime();
		Service = "General Service";
		IsFieldchange_opt = false;
		IsEmailProcess_opt = false;
		IsActivityComplete_opt = false;
		IsFileCreate_opt = false;
		TriggerType = "" ;//Meet ANY of the following conditions
		Condt_FieldName = "";
		Condt_Operator = "";
		action = new SmartTask_Action();
		Condt_EMailTemplName = "";
		Condt_ActivityName = "";
		Apply_ClientType = "Specific Clients";
		Apply_SpecificClientName = "";
	}
	
	public void SetFieldChangeCondt_func(String triggertype,String fieldname ,String operator ,String value_str) {
		IsFieldchange_opt = true;
		if(triggertype.equals(""))
			TriggerType = "Meet ALL of the following conditions"; //Meet ANY of the following conditions
		else
			TriggerType = triggertype; //Meet ANY of the following conditions

		if(fieldname.equals(""))
			Condt_FieldName = "Transferee - First Name";
		else
			Condt_FieldName = fieldname;


		if(operator.equals(""))
			Condt_Operator = "Equal to";
		else
			Condt_Operator = operator;

		Condt_Value = value_str;


	}

	public void SetEmailProcessCondt_func(String templatename) {
		IsEmailProcess_opt = true;
		Condt_EMailTemplName = templatename;
	}

	public void SetActivityCompleteCondt_func(String activityname) {
		IsActivityComplete_opt = true;
		Condt_ActivityName = activityname;
	}

	public void SetFileCreateCondt_func() {
		IsFileCreate_opt = true;
	}



	
}



