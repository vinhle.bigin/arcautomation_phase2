package businessObjects;

import dataDriven.glb_RefData;

public class HHGService_Info extends Services_Info{
	
	public String OriAddr;
	public String DesAddr;
	public String HHGPaymentType;
	public String HHGMoveDate;
	public String HHGDeliverDate;
	public Boolean IsInvoceCreate;
	public Boolean IsInvoiceClose;
	public Boolean IsEscrowFeeReceive;
	
	public String User;
	public String Assistant;
	

	public HHGService_Info() {
		// TODO Auto-generated constructor stub
		super();
		Type = "Household goods";
		Status = true;
		OriAddr = "AL, US";
		DesAddr = "UT, US";
		HHGPaymentType = "Credit";
		HHGMoveDate = "04/04/2019";
		HHGDeliverDate = "04/04/2019";
		IsInvoceCreate = true;
		IsInvoiceClose = true;
		IsEscrowFeeReceive = true;
		User = glb_RefData.Coordinator;
		Assistant = "";
	}
	
	public HHGService_Info(Contact_Info vendor_contact)
	{
		super();
		Type = "Household goods";
		Status = true;
		OriAddr = "AL, US";
		DesAddr = "UT, US";
		HHGPaymentType = "Credit";
		HHGMoveDate = "04/04/2019";
		HHGDeliverDate = "04/04/2019";
		IsInvoceCreate = true;
		IsInvoiceClose = true;
		IsEscrowFeeReceive = true;
		User = glb_RefData.Coordinator;
		Assistant = "";
		VendorCtactList.add(vendor_contact);
	}

}
