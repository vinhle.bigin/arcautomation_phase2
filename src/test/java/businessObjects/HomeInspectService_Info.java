package businessObjects;

import dataDriven.glb_RefData;

public class HomeInspectService_Info extends Services_Info{
	
		public boolean IsClear;
		public String Comment;
		
		public String InspectType;
		public String AssignedAgent;
		public String OrderDate;
		public String InspectDate;
		public String CompleteDate;
		public boolean IsFollowNeed;
		public String CancelDate;
		
		public String User;
		public String Assistant;
		

	public HomeInspectService_Info() {
		super();
		Status = true;
		Type = "Home inspection";
		
		IsClear = true;
		Comment = "Comment for testing";
		InspectType = "Heater";
		AssignedAgent = "None";
		OrderDate = "11/06/2020";
		InspectDate = "11/06/2020";
		CompleteDate = "11/06/2020";
		IsFollowNeed = true;
		CancelDate = "11/06/2020";
		User = glb_RefData.Coordinator;
		Assistant = "";
		
		
		
		// TODO Auto-generated constructor stub
	}
	
	
	public HomeInspectService_Info(Contact_Info vendor_contact)
	{
		super();
		Type = "Home inspection";
		Status = true;
		IsClear = true;
		Comment = "Comment for testing";
		InspectType = "Heater";
		AssignedAgent = "None";
		OrderDate = "11/06/2020";
		InspectDate = "11/06/2020";
		CompleteDate = "11/06/2020";
		IsFollowNeed = true;
		CancelDate = "11/14/2020";
		User = glb_RefData.Coordinator;
		Assistant = "";
		VendorCtactList.add(vendor_contact);
	}
	
	public void ReInit_ServiceInfo()
	{
		IsClear = true;
		Comment = "Comment for testing";
		InspectType = "Mold";
		AssignedAgent = "None";
		OrderDate = "11/07/2020";
		InspectDate = "11/07/2020";
		CompleteDate = "11/07/2020";
		IsFollowNeed = false;
		CancelDate = "11/07/2020";
		User = glb_RefData.Coordinator;
		Assistant = "";
	//	VendorCtactList.add(vendor_contact);
	}

}
