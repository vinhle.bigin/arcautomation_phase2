package businessObjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import configuration.TestConfigs;
import custom_Func.Data_Optimize;
import custom_Func.DateTime_Manage;
import custom_Func.FileManage;
import dataDriven.glb_RefData;

public class Mail_Info {
	public String Type;
	public String EmailAddr;

	//To send email
	
	public String ServiceType;
	public String Order_Descr;
	public String RecipientType;
	public String TemplateName;
	public List<String> To_list;
	public List<String> CC_list;
	public List<String> BCC_list;
	public String From;
	public String Subject;
	public String Content;
	public List<String> Attach_List = new ArrayList<String>();

	//FOR RECEIVED MAIL
	public String activatelink;
	public String resetlink;
	public Date sentDate;
	public List<FileManage> download_files;

	public Mail_Info() {
		// TODO Auto-generated constructor stub
		Random r = new Random();
		int ret2 = r.nextInt(100)+1;
		int ret = r.nextInt(100)+1;
		String addr = "test_mail"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret+ret2);
		Type = "Work Email";
		EmailAddr = addr+"@yopmail.com";
		ServiceType = "";
		RecipientType = "";

		TemplateName = "";
		To_list = new ArrayList<>();
		To_list.add(glb_RefData.glb_TestMailAccount);

		CC_list = new ArrayList<>();
		BCC_list = new ArrayList<>();
		From = glb_RefData.glb_EmailSender;
		Subject = "";
		Content ="";
		Attach_List = new ArrayList<>();

		//FOR RECEIVED EMAIL
		sentDate = DateTime_Manage.GetLocalDatetime();
				
		
		download_files = new ArrayList<>();
		
		Order_Descr="";
	}
	
	
	//This method is used before providing info on New Email modal
		public void Map_Template_Info_func(Template_Info tpl,boolean Is_TemplateContent)
		{
			ServiceType = tpl.ServiceType;
			RecipientType = tpl.Target;
			TemplateName = tpl.Name;

			/*
			if(tpl.EmailFrom != "Coordinator"&&tpl.EmailFrom != "Assistant")
				From = tpl.EmailFrom;
			*/
			Subject = tpl.Subject;

			if(Is_TemplateContent==true)
				Content = tpl.Content;
			
			if(tpl.DocName!="")
				Attach_List.add(tpl.DocName);
		}


		public void RenewContentAppraisalInfo_func(AppraisalService_Info apraisal_service)
		{
			String Content_str = "";
			
			apraisal_service.Calculate_Appraisal_Analysis();
			
			int i = 0;
			for(Double value:apraisal_service.SelectEst_List.values())
			{
				
				Content_str+= "\nAppraisal Estimate "+i+": "+String.format("%.2f",value);
				Content_str = Content_str.trim();
			}
			
			Content_str+= Renew_Service_Selected_Vendor_Info_func(apraisal_service);
			Content_str = Content_str.trim();
			
			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void
		
		public void RenewContent_HomeInspectInfo_func(HomeInspectService_Info Inspect_service)
		{
			String Content_str = "";
			
			Content_str+= "\nHomeInspect Specialist Assistant Full Name: "+Inspect_service.Assistant;
			Content_str = Content_str.trim();
			
			Content_str+= Renew_Service_Selected_Vendor_Info_func(Inspect_service);
			Content_str = Content_str.trim();
			
			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void
		
		
		public void RenewContent_InventManageInfo_func(InventManageService_Info Invent_service)
		{
			String Content_str = "";
			
			Content_str+= "IM List Price: "+Invent_service.HS_ListSale_section.ListPrice;
			Content_str = Content_str.trim();
			Content_str+= "\nIM Start Listing Date: "+Invent_service.HS_ListSale_section.ListStartDate;
			Content_str = Content_str.trim();
			Content_str+= "\nIM End Listing Date: "+Invent_service.HS_ListSale_section.ListEndDate;
			Content_str = Content_str.trim();
			Content_str+= "\nIM Sale Price: "+Invent_service.HS_ListSale_section.SalePrice;
			Content_str+= "\nIM Closing Date: "+Invent_service.HS_ListSale_section.CloseDate;
			Content_str = Content_str.trim();
			Content_str+= "\nIM Listing Link: ";
			Content_str = Content_str.trim();
			Content_str+= "\nIM Average Days on Market: "+Invent_service.HSAnal_AverageDays;
			Content_str = Content_str.trim();
			Content_str+= "\nIM Property Price Range From: "+Invent_service.HSAnal_PriceRangeFrom;
			Content_str = Content_str.trim();
			Content_str+= "\nIM Property Price Range To: "+Invent_service.HSAnal_PriceRangeTo;
			Content_str = Content_str.trim();
			Content_str+= "\nIM Suggested Selling Price: "+Invent_service.HSAnal_SugesstSellPrice;
			Content_str = Content_str.trim();
			Content_str+= "\nIM Suggested List Price: "+Invent_service.HSAnal_SugesstListPrice;
			Content_str = Content_str.trim();
			Content_str+= Renew_Service_Selected_Vendor_Info_func(Invent_service);
			Content_str = Content_str.trim();
			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void
		
		public void RenewContent_HBOInfo_func(HBOService_Info hbo_service)
		{
			String Content_str = "";
			String offer_amount ="";
			String offer_date = "";
			String offer_expire_date = "";
			

			for(HBO_Offer_Info cur_offer:hbo_service.Offer_list)
			{
				switch(cur_offer.Type)
				{
				case "Guaranteed offer":
					offer_amount =  "HBO Guaranteed Offer Amount: ";
					offer_date =  "HBO Guaranteed Offer Date: ";
					offer_expire_date =  "HBO Guaranteed Offer Expiration Date: ";
				
					
					break;
				
				case "Amended from zero offer":
					offer_amount= "HBO Amended from zero Offer Amount: ";
					offer_date= "HBO Amended from zero Offer Date: ";
					offer_expire_date= "HBO Amended from zero Offer Expiration Date: ";
					
					break;
				
				case "BVO fall through offer":
					offer_amount= "HBO BVO Fall Through Offer Amount: ";
					offer_date= "HBO BVO Fall Through Offer Date: ";
					offer_expire_date= "HBO BVO Fall Through Offer Expiration Date: ";
					
					break;
				
				case "Direct offer":
					offer_amount= "HBO Directed Offer Amount: ";
					offer_date= "HBO Directed Offer Date: ";
					
					
				case "BVO offer":
					offer_amount= "HBO BVO Offer Amount: ";
					offer_date= "HBO BVO Offer Date: ";
					offer_expire_date= "HBO BVO Offer Expiration Date: ";
					
					break;
				
				case "Amended offer":
					offer_amount = "HBO Amended Offer Amount: ";
					offer_date = "HBO Amended Offer Date: ";
					offer_expire_date = "HBO Amended Offer Expiration Date: ";
					
					break;
				}//end switch
				
				Content_str+= "\n"+offer_amount+cur_offer.Amount;
				Content_str+= "\n"+offer_date+cur_offer.Date;
				
				if(!offer_expire_date.equals(""))
					Content_str+= "\n"+offer_expire_date+cur_offer.ExpireDate;
				
			Content_str+= "\nHBO Contract Date: "+cur_offer.ContractDate;
			Content_str+= "\nHBO Acceptance Date: "+cur_offer.AcceptDate;
			Content_str+= "\nHBO Vacate Period: "+cur_offer.VacatePeriod; 
			}
			
			Content_str+= "\nHBO Acquit Type: "+hbo_service.Transt_AcquisType;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Acquit Date: "+hbo_service.Transt_AcquisDate;
			Content_str = Content_str.trim();
		
			Content_str+= "\nHBO Resale Amount: "+hbo_service.Transt_ResaleAmount;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Resale Date: "+hbo_service.Transt_ResaleDate;
			Content_str = Content_str.trim();
			
			Content_str+= "\nHBO Special Instruct: "+hbo_service.Transt_SpecialInstruct;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Special Full Name: "+hbo_service.Transt_HBOSpecialList;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Special Assistant Full Name: "+hbo_service.Transt_Assistant;
			Content_str = Content_str.trim();
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Actic Equit Amount: "+hbo_service.Transt_AnticEquit_Amount;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Actic Equit Date: "+hbo_service.Transt_AnticEquit_Date;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Actic Equit Proceess Return Amount: "+hbo_service.Transt_AnticEquitReturn_Amount;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Actic Equit Proceess Return Date: "+hbo_service.Transt_AnticEquitReturn_Date;
			Content_str = Content_str.trim();	
			Content_str+= "\nHBO Actual Equit Amount Send: "+hbo_service.Transt_ActualEquit_Amount;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Actual Equit Date Send: "+hbo_service.Transt_ActualEquit_Date;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Actual Equit Proceed Return Amount: "+hbo_service.Transt_ActualEquitReturn_Amount;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Actual Equit Proceed Return Date: "+hbo_service.Transt_ActualEquitReturn_Date;
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Comment: ";
			Content_str = Content_str.trim();
			Content_str+= "\nHBO Actual Proceed Sent EE: ";
			Content_str = Content_str.trim();
			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void
		
		
		//====This method is used to assign value into Template object before verifying the Observation Tiny/Email Content
		//with Expected content that contains the Transfree General Info
		public void RenewContentTransGenInfo_func(Transferee_Info transf_tmp)
		{
			String Content_str = "";
			Transferee_Info transf = transf_tmp;
			
			

			//Content_str += "Trans ID: \n";
			Content_str += "Trans FName: "+transf.firstname;
			Content_str = Content_str.trim();
			Content_str += "\nTrans LName: "+transf.lastname;
			Content_str = Content_str.trim();
			Content_str += "\nTrans FullName: "+transf.FullName;
			Content_str = Content_str.trim();
			//Content_str += "\nTrans JobTitle: \\n"
			Content_str = Content_str.trim();
			Content_str += "\nTrans Coordinator: "+transf.Coordinator;
			Content_str = Content_str.trim();
			Content_str += "\nTrans Coor Assistant: "+transf.CoordinatorAssistant;
			Content_str = Content_str.trim();
			Content_str += "\nClient Name: "+transf.Client.CompanyName;
			Content_str = Content_str.trim();
			//Content_str += "Client Cotanct: \\n"
			Content_str += "\nTrans Status: "+transf.Status;
			Content_str = Content_str.trim();
			//Content_str += "Trans Status Change Date: \n"
			//Content_str += "Trans IsOveride: \n"
			//Content_str += "Trans IsQualified: \n"
			//Content_str += "Trans FileEnter CreateDate: \n"
			Content_str += "\nTrans IsReceiveSignABAD: "+transf.IsReceivedSignedABAD;
			Content_str = Content_str.trim();
			Content_str += "\nTrans IsReferListSide: "+transf.ReferedListSide;
			Content_str = Content_str.trim();
			Content_str += "\nTrans IsReferBuySide: "+transf.ReferedBuySide;
			Content_str = Content_str.trim();
			Content_str += "\nTrans IsReferToTitleComp: "+transf.ReferedToTitleComp;
			Content_str = Content_str.trim();
			Content_str += "\nTrans Mobile PhoneNumb: "+transf.PhoneList.get(0).Numb;
			Content_str = Content_str.trim();
			Content_str += "\nTrans WorkEmail: "+this.EmailAddr;
			Content_str = Content_str.trim();
			
			this.Content = Content_str;
		}

		////====This method is used to assign value into Template object befor verifying the Observation Tiny/Email Content
		//with Expected content that contains the Transfree Other Info
		public void RenewContentTransOtherInfo_func(Transferee_Info transf_tmp)
		{
			String Content_str = "";
			Transferee_Info transf = transf_tmp;

			//Content_str += "Trans ID: \n"
			Content_str += "MoveType: "+transf.Other_info.MoveType;
			Content_str += "\nInItDate: "+transf.Other_info.InitDate;
			Content_str += "\nEffectDate: "+transf.Other_info.EffectDate;
			//Content_str += "\nTrans JobTitle: \\n"
			Content_str += "\nMoveInDate: "+transf.Other_info.InterMoveInDate;
			Content_str += "\nMoveOutDate: "+transf.Other_info.InterMoveOutDate;
			Content_str += "\nExpectMoveDate: "+transf.Other_info.ExpectedMoveByDate;
			//Content_str += "Client Cotanct: \\n"
			Content_str += "\nMoveEnd: "+transf.Other_info.MoveEnd+"\n";


			this.Content = Content_str;
		}


		public void RenewContentTransAddrInfo_func(Transferee_Info transf_tmp)
		{
			String Content_str = "";
			Transferee_Info transf = transf_tmp;

			Content_str+= "OrgRes_Addr1: "+transf.OriResAddr_info.Addr1;

			Content_str+= "\nOrgRes_Addr2: "+transf.OriResAddr_info.Addr2;

			Content_str+= "\nOrgRes_City: "+transf.OriResAddr_info.City;

			Content_str+= "\nOrgRes_State: "+transf.OriResAddr_info.State;

			Content_str+= "\nOrgRes_Zip: "+transf.OriResAddr_info.Zip;

			Content_str+= "\nOrgRes_Country: "+transf.OriResAddr_info.Country;

			//Orginal Office Info
			Content_str+= "\nOrgOff_Addr1: "+transf.OriOffAddr_info.Addr1;

			Content_str+= "\nOrgOff_Addr2: "+transf.OriOffAddr_info.Addr2;

			Content_str+= "\nOrgOff_City: "+transf.OriOffAddr_info.City;

			Content_str+= "\nOrgOff_State: "+transf.OriOffAddr_info.State;

			Content_str+= "\nOrgOff_Zip: "+transf.OriOffAddr_info.Zip;

			Content_str+= "\nOrgOff_Country: "+transf.OriOffAddr_info.Country;

			//Destination Residient Info
			Content_str+= "\nDesRes_Addr1: "+transf.DesResAddr_info.Addr1;

			Content_str+= "\nDesRes_Addr2: "+transf.DesResAddr_info.Addr2;

			Content_str+= "\nDesRes_City: "+transf.DesResAddr_info.City;

			Content_str+= "\nDesRes_State: "+transf.DesResAddr_info.State;

			Content_str+= "\nDesRes_Zip: "+transf.DesResAddr_info.Zip;

			Content_str+= "\nDesRes_Country: "+transf.DesResAddr_info.Country;

			//Destionation Office Info
			Content_str+= "\nDesOff_Addr1: "+transf.DesOffAddr_info.Addr1;

			Content_str+= "\nDesOff_Addr2: "+transf.DesOffAddr_info.Addr2;

			Content_str+= "\nDesOff_City: "+transf.DesOffAddr_info.City;

			Content_str+= "\nDesOff_State: "+transf.DesOffAddr_info.State;

			Content_str+= "\nDesOff_Zip: "+transf.DesOffAddr_info.Zip;

			Content_str+= "\nDesOff_Country: "+transf.DesOffAddr_info.Country;


			this.Content = Content_str;
		}// end void


		public void RenewContentTransHomeSaleInfo_func(HSService_Info service)
		{
			HSService_Info service_tmp = service;
			String Content_str = "";
			Content_str+= "HS_TookBuyOut: "+ service_tmp.IsTookBuyOut;
			Content_str+= "\nHS_ListPrice: " + service_tmp.ListPrice;
			Content_str+= "\nHS_CommissionPercent: "+ service_tmp.CommissionPercent;
			Content_str+= "\nHS_ListStartDate: " + service_tmp.ListStartDate;
			Content_str+= "\nHS_ListEndDate: " + service_tmp.ListEndDate;
			Content_str+= "\nHS_ClosingDate: " + service_tmp.CloseDate;
			Content_str+= "\nHS_RebateAmount: " + service_tmp.RebateAmount;
			Content_str+= "\nHS_DateBuyOutAccept: " + service_tmp.BuyOutAcceptDate;
			
			//=========Escrow Fee, Invoice Close is not available for HS service any more
			//Content_str+= "\nHS_EscrowFeePercent: "+ service_tmp.EscrowFeePercent;
		//	Content_str+= "\nHS_EscowFeeAmount: " + service_tmp.EscrowFeeAmount;
			Content_str+= "\nHS_Rebate: " + service_tmp.IsRebaseSent;
			

		//	Content_str+= "\nHS_IsInvoiceCreate: " + service_tmp.IsInvoiceCreate;
		//	Content_str+= "\nHS_InvoiceClosedOut: " + service_tmp.IsInvoiceClose;
			Content_str+= "\nHS_ReffeeReceived: " + service_tmp.IsRefFeeReceive;
			Content_str+= "\nHS_SignedRefFeeAgree: " + service_tmp.IsSignRefFeeAgree;
			
			Content_str+=Renew_Service_Selected_Vendor_Info_func(service);
	

			this.Content = Content_str;
		}//end void
		
		
	

		public void RenewContentTransHomePurchaseInfo_func(HPService_Info service_tmp)
		{
			String Content_str = "";
			Content_str+= "\nHP_IsSignRefFeeAgree: "+ service_tmp.IsSignRefFeeAgree;
			Content_str+= "\nHP_SalePrice: "+ service_tmp.SalePrice;
			Content_str+= "\nHP_LoanAmount: "+ service_tmp.LoanAmount;
			Content_str+= "\nHP_Commission: "+ service_tmp.CommissionPercent;
			Content_str+= "\nHP_RebateAmount: "+ service_tmp.RebateAmount;
			Content_str+= "\nHP_CloseDate: "+ service_tmp.CloseDate;
			Content_str+= "\nHP_HuntTripDate: "+ service_tmp.HouseHuntTripDate;
			Content_str+= "\nHP_EscrowFeeAmount: "+ service_tmp.EscrowFeeAmount;
			Content_str+= "\nHP_EscrowfePercent: "+ service_tmp.EscrowFeePercent;
			Content_str+= "\nHP_IsRebateSent: ";
			Content_str+= "\nHP_IsInvoiceCloseOut: " + service_tmp.IsInvoiceClose;
			Content_str+= "\nHP_IsEscrowFeeReceive: "+ service_tmp.IsEscrowFeeReceive;
			Content_str+= "\nHP_IsInvoiceCreate: "+ service_tmp.IsInvoiceCreate;
			Content_str+= "\nHP_IsTransfereeRent: "+ service_tmp.IsTransfRent;
			
			Content_str+=Renew_Service_Selected_Vendor_Info_func(service_tmp);
			//Content_str+=Renew_Service_Selected_Vendor_Info_func(service_tmp.VendorCtactList.get(0));
			


			this.Content = Content_str;
		}//end void

		public void RenewContent_HHG_Info_func(Service_Order_Info hhg_order_info)
		{

			//Calculate for Bill Invoice Amounts before parse them into the content to verify
			hhg_order_info.Calculate_BillInvoice();
			
			String Content_str = "";
			Content_str+= "HHG_Order_Desr: "+hhg_order_info.Description;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Tariff Code: "+hhg_order_info.TariffCode;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Service Type: "+hhg_order_info.ServiceType;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Pack Date Actual End: "+hhg_order_info.ServiceDate_PackDate_ActEnd;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Load Date Actual End: ";
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Deliver Date Actual End: "+hhg_order_info.ServiceDate_DeliDate_ActEnd;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Actual Storage Weight: ";
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Actual Ship Weight: "+ Data_Optimize.Convert_Double_To_String_func(hhg_order_info.Ship_WeightAct,"%.0f") ;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Vendor Name: "+hhg_order_info.VendorName;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Total Service Charge: "+ Data_Optimize.Format_Double_WithCurrency(hhg_order_info.TotalServiceCharge) ;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Override Amount: "+Data_Optimize.Format_Double_WithCurrency(hhg_order_info.Override_Amount);
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Total Invoice Client: "+Data_Optimize.Format_Double_WithCurrency(hhg_order_info.TotalInvoiceClient_Amount);
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Total Bill Mover: "+Data_Optimize.Format_Double_WithCurrency(hhg_order_info.TotalBillMover_Amount);
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Date Bill Client: "+hhg_order_info.DateBilledCient;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Payment Receive Date: "+hhg_order_info.DateReceviedPayment;
			Content_str = Content_str.trim();
			Content_str+= "\nHHG Order Invoice Submit Date: "+hhg_order_info.DateInvoiceSubmitted;
			Content_str = Content_str.trim();
			
			this.Content = Content_str;
		}//end void

		public void RenewContentMortgageInfo_func(MGService_Info service_tmp)
		{
			String Content_str = "";
			Content_str+= "MG_mortgage_amount: " + service_tmp.MortgageAmount;
			Content_str = Content_str.trim();
			Content_str+= "\nMG_index_rate: " + service_tmp.IndexRate;
			Content_str = Content_str.trim();
			Content_str+= "\nMG_amt_needed_at_closing: ";
			Content_str = Content_str.trim();
			Content_str+= "\nMG_down_payment: ";
			Content_str = Content_str.trim();
			Content_str+= "\nMG_closing_date: ";
			Content_str = Content_str.trim();
			
			Content_str+=Renew_Service_Selected_Vendor_Info_func(service_tmp);
			
			this.Content = Content_str;
		}//end void

		public void RenewContentTQInfo_func(TQService_Info service_tmp)
		{
			String Content_str = "";
			Content_str+= "TQ_move_out_date: "+ service_tmp.MoveOutDate;
			Content_str+= "\nTQ_move_in_date: " + service_tmp.MoveInDate;
			Content_str+= "\nTQ_pet: "  + service_tmp.Pets;
			Content_str+= "\nTQ_apartment_type: "  + service_tmp.ApartmentType;
			Content_str+= "\nTQ_baths: "  + service_tmp.Baths;
			Content_str+= "\nTQ_budget: "  + service_tmp.Budget;
			Content_str+= "\nTQ_payment_type: "   + service_tmp.PaymentType;
			Content_str+= "\nTQ_escrow_fee_received: " + service_tmp.IsEscrowFeeReceive;
			Content_str+= "\nTQ_invoice_created: " + service_tmp.IsInvoiceCreate;
			Content_str+= "\nTQ_invoice_closed_out: "  + service_tmp.IsInvoiceClose;
			
			Content_str+=Renew_Service_Selected_Vendor_Info_func(service_tmp);


			this.Content = Content_str;
		}//end void



		//RENEW THE CONTENT WITH CLIENTINFO
		public void RenewContent_ClientInfo_func(Client_Info client_info)
		{
			String Content_str = "";
			Content_str+= "company_name: " +client_info.CompanyName;
			//Content_str+= "\ncontact_salutation: "+client_info.
			Content_str+= "\ncontact_first_name: " +client_info.Contacts.get(0).firstname;
			//	Content_str+= "\ncontact_mid_name: " +client_info.;
			Content_str+= "\ncontact_last_name: " +client_info.Contacts.get(0).lastname;
			Content_str+= "\ncontact_full_name: " +client_info.Contacts.get(0).FullName;
			Content_str+= "\ncontact_phone_number: " +client_info.Contacts.get(0).PhoneList.get(0).Numb;
			Content_str+= "\ncontact_email: " +client_info.Contacts.get(0).email_info.EmailAddr;
			Content_str+= "\ncontact_address1: "+client_info.Contacts.get(0).AddrInfo.Addr1;
			//	Content_str+= "\ncontact_address2: "
			Content_str+= "\ncontact_city: "+client_info.Contacts.get(0).AddrInfo.City;
			Content_str+= "\ncontact_state: "+client_info.Contacts.get(0).AddrInfo.State;
			Content_str+= "\ncontact_zip: " +client_info.Contacts.get(0).AddrInfo.Zip;
			Content_str+= "\ncontact_country: "+client_info.Contacts.get(0).AddrInfo.Country;

			this.Content = Content_str;
		}//end void


/*
		//RENEW THE CONTENT WITH VENDORINFO
		public void RenewContent_VendorInfo_func(Vendor vendor_info)
		{
			String Content_str = "";
			Content_str+= "vendor_name: " +vendor_info.vendorName;
			Content_str+= "\nvendorcontact_first_name: ";
			Content_str+= "\nvendorcontact_last_name: ";
			Content_str+= "\nvendorcontact_full_name: ";
			Content_str+= "\nvendorcontact_phone_number: ";
			Content_str+= "\nvendorcontact_email: ";
			Content_str+= "\nvendorcontact_address1: ";
			Content_str+= "\nvendorcontact_city: ";
			Content_str+= "\nvendorcontact_state: ";
			Content_str+= "\nvendorcontact_zip: ";
			Content_str+= "\nvendorcontact_country: ";


			this.Content = Content_str;
		}//end void
*/

		//RENEW THE CONTENT WITH CUSTOMFIELD
		public void RenewContent_CustomField_func(CustomField_Info customfield)
		{
			String Content_str = "";
			Content_str+= customfield.FieldName+": " +customfield.ValueList.get(0);
			this.Content = Content_str;
		}//end void

		//METHOD FOR RECEIVED EMAIL
		public void GetActivateLink()
		{
			//<a href="https://agoyu-dev.bigin.top/auth/activate/6/5BlMOqMItoOw5008NfgQkt0Pb4Jiqrf0" style="color: #ffffff; text-decoration: none;">Active your email
			String temp_1 ="" ;// TestConfigs.glb_URL+ "/auth/activate";
			String temp_2 = "Active your email";

			int index_1 = this.Content.indexOf(temp_1);
			int index_2 = this.Content.indexOf(temp_2);

			String temp_str =  this.Content.substring(index_1, index_2);


			temp_str = temp_str.replace("<a href=\"","");
			temp_str = temp_str.replace("\"","");
			temp_str = temp_str.replace("<a href=\"","");
			temp_str = temp_str.replace("\"","");

			//OR can use this regex temp_str = temp_str.replaceAll(" style.+\$","")
			int index3 =  temp_str.indexOf(" style");
			temp_str =  temp_str.substring(0, index3);
			this.activatelink = temp_str;

		}


		public void GetResetPassLink()
		{
			//<a href="https://agoyu-dev.bigin.top/auth/activate/6/5BlMOqMItoOw5008NfgQkt0Pb4Jiqrf0" style="color: #ffffff; text-decoration: none;">Active your email
			String temp_1 = "";//TestConfigs.glb_URL+ "/auth/reset";
			String temp_2 = "Reset Your Password";

			int index_1 = this.Content.indexOf(temp_1);
			int index_2 = this.Content.indexOf(temp_2);

			String temp_str =  this.Content.substring(index_1, index_2);


			temp_str = temp_str.replace("<a href=\"","");
			temp_str = temp_str.replace("\"","");
			temp_str = temp_str.replace("<a href=\"","");
			temp_str = temp_str.replace("\"","");
			//OR can use this regex temp_str = temp_str.replaceAll(" style.+\$","")
			int index3 =  temp_str.indexOf(" style");
			temp_str =  temp_str.substring(0, index3);
			this.activatelink = temp_str;

		}

		public void VerifyActivateEmail()
		{

			String heading_content = "Agoyu Confirmation";

			String body_content = "In order to finish your registration you need to confirm your email address. It's easy - just click the Confirm Your Email link below.";

			String active_lnk = "";//TestConfigs.glb_URL+ "/auth/activate";

			String active_lbl ="Active your email";


			if(!this.Content.contains(heading_content))
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Incorrect Confirmation text.[Observed:"+this.Content +"-Expected:"+heading_content+"].";
			}

			if(!this.Content.contains(body_content))
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Incorrect main text.[Observed:"+this.Content +"-Expected:"+body_content+"].";
			}

			if(!this.Content.contains(active_lnk))
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Incorrect Activate link.[Observed:"+this.Content +"-Expected:"+active_lnk+"].";
			}


			if(!this.Content.contains(active_lbl))
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Incorrect Activate text.[Observed:"+this.Content +"-Expected:"+active_lbl+"].";
			}

		}

		public void VerifyWelcomeEmail()
		{

			String heading_content = "Congratulations!";

			String body_content = "You�re now part of a community that connects regular people like you and me, with the world�s most recognized moving companies across the world. Find a moving estimate that suits your budget. And discover techniques to save money during the process.";


			if(!this.Content.contains(heading_content))
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage = "Incorrect Welcome text.[Observed:"+this.Content +"-Expected:"+heading_content+"].";
			}

			if(!this.Content.contains(body_content))
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage = "Incorrect main text.[Observed:"+this.Content +"-Expected:"+body_content+"].";
			}


		}


		public void Init_ResetPassword_Email_Info(String portal_url)
		{
			this.From = glb_RefData.SupportEmail;
			
			this.Subject = "Reset your account password.";
						
			this.Content  = "A password reset has been requested for your ARC portal account. Please click on link below to update your password:";

		
		}//end void


		public void VerifyContentEmail(Mail_Info expect_compare_mail,String emailcontent_operator)
		{
			if(emailcontent_operator.equals("Equals")&&!this.Content.equals(expect_compare_mail.Content))
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Incorrect Email Content.[Observed:"+this.Content +"-Expected:"+expect_compare_mail.Content+"].";

			}
			
			else if(emailcontent_operator.equals("Contains")&&!this.Content.contains(expect_compare_mail.Content))
			{
				TestConfigs.glb_TCStatus = false;
				TestConfigs.glb_TCFailedMessage += "Incorrect Email Content.[Observed:"+this.Content +"-Expected:"+expect_compare_mail.Content+"].";

			}
		}

		public void VerifyAttachmentContent_func(String expect_content)
		{
			for(int i =0;i<download_files.size();i++)
			{
				if(!expect_content.equals(""))
				{
					String download_file_content = download_files.get(i).Content.trim();
					if(!download_file_content.equals(expect_content))
					{
						TestConfigs.glb_TCStatus = false;
						TestConfigs.glb_TCFailedMessage += "Incorrect Attachment["+download_files.get(i).File_Name+"] Content.[Observed:"+download_files.get(i).Content +"-Expected:"+expect_content+"].";

					}//end if download_file
				}//end expect.content.equal
				
			}//end for
			
			
		}//end void
		
		public Note_Info Convert_To_Note_Info()
		{
			Note_Info note = new Note_Info();
			note.AssignType = RecipientType;
			note.Title = Subject;
			note.Content = Content;
			note.Service = ServiceType;
			
			return note;
		}
		
		
		private String Renew_Service_Selected_Vendor_Info_func(Services_Info service)
		{
			String Content_str="";

			String vendorname = "";
			String vendor_ctact_fNam="";
			String vendor_ctact_lNam="";
			String vendor_ctact_fullNam="";
			String vendor_ctact_phone="";
			String vendor_ctact_email="";
			
			String prefix_servicetype = "";
			
			if(service.VendorCtactList.size()>0)
			{
				Contact_Info contact_info = service.VendorCtactList.get(0);
				
				prefix_servicetype = "\n"+service.Type;
				
				vendorname = contact_info.VendorName;
				vendor_ctact_fNam=contact_info.firstname;
				vendor_ctact_lNam=contact_info.lastname;
				vendor_ctact_fullNam=contact_info.FullName;
				vendor_ctact_phone= contact_info.PhoneList.get(0).Numb;
				vendor_ctact_email=contact_info.email_info.EmailAddr;
					
				prefix_servicetype+="Selected" +contact_info.ServiceType;
			}
			
			
			
			Content_str+= "\n"+prefix_servicetype+"VendorName: "+vendorname;
			Content_str+= "\n"+prefix_servicetype+"VendorContactFirstName: "+ vendor_ctact_fNam;
			Content_str+= "\n"+prefix_servicetype+"VendorContactLastName: " + vendor_ctact_lNam;
			Content_str+= "\n"+prefix_servicetype+"VendorContactFullName: "+ vendor_ctact_fullNam;
			Content_str+= "\n"+prefix_servicetype+"VendorContactPhone: " + vendor_ctact_phone;
			Content_str+= "\n"+prefix_servicetype+"VendorContactEmail: " + vendor_ctact_email;
		
					
			return Content_str;
		}

}
