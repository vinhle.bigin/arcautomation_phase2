package businessObjects;

public class HPService_Info extends Services_Info{

	public String SalePrice;
	public String LoanAmount;
	public String CommissionPercent;
	public String CloseDate;
	public String HouseHuntTripDate;
	public double RebateAmount;
	public String EscrowFeeAmount;
	public String EscrowFeePercent;
	public Boolean IsInvoiceCreate;
	public Boolean IsInvoiceClose;
	public Boolean IsEscrowFeeReceive;
	public Boolean IsSignRefFeeAgree;
	public Boolean IsTransfRent;
	public Boolean IsRebateSent;
	public String ApproHuntTripDate;
	public String ApproCloseDate;
	
	public HPService_Info() {
		// TODO Auto-generated constructor stub
		super();
		Type = "Home purchase";
		Status = true;
		SalePrice = "$20.00";
		LoanAmount = "$1,000.00";
		CommissionPercent = "50.00%";

		CloseDate = "04/04/2019";
		HouseHuntTripDate = "04/04/2019";
		RebateAmount = 500;
		EscrowFeeAmount = "$2,000.00";
		EscrowFeePercent = "20.00%";
		IsInvoiceCreate = true;
		IsInvoiceClose = true;
		IsEscrowFeeReceive = true;
		IsSignRefFeeAgree = true;
		IsTransfRent = true;
		IsRebateSent= true;
		
		ApproHuntTripDate = "04/04/2019";
		ApproCloseDate = "05/04/2019";
		
	}
	
	
	
	public HPService_Info(Contact_Info vendor_contact)
	{
		super();
		Type = "Home purchase";
		Status = true;
		SalePrice = "$20.00";
		LoanAmount = "$1,000.00";
		CommissionPercent = "50.00%";

		CloseDate = "04/04/2019";
		HouseHuntTripDate = "04/04/2019";
		RebateAmount = 500;
		EscrowFeeAmount = "$2,000.00";
		EscrowFeePercent = "20.00%";
		IsInvoiceCreate = true;
		IsInvoiceClose = true;
		IsEscrowFeeReceive = true;
		IsSignRefFeeAgree = true;
		IsTransfRent = true;
		IsRebateSent= true;
		
		ApproHuntTripDate = "04/04/2019";
		ApproCloseDate = "05/04/2019";
		VendorCtactList.add(vendor_contact);
	}

}
