package businessObjects;



public class HSService_Info extends Services_Info{

	public String SalePrice;

	public String CommissionPercent;
	public String CloseDate;
	public String RebateAmount;
	public String ListPrice;
	public String ListStartDate;
	public String ListEndDate;
	public String BuyOutAcceptDate;


	public boolean IsSignRefFeeAgree;
	public boolean IsRebaseSent;
	public boolean IsTookBuyOut;
	public String OriListPrice;
	public String CurrentPrice;
	public String HS_1st_PriceReduce;
	public String HS_2st_PriceReduce;
	public String HS_3st_PriceReduce;
	public String HS_4st_PriceReduce;
	public String HS_5st_PriceReduce;
	public String HS_6st_PriceReduce;
	public boolean IsRefFeeReceive;
	public boolean IsNotAutoCal;
	public String RefPercent;
	public String RefFeeAmount_vlue;

 

	public HSService_Info() {
		// TODO Auto-generated constructor stub
		super();
		Type = "Home sale";
		Status = true;
		SalePrice = "$20.00";

		CommissionPercent = "50.00%";

		CloseDate = "04/04/2019";

		RebateAmount = "$0.00";//just apply for some Rich State: Washinton DC
		ListPrice = "$200.00";
		ListStartDate = "04/05/2019";
		ListEndDate = "04/06/2019";
		BuyOutAcceptDate = "04/07/2019";
	
		OriListPrice = "$5.00";
		CurrentPrice = "$7.00";
		HS_1st_PriceReduce = "$8.00";
		HS_2st_PriceReduce = "$9.00";
		HS_3st_PriceReduce = "$10.00";
		HS_4st_PriceReduce = "$11.00";
		HS_5st_PriceReduce = "$12.00";
		HS_6st_PriceReduce = "$13.00";
		IsRefFeeReceive = true;
		IsNotAutoCal = false;
		RefPercent = "20.00%";
		RefFeeAmount_vlue = "$45.00";

	
		IsSignRefFeeAgree = true;
		IsRebaseSent = true;
		IsTookBuyOut = true;
	}
	
	public HSService_Info(Contact_Info vendor_contact)
	{
		super();
		Type = "Home sale";
		Status = true;
		SalePrice = "$20.00";

		CommissionPercent = "50.00%";

		CloseDate = "04/04/2019";

		RebateAmount = "$0.00";//just apply for some Rich State: Washinton DC
		ListPrice = "$200.00";
		ListStartDate = "04/05/2019";
		ListEndDate = "04/06/2019";
		BuyOutAcceptDate = "04/07/2019";
	
		OriListPrice = "$5.00";
		CurrentPrice = "$7.00";
		HS_1st_PriceReduce = "$8.00";
		HS_2st_PriceReduce = "$9.00";
		HS_3st_PriceReduce = "$10.00";
		HS_4st_PriceReduce = "$11.00";
		HS_5st_PriceReduce = "$12.00";
		HS_6st_PriceReduce = "$13.00";
		IsRefFeeReceive = true;
		IsNotAutoCal = false;
		RefPercent = "20.00%";
		RefFeeAmount_vlue = "$45.00";

	
		IsSignRefFeeAgree = true;
		IsRebaseSent = true;
		IsTookBuyOut = true;
		VendorCtactList.add(vendor_contact);
	}
	
	
	
	
	public void Caculate_Referral_Amount()
		
	{
		if(IsNotAutoCal == false)
		{
			double sale_price = Double.parseDouble(SalePrice.replace("$", ""));
			double commission_percent = Double.parseDouble(CommissionPercent.replace("%", ""));
			double ref_percent = Double.parseDouble(RefPercent.replace("%", ""));
			
			double ref_fee_amount = sale_price*(commission_percent/100)*(ref_percent/100);
			
			RefFeeAmount_vlue = "$"+String.format("%.2f", ref_fee_amount);
		}
		
	
	}
	
	

}
