package businessObjects;

public class TQService_Info extends Services_Info{
	
	public String ServiceDesr;
	public String Currency;
	public String AccountPurpose;
	public String NotiDate;
	public String ServiceDate;
	public String MoveInDate;
	public String MoveOutDate;
	public String DayAuth;
	public String ApartmentType;
	public String ApartmentFinish;
	public String Baths;
	public String Budget;
	public String Pets;
	public String SpecialInstruct;
	public String WhoPay;
	public String PaymentType;
	public Boolean IsInvoiceCreate;
	public Boolean IsInvoiceClose;
	public Boolean IsEscrowFeeReceive;
	public Boolean IsBillFirstDay;

	public TQService_Info() {
		super();
		Type = "Temporary quarter";
		Status = true;
		ServiceDesr = "HHG Service Descr Testing";
		Currency = "VND - Viet Nam";
		AccountPurpose = "Reallocation";
		NotiDate = "04/04/2019";
		ServiceDate = "04/04/2019";
		MoveInDate = "04/04/2019";
		MoveOutDate = "05/04/2019";
		DayAuth = "2";
		ApartmentType = "Studio";
		ApartmentFinish = "Unfurnished";
		Baths = "2";
		Budget = "$20,000.00";
		Pets = "1";
		SpecialInstruct = "Go straight on testing";
		WhoPay = "Assignee";
		PaymentType = "Credit";
		IsInvoiceCreate = true;
		IsInvoiceClose = true;
		IsEscrowFeeReceive = true;
		IsBillFirstDay = true;
		// TODO Auto-generated constructor stub
	}
	
	
	public TQService_Info(Contact_Info vendor_contact)
	{
		super();
		Type = "Temporary quarter";
		Status = true;
		ServiceDesr = "HHG Service Descr Testing";
		Currency = "VND - Viet Nam";
		AccountPurpose = "Reallocation";
		NotiDate = "04/04/2019";
		ServiceDate = "04/04/2019";
		MoveInDate = "04/04/2019";
		MoveOutDate = "05/04/2019";
		DayAuth = "2";
		ApartmentType = "Studio";
		ApartmentFinish = "Unfurnished";
		Baths = "2";
		Budget = "$20,000.00";
		Pets = "1";
		SpecialInstruct = "Go straight on testing";
		WhoPay = "Assignee";
		PaymentType = "Credit";
		IsInvoiceCreate = true;
		IsInvoiceClose = true;
		IsEscrowFeeReceive = true;
		IsBillFirstDay = true;
		VendorCtactList.add(vendor_contact);
	}

}
