package businessObjects;

import custom_Func.DateTime_Manage;

public class BankAccount_Info {
	
	public String AccountType;
	public String TransactionType;
	public String AccountName;
	public String Payee;
	public String BankName;
	public String AccountNumber;
	public String HolderName;
	public String Re_AccountType;
	public int Routing;
	public String SwiftCode;
	public String IBAN;

	
	public BankAccount_Info() {
		
		AccountType = "Wire Account"; //USD Direct Deposit/ACH (Checking,Saving)
		
		TransactionType = "DOM"; //INTL
		
		DateTime_Manage dt = new DateTime_Manage();
		AccountName = "Account_" + dt.getCurrentLocalTime();
		
		Payee = "Payee_" + dt.getCurrentLocalTime();
		
		BankName = "Bank_" + dt.getCurrentLocalTime();
		
		AccountNumber = "AccountNumber_" + dt.getCurrentLocalTime();
		
		HolderName = "HolderName_" + dt.getCurrentLocalTime();
		
		Re_AccountType = "Re_AccountType_" + dt.getCurrentLocalTime();
		
		Routing = 123456789;
		
		SwiftCode = "SwiftCode_";
		
		IBAN = "IBAN";
		
		
		
	}
}
