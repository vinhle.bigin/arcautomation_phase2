package businessObjects;

import java.util.Date;
import java.util.Random;

import custom_Func.DateTime_Manage;
import dataDriven.glb_RefData;


public class Service_Order_Info  extends Services_Info{
	
	public String Description;
	public boolean Is_Domestic;
	public boolean Is_International;
	public String ServiceType;
	public String OrderType;
	public String OtherServiceType;
	public String DosmeticMoveType;
	public String ServiceName;
	public String VendorName;
	
	public String ShipModeAuth;
	public String OrderDate;
	public String ContactDate;
	public String AssignDate;
	public String SurveyDate;
	public String Other;
	public String OrderInfo_Status;
	public String OrderInfo_DateIn;
	public String OrderInfo_DateOut;
	
	public String ServiceDate_PackDate_EstStart;
	public String ServiceDate_PackDate_ActStart;
	public String ServiceDate_PackDate_EstEnd;
	public String ServiceDate_PackDate_ActEnd;
	public String ServiceDate_DeliDate_EstStart;
	public String ServiceDate_DeliDate_ActStart;
	public String ServiceDate_DeliDate_EstEnd;
	public String ServiceDate_DeliDate_ActEnd;
	public String ServiceDate_SitDate_EstStart;
	public String ServiceDate_SitDate_ActStart;
	public String ServiceDate_SitDate_EstEnd;
	public String ServiceDate_SitDate_ActEnd;
	public String ServiceDate_Storage_EstStart;
	public String ServiceDate_Storage_ActStart;
	public String ServiceDate_Storage_EstEnd;
	public String ServiceDate_Storage_ActEnd;
	public String ServiceDate_Residence_EstStart;
	public String ServiceDate_Residence_ActStart;
	public String ServiceDate_Residence_EstEnd;
	public String ServiceDate_Residence_ActEnd;
	
	public String StorageInfo_AuthWeight;
	public String StorageInfo_EstWeight;
	public String StorageInfo_ActWeight;
	public String StorageInfo_AuthCube;
	public String StorageInfo_EstCube;
	public String StorageInfo_ActCube;
	public String StorageInfo_Make;
	public String StorageInfo_Model;
	public String StorageInfo_Year;
	
	public String Ship_WeightAuth;
	public String Ship_WeightEst;
	public double Ship_WeightAct;
	public String Ship_DistanceAuth;
	public String Ship_DistanceEst;
	public String Ship_DistanceAct;
	public String Ship_AutoAuth;
	public String Ship_AutoEst;
	public String Ship_AutoAct;
	public String Ship_Descript;
	public String Ship_AirSize_Auth;
	public String Ship_AirSize_Est;
	public String Ship_AirSize_Act;
	public String Ship_AutoHHGAuth;
	public String Ship_AutoHHGEst;
	public String Ship_AutoHHGAct;
	public String Ship_CubeAuth;
	public String Ship_CubeEst;
	public String Ship_CubeAct;
	public String Ship_ContainerAuth;
	public String Ship_ContainerEst;
	public String Ship_ContainerAct;
	
	public double Insure_ValueInsurAmount;
	public double Insure_InsurAuthCap_Amount;
	public String Insure_DateVlueInvenReceiv;
	public String Insure_PaidBy;
	public String Insure_ReimBy;
	public String Insure_Terms;
	public String Insure_Special;
	
	public String StorgeTransit_DaysAuth;
	public String AtOrigin;
	public String AtDes;
	public String PaidBy;
	public String ReimbursedBy;
	public String Terms;
	public String SpecialExpect;
	
	public String ClientType;
	public String ChampCommercial;
	public String SCAC;
	public String TariffCode;
	public String BillType;
	
	public double TotalServiceCharge;
	public String EscrowPercentage;
	public double EscrowAmount;
	public String DateBilledCient;
	public String DateReceviedPayment;
	public String DateInvoiceSubmitted;
	public double Override_Amount;
	public double TotalBillMover_Amount ;
	public double TotalInvoiceClient_Amount;
	public double TotalARCProfit_Amount;
	public double MoveManageFee_Amount;
	public double Audit_1,Audit_2,Audit_3;
	
	public String Faci_FacilityName;
	public String Faci_ContactName;
	public Mail_Info email_info;
	public Address_Info AddrInfo;
	public String Faci_Paidby;
	public String Faci_Reimby;
	public String Faci_Terms;
	public String Faci_Special;
	
	public String BillStorage_ActualDate;
	public String BillStorage_StartDate;
	public String BillStorage_EndDate;
	public String BillStorage_SupplierDays;
	public String BillStorage_TotalDays;
	public String BillStorage_ZipCode;
	
	public String AutoTrans_Make;
	public String AutoTrans_Year;
	public String AutoTrans_Model;
	public String AutoTrans_VIN;
	public String AutoTrans_Plate;
	public String AutoTrans_EstLoadDate;
	public String AutoTrans_ActLoadDate;
	public String AutoTrans_EstDeliDate;
	public String AutoTrans_ActDeliDate;
	public String AutoTrans_SpecNote;
	
	public Service_Order_Info() {
		// TODO Auto-generated constructor stub
		Is_Domestic = false;
		Is_International = false;
		ServiceType ="";
		OrderType ="";
		OtherServiceType="";
		DosmeticMoveType="";
		ServiceName = "Household goods";
		
		Random r = new Random();
		
		int ret2 = r.nextInt(100)+1;
		
		Description = "Order_"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		VendorName = "";
		
		ShipModeAuth = "";
		
		Date dt = DateTime_Manage.Get_DateTime_WithTimeZone(glb_RefData.glb_ARCUser_TimeZone);
		OrderDate = DateTime_Manage.FormatDateTime_WithTimeZone(glb_RefData.glb_ARCUser_TimeZone,dt , "MM/dd/yyyy"); 
		ContactDate = "";
		AssignDate = "";
		SurveyDate = "";
		Other = "";
		OrderInfo_Status = "";
		OrderInfo_DateIn = "";
		OrderInfo_DateOut = "";
		
		ServiceDate_PackDate_EstStart = "";
		ServiceDate_PackDate_ActStart = "";
		ServiceDate_PackDate_EstEnd = "";
		ServiceDate_PackDate_ActEnd = "";
		ServiceDate_DeliDate_EstStart = "";
		ServiceDate_DeliDate_ActStart = "";
		ServiceDate_DeliDate_EstEnd = "";
		ServiceDate_DeliDate_ActEnd = "";
		ServiceDate_SitDate_EstStart = "";
		ServiceDate_SitDate_ActStart = "";
		ServiceDate_SitDate_EstEnd = "";
		ServiceDate_SitDate_ActEnd = "";
		ServiceDate_Storage_EstStart = "";
		ServiceDate_Storage_ActStart = "";
		ServiceDate_Storage_EstEnd = "";
		ServiceDate_Storage_ActEnd = "";
		ServiceDate_Residence_EstStart = "";
		ServiceDate_Residence_ActStart = "";
		ServiceDate_Residence_EstEnd = "";
		ServiceDate_Residence_ActEnd = "";
		
		StorageInfo_AuthWeight = "";
		StorageInfo_EstWeight = "";
		StorageInfo_ActWeight = "";
		StorageInfo_AuthCube = "";
		StorageInfo_EstCube = "";
		StorageInfo_ActCube = "";
		StorageInfo_Make = "";
		StorageInfo_Model = "";
		StorageInfo_Year = "";
		
		Ship_WeightAuth = "";
		Ship_WeightEst = "";
		Ship_WeightAct = 0;
		Ship_DistanceAuth = "";
		Ship_DistanceEst = "";
		Ship_DistanceAct = "";
		Ship_AutoHHGAuth = "";
		Ship_AutoHHGEst = "";
		Ship_AutoHHGAct = "";
		Ship_Descript = "";
		Ship_AirSize_Auth = "";
		Ship_AirSize_Est = "";
		Ship_AirSize_Act = "";
		Ship_CubeAuth = "";
		Ship_CubeEst = "";
		Ship_CubeAct = "";
		Ship_ContainerAuth = "";
		Ship_ContainerEst = "";
		Ship_ContainerAct = "";
		Ship_AutoAuth = "";
		Ship_AutoEst = "";
		Ship_AutoAct = "";
		
		Insure_ValueInsurAmount = 0.00;
		Insure_InsurAuthCap_Amount = 0.00;
		Insure_DateVlueInvenReceiv = "";
		Insure_PaidBy = "";
		Insure_ReimBy = "";
		Insure_Terms = "";
		Insure_Special = "";
		
		StorgeTransit_DaysAuth = "";
		AtOrigin = "";
		AtDes = "";
		PaidBy = "";
		ReimbursedBy = "";
		Terms = "";
		SpecialExpect = "";
		
		ClientType = "";
		ChampCommercial = "";
		SCAC = "";
		TariffCode = "";
		BillType = "";
		
		BillStorage_ActualDate = "";
		BillStorage_StartDate = "";
		BillStorage_EndDate = "";
		BillStorage_SupplierDays = "";
		BillStorage_TotalDays = "";
		BillStorage_ZipCode = "";
		
		TotalServiceCharge = 0;
		EscrowPercentage = "";
		EscrowAmount = 0;
		DateBilledCient = "";
		DateReceviedPayment = "";
		DateInvoiceSubmitted = "";
		
		Faci_FacilityName = "";
		Faci_ContactName = "";
		AddrInfo = new Address_Info();
		email_info = new Mail_Info();
		Faci_Paidby = "";
		Faci_Reimby = "";
		Faci_Terms = "";
		Faci_Special = "";
		
		AutoTrans_Make = "";
		AutoTrans_Year = "";
		AutoTrans_Model = "";
		AutoTrans_VIN = "";
		AutoTrans_Plate = "";
		AutoTrans_EstLoadDate = "";
		AutoTrans_ActLoadDate = "";
		AutoTrans_EstDeliDate = "";
		AutoTrans_ActDeliDate = "";
		AutoTrans_SpecNote = "";
		Override_Amount = 0;
		TotalBillMover_Amount = 0;
		TotalInvoiceClient_Amount = 0;
		TotalARCProfit_Amount = 0;
		MoveManageFee_Amount = 0;
		Audit_1=Audit_2=Audit_3 = 0;
		
}
	
	public void Init_All_DomesticOrder_Info(String vendor_name)
	{
		InIt_VendorSelected(vendor_name);
		InIt_Dosmetic_Order_Info();
		//InIt_International_Order_Info();
		InIt_OrderInfo();
		InIt_ServiceDates();
		InIt_BillingInvoice();
		InIt_Insurance();
		Init_StorageInfo();
		InIt_StorageTransit();
		InIt_ShipmentInfo();
		InIt_ServiceCharge();
		InIt_AutoTransport();
		InIt_FacilityInfo();
		InIt_BillStorage();
	}
	
	public void Init_All_InternationOrder_Info(String vendor_name)
	{
		InIt_VendorSelected(vendor_name);
		//InIt_Dosmetic_Order_Info();
		InIt_International_Order_Info();
		InIt_OrderInfo();
		InIt_ServiceDates();
		InIt_BillingInvoice();
		InIt_Insurance();
		Init_StorageInfo();
		InIt_StorageTransit();
		InIt_ShipmentInfo();
		InIt_ServiceCharge();
		InIt_AutoTransport();
		InIt_FacilityInfo();
		InIt_BillStorage();
	}
	
	
	
	public void InIt_Dosmetic_Order_Info()
	{
		Is_Domestic = true;
		Is_International = false;
		
		ServiceType ="HHG";// UAB,etc., Other
		
		OrderType ="Surface"; // Vehicle
		
		//OtherServiceType="Service for Testing";
		
		DosmeticMoveType="Interstate";//Intrastate
		
	}
	
	
	public void InIt_International_Order_Info()
	{
		
		Is_Domestic = false;
		
		Is_International = true;
		
		ServiceType ="HHE";// UAB,etc., Other
		
		OrderType ="Surface"; // Vehicle
		
		//OtherServiceType="Service for Testing";
		
	}
	
	public void InIt_VendorSelected(String vendor_name)
	{
		VendorName = vendor_name;
	}
	
	public void InIt_OrderInfo()
	{
		Date Date = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),6);
	
		Random r = new Random();
		
		int ret2 = r.nextInt(100)+5;
		
		Description = "Order_"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		ShipModeAuth = "Land"; //Sea
		
		DosmeticMoveType= "Intrastate";//"Interstate"
		
		OrderDate = DateTime_Manage.ConvertDatetoString(Date);
		
		ContactDate = DateTime_Manage.ConvertDatetoString(Date);
		
		AssignDate = DateTime_Manage.ConvertDatetoString(Date);
		
		SurveyDate = DateTime_Manage.ConvertDatetoString(Date);
		
		OrderInfo_Status = "Into Storage"; //Out of Storage
		
		OrderInfo_DateIn = DateTime_Manage.ConvertDatetoString(Date);
		
		OrderInfo_DateOut = DateTime_Manage.ConvertDatetoString(Date);
		
		Other = "This is testing !";
	}
	
	public void InIt_ServiceDates()
	{
		Date Date_Start = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),6);
		
		Date Date_End = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),11);

		ServiceDate_PackDate_EstStart = ServiceDate_PackDate_ActStart = DateTime_Manage.ConvertDatetoString(Date_Start);
		
		ServiceDate_PackDate_EstEnd = ServiceDate_PackDate_ActEnd = DateTime_Manage.ConvertDatetoString(Date_End);
		
		ServiceDate_DeliDate_EstStart = ServiceDate_DeliDate_ActStart = DateTime_Manage.ConvertDatetoString(Date_Start);
		
		ServiceDate_DeliDate_EstEnd = ServiceDate_DeliDate_ActEnd = DateTime_Manage.ConvertDatetoString(Date_End);
		
		ServiceDate_SitDate_EstStart = ServiceDate_SitDate_ActStart = DateTime_Manage.ConvertDatetoString(Date_Start);
		
		ServiceDate_SitDate_EstEnd = ServiceDate_SitDate_ActEnd = DateTime_Manage.ConvertDatetoString(Date_End);
		
		ServiceDate_Storage_EstStart = ServiceDate_Storage_ActStart = DateTime_Manage.ConvertDatetoString(Date_Start);
		
		ServiceDate_Storage_EstEnd = ServiceDate_Storage_ActEnd = DateTime_Manage.ConvertDatetoString(Date_End);
		
		ServiceDate_Residence_EstStart = ServiceDate_Residence_ActStart = DateTime_Manage.ConvertDatetoString(Date_Start);
		
		ServiceDate_Residence_EstEnd = ServiceDate_Residence_ActEnd = DateTime_Manage.ConvertDatetoString(Date_End);
	}
	
	public void InIt_BillingInvoice()
	{
		Date Date = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),6);
		
		TotalServiceCharge = 9999;
		
		EscrowPercentage = "10.00%";
		
		EscrowAmount = 7777;	
	
		DateBilledCient = DateTime_Manage.ConvertDatetoString(Date);
		
		DateReceviedPayment = DateTime_Manage.ConvertDatetoString(Date);
		
		DateInvoiceSubmitted = DateTime_Manage.ConvertDatetoString(Date);
	}
	
	public void InIt_Insurance()
	{
		Date Date = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),6);

		Insure_ValueInsurAmount = 6666;
		
		Insure_InsurAuthCap_Amount = 5555;
		
		Insure_DateVlueInvenReceiv = DateTime_Manage.ConvertDatetoString(Date);
		
		Insure_PaidBy = "Assignee"; //Client
		
		Insure_ReimBy = "Client"; //Relocation Company
		
		Insure_Terms = "This is testing !";
		
		Insure_Special = "This is testing 1 !";
	}
	
	public void Init_StorageInfo()
	{
		Random r = new Random();
		
		int ret2 = r.nextInt(100)+5;
		
		StorageInfo_AuthWeight = "1,111.00";
		
		StorageInfo_EstWeight = "2,222.00";
		
		StorageInfo_ActWeight = "3,333.00";
		
		StorageInfo_AuthCube = "4,444.00";
		
		StorageInfo_EstCube = "5,555.00";
		
		StorageInfo_ActCube = "6,666.00";
		
		StorageInfo_Make = "Make_"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		StorageInfo_Model = "Model_"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		StorageInfo_Year = "2021";
	}
	
	public void InIt_StorageTransit()
	{
		StorgeTransit_DaysAuth = "3";
		
		AtOrigin = "2";
		
		AtDes = "1";
		
		PaidBy = "Assignee"; //Client
		
		ReimbursedBy = "Client"; //Relocation Company
		
		Terms = "This is testing !";
		
		SpecialExpect = "This is testing 1 !";
	}
	
	public void InIt_ShipmentInfo()
	{
		Random r = new Random();
		
		int ret2 = r.nextInt(100)+5;
		
		Ship_Descript = "ShipDescript_"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		Ship_WeightAuth = "1,212.00";
		
		Ship_WeightEst = "1,313.00";
		
		Ship_WeightAct = 1414;
		
		Ship_AutoHHGAuth = "1,515.00";
		
		Ship_AutoHHGEst = "1,616.00";
		
		Ship_AutoHHGAct = "1,717.00";
		
		Ship_AirSize_Auth = "LDN"; //E, D, Other
		
		Ship_AirSize_Est = "LDN"; //E, D, Other
		
		Ship_AirSize_Act = "LDN"; //E, D, Other
		
		Ship_CubeAuth = "1,234.00";
		
		Ship_CubeEst = "2,345.00";
		
		Ship_CubeAct = "3,456.00";
		
		Ship_ContainerAuth = "4,567.00";
		
		Ship_ContainerEst = "5,678.00";
		
		Ship_ContainerAct = "6,789.00";
		
		Ship_AutoAuth = "1,111.00";
		
		Ship_AutoEst = "2,222.00";
		
		Ship_AutoAct = "3,333.00";
	}
	
	public void InIt_ServiceCharge()
	{
		ClientType = "Government"; //Corporate
		
		ChampCommercial = "Champ"; //Commercial
		
		SCAC = "test123";
		
		TariffCode = "GSA01";
		
		BillType = "COD"; //Corporate
	}
	
	public void InIt_AutoTransport()
	{
		Random r = new Random();
		
		int ret2 = r.nextInt(100)+5;
		
		Date Est_Date = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),2);
		
		Date Act_Date = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),6);
		
		AutoTrans_Make = "Make_"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		AutoTrans_Year = "2021";
		
		AutoTrans_Model = "Model_"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		AutoTrans_VIN = "VIN_"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		AutoTrans_Plate = "Plate_"+ DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		AutoTrans_EstLoadDate = DateTime_Manage.ConvertDatetoString(Est_Date);
		
		AutoTrans_ActLoadDate = DateTime_Manage.ConvertDatetoString(Act_Date);
		
		AutoTrans_EstDeliDate = DateTime_Manage.ConvertDatetoString(Est_Date);
		
		AutoTrans_ActDeliDate = DateTime_Manage.ConvertDatetoString(Act_Date);
		
		AutoTrans_SpecNote = "This is testing !";
	}
	
	public void InIt_FacilityInfo()
	{
		Faci_FacilityName = "Cali Street";
		
		Faci_ContactName = "20 Cali Street";
		
		email_info.EmailAddr = this.VendorName +"@yopmail.com";
		
		AddrInfo.Addr1 = "20 Cali Street";
		
		AddrInfo.City = "Cali";
		
		AddrInfo.State  = "Texas";
		
		AddrInfo.Country  = "United States of America";
		
		AddrInfo.Zip = "88888";
		
		Faci_Paidby = "Assignee";  //Client
		
		Faci_Reimby = "Client"; //Relocation Company
		
		Faci_Terms = "This is testing !";
		
		Faci_Special = "This is testing 1 !";
	}
	
	public void InIt_BillStorage()
	{
		Date Date = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),2);
		
		BillStorage_ActualDate = DateTime_Manage.ConvertDatetoString(Date);
		
		BillStorage_StartDate = DateTime_Manage.ConvertDatetoString(Date);
		
		BillStorage_EndDate = DateTime_Manage.ConvertDatetoString(Date);
		
		BillStorage_SupplierDays = "123";
		
		BillStorage_TotalDays = "567";
		
		BillStorage_ZipCode = "345";
	}
	
	public void Calculate_BillInvoice()
	{
		TotalInvoiceClient_Amount = MoveManageFee_Amount+ TotalServiceCharge;
		System.out.println(TotalInvoiceClient_Amount);
		
		TotalARCProfit_Amount = EscrowAmount + MoveManageFee_Amount + Audit_1+Audit_2+Audit_3;
		System.out.println(TotalARCProfit_Amount);
		
		TotalBillMover_Amount = TotalInvoiceClient_Amount-TotalARCProfit_Amount;
		System.out.println(TotalBillMover_Amount);
		
	}
	
}
