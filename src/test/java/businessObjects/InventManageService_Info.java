package businessObjects;

public class InventManageService_Info extends Services_Info{
	
	public HSService_Info HS_ListSale_section;
	
	public String HSAnal_PropertyType;
	public String HSAnal_LocationType;
	public String HSAnal_ListCompAgent;
	public String HSAnal_Indicate;
	public String HSAnal_Improvements;
	public String HSAnal_PropertyInfo;
	public String HSAnal_ProperCondition;
	public String HSAnal_InteriorCondition;
	public String HSAnal_ExeriorCondition;
	public String HSAnal_Inspections;
	public String HSAnal_PropertyFinanceMeans;
	public String HSAnal_FinanceDescript;
	public Boolean HSAnal_CommonInterestDev;
	public String HSAnal_OwnersPercent;
	public String HSAnal_InvestorsPercent;
	public String HSAnal_HOAFee;
	public String HSAnal_HOAFeePaidFrequent;
	public String HSAnal_PriceRangeFrom;
	public String HSAnal_PriceRangeTo;
	public String HSAnal_AverageDays;
	public String HSAnal_SugesstSellPrice;
	public String HSAnal_SugesstListPrice;
	public String HSAnal_MarketConditionsCmt;
	public String HSAnal_Exceed;

	public InventManageService_Info() {
		super();
		Type = "Inventory management";
		Status = true;
		HS_ListSale_section = new HSService_Info();
		
		HSAnal_PropertyType = "Condo";
		HSAnal_LocationType = "Urban";
		HSAnal_ListCompAgent = "Company";
		HSAnal_Indicate = "Refrigerator";
		HSAnal_Improvements = "Improvement";
		HSAnal_PropertyInfo = "Information";
		HSAnal_ProperCondition = "General";
		HSAnal_InteriorCondition = "Interior";
		HSAnal_ExeriorCondition = "Exterior";
		HSAnal_Inspections = "Inspections";
		HSAnal_PropertyFinanceMeans = "FHA";
		HSAnal_FinanceDescript = "Financing";
		HSAnal_CommonInterestDev = false;
		HSAnal_OwnersPercent = "80%";
		HSAnal_InvestorsPercent = "20%";
		HSAnal_HOAFee = "$1,234.00";
		HSAnal_HOAFeePaidFrequent = "Monthly";
		HSAnal_PriceRangeFrom = "2";
		HSAnal_PriceRangeTo = "3";
		HSAnal_AverageDays = "4";
		HSAnal_SugesstSellPrice = "$213.00";
		HSAnal_SugesstListPrice = "$555.00";
		HSAnal_MarketConditionsCmt = "Comment";
		HSAnal_Exceed = "1";
		
		// TODO Auto-generated constructor stub
	}
	
	
	public InventManageService_Info(Contact_Info vendor_contact)
	{
		super();
		Type = "Inventory management";
		Status = true;
		HS_ListSale_section = new HSService_Info();
		
		VendorCtactList.add(vendor_contact);
	}

}
