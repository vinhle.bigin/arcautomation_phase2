package businessObjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import custom_Func.DateTime_Manage;
import Enums.Approval;

public class SubExpense_Info extends Services_Info{

	public Date Exp_Date;
	public String Purpose;
	public String ServiceType; 
	public Double Amount;
	public List<String> FilesName;
	public String Descr;
	public Approval SubExApproval;
	
	//Approve Expense modal
	public String Transaction;
	public String ExpenseCode;
	public String InvoiceNo;
	public Date InvoiceDate;
	public String Note;
	public Boolean ClientView;
	public Boolean TransfView;
	
	public SubExpense_Info() {
		
		super();
		
		Type = "Expense management";
				
		Exp_Date = DateTime_Manage.GetCurrentLocalDate_Date();	
	
		Purpose = "Temporary Living"; //Final Trip, House Hunting, Return Trip/Home Visit, Real Estate, Household Goods, All-purpose
		
		ServiceType = "Airfare"; //Taxi/Car Rental, Lodging, Meals, Lease Cancellation, Mileage
		
		Random random = new Random();
		Amount = random.nextDouble();
		
		FilesName = new ArrayList<String>();
		
		Descr = "This is testing !";
		
		Transaction = "ID"+ DateTime_Manage.getCurrentLocalTime();
		
		ExpenseCode = "100A - Shipment of Household Goods";
		
		InvoiceNo = "Invoice No";
		
		InvoiceDate = DateTime_Manage.GetCurrentLocalDate_Date();
		
		ClientView = true;
		
		TransfView = true;
	}
	
	public void InIt_Attachments(String file_name)
	{
		FilesName.add(file_name);
	}
}
