package businessObjects;

public class Print_Info {
	
	public String PrintOutType;
	public String Template;
	public boolean PDF;
	public boolean Word;
	public boolean PrintCurrentSearch;
	public boolean AllTransfContact;
	
	public Print_Info() {
		PrintOutType = "Address Label"; //"Letter"
		Template = "QAtesting Transferee Address";
		PDF = false;
		Word = false;
		PrintCurrentSearch = false;
		AllTransfContact = false;
		
		
	}

}
