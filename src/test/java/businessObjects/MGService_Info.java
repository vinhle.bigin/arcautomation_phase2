package businessObjects;

public class MGService_Info extends Services_Info{
	
	public double MortgageAmount;
	public String IndexRate;
	public String AmountAtClose;
	public String DownPayment;
	public String CloseDate;
	public String LockInDate;

	public MGService_Info() {
		// TODO Auto-generated constructor stub
		super();
		Type = "Mortgage";
		Status = true;
		MortgageAmount = 1000.00;
		IndexRate = "50";
		AmountAtClose = "$200.00";
		DownPayment = "$100.00";
		CloseDate = "04/04/2019";
		LockInDate = "04/05/2019";
	}
	
	
	
	public MGService_Info(Contact_Info vendor_contact)
	{
		super();
		Type = "Mortgage";
		Status = true;
		MortgageAmount = 1000.00;
		IndexRate = "50";
		AmountAtClose = "$200.00";
		DownPayment = "$100.00";
		CloseDate = "04/04/2019";
		LockInDate = "04/05/2019";
		VendorCtactList.add(vendor_contact);
	}

}
