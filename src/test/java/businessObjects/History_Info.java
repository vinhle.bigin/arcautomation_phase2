package businessObjects;

import java.util.Date;

import custom_Func.DateTime_Manage;

public class History_Info {
	public String StartDate;

	public String EndDate;

	public String Type;

	public String Detail;

	public String User;

	public History_Info() {
		// TODO Auto-generated constructor stub
		StartDate = DateTime_Manage.GetCurrentLocalDate_Str();

		Date dt = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),1);
		EndDate = DateTime_Manage.ConvertDatetoString(dt);
		System.out.println("EndDate: "+EndDate);

		Type = "Change Log";

		Detail = "Updated Ownership";

		User = "";
	}
	


}
