
package businessObjects;

import java.util.Random;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import custom_Func.DateTime_Manage;
import dataDriven.glb_RefData;

public class Activity_Info {
	
	
		public boolean Is_SmarTask_Activity;
		
		public String SM_Assignee;
		public String Type;

		public String StartDate;

		public String EndDate;

		public String Service;

		public String Title;

		public String Content;

		public boolean Closed;

		public String ScheduleWith;

		public  String reasonClosing;

		public String Priority;

		public String Status;

		public int SM_Days;

		public String Date;
		
		public String Unit;

		public String Assignee;
		
		public String Recepient;
		
		public String AssigneeType;
		
		public String RelatedTo;
		
		public String Vendor;
		
		public boolean ClientView;
		
		public boolean VendorView;
		
		public boolean TransfereeView;
		
		public List<String> FilesName;

		public Activity_Info() {
			
			
			SM_Assignee = "Coordinator";
			Is_SmarTask_Activity = false;

			Assignee = "Coordinator";
			
			Type = "Task"; // "Follow Up"

			StartDate = DateTime_Manage.GetCurrentLocalDate_Str()+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
			
			Date dt = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),1);
			
			EndDate = DateTime_Manage.ConvertDatetoString(dt)+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
			
			System.out.println("EndDate: "+EndDate);


			Service = "General";
			Random r = new Random();
			int ret2 = r.nextInt(100)+1;
			Title = "Activity-Title for Testing"+DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);

			Content = "Activity-Content for Testing"+DateTime_Manage.getCurrentLocalTime();

			Closed = false;

		
			reasonClosing = "Reason for closing";

			Priority = "High";
			Status = "Active";

			ScheduleWith = "";

			Date = "Trigger Date";

			SM_Days = 1;
			
			Unit = "Business";
			AssigneeType = "Transferee"; //"Transferee" // "Vendor" | "Client";
			
			ClientView = false;
			
			VendorView = false;
			
			TransfereeView = false;
			
			FilesName = new ArrayList<String>();
		}
		
		//Constructure for cases: create activity with associated HHG Order, Vendor info
		public Activity_Info(String RelatedTo_str, String Vendor_str) {
			
			
			SM_Assignee = "Coordinator";
			Is_SmarTask_Activity = false;

			Assignee = "Coordinator";
			
			Type = "Task"; // "Follow Up"

			StartDate = DateTime_Manage.GetCurrentLocalDate_Str()+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
			
			Date dt = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),1);
			
			EndDate = DateTime_Manage.ConvertDatetoString(dt)+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
			
			System.out.println("EndDate: "+EndDate);


			Service = "General";
			Random r = new Random();
			int ret2 = r.nextInt(100)+1;
			Title = "Activity-Title for Testing"+DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);

			Content = "Activity-Content for Testing"+DateTime_Manage.getCurrentLocalTime();

			Closed = false;

		
			reasonClosing = "Reason for closing";

			Priority = "High";
			Status = "Active";

			ScheduleWith = "";

			Date = "Trigger Date";

			SM_Days = 1;
			
			Unit = "Business";
			AssigneeType = "Transferee"; //"Transferee" // "Vendor" | "Client";
			
			RelatedTo = RelatedTo_str;
			
			Vendor = Vendor_str;
			
			FilesName = new ArrayList<String>();
		}


		public void RegenerateInfo() {
			Title = "Updated_Activity-Title for Testing"+DateTime_Manage.getCurrentLocalTime();

			Content = "Updated_Activity-Content for Testing"+DateTime_Manage.getCurrentLocalTime();
		}
		
		
		public void Init_SM_Activity_Info(String smarttask_name, int days, String unit)
		{
			Is_SmarTask_Activity = true;
			Title = "SM_Activity_"+smarttask_name;
			SM_Days = days;
			Unit= unit;
			Calculate_STart_End_Date_func();
		}
		
		public void InIt_Attachments(String file_name)
		{
			FilesName.add(file_name);
		}
		
		
		public void Calculate_STart_End_Date_func()
		{
			String date_str="";
			String date_str_2="";
			Date dt_tmp=DateTime_Manage.Get_DateTime_WithTimeZone(glb_RefData.glb_ARCUser_TimeZone);
						
			if(Is_SmarTask_Activity == true && Unit.equals("Business"))
			{
				dt_tmp = DateTime_Manage.increase_bussiness_day_func(dt_tmp, SM_Days);
			
			}//end if
			
			else if(Is_SmarTask_Activity == true && Unit.equals("Calendar"))
			{
				dt_tmp = DateTime_Manage.IncreaseDay(dt_tmp, SM_Days);
			
			}//end else
			
			dt_tmp.setHours(1);
			dt_tmp.setMinutes(0);
			
			date_str = DateTime_Manage.Format_DateTime_To_String(dt_tmp);
			//date_str = DateTime_Manage.FormatDateTime_WithTimeZone("UTC",dt_tmp);
			
			dt_tmp.setMinutes(dt_tmp.getMinutes()+30);
			
			date_str_2 =  DateTime_Manage.Format_DateTime_To_String(dt_tmp);
			
			StartDate = date_str;
			
			EndDate = date_str_2;
		}


	

}
