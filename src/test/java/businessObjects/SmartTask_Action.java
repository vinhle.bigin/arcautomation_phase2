package businessObjects;

public class SmartTask_Action {
	
	//===CLASS ACTION

	
		public String Type;
		public Activity_Info Act_Activity;
		
		public String FieldUpdate_FieldName;
		public String FieldUpdate_ValueTo;
		public String Email_Template;
		public String Email_Recipient;
		public boolean Email_IsImmediate;
		public String Email_AfterDays;
		public String Email_AfterUnit;
		public boolean Email_IsAttachSurvey;


		public void InitSendEmailAction_func(String templatename)
		{
			Type = "Sending Email Action";
			Email_Template = templatename;
			Email_Recipient = "Transferee";
			Email_IsImmediate = true;
			Email_IsAttachSurvey = false;
			
		}



		public void InitUpdateFieldAction_func(String fieldname , String newvalue)
		{
			Type = "Updating Field Action";
			FieldUpdate_FieldName = fieldname;
			FieldUpdate_ValueTo = newvalue;

			if(fieldname=="")
				FieldUpdate_FieldName = "Transferee - First Name";
			else
				FieldUpdate_FieldName = fieldname;


			FieldUpdate_ValueTo = newvalue;
		}



		public void Add_Existing_Activity_To_Action(Activity_Info activity_tmp)
		{
			Type = "Creating Activity Action";
			Act_Activity = activity_tmp;

		}
		
		public void InIt_NewActivity_Action(String sm_name, int days, String unit)
		{
			Type = "Creating Activity Action";
			
			Activity_Info activity_tmp = new Activity_Info();
			activity_tmp.Init_SM_Activity_Info(sm_name, days, unit);
			
			Act_Activity = activity_tmp;
			

		}

	
	public SmartTask_Action() {
	// TODO Auto-generated constructor stub
	Type = ""; //Updating Field Action, Creating Activity Action
	Email_Template = "";
	FieldUpdate_FieldName = "";
	FieldUpdate_ValueTo = "";
	Act_Activity = new Activity_Info();
}

}
