package businessObjects;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import custom_Func.DateTime_Manage;
import dataDriven.glb_RefData;

public class HBOService_Info extends Services_Info{
	
	public String Ins_Type;
	public String Ins_OrderDate;
	public String Ins_InsDate;
	public String Ins_CompletedDate;
	public String Ins_CancelledDate;
	public Boolean Ins_FollowUp;
	
	public String Repair_RepairType;
	public String Repair_Amount;
	public String Repair_OptionSlected;
	public Boolean Repair_Completed;
	
	public String Seller_AdjustExplain;
	public String Seller_Amount;
	
	public HSService_Info HS_ListSale_section;
	public InventManageService_Info IM_HSAnalysis_section;
		
	public String Transt_UserList;
	public String Transt_Assistant;
	public String Transt_HBOSpecialList;
	public String Transt_AcquisType;
	public String Transt_AcquisAmount;
	public String Transt_AcquisDate;
	public String Transt_GuarantOfferType;
	public String Transt_ResaleAmount;
	public String Transt_ResaleDate;
	public String Transt_SpecialInstruct;
	
	public String Transt_AnticEquit_Amount;
	public String Transt_AnticEquit_Date;
	public String Transt_AnticEquitReturn_Amount;
	public String Transt_AnticEquitReturn_Date;
	
	public String Transt_ActualEquit_Amount;
	public String Transt_ActualEquit_Date;
	public String Transt_ActualEquitReturn_Amount;
	public String Transt_ActualEquitReturn_Date;
	
	public List<HBO_Offer_Info> Offer_list;
	
	

	public HBOService_Info() {
		super();
		Type = "Home buyout";
		Status = true;
		
		HS_ListSale_section = new HSService_Info();
		IM_HSAnalysis_section = new InventManageService_Info();
		
		Ins_Type = "";
		Ins_OrderDate = "";
		Ins_InsDate = "";
		Ins_CompletedDate = "";
		Ins_CancelledDate = "";
		Ins_FollowUp = false;
		
		Repair_RepairType = "";
		Repair_Amount = "";
		Repair_OptionSlected = "";
		Repair_Completed = false;
		
		Seller_AdjustExplain = "";
		Seller_Amount = "";
		// TODO Auto-generated constructor stub
		
		Offer_list = new ArrayList<HBO_Offer_Info>();
		
		Transt_SpecialInstruct = "";
		Transt_AnticEquit_Amount = "$0.00";
		Transt_AnticEquit_Date = "";
		Transt_AnticEquitReturn_Amount ="$0.00";
		Transt_AnticEquitReturn_Date = "";
		
		Transt_ActualEquit_Amount ="$0.00";
		Transt_ActualEquit_Date = "";
		Transt_ActualEquitReturn_Amount ="$0.00";
		Transt_ActualEquitReturn_Date = "";
		
		
	}

	
	
	public HBOService_Info(Contact_Info vendor_contact)
	{
		super();
		Type = "Home buyout";
		Status = true;
		
		VendorCtactList.add(vendor_contact);
		Transt_AnticEquit_Amount = "$0.00";
		Transt_AnticEquit_Date = "";
		Transt_AnticEquitReturn_Amount ="$0.00";
		Transt_AnticEquitReturn_Date = "";
		
		Transt_ActualEquit_Amount ="$0.00";
		Transt_ActualEquit_Date = "";
		Transt_ActualEquitReturn_Amount ="$0.00";
		Transt_ActualEquitReturn_Date = "";
	}
	
	
	public void Init_InspectionsNeed()
	{
		Date Date = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),6);
		
		Ins_Type = "Spa";
		
		Ins_OrderDate = DateTime_Manage.ConvertDatetoString(Date);
		
		Ins_InsDate = DateTime_Manage.ConvertDatetoString(Date);
	
		Ins_CompletedDate = DateTime_Manage.ConvertDatetoString(Date);
		
		Ins_CancelledDate = DateTime_Manage.ConvertDatetoString(Date);
		
		Ins_FollowUp = true;
	}
	
	public void Init_RequiredRepair()
	{
		Random r = new Random();
		
		int ret2 = r.nextInt(100)+1;
		
		Repair_RepairType = "RepairType_" + DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		Repair_Amount = "1234";
		
		Repair_OptionSlected = "EE to complete";
		
		Repair_Completed = true;
	}
	
	public void Init_SellerConcession()
	{
		Random r = new Random();
		
		int ret2 = r.nextInt(100)+1;
		
		Seller_AdjustExplain = "Adjust_" + DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		Seller_Amount = "1234";
	}
	
	
	public void Init_TransactionInfo(String acquist_type)
	{
		//acquist_type must be consistent with the HBO Offer Type: Amended, Amended From Zero, BVO, BVO Fall through, GBO, Home Sale, Inventory, None
		Date Date_tmp = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),2);
		
		Transt_UserList = glb_RefData.Coordinator;
		Transt_Assistant = "Daniel Yang";
		
		Transt_HBOSpecialList = glb_RefData.Coordinator;
		Transt_AcquisType = acquist_type;
		Transt_AcquisAmount = "$354.00";
	
		Transt_AcquisDate =  DateTime_Manage.ConvertDatetoString(Date_tmp);
		Transt_GuarantOfferType ="Direct Offer";//Based on appraisals, Direct Offer
		Transt_ResaleAmount = "$0.00";
		Transt_ResaleDate = "";
		//Transt_ResaleDate = DateTime_Manage.ConvertDatetoString(Date_tmp);
		//Transt_SpecialInstruct = "Note for testing";
	}
	
	public void Init_OfferInfo(String offer_type)
	{
		HBO_Offer_Info offer_info = new HBO_Offer_Info(offer_type);
		
		Offer_list.add(offer_info);
		
	}

}
