package businessObjects;

import java.util.ArrayList;
import java.util.List;

public class Client_Info extends UserAccount_Info {

	public static final String Title = null;
	public String Status;
	public String CompanyName;
	public List<Address_Info> AddrInfo;
	public List<Contact_Info> Contacts;
	public String Description;
	public String Type;
	public String Region;
	public String Territory;
	public String Revenue;
	public String ReferredBy;
	public String Industry;
	public String AccountManager;
	public String OfEmployees;
	public String Approval_option;

	public Client_Info() {
		// TODO Auto-generated constructor stub
		super();

		Status = "Active";
		CompanyName = "Client_" + this.firstname;

		PhoneList = new ArrayList<>();
		PhoneList.add(new Phone_Info());

		// add new contact data to the list
		Contacts = new ArrayList<>();

		AddrInfo = new ArrayList<>();
		AddrInfo.add(new Address_Info());
		Description = "this is for testing.";
		Type = "Corporate";// "Government | "Corporate"
		Region = "Southeast"; // "Northeast | West | Midwest"
		Territory = "South East";
		Revenue = "10000000";
		ReferredBy = "Friend";
		Industry = "Legal";
		AccountManager = "Linh Nguyen";
		OfEmployees = "10";

		// Preferences option
		Approval_option = "No Required"; // "Any Contact", "Specified Contact", "Entering Contact";

	}

	public Client_Info(Contact_Info contact) {
		// TODO Auto-generated constructor stub
		super();

		Status = "Active";
		CompanyName = "Client_" + this.firstname;

		PhoneList = new ArrayList<>();
		PhoneList.add(new Phone_Info());

		// add new contact data to the list
		Contacts = new ArrayList<>();

		Contacts.add(contact);

		AddrInfo = new ArrayList<>();
		AddrInfo.add(new Address_Info());
		Description = "this is for testing.";
		Type = "Corporate";// "Government | "Corporate"
		Region = "Southeast"; // "Northeast | West | Midwest"
		Territory = "South East";
		Revenue = "10000000";
		ReferredBy = "Friend";
		Industry = "Legal";
		AccountManager = "Linh Nguyen";
		OfEmployees = "10";

		// Preferences option
		Approval_option = "No Required"; // "Any Contact", "Specified Contact", "Entering Contact";

	}

	public void ReGenerateInfo() {
		// UserAccount_Info u_tmp = new UserAccount_Info();
		this.CompanyName = "Update " + this.CompanyName;
		// Status = "Inactive";
		Type = "Government";// "Government | "Corporate"
		Region = "Northeast";
		Territory = "East";
		Revenue = "90000000";
		ReferredBy = "Called In";
		Industry = "Education";
		AccountManager = "QATesting VN";
		OfEmployees = "10";
	}

}
