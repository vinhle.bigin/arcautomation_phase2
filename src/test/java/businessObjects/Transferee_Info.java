package businessObjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import custom_Func.DateTime_Manage;
import dataDriven.glb_RefData;

public class Transferee_Info extends UserAccount_Info {
	//General Information
		public String Salutation;
		public String TransfereeID;
		public String Pronunciation;
		public String Nickname;
		
		public String Coordinator;
		public String CoordinatorAssistant;
		public String TeamLeader;
		public String UrgentInstructions;
		public String AddintionNote;
		public Boolean IsExecutiveOfficer;
		
		public Client_Info Client;
		

		//STATUS
		public String Status;
		public String StatusChangeDate;
		public Boolean IsOverRide;
		public Boolean IsQualified;
		public String FileEnterDate;
		
		public Boolean IsReceivedSignedABAD;
		public Boolean ReferedListSide;
		public Boolean ReferedBuySide;
		public Boolean ReferedToTitleComp;

		//PARTNER INFO
		public String PartnerFname;
		public String PartnerLname;
		public String PartnerMid;
		public String PartnerSSN;
		//OTHERS' INFO
		public Other_Info Other_info;
		public Address_Info OriResAddr_info;
		public Address_Info DesResAddr_info;
		public Address_Info OriOffAddr_info;
		public Address_Info DesOffAddr_info;
		public List<Dependent_Info> Dependent_list;
		//public Bank_Info Bank_info;
		public List<Services_Info> ServiceList;
		public	HHGService_Info HHG_service;
		public	HPService_Info HP_service;
		public	HSService_Info HS_service;
		
		public  TQService_Info TQ_service;
		public	MGService_Info MG_service;
		public 	AppraisalService_Info Appraisal_service;
		public HomeInspectService_Info HomeInspect_service;
		public InventManageService_Info InventManage_service;
		public HBOService_Info HBO_service;
		
		public SubExpense_Info Expense_service;
		
		public List<ReimBurs_Info> ReimBursList;
		
		//UserInfo
		public String Linkedin_Profile;
		public String FB_Profile;
		
		//TimeZone
		public String TimeZone_country;
		
		//Password
		public String new_password;
		public String retype_password;
		public String Policy;
		public String effectDate;
		

	public Transferee_Info() {
		// TODO Auto-generated constructor stub
			super();
			role = "";
			Initiate_Info();
			
			
	}
		
	
	public void Initiate_Info()
	{
		role = "Transferee";
		this.firstname ="Transf_" + this.firstname;
		FullName = this.firstname + " " + this.lastname;
		Salutation = "Mr.";
		TransfereeID = "";
		Pronunciation = "";
		Nickname ="";
		Jobtitle = "";
		Coordinator = "QATesting VN";
		CoordinatorAssistant = "";
		TeamLeader = "";
		UrgentInstructions = "";
		AddintionNote = "";
		IsExecutiveOfficer = false;
		PhoneList = new ArrayList<Phone_Info>();
		PhoneList.add(new Phone_Info());

		MailList = new ArrayList<Mail_Info>();
		Mail_Info newmail = new Mail_Info();

		//newmail.From = this.Coordinator;
		MailList.add(newmail);

		Client = glb_RefData.Client;
	
		//STATUS
		Status = "Active";
		StatusChangeDate="";
		IsOverRide = false;
		IsQualified = false;
		
		Date user_dt = DateTime_Manage.GetLocalDatetime();
		
		FileEnterDate = DateTime_Manage.FormatDateTime_WithTimeZone(glb_RefData.glb_ARCUser_TimeZone,user_dt,"MM/dd/yyyy");
		
		IsReceivedSignedABAD = false;
		ReferedListSide = false;
		ReferedBuySide = false;
		ReferedToTitleComp = false;

		//PARTNER INFO
		PartnerFname = "";
		PartnerLname = "";
		PartnerMid = "";
		PartnerSSN = "";

		//OTHERS' INFO
		Other_info = new Other_Info();
		OriResAddr_info = new AddrOriRes_Info();
		DesResAddr_info = new AddrDesRes_Info();
		OriOffAddr_info = new AddrOriOff_Info();
		DesOffAddr_info = new AddrDesOff_Info();
		Dependent_list = new ArrayList<Dependent_Info>();
		Dependent_list.add(new Dependent_Info());
		//Bank_info = new BankInfo();

		HHG_service = new HHGService_Info();
		HP_service = new HPService_Info();
		HS_service = new HSService_Info();
		TQ_service = new TQService_Info();
		MG_service = new MGService_Info();
		Appraisal_service = new AppraisalService_Info();
		HomeInspect_service = new HomeInspectService_Info();
		
		HBO_service = new HBOService_Info();	
		HBO_service.Init_InspectionsNeed();
		HBO_service.Init_RequiredRepair();
		HBO_service.Init_SellerConcession();
		HBO_service.Init_TransactionInfo("GBO");
		HBO_service.Init_OfferInfo("Guaranteed offer");
		
		InventManage_service = new InventManageService_Info();
		
		Expense_service = new SubExpense_Info();

		ServiceList = new ArrayList<Services_Info>();
		ServiceList.add(HHG_service);
		ServiceList.add(HP_service);

		ServiceList.add(HS_service);
		ServiceList.add(TQ_service);
		ServiceList.add(MG_service);
		ServiceList.add(Appraisal_service);
		ServiceList.add(HomeInspect_service);
		ServiceList.add(HBO_service);
		ServiceList.add(InventManage_service);
		ServiceList.add(Expense_service);
		
		ReimBursList = new ArrayList<ReimBurs_Info>();
		ReimBursList.add(new ReimBurs_Info());
		
		Policy = "";
		effectDate = "";
		System.out.println("Completed Initiate Transferee Info");
	}
		//METHOD
	
		public void ReGenerate_GeneralInfo_func()
		{
			Salutation = "Mr.";
			TransfereeID = "";
			Pronunciation = "";
			Nickname ="Nick for testing";
			Jobtitle = "PM for testing";
			CoordinatorAssistant = "";
			TeamLeader = "";
			UrgentInstructions = "This Instruction for testing";
			AddintionNote = "Addtional Note for testing";
			IsExecutiveOfficer = true;
			
			StatusChangeDate="";
			IsOverRide = true;
			IsQualified = true;
			//FileEnterDate = DateTime_Manage.GetCurrentLocalDate_Str();
			IsReceivedSignedABAD = true;
			ReferedListSide = true;
			ReferedBuySide = true;
			ReferedToTitleComp = true;
		}

		public void ReGenenrateInfo()
		{
			this.firstname ="Updated" + this.firstname;
			this.lastname = this.lastname + "";
			FullName = this.firstname + " " + this.lastname;
			Salutation = "Dr.";
			Pronunciation = "Director";
			Nickname ="tester";
			Jobtitle = "IT leader";
			Coordinator = "David Gibbons";
			CoordinatorAssistant = "Daniel Yang";
			TeamLeader = "David Gibbons";
			UrgentInstructions = "this is instruct testing";
			AddintionNote = "this is addnote testing";
			IsExecutiveOfficer = true;
			PhoneList = new ArrayList<Phone_Info>();
			PhoneList.add(new Phone_Info());

			MailList = new ArrayList<Mail_Info>();
			MailList.add(new Mail_Info());

			Linkedin_Profile = "http://" + this.Nickname + ".com";
			FB_Profile = "http://" + this.Nickname + ".com";
			
			TimeZone_country = "Africa/Accra (GMT)";
			
			new_password = retype_password = "123123123";
			
			//STATUS;
			Status = "Active";
			StatusChangeDate="";
			IsOverRide = false;
			IsQualified = true;
			IsReceivedSignedABAD = false;
			ReferedListSide = false;
			ReferedBuySide = false;
			ReferedToTitleComp = false;

			//PARTNER INFO
			PartnerFname = "Partner_UFname";
			PartnerLname = "Partner_ULname";
			PartnerMid = "";
			PartnerSSN = "123";
			//OTHERS' INFO
			Other_info = new Other_Info();
			OriResAddr_info = new AddrOriRes_Info();
			DesResAddr_info = new AddrDesRes_Info();
			OriOffAddr_info = new AddrOriOff_Info();
			DesOffAddr_info = new AddrDesOff_Info();

			//Bank_info = new BankInfo();
			ServiceList.add(new TQService_Info());

		}
	


		



	

}
