package businessObjects;

import java.util.Random;
import java.util.Date;
import custom_Func.DateTime_Manage;

public class Policy_Info{
	
	public String PolicyName;
	public String ActiveDate;
	public String ExpirationDate;
	public String Description;
	//public String ClientName;
	public String EnterDate;
	public Client_Info client;
	//public String ClientCompany;
	
	public Policy_Info(Client_Info client) {
		
		this.client  = client;
		
		Random r = new Random();
		int ret2 = r.nextInt(100)+1;		
		PolicyName = "Policy Title for Testing"+DateTime_Manage.getCurrentLocalTime()+Integer.toString(ret2);
		
		ActiveDate = DateTime_Manage.GetCurrentLocalDate_Str()+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
		
		Date dt = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),1);
		
		ExpirationDate = DateTime_Manage.ConvertDatetoString(dt)+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
		
		System.out.println("ExpirationDate: "+ExpirationDate);
		
		Description = "Policy-Content for Testing"+DateTime_Manage.getCurrentLocalTime();
		
		//ClientCompany = this.CompanyName;
		
		EnterDate = DateTime_Manage.ConvertDatetoString(dt);
		
	}
	
	
	
	
	public void ReGenerate_Policy() {
		
		this.PolicyName = "Update "+ this.PolicyName;
		
		Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua";
	
	}

}
