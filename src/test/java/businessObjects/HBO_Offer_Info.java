package businessObjects;

import java.util.Date;
import java.util.Random;


import custom_Func.DateTime_Manage;

public class HBO_Offer_Info {
	
	public String Type;
	public double Amount;
	public String Date;
	public String ExpireDate;
	public boolean IsSelcted;
	public String ContractDate;
	public String AcceptDate;
	public String VacatePeriod;
	
	public boolean Contigen_IsTitle;
	public String Contigen_Title_Note;
	public boolean Contigen_IsInspect;
	public String Contigen_Inspect_Note;
	public boolean Contigen_IsPersonProp;
	public String Contigen_PersonProp_Note;
	public boolean Contigen_IsInspectClear;
	public String  Contigen_InspectClear_Note;

	public HBO_Offer_Info(String type) {
		
		Type =type;//Guaranteed offer, Amended from zero offer, BVO fall through offer, Direct offer, 
		           // Amended offer, BVO offer

		Date Date_tmp = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),1);
		Random random = new Random();
		Amount = random.nextDouble();
		//Amount=3.00;
		Date  = DateTime_Manage.ConvertDatetoString(Date_tmp);
		ExpireDate = DateTime_Manage.ConvertDatetoString(Date_tmp);
		IsSelcted = false;
		ContractDate = DateTime_Manage.ConvertDatetoString(Date_tmp);
		AcceptDate = DateTime_Manage.ConvertDatetoString(Date_tmp);
		VacatePeriod = DateTime_Manage.ConvertDatetoString(Date_tmp);
		
		Contigen_IsTitle = true;
		Contigen_Title_Note = "TitleNote for Testing";
		Contigen_IsInspect = true;
		Contigen_Inspect_Note = "Inspect Note for Testing";
		Contigen_IsPersonProp = true;
		Contigen_PersonProp_Note = "Personal Property Note for Testing";
		Contigen_IsInspectClear = true;
		Contigen_InspectClear_Note = "Inspect Clear Note for testing";
	}

	
	


}
