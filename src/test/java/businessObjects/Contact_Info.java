package businessObjects;

import java.util.ArrayList;
import java.util.Date;


import custom_Func.DateTime_Manage;

public class Contact_Info extends UserAccount_Info{
	
	public Address_Info AddrInfo;


	public boolean IsDefault;//false;
	public String ServiceType; // Inspection Agents , Realtor - Agent, Van Lines, Realtor - Broker, Title and closing management company, Lender, Interim Housing
	public String VendorName;
	public String ClientName;

	public Mail_Info email_info;
	
	//Appraisal Info
	public String OrderDate;
	public String WalkThru;
	public String ApprsaiReceive;
	public String VerbalValue;
	public String SpecialInstruct;

	public Contact_Info() {
		// TODO Auto-generated constructor stub
		super();

		FullName = this.firstname+ " " + this.lastname;

		PhoneList = new ArrayList<>();
		PhoneList.add(new Phone_Info());

		AddrInfo = new Address_Info();

		email_info = new Mail_Info();
		email_info.EmailAddr = this.firstname +"@yopmail.com";
		Jobtitle = "IT leader";

		
		IsDefault = true;
		//HHG
		ServiceType = "";

		//HP-HS
		//ServiceType = "Realtor Broker"//Title

		//TQ
		//ServiceType = "Interim Housing"//Title

		//MORTGAGE
		//ServiceType = "Lender"//Title

		VendorName = "";
		ClientName = "";
		super.role = "";
	}
	
	
	public void Init_Vendor_Contact_Info(String vendorName)
	{
		//super.role = "";
		VendorName = vendorName;
		
		ServiceType = "Van Lines";
		
		AddrInfo.Addr1 = "20 Cali Street";
		
		AddrInfo.Addr2 = "50 High Road";
		
		AddrInfo.City = "Cali";
		
		AddrInfo.State  = "Texas";
		
		AddrInfo.Country  = "United States of America";
		
		AddrInfo.Zip = "88888";
	}
	
	
	public void Init_Client_Contact_Info(String clientname)
	{
		//super.role = "";
		ClientName = clientname;
		//ServiceType = "Van Lines";
	}
	
	
	public void Init_AppraiserInfo()
	{
		Date Date = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),1);
		
		OrderDate = WalkThru = ApprsaiReceive = DateTime_Manage.ConvertDatetoString(Date);
		VerbalValue="";
		SpecialInstruct="";
	}
	
	
	

}
