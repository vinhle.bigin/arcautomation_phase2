package businessObjects;



import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import dataDriven.glb_RefData;

public class AppraisalService_Info extends Services_Info{
	
	
	public Address_Info Addr_info;
	public Map<String, Double> SelectEst_List;
	
	public double EstimatedPrice_From;
	
	public double EstimatedPrice_To;
	
	public double ListPercent_From;
	
	public double ListPercent_To;
	
	public double ListPrice_From;
	
	public double ListPrice_To;
	
	public double EstimatedPrice_Average;
	
	public double VariantPercent;
	
	
	
	
	public String URL;
	public String User;
	public String Assistant;
	
	public AppraisalService_Info() {
		// TODO Auto-generated constructor stub
		super();
		Type = "Appraisal";
		Status = true;
		Addr_info = new Address_Info();
		SelectEst_List = new HashMap<String, Double>();
		URL = "";
		User = glb_RefData.Coordinator;
		Assistant = "Em Manager";
		
		
		
		
	}
	
	public AppraisalService_Info(Contact_Info appraiser_contact)
	{
		super();
		Type = "Appraisal";
		Status = true;
		
		Addr_info = new Address_Info();
		SelectEst_List = new HashMap<String, Double>();
		
		URL = "https://arc-uat.bigin.top/";
		User = "Linh Nguyen";
		Assistant = "Em Manager";
		VendorCtactList.add(appraiser_contact);
		
	}
	
	public void InIt_Appraisal_Analysis()
	{
		
		for(Contact_Info ctact: VendorCtactList)
		{
			Double saleprice = Double.valueOf(new Random().nextInt(500));
			
			SelectEst_List.put(ctact.FullName, saleprice);
		}
		
		ListPercent_From = 20;
		
		ListPercent_To = 40;
		//sale_price = NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(sale_price);
		
	}
	
	public void Calculate_Appraisal_Analysis()
	{
		//Estimated Price Variance Percentage: 
		//(the highest selected estimation - the lowest selected estimation) * 100% / the highest selected estimation
		
		EstimatedPrice_From = Collections.min(SelectEst_List.values());
		
		EstimatedPrice_To = Collections.max(SelectEst_List.values());
		
		int size = SelectEst_List.size();
		
		System.out.println(EstimatedPrice_From +" - "+ EstimatedPrice_To);
		
		EstimatedPrice_Average = (EstimatedPrice_From + EstimatedPrice_To)/size;
		System.out.println("EstimatedPrice_Average: "+EstimatedPrice_Average);
		
		VariantPercent = (EstimatedPrice_To -EstimatedPrice_From)/EstimatedPrice_To;
		
		System.out.println("VariantPercent: "+String.format("%.2f", VariantPercent));
		
		ListPrice_From = EstimatedPrice_Average*ListPercent_From/100;
		System.out.println("ListPrice_From: "+String.format("%.2f", ListPrice_From));
		
		ListPrice_To = EstimatedPrice_Average*ListPercent_To/100;
		System.out.println("ListPrice_To: "+String.format("%.2f", ListPrice_To));
	}

}
