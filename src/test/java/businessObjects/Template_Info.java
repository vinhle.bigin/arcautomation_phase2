	package businessObjects;

import custom_Func.DateTime_Manage;
import dataDriven.glb_RefData;

public class Template_Info {
	public String Name;

	public String Type;
	public String Target;
	public String ServiceType;
	public String EmailFrom;
	public String Description;
	public String Subject;
	public String Content;
	public String DocName;
	public Boolean IsActive;
	public String CodeField_Content_str;
	public String DocProcessType;

	public Template_Info() {
		// TODO Auto-generated constructor stub
		Type = "Email";
		Target = "Transferee";
		ServiceType = "General";
		
		
		EmailFrom = glb_RefData.Coordinator;//"Assistant";
		
		
		DateTime_Manage dt = new DateTime_Manage();
		Name = Type+ "-Template_" + dt.getCurrentLocalTime();
		Subject = "QA_Testing"+ Name;
		Content = "This is for testing.";
		IsActive=true;

		DocName = "";//"Email_Template_Doc.docx"
		DocProcessType = "DOCX" ;//"PDF";
	}
	
	
	public void Generate_Client_Template_func()
	{
		Type = "Email";
		Target = "Client";
		ServiceType = "General";
			
		EmailFrom = glb_RefData.glb_ARC_FullName;//"Assistant";
		
		DateTime_Manage dt = new DateTime_Manage();
		Name = Type+ "-Template_" + dt.getCurrentLocalTime();
		Subject = "QA_Testing"+ Name;
		Content = "This is for testing.";
		IsActive=true;

		DocName = "";//"Email_Template_Doc.docx"
		DocProcessType = "DOCX" ;//"PDF";
	}
	
	public void Generate_Vendor_Template_func()
	{
		Type = "Email";
		Target = "Vendor";
		ServiceType = "Home sale";
		
		
		EmailFrom = glb_RefData.glb_ARC_FullName;//"Assistant";
		
		
		DateTime_Manage dt = new DateTime_Manage();
		Name = Type+ "-Template_" + dt.getCurrentLocalTime();
		Subject = "QA_Testing"+ Name;
		Content = "This is for testing.";
		IsActive=true;

		DocName = "";//"Email_Template_Doc.docx"
		DocProcessType = "DOCX" ;//"PDF";
	}
	


		//==this method to handle filling the code field into Template Content text field
		public void AddFieldCode_TransfGenInfo_func()
		{
			String Content_str = "";

			//Content_str += "Trans ID: ${transferee=id}\n"
			Content_str += "Trans FName: ${transferee=first_name}\n";
			Content_str += "Trans LName: ${transferee=last_name}\n";
			Content_str += "Trans FullName: ${transferee=full_name}\n";
			//Content_str += "Trans JobTitle: \\n"
			Content_str += "Trans Coordinator: ${coordinator=fullname}\n";
			Content_str += "Trans Coor Assistant: ${coordinator_assistant=fullname}\n";
			Content_str += "Client Name: ${transferee=client}\n";
			//Content_str += "Client Cotanct: \\n"
			Content_str += "Trans Status: ${transferee=status}\n";
			//Content_str += "Trans Status Change Date: \n"
			//Content_str += "Trans IsOveride: \n"
			//Content_str += "Trans IsQualified: \n"
			//Content_str += "Trans FileEnter CreateDate: \n"
			Content_str += "Trans IsReceiveSignABAD: ${transferee=is_received_signed_abad}\n";
			Content_str += "Trans IsReferListSide: ${transferee=is_referred_list_side}\n";
			Content_str += "Trans IsReferBuySide: ${transferee=is_referred_buy_side}\n";
			Content_str += "Trans IsReferToTitleComp: ${transferee=is_referred_to_title_company}\n";
			Content_str += "Trans Mobile PhoneNumb: ${transferee=mobile_phone}\n";
			Content_str += "Trans WorkEmail: ${transferee=work_email}";

			this.Content = Content_str;


		}


		//Add Fieldcode of Transferee Other Info into Content
		public void AddFieldCode_TransfOtherInfo_func()
		{
			String Content_str = "";

			Content_str+="MoveType: ${transferee=move_type}";

			Content_str+="\nInItDate: ${transferee=initiation_date}";

			Content_str+="\nEffectDate: ${transferee=effective_date}";

			Content_str+="\nMoveInDate: ${transferee=interim_move_in_date}";

			Content_str+="\nMoveOutDate: ${transferee=interim_move_out_date}";

			Content_str+="\nExpectMoveDate: ${transferee=expected_move_date}";

			Content_str+="\nMoveEnd: ${transferee=move_end_date}\n";


			this.Content = Content_str;


		}


		//Add Fieldcode of Transferee Other Info into Content
		public void AddFieldCode_TransfAddrInfo_func()
		{
			String Content_str = "";

			Content_str+= "OrgRes_Addr1: ${transferee=origin_residence_address1}";

			Content_str+= "\nOrgRes_Addr2: ${transferee=origin_residence_address2}";

			Content_str+= "\nOrgRes_City: ${transferee=origin_residence_city}";

			Content_str+= "\nOrgRes_State: ${transferee=origin_residence_state}";

			Content_str+= "\nOrgRes_Zip: ${transferee=origin_residence_zipcode}";

			Content_str+= "\nOrgRes_Country: ${transferee=origin_residence_country}";

			//Orginal Office Info
			Content_str+= "\nOrgOff_Addr1: ${transferee=origin_office_address1}";

			Content_str+= "\nOrgOff_Addr2: ${transferee=origin_office_address2}";

			Content_str+= "\nOrgOff_City: ${transferee=origin_office_city}";

			Content_str+= "\nOrgOff_State: ${transferee=origin_office_state}";

			Content_str+= "\nOrgOff_Zip: ${transferee=origin_office_zipcode}";

			Content_str+= "\nOrgOff_Country: ${transferee=origin_office_country}";

			//Destination Residient Info
			Content_str+= "\nDesRes_Addr1: ${transferee=destination_office_address1}";

			Content_str+= "\nDesRes_Addr2: ${transferee=destination_office_address2}";

			Content_str+= "\nDesRes_City: ${transferee=destination_office_city}";

			Content_str+= "\nDesRes_State: ${transferee=destination_office_state}";

			Content_str+= "\nDesRes_Zip: ${transferee=destination_office_zipcode}";

			Content_str+= "\nDesRes_Country: ${transferee=destination_office_country}";

			//Destionation Office Info
			Content_str+= "\nDesOff_Addr1: ${transferee=destination_residence_address1}";

			Content_str+= "\nDesOff_Addr2: ${transferee=destination_residence_address2}";

			Content_str+= "\nDesOff_City: ${transferee=destination_residence_city}";

			Content_str+= "\nDesOff_State: ${transferee=destination_residence_state}";

			Content_str+= "\nDesOff_Zip: ${transferee=destination_residence_zipcode}";

			Content_str+= "\nDesOff_Country: ${transferee=destination_residence_country}";

			this.Content = Content_str;


		}//end void

			
		
		public void AddFieldCode_ApprisalInfo_func(Services_Info service_tmp)
		{
			String Content_str = "";
			Content_str+= "\nAppraisal Estimate 1: ${service_appraisal=appraisal_estimate_1}";
			Content_str+= "\nAppraisal Estimate 2: ${service_appraisal=appraisal_estimate_2}";
			Content_str+= "\nAppraisal Estimate 3: ${service_appraisal=appraisal_estimate_3}";
			Content_str+= "\nAppraisal Estimate 4: ${service_appraisal=appraisal_estimate_4}";
			Content_str+= "\nAppraisal Estimate 5: ${service_appraisal=appraisal_estimate_5}";
			
			Content_str+= Add_FieldCode_Transf_Service_Info_func(service_tmp);
			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void
		
		public void AddFieldCode_HomeInspectInfo_func(Services_Info service_tmp)
		{
			String Content_str = "";
			
			Content_str+= "\nHomeInspect Specialist Assistant Full Name: ${service_home_inspection=home_inspection_specialist_assistant_first_name} ";
			Content_str+= "${service_home_inspection=home_inspection_specialist_assistant_last_name}";


			Content_str+= Add_FieldCode_Transf_Service_Info_func(service_tmp);
			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void
		
		
		public void AddFieldCode_InventManageInfo_func(Services_Info service_tmp)
		{
			String Content_str = "";
			
			Content_str+= "IM List Price: ${service_inventory_management=inventory_management_list_price}";
			Content_str+= "\nIM Start Listing Date: ${service_inventory_management=inventory_management_start_listing_date}";
			Content_str+= "\nIM End Listing Date: ${service_inventory_management=inventory_management_end_listing_date}";
			Content_str+= "\nIM Sale Price: ${service_inventory_management=inventory_management_sale_price}";
			Content_str+= "\nIM Closing Date: ${service_inventory_management=inventory_management_closing_date}";
			Content_str+= "\nIM Listing Link: ${service_inventory_management=inventory_management_listing_link}";
			Content_str+= "\nIM Average Days on Market: ${service_inventory_management=inventory_management_average_days_on_market}";
			Content_str+= "\nIM Property Price Range From: ${service_inventory_management=inventory_management_property_price_range_from}";
			Content_str+= "\nIM Property Price Range To: ${service_inventory_management=inventory_management_property_price_range_to}";
			Content_str+= "\nIM Suggested Selling Price: ${service_inventory_management=inventory_management_suggested_selling_price}";
			Content_str+= "\nIM Suggested List Price: ${service_inventory_management=inventory_management_suggested_list_price}";


			Content_str+= Add_FieldCode_Transf_Service_Info_func(service_tmp);
			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void
		
		public void AddFieldCode_HBOInfo_func(HBOService_Info hbo_service)
		{
			String Content_str = "";
			for(HBO_Offer_Info cur_offer:hbo_service.Offer_list)
			{
				switch(cur_offer.Type)
				{
				case "Guaranteed offer":
					Content_str+= "\nHBO Guaranteed Offer Amount: ${service_home_buyout=home_buyout_guaranteed_offer_amount}";
					Content_str+= "\nHBO Guaranteed Offer Date: ${service_home_buyout=home_buyout_guaranteed_offer_date}";
					Content_str+= "\nHBO Guaranteed Offer Expiration Date: ${service_home_buyout=home_buyout_guaranteed_offer_expiration_date}";
				
					break;
				
				case "Amended from zero offer":
					Content_str+= "\nHBO Amended from zero Offer Amount: ${service_home_buyout=home_buyout_amended_from_zero_offer_amount}";
					Content_str+= "\nHBO Amended from zero Offer Date: ${service_home_buyout=home_buyout_amended_from_zero_offer_date}";
					Content_str+= "\nHBO Amended from zero Offer Expiration Date: ${service_home_buyout=home_buyout_amended_from_zero_offer_expiration_date}";
					
					break;
				
				case "BVO fall through offer":
					Content_str+= "\nHBO BVO Fall Through Offer Amount: ${service_home_buyout=home_buyout_bvo_fall_through_offer_amount}";
					Content_str+= "\nHBO BVO Fall Through Offer Date: ${service_home_buyout=home_buyout_bvo_fall_through_offer_date}";
					Content_str+= "\nHBO BVO Fall Through Offer Expiration Date: ${service_home_buyout=home_buyout_bvo_fall_through_offer_expiration_date}";
										
					break;
				
				case "Direct offer":
					Content_str+= "\nHBO Directed Offer Amount: ${service_home_buyout=home_buyout_directed_offer_amount}";
					Content_str+= "\nHBO Directed Offer Date: ${service_home_buyout=home_buyout_directed_offer_date}";
										
					
				case "BVO offer":
					Content_str+= "\nHBO BVO Offer Amount: ${service_home_buyout=home_buyout_bvo_offer_amount}";
					Content_str+= "\nHBO BVO Offer Date: ${service_home_buyout=home_buyout_bvo_offer_date}";
					Content_str+= "\nHBO BVO Offer Expiration Date: ${service_home_buyout=home_buyout_bvo_offer_expiration_date}";
					
					break;
				
				case "Amended offer":
					Content_str+= "\nHBO Amended Offer Amount: ${service_home_buyout=home_buyout_amended_offer_amount}";
					Content_str+= "\nHBO Amended Offer Date: ${service_home_buyout=home_buyout_amended_offer_date}";
					Content_str+= "\nHBO Amended Offer Expiration Date: ${service_home_buyout=home_buyout_amended_offer_expiration_date}";
					
					
					break;
				}//end switch
				
				Content_str+= "\nHBO Contract Date: ${service_home_buyout=home_buyout_contract_date}";
				Content_str+= "\nHBO Acceptance Date: ${service_home_buyout=home_buyout_acceptance_date}";
				Content_str+= "\nHBO Vacate Period: ${service_home_buyout=home_buyout_vacate_period}";
			}
						
			Content_str+= "\nHBO Acquit Type: ${service_home_buyout=home_buyout_acquisition_types}";
			Content_str+= "\nHBO Acquit Date: ${service_home_buyout=home_buyout_acquisition_date}";
			Content_str+= "\nHBO Resale Amount: ${service_home_buyout=home_buyout_resale_amount}";
			Content_str+= "\nHBO Resale Date: ${service_home_buyout=home_buyout_resale_date}";
			
			Content_str+= "\nHBO Special Instruct: ${service_home_buyout=home_buyout_special_instructions}";
			Content_str+= "\nHBO Special Full Name: ${service_home_buyout=home_buyout_specialist_first_name} ${service_home_buyout=home_buyout_specialist_last_name}";
			Content_str+= "\nHBO Special Assistant Full Name: ${service_home_buyout=home_buyout_specialist_assistant_first_name} "+"${service_home_buyout=home_buyout_specialist_assistant_last_name}";
		
			Content_str+= "\nHBO Actic Equit Amount: ${service_home_buyout=home_buyout_anticipated_equity_amount}";
			Content_str+= "\nHBO Actic Equit Date: ${service_home_buyout=home_buyout_anticipated_equity_date}";
			
			Content_str+= "\nHBO Actic Equit Proceess Return Amount: ${service_home_buyout=home_buyout_anticipated_equity_proceeds_returned_amount}";
			Content_str+= "\nHBO Actic Equit Proceess Return Date: ${service_home_buyout=home_buyout_anticipated_proceed_return_date}";
			
			Content_str+= "\nHBO Actual Equit Amount Send: ${service_home_buyout=home_buyout_actual_equity_sent_to_ee}";
			Content_str+= "\nHBO Actual Equit Date Send: ${service_home_buyout=home_buyout_actual_date_equity_sent_to_ee}";
			Content_str+= "\nHBO Actual Equit Proceed Return Amount: ${service_home_buyout=home_buyout_actual_equity_proceeds_returned_amount}";
			Content_str+= "\nHBO Actual Equit Proceed Return Date: ${service_home_buyout=home_buyout_actual_date_equity_proceeds_returned}";
			Content_str+= "\nHBO Comment: ${service_home_buyout=home_buyout_comments}";
			Content_str+= "\nHBO Actual Proceed Sent EE: ${service_home_buyout=home_buyout_actual_date_equity_sent_to_ee}";
			

			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void

		public void AddFieldCode_TransfHomeSaleInfo_func(Services_Info service_tmp)
		{
			String Content_str = "";

			Content_str+= "HS_TookBuyOut: ${service_home_sale=took_buyout}";
			Content_str+= "\nHS_ListPrice: ${service_home_sale=list_price}";
			Content_str+= "\nHS_CommissionPercent: ${service_home_sale=commission_percent}";
			Content_str+= "\nHS_ListStartDate: ${service_home_sale=listing_start_date}";
			Content_str+= "\nHS_ListEndDate: ${service_home_sale=listing_end_date}";
			Content_str+= "\nHS_ClosingDate: ${service_home_sale=closing_date}";
			Content_str+= "\nHS_RebateAmount: ${service_home_sale=rebate_amount}";
			Content_str+= "\nHS_DateBuyOutAccept: ${service_home_sale=date_buy_out_accepted}";
			//Content_str+= "\nHS_EscrowFeePercent: ${service_home_sale=escrow_fee_percent}";
			//Content_str+= "\nHS_EscowFeeAmount: ${service_home_sale=escow_fee_ammount}";
			Content_str+= "\nHS_Rebate: ${service_home_sale=rebate}";
			//Content_str+= "\nHS_IsInvoiceCreate: ${service_home_sale=is_invoice_created}";
			//Content_str+= "\nHS_InvoiceClosedOut: ${service_home_sale=is_invoice_closed_out}";
			Content_str+= "\nHS_ReffeeReceived: ${service_home_sale=referral_fee_received}";
			Content_str+= "\nHS_SignedRefFeeAgree: ${service_home_sale=signed_referral_fee_agreeement}";
			
			Content_str+= Add_FieldCode_Transf_Service_Info_func(service_tmp);
			
			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void


		public void AddFieldCode_TransfHomePurchaseInfo_func(Services_Info service_tmp)
		{
			String Content_str = "";
			Content_str+= "HP_IsSignRefFeeAgree: ${service_home_purchase=is_signed_referral_fee_agreement}";
			Content_str+= "\nHP_SalePrice: ${service_home_purchase=sale_price}";
			Content_str+= "\nHP_LoanAmount: ${service_home_purchase=loan_amount}";
			Content_str+= "\nHP_Commission: ${service_home_purchase=commission}";
			Content_str+= "\nHP_RebateAmount: ${service_home_purchase=rebate_amount}";
			Content_str+= "\nHP_CloseDate: ${service_home_purchase=closing_date}";
			Content_str+= "\nHP_HuntTripDate: ${service_home_purchase=house_hunting_trip_date}";
			Content_str+= "\nHP_EscrowFeeAmount: ${service_home_purchase=escrow_fee_amount}";
			Content_str+= "\nHP_EscrowfePercent: ${service_home_purchase=escrow_fee_percent}";
			Content_str+= "\nHP_IsRebateSent: ${service_home_purchase=is_rebate_sent}";
			Content_str+= "\nHP_IsInvoiceCloseOut: ${service_home_purchase=is_invoice_closed_out}";
			Content_str+= "\nHP_IsEscrowFeeReceive: ${service_home_purchase=is_escrow_fee_received}";
			Content_str+= "\nHP_IsInvoiceCreate: ${service_home_purchase=is_invoice_created}";
			Content_str+= "\nHP_IsTransfereeRent: ${service_home_purchase=is_transferee_renting}";
			
			Content_str+= Add_FieldCode_Transf_Service_Info_func(service_tmp);

			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void


		public void AddFieldCode_HHG_Info_func(Services_Info service_tmp)
		{
			String Content_str = "";
			Content_str+= "HHG_Order_Desr: ${service_hhg_order=order_description}";
			Content_str+= "\nHHG Order Tariff Code: ${service_hhg_order=tariff_code}";
			Content_str+= "\nHHG Order Service Type: ${service_hhg_order=hhg_service_type}";
			Content_str+= "\nHHG Order Pack Date Actual End: ${service_hhg_order=packing_date_actual_end}";
			Content_str+= "\nHHG Order Load Date Actual End: ${service_hhg_order=loading_date_actual_end}";
			Content_str+= "\nHHG Order Deliver Date Actual End: ${service_hhg_order=deliver_date_actual_end}";
			Content_str+= "\nHHG Order Actual Storage Weight: ${service_hhg_order=actual_storage_weight}";
			Content_str+= "\nHHG Order Actual Ship Weight: ${service_hhg_order=total_actual_shipment_weight}";
			Content_str+= "\nHHG Order Vendor Name: ${service_hhg_order=vendor_vanline_selected_name}";
			Content_str+= "\nHHG Order Total Service Charge: ${service_hhg_order=total_service_charge}";
			Content_str+= "\nHHG Order Override Amount: ${service_hhg_order=override_amount}";
			Content_str+= "\nHHG Order Total Invoice Client: ${service_hhg_order=total_invoice_to_client}";
			Content_str+= "\nHHG Order Total Bill Mover: ${service_hhg_order=total_bill_to_mover}";
			Content_str+= "\nHHG Order Date Bill Client: ${service_hhg_order=date_billed_to_client}";
			Content_str+= "\nHHG Order Payment Receive Date: ${service_hhg_order=date_received_payment_from_client}";
			Content_str+= "\nHHG Order Invoice Submit Date: ${service_hhg_order=date_invoice_submitted}";

			System.out.println(Content_str);
			
			this.Content = Content_str;
		}//end void


		public void AddFieldCode_TransfMortgageInfo_func(Services_Info service_tmp)
		{
			String Content_str = "";
			Content_str+= "MG_mortgage_amount: ${service_mortgage=mortgage_amount}";
			Content_str+= "\nMG_index_rate: ${service_mortgage=index_rate}";
			Content_str+= "\nMG_amt_needed_at_closing: ${service_mortgage=amt_needed_at_closing}";
			Content_str+= "\nMG_down_payment: ${service_mortgage=down_payment}";
			Content_str+= "\nMG_closing_date: ${service_mortgage=closing_date}";
			
			Content_str+= Add_FieldCode_Transf_Service_Info_func(service_tmp);
						
			System.out.println(Content_str);

			this.Content = Content_str;
		}//end void


		public void AddFieldCode_TransfTempQuarterInfo_func(Services_Info service_tmp)
		{
			String Content_str = "";
			Content_str+= "TQ_move_out_date: ${service_temporary_quarter=move_out_date}";
			Content_str+= "\nTQ_move_in_date: ${service_temporary_quarter=move_in_date}";
			Content_str+= "\nTQ_pet: ${service_temporary_quarter=pet}";
			Content_str+= "\nTQ_apartment_type: ${service_temporary_quarter=apartment_type}";
			Content_str+= "\nTQ_baths: ${service_temporary_quarter=baths}";
			Content_str+= "\nTQ_budget: ${service_temporary_quarter=budget}";
			Content_str+= "\nTQ_payment_type: ${service_temporary_quarter=payment_type}";
			Content_str+= "\nTQ_escrow_fee_received: ${service_temporary_quarter=escrow_fee_received}";
			Content_str+= "\nTQ_invoice_created: ${service_temporary_quarter=invoice_created}";
			Content_str+= "\nTQ_invoice_closed_out: ${service_temporary_quarter=invoice_closed_out}";
			
			Content_str+= Add_FieldCode_Transf_Service_Info_func(service_tmp);
			
			System.out.println(Content_str);

			this.Content = Content_str;
		}//end void


		//ADD FIELD CODE OF CLIENT INFO
		public void AddFieldCode_ClientInfo_func()
		{
			String Content_str = "";
			Content_str+= "company_name: ${client=company_name}";
			//Content_str+= "\ncontact_salutation: ${client=contact_salutation}";
			Content_str+= "\ncontact_first_name: ${client=contact_first_name}";
			//Content_str+= "\ncontact_mid_name: ${client=contact_mid_name}";
			Content_str+= "\ncontact_last_name: ${client=contact_last_name}";
			Content_str+= "\ncontact_full_name: ${client=contact_full_name}";
			Content_str+= "\ncontact_phone_number: ${client=contact_phone_number}";
			Content_str+= "\ncontact_email: ${client=contact_email}";
			Content_str+= "\ncontact_address1: ${client=contact_address1}";
			//Content_str+= "\ncontact_address2: ${client=contact_address2}";
			Content_str+= "\ncontact_city: ${client=contact_city}";
			Content_str+= "\ncontact_state: ${client=contact_state}";
			Content_str+= "\ncontact_zip: ${client=contact_zip}";
			Content_str+= "\ncontact_country: ${client=contact_country}";


			this.Content = Content_str;
		}//end void


		//ADD FIELD CODE OF CLIENT INFO
		public void AddFieldCode_VendorInfo_func()
		{
			String Content_str = "";
			Content_str+= "vendor_name: ${vendor=vendor_name}";
			Content_str+= "\nvendorcontact_first_name: ${vendor=contact_first_name}";
			Content_str+= "\nvendorcontact_last_name: ${vendor=contact_last_name}";
			Content_str+= "\nvendorcontact_full_name: ${vendor=contact_full_name}";
			Content_str+= "\nvendorcontact_phone_number: ${vendor=contact_phone_number}";
			Content_str+= "\nvendorcontact_email: ${vendor=contact_email}";
			Content_str+= "\nvendorcontact_address1: ${vendor=contact_address1}";
			Content_str+= "\nvendorcontact_city: ${vendor=contact_city}";
			Content_str+= "\nvendorcontact_state: ${vendor=contact_state}";
			Content_str+= "\nvendorcontact_zip: ${vendor=contact_zip}";
			Content_str+= "\nvendorcontact_country: ${vendor=contact_country}";

			this.Content = Content_str;
		}//end void


		//ADD FIELD CODE OF CUSTOM FIELD
		public void AddFieldCode_CustomField_func(CustomField_Info customfield)
		{
			String Content_str = "";
			Content_str+= customfield.FieldName+": ${ctf_"+customfield.FieldID+"_"+customfield.FieldName+"}";


			this.Content = Content_str;
		}//end void


		private String Add_FieldCode_Transf_Service_Info_func(Services_Info service_tmp)
		{
			//Contact info can be null;
			
			String Content_str="";
			
			String prefix_servicetype = "\n"+service_tmp.Type;
			
			
			if(service_tmp.VendorCtactList.size()>0)
			{
				Contact_Info contact = service_tmp.VendorCtactList.get(0);
				
				prefix_servicetype+="Selected" +contact.ServiceType;
						
				//for Contact Service Type Relo
				String para_service_name ="";
				
					
				String hs_type = new HSService_Info().Type;
				String hp_type = new HPService_Info().Type;
				String hhg_type = new HHGService_Info().Type;
				String tq_type = new TQService_Info().Type;
				String mg_type = new MGService_Info().Type;
				String hbo_type = new HBOService_Info().Type;
				String appr_type = new AppraisalService_Info().Type;
				String inspect_type = new HomeInspectService_Info().Type;
				String invent_type = new InventManageService_Info().Type;
				
				System.out.println("MG type = "+mg_type);
				
								
				if(service_tmp.Type.equals(hs_type))
					para_service_name = "service_home_sale";
				
				else if(service_tmp.Type.equals(hp_type))
					para_service_name = "service_home_purchase";
				
				else if(service_tmp.Type.equals(hhg_type))
					para_service_name = "service_home_purchase";
				
				else if(service_tmp.Type.equals(tq_type))
					para_service_name = "service_temporary_quarter";
					
				else if(service_tmp.Type.equals(mg_type))
					para_service_name = "service_mortgage";
				
				else if(service_tmp.Type.equals(hbo_type))
					para_service_name = "service_home_buyout";
				
				else if(service_tmp.Type.equals(appr_type))
				{
					para_service_name = "service_appraisal";
					}
					
				
				else if(service_tmp.Type.equals(inspect_type))
				{
					para_service_name = "service_home_inspection";
										
				}
					
				else if(service_tmp.Type.equals(invent_type))
				{
					para_service_name = "service_inventory_management";
				}
					
				
				
				//To generate SelectContactVendor based on Contact SErvice type
				//=============SAMPLE:
				/*
				${service_appraisal=appraiser_selected_contact_last_name}

				${service_appraisal=appraisal_management_selected_company}

				${service_inventory_management=inventory_management_selected_broker_contact_first_name}

				${service_inventory_management=inventory_management_selected_broker_company}
				*/

				
				//Modify for only Inventory Management
				String para_ctact_service_name="selected_<type>";
				if(service_tmp.Type.equals(invent_type))
				{
					para_ctact_service_name = para_ctact_service_name.replace("selected", "inventory_management_selected");
					
				}
				
				if(contact.ServiceType.contains("agent")||contact.ServiceType.contains("Realtor - Agent"))
				{
					para_ctact_service_name = para_ctact_service_name.replace("<type>", "agent");
				}
				
				else if(contact.ServiceType.contains("Broken"))
				{
					para_ctact_service_name = para_ctact_service_name.replace("<type>", "relo_director");
				}
				
				else if(contact.ServiceType.contains("title"))
				{
					para_ctact_service_name = para_ctact_service_name.replace("<type>", "title/closing");
				}
				
				else if(contact.ServiceType.contains("Appraisal"))
				{
					
					para_ctact_service_name = para_ctact_service_name.replace("selected", "appraisal_management_selected");
					para_ctact_service_name = para_ctact_service_name.replace("<type>", "");
					
				}
				
				else if(contact.ServiceType.contains("Appraiser"))
				{
				
					para_ctact_service_name = para_ctact_service_name.replace("selected", "appraiser_selected");
					para_ctact_service_name = para_ctact_service_name.replace("<type>", "");
				}
				
				else if(contact.ServiceType.contains("Inspection Agent"))
				{
					para_ctact_service_name = para_ctact_service_name.replace("selected", "home_inspection_selected");
					para_ctact_service_name = para_ctact_service_name.replace("<type>", "");
				}
				
				else
				{
					para_ctact_service_name = para_ctact_service_name.replace("_<type>", "");
				}
				
				//Selected Vendor Company, Contact
				String FCode_vendorname="";
				String FCode_ctact_fname="";
				String FCode_ctact_lname="";
				String FCode_ctact_fullname="";
				String FCode_ctact_phonename="";
				String FCode_ctact_emailname="";
				
				 if(service_tmp.Type.equals(invent_type)
					||contact.ServiceType.contains("Appraiser")
					||contact.ServiceType.contains("Inspection Agent")	 
					||contact.ServiceType.contains("Appraisal")
					
					)
				 {
					 	FCode_vendorname = "${"+para_service_name+"="+para_ctact_service_name+"_company}";
						FCode_ctact_fname = "${"+para_service_name+"="+para_ctact_service_name+"_contact_first_name}";
						FCode_ctact_lname = "${"+para_service_name+"="+para_ctact_service_name+"_contact_last_name}";
						FCode_ctact_fullname ="${"+para_service_name+"="+para_ctact_service_name+"_vendor_contact_fist_last}";
						
				}//end if
				 
				 else
				 {
					 	FCode_vendorname = "${"+para_service_name+"="+para_ctact_service_name+"_vendor_name}";
						FCode_ctact_fname = "${"+para_service_name+"="+para_ctact_service_name+"_vendor_contact_first_name}";
						FCode_ctact_lname = "${"+para_service_name+"="+para_ctact_service_name+"_vendor_contact_last_name}";
						FCode_ctact_fullname ="${"+para_service_name+"="+para_ctact_service_name+"_vendor_contact_full_name}";
						FCode_ctact_phonename = "${"+para_service_name+"="+para_ctact_service_name+"_vendor_contact_phone}";
						FCode_ctact_emailname = "${"+para_service_name+"="+para_ctact_service_name+"_vendor_contact_email}";
				 }//else
				
							
				Content_str+= "\n"+prefix_servicetype+"VendorName: "+FCode_vendorname;
				Content_str+= "\n"+prefix_servicetype+"VendorContactFirstName: "+FCode_ctact_fname;
				Content_str+= "\n"+prefix_servicetype+"VendorContactLastName: "+FCode_ctact_lname;
				Content_str+= "\n"+prefix_servicetype+"VendorContactFullName: "+FCode_ctact_fullname;
				Content_str+= "\n"+prefix_servicetype+"VendorContactPhone: "+FCode_ctact_phonename;
				Content_str+= "\n"+prefix_servicetype+"VendorContactEmail: "+FCode_ctact_emailname;
						
				
			}//end if size>0
			
			return Content_str;
		}



}
