package businessObjects;
import java.util.ArrayList;
import java.util.List;

public class Vendor_Info extends UserAccount_Info{
	
	public String VendorName;
	public String ContactName;
	public String Status;
	public String VendorContactType;
	public String VendorCode;
	public String Website;
	public String VendorCategories;
	public String Ownership;
	public List<Contact_Info> Contacts;
	
	public boolean SearchOpt_IsZip ;
	
	public boolean SearchOpt_IsUSA;
	
	public boolean SearchOpt_IsInter;
	
	public List<String> SearchOpt_ZipList;
	
	public List<String> SearchOpt_CityList;
	
	public List<String> SearchOpt_StateList;
	
	public boolean SearchOpt_IsPrefered;
	
	public Vendor_Info() {
		// TODO Auto-generated constructor stub
		super();
		
		Status = "Active"; //"Available" | "International"
		VendorName = "Vendor_"+this.firstname;
		VendorCode = "";
		VendorContactType = "Van Lines";
		Ownership = "";
		Website = "";
		VendorCategories = "";
		
		//add new contact data to the list
		Contacts = new ArrayList<>();
		
		Contact_Info contact_tmp = new Contact_Info();
		
		contact_tmp.Init_Vendor_Contact_Info(VendorName);
		
		Contacts.add(contact_tmp);
		
		
		SearchOpt_IsZip = false;
				
		SearchOpt_IsUSA = false;
		
		SearchOpt_IsInter = false;
		
		SearchOpt_ZipList = new ArrayList<>();
		
		SearchOpt_IsPrefered = false;
		
		SearchOpt_CityList  = new ArrayList<>();
		SearchOpt_StateList  = new ArrayList<>();

	}
	
	public void ReGenerateInfo()
	{
		//UserAccount_Info u_tmp = new UserAccount_Info();
		VendorName = "Update " + "Vendor_"+this.firstname;
		Status = "Inactive";
		VendorContactType = "Appraiser";
	}
	


}