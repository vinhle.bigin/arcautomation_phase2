
package dataDriven;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;

import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import businessObjects.Transferee_Info;
import businessObjects.UserAccount_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;



public class glb_RefData {
	
	public static Element xml_rootelement;
	
	public  static String ARCPortal ;
	public  static String TransfereePortal;
	public  static String VendorPortal;
	public  static String ClientPortal;
	
	
	//Login information
	public  static String glb_ARC_username;
	public  static String glb_ARC_pass;
	public  static String glb_ARC_FullName;
	public  static String glb_ARCUser_TimeZone;
	public  static String glb_TestMailAccount;
	public  static String glb_TestMailPass;
	
	public  static String Coordinator;
	
	public  static String CoordAssistant;
	
	public  static String TeamLead;
	
	public  static String Template;
	
	public  static String Smarttask;
	
	public  static String glb_EmailSender;
	
	public  static Transferee_Info Transferee;
	
	public  static Vendor_Info Vendor;
	
	public  static Client_Info Client;
	
	public static UserAccount_Info User;
	
	private  static List<Contact_Info> Vendor_Ctacts;
	
	public static String SupportEmail;
	
	private static String file_direction = TestConfigs.resources_path+ "TestData"+File.separator; 
	
	private static String file_name;
	
	
	public glb_RefData() {
		
	
	}//end void
	
	
	public static void getGlbRefInfo()
	{
		//Get URLs of the portals
		ARCPortal = xml_rootelement.getChildText("ARCPortal");
		TransfereePortal = xml_rootelement.getChildText("TransfereePortal");
		VendorPortal = xml_rootelement.getChildText("VendorPortal");
		ClientPortal = xml_rootelement.getChildText("ClientPortal");
			
		//Get Login Account Info
		glb_ARC_username = xml_rootelement.getChildText("glb_ARC_username");
		glb_ARC_pass = xml_rootelement.getChildText("glb_ARC_pass");
		glb_ARC_FullName = xml_rootelement.getChildText("glb_ARC_FullName");
		glb_ARCUser_TimeZone = xml_rootelement.getChildText("glb_ARCUser_TimeZone");
			
			
		//3. Get Email Account for testing
		glb_TestMailAccount = xml_rootelement.getChildText("glb_TestMailAccount");
		glb_TestMailPass = xml_rootelement.getChildText("glb_TestMailPass");
		
		Coordinator = xml_rootelement.getChildText("Coordinator");
		CoordAssistant = xml_rootelement.getChildText("CoordAssistant");
		TeamLead = xml_rootelement.getChildText("TeamLead");
		Template = xml_rootelement.getChildText("Template");
		
		Smarttask = xml_rootelement.getChildText("Smarttask");
		
		SupportEmail = xml_rootelement.getChildText("SupportEmail");
				
		System.out.println("Coordinator: "+ Coordinator);
		System.out.println("CoordAssistant: "+CoordAssistant);
		System.out.println("TeamLead: "+TeamLead);
		System.out.println("Template: "+Template);
		System.out.println("Smarttask: "+Smarttask);
		System.out.println("SupportEmail: "+SupportEmail);
		
		//Get Transferee Info
		Element E_childrent = xml_rootelement.getChild("MailSender");
		glb_EmailSender = E_childrent.getChildText("Email");
		System.out.println("glb_EmailSender: "+glb_EmailSender);
	}
	
	public static void ParseDataFromFile()
	{
		switch (TestConfigs.EnvName)
		{
		case "QA":
			file_name = "QA_TestData.xml";
			break;
		case "UAT":
			file_name = "UAT_TestData.xml";
			break;
		case "PROD":
			file_name = "PROD_TestData.xml";
			break;
			
		}
			
		xml_rootelement = new FileManage().Parse_XMLFile(file_direction+file_name);
		
		getGlbRefInfo();
		
		Client = getClientInfo();
		Vendor = getVendorInfo();
		Transferee = getTransfereeInfo();
	
	}
	
		public static Transferee_Info getTransfereeInfo()
		{
		//Get Transferee Info
		Element E_childrent = xml_rootelement.getChild("Transferee");
		
		Transferee = new Transferee_Info();
		Transferee.firstname = E_childrent.getChildText("FName");
		Transferee.lastname = E_childrent.getChildText("LName");
		Transferee.FullName = Transferee.firstname+" "+Transferee.lastname;
		Transferee.username = E_childrent.getChildText("UserName");
		Transferee.password = E_childrent.getChildText("Pass");
		Transferee.MailList.get(0).EmailAddr = E_childrent.getChildText("Email");
		
		return Transferee;
		
		}
		
		public static Client_Info getClientInfo()
		{
		//Get Transferee Info
		Element E_childrent = xml_rootelement.getChild("Client");
		Element ctact_e = E_childrent.getChild("Contact");
		//Client, Client Contactcontact
		
		System.out.println(E_childrent.getChildText("Name"));
		
		System.out.println(ctact_e.getChildText("FullName"));
		System.out.println(ctact_e.getChildText("UserName"));
		System.out.println(ctact_e.getChildText("Pass"));
		System.out.println(ctact_e.getChildText("Email"));
		
		
		Client = new Client_Info();
		Client.CompanyName = E_childrent.getChildText("Name");
		
		if(!ctact_e.getChildText("FullName").equals(""))
		{
			Client.Contacts.get(0).FullName = ctact_e.getChildText("FullName");
			Client.Contacts.get(0).username = ctact_e.getChildText("UserName");
			Client.Contacts.get(0).password = ctact_e.getChildText("Pass");
			Client.Contacts.get(0).email_info.EmailAddr = ctact_e.getChildText("Email");
		}
		
		
		return Client;
	}//end void

	public static Vendor_Info getVendorInfo()
	{
		//Vendor
		Vendor = new Vendor_Info();
		Element E_childrent = xml_rootelement.getChild("Vendor");
		
		Vendor.VendorName = E_childrent.getChildText("Name");
		
		Vendor_Ctacts = new ArrayList<Contact_Info>();
		
		//VEndor Contact parsing
		for(Element cur_ctact_e: E_childrent.getChildren("Contact"))
		{
			System.out.println(cur_ctact_e.getChildText("Service"));
			System.out.println(cur_ctact_e.getChildText("FullName"));
			System.out.println(cur_ctact_e.getChildText("UserName"));
			System.out.println(cur_ctact_e.getChildText("Pass"));
			System.out.println(cur_ctact_e.getChildText("Email"));
				
			
			Contact_Info ctact_temp = new Contact_Info();
			ctact_temp.ServiceType = cur_ctact_e.getChildText("Service");
			ctact_temp.FullName = cur_ctact_e.getChildText("FullName");
			ctact_temp.username = cur_ctact_e.getChildText("UserName");
			ctact_temp.password = cur_ctact_e.getChildText("Pass");
			ctact_temp.email_info.EmailAddr = cur_ctact_e.getChildText("Email");
			
			Vendor_Ctacts.add(ctact_temp);
		}
				
		Vendor.Contacts = Vendor_Ctacts;
		
		return Vendor;
	}
}



