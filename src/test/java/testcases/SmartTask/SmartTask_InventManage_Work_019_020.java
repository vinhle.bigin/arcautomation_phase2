package testcases.SmartTask;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.InventManageService_Info;
import businessObjects.SmartTask_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmartTask;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;




@Category(Catagory_SmartTask.class)
public class SmartTask_InventManage_Work_019_020  extends TestBase{
	
	
	 public SmartTask_InventManage_Work_019_020(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Transferee_Info transferee;
	 Vendor_Info vendor;
	
	 Client_Info  client_info;
	 SmartTask_Info newtask;
	 String new_value;
	
	@Before
	public void setup()
	{
		super.setup();
		
		//1. Prepare test data for Client
		client_info = new Client_Info();
		
		//2. Prepare test data for Vendor and Contact Info
		vendor = new Vendor_Info();
		
		vendor.SearchOpt_IsInter = true;
		
	//	vendor.Contacts.get(0).ServiceType = "Realtor - Agent";
	
		//3. Prepare test data for InventManage
		transferee = new Transferee_Info();
		
		transferee.InventManage_service = new InventManageService_Info(vendor.Contacts.get(0));
		
		transferee.Client =  client_info;
		
		//4. SmartTask Test Data:
		new_value = "$300.00";
		
		newtask = new SmartTask_Info();
		
		newtask.Service = "Inventory management";
		
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetFieldChangeCondt_func("", "Inventory Management - List Price", "", new_value);
		
		//===========Prepare Test Data:
		Admin.Login_to_ARC_Portal_as_admin();
	       
		 //2. Create Client
        Admin.ClientManage.Create_New_Client(client_info);
        
        
        /*
        //2. Create Vendors #1 and ContactS: CONTACT
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor);
		 
		Admin.VendorManage.Update_Vendor_Search_Option_Tab(vendor);
		 
		Admin.ContactManage.Create_New_Contact(vendor.Contacts.get(0),true);
		*/
      		
		
	}

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_019_Verify InventManage - Smarttask condition FieldChange + Activity Action Works Correctly")
	 
	public void TC_019_Verify_InventManage_Smarttask_condition_FieldChange_Action_Activity_Works() {
		
	
		//Set Action trigger for SM
		newtask.action.InIt_NewActivity_Action(newtask.Name, 1, "Business");
		
		Activity_Info Expected_activity = newtask.action.Act_Activity;
		Expected_activity.Assignee = transferee.FullName;
		Expected_activity.Service = transferee.InventManage_service.Type;
			
		//Pre-condition:
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee,true);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Steps:
		//1. Update Transferee InventManage Contact Modal Info
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		//Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.InventManage_service.Type, vendor.Contacts.get(0) );
	
		Admin.ServiceManage.Go_To_Service_Details(transferee.InventManage_service.Type);
		
		Admin.ServiceManage.Update_InventManage_List_and_Sale_Info_Section(transferee.InventManage_service.HS_ListSale_section);
				
		Admin.Wait_For_SM_Action_Is_Trigger_Effectly();
		
		//Verfication
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(Expected_activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(Expected_activity);
		
		if(TestConfigs.glb_TCStatus==true)
			Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(Expected_activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Category({Category_E2E.class})
	@Title("TC_020_InventManage - Verify Smarttask condition FieldChange + Action Send Email Work Correctly")
	public void TC_020_InventManage_Verify_Smarttask_Condtion_FieldChange_Action_SendEmail_Works() {
		
		//Pre-condition:
		Template_Info newtpl = new Template_Info();
		newtpl.ServiceType = newtask.Service;
		
		transferee.MailList.get(0).EmailAddr = glb_RefData.glb_TestMailAccount;
		transferee.MailList.get(0).Map_Template_Info_func(newtpl, true);

		//Set Action trigger for SM
		newtask.action.InitSendEmailAction_func(newtpl.Name);
				      
		//Steps:
		//Admin.Login_to_ARC_Portal_as_admin();
		//Pre-condition:
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee,true);
				
		Admin.TemplateManage.Create_New_Template(newtpl);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Create Transferee and Update Service Field to trigger SM
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		//Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.InventManage_service.Type, vendor.Contacts.get(0) );
	
		Admin.ServiceManage.Go_To_Service_Details(transferee.InventManage_service.Type);
		
		Admin.ServiceManage.Update_InventManage_List_and_Sale_Info_Section(transferee.InventManage_service.HS_ListSale_section);
		
		//This step is repeat as the rule: the Smartask will not work when update the service info at 1st time
		transferee.InventManage_service.HS_ListSale_section.ListPrice = new_value;
		
		Admin.ServiceManage.Update_InventManage_List_and_Sale_Info_Section(transferee.InventManage_service.HS_ListSale_section);
		
		//Verify Point
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), "","Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

	
	
}
