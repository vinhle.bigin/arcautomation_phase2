package testcases.SmartTask;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.SmartTask_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmartTask;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;




@Category(Catagory_SmartTask.class)
public class SmartTask_Work_008_009_010  extends TestBase{

	public SmartTask_Work_008_009_010(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Client_Info  client_info;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		client_info = new Client_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
		
		
	}
	

	@Test
	@Title("TC_008_Verify Smarttask with Action Send Email Work Correctly")
	 
	public void TC_008_Verify_Smarttask_Action_SendEmail_Works() {
		
		//Pre-condition:
		Transferee_Info transf = new Transferee_Info();
		transf.Client =  client_info;
		
		Template_Info newtpl = new Template_Info();
		
		transf.MailList.get(0).EmailAddr = glb_RefData.glb_TestMailAccount;
		transf.MailList.get(0).Map_Template_Info_func(newtpl, true);

		SmartTask_Info newtask = new SmartTask_Info();
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.action.InitSendEmailAction_func(newtpl.Name);
		
		newtask.SetFileCreateCondt_func();
      
		//Steps:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf, true);
		
		Admin.Verify_User_Receives_Email_Gmail(transf.MailList.get(0), "","Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_009_Verify Smarttask with Action UpdateField Works Correctly")
	@Issues({"#AA-45"})
	public void TC_009_Verify_Smarttask_Action_UpdateField_Works() {
		
		//Pre-condition:
		Transferee_Info transf = new Transferee_Info();
		transf.Client =  client_info;
		
		Transferee_Info transf_2 = transf;
		transf_2.firstname = "Update"+transf_2.firstname;
		transf_2.FullName = transf_2.firstname +" "+ transf_2.lastname;
		
		SmartTask_Info newtask = new SmartTask_Info();
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetFileCreateCondt_func();
		
		newtask.action.InitUpdateFieldAction_func("", "Update"+transf.firstname);
      
				
		//Steps:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf, true);
		
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Verify_transfee_is_shown_on_Tranferee_List(transf_2);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf_2);
		
		Admin.TransfManage.Verify_transfee_GeneralInfo_is_correct(true,transf_2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	

	@Test
	@Title("TC_010_Verify Smarttask condt ActivityComplete with Action New_Activity Works Correctly")
	@Category(Category_E2E.class)
	public void TC_010_Verify_Smarttask_condition_ActivityCompleted_Action_NewActivity_Works() {
		
		//Make Up Test Data:
		Transferee_Info transf_info = new Transferee_Info();
		transf_info.Client =  client_info;
		
		Activity_Info newact = new Activity_Info();
		newact.Type = "Follow Up";
		newact.Assignee = transf_info.FullName;
		
		SmartTask_Info newtask = new SmartTask_Info();
		
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetActivityCompleteCondt_func(newact.Title);
		
		newtask.action.InIt_NewActivity_Action(newtask.Name, 1, "Business");
		
		Activity_Info Expected_activity = newtask.action.Act_Activity;
		
		Expected_activity.Assignee = transf_info.FullName;
		
		//Pre-condition:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Steps:
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);	
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(newact);
		
		newact.Closed= true;
		
		Admin.ActivityManage.Update_Activity(newact.Title, newact);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(Expected_activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(Expected_activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(Expected_activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
}
