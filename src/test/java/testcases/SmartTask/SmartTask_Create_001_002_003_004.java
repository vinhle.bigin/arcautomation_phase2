package testcases.SmartTask;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.SmartTask_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmartTask;
import testcases.Catagory_SmokeTest;




@Category(Catagory_SmartTask.class)
public class SmartTask_Create_001_002_003_004  extends TestBase{
	
	
	
	public SmartTask_Create_001_002_003_004(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Client_Info  client_info;
	
	
	
	
	@Before
	public void setup()
	{
	
		super.setup();
		client_info = new Client_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
		
	}
	

	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_001_Verify Smarttask condition FileCreated with Action Create Activity Can Be Created")
	 
	public void TC_001_Verify_Smarttask_condt_FileCreated_Action_CreateActivity_Created() {
		
		//Pre-condition:
		
		Activity_Info newact = new Activity_Info();

		SmartTask_Info newtask = new SmartTask_Info();
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetFileCreateCondt_func();
		
		newtask.action.Add_Existing_Activity_To_Action(newact);
      
		//Steps:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.SmarTaskManage.Search_Smart_Task(newtask.Name);
		
		Admin.SmarTaskManage.Verify_Smart_Task_Info_is_Correct(newtask);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_002_Verify Smarttask condition ActivityComplete Can Be Created")
	 
	public void TC_002_Verify_Smarttask_condition_ActivityCompleted_Can_Be_Created() {
		
		//Make Up Test Data:
		Transferee_Info transf_info = new Transferee_Info();
		
		Activity_Info newact = new Activity_Info();
		newact.Type = "Follow Up";
		newact.Assignee = transf_info.FullName;
		
		Activity_Info newact_2 = new Activity_Info();

		SmartTask_Info newtask = new SmartTask_Info();
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetActivityCompleteCondt_func(newact.Title);
		
		newtask.action.Add_Existing_Activity_To_Action(newact_2);
		
		//Pre-condition:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);	
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(newact);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.SmarTaskManage.Search_Smart_Task(newtask.Name);
		
		Admin.SmarTaskManage.Verify_Smart_Task_Info_is_Correct(newtask);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_003_Verify Smarttask condition FieldChange Can Be Created")
	 
	public void TC_003_Verify_Smarttask_condition_FieldChange_Can_Be_Created() {
		
		//Pre-condition:
		Activity_Info newact = new Activity_Info();

		SmartTask_Info newtask = new SmartTask_Info();
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetFieldChangeCondt_func("", "", "", "");
		
		newtask.action.Add_Existing_Activity_To_Action(newact);
      
		//Steps:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.SmarTaskManage.Search_Smart_Task(newtask.Name);
		
		Admin.SmarTaskManage.Verify_Smart_Task_Info_is_Correct(newtask);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

	@Test
	@Title("TC_004_Verify Smarttask condition Send Email Can Be Created")
	 
	public void TC_004_Verify_Smarttask_condition_SendEmail_Can_Be_Created() {
		
		//Pre-condition:
		Template_Info newtpl = new Template_Info();
		
		Activity_Info newact = new Activity_Info();

		SmartTask_Info newtask = new SmartTask_Info();
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetEmailProcessCondt_func(newtpl.Name);
		
		newtask.action.Add_Existing_Activity_To_Action(newact);
      
		//Steps:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.SmarTaskManage.Search_Smart_Task(newtask.Name);
		
		Admin.SmarTaskManage.Verify_Smart_Task_Info_is_Correct(newtask);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
}
