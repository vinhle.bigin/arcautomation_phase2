package testcases.SmartTask;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.SmartTask_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmartTask;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;




@Category(Catagory_SmartTask.class)
public class SmartTask_Create_and_Work_005_006_007  extends TestBase{
	
	
	public SmartTask_Create_and_Work_005_006_007(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Client_Info  client_info;
	
	
	
	
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		client_info = new Client_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
		
		
	}
	

	@Test
	@Title("TC_005_Verify Smarttask with Action Send Email Can Be Created")
	 
	public void TC_005_Verify_Smarttask_Action_SendEmail_Can_Be_Created() {
		
		//Pre-condition:
		Template_Info newtpl = new Template_Info();

		SmartTask_Info newtask = new SmartTask_Info();
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.action.InitSendEmailAction_func(newtpl.Name);
		
		newtask.SetFileCreateCondt_func();
      
		//Steps:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.SmarTaskManage.Search_Smart_Task(newtask.Name);
		
		Admin.SmarTaskManage.Verify_Smart_Task_Info_is_Correct(newtask);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Category({Catagory_SmokeTest.class,Category_E2E.class})
	@Test
	@Title("TC_006_Verify Smarttask with Action UpdateField Can Be Created")
	 
	public void TC_006_Verify_Smarttask_Action_UpdateField_Can_Be_Created() {
		
		//Pre-condition:
		SmartTask_Info newtask = new SmartTask_Info();
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetFileCreateCondt_func();
		
		newtask.action.InitUpdateFieldAction_func("", "Update Testing From SmarTask");
      
		//Steps:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.SmarTaskManage.Search_Smart_Task(newtask.Name);
		
		Admin.SmarTaskManage.Verify_Smart_Task_Info_is_Correct(newtask);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_007_Verify Smarttask condition FileCreated with Action CreateActivity Works Correctly")
	 
	public void TC_007_Verify_Smarttask_condt_FileCreated_Action_CreateActivity_Work() {
		
		//Pre-condition:
		Transferee_Info transf = new Transferee_Info();
		
		transf.Client =  client_info;
		
		Activity_Info newact = new Activity_Info();

		SmartTask_Info newtask = new SmartTask_Info();
		
		newtask.SetFileCreateCondt_func();
		
		newtask.action.Add_Existing_Activity_To_Action(newact);
      
		//Steps:
		//Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf, true);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(newact);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(newact);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(newact);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
}
