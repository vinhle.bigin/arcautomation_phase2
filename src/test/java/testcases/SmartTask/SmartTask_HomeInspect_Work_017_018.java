package testcases.SmartTask;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.HomeInspectService_Info;
import businessObjects.SmartTask_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmartTask;
import testcases.Catagory_SmokeTest;




@Category(Catagory_SmartTask.class)
public class SmartTask_HomeInspect_Work_017_018  extends TestBase{
	
	
	 public SmartTask_HomeInspect_Work_017_018(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transferee;
	 Vendor_Info vendor;
	
	 Client_Info  client_info;
	 SmartTask_Info newtask;
	 
	
	
	
	
	
	
	@Before
	public void setup()
	{
		super.setup();
		//1. Prepare test data for Client
		client_info = new Client_Info();
		
		//2. Prepare test data for Vendor and Contact Info
		vendor = new Vendor_Info();
		
		vendor.SearchOpt_IsInter = true;
		
		vendor.Contacts.get(0).ServiceType = "Inspection Agents";
	
		//3. Prepare test data for HomeInpsect
		transferee = new Transferee_Info();
		transferee.MailList.get(0).EmailAddr = glb_RefData.glb_TestMailAccount;
		transferee.HomeInspect_service = new HomeInspectService_Info(vendor.Contacts.get(0));
		
		transferee.Client =  client_info;
		
		//4. SmartTask Test Data:
		String new_value = "Mold";
		newtask = new SmartTask_Info();
		newtask.Service = "Home inspection";
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		newtask.SetFieldChangeCondt_func("", "Home Inspection - Needed Type", "Equal to", new_value);
		
		//===========Prepare Test Data:
		Admin.Login_to_ARC_Portal_as_admin();
	       
		 //2. Create Client
        Admin.ClientManage.Create_New_Client(client_info);
        
      //Create Transfere:
      	Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee,true);
      	
      	Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
      	
      	Admin.ServiceManage.Go_To_Service_Details(transferee.HomeInspect_service.Type);
		
		Admin.ServiceManage.Create_HomeInspect_Service_Inspection_Need(transferee.HomeInspect_service);
        
        /*
        //2. Create Vendors #1 and ContactS: CONTACT
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor);
		 
		Admin.VendorManage.Update_Vendor_Search_Option_Tab(vendor);
		 
		Admin.ContactManage.Create_New_Contact(vendor.Contacts.get(0),true);
      	*/	
		
	}
	

	@Test
	@Title("TC_017_Verify HomeInspect - Smarttask condition FieldChange + Activity Action Works Correctly")
	@Issues({"#AA-44"}) 
	public void TC_017_Verify_HomeInpsect_Smarttask_condition_FieldChange_Action_Activity_Works() {
		
	
		//Set Action trigger for SM
		newtask.action.InIt_NewActivity_Action(newtask.Name, 1, "Business");
		
		Activity_Info Expected_activity = newtask.action.Act_Activity;
		Expected_activity.Assignee = transferee.FullName;
		Expected_activity.Service = transferee.HomeInspect_service.Type;
			
		
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Steps:
		//1. Update Transferee HomeInpsect Contact Modal Info
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		//Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.HomeInspect_service.Type, vendor.Contacts.get(0) );
	
		Admin.ServiceManage.Go_To_Service_Details(transferee.HomeInspect_service.Type);
		
			
		//Update InspectNeed
		String old_inspectType = transferee.HomeInspect_service.InspectType;
		transferee.HomeInspect_service.ReInit_ServiceInfo();
		transferee.HomeInspect_service.InspectType = newtask.Condt_Value;
		
		Admin.ServiceManage.Update_HomeInspect_Service_InspectionNeed(old_inspectType, transferee.HomeInspect_service);
		
				
		//Verfication
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(Expected_activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(Expected_activity);
		
		if(TestConfigs.glb_TCStatus==true)
			Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(Expected_activity);
		
		TestConfigs.glb_TCFailedMessage = "BUG: "+TestConfigs.glb_TCFailedMessage;
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_018_HomeInpsect - Verify Smarttask condition FieldChange + Action Send Email Work Correctly")
	@Issues({"#AA-44"}) 
	public void TC_018_HomeInpsect_Verify_Smarttask_Condtion_FieldChange_Action_SendEmail_Works() {
		
		//Pre-condition:
		Template_Info newtpl = new Template_Info();
		
		newtpl.ServiceType = newtask.Service;
		
		newtpl.Name = "Template_"+newtask.Name;
		
		newtpl.Subject = newtpl.Name;
		
		transferee.MailList.get(0).EmailAddr = glb_RefData.glb_TestMailAccount;
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl, true);

		//Set Action trigger for SM
		newtask.action.InitSendEmailAction_func(newtpl.Name);
				      
		Admin.TemplateManage.Create_New_Template(newtpl);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Create Transferee and Update Service Field to trigger SM
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.ServiceManage.Go_To_Service_Details(transferee.HomeInspect_service.Type);
		
		//Update HomeInspect Type
		String old_inspectType = transferee.HomeInspect_service.InspectType;
		
		transferee.HomeInspect_service.ReInit_ServiceInfo();
		
		transferee.HomeInspect_service.InspectType = newtask.Condt_Value;
		
		Admin.ServiceManage.Update_HomeInspect_Service_InspectionNeed(old_inspectType, transferee.HomeInspect_service);
		
		//Verify Point
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), "","Equals");
		
		TestConfigs.glb_TCFailedMessage = "BUG: "+TestConfigs.glb_TCFailedMessage;
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

	
	
}
