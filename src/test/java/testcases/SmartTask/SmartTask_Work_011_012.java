package testcases.SmartTask;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.SmartTask_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmartTask;
import testcases.Catagory_SmokeTest;




@Category(Catagory_SmartTask.class)
public class SmartTask_Work_011_012  extends TestBase{
	
	public SmartTask_Work_011_012(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Client_Info  client_info;
	
	
	
	
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		client_info = new Client_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
		
	}


	@Test
	@Title("TC_011_Verify Smarttask condition FieldChange Works Correctly")
	 
	public void TC_011_Verify_Smarttask_condition_FieldChange_Works() {
		
		//Make Up Test Data:
		Transferee_Info transf_info = new Transferee_Info();
		transf_info.Client =  client_info;
		
		String new_value = transf_info.firstname+"_Updated";
		
		Transferee_Info transf_info_2 = transf_info;
		
		transf_info_2.ReGenerate_GeneralInfo_func();
		
		SmartTask_Info newtask = new SmartTask_Info();
		
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetFieldChangeCondt_func("", "", "", new_value);
		
		newtask.action.InIt_NewActivity_Action(newtask.Name, 1, "Business");
		
		Activity_Info Expected_activity = newtask.action.Act_Activity;
				
			
		//Pre-condition:
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Steps:
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		transf_info_2.firstname = new_value;
		transf_info_2.FullName = transf_info_2.firstname+ " "+ transf_info_2.lastname;
		Expected_activity.Assignee = transf_info.FullName;
		
		Admin.TransfManage.Update_transfree_General_Info(transf_info_2);
		
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(Expected_activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(Expected_activity);
		
		if(TestConfigs.glb_TCStatus==true)
			Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(Expected_activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_012_Verify Smarttask condition Send Email Works Correctly")
	 
	public void TC_012_Verify_Smarttask_condition_SendEmail_Works_Correctly() {
		
		//==============Make Up Test Data:
		
		//Template Info
		Template_Info newtpl = new Template_Info();
		
		//Transferee Info
		
		Transferee_Info transf_info = new Transferee_Info();
		transf_info.Client =  client_info;
		
		transf_info.MailList.get(0).Map_Template_Info_func(newtpl, true);
		
		//Smart Task Info
		SmartTask_Info newtask = new SmartTask_Info();
		
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetEmailProcessCondt_func(newtpl.Name);
		
		newtask.action.InIt_NewActivity_Action(newtask.Name, 1, "Business");
		
		Activity_Info Expected_activity = newtask.action.Act_Activity;
		
		Expected_activity.Assignee = transf_info.FullName;
      
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Send_Email_to_transfree(transf_info.MailList.get(0), false);
	
		//Verify
		Admin.Verify_User_Receives_Email_Gmail(transf_info.MailList.get(0), "","Equals");
		
		if(TestConfigs.glb_TCStatus==true)
		{
			Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(Expected_activity);
			
			Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(Expected_activity);
			
			Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(Expected_activity);
		}//end if
				
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	

	
	
}
