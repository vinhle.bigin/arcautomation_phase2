package testcases.SmartTask;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.HBOService_Info;
import businessObjects.SmartTask_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmartTask;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;




@Category(Catagory_SmartTask.class)
public class SmartTask_HBO_Work_015_016 extends TestBase{
	
	
	public SmartTask_HBO_Work_015_016(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transferee;
	
	Client_Info  client_info;
	SmartTask_Info newtask;
	String HBO_Acquist_type;
 
	@Before
	public void setup()
	{
		super.setup();
	
		//pageobject = new PageObjects(test_cfig.glb_webdriver);
		
		//1. Prepare test data for Client
		client_info = new Client_Info();
		
		//2. Prepare test data for HBO
		transferee = new Transferee_Info();
		
		transferee.MailList.get(0).EmailAddr = glb_RefData.glb_TestMailAccount;
		
		transferee.HBO_service = new HBOService_Info();
		
		HBO_Acquist_type = "Amended";
		
		transferee.HBO_service.Init_TransactionInfo("BVO");
		
		transferee.Client =  client_info;
		
		//4. SmartTask Test Data:
		newtask = new SmartTask_Info();
		
		newtask.Service = "Home buyout";
		
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetFieldChangeCondt_func("", "Home Buyout - Acquisition Type", "", HBO_Acquist_type);
		
		//===========Prepare Test Data:
		Admin.Login_to_ARC_Portal_as_admin();
	       
		 //2. Create Client
        Admin.ClientManage.Create_New_Client(client_info);
      		
		
	}
	

	@Test
	@Title("TC_015_Verify HBO Service - Smarttask condition FieldChange + Activity Action Works Correctly")
	 
	public void TC_015_Verify_HBO_Smarttask_condition_FieldChange_Action_Activity_Works() {
		
	
		//Set Action trigger for SM
		newtask.action.InIt_NewActivity_Action(newtask.Name, 1, "Business");
		
		Activity_Info Expected_activity = newtask.action.Act_Activity;
		
		Expected_activity.Assignee = transferee.FullName;
		
		Expected_activity.Service = transferee.HBO_service.Type;
			
		//Pre-condition:
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee,true);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Steps:
		//1. Update Transferee HBO Contact Modal Info
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		Admin.ServiceManage.Go_To_Service_Details(transferee.HBO_service.Type);
		
		Admin.ServiceManage.Updates_HBO_Transaction_Info(transferee.HBO_service);
		
		//This step is repeat as the rule: the Smartask will not work when update the service info at 1st time
		transferee.HBO_service.Transt_AcquisType = HBO_Acquist_type;
		
		Admin.ServiceManage.Updates_HBO_Transaction_Info(transferee.HBO_service);
				
		//Verfication
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(Expected_activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(Expected_activity);
		
		if(TestConfigs.glb_TCStatus==true)
			Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(Expected_activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Category({Catagory_SmokeTest.class,Category_E2E.class})
	@Issues({"#AA-44"})
	@Test
	@Title("TC_016_HBO Service- Verify Smarttask condition FieldChange + Action Send Email Work Correctly")
	 
	public void TC_016_HBOService_Verify_Smarttask_Condtion_FieldChange_Action_SendEmail_Works() {
		
		//Pre-condition:
	
		
		Template_Info newtpl = new Template_Info();
		newtpl.ServiceType = newtask.Service;
		
		transferee.MailList.get(0).EmailAddr = glb_RefData.glb_TestMailAccount;
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl, true);

		//Set Action trigger for SM
		newtask.action.InitSendEmailAction_func(newtpl.Name);
				      
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Create Transferee and Update Service Field to trigger SM
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee,true);
				
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		Admin.ServiceManage.Go_To_Service_Details(transferee.HBO_service.Type);
		
		Admin.ServiceManage.Updates_HBO_Transaction_Info(transferee.HBO_service);
		
		transferee.HBO_service.Transt_AcquisType = HBO_Acquist_type;
		
		Admin.ServiceManage.Updates_HBO_Transaction_Info(transferee.HBO_service);
		
		//Verify Point
		//Refresh the localdatetime
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), "","Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }


}
