package testcases.SmartTask;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.HHGService_Info;
import businessObjects.Service_Order_Info;
import businessObjects.SmartTask_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Title;




public class SmartTask_HHG_Work_021_022_023 extends TestBase{
	
	
	public SmartTask_HHG_Work_021_022_023(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transferee;
	
	Client_Info  client_info;
	SmartTask_Info newtask;
	Service_Order_Info order;
 
	@Before
	public void setup()
	{
		super.setup();
		
		//1. Prepare test data for Client
		client_info = new Client_Info();
		
		//2. Prepare test data for HBO
		transferee = new Transferee_Info();
		
		transferee.MailList.get(0).EmailAddr = glb_RefData.glb_TestMailAccount;
		
		transferee.HHG_service = new HHGService_Info();
		
		transferee.HBO_service.Init_TransactionInfo("BVO");
		
		transferee.Client =  client_info;
		
		//4. SmartTask Test Data:
		String new_value = transferee.HHG_service.HHGDeliverDate;
		
		newtask = new SmartTask_Info();
		
		newtask.Service = "Household goods";
		
		newtask.Apply_SpecificClientName = client_info.CompanyName;
		
		newtask.SetFieldChangeCondt_func("", "Household Good - Order Delivery Date Actual Start", "", new_value);
		
		order = new Service_Order_Info();
		
		order.InIt_Dosmetic_Order_Info();
		
		order.InIt_ServiceDates();
		
		order.ServiceType = "HHG";
		
		//===========Prepare Test Data:
		Admin.Login_to_ARC_Portal_as_admin();
	       
		 //2. Create Client
        Admin.ClientManage.Create_New_Client(client_info);
      		
		
	}
	

	@Test
	@Title("TC_021_Verify HHG Service - Smarttask condition FieldChange + Activity Action Works Correctly")
	public void TC_021_Verify_HHG_Smarttask_condition_FieldChange_Action_Activity_Works() {
		
	
		//Set Action trigger for SM
		newtask.action.InIt_NewActivity_Action(newtask.Name, 1, "Business");
		
		Activity_Info Expected_activity = newtask.action.Act_Activity;
		
		Expected_activity.Assignee = transferee.FullName;
		
		Expected_activity.Service = transferee.HHG_service.Type;
			
		//Pre-condition:
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee,true);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Steps:
		//1. Update Transferee HHG Contact Modal Info
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		Admin.ServiceManage.Go_To_Service_Details(transferee.HHG_service.Type);
		
		Admin.ServiceManage.Create_New_HHG_Service_Order(order); 
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
				
		//Verfication
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(Expected_activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(Expected_activity);
		
		if(TestConfigs.glb_TCStatus==true)
			Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(Expected_activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_022_HHG Service- Verify Smarttask condition FieldChange + Action Send Email Work Correctly")
	public void TC_022_HHGService_Verify_Smarttask_Condtion_FieldChange_Action_SendEmail_Works() {
		
		//Pre-condition:
		Template_Info newtpl = new Template_Info();
		newtpl.ServiceType = newtask.Service;
		
		transferee.MailList.get(0).EmailAddr = glb_RefData.glb_TestMailAccount;
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl, true);

		//Set Action trigger for SM
		newtask.action.InitSendEmailAction_func(newtpl.Name);
				      
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl);
		
		Admin.SmarTaskManage.Create_Smart_Task(newtask);
		
		//Create Transferee and Update Service Field to trigger SM
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee,true);
				
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		Admin.ServiceManage.Go_To_Service_Details(transferee.HHG_service.Type);
		
		Admin.ServiceManage.Create_New_HHG_Service_Order(order); 
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), "","Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

	
	
}

