package testcases.TaskListManagement;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Contact_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;
import testcases.Catagory_SmokeTest;


@Category(Catagory_Activity.class)
public class TaskList_Manage_HHGService_019  extends TestBase{

	
	public TaskList_Manage_HHGService_019(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Transferee_Info transf_info;
	
	Vendor_Info Vendor_info;
	
	Service_Order_Info order;
	
	Contact_Info ctact_info;
	Activity_Info activity;
	
	
	@Before
	public void setup()
	{
		super.setup();
				
		//Test Data:
		Vendor_info = new Vendor_Info();
		
		transf_info = new Transferee_Info();
		
		Vendor_info.SearchOpt_IsUSA = true;
		
		ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		order = new Service_Order_Info();
		
		order.InIt_International_Order_Info();
		
		order.InIt_VendorSelected(ctact_info.VendorName);
				
		activity = new Activity_Info(order.Description, order.VendorName);
		
		activity.Assignee = transf_info.FullName;
		
		activity.Service = transf_info.HHG_service.Type;

		//Pre-conditions:
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		
		Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
	}


	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 019 Transferee - Task With HHG Service is created successfully")
	public void TC_019_Task_With_HHG_Service_is_created_successfully() {
      
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(activity.Service, ctact_info);
		
		Admin.ServiceManage.Create_New_HHG_Service_Order(order);
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.TaskListManage.Create_Activity(activity);		 

		Admin.TaskListManage.Search_Activity_With_Title(activity.Title);		 

		Admin.TaskListManage.Verify_Activity_Exist_In_The_Activity_List(activity);		 

		Admin.TaskListManage.Verify_Activity_Modal_Details_Is_Correct(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
}
	

