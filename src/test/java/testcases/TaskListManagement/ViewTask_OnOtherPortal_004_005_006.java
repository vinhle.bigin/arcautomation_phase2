package testcases.TaskListManagement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.ClientUser_Steps;
import stepsDefiniton.TransfUser_Steps;
import stepsDefiniton.VendorUser_Steps;
import testcases.Catagory_Activity;


@Category(Catagory_Activity.class)
public class ViewTask_OnOtherPortal_004_005_006  extends TestBase{

	public ViewTask_OnOtherPortal_004_005_006(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf;
	
	Client_Info client_info;
	
	Contact_Info contact_info;
	
	Activity_Info activity;
	
	Vendor_Info vendor_info;
	
	@Steps
	VendorUser_Steps Vendor;

	@Steps
	TransfUser_Steps Transferee;
	
	@Steps
	ClientUser_Steps Client;
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		client_info = new Client_Info();	
		
		vendor_info = new Vendor_Info();
		
		activity = new Activity_Info();
		
		transf = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
	}

	
	@Test
	@Title("TC 004 Transferee - View New Task In Client Portal")
	public void TC_004_View_New_Task_In_Client_Portal() {
		
		activity.ClientView = true;
		
		activity.Assignee = transf.FullName;
		
		Client.ClientManage.Create_New_Client(client_info);
		
		transf.Client = client_info;
		
		Contact_Info ctact_info = client_info.Contacts.get(0);
		
		activity.ClientView = true;
		
		Client.ContactManage.Create_New_Contact(ctact_info, true);
		
		Client.ContactManage.Update_Contact_Profile_Account(ctact_info);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf, true);

		Admin.TaskListManage.Create_Activity(activity);		
		
		Client.Login_to_ClientPortal(ctact_info.username, ctact_info.password);
		
		Admin.TransfManage.Search_for_Transferee_With_Firstname_Client_Portal(transf);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC 005 Transferee - View New Task In Vendor Portal")
	public void TC_005_View_New_Task_In_Vendor_Portal() {

		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor_info);	
		
		Contact_Info ctact_info = vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf.OriResAddr_info.City;
		
		transf.HHG_service.Type = "Household goods";
		
		Service_Order_Info order = new Service_Order_Info();
		
		order.InIt_International_Order_Info();
		
		order.InIt_VendorSelected(ctact_info.VendorName);
				
		Activity_Info activity = new Activity_Info(order.Description, order.VendorName);
		
		activity.VendorView = true;
		
		activity.Assignee = transf.FullName;
		
		activity.Service = "Household goods";
		
		//Create Vendor Contact	
		Vendor.ContactManage.Create_New_Contact(ctact_info,true);
		
		Vendor.ContactManage.Update_Contact_Profile_Account(ctact_info);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf);	
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf.HHG_service.Type, ctact_info);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf);
		
		Admin.ServiceManage.Create_New_HHG_Service_Order(order);
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.TaskListManage.Create_Activity(activity);		 	
		
		Vendor.Login_to_VendorPortal(ctact_info.username, ctact_info.password);
		
		Vendor.Register_ContactEmail_VendorPortal(ctact_info);
		
		Admin.TransfManage.Search_for_Transferee_With_Firstname_Client_Portal(transf);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);

    }
	
	@Test
	@Title("TC 006 Transferee - View New Task In Transferee Portal")
	public void TC_006_View_New_Task_In_Transferee_Portal() {

		activity.Assignee = transf.FullName;
		
		activity.TransfereeView = true;
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf, true);

		Admin.TransfManage.Update_Transfree_Account_Tab(transf);
		
		Admin.TaskListManage.Create_Activity(activity);
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

}
