package testcases.TaskListManagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import custom_Func.DateTime_Manage;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;
import testcases.Catagory_SmokeTest;


@Category(Catagory_Activity.class)
public class TaskList_Manage_SearchTaskList_088  extends TestBase{
	
	
	
	
	public TaskList_Manage_SearchTaskList_088(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	@Before
	public void setup()
	{
		super.setup();
	}
	
	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 088 Verify Date is searched successfully")
	public void TC_088_Verify_Date_is_searched_successfully() {

		Activity_Info activity = new Activity_Info();
		
		Transferee_Info transf = new Transferee_Info();
		
		activity.Assignee = transf.FullName;
		
		//Pre-condition: Make Up data for Start Date and End Date of Activity
		Date StartDate_1 = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),10);
		
		Date EndDate_1 = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),11);
		
		Date StartDate_2 = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),15);
		
		Date EndDate_2 = DateTime_Manage.IncreaseDay(DateTime_Manage.GetCurrentLocalDate_Date(),16);
		
		activity.StartDate= DateTime_Manage.ConvertDatetoString(StartDate_1)+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
		
		activity.EndDate= DateTime_Manage.ConvertDatetoString(EndDate_1)+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
		
		Activity_Info activity_2 = new Activity_Info();
		
		activity_2.Assignee = transf.FullName;
		
		activity_2.StartDate= DateTime_Manage.ConvertDatetoString(StartDate_2)+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
		
		activity_2.EndDate= DateTime_Manage.ConvertDatetoString(EndDate_2)+" "+DateTime_Manage.getCurrentLocalTime_HHMM();
		
		List<Activity_Info> Activity_list = new ArrayList<Activity_Info>();
		
		Activity_list.add(activity);
		
		Activity_list.add(activity_2);
		
		//Steps:
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf, true);
		
		int size_list = Activity_list.size();
		
		for(int j=0;j<size_list;j++)
		{
			Admin.TaskListManage.Create_Activity(Activity_list.get(j));
		}
		
		for(int j=0;j<size_list;j++)
		{
		Admin.TaskListManage.Search_Activity_AllConditions_func(Activity_list.get(j).Date,Activity_list.get(j).EndDate,"","","","");

		Admin.TaskListManage.Verify_Activity_Exist_In_The_Activity_List(Activity_list.get(j));
		}
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
    }
	
}
