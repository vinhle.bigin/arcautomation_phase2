package testcases.TaskListManagement;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;
import testcases.Catagory_SmokeTest;


@Category(Catagory_Activity.class)
public class UploadFile_InTask_009 extends TestBase {

	
	public UploadFile_InTask_009(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Activity_Info activity;
	
	
	Transferee_Info transf;
	
		
	@Before
	public void setup()
	{
		super.setup();
		
		transf = new Transferee_Info();
		
		activity = new Activity_Info();
		
		activity.Assignee = transf.FullName;
		
		activity.FilesName.add("HC_File_Activity.docx");
		
		String expect_content = "This is for testing content";
		
		new FileManage().CreateWordFile_func(activity.FilesName.get(0), expect_content);
		
		Admin.Login_to_ARC_Portal_as_admin();
	}
	
	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 009 Transferee - Verify File in New Task is uploaded successfully")
	public void TC_009_Verify_File_in_New_Task_is_uploaded_successfully() {
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf, true);

		Admin.TaskListManage.Create_Activity(activity);		 

		Admin.TaskListManage.Search_Activity_With_Title(activity.Title);		 

		Admin.TaskListManage.Verify_Activity_Exist_In_The_Activity_List(activity);		 

		Admin.TaskListManage.Verify_Activity_Modal_Details_Is_Correct(activity);		 

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

}
