package testcases.TaskListManagement;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;
import testcases.Catagory_SmokeTest;


@Category(Catagory_Activity.class)
public class TaskList_Manage_TaskType_001_002_003  extends TestBase{
	
	
	public TaskList_Manage_TaskType_001_002_003(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Activity_Info activity;
	
	Transferee_Info transf;
	
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		transf = new Transferee_Info();
		
		activity = new Activity_Info();
		
		activity.Assignee = transf.FullName;

		Admin.Login_to_ARC_Portal_as_admin();
	}
	
	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_001: Verify New Task for Transferee is created successfully")
	public void TC_001_Verify_New_Task_for_Transferee_is_created_successfully() {
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf, true);

		Admin.TaskListManage.Create_Activity(activity);		 

		Admin.TaskListManage.Search_Activity_With_Title(activity.Title);		 

		Admin.TaskListManage.Verify_Activity_Exist_In_The_Activity_List(activity);		 

		Admin.TaskListManage.Verify_Activity_Modal_Details_Is_Correct(activity);		 

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
    }
	
	@Test
	@Title("TC_002: Verify New Task for Client is created successfully")
	@Issues({"#AA-20"})
	public void TC_002_Verify_New_Task_for_Client_is_created_successfully() {
		
		Client_Info client = new Client_Info();
		
		activity.AssigneeType = "Client";
		
		activity.Assignee = client.CompanyName;
		
		//Pre-condition: Create New Client
		
		Admin.ClientManage.Create_New_Client(client);

		Admin.TaskListManage.Create_Activity(activity);		 

		Admin.TaskListManage.Search_Activity_With_Title(activity.Title);		 

		Admin.TaskListManage.Verify_Activity_Exist_In_The_Activity_List(activity);		 

		Admin.TaskListManage.Verify_Activity_Modal_Details_Is_Correct(activity);		 

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
    }
	
	@Test
	@Title("TC_003: Verify New Task for Vendor is created successfully")
	@Issues({"#AA-20"})
	
	public void TC_003_Verify_New_Task_for_Vendor_is_created_successfully() {
		
		Vendor_Info vendor = new Vendor_Info();
	
		activity.AssigneeType = "Vendor";
		
		activity.Assignee = vendor.VendorName;
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor);
		
		Admin.TaskListManage.Create_Activity(activity);		 

		Admin.TaskListManage.Search_Activity_With_Title(activity.Title);		 

		Admin.TaskListManage.Verify_Activity_Exist_In_The_Activity_List(activity);		 

		Admin.TaskListManage.Verify_Activity_Modal_Details_Is_Correct(activity);		 

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
    }

}
