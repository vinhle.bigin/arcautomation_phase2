package testcases.TaskListManagement;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;


@Category(Catagory_Activity.class)
public class TaskList_Manage_CloseTask_099  extends TestBase{
	
	public TaskList_Manage_CloseTask_099(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf;
	
	@Before
	public void setup()
	{
		super.setup();
		
		transf = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);

	}
	
	
	
	@Test
	@Title("Verify The Closed Activity Of Transferee Cannot Be Edited")
	public void TC_009_The_closed_Activity_Of_transferee_Cannot_Be_Editted() {
		
		Activity_Info activity = new Activity_Info();
		
		activity.Assignee = transf.FullName;
		
		Admin.TaskListManage.Create_Activity(activity);
		
		activity.Closed = true;
		
		activity.Status = "Closed";
		
		Admin.TaskListManage.Update_Activity(activity.Title, activity);
		
		Admin.TaskListManage.Search_Status_All_For_Task_func(activity.Status);
		
		Admin.TaskListManage.Verify_Task_Details_Can_NOT_Be_Editable(activity, " - On Closed Task");
			
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
}
