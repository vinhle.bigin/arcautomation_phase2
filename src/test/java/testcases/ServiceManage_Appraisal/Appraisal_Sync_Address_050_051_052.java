package testcases.ServiceManage_Appraisal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.AppraisalService_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;


@Category(Category_Services.class)
public class Appraisal_Sync_Address_050_051_052  extends TestBase{


	public Appraisal_Sync_Address_050_051_052(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Transferee_Info transf_info;
	Vendor_Info Vendor_info;
	AppraisalService_Info service;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		Vendor_info.SearchOpt_IsInter = true;
		
		Vendor_info.Contacts.get(0).ServiceType = "Appraiser";
		
		service = new AppraisalService_Info ();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
	}



	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 050 Verify Transf OrgRes Address Is Sync From Appraisal Property's Address")
	public void TC_050_Verify_Transf_OrgRes_Address_Sync_From_Appraisal_Property_Address() {
      
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.Appraisal_service.Type);
		
		Admin.ServiceManage.Update_ServiceDetails_Property_Section(transf_info.Appraisal_service.Type,transf_info.Appraisal_service.Addr_info);
		
		Admin.TransfManage.Verify_transf_Addr_OrgRes_is_correct(transf_info.Appraisal_service.Addr_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC 051 Verify Appraisal Property's Address Is Sync From Transf OrgRes Address")
	public void TC_051_Verify_Appraisal_Property_Addr_Is_Sync_From_Transf_OrgRes_Addressd() {
      
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.Appraisal_service.Type);
		
		Admin.ServiceManage.Verify_ServiceDetails_Property_Address_Is_Correct_ViewMode(transf_info.Appraisal_service.Type, transf_info.OriResAddr_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC 052 Appraisal Specialist is updated successfully")
	public void TC_052_Appraisal_Specialist_is_updated_successfully() {
      
		//Test Data:
		//Create Transferee
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		//Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.Appraisal_service.Type);
		
		Admin.ServiceManage.Update_AppraisalSpecialist_Info(service);
		
		Admin.ServiceManage.Verify_AppraisalSpecialist_Correct_ViewMode(service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	
	@Test
	@Title("TC 053 Appraisal Analysis is updated successfully")
	public void TC_053_Appraisal_Analysis_is_updated_successfully() {
      
		//Test Data:
	
		transf_info.Appraisal_service = new AppraisalService_Info(Vendor_info.Contacts.get(0));
		
		//Pre-conditions:
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		 
		Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.Appraisal_service.Type);
		
		Admin.ServiceManage.Update_Appraisal_Analysis_Detail(transf_info.Appraisal_service);
		
		Admin.ServiceManage.Verify_Appraisal_Analysis_Info_is_correct_ViewMode(transf_info.Appraisal_service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}

}
