package testcases.ServiceManage_Appraisal;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.AppraisalService_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;


@Category(Category_Services.class)
public class Appraisal_Analysis_053  extends TestBase{

	 
	 public Appraisal_Analysis_053(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Template_Info newtpl1;
	 Transferee_Info transferee;
	 Vendor_Info vendor,vendor_2;
	 List<Vendor_Info> vendors_list;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		//===========Prepare Test Data:
		//1. Prepare test data for Vendor and Contact Info
		vendor = new Vendor_Info();
		
		vendor_2 = new Vendor_Info();
		
		vendors_list = new ArrayList<Vendor_Info>();
		
		vendors_list.add(vendor);
		
		vendors_list.add(vendor_2);
				
		vendor_2.SearchOpt_IsUSA = vendor.SearchOpt_IsUSA = true;
		
		vendor_2.Contacts.get(0).ServiceType = vendor.Contacts.get(0).ServiceType = "Appraiser";
		
		vendor_2.Contacts.get(0).Init_Vendor_Contact_Info(vendor_2.VendorName);
		
		vendor_2.Contacts.get(0).ServiceType = vendor.Contacts.get(0).ServiceType;
		
		//2. Prepare test data for Appraisal Service
		transferee = new Transferee_Info();
		
		transferee.Appraisal_service = new AppraisalService_Info(vendor.Contacts.get(0));
		
		transferee.Appraisal_service.VendorCtactList.add(vendor_2.Contacts.get(0));
		
		transferee.Appraisal_service.InIt_Appraisal_Analysis();
		
		//================END PREPARE TEST DATA
		
	}

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 053 Appraisal Analysis is updated successfully")
	public void TC_053_Appraisal_Analysis_is_updated_successfully() {
      
		//Test Data:
		//==========================================Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		for(Vendor_Info cur_vendor: vendors_list)
		{
			//1. Create Vendors #1,#2 and ContactS: CONTACT #1, #2
			Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(cur_vendor);
			 
			Admin.VendorManage.Update_Vendor_Search_Option_Tab(cur_vendor);
			 
			Admin.ContactManage.Create_New_Contact(cur_vendor.Contacts.get(0),true);
		}//end for
		
		
		//2. CREATE TRANSFEREE - ADD VENDOR CONTACT TO THE SERVICE
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		for(Vendor_Info cur_vendor: vendors_list)
		{
			Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.Appraisal_service.Type, cur_vendor.Contacts.get(0));
			
		}//end for
		
		//3. Update Services Details
	
		Admin.ServiceManage.Update_Appraisal_Analysis_Detail(transferee.Appraisal_service);
		
		Admin.ServiceManage.Verify_Appraisal_Analysis_Info_is_correct_ViewMode(transferee.Appraisal_service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}

}
