package testcases.ServiceManage_Appraisal;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.AppraisalService_Info;
import businessObjects.Contact_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;




@Category(Category_Services.class)
public class Appraisal_AssignedVendorContact_001_002_003_004  extends TestBase{
	
	
	 public Appraisal_AssignedVendorContact_001_002_003_004(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;
	 Transferee_Info transferee;
	 Vendor_Info vendor;
	 


	
	
	@Before
	public void setup()
	{
		
		super.setup();
		
		vendor = new Vendor_Info();
		
		vendor.SearchOpt_IsInter = true;
		
		vendor.Contacts.get(0).ServiceType = "Appraiser";
		
		transferee = new Transferee_Info();
		
		transferee.Appraisal_service = new AppraisalService_Info(vendor.Contacts.get(0));
		
		//Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor);
		 
		Admin.VendorManage.Update_Vendor_Search_Option_Tab(vendor);
		 
	}
	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_001: Appraisal_Verify Appraiser Vendor Contact Can Be Added Into The Service")
	public void TC_001_Appraisal_Verify_Appraiser_Contact_Can_Be_Added_Into_The_Service() {
		
			//Create Vendor Contact
			Admin.ContactManage.Create_New_Contact(vendor.Contacts.get(0),true);
			
			Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
			
			Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
			
			Admin.ServiceManage.Go_To_Service_Details(transferee.Appraisal_service.Type);
			
			Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.Appraisal_service.Type, vendor.Contacts.get(0));
		
			Admin.ServiceManage.Verify_Vendor_Contact_Shown_Service_Assigned_Contact_Table(transferee.Appraisal_service.Type, vendor.Contacts.get(0));
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_002: Appraisal_Verify Only Contact With Same Vendor Name Can Be Added Into Service")
	//@Issues({"#AA-25"})
	public void TC_002_Appraisal_Verify_Only_Contact_With_Same_Vendor_Name_Can_Added_Into_Service() {
		
		Contact_Info vendor_contact2 = new Contact_Info();
		
		vendor_contact2.Init_Vendor_Contact_Info(vendor.VendorName);
		
		vendor_contact2.ServiceType = vendor.Contacts.get(0).ServiceType;
		
		vendor.Contacts.add(vendor_contact2);
		
		//Create Vendor Contact
		for(int i =0;i<vendor.Contacts.size();i++)
			Admin.ContactManage.Create_New_Contact(vendor.Contacts.get(i),true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		Admin.ServiceManage.Go_To_Service_Details(transferee.Appraisal_service.Type);
		
		//Make sure Main contact is shown before adding the #2 to the Service
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transferee.Appraisal_service.Type, vendor.Contacts.get(1));
		
		//if the Validate Above is failed, stop the execution
		if(TestConfigs.glb_TCStatus==true)
		{
			Admin.ServiceManage.Select_VendorContact_On_CustomSearch(vendor.Contacts.get(0));
			
			Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transferee.Appraisal_service.Type, vendor.Contacts.get(1));
		}
	
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
			
	}
	
	@Test
	@Title("TC_003: Appraisal_Verify Vendor Contact Can Be Removed From Service")
	public void TC_003_Appraisal_Verify_Vendor_Contact_Can_Be_Removed_From_Service() {
		
			//Create Vendor Contact
			Admin.ContactManage.Create_New_Contact(vendor.Contacts.get(0),true);
			
			Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
			
			Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
			
			Admin.ServiceManage.Go_To_Service_Details(transferee.Appraisal_service.Type);
			
			Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.Appraisal_service.Type, vendor.Contacts.get(0));
		
			Admin.ServiceManage.Remove_VendorContact_On_VendorList(vendor.Contacts.get(0));
			
			Admin.ServiceManage.Verify_Vendor_Contact_Not_Shown_Service_Assigned_Contact_Table(transferee.Appraisal_service.Type, vendor.Contacts.get(0));
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_004: Appraisal_Verify AppraisalMnage Vendor Contact Can Be Added Into The Service")
	public void TC_004_Appraisal_Verify_AppraisalMnage_Contact_Can_Be_Added_Into_The_Service() {
		
			vendor.Contacts.get(0).ServiceType = "Appraisal Management";
			
			//Create Vendor Contact
			Admin.ContactManage.Create_New_Contact(vendor.Contacts.get(0),true);
			
			Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
			
			Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
			
			Admin.ServiceManage.Go_To_Service_Details(transferee.Appraisal_service.Type);
			
			Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.Appraisal_service.Type, vendor.Contacts.get(0));
		
			Admin.ServiceManage.Verify_Vendor_Contact_Shown_Service_Assigned_Contact_Table(transferee.Appraisal_service.Type, vendor.Contacts.get(0));
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	


}
