package testcases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.remote.DesiredCapabilities;

import configuration.CapacitiesFactory;
import configuration.TestConfigs;
import configuration.DriverConfig.DriverBase;
import configuration.DriverConfig.DriverCreator;
import dataDriven.glb_RefData;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.Concurrent;
import net.thucydides.junit.annotations.TestData;
import stepsDefiniton.User_AllSteps;

@RunWith(SerenityParameterizedRunner.class)
@Concurrent(threads = "10x")
public class TC_SandBox {

    protected String userRole = "ROLE";
    
    //public final Transferee_Info transf; 
    public static String Array[];
    
    public static TestConfigs Test_Configs;
	
	public final DriverBase exec_driver;
    
	public glb_RefData Glb_Variable;
	
	@Steps
	public User_AllSteps Admin;
   
    
    
    public TC_SandBox(DriverBase exec_driver) {
    	this.exec_driver = exec_driver;
    }

    @Before
    public void beforemethod()
    {
    	System.out.println("Before Run Method:"+ this.userRole);
    	Glb_Variable = new glb_RefData();
		
		//exec_driver.CreateDriver();
	
		Admin.GetConfig(exec_driver);
    }
    
    @TestData
    public static Collection<Object[]> testData() {
    	
	Test_Configs = new TestConfigs();
    	
    	//return data name elements
    	CapacitiesFactory capacity_config = new CapacitiesFactory();
    	
    	ArrayList<Object> cap_sets = null;
		try {
			cap_sets = capacity_config.getCapacitiesSet();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
       	Object driver_array[][] = new Object[cap_sets.size()][1];
       	DriverBase driver  =null;    	
    	for(Object cur_set:cap_sets)
    	{
    		DesiredCapabilities capacities = null;
			try {
				capacities = capacity_config.getDesiredCapabilities(cur_set);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		driver = DriverCreator.GetDriverBase(capacities);
    		
    		System.out.println("=========SUCCESS GetDriverBase");
    		
    		driver_array[cap_sets.indexOf(cur_set)][0] = driver;
    	
    	}
    	
    	
    	return Arrays.asList(new Object[][]{{driver}});
    }

    @Test
    public void test1() {
       
    	
    	System.out.println("test 1 for test 1");
    	
    	Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

   

}