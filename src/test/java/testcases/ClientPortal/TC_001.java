package testcases.ClientPortal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;



public class TC_001 extends TestBase {
	
	public TC_001(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Transferee_Info transf_info;
		
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
	}
	
	
	@Test
	@Title("Verify New Transferee is created successfully")
	 
	public void RunTest_001() {
      
		//Admin.Login_to_Client_Portal_as_client_user();
		
		//Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info);
		
		//Admin.TransfManage.Verify_transfee_is_shown_on_Tranferee_List(transf_info);
		
		//Admin.TransfManage.Verify_transfee_GeneralInfo_is_correct(transf_info);
		
		//Admin.Login_to_ARC_Portal_as_admin();
		
		//Admin.Admin_create_new_transfree();
		
		//TC_run.Then_new_transfee_is_shown_on_Tranferee_List();
		
		//TC_run.AND_info_transfee_is_shown_correctly();
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

}
