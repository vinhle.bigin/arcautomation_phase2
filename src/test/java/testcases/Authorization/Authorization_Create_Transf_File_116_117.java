
package testcases.Authorization;


import org.junit.Assert;
import org.junit.Before;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import org.openqa.selenium.remote.DesiredCapabilities;

import configuration.TestConfigs;

import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.ClientUser_Steps;

import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;
import baseClasses.TestBase;
import businessObjects.Auth_Info;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import businessObjects.Policy_Info;
import businessObjects.Transferee_Info;

public class Authorization_Create_Transf_File_116_117 extends TestBase {
	
	public Authorization_Create_Transf_File_116_117(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	
	Auth_Info auth_info;

	Transferee_Info transf;
	
	Policy_Info p_info;

	@Steps
	ClientUser_Steps Client;

	@Before
	public void setup() {
		super.setup();
		Client.GetConfig(exec_driver);
		
		Client_Info client = new Client_Info(new Contact_Info());

		p_info = new Policy_Info(client);
		
		p_info.client.Approval_option = "No Required";
		
		auth_info = new Auth_Info();
			
		auth_info.Policy = p_info.PolicyName;
		
		auth_info.Client = p_info.client;
		
		//Steps
		Admin.Login_to_ARC_Portal_as_admin();

		Admin.ClientManage.Create_New_Client(p_info.client);

		Admin.ClientManage.Edit_Client_Preference_Info_NoApprove(p_info.client, p_info.client.Contacts.get(0));

		Admin.ContactManage.Create_New_Contact(p_info.client.Contacts.get(0), true);

		Admin.ContactManage.Update_Contact_Profile_Account(p_info.client.Contacts.get(0));

		Admin.ClientManage.Create_New_Policy(p_info);

		Admin.Log_Out_From_Portal();

		//Create New Auth
		Client.Login_to_ClientPortal(p_info.client.Contacts.get(0).username, p_info.client.Contacts.get(0).password);

		Client.Go_To_Authorization_Page();

		Client.AuthManage.Create_New_Auth(auth_info);

		Client.Log_Out_From_Portal();

		Admin.Login_to_ARC_Portal_as_admin();

		Admin.Go_To_Authorization_Page();

	}

	@Category({ Catagory_SmokeTest.class, Category_E2E.class })
	@Test
	@Title("TC_116 - Verify New Transferee is created successfully")
	public void TC_116_New_Transferee_Is_Created_By_Auth_Successfully() {

		Transferee_Info trans_info = Admin.AuthManage.Finish_Creating_Transferee_File_from_Auth(auth_info);

		Admin.Go_to_Transferees_Page();

		Admin.TransfManage.Search_for_Transferee_With_Name(trans_info);
		
		Admin.TransfManage.Verify_transfee_is_shown_on_Tranferee_List(trans_info);
		
		Admin.TransfManage.Verify_transfee_GeneralInfo_is_correct(true, trans_info);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

	@Test
	@Title("TC_117 - Verify the new transferee is shown on Client Portal-Transferee List page when user completes creating file from authoriation")
	public void TC_117_New_Transferee_File_From_auth_is_Shown_on_Client_Portal() {

		Transferee_Info trans_info = Admin.AuthManage.Finish_Creating_Transferee_File_from_Auth(auth_info);

		Admin.Log_Out_From_Portal();

		Client.Login_to_ClientPortal(p_info.client.Contacts.get(0).username, p_info.client.Contacts.get(0).password);

		Client.Go_to_Transferees_Page();

		Client.TransfManage.Verify_transfee_is_shown_on_Tranferee_List(trans_info);

		Client.TransfManage.Verify_transfee_GeneralInfo_is_correct(false, trans_info);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
}
