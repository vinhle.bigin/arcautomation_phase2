/*
 * package testcases.Authorization;
 * 
 * 
 * import org.junit.Assert; import org.junit.Before;
 * 
 * import org.junit.Test; import org.junit.experimental.categories.Category;
 * import org.junit.runner.RunWith;
 * 
 * import configuration.DriverFactory; import
 * net.serenitybdd.junit.runners.SerenityRunner; import
 * net.thucydides.core.annotations.Steps; import
 * net.thucydides.core.annotations.Title; import objects.Auth_Info; import
 * objects.Client_Info; import objects.Contact_Info; import
 * objects.Transferee_Info; import stepsDefiniton.User_AllSteps; import
 * testcases.Catagory_SmokeTest; import testcases.TestBase; import
 * stepsDefiniton.ClientUser_Steps;
 * 
 * 
 * 
 *  public class Create_New_Auth_001_002_003_004
 * extends TestBase{
 * 
 * 
 * 
 * public Create_New_Auth_001_002_003_004(DriverFactory Driver_Config) {
 * super(glb_capacities); // TODO Auto-generated constructor stub }
 * 
 * Auth_Info auth_info; Client_Info client_info; Contact_Info ctact_info;
 * 
 * @Steps ClientUser_Steps Client;
 * 
 * 
 * @Before public void setup() { super.setup(); auth_info = new Auth_Info();
 * client_info = new Client_Info(); Client.GetConfig(Test_Configs);
 * Admin.Login_to_ARC_Portal_as_admin();
 * Admin.ClientManage.Create_New_Client(client_info);
 * 
 * ctact_info = client_info.Contacts.get(0);
 * 
 * Admin.ContactManage.Create_New_Contact(ctact_info,true);
 * 
 * Admin.ContactManage.Update_Contact_Profile_Account(ctact_info);
 * 
 * }
 * 
 * @Category(Catagory_SmokeTest.class)
 * 
 * @Test
 * 
 * @Title("TC_002_Verify New Auth is created successfully with NO APPROVAL REQUIRED at Client Portal"
 * )
 * 
 * public void
 * TC_002_New_Auth_is_created_successfully_at_Client_Portal_Approve() {
 * 
 * //Test data auth_info.AuthStatus = "Approved"; client_info.Approval_option =
 * "No Required";
 * 
 * 
 * //Steps
 * 
 * Admin.ClientManage.Edit_Client_Preference_Info_NoApprove(client_info,
 * ctact_info);
 * 
 * Client.Client_login_to_Client_Portal();
 * 
 * Admin.Go_To_Authorization_Page();
 * 
 * Client.AuthManage.Create_New_Auth(auth_info);
 * 
 * Client.AuthManage.Search_For_Existing_Auth(auth_info);
 * 
 * Client.AuthManage.Verify_Auth_Is_Displayed_On_Auth_List(auth_info);
 * 
 * Client.AuthManage.Verify_Auth_Details_Info_ViewMode(auth_info);
 * 
 * Assert.assertTrue(Test_Configs.glb_TCFailedMessage,
 * Test_Configs.glb_TCStatus); }
 * 
 * @Category(Catagory_SmokeTest.class)
 * 
 * @Test
 * 
 * @Title("TC_001_Verify New Auth is created successfully with ANY CONTACT CAN APPROVE at Client Portal"
 * )
 * 
 * public void
 * TC_001_New_Auth_is_created_successfully_at_Client_Portal_Anycontact() {
 * 
 * //Test data auth_info.AuthStatus = "Entered"; client_info.Approval_option =
 * "Any Contact";
 * 
 * //Steps Admin.Login_to_ARC_Portal_as_admin();
 * 
 * Admin.ClientManage.Edit_Client_Preference_Info_AnyContact(client_info);
 * 
 * Client.Client_login_to_Client_Portal();
 * 
 * Admin.Go_To_Authorization_Page();
 * 
 * Client.AuthManage.Create_New_Auth(auth_info);
 * 
 * Client.AuthManage.Search_For_Existing_Auth(auth_info);
 * 
 * Client.AuthManage.Verify_Auth_Is_Displayed_On_Auth_List(auth_info);
 * 
 * Client.AuthManage.Verify_Auth_Details_Info_ViewMode(auth_info);
 * 
 * Assert.assertTrue(Test_Configs.glb_TCFailedMessage,
 * Test_Configs.glb_TCStatus); }
 * 
 * @Category(Catagory_SmokeTest.class)
 * 
 * @Test
 * 
 * @Title("TC_004_Verify New Auth is created successfully with ONE OF SELECTED MUST APPROVE at Client Portal"
 * )
 * 
 * public void
 * TC_004_New_Auth_is_created_successfully_at_Client_Portal_Oneofselected() {
 * 
 * //Test data auth_info.AuthStatus = "Entered"; client_info.Approval_option =
 * "Specified Contact";
 * 
 * //Steps Admin.Login_to_ARC_Portal_as_admin();
 * 
 * Admin.ClientManage.Edit_Client_Preference_Info_Oneofselected(client_info);
 * 
 * Client.Client_login_to_Client_Portal();
 * 
 * Admin.Go_To_Authorization_Page();
 * 
 * Client.AuthManage.Create_New_Auth(auth_info);
 * 
 * Client.AuthManage.Search_For_Existing_Auth(auth_info);
 * 
 * Client.AuthManage.Verify_Auth_Is_Displayed_On_Auth_List(auth_info);
 * 
 * Client.AuthManage.Verify_Auth_Details_Info_ViewMode(auth_info);
 * 
 * Assert.assertTrue(Test_Configs.glb_TCFailedMessage,
 * Test_Configs.glb_TCStatus); }
 * 
 * @Category(Catagory_SmokeTest.class)
 * 
 * @Test
 * 
 * @Title("TC_003_Verify New Auth is created successfully with ENTERING CONTACT MUST APPROVE at Client Portal"
 * )
 * 
 * public void
 * TC_003_New_Auth_is_created_successfully_at_Client_Portal_Entering() { //Test
 * data auth_info.AuthStatus = "Entered"; client_info.Approval_option =
 * "Entering Contact";
 * 
 * //Steps Admin.Login_to_ARC_Portal_as_admin();
 * 
 * Admin.ClientManage.Edit_Client_Preference_Info_Entering(client_info);
 * 
 * Client.Client_login_to_Client_Portal();
 * 
 * Admin.Go_To_Authorization_Page();
 * 
 * Client.AuthManage.Create_New_Auth(auth_info);
 * 
 * Client.AuthManage.Search_For_Existing_Auth(auth_info);
 * 
 * Client.AuthManage.Verify_Auth_Is_Displayed_On_Auth_List(auth_info);
 * 
 * Client.AuthManage.Verify_Auth_Details_Info_ViewMode(auth_info);
 * 
 * Assert.assertTrue(Test_Configs.glb_TCFailedMessage,
 * Test_Configs.glb_TCStatus); }
 * 
 * }
 */