package testcases.Authorization;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Auth_Info;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import businessObjects.Policy_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.ClientUser_Steps;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;

public class Authorization_Approve_Assign_109 extends TestBase {

	public Authorization_Approve_Assign_109(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Auth_Info auth_info;
	
	Policy_Info p_info;

	@Steps
	ClientUser_Steps Client;

	@Before
	public void setup() {

		super.setup();

		auth_info = new Auth_Info();
		
		Client_Info client = new Client_Info(new Contact_Info());

		p_info = new Policy_Info(client);

		Client.GetConfig(exec_driver);

		Admin.Login_to_ARC_Portal_as_admin();

		Admin.ClientManage.Create_New_Client(p_info.client);

		Admin.ContactManage.Create_New_Contact(p_info.client.Contacts.get(0), true);

		Admin.ContactManage.Update_Contact_Profile_Account(p_info.client.Contacts.get(0));

	}

	@Category(Category_E2E.class)
	@Test
	@Title("TC_109 - Verify Auth is assigned by admin successfully")
	public void TC_109_Auth_Is_Assigned_By_Admin_Successfully() {
		// Test data
		auth_info.AuthStatus = "Approved";

		p_info.client.Approval_option = "No Required";
		
		auth_info.Policy = p_info.PolicyName;

		// Steps

		Admin.ClientManage.Edit_Client_Preference_Info_NoApprove(p_info.client, p_info.client.Contacts.get(0));

		Admin.ClientManage.Create_New_Policy(p_info);

		Admin.Log_Out_From_Portal();

		Client.Login_to_ClientPortal(p_info.client.Contacts.get(0).username, p_info.client.Contacts.get(0).password);

		Admin.Go_To_Authorization_Page();

		Client.AuthManage.Create_New_Auth(auth_info);

		Admin.Login_to_ARC_Portal_as_admin();

		Admin.Go_To_Authorization_Page();

		Admin.AuthManage.Search_For_Existing_Auth(auth_info);

		Admin.AuthManage.Goto_Detail_Auth(auth_info);

		Admin.AuthManage.Assign_Auth(auth_info);

		auth_info.AuthStatus = "Assigned";

		Client.AuthManage.Verify_Auth_Details_Info_ViewMode(auth_info);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

	//@Category(Category_E2E.class)
	@Test
	@Title("TC_110 - Verify Auth is assigned by client successfully")
	public void TC_110_Auth_Is_Assigned_By_Client_Successfully() {
		Client.Client_login_to_Client_Portal();

		Admin.Go_To_Authorization_Page();

		Admin.AuthManage.Create_New_Auth(auth_info);

		Admin.Login_to_ARC_Portal_as_admin();
		Admin.Go_To_Authorization_Page();

		Admin.AuthManage.Search_For_Existing_Auth(auth_info);

		Admin.AuthManage.Goto_Detail_Auth(auth_info);

		Admin.AuthManage.Approve_Auth(auth_info);

		Admin.AuthManage.Search_For_Existing_Auth(auth_info);

		Admin.AuthManage.Goto_Detail_Auth(auth_info);

		Admin.AuthManage.Assign_Auth(auth_info);

		Client.AuthManage.Verify_Auth_Details_Info_ViewMode(auth_info);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

	//@Category(Category_E2E.class)
	@Test
	@Title("TC_111 - Verify Auth is deleted successfully")
	public void TC_111_Auth_Delete_Successfully() {
		Client.Client_login_to_Client_Portal();

		Admin.Go_To_Authorization_Page();

		Admin.AuthManage.Create_New_Auth(auth_info);

		Admin.Login_to_ARC_Portal_as_admin();
		Admin.Go_To_Authorization_Page();

		Admin.AuthManage.Search_For_Existing_Auth(auth_info);

		Admin.AuthManage.Goto_Detail_Auth(auth_info);

		Admin.AuthManage.Delete_Existing_Auth(auth_info);

		Admin.AuthManage.Verify_Auth_Is_Not_Displayed_On_Auth_List(auth_info);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

	//@Category(Category_E2E.class)
	@Test
	@Title("TC_112 - Verify Auth is approved by admin successfully")
	public void TC_112_Approve_Auth_by_Admin() {
		Client.Client_login_to_Client_Portal();

		Admin.Go_To_Authorization_Page();

		Admin.AuthManage.Create_New_Auth(auth_info);

		Admin.Login_to_ARC_Portal_as_admin();

		Admin.Go_To_Authorization_Page();

		Admin.AuthManage.Search_For_Existing_Auth(auth_info);

		Admin.AuthManage.Goto_Detail_Auth(auth_info);

		Admin.AuthManage.Approve_Auth(auth_info);

		Client.AuthManage.Verify_Auth_Details_Info_ViewMode(auth_info);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

}// endclass
