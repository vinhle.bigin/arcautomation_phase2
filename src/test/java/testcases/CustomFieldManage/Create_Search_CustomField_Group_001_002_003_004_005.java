package testcases.CustomFieldManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.CustomGroup_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;


public class Create_Search_CustomField_Group_001_002_003_004_005 extends TestBase{

	public Create_Search_CustomField_Group_001_002_003_004_005(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	TestConfigs test_cfig;
	
	CustomGroup_Info group_info;
	
	@Before
	public void setup()
	{
		super.setup();
		
		group_info = new CustomGroup_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	
	}
	
	@Test
	@Title("TC_001: Verify New Custom Field Group is created successfully with Transferee type and Custom Fields tab")
	public void TC_001_Verify_New_Group_created_with_Transferee_type_and_CustomFields_tab() 
	{
		Admin.CustomFieldManage.Create_New_Custom_Field_Group(group_info);
		
		Admin.CustomFieldManage.Search_for_New_CFGroup(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup_ExistTable(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup(group_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_002: Verify New Custom Field Group is created successfully with Transferee type and Address tab")
	public void TC_002_Verify_New_CustomField_Group_is_created_successfully_with_Transferee_type_and_Address_tab() 
	{
		group_info.Tab = "Address";
		
		Admin.CustomFieldManage.Create_New_Custom_Field_Group(group_info);
		
		Admin.CustomFieldManage.Search_for_New_CFGroup(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup_ExistTable(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup(group_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_003: Verify New Custom Field Group is created successfully with Transferee type and Address tab")
	public void TC_003_Verify_New_CustomField_Group_is_created_successfully_with_Transferee_type_and_Address_tab() 
	{
		group_info.Tab = "Bank Account";
		
		Admin.CustomFieldManage.Create_New_Custom_Field_Group(group_info);
		
		Admin.CustomFieldManage.Search_for_New_CFGroup(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup_ExistTable(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup(group_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_004: Verify New Custom Field Group is created successfully with Transferee type and Dependents tab")
	public void TC_004_Verify_New_CustomField_Group_is_created_successfully_with_Transferee_type_and_Dependents_tab() 
	{
		group_info.Tab = "Dependents";
		
		Admin.CustomFieldManage.Create_New_Custom_Field_Group(group_info);
		
		Admin.CustomFieldManage.Search_for_New_CFGroup(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup_ExistTable(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup(group_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_005: Verify New Custom Field Group is created successfully with Transferee type and General Info tab")
	public void TC_005_Verify_New_CustomField_Group_is_created_successfully_with_Transferee_type_and_GeneralInfo_tab() 
	{
		group_info.Tab = "General Info";
		
		Admin.CustomFieldManage.Create_New_Custom_Field_Group(group_info);
		
		Admin.CustomFieldManage.Search_for_New_CFGroup(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup_ExistTable(group_info);
		
		Admin.CustomFieldManage.Verify_CFGroup(group_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
}

