package testcases.CustomFieldManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import businessObjects.CustomField_Info;
import businessObjects.CustomGroup_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;


public class Create_CustomField_Transf_Type_006_007_008_009_010 extends TestBase{

	

	public Create_CustomField_Transf_Type_006_007_008_009_010(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	TestConfigs test_cfig;
	
	Client_Info client_info;
	
	Transferee_Info transf;
	
	CustomGroup_Info group_info;
	
	CustomField_Info field_info;
	
	@Before
	public void setup()
	{
		super.setup();
		
		client_info = new Client_Info();
		
		transf = new Transferee_Info();
		
		group_info = new CustomGroup_Info();
		
		field_info = new CustomField_Info();
		
		transf.Client = client_info;
		
		Admin.Login_to_ARC_Portal_as_admin();	
		
        Admin.ClientManage.Create_New_Client(client_info);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
	}
	
	@Test
	@Title("TC_006: Verify New Custom Field is created successfully with Textbox type")
	public void TC_006_Verify_New_CustomField_created_with_Textbox_type() 
	{
		field_info.ControlType = "Textbox";
	
		group_info.Tab = "Address";
		
		CustomField_Info custom_field = new CustomField_Info(field_info.ControlType, group_info);
		
		custom_field.client.CompanyName = client_info.CompanyName;
		
		custom_field.custom_group.Tab = field_info.ControlType;
		
		custom_field.custom_group.Name = group_info.Name;

		Admin.CustomFieldManage.Create_New_Custom_Field_Group(group_info);
		
		Admin.CustomFieldManage.Create_New_Custom_Field(custom_field);
		
		Admin.CustomFieldManage.Verify_Custom_Field_Details_Is_Correct_On_ViewMode(custom_field);
		
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Search_for_Transferee_With_Name(transf);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf);
		
		Admin.TransfManage.Goto_AddressTab();
		
		Admin.CustomFieldManage.Verify_CustomField_Display_TransferDetails(custom_field);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_007: Verify New Custom Field is created successfully with Checkbox type")
	public void TC_007_Verify_New_CustomField_created_with_Textbox_type() 
	{
		field_info.ControlType = "Checkbox";
		
		Admin.CustomFieldManage.Create_New_Custom_Field(field_info);
		
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Search_for_Transferee_With_Name(transf);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_008: Verify New Custom Field is created successfully with Numeric Field type")
	public void TC_008_Verify_New_CustomField_created_with_NumericField_type() 
	{
		field_info.ControlType = "Numeric Field";
		
		Admin.CustomFieldManage.Create_New_Custom_Field(field_info);
		
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Search_for_Transferee_With_Name(transf);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_009: Verify New Custom Field is created successfully with Date Picker type")
	public void TC_009_Verify_New_CustomField_created_with_DatePicker_type() 
	{
		field_info.ControlType = "Date Picker";
		
		Admin.CustomFieldManage.Create_New_Custom_Field(field_info);
		
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Search_for_Transferee_With_Name(transf);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
}
