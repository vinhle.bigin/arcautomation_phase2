package testcases.TransfPortal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class})
public class TransfPortal_Profile_Update_Password_Section_016_017_018  extends TestBase{

	
	public TransfPortal_Profile_Update_Password_Section_016_017_018(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf;
	
	Note_Info note_Info;
	
	@Steps
	TransfUser_Steps Transferee;
	


	
	@Before
	public void setup()
	{
		
		super.setup();
		
		Transferee.GetConfig(exec_driver);
		
		transf = new Transferee_Info();
		
		note_Info = new Note_Info();
		
	
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf);
		
		Admin.Log_Out_From_Portal();
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
	}
	

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 017 Cancel Password Section Updates")
	public void TC_017_Cancel_Password_Section_Updates() 
	{
		Transferee.Go_to_Profile_Page();
		
		transf.ReGenenrateInfo();
		
		Transferee.TransfManage.Cancel_Password_Section_Updates(transf);
		
		Transferee.Log_Out_From_Portal();
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Verify_Account_Can_Login("");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC 018 Update Password Section")
	public void TC_018_Update_Password_Section() 
	{
		Transferee.Go_to_Profile_Page();
		
		transf.ReGenenrateInfo();
		
		Transferee.TransfManage.Update_Password_Section(transf);
		
		Transferee.Log_Out_From_Portal();
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.new_password);
		
		Transferee.Verify_Account_Can_Login("");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}


}
