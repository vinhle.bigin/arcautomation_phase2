package testcases.TransfPortal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class})
public class TransfPortal_Profile_Update_TimeZone_Section_011  extends TestBase{

	
	public TransfPortal_Profile_Update_TimeZone_Section_011(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf;
	
	Note_Info note_Info;
	
	@Steps
	TransfUser_Steps Transferee;
	

	@Before
	public void setup()
	{
	
		super.setup();
		
		Transferee.GetConfig(exec_driver);
		
		transf = new Transferee_Info();
		
		note_Info = new Note_Info();
		
	
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf);
		
		Admin.Log_Out_From_Portal();
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
	}
	

	
	@Test
	@Title("TC 011 Update Time Zone Section")
	@Issues({"#AA-43"})
	public void TC_011_Update_TimeZone_Section() 
	{
		Transferee.Go_to_Profile_Page();
		
		transf.ReGenenrateInfo();
		
		Transferee.TransfManage.Update_TimeZone_Section(transf);
		
		Transferee.TransfManage.Verify_TimeZone_Section(transf);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

}
