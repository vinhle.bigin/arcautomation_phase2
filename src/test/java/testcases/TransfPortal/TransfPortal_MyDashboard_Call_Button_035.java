package testcases.TransfPortal;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;



public class TransfPortal_MyDashboard_Call_Button_035  extends TestBase{

public TransfPortal_MyDashboard_Call_Button_035(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf;
	
	@Steps
	TransfUser_Steps Transferee;
	
	
	
	@Before
	public void setup()
	{
		super.setup();
		Transferee.GetConfig(exec_driver);
		
		transf.MailList.get(0).EmailAddr = glb_RefData.glb_TestMailAccount;
		
		transf = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf);
		
		Admin.Log_Out_From_Portal();
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
	}
	
	
	
	@Test
	@Title("TC 003 Verify data in all sections on Transferee Portal when inputting data on ARC Portal")
	public void TC_003_Verify_Data_All_Sections_On_TransfPortal() 
	{
		
	}
}
