package testcases.TransfPortal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Document_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Catagory_Documents;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Documents.class})
public class TransfPortal_MyInfo_Document_Manage_026_029_031_034  extends TestBase {

	
	
	public TransfPortal_MyInfo_Document_Manage_026_029_031_034(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	@Steps
	TransfUser_Steps Transferee;
	


	@Before
	public void setup()
	{
		super.setup();
		
		Transferee.GetConfig(exec_driver);
				
		transf_info = new Transferee_Info();
		transf_info.permision_CreateDoc = "Allow";
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
	}

	
	
	@Category({Catagory_SmokeTest.class,Category_E2E.class})
	@Test
	@Title("TC 026 Verify New Document Can Be Created for Transferee")
	public void TC_026_Verify_New_Document_Can_Be_Created_For_Transferee() 
	{
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Document_Info document = new Document_Info();
		
		FileManage file_mnage  = new FileManage();
		
		file_mnage.CreateWordFile_func(document.file_name, document.description);
		
		Transferee.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Transferee.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Transferee.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Transferee.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC_029_TransfereeUser_Can_View_New_Document")
	public void TC_029_TransfereeUser_Can_View_New_Document() {
		
		Document_Info document = new Document_Info();
		
		document.TransView = true;
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Transferee.Go_To_Document_Tab();
		
		Transferee.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Transferee.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Transferee.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_031_Verify Transferee Document Can Be Updated")
	public void TC_031_Verify_Transferee_Document_Can_Be_Updated() 
	{
		Document_Info document = new Document_Info();
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Transferee.DocumentManage.Create_New_Document_from_Doc_Tab(document);
				
		Transferee.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		//Update document
		document.ReGenerate_Info();
		
		Transferee.DocumentManage.Update_Existing_Document(document);
		
		Transferee.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Transferee.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC_034_Verify Transferee Document file Can Be Downloaded")
	public void TC_034_Verify_Document_Can_Be_Downloaded() 
	{
		
		String Expect_content = "Document's Testing Content";
		
		Document_Info document = new Document_Info();
		
		new FileManage().CreateWordFile_func(document.file_name, Expect_content);
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Transferee.Go_To_Document_Tab();
		
		Transferee.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Transferee.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Transferee.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		//Verify
		if(TestConfigs.glb_TCStatus!=false)
		{
			Transferee.DocumentManage.Download_Document(document.file_name, "");
			
			Transferee.DocumentManage.Verify_Downloaded_File_Content_Is_Correct(document.file_name,Expect_content);
			
			//Post-condition-Delete the downloaded file:
			new FileManage().Delete_Downloaded_File_func(document.file_name);
		
		}
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}

}
