package testcases.TransfPortal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class})
public class TransfPortal_Profile_Update_User_Profile_section_008  extends TestBase{

	
	public TransfPortal_Profile_Update_User_Profile_section_008(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf;
	
	@Steps
	TransfUser_Steps Transferee;
	
	
	
	@Before
	public void setup()
	{
		super.setup();
	
		Transferee.GetConfig(exec_driver);
		
		transf = new Transferee_Info();
		
	
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf);
		
		Admin.Log_Out_From_Portal();
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
	}
	
	
	
	@Test
	@Title("TC 003 Verify data in all sections on Transferee Portal when inputting data on ARC Portal")
	public void TC_003_Verify_Data_All_Sections_On_TransfPortal() 
	{
		Transferee.Go_to_Profile_Page();
		
		Transferee.TransfManage.Verify_User_Profile(transf);
		
		Transferee.TransfManage.Verify_PhonesEmails_Section(transf);
		
		Transferee.TransfManage.Verify_Fullname_in_Sidebar(transf);
		
		Transferee.TransfManage.Verify_Fullname_in_TopMenu(transf);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC 008 Update User Profile Section")
	public void TC_008_Update_User_Profile_Section() 
	{
		Transferee.Go_to_Profile_Page();
		
		transf.ReGenenrateInfo();
		
		Transferee.TransfManage.Update_User_Profile_Section(transf);	
		
		Transferee.TransfManage.Update_PhoneEmail_Section(transf);
		
		Transferee.TransfManage.Verify_User_Profile(transf);
		
		Transferee.TransfManage.Verify_transfee_GeneralInfo_is_correct(true,transf);
		
		Transferee.TransfManage.Verify_Fullname_in_Sidebar(transf);
		
		Transferee.TransfManage.Verify_Fullname_in_TopMenu(transf);
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Search_for_Transferee_With_Name(transf);
		
		Transferee.TransfManage.Verify_transfee_GeneralInfo_is_correct(false,transf);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	

}
