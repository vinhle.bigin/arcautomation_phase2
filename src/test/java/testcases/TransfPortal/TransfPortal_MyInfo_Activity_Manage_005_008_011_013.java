package testcases.TransfPortal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Catagory_Activity;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Activity.class})
public class TransfPortal_MyInfo_Activity_Manage_005_008_011_013  extends TestBase{
	
	
	
	public TransfPortal_MyInfo_Activity_Manage_005_008_011_013(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf;
	
	@Steps
	TransfUser_Steps Transferee;
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		Transferee.GetConfig(exec_driver);
		transf = new Transferee_Info();
		
		
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf);
		
	}

	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 005 Create New Activity in top Short Cut - New note can be add successfully")
	public void TC_005_Create_New_Activity_in_Top_Short_Cut() {
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Go_to_Transferees_Page();
      
		Activity_Info activity = new Activity_Info();
		
		activity.Type = "Follow Up";
		
		activity.Assignee = transf.FullName;
		
		Transferee.ActivityManage.Create_Activity_Via_Heading_Shortcut(activity);
		
		Transferee.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Transferee.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Transferee.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
		
	@Test
	@Title("TC 008 Create New Activity in Activity Tab - New note can be add successfully")
	public void TC_008_Create_New_Activity_in_Activity_Tab() {
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Go_to_Transferees_Page();
      
		Activity_Info activity = new Activity_Info();
		
		activity.Assignee  = transf.FullName;
		
		Transferee.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
			
		Transferee.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Transferee.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Transferee.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_011_TransfereeUser_Can_View_New_Activity")
	public void TC_011_TransfereeUser_Can_View_New_Activity() {
		
		Activity_Info activity = new Activity_Info();
      
		activity.Assignee = transf.FullName;
		
		activity.TransfereeView = true;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC 013 Update Activity in Activity Tab")
	public void TC_013_Update_Activity_in_Activity_Tab() {
		
		Activity_Info activity = new Activity_Info();
		
		Activity_Info activity_2 = new Activity_Info();
		
		activity_2.Type = "Follow Up";
		
		activity_2.Assignee  = transf.FullName;
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Transferee.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Transferee.ActivityManage.Search_For_Activity_On_Activity_Tab(activity_2);
				
		Transferee.ActivityManage.Update_Activity(activity.Title, activity_2);
		
		Transferee.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity_2);
		
		Transferee.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity_2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

}
