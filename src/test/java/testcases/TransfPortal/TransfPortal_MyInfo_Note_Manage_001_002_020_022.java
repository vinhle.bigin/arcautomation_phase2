package testcases.TransfPortal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Catagory_Notes;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Notes.class})
public class TransfPortal_MyInfo_Note_Manage_001_002_020_022  extends TestBase{
	

	
	public TransfPortal_MyInfo_Note_Manage_001_002_020_022(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf;
	
	Note_Info note_Info;
	
	@Steps
	TransfUser_Steps Transferee;
	
	

	@Before
	public void setup()
	{
		super.setup();
	
		Transferee.GetConfig(exec_driver);
		
		transf = new Transferee_Info();
		
		note_Info = new Note_Info();
		
	
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf);
		
	}


	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 002 Create New Note in top Short Cut - New note can be add successfully")
	public void TC_002_Create_New_Note_in_Top_Short_Cut() {
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Go_to_Transferees_Page();
      
		Transferee.NoteManage.Create_Note_via_Top_Note_Shortcut(note_Info);
		
		Transferee.Go_to_Note_Tab();

		Transferee.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Transferee.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
		
	@Test
	@Title("TC 001 Create New Note in Notes Tab - New note can be add successfully")
	public void TC_001_Create_New_Note_in_Notes_Tab() {
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Go_to_Transferees_Page();
      
		Transferee.NoteManage.Create_New_Note_From_Note_Tab(note_Info);

		Transferee.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Transferee.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_020_TransfereeUser_Can_View_New_Note")
	public void TC_020_TransfereeUser_Can_View_New_Note() {
      
		Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Transferee.Go_to_Note_Tab();
		
		Transferee.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Transferee.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC 022 Update Note in Notes Tab")
	public void TC_022_Update_Note_in_Notes_Tab() {
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Note_Info note_Info2 = new Note_Info();
      
		Transferee.NoteManage.Create_New_Note_From_Note_Tab(note_Info);		
		
		Transferee.NoteManage.Update_Exiting_Note(note_Info.Title, note_Info2);
	
		Transferee.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

}
