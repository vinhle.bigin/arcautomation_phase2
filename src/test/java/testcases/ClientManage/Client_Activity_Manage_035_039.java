package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;
import testcases.Catagory_SmokeTest;


public class Client_Activity_Manage_035_039 extends TestBase{

public Client_Activity_Manage_035_039(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


public Client_Info client_info;



@Before
public void setup()
{
	super.setup();
	
	client_info = new Client_Info();
	
	Admin.Login_to_ARC_Portal_as_admin();
	
	Admin.ClientManage.Create_New_Client(client_info);
	
	
}


	
	
	@Category({Catagory_SmokeTest.class,Catagory_Activity.class})
	@Test
	@Title("TC: 035_New_Activity can be added for transferee Via Heading Shortcut")
	@Issues({"#AA-25"})
	public void TC_035_New_Client_Activity_can_be_added_Via_Heading_Shortcut() {
      
		Activity_Info activity = new Activity_Info();
		activity.Type = "Follow Up";
		activity.AssigneeType = "Client";
		activity.Assignee = client_info.CompanyName;
		
		Admin.ActivityManage.Create_Activity_Via_Heading_Shortcut(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Category({Catagory_Activity.class})
	@Title("TC: 039_Verify Client Activity can be deleted")
	public void TC_039_Verify_Client_Activity_Can_Be_Deleted()
	{
		Activity_Info activity = new Activity_Info();
		activity.AssigneeType = "Client";
		activity.Assignee = client_info.CompanyName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Delete_Activity(activity);
		
		Admin.ActivityManage.Verify_Activity_Is_NOT_Shown_On_Table(activity,"After being deleted");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	


}
