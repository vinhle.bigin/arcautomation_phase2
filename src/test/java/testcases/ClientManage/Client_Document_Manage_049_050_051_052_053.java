package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import businessObjects.Document_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Documents;
import testcases.Catagory_SmokeTest;




@Category(Catagory_Documents.class)
public class Client_Document_Manage_049_050_051_052_053  extends TestBase{

	
	
	public Client_Document_Manage_049_050_051_052_053(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	TestConfigs test_cfig;
	Client_Info client_info;
	

	@Before
	public void setup()
	{
	super.setup();
	
	client_info = new Client_Info();

	Admin.Login_to_ARC_Portal_as_admin();
	}

		
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC:049_Verify New Document Can Be Created for Client")
	public void TC_049_Verify_New_Document_Can_Be_Created_For_Client() 
	{
		Document_Info document = new Document_Info();
			
		//Admin.ClientManage.Go_To_Client_Details_Page(client_info.CompanyName);
		
		Admin.ClientManage.Create_New_Client(client_info);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Admin.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	
	}

	@Test
	@Title("TC:050_Verify Client Document Can Be Updated")
	public void TC_050_Verify_Client_Document_Can_Be_Updated() 
	{
		Document_Info document = new Document_Info();
		
		Document_Info document2 = new Document_Info();
		
		//Admin.ClientManage.Go_To_Client_Details_Page(client_info.CompanyName);
		
		Admin.ClientManage.Create_New_Client(client_info);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		//Update document
		document2.ReGenerate_Info();
		
		Admin.DocumentManage.Update_Existing_Document(document2);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document2.file_name);
		
		Admin.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC:051_Verify Client Document Can Be Removed")
	public void TC_051_Verify_Client_Document_Can_Be_Removed() 
	{
		Document_Info document = new Document_Info();
		
		//Admin.ClientManage.Go_To_Client_Details_Page(client_info.CompanyName);
		
		Admin.ClientManage.Create_New_Client(client_info);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		//Remove document
	
		Admin.DocumentManage.Remove_Document(document.file_name);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_NOT_Shown_On_List(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC:053_Verify Document file Can Be Downloaded") //Test case would be failed because Access is denied.
	public void TC_053_Verify_Document_Can_Be_Downloaded() 
	{
		//==TEST DATA
		//String destination_file_name = "DownloadFile_doc.docx";
		
		String Expect_content = "Document's Testing Content";
		
		Document_Info document = new Document_Info();
		
		new FileManage().CreateWordFile_func(document.file_name, Expect_content);
		
		//=======Steps:
		
		//Admin.ClientManage.Go_To_Client_Details_Page(client_info.CompanyName);
		
		Admin.ClientManage.Create_New_Client(client_info);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		//VErify
		if(TestConfigs.glb_TCStatus!=false)
		{
			Admin.DocumentManage.Download_Document(document.file_name, "");
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Admin.DocumentManage.Verify_Downloaded_File_Content_Is_Correct(document.file_name,Expect_content);
			
			//Post-condition-Delete the downloaded file:
			//new FileManage().Delete_Downloaded_File_func(document.file_name);
		
		}
		
		
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC:052_Verify Document Can Be Search For With Title")
	public void TC_052_Verify_Document_Can_Be_Search_For_With_Title() 
	{
		String Expect_content = "Document's Testing Content";
		
		Document_Info document = new Document_Info();
		
		Document_Info document_2 = new Document_Info();
		document_2.file_name = "Testing2doc.docx";
		
		//Admin.ClientManage.Go_To_Client_Details_Page(client_info.CompanyName);
		
		Admin.ClientManage.Create_New_Client(client_info);
		
		for(int i =0;i<2;i++)
		{
			new FileManage().CreateWordFile_func(document.file_name, Expect_content);
			
			Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		}
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Admin.DocumentManage.Verify_Document_Info_NOT_Shown_On_List(document_2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	

}
