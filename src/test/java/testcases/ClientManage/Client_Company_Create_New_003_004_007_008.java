package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_ClientManage;
import testcases.Category_E2E;



@Category(Category_ClientManage.class)
public class Client_Company_Create_New_003_004_007_008  extends TestBase{
	

	public Client_Company_Create_New_003_004_007_008(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Client_Info client_info;
	
	

	
	
	@Before
	public void setup()
	{
		super.setup();
		client_info = new Client_Info();
		
	}
	

	@Category({Catagory_SmokeTest.class,Category_E2E.class})
	@Test
	@Title("TC_007: Verify New Client can be created from New Client button")
	 
	public void TC_007_Verify_New_Client_can_be_Created_From_New_Client_button() {
      
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
        
        Admin.Go_to_Client_Page();
        
        Admin.ClientManage.Search_For_Existing_Client(client_info);
                   
        Admin.ClientManage.Verify_Client_Is_Displayed_On_Clients_List(client_info);
        
        Admin.ClientManage.Verify_Client_General_Info_Is_Correct_On_ViewMode(client_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_008: Verify New Client can be created from New shortcut icon")
	 
	public void TC_008_Verify_New_Client_can_be_Created_From_New_shortcut_icon() {
      
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
        
        Admin.Go_to_Client_Page();
        
        Admin.ClientManage.Search_For_Existing_Client(client_info);
                   
        Admin.ClientManage.Verify_Client_Is_Displayed_On_Clients_List(client_info);
        
        Admin.ClientManage.Verify_Client_General_Info_Is_Correct_On_ViewMode(client_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_003: Client Address cant be created successfully")
	 
	public void TC_003_Client_Address_cant_Be_created_Successfully() {
      
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
        
        Admin.ClientManage.Create_Client_Address_Info(client_info.AddrInfo.get(0));
        
        Admin.ClientManage.Verify_Client_Address_Info_Is_Correct_On_ViewMode(client_info.AddrInfo.get(0));
                   
        Admin.ClientManage.Verify_Client_Address_Info_Shown_On_Table(client_info.AddrInfo.get(0));
        
     	Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_004: Default Address Is Generated After Client Is Created")
	 
	public void TC_004_Default_Address_Is_Generated_After_Client_Is_Created() {
      
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
        
        Admin.ClientManage.Verify_Address_Auto_Generated_After_Client_Created();
                
     	Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

}
