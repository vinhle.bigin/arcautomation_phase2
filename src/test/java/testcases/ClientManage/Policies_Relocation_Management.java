package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import businessObjects.Policy_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.ClientUser_Steps;



public class Policies_Relocation_Management extends TestBase{
	
	

	public Policies_Relocation_Management(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	//TestConfigs test_cfig;
	//Client_Info client_info;
	Policy_Info policy_info;
	
	@Steps
	ClientUser_Steps Client;
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		policy_info = new Policy_Info(new Client_Info());
		
		//Steps
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.ClientManage.Create_New_Client(policy_info.client);
		
		Admin.ContactManage.Create_New_Contact(policy_info.client.Contacts.get(0), true);
		
	}

		
		@Test
		@Title("TC: 042_Verify Admin Can Be Created New Relocation Policy")
		public void TC_042_Verify_Admin_Can_Be_Created_New_Relocation_Policy() {
			
			Admin.ClientManage.Create_New_Policy(policy_info);
			
			Admin.ClientManage.Search_For_Policy_With_Title(policy_info.PolicyName);
			
			Admin.ClientManage.Verify_Policy_Is_Displayed_On_Policies_List(policy_info);
			
			Admin.ClientManage.Verify_Modal_Policy_Info_Is_Correct(policy_info);
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }
		
		@Test
		@Title("TC: 042_Verify Admin Can Be Edited Relocation Policy")
		public void TC_042_Verify_Admin_Can_Be_Edited_Relocation_Policy() {
			
			Admin.ClientManage.Create_New_Policy(policy_info);
			
			Admin.ClientManage.Search_For_Policy_With_Title(policy_info.PolicyName);
			
			Admin.ClientManage.Verify_Policy_Is_Displayed_On_Policies_List(policy_info);
			
			Admin.ClientManage.Verify_Modal_Policy_Info_Is_Correct(policy_info);
			
			policy_info.ReGenerate_Policy();
			
			Admin.ClientManage.Update_Policy_Info(policy_info);
			
			Admin.ClientManage.Verify_Policy_Is_Displayed_On_Policies_List(policy_info);
			
			Admin.ClientManage.Verify_Modal_Policy_Info_Is_Correct(policy_info);
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }
		
		

	
}
