package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_ClientManage;



@Category(Category_ClientManage.class)
public class Client_Contact_Edit_Information_023_024 extends TestBase {
	
	public Client_Contact_Edit_Information_023_024(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	TestConfigs test_cfig;
	Client_Info client_info;
	
	
	
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		client_info = new Client_Info(new Contact_Info());
		
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
		
	}

	@Test
	@Title("TC:023_Verify Existing Client Contact can be updated")
	 
	public void TC_023_Verify_Existing_Contact_Can_Be_Update() {
      
		Contact_Info ctact_info = client_info.Contacts.get(0);
		
		Contact_Info ctact_info_2 = new Contact_Info();
		
		ctact_info_2.ClientName = ctact_info.ClientName;
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		//Update Contact
		Admin.ContactManage.Update_Contact(ctact_info.FullName,ctact_info_2);
		
		//Verify old contact info is not shown on table
		 Admin.ContactManage.Verify_Contact_NOT_Shown_On_Table(ctact_info);
                   
		///Verify NEW contact info is shown on table 
        Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info_2);
        
        Admin.ContactManage.Verify_Modal_Contact_Detail_Is_Correct(ctact_info_2);
        
        Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

	@Test
	@Title("TC:024_Verify Existing Client Contact can be updated the account")
	
	public void TC_024_Verify_Existing_Contact_Can_Be_Update_The_Account() {
	      
		Contact_Info ctact_info = client_info.Contacts.get(0);
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
                   
        Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
        
        Admin.ContactManage.Update_Contact_Account(ctact_info);
        
        Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
    }
}