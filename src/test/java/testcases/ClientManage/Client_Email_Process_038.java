package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import businessObjects.Mail_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.ClientUser_Steps;
import testcases.Catagory_SmokeTest;
import testcases.Catagory_Template;
import testcases.Category_E2E;


@Category(Catagory_Template.class)
public class Client_Email_Process_038  extends TestBase{
	
	
	

	public Client_Email_Process_038(DesiredCapabilities glb_capacities) {
		// TODO Auto-generated constructor stub
		super(glb_capacities);
	}
	
	public Client_Info client_info;
	Transferee_Info transf_info;
	Template_Info newtpl;
	

	@Steps
	public ClientUser_Steps client_user;

	



	@Before
	public void setup()
	{
		super.setup();
		
		newtpl = new Template_Info();
		newtpl.Target = "Client";
		
		client_info = new Client_Info(new Contact_Info() );
		
		client_info.Contacts.get(0).email_info.EmailAddr =glb_RefData.glb_TestMailAccount;
		
		transf_info = new Transferee_Info();
		transf_info.Client = client_info;
		
	}


	@Category({Catagory_SmokeTest.class,Category_E2E.class})
		@Test
		@Title("TC_038_Verify Template Email without document can be sent to Client successfully")
		public void TC_038_Verify_Template_Email_without_document_can_be_sent_to_client() {
	      
			
			Mail_Info data_mail = client_info.Contacts.get(0).email_info;
			
			Admin.Login_to_ARC_Portal_as_admin();
			
			//Create new template
			Admin.TemplateManage.Create_New_Template(newtpl);
			
			Admin.ClientManage.Create_New_Client(client_info);
			
			Admin.ContactManage.Create_New_Contact(client_info.Contacts.get(0),true);
			
			//Create Transfereee associate with the created client - Client Contact
			Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
			
			//Send email to client
			data_mail.Map_Template_Info_func(newtpl, true);
			
			Admin.ClientManage.Send_Email_to_client(data_mail);
			
			//Verify
			client_user.Verify_User_Receives_Email_Gmail(data_mail, "","Equals");
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }

}
