package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import businessObjects.Note_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Notes;
import testcases.Catagory_SmokeTest;


@Category(Catagory_Notes.class)
public class Client_Note_Manage_042_043_044_045 extends TestBase{

	public Client_Note_Manage_042_043_044_045(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Client_Info client_info;
	
	public Note_Info note_Info;


	@Before
	public void setup()
	{
		super.setup();
		
		client_info = new Client_Info();
		
		note_Info = new Note_Info("Client",client_info.Contacts.get(0));
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.ClientManage.Create_New_Client(client_info);
		
		
	}


		
	@Category(Catagory_SmokeTest.class)
		@Test
		@Title("TC: 042_Verify Client Note Can Be Created From Top Note-Shortcut")
		public void TC_042_Verify_Client_Note_Can_Be_Created_From_Top_Shortcut() {
			
			Contact_Info ctact_info = client_info.Contacts.get(0);
			
			Admin.ContactManage.Create_New_Contact(ctact_info,true);
	                   
	        Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
	      
			Admin.NoteManage.Create_Note_via_Top_Note_Shortcut(note_Info);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
				
			Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }
		
		@Test
		@Title("TC: 043_Verify Client Note Can Be Created From Note Tab")
		public void TC_044_Verify_Client_Note_Can_Be_Created_From_Note_Tab() {
			
			Contact_Info ctact_info = client_info.Contacts.get(0);
			
			Admin.ContactManage.Create_New_Contact(ctact_info,true);
			
			Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
	      
			Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
				
			Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info); 
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }
		
		@Test
		@Title("TC: 045_Verify Client Note Can Be Edited")
		public void TC_045_Verify_Client_Note_Can_Be_Edited() {
			
			Contact_Info ctact_info = client_info.Contacts.get(0);
			
			Note_Info note_info_2 = new Note_Info("Client", ctact_info);
			
			Admin.ContactManage.Create_New_Contact(ctact_info,true);
			
			Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
	      
			Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
			
			note_info_2.ReGenerateNoteInfo();
			
			Admin.NoteManage.Update_Exiting_Note(note_Info.Title, note_info_2);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_info_2.Title);
			
			Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_info_2); 
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }
		
		@Test
		@Title("TC_034_Verify Vendor Note Can Be Removed")
		public void TC_034_Verify_Vendor_Note_Can_Be_Removed() {
			
			Contact_Info ctact_info = client_info.Contacts.get(0);
			
			Admin.ContactManage.Create_New_Contact(ctact_info,true);
			
			Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
			
			Admin.NoteManage.Create_Note_via_Top_Note_Shortcut(note_Info);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
			
			Admin.NoteManage.Delete_Note_func(note_Info);
			
			Admin.NoteManage.Verify_Data_NOT_Shown_On_Table(note_Info);
						
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
			
			
		}

	
}