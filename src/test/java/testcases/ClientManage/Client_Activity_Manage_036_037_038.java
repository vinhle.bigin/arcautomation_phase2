package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Client_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;


@Category({Catagory_Activity.class})
public class Client_Activity_Manage_036_037_038 extends TestBase{


public Client_Activity_Manage_036_037_038(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

public Client_Info client_info;




@Before
public void setup()
{
	super.setup();
	
	client_info = new Client_Info();
	
	Admin.Login_to_ARC_Portal_as_admin();
	
	Admin.ClientManage.Create_New_Client(client_info);
	
	
}


	@Test
	@Title("TC:036A_New Follow Activity can be added for client successfully")
	@Issues({"#AA-20"})
	public void TC_036A_New_Follow_Activity_can_be_added_for_client() {
      
		Activity_Info activity = new Activity_Info();
		
		activity.Type = "Follow Up";
		
		activity.AssigneeType = "Client";
		
		activity.Assignee = client_info.CompanyName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Issues({"#AA-20"})
	@Title("TC:036B_New Task Activity can be added for client successfully")
	public void TC_036B_New_Task_Activity_can_be_added_for_client() {
      
		Activity_Info activity = new Activity_Info();
		
		activity.AssigneeType = "Client";
		
		activity.Assignee = client_info.CompanyName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Issues({"#AA-20"})
	@Title("TC_037_Activity can be updated for client successfully")
	public void TC_037_Activity_can_be_updated_for_client() {
      
		Activity_Info activity = new Activity_Info();
		
		activity.AssigneeType = "Client";
		
		activity.Assignee = client_info.CompanyName;
		
		Activity_Info activity_2 = new Activity_Info();
		
		activity_2.Type = "Follow Up";
		
		activity_2.AssigneeType = "Client";
		
		activity_2.Assignee = client_info.CompanyName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity_2);
				
		Admin.ActivityManage.Update_Activity(activity.Title, activity_2);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity_2);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity_2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Issues({"#AA-20"})
	@Title("TC_038_The closed Activity of client cannot be editted")
	public void TC_038_The_closed_Activity_Of_client_Cannot_Be_Editted()
	{
		Activity_Info activity = new Activity_Info();
		
		activity.AssigneeType = "Client";
		
		activity.Assignee = client_info.CompanyName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		//Close the Activity
		activity.Closed = true;
		
		Admin.ActivityManage.Update_Activity(activity.Title, activity);
		
		//Verify Step
		
		Admin.ActivityManage.Verify_Activity_Details_Can_NOT_Be_Editable(activity,"- On Closed Activiy");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	

}
