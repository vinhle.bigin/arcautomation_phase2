package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_ClientManage;



@Category(Category_ClientManage.class)
public class Client_Company_Edit_Information_009_010_011_012  extends TestBase{
	
	
	public Client_Company_Edit_Information_009_010_011_012(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Client_Info client_info;
	
	

	
	
	@Before
	public void setup()
	{
		super.setup();
		client_info = new Client_Info();
		
	}
	

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_001: Verify New Client can be editted information")
	 
	public void TC_001_Verify_New_Client_can_be_editted_information() {
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
        
        client_info.ReGenerateInfo();
        
        Admin.ClientManage.Update_Client_Company_Info(client_info);
        
        Admin.ClientManage.Update_Client_Company_Status(client_info);
        
        Admin.ClientManage.Verify_Client_General_Info_Is_Correct_On_ViewMode(client_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
}
