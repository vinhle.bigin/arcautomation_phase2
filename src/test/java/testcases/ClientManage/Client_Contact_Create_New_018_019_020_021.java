package testcases.ClientManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_ClientManage;
import testcases.Category_E2E;



@Category(Category_ClientManage.class)
public class Client_Contact_Create_New_018_019_020_021  extends TestBase{
	
	
	public Client_Contact_Create_New_018_019_020_021(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Client_Info client_info;
	
	
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		client_info = new Client_Info(new Contact_Info());
		
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.ClientManage.Create_New_Client(client_info);
		
	}
	


	@Category({Catagory_SmokeTest.class,Category_E2E.class})
	@Test
	@Title("TC:018_Verify New Client Contact can be created")
	 
	public void TC_018_Verify_New_Contact_Can_Be_Created() {
      
		Contact_Info ctact_info = client_info.Contacts.get(0);
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
                   
        Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
        
    	Admin.ContactManage.Verify_Contact_Is_Currently_Set_As_Default(ctact_info.FullName);
        
        Admin.ContactManage.Verify_Modal_Contact_Detail_Is_Correct(ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_019_Verify Client Contact can be removed")
	 
	public void TC_019_Verify_Contact_Can_Be_Removed() {
      
		Contact_Info ctact_info = client_info.Contacts.get(0);
		
		
		Contact_Info ctact_info_2 = new Contact_Info();
		ctact_info.Init_Client_Contact_Info(client_info.CompanyName);
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.ContactManage.Create_New_Contact(ctact_info_2,true);
		
		//Remove contact
		Admin.ContactManage.Remove_Contact(ctact_info.FullName);
		
		//Verify removed contact info is not shown on table
		 Admin.ContactManage.Verify_Contact_NOT_Shown_On_Table(ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_020_Verify Contact Client can be set as default successfully")
	 
	public void TC_020_Verify_Contact_Client_Can_Set_AS_Default() {
      
		Contact_Info ctact_info = client_info.Contacts.get(0);
		
		Contact_Info ctact_info_2 = new Contact_Info();
		
		ctact_info_2.ClientName = ctact_info.ClientName;
		
		client_info.Contacts.add(ctact_info_2);
		//Steps:
		for(int i=0;i<client_info.Contacts.size();i++)
		{
			Admin.ContactManage.Create_New_Contact(client_info.Contacts.get(i),true);
		}
					
		//Verify: SetAsDefault link is shown for the Contact #2
                   
        Admin.ContactManage.Verify_Contact_Is_NOT_Set_As_Default(ctact_info_2.FullName);
        
        if(TestConfigs.glb_TCStatus==true)
        {
        	Admin.ContactManage.Set_Contact_As_Default(ctact_info_2.FullName);
        	  
        	Admin.ContactManage.Verify_Contact_Is_Currently_Set_As_Default(ctact_info_2.FullName);
        }
        
        Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	 
	@Test
	@Title("TC_021_Verify Unique Contact cannot be removed")
	 
	public void TC_021_Verify_Unique_Contact_Cannot_Be_Removed() {
      
		Contact_Info ctact_info = client_info.Contacts.get(0);
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
                   
        Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
        
    	Admin.ContactManage.Verify_Remove_Icon_NOT_shown_For_The_Unique_Contact(ctact_info.FullName);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	

}
