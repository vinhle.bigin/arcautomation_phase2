package testcases.ServiceManage_Inventorty;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;



@Category(Category_Services.class)
public class InventManage_HomeListing_Sale_008_009_010_011_012  extends TestBase{

	
	public InventManage_HomeListing_Sale_008_009_010_011_012(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Transferee_Info transf_info;

	@Before
	public void setup()
	{super.setup();
		
		transf_info = new Transferee_Info();
		
		transf_info.HS_service.Caculate_Referral_Amount();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
	}

	
	
	
	@Test
	@Title("TC_008 InventManage- Verify Home Listing Information Can Be Copied From Home Sale Infomation")
	public void TC_008_InventManage_Verify_HomeListing_Info_Can_Copied_From_Home_Sale() {
      
		//Create Transferee
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Update_transfree_HomeSale_Info(transf_info);	
				
		Admin.ServiceManage.Go_To_Service_Details(transf_info.InventManage_service.Type);
		
		Admin.ServiceManage.Copy_InventManage_List_Sale_Info_from_HomeSale_Service();
		
		Admin.ServiceManage.Verify_InventManage_HomeList_Sale_Info_Correct_ViewMode(transf_info.HS_service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_009 InventManage - Referral fee - Verify Home Listing Information Can Be Updated with Not Auto Calculate")
	public void TC_009_InventManage_ReferralFee_Verify_HomeListing_Info_Can_Be_Updated_NoTAutoCalculate() {
      
		//Create Transferee
		transf_info.InventManage_service.HS_ListSale_section.IsNotAutoCal = true;
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.InventManage_service.Type);
		
		Admin.ServiceManage.Update_InventManage_List_and_Sale_Info_Section(transf_info.InventManage_service.HS_ListSale_section);
		
		Admin.ServiceManage.Verify_InventManage_HomeList_Sale_Info_Correct_ViewMode(transf_info.InventManage_service.HS_ListSale_section);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC_010 InventManage - Referral fee - Verify Home Listing Info Can Be Updated with Auto Calculate")
	public void TC_010_InventManage_ReferralFee_Verify_HomeListing_Info_Can_Be_Updated_AutoCalculate() {
      
		
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.InventManage_service.Type);
		
		Admin.ServiceManage.Update_InventManage_List_and_Sale_Info_Section(transf_info.InventManage_service.HS_ListSale_section);
				
		Admin.ServiceManage.Verify_InventManage_HomeList_Sale_Info_Correct_ViewMode(transf_info.HS_service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	
	@Test
	@Title("TC_011 InventManage - Verify HomeSale Manage Section Is Correct")
	public void TC_011_InventManage_Verify_HomeSale_Manage_Section_Is_Correct() {
      
		//Pre-condition:
		transf_info.HBO_service.Init_TransactionInfo("BVO");
		transf_info.HBO_service.Init_OfferInfo("BVO offer");
		
		//1. Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		//2. Update HBO Offer Price and select the matched AcquistionType
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);
		
		Admin.ServiceManage.Add_New_HBO_Offer(transf_info.HBO_service);
		
		Admin.ServiceManage.Updates_HBO_Transaction_Info(transf_info.HBO_service);
		
		//3. Update Invent - HOMEListSale info
		Admin.ServiceManage.Go_To_Service_Details(transf_info.InventManage_service.Type);
		
		Admin.ServiceManage.Update_InventManage_List_and_Sale_Info_Section(transf_info.InventManage_service.HS_ListSale_section);
		
		//3. Update Invent - HS Analysis info
		Admin.ServiceManage.Update_InventManage_HomeSaleAnalysis_Info(transf_info.InventManage_service);
		
		//VErify Point
		Admin.ServiceManage.Verify_Invent_HSManage_Info_Correct_ViewMode(transf_info.InventManage_service,transf_info.HBO_service.Offer_list.get(0).Amount);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	
	@Test
	@Title("TC_012 InventManage - Verify HBOOfferPrice is Empty if HBO_AcquistionType NOT Matched HBO_OfferType")
	public void TC_012_Verify_HBOOfferPrice_Empty_if_HBOAcquistionType_NOT_Matched_HBOOfferType() {
      
		//Pre-condition:
		transf_info.HBO_service.Init_TransactionInfo("BVO");
		transf_info.HBO_service.Init_OfferInfo("Amended offer");
		
		//1. Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		//2. Update HBO Offer Price and select the matched AcquistionType
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);
		
		Admin.ServiceManage.Add_New_HBO_Offer(transf_info.HBO_service);
		
		Admin.ServiceManage.Updates_HBO_Transaction_Info(transf_info.HBO_service);
		
		//3. Update Invent - HOMEListSale info
		Admin.ServiceManage.Go_To_Service_Details(transf_info.InventManage_service.Type);
		
		Admin.ServiceManage.Update_InventManage_List_and_Sale_Info_Section(transf_info.InventManage_service.HS_ListSale_section);
		
		//3. Update Invent - HS Analysis info
		Admin.ServiceManage.Update_InventManage_HomeSaleAnalysis_Info(transf_info.InventManage_service);
		
		//VErify Point
		Admin.ServiceManage.Verify_Invent_HSManage_Info_Correct_ViewMode(transf_info.InventManage_service,0);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}


}
