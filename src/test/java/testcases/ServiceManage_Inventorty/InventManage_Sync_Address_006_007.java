package testcases.ServiceManage_Inventorty;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;



@Category(Category_Services.class)
public class InventManage_Sync_Address_006_007  extends TestBase{

	
	public InventManage_Sync_Address_006_007(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
	}

	
	@Test
	@Title("TC_006 InventManage- Property Address information is loaded from the Origin Residence address")
	public void TC_006_InventManage_Address_information_loaded_from_OriRes_Addres() {
      
		//Create Transferee
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.InventManage_service.Type);
		
		Admin.ServiceManage.Verify_ServiceDetails_Property_Address_Is_Correct_ViewMode(transf_info.InventManage_service.Type,transf_info.DesResAddr_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC_007 InventManage - Transferee Origin Residence Address is refreshed based on updated InventManage Property")
	public void TC_007_Transferee_OriRes_Address_refreshed_based_updated_InventManage_Property() {
      
		//Create Transferee
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.InventManage_service.Type);
		
		Admin.ServiceManage.Update_ServiceDetails_Property_Section(transf_info.InventManage_service.Type,transf_info.OriResAddr_info);
		
		Admin.TransfManage.Verify_transf_Addr_OrgRes_is_correct(transf_info.OriResAddr_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}


}
