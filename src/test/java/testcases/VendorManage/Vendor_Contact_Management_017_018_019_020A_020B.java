package testcases.VendorManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_VendorManage;



@Category({Category_VendorManage.class})
public class Vendor_Contact_Management_017_018_019_020A_020B  extends TestBase{

	

	public Vendor_Contact_Management_017_018_019_020A_020B(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Vendor_Info Vendor_info;
	
	
	@Before
	public void setup()
	{
	
		super.setup();
		
		Vendor_info = new Vendor_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		
	}
	

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_017_Verify New Vendor Contact can be created")
	 
	public void TC_017_Verify_New_Contact_Can_Be_Created() {
      
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
                   
        Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
        
    	Admin.ContactManage.Verify_Contact_Is_Currently_Set_As_Default(ctact_info.FullName);
        
        Admin.ContactManage.Verify_Modal_Contact_Detail_Is_Correct(ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_018_Verify Existing Vendor Contact can be updated")
	 
	public void TC_018_Verify_Existing_Contact_Can_Be_Update() {
      
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		Contact_Info ctact_info_2 = new Contact_Info();
		
		ctact_info_2.VendorName = ctact_info.VendorName;
	
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		//Update Contact
		ctact_info_2.Init_Vendor_Contact_Info(ctact_info.VendorName);
		
		Admin.ContactManage.Update_Contact(ctact_info.FullName,ctact_info_2);
		
		//Verify old contact info is not shown on table
		 Admin.ContactManage.Verify_Contact_NOT_Shown_On_Table(ctact_info);
                   
		///Verify NEW contact info is shown on table 
        Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info_2);
        
        Admin.ContactManage.Verify_Modal_Contact_Detail_Is_Correct(ctact_info_2);
        
        Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	

	@Test
	@Title("TC_020B_Verify Vendor Contact can be removed")
	 
	public void TC_020B_Verify_Contact_Can_Be_Removed() {
      
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		Contact_Info ctact_info_2 = new Contact_Info();
		
		ctact_info_2.Init_Vendor_Contact_Info(ctact_info.VendorName);
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.ContactManage.Create_New_Contact(ctact_info_2,true);
		
		//Remove contact
		Admin.ContactManage.Remove_Contact(ctact_info.FullName);
		
		//Verify removed contact info is not shown on table
		 Admin.ContactManage.Verify_Contact_NOT_Shown_On_Table(ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_019_Verify Contact Vendor can be set as default successfully")
	 
	public void TC_019_Verify_Contact_Vendor_Can_Set_AS_Default() {
      
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		Contact_Info ctact_info_2 = new Contact_Info();
		
		ctact_info_2.Init_Vendor_Contact_Info(ctact_info.VendorName);
		
		Vendor_info.Contacts.add(ctact_info_2);
		//Steps:
		for(int i=0;i<Vendor_info.Contacts.size();i++)
		{
			Admin.ContactManage.Create_New_Contact(Vendor_info.Contacts.get(i),true);
		}
					
		//Verify: SetAsDefault link is shown for the Contact #2
                   
        Admin.ContactManage.Verify_Contact_Is_NOT_Set_As_Default(ctact_info_2.FullName);
        
        if(TestConfigs.glb_TCStatus==true)
        {
        	Admin.ContactManage.Set_Contact_As_Default(ctact_info_2.FullName);
        	  
        	Admin.ContactManage.Verify_Contact_Is_Currently_Set_As_Default(ctact_info_2.FullName);
        }
        
        Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_020A_Verify Unique Contact cannot be removed")
	 
	public void TC_020AVerify_Unique_Contact_Cannot_Be_Removed() {
      
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
                   
        Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
        
    	Admin.ContactManage.Verify_Remove_Icon_NOT_shown_For_The_Unique_Contact(ctact_info.FullName);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	

}
