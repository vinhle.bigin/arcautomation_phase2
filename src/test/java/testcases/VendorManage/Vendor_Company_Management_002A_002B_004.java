package testcases.VendorManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_VendorManage;



@Category({Category_VendorManage.class})
public class Vendor_Company_Management_002A_002B_004  extends TestBase {
	
	
	public Vendor_Company_Management_002A_002B_004(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Vendor_Info Vendor_info;
	
	
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		Vendor_info = new Vendor_Info();
		
	}
	

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_002A: Verify New Vendor can be created from New Vendor button")
	 
	public void TC_002A_Verify_New_Vendor_can_be_Created_From_New_Vendor_button() {
      
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.VendorManage.Create_New_Vendor_Via_New_Vendor_Button(Vendor_info);
        
        Admin.Go_to_Vendor_Page();
        
        Admin.VendorManage.Search_For_Existing_VendorName(Vendor_info);
                   
        Admin.VendorManage.Verify_Vendor_Is_Displayed_On_Vendors_List(Vendor_info);
        
        Admin.VendorManage.Verify_Vendor_General_Info_Is_Correct_On_ViewMode(Vendor_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_002B: Verify New Vendor can be created from New shortcut icon")
	 
	public void TC_002B_Verify_New_Vendor_can_be_Created_From_New_shortcut_icon() {
      
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
        
        Admin.Go_to_Vendor_Page();
        
        Admin.VendorManage.Search_For_Existing_VendorName(Vendor_info);
                   
        Admin.VendorManage.Verify_Vendor_Is_Displayed_On_Vendors_List(Vendor_info);
        
        Admin.VendorManage.Verify_Vendor_General_Info_Is_Correct_On_ViewMode(Vendor_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_004: Verify Vendor Company can be editted information")
	 
	public void TC_004_Verify_Vendor_Comany_can_be_editted_information() { 
		
		Admin.Login_to_ARC_Portal_as_admin();
	       
        Admin.VendorManage.Create_New_Vendor_Via_New_Vendor_Button(Vendor_info);
        
        Vendor_info.ReGenerateInfo();
        
        Admin.VendorManage.Update_Info_For_Existing_Vendor(Vendor_info.VendorName, Vendor_info);
        
        Admin.VendorManage.Update_Status_For_Existing_Vendor(Vendor_info);
        
        Admin.VendorManage.Search_For_Existing_VendorName(Vendor_info);
        
        Admin.VendorManage.Verify_Vendor_Is_Displayed_On_Vendors_List(Vendor_info);
        
        Admin.VendorManage.Verify_Vendor_General_Info_Is_Correct_On_ViewMode(Vendor_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	

}
