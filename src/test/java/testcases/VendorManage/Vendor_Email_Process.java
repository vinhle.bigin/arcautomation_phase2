package testcases.VendorManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Mail_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.VendorUser_Steps;
import testcases.Catagory_SmokeTest;
import testcases.Category_VendorManage;


@Category({Category_VendorManage.class})
public class Vendor_Email_Process  extends TestBase{
	
	
	public Vendor_Email_Process(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	public Vendor_Info Vendor_info;
	public Contact_Info Vendor_contact;
	Transferee_Info transf_info;
	Template_Info newtpl;
	
	
	@Steps
	public VendorUser_Steps Vendor_user;



	@Before
	public void setup()
	{	super.setup();
	
		
		newtpl = new Template_Info();
		newtpl.Target = "Vendor";
		
		Vendor_info = new Vendor_Info();
		
		Vendor_contact = new Contact_Info();
		
		Vendor_contact = Vendor_info.Contacts.get(0);
		
		Vendor_contact.email_info.EmailAddr = glb_RefData.glb_TestMailAccount;
		
		transf_info = new Transferee_Info();
	}


		
	@Category(Catagory_SmokeTest.class)
		@Test
		@Title("TC_038_Verify Template Email without document can be sent to Vendor successfully")
		public void TC_038_Verify_Template_Email_without_document_can_be_sent_to_Vendor() {
	      
			
			Mail_Info data_mail = Vendor_contact.email_info;
			
			Admin.Login_to_ARC_Portal_as_admin();
			
			//Create new template
			Admin.TemplateManage.Create_New_Template(newtpl);
			
			Admin.VendorManage.Create_New_Vendor_Via_New_Vendor_Button(Vendor_info);
			
			Admin.ContactManage.Create_New_Contact(Vendor_contact,true);
			
			//Create Transfereee associate with the created Vendor - Vendor Contact
			Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
			
			//Send email to Vendor
			data_mail.Map_Template_Info_func(newtpl, false);
			
			Admin.VendorManage.Send_Email_to_Vendor(data_mail);
			
			//Verify
			Vendor_user.Verify_User_Receives_Email_Gmail(data_mail, "","Equals");
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }

}
