package testcases.VendorManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;
import testcases.Catagory_SmokeTest;
import testcases.Category_VendorManage;


@Category({Category_VendorManage.class,Catagory_Activity.class})
public class Vendor_Activity_Manage_013_014_015_016  extends TestBase{


public Vendor_Activity_Manage_013_014_015_016(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

public Vendor_Info Vendor_info;




@Before
public void setup()
{
	
	super.setup();
	
	Vendor_info = new Vendor_Info();
	

	
	Admin.Login_to_ARC_Portal_as_admin();
	
	Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
	
	
}


	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_013_New Follow Activity can be added for Vendor successfully")
	@Issues({"#AA-20"})
	public void TC_013_New_Follow_Activity_can_be_added_for_Vendor() {
      
		Activity_Info activity = new Activity_Info();
		
		activity.Type = "Follow Up";
		
		activity.AssigneeType = "Vendor";
		
		activity.Assignee = Vendor_info.VendorName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Issues({"#AA-20"})
	@Title("TC_014_New Task Activity can be added for Vendor successfully")
		public void TC_014_New_Task_Activity_can_be_added_for_Vendor() {
      
		Activity_Info activity = new Activity_Info();
		
		activity.AssigneeType = "Vendor";
		
		activity.Assignee = Vendor_info.VendorName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Issues({"#AA-20"})
	@Title("TC_015_Activity can be updated for Vendor successfully")
	public void TC_015_Activity_can_be_updated_for_Vendor() {
      
		Activity_Info activity = new Activity_Info();
		
		activity.AssigneeType = "Vendor";
		
		activity.Assignee = Vendor_info.VendorName;
		
		Activity_Info activity_2 = new Activity_Info();
		
		activity_2.Type = "Follow Up";
		
		activity_2.AssigneeType = "Vendor";
		
		activity_2.Assignee = Vendor_info.VendorName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity_2);
				
		Admin.ActivityManage.Update_Activity(activity.Title, activity_2);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity_2);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity_2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Issues({"#AA-20"})
	@Title("TC_016_The closed Activity of Vendor cannot be editted")
	public void TC_016_The_closed_Activity_Of_Vendor_Cannot_Be_Editted()
	{
		Activity_Info activity = new Activity_Info();
		
		activity.AssigneeType = "Vendor";
		
		activity.Assignee = Vendor_info.VendorName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		//Close the Activity
		activity.Closed = true;
		
		Admin.ActivityManage.Update_Activity(activity.Title, activity);
		
		//Verify Step
		
		Admin.ActivityManage.Verify_Activity_Details_Can_NOT_Be_Editable(activity,"- On Closed Activiy");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	
	
	
	


}
