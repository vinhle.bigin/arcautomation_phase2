package testcases.VendorManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;
import testcases.Category_VendorManage;


@Category({Category_VendorManage.class,Catagory_Activity.class})
public class Vendor_Activity_Manage_TC_017_018  extends TestBase{


public Vendor_Activity_Manage_TC_017_018(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


public Vendor_Info Vendor_info;



@Before
public void setup()
{
	super.setup();
	
	Vendor_info = new Vendor_Info();
	

	
	Admin.Login_to_ARC_Portal_as_admin();
	
	Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
	
	
}


	
	
	
	@Test
	@Title("TC_017_New_Activity can be added for transferee Via Heading Shortcut")
	public void TC_017_New_Vendor_Activity_can_be_added_Via_Heading_Shortcut() {
      
		Activity_Info activity = new Activity_Info();
		activity.Type = "Follow Up";
		activity.AssigneeType = "Vendor";
		activity.Assignee = Vendor_info.VendorName;
		
		Admin.ActivityManage.Create_Activity_Via_Heading_Shortcut(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_018_Verify Vendor Activity can be deleted")
	public void TC_018_Verify_Vendor_Activity_Can_Be_Deleted()
	{
		Activity_Info activity = new Activity_Info();
		activity.AssigneeType = "Vendor";
		activity.Assignee = Vendor_info.VendorName;
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Delete_Activity(activity);
		
		Admin.ActivityManage.Verify_Activity_Is_NOT_Shown_On_Table(activity,"After being deleted");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	
	
	


}
