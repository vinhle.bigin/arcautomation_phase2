package testcases.VendorManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Document_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_VendorManage;


@Category({Category_VendorManage.class})
public class Vendor_Document_Manage_037_038_041_042_043  extends TestBase{

	
	public Vendor_Document_Manage_037_038_041_042_043(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Vendor_Info Vendor_info;
	
	

	@Before
	public void setup()
	{
		super.setup();
		
		Vendor_info = new Vendor_Info();
		
	
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_New_Vendor_Button(Vendor_info);
		
	
		
	}

	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_037_Verify New Document Can Be Created for Vendor")
	public void TC_037_Verify_New_Document_Can_Be_Created_For_Vendor() 
	{
		Document_Info document = new Document_Info();
		
		document.Init_Vendor_Doc_Info();
				
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Admin.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	
	}
	
	@Test
	@Title("TC_041_Verify Vendor Document Can Be Updated")
	public void TC_041_Verify_Vendor_Document_Can_Be_Updated() 
	{	
		Document_Info document = new Document_Info();
		
		Document_Info document2 = new Document_Info();
		
		document.Init_Vendor_Doc_Info();
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		//Update document
		document2.ReGenerate_Info();
		
		Admin.DocumentManage.Update_Existing_Document(document2);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document2.file_name);
		
		Admin.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC_042_Verify Vendor Document Can Be Removed")
	public void TC_042_Verify_Vendor_Document_Can_Be_Removed() 
	{
		Document_Info document = new Document_Info();
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		//Remove document
	
		Admin.DocumentManage.Remove_Document(document.file_name);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_NOT_Shown_On_List(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC_043_Verify Document file Can Be Downloaded")
	public void TC_043_Verify_Document_Can_Be_Downloaded() 
	{
		//==TEST DATA
		//String destination_file_name = "DownloadFile_doc.docx";
		
		String Expect_content = "Document's Testing Content";
		
		Document_Info document = new Document_Info();
		
		new FileManage().CreateWordFile_func(document.file_name, Expect_content);
		
		//=======Steps:
		
	
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		//VErify
		if(TestConfigs.glb_TCStatus!=false)
		{
			Admin.DocumentManage.Download_Document(document.file_name, "");
			
			Admin.DocumentManage.Verify_Downloaded_File_Content_Is_Correct(document.file_name,Expect_content);
			
			//Post-condition-Delete the downloaded file:
			new FileManage().Delete_Downloaded_File_func(document.file_name);
		
		}
		
		 
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC_038_Verify Document Can Be Search For With Title")
	public void TC_038_Verify_Document_Can_Be_Search_For_With_Title() 
	{
		String Expect_content = "Document's Testing Content";
		
		Document_Info document = new Document_Info();
		
		Document_Info document_2 = new Document_Info();
		document_2.file_name = "Testing2doc.docx";
		
	
		
		for(int i =0;i<2;i++)
		{
			new FileManage().CreateWordFile_func(document.file_name, Expect_content);
			
			Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		}
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Admin.DocumentManage.Verify_Document_Info_NOT_Shown_On_List(document_2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	
	

	
	

}
