package testcases.VendorManage;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Note_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_VendorManage;


@Category({Category_VendorManage.class})
public class Vendor_Note_Manage_033A_033B  extends TestBase{
	
	
	public Vendor_Note_Manage_033A_033B(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Vendor_Info Vendor_info;
	
	public Note_Info note_Info;



	@Before
	public void setup()
	{
		super.setup();
		
		Vendor_info = new Vendor_Info();
		
		note_Info = new Note_Info("Vendor",Vendor_info.Contacts.get(0));
		
		
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_New_Vendor_Button(Vendor_info);
		//update code
		
	}

		
		
	@Category(Catagory_SmokeTest.class)
		@Test 
		@Title("TC_033A_Verify Vendor Note Can Be Created From Top Note-Shortcut")
		@Issues({"#AA-29"})
		public void TC_033A_Verify_Vendor_Note_Can_Be_Created_From_Top_Shortcut() {
			
			Contact_Info ctact_info = Vendor_info.Contacts.get(0);
			
			Admin.ContactManage.Create_New_Contact(ctact_info,true);
			
			Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
			
			Admin.NoteManage.Create_Note_via_Top_Note_Shortcut(note_Info);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
				
			Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }
		
		@Test
		@Title("TC_033B_Verify Vendor Note Can Be Created From Note Tab")
		@Issues({"#AA-29"})
		public void TC_033B_Verify_Vendor_Note_Can_Be_Created_From_Note_Tab() {
	      
			Contact_Info ctact_info = Vendor_info.Contacts.get(0);
			
			Admin.ContactManage.Create_New_Contact(ctact_info,true);
			
			Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
			
			Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
				
			Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info); 
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }
		
		@Test
		@Title("TC_034_Verify Vendor Note Can Be Update From Note Tab")
		public void TC_034_Verify_Vendor_Note_Can_Be_Update_From_Note_Tab() {
			
			Contact_Info ctact_info = Vendor_info.Contacts.get(0);
			
			Note_Info note_info_2 = new Note_Info("Vendor", ctact_info);
			
			Admin.ContactManage.Create_New_Contact(ctact_info,true);
			
			Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
			
			Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
			
			note_info_2.ReGenerateNoteInfo();
			
			Admin.NoteManage.Update_Exiting_Note(note_Info.Title, note_info_2);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_info_2.Title);
			
			Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_info_2); 
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		}
		
		@Test
		@Title("TC_034_Verify Vendor Note Can Be Removed")
		public void TC_034_Verify_Vendor_Note_Can_Be_Removed() {
			
			Contact_Info ctact_info = Vendor_info.Contacts.get(0); 
			
			Admin.ContactManage.Create_New_Contact(ctact_info,true);
			
			Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
			
			Admin.NoteManage.Create_Note_via_Top_Note_Shortcut(note_Info);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
			
			Admin.NoteManage.Delete_Note_func(note_Info);
			
			Admin.NoteManage.Verify_Data_NOT_Shown_On_Table(note_Info);
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
			
			
		}

}