package testcases.ServiceManage_HBO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.HBOService_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HBO_Required_Repair_Section_020_021_022  extends TestBase{

	
	public HBO_Required_Repair_Section_020_021_022(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	HBOService_Info service;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		transf_info.HBO_service.Type = "Home buyout";

		service = new HBOService_Info();
	
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		service.Init_RequiredRepair();
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);

	}


	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_020 HBO - Verify Repair is created successfully")
	public void TC_020_HBO_Verify_Repair_is_created_successfully() 
	{
		Admin.ServiceManage.HBO_Create_New_Repair(service);
		
		Admin.ServiceManage.HBO_Verify_NewRepair_is_correct(service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_021 HBO - Verify Repair is editted successfully")
	public void TC_021_HBO_Verify_Repair_is_editted_successfully() 
	{
		Admin.ServiceManage.HBO_Create_New_Repair(service);
		
		Admin.ServiceManage.HBO_Edit_Repair(service);
		
		Admin.ServiceManage.HBO_Verify_NewRepair_is_correct(service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_022 HBO - Verify Repair is removed successfully")
	public void TC_022_HBO_Verify_Repair_is_removed_successfully() 
	{	
		Admin.ServiceManage.HBO_Create_New_Repair(service);
		
		Admin.ServiceManage.HBO_Remove_Repair_IntheTable(service);
		
		Admin.ServiceManage.HBO_Verify_Repair_Not_Show_IntheTable(service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
}
