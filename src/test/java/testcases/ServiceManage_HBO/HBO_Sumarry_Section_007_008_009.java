package testcases.ServiceManage_HBO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.InventManageService_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HBO_Sumarry_Section_007_008_009  extends TestBase{
	

	public HBO_Sumarry_Section_007_008_009(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
	}


	
	@Test
	@Title("TC_007 HBO - Verify Current Listing Price is updated successfully")
	public void TC_007_HBO_Verify_Current_Listing_Price_is_updated_successfully() 
	{
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HS_service.Type);
		
		Admin.ServiceManage.Update_transfree_HomeSale_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);
		
		Admin.ServiceManage.Verify_HBO_Summary_Section_Is_Correct_ViewMode(0, transf_info.HS_service.ListPrice, transf_info.HS_service.ListStartDate,"","");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC_008 HBO- Verify Buyout Offer Price is updated successfully")
	public void TC_008_HBO_Verify_BuyoutOfferPrice_is_updated_successfully() 
	{
		transf_info.HBO_service.Init_OfferInfo("Amended offer");
		
		transf_info.HBO_service.Init_TransactionInfo("Amended");
		
		//Steps:
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);
		
		Admin.ServiceManage.Add_New_HBO_Offer(transf_info.HBO_service);
		
		Admin.ServiceManage.Updates_HBO_Transaction_Info(transf_info.HBO_service);
		
		Admin.ServiceManage.Verify_HBO_Summary_Section_Is_Correct_ViewMode(transf_info.HBO_service.Offer_list.get(1).Amount, "","","","");

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_009 HBO- Verify Estimated Selling Price Price is updated successfully")
	public void TC_009_HBO_Verify_EstSellPrice_is_updated_successfully() 
	{
		Admin.ServiceManage.Go_To_Service_Details(transf_info.InventManage_service.Type);
		
		InventManageService_Info service = new InventManageService_Info();
		
		Admin.ServiceManage.Update_InventManage_HomeSaleAnalysis_Info(service);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);
		
		Admin.ServiceManage.Verify_HBO_Summary_Section_Is_Correct_ViewMode(0, "", "", transf_info.InventManage_service.HSAnal_AverageDays, transf_info.InventManage_service.HSAnal_SugesstSellPrice);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	
}
      