package testcases.ServiceManage_HBO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.HBOService_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HBO_Inspections_Need_Section_016_017_018  extends TestBase{


	public HBO_Inspections_Need_Section_016_017_018(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	HBOService_Info service;
	

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		transf_info.HBO_service.Type = "Home buyout";
		
		service = new HBOService_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		service.Init_InspectionsNeed();
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);

	}


	
	@Test
	@Title("TC_016 HBO - Verify Inspection is created successfully")
	public void TC_016_HBO_Verify_Inspection_is_created_successfully() 
	{
		Admin.ServiceManage.HBO_Create_New_Inspection(service);
		
		Admin.ServiceManage.HBO_Verify_NewInspection_is_correct(service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_017 HBO - Verify Inspection is editted successfully")
	public void TC_017_HBO_Verify_Inspection_is_editted_successfully() 
	{
		Admin.ServiceManage.HBO_Create_New_Inspection(service);
		
		Admin.ServiceManage.HBO_Edit_Inspection(service);
		
		Admin.ServiceManage.HBO_Verify_NewInspection_is_correct(service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_018 HBO - Verify Inspection is removed successfully")
	public void TC_018_HBO_Verify_Inspection_is_removed_successfully() 
	{
		Admin.ServiceManage.HBO_Create_New_Inspection(service);
		
		Admin.ServiceManage.HBO_Remove_Inspection_IntheTable(service);
		
		Admin.ServiceManage.HBO_Verify_Inspection_Not_Show_IntheTable(service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
}
