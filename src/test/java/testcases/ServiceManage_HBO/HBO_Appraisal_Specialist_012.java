package testcases.ServiceManage_HBO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HBO_Appraisal_Specialist_012  extends TestBase{

	
	
	public HBO_Appraisal_Specialist_012(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
	}


	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_012 HBO - Verify The User List is updated successfully")
	public void TC_012_HBO_Verify_The_User_List_is_updated_successfully() 
	{		
		transf_info.HBO_service.Init_TransactionInfo("Amended");
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);

		Admin.ServiceManage.Updates_HBO_Transaction_Info(transf_info.HBO_service);
		
		Admin.ServiceManage.HBO_Verify_Transaction_Details_Section_is_correct(transf_info.HBO_service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
}
