package testcases.ServiceManage_HBO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HBO_Update_OriginalAddress_001_002  extends TestBase{
	
	
	public HBO_Update_OriginalAddress_001_002(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
	}

	
	
	@Test
	@Title("TC_001 HBO- Property Info - Original Address information is loaded from the Origin - Residence address")
	public void TC_001_HBO_PropertyInfo_is_loaded_from_the_OriRes_address() {
      
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);
		
		Admin.ServiceManage.Verify_ServiceDetails_Property_Address_Is_Correct_ViewMode(transf_info.HBO_service.Type,transf_info.OriResAddr_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC_002 HBO - OriRes Address is refreshed based on updated HBO Property")
	public void TC_002_HBO_OriRes_Address_refreshed_based_updated_HBO_Property() {
     
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);
		
		Admin.ServiceManage.Update_ServiceDetails_Property_Section(transf_info.HBO_service.Type,transf_info.OriResAddr_info);
		
		Admin.ServiceManage.Verify_ServiceDetails_Property_Address_Is_Correct_ViewMode(transf_info.HBO_service.Type,transf_info.OriResAddr_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
}
