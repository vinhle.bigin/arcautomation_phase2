package testcases.ServiceManage_HBO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.HBOService_Info;
import businessObjects.InventManageService_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HBO_View_Info_In_Transferee_Portal_028  extends TestBase{
	

	
	public HBO_View_Info_In_Transferee_Portal_028(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	
	HBOService_Info service;
	
	@Steps
	TransfUser_Steps Transferee;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		service = new HBOService_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		//Create Transferee
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
	}


	
	@Test
	@Title("TC_028 HBO - Verify Info is viewed in Transferee Portal")
	public void TC_028_HBO_Verify_Info_Is_Viewed_In_Transferee_Portal() 
	{
		InventManageService_Info service_Invent = new InventManageService_Info();
		
		transf_info.HBO_service.Init_OfferInfo("Amended offer");
		
		transf_info.HBO_service.Init_TransactionInfo("Amended");
		
		service.Init_InspectionsNeed();
		
		service.Init_RequiredRepair();
		
		service.Init_SellerConcession();
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HS_service.Type);
		
		Admin.ServiceManage.Update_transfree_HomeSale_Info(transf_info);
	
		Admin.ServiceManage.Go_To_Service_Details(transf_info.InventManage_service.Type);
	
		Admin.ServiceManage.Update_InventManage_HomeSaleAnalysis_Info(service_Invent);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);
		
		Admin.ServiceManage.Add_New_HBO_Offer(transf_info.HBO_service);

		Admin.ServiceManage.Updates_HBO_Transaction_Info(transf_info.HBO_service);
		
		Admin.ServiceManage.HBO_Create_New_Inspection(service);
		
		Admin.ServiceManage.HBO_Create_New_Repair(service);
		
		Admin.ServiceManage.HBO_Create_New_Concession(service);
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HBO_service.Type);
		
		//Verify 
		Admin.ServiceManage.Verify_ServiceDetails_Property_Address_Is_Correct_ViewMode(transf_info.HBO_service.Type,transf_info.OriResAddr_info);
		
		Admin.ServiceManage.Verify_HBO_Summary_Section_Is_Correct_ViewMode(transf_info.HBO_service.Offer_list.get(0).Amount, transf_info.HS_service.ListPrice, transf_info.HS_service.ListStartDate, transf_info.InventManage_service.HSAnal_AverageDays, transf_info.InventManage_service.HSAnal_SugesstSellPrice);	
		
		Admin.ServiceManage.HBO_Verify_Transaction_Details_Section_is_correct(transf_info.HBO_service);
		
		Admin.ServiceManage.HBO_Verify_Offer_Detail_Section_is_correct(transf_info.HBO_service);
		
		Admin.ServiceManage.HBO_Verify_NewInspection_is_correct(service);
		
		Admin.ServiceManage.HBO_Verify_NewRepair_is_correct(service);
		
		Admin.ServiceManage.HBO_Verify_NewConcession_is_correct(service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
}
