package testcases.TemplateManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.LetterTemplate_info;
import businessObjects.Template_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class Create_LetterTemplate_010_011_012  extends TestBase{
	
	
	public Create_LetterTemplate_010_011_012(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;

	Template_Info newtpl2;

	Template_Info newtpl3;
	
	@Before
	public void setup()
	{
		super.setup();
		newtpl1 = new LetterTemplate_info();

	}
	
	
	@Test
	@Title("TC_010_Letter Client Template Can Be Created Successfully")
	 
	public void Verify_Letter_Client_Template_Can_Be_Created() {
		
		newtpl1.Target = "Client";
     
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "After Creating the template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_011_Letter Vendor Template Can Be Created Successfully")
	 
	public void Verify_Letter_Vendor_Template_Can_Be_Created() {
		
		newtpl1.Target = "Vendor";
     
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "After Creating the template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_012_Letter Transferee Template Can Be Created Successfully")
	 
	public void Verify_Letter_Transferee_Template_Can_Be_Created() {
		
		//newtpl1.Target = "Transferee";
     
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "After Creating the template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	

}
