package testcases.TemplateManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Template_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class Create_Appraisal_EmailTemplate_042_043_044  extends TestBase{
	
	
	public Create_Appraisal_EmailTemplate_042_043_044(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Template_Info newtpl1;


	
	@Before
	public void setup()
	{
		super.setup();
		
		newtpl1 = new Template_Info();
		
	
		
		
	}
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_042_ Client Appraisal Template Can Be Created Successfully")
	 
	public void TC_042_Verify_Appraisal_Client_Template_Can_Be_Created() {
		
		newtpl1.Generate_Client_Template_func();
		
		newtpl1.ServiceType = "Appraisal";
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "After Creating the Appraisal template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_043_Vendor Appraisal Template Can Be Created Successfully")
	 
	public void TC_043_Verify_Appraisal_Vendor_Template_Can_Be_Created() {
		
		newtpl1.Generate_Vendor_Template_func();
		
		newtpl1.ServiceType = "Appraisal";
		
		newtpl1.Target = "Vendor";
     
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "After Creating the Appraisal template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_044_Transferee Appraisal Template Can Be Created Successfully")
	 
	public void TC_044_Verify_Appraisal_Transferee_Template_Can_Be_Created() {
		
		//newtpl1.Target = "Transferee";
		newtpl1.ServiceType = "Appraisal";
     
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "After Creating the Appraisal template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	

}
