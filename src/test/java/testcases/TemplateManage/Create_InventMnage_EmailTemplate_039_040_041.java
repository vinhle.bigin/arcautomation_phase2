package testcases.TemplateManage;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Template_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.User_AllSteps;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class Create_InventMnage_EmailTemplate_039_040_041  extends TestBase{
	
	
	public Create_InventMnage_EmailTemplate_039_040_041(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;

	@Steps
	static User_AllSteps Admin;
	
	
	@BeforeClass
	public static void beforeclass()
	{
		
	}
	
	@AfterClass
	public static void afterclass()
	{
		
	}
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		newtpl1 = new Template_Info();
	
		
	}
	
	@Test
	@Title("TC_039_InventMnage_Email Client Template Can Be Created Successfully")
	 
	public void TC_039_Verify_InventMnage_Email_Client_Template_Can_Be_Created() {
		
		newtpl1.Generate_Client_Template_func();
		
		newtpl1.ServiceType = "Inventory management";
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "After Creating the Inventory management template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_040_InventMnage_Email Vendor Template Can Be Created Successfully")
	 
	public void TC_040_Verify_InventMnage_Email_Vendor_Template_Can_Be_Created() {
		
		newtpl1.Generate_Vendor_Template_func();
		
		newtpl1.ServiceType = "Inventory management";
		
		newtpl1.Target = "Vendor";
     
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "After Creating the Inventory management template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_041_InventMnage_Email Transferee Template Can Be Created Successfully")
	 
	public void TC_041_Verify_InventMnage_Email_Transferee_Template_Can_Be_Created() {
		
		//newtpl1.Target = "Transferee";
		newtpl1.ServiceType = "Inventory management";
     
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "After Creating the Inventory management template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	

}
