package testcases.TemplateManage;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class Transf_General_FieldCode_019_020_021_022  extends TestBase{
	
	
	 public Transf_General_FieldCode_019_020_021_022(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;
	 Transferee_Info transferee;

	@Before
	public void setup()
	{
		super.setup();
		
		newtpl1 = new Template_Info();
		
		transferee = new Transferee_Info();
		
}
	
	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_019_Verify Transferee General Field Code Value On Email Modal Content")
	 
	public void TC_019_Verify_Transferee_General_field_Code_Value_Email_Modal_Content() {
		
		newtpl1.AddFieldCode_TransfGenInfo_func();
		
		transferee.MailList.get(0).RenewContentTransGenInfo_func(transferee);
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		Admin.TransfManage.Update_transfree_General_Info(transferee);
		
		Admin.TransfManage.Verify_Email_Modal_Details(transferee.MailList.get(0));
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_020_Verify Transferee General Field Code Value On Email Modal - Document")
	@Issues({"#AA-25"}) 
	public void TC_020_Verify_Transferee_General_field_Code_Value_Email_Modal_Document() {
		
		//Make Up Data:
		newtpl1.AddFieldCode_TransfGenInfo_func();
		
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).RenewContentTransGenInfo_func(transferee);
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		//Steps:
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		Admin.TransfManage.Update_transfree_General_Info(transferee);
		
		Admin.TransfManage.Open_Modal_Sending_Email_Form(transferee.MailList.get(0));
		
		Admin.TransfManage.Verify_Email_Modal_Document_Content_Is_Correct(newtpl1.DocName, newtpl1.Content);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_021_Verify Transferee General Field Code Value On Received Email and DOCX")
	 
	public void TC_021_Verify_Transf_General_field_Code_Received_email_Content_and_DOCX() {
		
		//Make Up Data:
		newtpl1.AddFieldCode_TransfGenInfo_func();
		
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).RenewContentTransGenInfo_func(transferee);
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		Admin.TransfManage.Update_transfree_General_Info(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Contains");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_022_Verify Transferee General Field Code On Received Email and PDF")
	 
	public void TC_022_Verify_Transf_General_field_Code_Received_email_Content_and_PDF() {
		
		//Make Up Data:
		newtpl1.AddFieldCode_TransfGenInfo_func();
		newtpl1.DocProcessType = "PDF";
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).RenewContentTransGenInfo_func(transferee);
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		Admin.TransfManage.Update_transfree_General_Info(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	
	

}
