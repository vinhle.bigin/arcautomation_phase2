package testcases.TemplateManage;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.HHGService_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class Transf_HHG_FieldCode_068_069_070_071  extends TestBase{
	
	
	 public Transf_HHG_FieldCode_068_069_070_071(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;
	 Transferee_Info transferee;
	 Vendor_Info vendor;
	 Service_Order_Info HHG_Order_info;

	@Before
	public void setup()
	{
		//=============Test Data
		super.setup();
		
		vendor = new Vendor_Info();
		vendor.SearchOpt_IsUSA = true;
		vendor.Contacts.get(0).ServiceType = "Van Lines";
		
		transferee = new Transferee_Info();
		transferee.HHG_service = new HHGService_Info(vendor.Contacts.get(0));
		
		HHG_Order_info = new Service_Order_Info();
		HHG_Order_info.Init_All_DomesticOrder_Info(vendor.VendorName);
		HHG_Order_info.Type = "HHG";// so that the Tariff Code Info is included in the updating method
		
		
		newtpl1 = new Template_Info();
		newtpl1.ServiceType = transferee.HHG_service.Type;
		newtpl1.AddFieldCode_HHG_Info_func(transferee.HHG_service);
		
		//Prepare Email Info for include selecting order
		transferee.MailList.get(0).RenewContent_HHG_Info_func(HHG_Order_info);
		transferee.MailList.get(0).Order_Descr = HHG_Order_info.Description;
		
		Contact_Info vendor_contact_tmp = vendor.Contacts.get(0);
		
		//==============Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(vendor);
		 
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(vendor.Contacts.get(0),true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		//Create Transferee and Update HHG SErevice Order Info
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.HHG_service.Type, vendor_contact_tmp);
		
		Admin.ServiceManage.Create_New_HHG_Service_Order(HHG_Order_info); 
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(HHG_Order_info);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(HHG_Order_info);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(HHG_Order_info);
		
		Admin.ServiceManage.Update_HHG_ShipmentInfo_Section(HHG_Order_info);
		
		Admin.ServiceManage.Update_HHG_ServiceCharge_Section(HHG_Order_info);
		
		Admin.ServiceManage.Update_HHG_BillingInvoice_Section(HHG_Order_info);
		
		
}
	

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_068_Verify Transferee HHG Field Code Value On Email Modal Content")
	 
	public void TC_068_Verify_Transferee_HHG_field_Code_Value_Email_Modal_Content() {
		
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Verify_Email_Modal_Details(transferee.MailList.get(0));
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_069_Verify Transferee HHG Field Code Value On Email Modal - Document")
	@Issues({"#AA-25"})
	public void TC_069_Verify_Transferee_HHG_field_Code_Value_Email_Modal_Document() {
		
		//Make Up Data:
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		//Steps:
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Open_Modal_Sending_Email_Form(transferee.MailList.get(0));
		
		Admin.TransfManage.Verify_Email_Modal_Document_Content_Is_Correct(newtpl1.DocName, newtpl1.Content);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_070_Verify Transferee HHG Field Code Value On Received Email and DOCX")
	 
	public void TC_070_Verify_Transf_HHG_field_Code_Received_email_Content_and_DOCX() {
		
		//Make Up Data:
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_071_Verify Transferee HHG Field Code On Received Email and PDF")
	 
	public void TC_071_Verify_Transf_HHG_field_Code_Received_email_Content_and_PDF() {
		
		//Make Up Data:
		newtpl1.DocProcessType = "PDF";
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	
	

}
