package testcases.TemplateManage;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class TemplateDocumentProcess_016_017_018  extends TestBase{
	
	
	 public TemplateDocumentProcess_016_017_018(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;
	 Transferee_Info transferee;

	
	@Before
	public void setup()
	{
		super.setup();
		
		newtpl1 = new Template_Info();
		
		newtpl1.DocName = "email_template.docx";
				
		newtpl1.AddFieldCode_TransfGenInfo_func();
		
		transferee = new Transferee_Info();
		
		transferee.MailList.get(0).RenewContentTransGenInfo_func(transferee);
		
		FileManage file_access = new FileManage();
		
		file_access.CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
		
		
		
	}
	
	
	
	

	
	@Test
	@Title("TC_016_Admin can add process document docx in template")
	 
	public void TC_016_Admin_Can_Add_Process_Docx_Document_In_Template() {
		
	    
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		if(TestConfigs.glb_TCStatus==true)
		{
			Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		}
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_017_Admin can add process document PDF in template")
	 
	public void TC_017_Admin_Can_Add_Process_PDF_Document_In_Template() {
		
		newtpl1.DocProcessType = "PDF";
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		if(TestConfigs.glb_TCStatus==true)
		{
			Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		}
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_017_Admin can add normal document in template")
	 
	public void TC_018_Admin_Can_Add_Normal_Document_In_Template() {
		
		newtpl1.DocProcessType = "None";
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		if(TestConfigs.glb_TCStatus==true)
		{
			Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl1);
		}
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	
	

}
