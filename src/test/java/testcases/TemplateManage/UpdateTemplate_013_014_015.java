package testcases.TemplateManage;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class UpdateTemplate_013_014_015  extends TestBase{
	
	
	 public UpdateTemplate_013_014_015(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;

	Transferee_Info tranf_info;
	
	@Before
	public void setup()
	{	super.setup();
		
		newtpl1 = new Template_Info();
		
		tranf_info = new Transferee_Info();
		
		
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		
		
		
		
		
		
	}
	
	
	

	
	@Test
	@Title("TC_013_Template can be updated successfully")
	 
	public void TC_013_Verify_Template_Can_Be_Updated() {
		
		 Template_Info newtpl2 = new Template_Info();
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
				
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Update_Existing_Template(newtpl1.Name, newtpl2);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl2, "After Update the template");
		
		Admin.TemplateManage.Verify_Template_Details_Is_Correct(newtpl2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	

	@Test
	@Title("TC_014_Template can be Active and In-Active successfully")
	 
	public void TC_014_Verify_Template_Can_Be_Active_and_InActive() {
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
					
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(tranf_info, true);
		
		//Verify Active Template
		Admin.TransfManage.Verify_Template_Displayed_On_Email_Modal(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		//Update Template
		newtpl1.IsActive = false;
		
		Admin.TemplateManage.Update_Existing_Template(newtpl1.Name, newtpl1);
		
		//Verify In-Active Template
		Admin.TransfManage.Go_To_Transfeee_Details(tranf_info);
		
		Admin.TransfManage.Verify_Template_NOT_Displayed_On_Email_Modal(newtpl1);
			
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

	@Test
	@Title("TC_015_Template Can Be Removed Successfully")
	public void TC_015_Template_Can_Be_Removed_Successfully()
	{
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TemplateManage.Remove_Existing_Template(newtpl1);
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Not_Displayed_On_List(newtpl1);
	}
	
	

}
