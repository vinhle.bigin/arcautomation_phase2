package testcases.TemplateManage;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class Transf_HBO_FieldCode_052_053_054_055  extends TestBase{
	

	 public Transf_HBO_FieldCode_052_053_054_055(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;
	 Transferee_Info transferee;
	 Vendor_Info vendor;

	@Before
	public void setup()
	{
		super.setup();
		
		vendor = new Vendor_Info();
		
		vendor.SearchOpt_IsUSA = true;
		
		transferee = new Transferee_Info();
		
		newtpl1 = new Template_Info();
		
		newtpl1.ServiceType = transferee.HBO_service.Type;
		
		newtpl1.AddFieldCode_HBOInfo_func(transferee.HBO_service);
		
		transferee.MailList.get(0).RenewContent_HBOInfo_func(transferee.HBO_service);
		
	
		//Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(vendor);
		 
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		Admin.ServiceManage.Go_To_Service_Details(transferee.HBO_service.Type);
		
		//Admin.ServiceManage.Update_transfree__Info(transferee);
		Admin.ServiceManage.Updates_HBO_Transaction_Info(transferee.HBO_service);
		
		Admin.ServiceManage.Add_New_HBO_Offer(transferee.HBO_service);
		
		Admin.ServiceManage.HBO_Create_New_Concession(transferee.HBO_service);
		
		Admin.ServiceManage.HBO_Create_New_Inspection(transferee.HBO_service);
		
		Admin.ServiceManage.HBO_Create_New_Repair(transferee.HBO_service);
		
	
}
	

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_052_Verify Transferee HBO Field Code Value On Email Modal Content")
	 
	public void TC_052_Verify_Transferee_HBO_field_Code_Value_Email_Modal_Content() {
		
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Verify_Email_Modal_Details(transferee.MailList.get(0));
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_053_Verify Transferee HBO Field Code Value On Email Modal - Document")
	@Issues({"#AA-25"})
	public void TC_053_Verify_Transferee_HBO_field_Code_Value_Email_Modal_Document() {
		
		//Make Up Data:
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		//Steps:
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Open_Modal_Sending_Email_Form(transferee.MailList.get(0));
		
		Admin.TransfManage.Verify_Email_Modal_Document_Content_Is_Correct(newtpl1.DocName, newtpl1.Content);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_054_Verify Transferee HBO Field Code Value On Received Email and DOCX")
	 
	public void TC_054_Verify_Transf_HBO_field_Code_Received_email_Content_and_DOCX() {
		
		//Make Up Data:
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_055_Verify Transferee HBO Field Code On Received Email and PDF")
	 
	public void TC_055_Verify_Transf_HBO_field_Code_Received_email_Content_and_PDF() {
		
		//Make Up Data:
		newtpl1.DocProcessType = "PDF";
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	
	

}
