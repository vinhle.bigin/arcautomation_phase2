package testcases.TemplateManage;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.AddressTemplate_Info;
import businessObjects.LetterTemplate_info;
import businessObjects.Template_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class SearchTemplate_001_002_003  extends TestBase{

	public SearchTemplate_001_002_003(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	static Template_Info newtpl1;

	static Template_Info newtpl2;

	static Template_Info newtpl3;
	
	@Before
	public void setup()
	{
		
		super.setup();
		
		newtpl1 = new AddressTemplate_Info();

		newtpl2 = new LetterTemplate_info();

		newtpl3 = new Template_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		List<Template_Info> list = new ArrayList<Template_Info>();

		list.add(newtpl1);

		list.add(newtpl2);

		list.add(newtpl3);
		
		for(int i =0;i<list.size();i++)
			
			Admin.TemplateManage.Create_New_Template(list.get(i));

	
		
	}
	
	
	
	

	
	@Test
	@Title("TC_001_Verify Template Address Lable Type Can Be Search")
	 
	public void Verify_Template_Address_Lable_Type_Can_Be_Search() {
     
	Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl1);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl1, "When Search with Address Type condition");
		
		Admin.TemplateManage.Verify_Template_Not_Displayed_On_List(newtpl2);
		
		Admin.TemplateManage.Verify_Template_Not_Displayed_On_List(newtpl3);
		
		
			
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_002_Verify Template Email Type Can Be Search")
	 
	public void Verify_Template_Email_Type_Can_Be_Search() {
      
		
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl3);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl3, "When Search with Email Type condition");
		
		Admin.TemplateManage.Verify_Template_Not_Displayed_On_List(newtpl1);
		
		Admin.TemplateManage.Verify_Template_Not_Displayed_On_List(newtpl2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_003_Verify Template Letter Type Can Be Search")
	 
	public void Verify_Template_Letter_Type_Can_Be_Search() {
      
		Admin.TemplateManage.Search_For_Existing_Template("Type", newtpl2);
		
		Admin.TemplateManage.Verify_Template_Displayed_On_List(newtpl2, "When Search with Letter Type condition");
		
		Admin.TemplateManage.Verify_Template_Not_Displayed_On_List(newtpl1);
		
		Admin.TemplateManage.Verify_Template_Not_Displayed_On_List(newtpl3);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }

}
