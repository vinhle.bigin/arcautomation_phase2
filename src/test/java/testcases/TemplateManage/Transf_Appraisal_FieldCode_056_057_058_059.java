package testcases.TemplateManage;



import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.AppraisalService_Info;
import businessObjects.Contact_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class Transf_Appraisal_FieldCode_056_057_058_059  extends TestBase{
	
	 public Transf_Appraisal_FieldCode_056_057_058_059(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;
	 Transferee_Info transferee;
	 Vendor_Info vendor,vendor_2;

	@Before
	public void setup()
	{
		super.setup();
		
		//===========Prepare Test Data:
		//1. Prepare test data for Vendor and Contact Info
		vendor = new Vendor_Info();
		
		vendor_2 = new Vendor_Info();
		
		List<Vendor_Info> vendors_list = new ArrayList<Vendor_Info>();
		
		vendors_list.add(vendor);
		
		vendors_list.add(vendor_2);
				
		vendor_2.SearchOpt_IsUSA = vendor.SearchOpt_IsUSA = true;
		
		vendor_2.Contacts.get(0).ServiceType = vendor.Contacts.get(0).ServiceType = "Appraiser";
		
		vendor_2.Contacts.get(0).Init_Vendor_Contact_Info(vendor_2.VendorName);
		
		vendor_2.Contacts.get(0).ServiceType = vendor.Contacts.get(0).ServiceType;
		
		//2. Prepare test data for Appraisal Service
		transferee = new Transferee_Info();
		
		transferee.Appraisal_service = new AppraisalService_Info(vendor.Contacts.get(0));
		
		transferee.Appraisal_service.VendorCtactList.add(vendor_2.Contacts.get(0));
		
		transferee.Appraisal_service.InIt_Appraisal_Analysis();
		
		//3. Prepare test data for Template info
		newtpl1 = new Template_Info();
		
		newtpl1.ServiceType = transferee.Appraisal_service.Type;
		
		newtpl1.AddFieldCode_ApprisalInfo_func(transferee.Appraisal_service);
		
		transferee.MailList.get(0).RenewContentAppraisalInfo_func(transferee.Appraisal_service);
				
		Contact_Info vendor_contact_tmp = vendor.Contacts.get(0);
		
		//================END PREPARE TEST DATA
		
		//==========================================Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		for(Vendor_Info cur_vendor: vendors_list)
		{
			//1. Create Vendors #1,#2 and ContactS: CONTACT #1, #2
			Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(cur_vendor);
			 
			Admin.VendorManage.Update_Vendor_Search_Option_Tab(cur_vendor);
			 
			Admin.ContactManage.Create_New_Contact(cur_vendor.Contacts.get(0),true);
		}//end for
		
		
		//2. CREATE TRANSFEREE - ADD VENDOR CONTACT TO THE SERVICE
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		for(Vendor_Info cur_vendor: vendors_list)
		{
			Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.Appraisal_service.Type, cur_vendor.Contacts.get(0));
			
		}//end for
		
		//3. Update Services Details
		Admin.ServiceManage.Set_Vendor_Contact_as_Provideder_Service_Details(vendor_contact_tmp.firstname);
		
		Admin.ServiceManage.Update_Appraisal_Analysis_Detail(transferee.Appraisal_service);
		
		Admin.ServiceManage.Update_AppraisalSpecialist_Info(transferee.Appraisal_service);
			
}
	
	
	@Test
	@Title("TC_056_Verify Transferee Appraisal Field Code Value On Email Modal Content")
	@Issues({"#AA-39"})
	public void TC_056_Verify_Transferee_Appraisal_field_Code_Value_Email_Modal_Content() {
		
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Verify_Email_Modal_Details(transferee.MailList.get(0));
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_057_Verify Transferee Appraisal Field Code Value On Email Modal - Document")
	@Issues({"#AA-25"})
	public void TC_057_Verify_Transferee_Appraisal_field_Code_Value_Email_Modal_Document() {
		
		//Make Up Data:
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		//Steps:
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Open_Modal_Sending_Email_Form(transferee.MailList.get(0));
		
		Admin.TransfManage.Verify_Email_Modal_Document_Content_Is_Correct(newtpl1.DocName, newtpl1.Content);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_058_Verify Transferee Appraisal Field Code Value On Received Email and DOCX")
	@Issues({"#AA-25"})
	public void TC_058_Verify_Transf_Appraisal_field_Code_Received_email_Content_and_DOCX() {
		
		//Make Up Data:
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Contains");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_059_Verify Transferee Appraisal Field Code On Received Email and PDF")
	@Issues({"#AA-25"})
	public void TC_059_Verify_Transf_Appraisal_field_Code_Received_email_Content_and_PDF() {
		
		//Make Up Data:
		newtpl1.DocProcessType = "PDF";
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	


}
