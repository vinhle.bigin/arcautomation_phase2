package testcases.TemplateManage;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.HomeInspectService_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Catagory_Template;




@Category(Catagory_Template.class)
public class Transf_HomeInspect_FieldCode_060_061_062_063  extends TestBase{
	
	 public Transf_HomeInspect_FieldCode_060_061_062_063(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Template_Info newtpl1;
	 Transferee_Info transferee;
	 Vendor_Info vendor;

	@Before
	public void setup()
	{
		super.setup();
		
		vendor = new Vendor_Info();
		
		vendor.SearchOpt_IsUSA = true;
		
		vendor.Contacts.get(0).ServiceType = "Inspection Agents";
		
		transferee = new Transferee_Info();
		
		transferee.HomeInspect_service = new HomeInspectService_Info(vendor.Contacts.get(0));
		
		newtpl1 = new Template_Info();
		
		newtpl1.ServiceType = transferee.HomeInspect_service.Type;
		
		newtpl1.AddFieldCode_HomeInspectInfo_func(transferee.HomeInspect_service);
		
		transferee.MailList.get(0).RenewContent_HomeInspectInfo_func(transferee.HomeInspect_service);
				
		Contact_Info vendor_contact_tmp = vendor.Contacts.get(0);
		//Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(vendor);
		 
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(vendor.Contacts.get(0),true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		Admin.ServiceManage.Go_To_Service_Details(transferee.HomeInspect_service.Type);
		
		Admin.ServiceManage.Create_HomeInspect_Service_Inspection_Need(transferee.HomeInspect_service);
		
		Admin.ServiceManage.Update_HomeInspect_Service_Inspection_Result(transferee.HomeInspect_service);
		
		Admin.ServiceManage.Update_HomeInspect_Specialist(transferee.HomeInspect_service);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transferee.HomeInspect_service.Type, vendor_contact_tmp);
		
		Admin.ServiceManage.Set_Vendor_Contact_as_Provideder_Service_Details(vendor_contact_tmp.firstname);
		
}
	

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_060_Verify Transferee HomeInspect Field Code Value On Email Modal Content")
	@Issues({"#AA-25"})
	public void TC_060_Verify_Transferee_HomeInspect_field_Code_Value_Email_Modal_Content() {
		
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Verify_Email_Modal_Details(transferee.MailList.get(0));
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	@Test
	@Title("TC_061_Verify Transferee HomeInspect Field Code Value On Email Modal - Document")
	@Issues({"#AA-25"})
	public void TC_061_Verify_Transferee_HomeInspect_field_Code_Value_Email_Modal_Document() {
		
		//Make Up Data:
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		//Steps:
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Open_Modal_Sending_Email_Form(transferee.MailList.get(0));
		
		Admin.TransfManage.Verify_Email_Modal_Document_Content_Is_Correct(newtpl1.DocName, newtpl1.Content);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_062_Verify Transferee HomeInspect Field Code Value On Received Email and DOCX")
	 
	public void TC_062_Verify_Transf_HomeInspect_field_Code_Received_email_Content_and_DOCX() {
		
		//Make Up Data:
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_063_Verify Transferee HomeInspect Field Code On Received Email and PDF")
	 
	public void TC_063_Verify_Transf_HomeInspect_field_Code_Received_email_Content_and_PDF() {
		
		//Make Up Data:
		newtpl1.DocProcessType = "PDF";
		newtpl1.DocName = "email-template.docx";
		
		new FileManage().CreateWordFile_func(newtpl1.DocName, newtpl1.Content);
				
		transferee.MailList.get(0).Map_Template_Info_func(newtpl1, false);
		
		String expected_content = transferee.MailList.get(0).Content;
		
		//Steps:
		Admin.TemplateManage.Create_New_Template(newtpl1);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Send_Email_to_transfree(transferee.MailList.get(0), false);
		
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), expected_content,"Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	
	

}
