package testcases;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;

public class TC_SandBox3 extends TestBase{

	public TC_SandBox3(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	@Before
	public void setup()
	{
		super.setup();
		
     }
	
	
	
	@Category(Catagory_Parallel_2.class)
	@Test
	@Title("TC_001_LamdaTest")
	 
	public void TC_001_LamdaTest() {
		
		Admin.Login_to_ARC_Portal_as_admin();
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
    
	
	
	
}
