package testcases;


import org.junit.Assert;
import org.junit.Before;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import configuration.TestConfigs;

import custom_Func.Email_Manage;

import net.thucydides.core.annotations.Title;


public class TC_SandBox2 extends TestBase{

	
	 public TC_SandBox2(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO 
	 }
	
	@Before
	public void setup()
	{
		super.setup();
	}
	
	@Test
	@Category(Catagory_Parallel_2.class)
	@Title("TC_Testing_Parallel_2")
	public void TC_Testing_Parallel_2() {
		
		Email_Manage mail_connector = new Email_Manage("Gmail");
		
		mail_connector.FetchEmail_func(0);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
	}
	
}
