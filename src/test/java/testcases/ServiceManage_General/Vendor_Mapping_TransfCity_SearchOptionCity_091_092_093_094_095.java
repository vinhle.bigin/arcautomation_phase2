package testcases.ServiceManage_General;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;


@Category({Catagory_SmokeTest.class,Category_Services.class})
public class Vendor_Mapping_TransfCity_SearchOptionCity_091_092_093_094_095  extends TestBase{

	
	public Vendor_Mapping_TransfCity_SearchOptionCity_091_092_093_094_095(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	Vendor_Info Vendor_info;
	Contact_Info ctact_info;
	String search_City;

	@Before
	public void setup()
	{
		//Test Data:
		super.setup();
		search_City = "Test City";
		
		
		transf_info = new Transferee_Info();
		transf_info.OriResAddr_info.City = search_City;
		
		Vendor_info = new Vendor_Info();
		Vendor_info.SearchOpt_IsZip = true;
		
		Vendor_info.SearchOpt_CityList.add(search_City);
		
		ctact_info = Vendor_info.Contacts.get(0);
		
		//Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		 
		
	}
	

	

	
	@Test
	@Title("TC_091: HomeSale_City Mapping - Verify Vendor shown when Transferee City match SearchOption City")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_091_HS_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_SearchOption_City() {
      
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Realtor - Broker";
		
		String transf_servicename = transf_info.HS_service.Type;
		
		transf_info.OriResAddr_info.City = search_City;
		
				
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_092: HHG_City Mapping - Verify Vendor shown when Transferee City match SearchOption City")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_092_HHG_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_SearchOption_City() {
      
		//Test Data:
		ctact_info.ServiceType = "Van Lines";
	
		String transf_servicename = transf_info.HHG_service.Type;
		
		transf_info.OriResAddr_info.City = search_City;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_093: HomePurchase_City Mapping - Verify Vendor shown when Transferee City match SearchOption City")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_093_HP_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_SearchOption_City() {
      
		//Test Data:
		ctact_info.ServiceType = "Title and closing management company";
		
		String transf_servicename = transf_info.HP_service.Type;
		
		transf_info.DesOffAddr_info.City = search_City;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_094: TQ_City Mapping - Verify Vendor shown when Transferee City match SearchOption Cityg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_094_TQ_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_SearchOption_City() {
      
		//Test Data:
		ctact_info.ServiceType = "Interim Housing";
		
		String transf_servicename = transf_info.TQ_service.Type;
		
		transf_info.DesOffAddr_info.City = search_City;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	
	@Test
	@Title("TC_095: Mortgage_City Mapping - Verify Vendor shown when Transferee City match SearchOption Cityg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_095_MG_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_SearchOption_City() {
      
		//Test Data:
		transf_info.DesOffAddr_info.City = search_City;
		
		ctact_info.ServiceType = "Lender";
		
		String transf_servicename = transf_info.MG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	

	

}
