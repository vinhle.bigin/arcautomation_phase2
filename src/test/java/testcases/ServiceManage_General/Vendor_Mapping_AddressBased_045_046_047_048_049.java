package testcases.ServiceManage_General;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;
import testcases.Category_Services;


@Category({Catagory_SmokeTest.class,Category_Services.class})
public class Vendor_Mapping_AddressBased_045_046_047_048_049  extends TestBase{

	
	public Vendor_Mapping_AddressBased_045_046_047_048_049(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	Vendor_Info Vendor_info;
	

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		Vendor_info.SearchOpt_IsUSA = true;
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		
	}
	
	

	
	@Test
	@Title("TC_045: Home Sale - Vendor Shown Base On Origin - Residence Address Matching")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_045_HS_Vendor_shown_base_Origin_Residence_Address_match() {
      
		//Test Data:
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Realtor - Broker";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		String transf_servicename = transf_info.HS_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Category(Category_E2E.class)
	@Test
	@Title("TC_046: HHG - Vendor Shown Base On Origin - Residence Address Matching")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_046_HHG_Vendor_shown_base_Origin_Residence_Address_match() {
      
		//Test Data:
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
	
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		String transf_servicename = transf_info.HHG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	
	@Test
	@Title("TC_047: Mortgage - Vendor Shown Base On Destination - Office Address Matching")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_047_MG_Vendor_shown_base_Destination_Office_Address_match() {
      
		//Test Data:
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Lender";
		
		ctact_info.AddrInfo.City = transf_info.DesOffAddr_info.City;
		
		String transf_servicename = transf_info.MG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_048: HomePurchase - Vendor Shown Base On Destination - Office Address Matching")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_048_HP_Vendor_shown_base_Destination_Office_Address_match() {
      
		//Test Data:
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Title and closing management company";
		
		ctact_info.AddrInfo.City = transf_info.DesOffAddr_info.City;
		
		String transf_servicename = transf_info.HP_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_048: TQ Service - Vendor Shown Base On Destination - Office Address Matching")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_049_TQ_Vendor_shown_base_Destination_Office_Address_match() {
      
		//Test Data:
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Interim Housing";
		
		ctact_info.AddrInfo.City = transf_info.DesOffAddr_info.City;
		
		String transf_servicename = transf_info.TQ_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	

}
