package testcases.ServiceManage_General;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;


@Category(Category_Services.class)
public class Vendor_Mapping_TransfVendorSearchOption_State_NotMatch_081_082_083_084_085  extends TestBase{

	
	public Vendor_Mapping_TransfVendorSearchOption_State_NotMatch_081_082_083_084_085(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	Vendor_Info Vendor_info;
	Contact_Info ctact_info;
	

	@Before
	public void setup()
	{
		//Test Data:
	
		super.setup();
		String invalid_State = "Idaho";
		String vendorcontact_State = "Maryland";
		
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		Vendor_info.SearchOpt_IsZip = true;
		Vendor_info.SearchOpt_StateList.add(invalid_State);
		
		//
		ctact_info = Vendor_info.Contacts.get(0);
		ctact_info.AddrInfo.State = vendorcontact_State;
		
		//Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		 
		
	}
	

	

	
	@Test
	@Title("TC_081: HomeSale_State Mapping - Verify Vendor shown when Transferee State Not match Vendor State")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_081_HS_StateMapping_Verify_Vendor_shown_when_Transferee_State_Not_match_Vendor_State() {
      
				
		ctact_info.ServiceType = "Realtor - Broker";
				
		String transf_servicename = transf_info.HS_service.Type;
		
				
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_082: HHG_State Mapping - Verify Vendor shown when Transferee State Not match Vendor State")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_082_HHG_StateMapping_Verify_Vendor_shown_when_Transferee_State_Not_match_Vendor_State() {
      
		//Test Data:
		ctact_info.ServiceType = "Van Lines";
	
		String transf_servicename = transf_info.HHG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_083: HomePurchase_State Mapping - Verify Vendor shown when Transferee State Not match Vendor State")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_083_HP_StateMapping_Verify_Vendor_shown_when_Transferee_State_Not_match_Vendor_State() {
      
		//Test Data:
		ctact_info.ServiceType = "Title and closing management company";
		
		String transf_servicename = transf_info.HP_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_084: TQ_State Mapping - Verify Vendor shown when Transferee State Not match Vendor Stateg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_084_TQ_StateMapping_Verify_Vendor_shown_when_Transferee_State_Not_match_Vendor_State() {
      
		//Test Data:
		ctact_info.ServiceType = "Interim Housing";
		
		String transf_servicename = transf_info.TQ_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	
	@Test
	@Title("TC_085: Mortgage_State Mapping - Verify Vendor shown when Transferee State Not match Vendor Stateg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_085_MG_StateMapping_Verify_Vendor_shown_when_Transferee_State_Not_match_Vendor_State() {
      
		//Test Data:
		ctact_info.ServiceType = "Lender";
		
		String transf_servicename = transf_info.MG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	

	

}
