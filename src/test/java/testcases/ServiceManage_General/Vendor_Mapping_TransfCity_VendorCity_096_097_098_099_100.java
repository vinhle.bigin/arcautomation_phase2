package testcases.ServiceManage_General;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;


@Category(Category_Services.class)
public class Vendor_Mapping_TransfCity_VendorCity_096_097_098_099_100  extends TestBase{

	
	public Vendor_Mapping_TransfCity_VendorCity_096_097_098_099_100(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	Vendor_Info Vendor_info;
	Contact_Info ctact_info;
	

	
	@Before
	public void setup()
	{
		super.setup();
		//Test Data:
		String invalid_City = "Test City";
		
		
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		Vendor_info.SearchOpt_IsZip = true;
		Vendor_info.SearchOpt_CityList.add(invalid_City);
		
		
		ctact_info = Vendor_info.Contacts.get(0);
		
		
		//Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		 
		
	}
	

	
	

	
	@Test
	@Title("TC_096: HomeSale_City Mapping - Verify Vendor shown when Transferee City match Vendor City")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_096_HS_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_Vendor_City() {
      
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		ctact_info.ServiceType = "Realtor - Broker";
		
		String transf_servicename = transf_info.HS_service.Type;
		
				
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_097: HHG_City Mapping - Verify Vendor shown when Transferee City match Vendor City")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_097_HHG_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_Vendor_City() {
      
		//Test Data:
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		ctact_info.ServiceType = "Van Lines";
	
		String transf_servicename = transf_info.HHG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_098: HomePurchase_City Mapping - Verify Vendor shown when Transferee City match Vendor City")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_098_HP_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_Vendor_City() {
      
		//Test Data:
		ctact_info.AddrInfo.City = transf_info.DesOffAddr_info.City;
		
		ctact_info.ServiceType = "Title and closing management company";
		
		String transf_servicename = transf_info.HP_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_099: TQ_City Mapping - Verify Vendor shown when Transferee City match Vendor Cityg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_099_TQ_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_Vendor_City() {
      
		//Test Data:
		ctact_info.AddrInfo.City = transf_info.DesOffAddr_info.City;
		
		ctact_info.ServiceType = "Interim Housing";
		
		String transf_servicename = transf_info.TQ_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	
	@Test
	@Title("TC_100: Mortgage_City Mapping - Verify Vendor shown when Transferee City match Vendor Cityg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_100_MG_CityMapping_Verify_Vendor_shown_when_Transferee_City_match_Vendor_City() {
      
		//Test Data:
		ctact_info.AddrInfo.City = transf_info.DesOffAddr_info.City;
		
		ctact_info.ServiceType = "Lender";
		
		String transf_servicename = transf_info.MG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	

	

}
