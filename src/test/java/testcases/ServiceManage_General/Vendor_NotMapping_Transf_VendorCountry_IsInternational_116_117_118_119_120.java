package testcases.ServiceManage_General;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;


@Category(Category_Services.class)
public class Vendor_NotMapping_Transf_VendorCountry_IsInternational_116_117_118_119_120  extends TestBase{

	
	public Vendor_NotMapping_Transf_VendorCountry_IsInternational_116_117_118_119_120(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	Vendor_Info Vendor_info;
	Contact_Info ctact_info;
	String country_str;
	String country_str_2;

	
	@Before
	public void setup()
	{
		super.setup();
		
		//Test Data:
		 country_str = "Viet Nam";
		country_str_2 = "Brazil";
		
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		Vendor_info.SearchOpt_IsInter = true;
			
		ctact_info = Vendor_info.Contacts.get(0);
		
		
		//Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		 
		
	}
	

	
	

	
	@Test
	@Title("TC_116: HomeSale_Country Mapping - Verify Vendor shown when Transferee Country Not match Vendor Country")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_116_HS_CountryMapping_Verify_Vendor_shown_when_Transferee_Country_NOT_match_Vendor_Country() {
      
		ctact_info.AddrInfo.Country = country_str_2;
		transf_info.OriResAddr_info.Country = country_str;
		
		ctact_info.ServiceType = "Realtor - Broker";
		
		String transf_servicename = transf_info.HS_service.Type;
		
				
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_117: HHG_Country Mapping - Verify Vendor shown when Transferee Country Not match Vendor Country")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_117_HHG_CountryMapping_Verify_Vendor_shown_when_Transferee_Country_NOT_match_Vendor_Country() {
      
		//Test Data:
		ctact_info.AddrInfo.Country = country_str_2;
		
		transf_info.OriResAddr_info.Country = country_str;
		
		
		ctact_info.ServiceType = "Van Lines";
	
		String transf_servicename = transf_info.HHG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_118: HomePurchase_Country Mapping - Verify Vendor shown when Transferee Country Not match Vendor Country")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_118_HP_CountryMapping_Verify_Vendor_shown_when_Transferee_Country_NOT_match_Vendor_Country() {
      
		//Test Data:
		ctact_info.AddrInfo.Country = country_str_2;
		
		transf_info.DesOffAddr_info.Country = country_str;
		
		ctact_info.ServiceType = "Title and closing management company";
		
		String transf_servicename = transf_info.HP_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_119: TQ_Country Mapping - Verify Vendor shown when Transferee Country Not match Vendor Countryg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_119_TQ_CountryMapping_Verify_Vendor_shown_when_Transferee_Country_NOT_match_Vendor_Country() {
      
		//Test Data:
		ctact_info.AddrInfo.Country = country_str_2;
		
		transf_info.DesOffAddr_info.Country = country_str;
		
		ctact_info.AddrInfo.Country = transf_info.DesOffAddr_info.Country;
		
		ctact_info.ServiceType = "Interim Housing";
		
		String transf_servicename = transf_info.TQ_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	
	@Test
	@Title("TC_120: Mortgage_Country Mapping - Verify Vendor shown when Transferee Country Not match Vendor Countryg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_120_MG_CountryMapping_Verify_Vendor_shown_when_Transferee_Country_NOT_match_Vendor_Country() {
      
		//Test Data:
		ctact_info.AddrInfo.Country = country_str_2;
		
		transf_info.DesOffAddr_info.Country = country_str;
		
		ctact_info.ServiceType = "Lender";
		
		String transf_servicename = transf_info.MG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	

	

}
