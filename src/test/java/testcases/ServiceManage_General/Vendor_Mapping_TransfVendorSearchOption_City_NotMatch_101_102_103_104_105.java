package testcases.ServiceManage_General;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;


@Category(Category_Services.class)
public class Vendor_Mapping_TransfVendorSearchOption_City_NotMatch_101_102_103_104_105  extends TestBase {

	public Vendor_Mapping_TransfVendorSearchOption_City_NotMatch_101_102_103_104_105(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	Vendor_Info Vendor_info;
	Contact_Info ctact_info;
	

	
	@Before
	public void setup()
	{
		super.setup();
		//Test Data:
		String invalid_City = "Test City 1";
		String vendorcontact_City = "Test City 2";
	
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		Vendor_info.SearchOpt_IsZip = true;
		Vendor_info.SearchOpt_CityList.add(invalid_City);
		
		//
		ctact_info = Vendor_info.Contacts.get(0);
		ctact_info.AddrInfo.City = vendorcontact_City;
		
		//Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		 
		
	}
	

	

	
	@Test
	@Title("TC_101: HomeSale_City Mapping - Verify Vendor shown when Transferee City NOT match Vendor City")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_101_HS_Mapping_Verify_Vendor_shown_when_Transferee_City_Not_Match_Vendor_City() {
      
				
		ctact_info.ServiceType = "Realtor - Broker";
				
		String transf_servicename = transf_info.HS_service.Type;
		
				
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_102: HHG_City Mapping - Verify Vendor shown when Transferee City NOT match Vendor City")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_102_HHG_Mapping_Verify_Vendor_shown_when_Transferee_City_Not_Match_Vendor_City() {
      
		//Test Data:
		ctact_info.ServiceType = "Van Lines";
	
		String transf_servicename = transf_info.HHG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_103: HomePurchase_City Mapping - Verify Vendor shown when Transferee City NOT match Vendor City")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_103_HP_Mapping_Verify_Vendor_shown_when_Transferee_City_Not_Match_Vendor_City() {
      
		//Test Data:
		ctact_info.ServiceType = "Title and closing management company";
		
		String transf_servicename = transf_info.HP_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_104: TQ_City Mapping - Verify Vendor shown when Transferee City NOT match Vendor Cityg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_104_TQ_Mapping_Verify_Vendor_shown_when_Transferee_City_Not_Match_Vendor_City() {
      
		//Test Data:
		ctact_info.ServiceType = "Interim Housing";
		
		String transf_servicename = transf_info.TQ_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	
	@Test
	@Title("TC_105: Mortgage_City Mapping - Verify Vendor shown when Transferee City NOT match Vendor Cityg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_105_MG_Mapping_Verify_Vendor_shown_when_Transferee_City_Not_Match_Vendor_City() {
      
		//Test Data:
		ctact_info.ServiceType = "Lender";
		
		String transf_servicename = transf_info.MG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	

	

}
