package testcases.ServiceManage_General;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;



@Category({Catagory_SmokeTest.class,Category_Services.class})
public class Transferee_ServiceDetails_025_026_027_028_029  extends TestBase{
	

	public Transferee_ServiceDetails_025_026_027_028_029(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// for testing TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	
	@Before
	public void setup()
	{
		super.setup();
		transf_info = new Transferee_Info();
		
	}
	

	@Test
	@Title("TC_025: Transferee - Home Sale Detail Info Can Be Editted")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_025_Verify_Transferee_HomeSale_Detail_Info_Can_Be_Editted() {
      
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Update_transfree_HomeSale_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HS_service.Type);
		
		Admin.ServiceManage.Verify_transfee_HomeSale_Info_is_correct(transf_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_026: Transferee - Home Purchase Detail Info Can Be Editted")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_026_Verify_Transferee_HomePurChase_Detail_Info_Can_Be_Editted() {
      
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Update_transfree_HomePurchase_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HP_service.Type);
		
		Admin.ServiceManage.Verify_transfee_HomePurchase_Info_is_correct(transf_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_027: Transferee - Mortgage Detail Info Can Be Editted")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_027_Verify_Transferee_Mortgage_Detail_Info_Can_Be_Editted() {
      
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Update_transfree_Morgage_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.MG_service.Type);
		
		Admin.ServiceManage.Verify_transfee_Morgage_Info_is_correct(transf_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_028: Transferee - TQ Detail Info Can Be Editted")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_028_Verify_Transferee_TQ_Detail_Info_Can_Be_Editted() {
      
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Update_transfree_TQ_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.TQ_service.Type);
		
		Admin.ServiceManage.Verify_transfee_TQ_Info_is_correct(transf_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	

	
	
	
	
	

}
