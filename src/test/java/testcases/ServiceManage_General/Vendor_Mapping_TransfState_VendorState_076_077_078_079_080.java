package testcases.ServiceManage_General;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;


@Category(Category_Services.class)
public class Vendor_Mapping_TransfState_VendorState_076_077_078_079_080  extends TestBase{

	
	public Vendor_Mapping_TransfState_VendorState_076_077_078_079_080(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	Vendor_Info Vendor_info;
	Contact_Info ctact_info;
	

	@Before
	public void setup()
	{
		super.setup();
		//Test Data:
		String invalid_State = "Idaho";
		
	
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		Vendor_info.SearchOpt_IsZip = true;
		Vendor_info.SearchOpt_StateList.add(invalid_State);
		
		
		ctact_info = Vendor_info.Contacts.get(0);
		
		
		//Pre-conditions:
		Admin.Login_to_ARC_Portal_as_admin();
		
		 Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);
		 
		 Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		 
		
	}

	
	

	
	@Test
	@Title("TC_076: HomeSale_State Mapping - Verify Vendor shown when Transferee State match Vendor State")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_076_HS_StateMapping_Verify_Vendor_shown_when_Transferee_State_match_Vendor_State() {
      
		ctact_info.AddrInfo.State = transf_info.OriResAddr_info.State;
		
		ctact_info.ServiceType = "Realtor - Broker";
		
		String transf_servicename = transf_info.HS_service.Type;
		
				
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_077: HHG_State Mapping - Verify Vendor shown when Transferee State match Vendor State")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_077_HHG_StateMapping_Verify_Vendor_shown_when_Transferee_State_match_Vendor_State() {
      
		//Test Data:
		ctact_info.AddrInfo.State = transf_info.OriResAddr_info.State;
		
		ctact_info.ServiceType = "Van Lines";
	
		String transf_servicename = transf_info.HHG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_078: HomePurchase_State Mapping - Verify Vendor shown when Transferee State match Vendor State")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_078_HP_StateMapping_Verify_Vendor_shown_when_Transferee_State_match_Vendor_State() {
      
		//Test Data:
		ctact_info.AddrInfo.State = transf_info.DesOffAddr_info.State;
		
		ctact_info.ServiceType = "Title and closing management company";
		
		String transf_servicename = transf_info.HP_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_079: TQ_State Mapping - Verify Vendor shown when Transferee State match Vendor Stateg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_079_TQ_StateMapping_Verify_Vendor_shown_when_Transferee_State_match_Vendor_State() {
      
		//Test Data:
		ctact_info.AddrInfo.State = transf_info.DesOffAddr_info.State;
		
		ctact_info.ServiceType = "Interim Housing";
		
		String transf_servicename = transf_info.TQ_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	
	@Test
	@Title("TC_080: Mortgage_State Mapping - Verify Vendor shown when Transferee State match Vendor Stateg")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_080_MG_StateMapping_Verify_Vendor_shown_when_Transferee_State_match_Vendor_State() {
      
		//Test Data:
		ctact_info.AddrInfo.State = transf_info.DesOffAddr_info.State;
		
		ctact_info.ServiceType = "Lender";
		
		String transf_servicename = transf_info.MG_service.Type;
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_servicename, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	

	

}
