package testcases.ServiceManage_HomeInspect;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;
import testcases.Category_Services;



@Category(Category_Services.class)
public class HomeInspect_001_002_003_004_005  extends TestBase
{
	
	public HomeInspect_001_002_003_004_005(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;

	
	@Before
	public void setup()
	{	
		super.setup();
		transf_info = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HomeInspect_service.Type);
		
		
	}
	
	
	
	
	
	@Category({Catagory_SmokeTest.class,Category_E2E.class})
	@Test
	@Title("TC_001: Home Inspection - Verify Home Inspect Specialist can be updated")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_001_Home_Inspection_Verify_Home_Inspect_Specialist_Can_updated() {
      
		Admin.ServiceManage.Update_HomeInspect_Specialist(transf_info.HomeInspect_service);
		
		Admin.ServiceManage.Verify_Inspection_Specialist_ViewMode(transf_info.HomeInspect_service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }//end test
	
	
	@Test
	@Title("TC_002: Home Inspection - Verify Home Inspect Result Section can be updated")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_002_Home_Inspection_Verify_Home_Inspect_Result_Section_Can_updated() {
      
		Admin.ServiceManage.Update_HomeInspect_Service_Inspection_Result(transf_info.HomeInspect_service);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HomeInspect_service.Type);
		
		Admin.ServiceManage.Verify_Inspection_Result_ViewMode(transf_info.HomeInspect_service);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }//end test
	
	

	@Test
	@Title("TC_003: Home Inspection - Verify Home Inspect Need Can Be Created")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_003_Home_Inspection_Verify_Home_Inspect_Need_Section_Can_Created() {
      
		Admin.ServiceManage.Create_HomeInspect_Service_Inspection_Need(transf_info.HomeInspect_service);
		
		Admin.ServiceManage.Verify_InspectNeed_Shown_On_Table(transf_info.HomeInspect_service);
		
		Admin.ServiceManage.Verify_Modal_InspectNeed_Info_ViewMode(transf_info.HomeInspect_service,"After Creating");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }//end test
	
	
	@Test
	@Title("TC_004: Home Inspection - Verify Home Inspect Need Can Be Updated")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_004_Home_Inspection_Verify_Home_Inspect_Need_Section_Can_Updated() {
		
		String old_inspectType = transf_info.HomeInspect_service.InspectType;
		      
		Admin.ServiceManage.Create_HomeInspect_Service_Inspection_Need(transf_info.HomeInspect_service);
		
		transf_info.HomeInspect_service.ReInit_ServiceInfo();
		Admin.ServiceManage.Update_HomeInspect_Service_InspectionNeed(old_inspectType,transf_info.HomeInspect_service);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HomeInspect_service.Type);
		
		Admin.ServiceManage.Verify_InspectNeed_Shown_On_Table(transf_info.HomeInspect_service);
		
		Admin.ServiceManage.Verify_Modal_InspectNeed_Info_ViewMode(transf_info.HomeInspect_service,"After Updating InspectNeed");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }//end test
	
	@Test
	@Title("TC_005: Home Inspection - Verify Home Inspect Need Can Be Removed")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_005_Home_Inspection_Verify_Home_Inspect_Need_Section_Can_Removed() {
		
		Admin.ServiceManage.Create_HomeInspect_Service_Inspection_Need(transf_info.HomeInspect_service);
		
		Admin.ServiceManage.Remove_HomeInspect_Service_InspectionNeed(transf_info.HomeInspect_service.InspectType);
		
		Admin.ServiceManage.Verify_InspectNeed_Not_Shown_On_Table(transf_info.HomeInspect_service.InspectType,"After the InspectNeed is removed.\n");
		
		
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }//end test

	
	
	
	
	

}
