package testcases.ServiceManage_HomeInspect;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;



@Category(Category_Services.class)
public class HomeInspect_Sync_Address_010_011  extends TestBase
{

	
	public HomeInspect_Sync_Address_010_011(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;

	
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
	}

	
	
	@Test
	@Title("TC_010 HomeInspect- Property Address information is loaded from the Destination Residence address")
	public void TC_010_HomeInspect_Address_information_loaded_from_DesRes_Addres() {
      
		//Create Transferee
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HomeInspect_service.Type);
		
		Admin.ServiceManage.Verify_ServiceDetails_Property_Address_Is_Correct_ViewMode(transf_info.HomeInspect_service.Type,transf_info.DesResAddr_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC_011 HomeInspect - Transferee DesRes Address is refreshed based on updated HomeInspect Property")
	public void TC_011_Transferee_DesRes_Address_refreshed_based_updated_HomeInspect_Property() {
      
		//Create Transferee
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HomeInspect_service.Type);
		
		Admin.ServiceManage.Update_ServiceDetails_Property_Section(transf_info.HomeInspect_service.Type,transf_info.DesResAddr_info);
		
		Admin.TransfManage.Verify_transf_Addr_DesRes_is_correct(transf_info.DesResAddr_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}


}
