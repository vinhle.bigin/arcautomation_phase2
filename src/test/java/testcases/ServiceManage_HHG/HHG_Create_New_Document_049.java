package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Document_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Documents;
import testcases.Category_Services;


@Category({Category_Services.class,Catagory_Documents.class})
public class HHG_Create_New_Document_049  extends TestBase{

	
	
	public HHG_Create_New_Document_049(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	

	@Before
	public void setup()
	{
		
		
		super.setup();
		transf_info = new Transferee_Info();
		
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
	}


	
	@Test
	@Title("TC 049 HHG Verify New Document Is Created Successfully")
	public void TC_049_HHG_Verify_New_Document_Is_Created_Successfully() 
	{
		Document_Info document = new Document_Info();
		
		document.ServiceType = "Household goods";
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HHG_service.Type);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Admin.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
}