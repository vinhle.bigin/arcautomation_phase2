package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Note_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Notes;
import testcases.Category_Services;


@Category({Category_Services.class,Catagory_Notes.class})
public class HHG_Create_New_Note_048  extends TestBase{

	
	
	public HHG_Create_New_Note_048(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	
	Vendor_Info Vendor_info;
	
	Service_Order_Info order;
	
	Note_Info note_info;

	
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		note_info = new Note_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		transf_info.HHG_service.Type = "Household goods";
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);	

	}


	
	@Test
	@Title("TC 048 HHG Verify Note of HHG is created successfully")
	public void TC_048_HHG_Verify_Note_of_HHG_is_created_successfully() {
		
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		note_info.Service = "Household goods";
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(note_info.Service, ctact_info);
		
		Admin.NoteManage.Create_New_Note_From_Note_Tab(note_info);
		
		Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_info.Title);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_info,true);
		
		Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
	}
}
