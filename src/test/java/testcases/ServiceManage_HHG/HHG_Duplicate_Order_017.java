package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_E2E;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HHG_Duplicate_Order_017  extends TestBase{

	
	
	public HHG_Duplicate_Order_017(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	Service_Order_Info order;
	

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		order = new Service_Order_Info();
		
		order.InIt_International_Order_Info();
		
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		transf_info.HHG_service.Type = "Household goods";
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Create_New_HHG_Service_Order(order);
	}

	@Category(Category_E2E.class)
	@Test
	@Title("TC 017 Verify Order is copied successfully")
	public void TC_017_Verify_Order_is_copied_successfully() {
	
		Admin.ServiceManage.Duplicate_New_HHG_Service_Order(order);
			
		order.Description = order.Description+"_Copy"; 
		
		Admin.ServiceManage.Verify_Vendor_Contact_Can_Assigned_Order(order);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ShipmentInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_Insurance_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_StorageInTransit_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillingInvoice_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}

}
