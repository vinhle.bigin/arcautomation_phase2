package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.HHGService_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HHG_Add_Contact_Vendor_007_008  extends TestBase{

	
	public HHG_Add_Contact_Vendor_007_008(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	Vendor_Info Vendor_info;
	

	
	@Before
	public void setup()
	{
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		Vendor_info.SearchOpt_IsInter = true;
		
		Vendor_info.Contacts.get(0).ServiceType = "Van Lines";
		
		transf_info = new Transferee_Info();
		
		transf_info.HHG_service = new HHGService_Info(Vendor_info.Contacts.get(0));
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);	
		
		Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		
	}

	
	
	@Test
	@Title("TC 007 HHG Verify Vendor Contact Can Be Add In The Table")
	@Issues({"#AA-37"})
	public void TC_007_HHG_Verify_Vendor_Contact_Can_Be_Add_In_The_Table() 
	{
		Admin.ContactManage.Create_New_Contact(Vendor_info.Contacts.get(0),true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info, true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HHG_service.Type);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, Vendor_info.Contacts.get(0));
	
		Admin.ServiceManage.Verify_Vendor_Contact_Shown_Service_Assigned_Contact_Table(transf_info.HHG_service.Type,Vendor_info.Contacts.get(0));
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_008 HHG Verify Only Contact With Same Vendor Name Can Be Added Into Service")
	public void TC_008_HHG_Verify_Only_Contact_With_Same_Vendor_Name_Can_Added_Into_Service() {
		
		Contact_Info vendor_contact2 = new Contact_Info();
		
		vendor_contact2.Init_Vendor_Contact_Info(Vendor_info.VendorName);
		
		vendor_contact2.ServiceType = Vendor_info.Contacts.get(0).ServiceType;
		
		Vendor_info.Contacts.add(vendor_contact2);
		
		//Create Vendor Contact
		for(int i =0;i<Vendor_info.Contacts.size();i++)
			Admin.ContactManage.Create_New_Contact(Vendor_info.Contacts.get(i),true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info, true);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HHG_service.Type);
		
		//Make sure Main contact is shown before adding the #2 to the Service
		Admin.ServiceManage.Verify_Service_Details_Vendor_Selection_List_Is_Correct(transf_info.HHG_service.Type, Vendor_info.Contacts.get(1));
		
		//if the Validate Above is failed, stop the execution
		if(TestConfigs.glb_TCStatus==true)
		{
			Admin.ServiceManage.Select_VendorContact_On_CustomSearch(Vendor_info.Contacts.get(0));
			
			Admin.ServiceManage.Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(transf_info.HHG_service.Type, Vendor_info.Contacts.get(1));
		}
	
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
			
	}
	
}
		