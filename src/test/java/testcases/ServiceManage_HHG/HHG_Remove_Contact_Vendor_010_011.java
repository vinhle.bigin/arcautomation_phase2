package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HHG_Remove_Contact_Vendor_010_011  extends TestBase{

	
	public HHG_Remove_Contact_Vendor_010_011(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	Vendor_Info Vendor_info;
	
	Service_Order_Info order;
	
	Contact_Info ctact_info;
	

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		order = new Service_Order_Info();
		
		ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		transf_info.HHG_service.Type = "Household goods";
		
		order.InIt_Dosmetic_Order_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);	
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
	}


	
	@Test
	@Title("TC 010 HHG Verify Vendor Contact Can Be Removed In The Table")
	public void TC_010_HHG_Verify_Vendor_Contact_Can_Be_Removed_In_The_Table() {
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);

		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, ctact_info);
		
		Admin.ServiceManage.Remove_VendorContact_On_VendorList(ctact_info);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Not_Shown_Service_Assigned_Contact_Table(transf_info.HHG_service.Type, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC 011 Verify Selected Vendor Can Be Removed In The Table")
	public void TC_011_Verify_Selected_Vendor_Can_Be_Removed_In_The_Table() {
		
		Vendor_Info Vendor_info2 = new Vendor_Info();
		
		Contact_Info vendor_contact2 = new Contact_Info();
		
		vendor_contact2 = Vendor_info2.Contacts.get(0);
		
		vendor_contact2.ServiceType = "Van Lines";
		
		vendor_contact2.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		order.InIt_VendorSelected(ctact_info.VendorName);
		
		String Expected_msg = "The vendor: " + Vendor_info.VendorName +"is already selected for HHG order: " + order.Description + ". Please, deselect this vendor before removing this vendor out the HHG service.";

		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info2);
		
		Admin.ContactManage.Create_New_Contact(vendor_contact2,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, ctact_info);
		
		Admin.ServiceManage.Create_New_HHG_Service_Order(order); 
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Remove_VendorContact_On_VendorList(ctact_info);
		
		Admin.Verify_Error_Message_Is_Shown(Expected_msg, "when trying to removed vendor contact from Contact List that assigned to Order.\n");
		
		order.InIt_VendorSelected(vendor_contact2.VendorName);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, vendor_contact2);
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Remove_VendorContact_On_VendorList(ctact_info);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Not_Shown_Service_Assigned_Contact_Table(transf_info.HHG_service.Type, ctact_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
}
