package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Contact_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Activity;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;


@Category({Category_Services.class,Catagory_Activity.class})
public class HHG_Create_New_Activity_047  extends TestBase{

	
	public HHG_Create_New_Activity_047(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	
	Vendor_Info Vendor_info;
	
	Service_Order_Info order;
	
	Contact_Info ctact_info;
	Activity_Info activity;
	

	@Before
	public void setup()
	{
		super.setup();
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		Vendor_info.SearchOpt_IsUSA = true;
		
		transf_info.HHG_service.Type = "Household goods";
		
		ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		order = new Service_Order_Info();
		
		order.InIt_International_Order_Info();
		
		order.InIt_VendorSelected(ctact_info.VendorName);
				
		activity = new Activity_Info(order.Description, order.VendorName);
		
		activity.Assignee = transf_info.FullName;
		
		activity.Service = transf_info.HHG_service.Type;
		
		activity.Assignee  = transf_info.FullName;
		
		//Pre-condition: Create Vendor, Vendor Contact
		
		Admin.Login_to_ARC_Portal_as_admin();	 
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);	
		
		Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);

	}

	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 047 HHG Verify Activity of HHG is created successfully")
	public void TC_047_HHG_Verify_Activity_of_HHG_is_created_successfully() {
		
		
		//Create Transferee with the address match Vendor's Contact address
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(activity.Service, ctact_info);
		
		//Create HHG SErvice Order
		
		Admin.ServiceManage.Create_New_HHG_Service_Order(order);
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		//Create activities that associate to the Order
		
		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
			
		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
	}
}