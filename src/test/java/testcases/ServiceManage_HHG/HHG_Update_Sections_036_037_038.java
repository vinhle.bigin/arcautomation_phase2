package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HHG_Update_Sections_036_037_038  extends TestBase{

	
	
	public HHG_Update_Sections_036_037_038(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	Vendor_Info Vendor_info;
	
	Contact_Info ctact_info;
	
	Service_Order_Info order;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		order = new Service_Order_Info();
		
		ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		transf_info.HHG_service.Type = "Household goods";
		
		order.InIt_VendorSelected(ctact_info.VendorName);
		
		order.InIt_Dosmetic_Order_Info();
		
		order.InIt_OrderInfo();
		
		order.InIt_ServiceDates();
		
		order.InIt_ShipmentInfo();
		
		order.InIt_Insurance();
		
		order.InIt_BillStorage();
		
		order.InIt_AutoTransport();
		
		order.Init_StorageInfo();
		
		order.InIt_FacilityInfo();
		
		order.InIt_StorageTransit();
		
		order.InIt_ServiceCharge();
		
		order.InIt_BillingInvoice();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);		 
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, ctact_info);

	}

	
	@Test
	@Title("TC 036 HHG Verify All Sections of UAB Type Is Updated Successfully")
	public void TC_036_HHG_Verify_All_Sections_of_UAB_Is_Updated_Successfully() 
	{
		order.ServiceType = "UAB";

		Admin.ServiceManage.Create_New_HHG_Service_Order(order);
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
		
		Admin.ServiceManage.Update_HHG_ShipmentInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_Insurance_Section(order);
		
		Admin.ServiceManage.Update_HHG_StorageTransit_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceCharge_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillingInvoice_Section(order);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Can_Assigned_Order(order);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ShipmentInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_Insurance_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_StorageInTransit_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillingInvoice_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);		
	}
	
	@Test
	@Title("TC 037 HHG Verify All Sections of SIT Type Is Updated Successfully")
	public void TC_037_HHG_Verify_All_Sections_of_SIT_Is_Updated_Successfully() 
	{
		order.ServiceType = "SIT";

		Admin.ServiceManage.Create_New_HHG_Service_Order(order); 
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
		
		Admin.ServiceManage.Update_HHG_StorageInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_Insurance_Section(order);
		
		Admin.ServiceManage.Update_HHG_FacilityInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceCharge_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillStorage_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillingInvoice_Section(order);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Can_Assigned_Order(order);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_StorageInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_Insurance_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_FacilityInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillStorage_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillingInvoice_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);	
	}
	
	@Test
	@Title("TC 038 HHG Verify All Sections of POV Type Is Updated Successfully")
	public void TC_038_HHG_Verify_All_Sections_of_POV_Is_Updated_Successfully() 
	{
		order.ServiceType = "POV";

		Admin.ServiceManage.Create_New_HHG_Service_Order(order);
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
		
		Admin.ServiceManage.Update_HHG_ShipmentInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_Insurance_Section(order);
		
		Admin.ServiceManage.Update_HHG_AutoTransport_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceCharge_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillingInvoice_Section(order);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Can_Assigned_Order(order);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ShipmentInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_Insurance_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_AutoTransport_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillingInvoice_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);		
	}
	
}