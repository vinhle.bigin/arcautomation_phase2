package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;


public class HHG_Update_Sections_033_034_035  extends TestBase{


	
	public HHG_Update_Sections_033_034_035(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	Vendor_Info Vendor_info;
	
	Contact_Info ctact_info;
	
	Service_Order_Info order;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		order = new Service_Order_Info();
		
		ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		transf_info.HHG_service.Type = "Household goods";

		order.InIt_Dosmetic_Order_Info();

		order.InIt_VendorSelected(ctact_info.VendorName);
		
		order.InIt_OrderInfo();
		
		order.InIt_ServiceDates();
		
		order.InIt_ShipmentInfo();
		
		order.InIt_Insurance();
		
		order.InIt_AutoTransport();
		
		order.Init_StorageInfo();
		
		order.InIt_BillStorage();
		
		order.InIt_FacilityInfo();
		
		order.InIt_StorageTransit();
		
		order.InIt_ServiceCharge();
		
		order.InIt_BillingInvoice();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);		 
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, ctact_info);

	}

	
	
	@Test
	@Title("TC 033 HHG Verify All Sections of HHG Type Is Updated Successfully")
	public void TC_033_HHG_Verify_All_Sections_of_HHG_Is_Updated_Successfully() 
	{	
		order.ServiceType = "HHG";

		Admin.ServiceManage.Create_New_HHG_Service_Order(order); 
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
		
		Admin.ServiceManage.Update_HHG_ShipmentInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceCharge_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillingInvoice_Section(order);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Can_Assigned_Order(order);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ShipmentInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillingInvoice_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC 034 HHG Verify All Sections of STG Type Is Updated Successfully")
	public void TC_034_HHG_Verify_All_Sections_of_STG_Is_Updated_Successfully() 
	{	
		order.ServiceType = "STG";

		Admin.ServiceManage.Create_New_HHG_Service_Order(order); 
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
		
		Admin.ServiceManage.Update_HHG_StorageInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_Insurance_Section(order);
		
		Admin.ServiceManage.Update_HHG_FacilityInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceCharge_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillingInvoice_Section(order);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Can_Assigned_Order(order);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_StorageInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_Insurance_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_FacilityInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillingInvoice_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC 035 HHG Verify All Sections of EXT Type Is Updated Successfully")
	public void TC_035_HHG_Verify_All_Sections_of_EXT_Is_Updated_Successfully() 
	{
		order.ServiceType = "EXT";

		Admin.ServiceManage.Create_New_HHG_Service_Order(order); 
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
		
		Admin.ServiceManage.Update_HHG_StorageInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_Insurance_Section(order);
		
		Admin.ServiceManage.Update_HHG_FacilityInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceCharge_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillStorage_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillingInvoice_Section(order);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Can_Assigned_Order(order);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_StorageInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_Insurance_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_FacilityInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillStorage_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillingInvoice_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
}
