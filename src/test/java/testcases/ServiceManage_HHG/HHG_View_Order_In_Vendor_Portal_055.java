package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.VendorUser_Steps;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HHG_View_Order_In_Vendor_Portal_055  extends TestBase{


		
	public HHG_View_Order_In_Vendor_Portal_055(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	
	Service_Order_Info order;
	
	Contact_Info ctact_info;
	
	Vendor_Info Vendor_info;
	
	@Steps
	VendorUser_Steps Vendor;
	

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		ctact_info = new Contact_Info();
		
		ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		order = new Service_Order_Info();
		
		order.InIt_International_Order_Info();

		order.InIt_VendorSelected(ctact_info.VendorName);
		
		order.InIt_OrderInfo();
		
		order.InIt_ServiceDates();
		
		order.InIt_ShipmentInfo();
		
		order.InIt_Insurance();
		
		order.InIt_StorageTransit();
		
		order.InIt_ServiceCharge();
		
		order.InIt_BillingInvoice();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);		 
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Vendor.ContactManage.Update_Contact_Profile_Account(ctact_info);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, ctact_info);
	}


	
	@Test
	@Title("TC 055 HHG View Order Details In Vendor Portal")
	public void TC_055_HHG_View_Order_Details_In_Vendor_Portal() {
	
		Admin.ServiceManage.Create_New_HHG_Service_Order(order);
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
		
		Admin.ServiceManage.Update_HHG_ShipmentInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_Insurance_Section(order);
		
		Admin.ServiceManage.Update_HHG_StorageTransit_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceCharge_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillingInvoice_Section(order);
		
		Vendor.Login_to_VendorPortal(ctact_info.username, ctact_info.password);
		
		Admin.TransfManage.Search_for_Transferee_With_Firstname_Client_Portal(transf_info);
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HHG_service.Type);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ShipmentInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_Insurance_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_StorageInTransit_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	


	
}
