package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HHG_View_Order_In_Transf_Portal_051  extends TestBase{

	
	public HHG_View_Order_In_Transf_Portal_051(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Transferee_Info transf_info;
	
	Service_Order_Info order;
	
	Contact_Info ctact_info;
	
	Vendor_Info Vendor_info;
	
	@Steps
	TransfUser_Steps Transferee;


	@Before
	public void setup()
	{
		super.setup(); 
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		ctact_info = new Contact_Info();
		
		Contact_Info ctact1_info = new Contact_Info();
		
		ctact1_info = Vendor_info.Contacts.get(0);
		
		ctact1_info.ServiceType = "Van Lines";
		
		ctact1_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		order = new Service_Order_Info();
		
		order.InIt_International_Order_Info();

		order.InIt_VendorSelected(ctact1_info.VendorName);
		
		order.InIt_OrderInfo();
		
		order.InIt_ServiceDates();
		
		order.InIt_ShipmentInfo();
		
		order.InIt_Insurance();
		
		order.InIt_StorageTransit();
		
		order.InIt_ServiceCharge();
		
		order.InIt_BillingInvoice();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);		 
		
		Admin.ContactManage.Create_New_Contact(ctact1_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, ctact1_info);
	}

	
	@Test
	@Title("TC 051 HHG View Order Details In Transferee Portal")
	@Issues({"#AA-26"})
	public void TC_051_HHG_View_Order_Details_In_Transferee_Portal() {
	
		Admin.ServiceManage.Create_New_HHG_Service_Order(order);
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Update_HHG_OrderInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceDates_Section(order);
		
		Admin.ServiceManage.Update_HHG_ShipmentInfo_Section(order);
		
		Admin.ServiceManage.Update_HHG_Insurance_Section(order);
		
		Admin.ServiceManage.Update_HHG_StorageTransit_Section(order);
		
		Admin.ServiceManage.Update_HHG_ServiceCharge_Section(order);
		
		Admin.ServiceManage.Update_HHG_BillingInvoice_Section(order);
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Go_to_Transferees_Page();
		
		Admin.ServiceManage.Go_To_Service_Details(transf_info.HHG_service.Type);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Can_Assigned_Order(order);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ShipmentInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_Insurance_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_StorageInTransit_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	


}
