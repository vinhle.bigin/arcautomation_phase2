package testcases.ServiceManage_HHG;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;


@Category(Category_Services.class)
public class HHG_Create_New_Order_016  extends TestBase{

	
	
	public HHG_Create_New_Order_016(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	Transferee_Info transf_info;
	
	Vendor_Info Vendor_info;

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		Vendor_info = new Vendor_Info();
		
		Vendor_info.SearchOpt_IsUSA = true;
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(Vendor_info);	
		
		Admin.VendorManage.Update_Vendor_Search_Option_Tab(Vendor_info);
		
	}


	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 016 Create Order is created successfully")
	public void TC_016_Create_Order_is_created_successfully() {
      
		//Test Data:
		Contact_Info ctact_info = Vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		Service_Order_Info order = new Service_Order_Info();
		
		order.InIt_International_Order_Info();
		
		order.InIt_VendorSelected(ctact_info.VendorName);
		
		transf_info.HHG_service.Type = "Household goods";
		
		//Create Vendor Contact
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, ctact_info);
		
		Admin.ServiceManage.Create_New_HHG_Service_Order(order);
		
		Admin.ServiceManage.Update_HHG_VendorName_Section(order);
		
		Admin.ServiceManage.Verify_Vendor_Contact_Can_Assigned_Order(order);
		
		Admin.ServiceManage.Verify_HHG_OrderInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceDates_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ShipmentInfo_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_Insurance_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_StorageInTransit_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_ServiceCharge_Section_Is_Correct(order);
		
		Admin.ServiceManage.Verify_HHG_BillingInvoice_Section_Is_Correct(order);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
}
	

