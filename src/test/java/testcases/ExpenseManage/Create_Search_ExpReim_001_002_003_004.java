package testcases.ExpenseManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;


import baseClasses.TestBase;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import businessObjects.Activity_Info;
import businessObjects.BankAccount_Info;
import businessObjects.ReimBurs_Info;
import businessObjects.SubExpense_Info;
import businessObjects.Transferee_Info;
import stepsDefiniton.TransfUser_Steps;

public class Create_Search_ExpReim_001_002_003_004 extends TestBase{

	public Create_Search_ExpReim_001_002_003_004(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}
	
	Transferee_Info transf;
	ReimBurs_Info reim;
	SubExpense_Info subexp;
	BankAccount_Info payment;
	
	@Steps
	TransfUser_Steps Transferee;

	@Before
	public void setup()
	{
		super.setup();
		
		Transferee.GetConfig(exec_driver);
		
		transf = new Transferee_Info();
		
		reim = new ReimBurs_Info();
		
		subexp = new SubExpense_Info();
		
		payment = new BankAccount_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf);
		
		Admin.Log_Out_From_Portal();
		
		Transferee.Login_to_TransfereePortal(transf.username, transf.password);	
		
	}
	
	@Test
	@Title("TC_001_Verify New Expense Reimbursement is created successfully by Transferee")
	public void TC_001_Verify_New_Expense_Reim_is_created_successfully_by_Transferee() 
	{
		Transferee.ExpenseManage.Create_New_ExpReim(reim, subexp, payment);
		
		Transferee.ExpenseManage.Search_ExpReim(reim.Name);
		
		Transferee.ExpenseManage.Verify_ExpReimExistTable(reim);
		
		Transferee.ExpenseManage.Verify_ExpReimDetail(reim, subexp, payment);
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_002_Verify New Expense Reimbursement is updated successfully by Transferee")
	public void TC_002_Verify_New_Expense_Reim_is_updated_successfully_by_Transferee() 
	{
		Transferee.ExpenseManage.New_ExpReim_is_Saved_Draft(reim, subexp, payment);
		
		Transferee.ExpenseManage.Search_ExpReim(reim.Name);
		
		Transferee.ExpenseManage.Update_ExpReim(reim, subexp, payment);
		
		Transferee.ExpenseManage.Verify_ExpReimExistTable(reim);
		
		Transferee.ExpenseManage.Verify_ExpReimDetail(reim, subexp, payment);
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_003_The submitted expense reimbursement cannot be editted by Transferee")
	public void TC_003_The_Submitted_Expense_Reim_cannot_be_editted_by_Transferee() 
	{
		Transferee.ExpenseManage.Create_New_ExpReim(reim, subexp, payment);
		
		Transferee.ExpenseManage.Search_ExpReim(reim.Name);

		Transferee.ExpenseManage.Verify_ExpReim_Details_Can_NOT_Be_Editable(reim, subexp, payment);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Test
	@Title("TC_004_Verify Expense Reimbursement can be deleted")
	public void TC_004_Verify_ExpReim_Can_Be_Deleted()
	{
		Transferee.ExpenseManage.New_ExpReim_is_Saved_Draft(reim, subexp, payment);
		
		Transferee.ExpenseManage.Search_ExpReim(reim.Name);
		
		Transferee.ExpenseManage.Delete_ExpReim(reim);
		
		Transferee.ExpenseManage.Verify_ExpReim_Is_NOT_Shown_On_Table(reim, "After being deleted");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
}
