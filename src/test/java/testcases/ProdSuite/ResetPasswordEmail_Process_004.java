package testcases.ProdSuite;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Mail_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Category_PROD;


@Category(Category_PROD.class)
public class ResetPasswordEmail_Process_004  extends TestBase{
	
	
	public ResetPasswordEmail_Process_004(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	public Mail_Info data_mail;

	
	@Steps
	public TransfUser_Steps transf_user;


	@Before
	public void setup()
	{
		super.setup();
		
		data_mail = new Mail_Info();
		
		data_mail.Init_ResetPassword_Email_Info(glb_RefData.ARCPortal);
	}

		
		@Test
		@Title("TC_004_Verify ResetPassword Email Is Sent Successfully")
		public void TC_004_Verify_ResetPassword_Email_Is_Sent_Successfully()
		{
			//Reset Password
			Admin.Reset_Password(glb_RefData.ARCPortal,glb_RefData.glb_TestMailAccount);
					
			//Verify Email is sent successfully
			Admin.Verify_User_Receives_Email_Gmail(data_mail, "","Contains");
							
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		}

}
