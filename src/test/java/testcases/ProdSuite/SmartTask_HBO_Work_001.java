package testcases.ProdSuite;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import businessObjects.HBOService_Info;
import businessObjects.SmartTask_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Category_PROD;




@Category(Category_PROD.class)
public class SmartTask_HBO_Work_001 extends TestBase{
	
	
	public SmartTask_HBO_Work_001(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transferee;
	
	Client_Info  client_info;
	Template_Info newtpl;
	SmartTask_Info newtask;
 
	@Before
	public void setup()
	{
		super.setup();
	
		//pageobject = new PageObjects(test_cfig.glb_webdriver);
		
		//Template
		newtpl = new Template_Info();
		
		newtpl.Name = glb_RefData.Template;
		
		newtpl.ServiceType = "Home buyout";
		
		//1. Prepare test data for Client
		client_info =  glb_RefData.Client;
		
		//2. Prepare test data for HBO
		transferee = glb_RefData.Transferee;
		
		transferee.HBO_service = new HBOService_Info();
		
		transferee.HBO_service.Init_TransactionInfo("None");//"BVO", "Amended"
		
		transferee.Client =  client_info;
		
		transferee.MailList.get(0).Map_Template_Info_func(newtpl, true);
		
		//===========	//Pre-condition:
		Admin.Login_to_ARC_Portal_as_admin();
		
		//Update Template content
		Admin.TemplateManage.Update_Existing_Template(newtpl.Name, newtpl);
		
		//Update Transfere SErvice HBO info
		Update_HBO_Transaction_Acquist_Type_Info("BVO");

	       
		}
	
	

	@Issues({"#AA-44"})
	@Test
	@Title("TC_001_HBO Service- Verify Smarttask condition FieldChange + Action Send Email Work Correctly")
	 
	public void TC_001_HBOService_Verify_Smarttask_Condtion_FieldChange_Action_SendEmail_Works() {
		
		//Steps:
		String HBO_Acquist_type = "Amended";
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.TransfManage.Update_transfree_Email_section(transferee.MailList.get(0));
		
		Update_HBO_Transaction_Acquist_Type_Info(HBO_Acquist_type);
				
		//Verify Point
		Admin.Verify_User_Receives_Email_Gmail(transferee.MailList.get(0), "","Equals");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	///private methods
	private void Update_HBO_Transaction_Acquist_Type_Info(String transt_acquistype)
	{
		transferee.HBO_service.Transt_AcquisType = transt_acquistype;
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transferee);
		
		Admin.ServiceManage.Go_To_Service_Details(transferee.HBO_service.Type);
		
		Admin.ServiceManage.Updates_HBO_Transaction_Info(transferee.HBO_service);
	}

	
	
}
