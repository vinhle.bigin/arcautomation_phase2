package testcases.ProdSuite;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Document_Info;
import businessObjects.HBOService_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Title;
import testcases.Category_PROD;


@Category(Category_PROD.class)
public class Transf_DocumentManage_002  extends TestBase{

	public Transf_DocumentManage_002(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transferee;
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		transferee = glb_RefData.Transferee;
		
		transferee.HBO_service = new HBOService_Info();
		
		transferee.HBO_service.Init_TransactionInfo("None");//"BVO", "Amended"
		
		Admin.Login_to_ARC_Portal_as_admin();
		
	}

	@Test
	@Title("TC_002_Verify Transferee Document file Can Be Downloaded")
	public void TC_002_Verify_Document_Can_Be_Downloaded() 
	{
		//==TEST DATA
		//String destination_file_name = "DownloadFile_doc.docx";
		
		String Expect_content = "Document's Testing Content";
		
		Document_Info document = new Document_Info();
		
		new FileManage().CreateWordFile_func(document.file_name, Expect_content);
		
		//=======Steps:
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		//Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		//VErify
		if(TestConfigs.glb_TCStatus!=false)
		{
			Admin.DocumentManage.Download_Document(document.file_name, "");
			
			Admin.DocumentManage.Verify_Downloaded_File_Content_Is_Correct(document.file_name,Expect_content);
			
			//Post-condition-Delete the downloaded file:
			new FileManage().Delete_Downloaded_File_func(document.file_name);
		
		}
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	

}
