package testcases.ProdSuite;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Mail_Info;
import businessObjects.Note_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Category_PROD;


@Category(Category_PROD.class)
public class Transferee_Email_Process_003  extends TestBase{
	
	public Transferee_Email_Process_003(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	public Transferee_Info transf;

	public Template_Info newtpl;
	
	public Mail_Info data_mail;

	
	@Steps
	public TransfUser_Steps transf_user;


	@Before
	public void setup()
	{
		super.setup();
		
		//Template info
		newtpl = new Template_Info();
		
		newtpl.Name = glb_RefData.Template;
				
		newtpl.ServiceType = "Home buyout";
		
		//Transf info
		transf = glb_RefData.Transferee;
		
		transf.MailList.get(0).Map_Template_Info_func(newtpl, false);
		
		transf.MailList.get(0).Content = "This for TC Process Email.";

		data_mail = transf.MailList.get(0);
			
		//Pre-condition Steps
		Admin.Login_to_ARC_Portal_as_admin();
		
		//Update Template
		Admin.TemplateManage.Update_Existing_Template(newtpl.Name, newtpl);
		
		//Update Transferee SErvice Tab
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf);
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf);
		
	}

	
		@Test
		@Title("TC_003_Verify The sent Email is saved as a note after user receive email")
		public void TC_003_Verify_The_Sent_Email_Is_Saved_As_A_Note_After_User_Receive_Email()
		{
				
			Note_Info note = data_mail.Convert_To_Note_Info();
			
			Admin.TransfManage.Send_Email_to_transfree(data_mail,true);
			
			//Verify Email is sent successfully
			transf_user.Verify_User_Receives_Email_Gmail(data_mail, "","Contains");
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note,true);
				
			Admin.NoteManage.Verify_Modal_Note_Details_Correct(note);
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		}

}
