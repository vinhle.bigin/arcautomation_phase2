package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Notes;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Notes.class})
public class Transf_NoteMange_202  extends TestBase{

	public Transf_NoteMange_202(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	public Transferee_Info transf_info;
	public Note_Info note_Info1;



	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		note_Info1 = new Note_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
	}

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_202_Update_New_Note_From_Transf_List_Failed_by_AA-17")
	@Issues({"#AA-17"})
	public void TC_202_Verify_Note_Can_Be_Updated_Tranf_List() {
		
		Note_Info note_Info2 = new Note_Info();
		
		//String oldtitle = note_Info1.Title.get();
      
		Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info1);
		
		System.out.println(note_Info1.Service);			
		
		Admin.NoteManage.Update_Exiting_Note(note_Info1.Title, note_Info2);
	
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info1,true);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
}
