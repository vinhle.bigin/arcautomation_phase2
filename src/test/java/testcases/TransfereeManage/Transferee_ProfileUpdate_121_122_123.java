package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;



@Category({Category_TransfManage.class})
public class Transferee_ProfileUpdate_121_122_123  extends TestBase{
	

	public Transferee_ProfileUpdate_121_122_123(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Transferee_Info transf_info;
	

	
	@Steps
	TransfUser_Steps Transferee;
	

	@Before
	public void setup()
	{
		super.setup();
		Transferee.GetConfig(exec_driver);
		
		transf_info = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		transf_info.permision_CreateDoc = "Deny";
		
		transf_info.permision_EditDoc = "Allow";
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
		Admin.Log_Out_From_Portal();
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Go_to_Transferees_Page();
		
	}
	
	
	

	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_121: Verify Transferee's Deny Create Activity Permission Work Correctly")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_121_Verify_Transferee_Deny_Create_Activity_Permission_Work() {
      
		Transferee.ActivityManage.Verify_User_Cant_Create_Activity();
		
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_122: Verify Transferee's Deny Create Document Permission Work Correctly")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_122_Verify_Transferee_Deny_Create_Doc_Permission_Work() {
      
		Transferee.DocumentManage.Verify_User_Cant_Create_Document();
		
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
		
    }
	
	
	@Test
	@Title("123: Verify Transferee's Deny Create Note Permission Work Correctly")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_123_Verify_Transferee_Deny_Create_Note_Permission_Work() {
		
		Transferee.NoteManage.Verify_User_Cant_Create_Note();
   		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
}
