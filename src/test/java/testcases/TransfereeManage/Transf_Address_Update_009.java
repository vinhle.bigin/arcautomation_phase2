package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;


@Category(Category_TransfManage.class)
public class Transf_Address_Update_009  extends TestBase{


	
	

public Transf_Address_Update_009(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}




public Transferee_Info transf_info;


@Before
public void setup()
{
	transf_info = new Transferee_Info();
	
	super.setup();
	
	Admin.Login_to_ARC_Portal_as_admin();
	
	Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
	
	
}


	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_009_Verify Transferee - Address can be editted successfully")
	public void TC_009_Verify_Transferees_Address_can_be_editted() {
      
		Admin.TransfManage.Update_transfree_Address_Info(transf_info);
		
		Admin.TransfManage.Verify_transfee_Addressinfo_is_correct(transf_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	


}
