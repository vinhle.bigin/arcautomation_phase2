package testcases.TransfereeManage;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Note_Info;
import businessObjects.Print_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmartTask;


@Category(Catagory_SmartTask.class)
public class Transf_PrintManage_208_209_210_211 extends TestBase{
	
	 public Transf_PrintManage_208_209_210_211(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transferee;
	 Print_Info print_info;
	 Note_Info note_Info;
	 
	
	@Before
	public void setup()
	{
		super.setup();
		
		transferee = new Transferee_Info();
		
		print_info = new Print_Info();
		
		note_Info = new Note_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee, true);
	}
	
	@Test
	@Title("TC 208: Verify the new note is added after printing file PDF and current search")
	public void TC_208_Verify_The_New_Note_Is_Added_After_Printing_File_PDF_And_Current_Search() {
		
		note_Info.Note_For_Printing(print_info.Template);
		
		print_info.PDF = true;
		
		Admin.TransfManage.Print_Via_ShortCut_with_Current_Search(print_info);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC 209: Verify the new note is added after printing file PDF and some selected transf")
	public void TC_209_Verify_The_New_Note_Is_Added_After_Printing_File_PDF_And_some_selected_transf() {
	
		note_Info.Note_For_Printing(print_info.Template);
		
		print_info.PDF = true;
		
		Transferee_Info transferee2 = new Transferee_Info();
		
		List<Transferee_Info> transf_list = new ArrayList<Transferee_Info>();
		
		transf_list.add(transferee);
		
		transf_list.add(transferee2);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee2, true);
		
		Admin.TransfManage.Print_Via_ShortCut_with_Some_Transf(print_info, transf_list);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Search_for_Transferee_With_Name(transferee);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transferee);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC 210: Verify the new note is added after printing file Word and current search")
	public void TC_210_Verify_The_New_Note_Is_Added_After_Printing_File_Word_And_Current_Search() {
		
		note_Info.Note_For_Printing(print_info.Template);
		
		print_info.Word = true;
		
		Admin.TransfManage.Print_Via_ShortCut_with_Current_Search(print_info);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
	
	@Test
	@Title("TC 211: Verify the new note is added after printing file Word and some selected transf")
	public void TC_211_Verify_The_New_Note_Is_Added_After_Printing_File_Word_And_Some_Selected_Transf() {
		
		note_Info.Note_For_Printing(print_info.Template);
		
		print_info.Word = true;
		
		Transferee_Info transferee2 = new Transferee_Info();
		
		List<Transferee_Info> transf_list = new ArrayList<Transferee_Info>();
		
		transf_list.add(transferee);
		
		transf_list.add(transferee2);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transferee2, true);
		
		Admin.TransfManage.Print_Via_ShortCut_with_Some_Transf(print_info, transf_list);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Admin.Go_to_Transferees_Page();
		
		Admin.TransfManage.Search_for_Transferee_With_Name(transferee);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	}
		
}
