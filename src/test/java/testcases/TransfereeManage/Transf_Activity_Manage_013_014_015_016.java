package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Parallel_1;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;
import testcases.Category_TransfManage;

@Category(Category_TransfManage.class)
public class Transf_Activity_Manage_013_014_015_016 extends TestBase {

	public Transf_Activity_Manage_013_014_015_016(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	public Transferee_Info transf_info;

	@Before
	public void setup() {
		super.setup();

		transf_info = new Transferee_Info();

		Admin.Login_to_ARC_Portal_as_admin();

		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info, true);

	}

	@Category({ Catagory_SmokeTest.class, Catagory_Parallel_1.class, Category_E2E.class })

	@Test
	@Title("TC_013_New Follow Activity can be added for transferee successfully")
	public void TC_013_New_Follow_Activity_can_be_added_for_transferee() {

		Activity_Info activity = new Activity_Info();

		activity.Type = "Follow Up";

		activity.Assignee = transf_info.FullName;

		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);

		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);

		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);

		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

	@Test
	@Title("TC_014_New Task Activity can be added for transferee successfully")
	public void TC_014_New_Task_Activity_can_be_added_for_transferee() {

		Activity_Info activity = new Activity_Info();

		activity.Assignee = transf_info.FullName;

		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);

		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);

		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);

		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_015_Activity can be updated for transferee successfully")
	public void TC_015_Activity_can_be_updated_for_transferee() {

		Activity_Info activity = new Activity_Info();

		Activity_Info activity_2 = new Activity_Info();

		activity_2.Type = "Follow Up";

		activity_2.Assignee = transf_info.FullName;

		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);

		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity_2);

		Admin.ActivityManage.Update_Activity(activity.Title, activity_2);

		Admin.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity_2);

		Admin.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity_2);

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

	@Test
	@Title("TC_016_The closed Activity of transferee cannot be editted")
	public void TC_016_The_closed_Activity_Of_transferee_Cannot_Be_Editted() {
		Activity_Info activity = new Activity_Info();

		Admin.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);

		// Close the Activity
		activity.Closed = true;

		Admin.ActivityManage.Update_Activity(activity.Title, activity);

		// Verify Step

		Admin.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);

		Admin.ActivityManage.Verify_Activity_Details_Can_NOT_Be_Editable(activity, "- On Closed Activiy");

		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

}
