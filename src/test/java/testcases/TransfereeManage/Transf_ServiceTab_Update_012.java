package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_Services;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Category_Services.class})
public class Transf_ServiceTab_Update_012 extends TestBase{


public Transf_ServiceTab_Update_012(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

public Transferee_Info transf_info;



@Before
public void before()
{
	super.setup();
	
	transf_info = new Transferee_Info();
		
	Admin.Login_to_ARC_Portal_as_admin();
	
	Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
	
	
}

	
	
	@Category(Catagory_SmokeTest.class)	
	@Test
	@Title("TC_012_Verify Transferee - Service Tab - New Service can be turned on")
	public void TC_012_Verify_Transferees_New_Service_can_be_turned_on() {
      
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		Admin.ServiceManage.Verify_transfee_ServiceTab_Is_Correct_ViewMode(transf_info.ServiceList);
		
		Admin.ServiceManage.Verify_Transfee_Service_List_shown_correct_on_Domestic_Tab(transf_info.ServiceList);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_013_Verify Transferee - Service Tab - Service can be turned off")
	public void TC_013_Verify_Transferee_ServiceTab_Service_can_be_turned_Off() {
      
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
		
		String remove_service_type = transf_info.ServiceList.get(0).Type;
		transf_info.ServiceList.remove(0);
		
		//Turn off the service option
		Admin.ServiceManage.Turn_Off_transfree_Service_On_Service_Tab(remove_service_type);
		
		Admin.ServiceManage.Verify_transfee_ServiceTab_Is_Correct_ViewMode(transf_info.ServiceList);
		
		Admin.ServiceManage.Verify_Transfee_Service_List_shown_correct_on_Domestic_Tab(transf_info.ServiceList);
		
		Admin.ServiceManage.Verify_Unselected_Service_Not_shown_on_Domestic_Tab(remove_service_type);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
    }
	


}
