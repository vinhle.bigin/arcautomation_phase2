package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Notes;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Notes.class})
public class Transf_Create_New_Note_In_TopMenu_TC_200  extends TestBase{
	
	public Transf_Create_New_Note_In_TopMenu_TC_200(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}





	public Transferee_Info transf_info;
	public Note_Info note_Info;

	@Before
	public void setup()
	{
		transf_info = new Transferee_Info();
		
		note_Info = new Note_Info();
		
		super.setup();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		
	}

	
		
		
		
		@Test
		@Title("TC_200_New_Transf_Create_New_Note_On_Top_Transferee_Detail - New note can be add successfully")
		public void TC_200_Verify_Note_Can_Be_Created_On_TransfereeDetails_TopShortcut() {
	      
			Admin.NoteManage.Create_Note_via_Top_Note_Shortcut(note_Info);

			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
			
			Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	    }
}
