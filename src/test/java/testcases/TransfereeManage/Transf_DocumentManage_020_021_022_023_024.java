package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Document_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Documents;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Documents.class})
public class Transf_DocumentManage_020_021_022_023_024  extends TestBase{

	public Transf_DocumentManage_020_021_022_023_024(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
	
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
	
		
	}

	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_020_Verify New Document Can Be Created for Transferee")
	public void TC_020_Verify_New_Document_Can_Be_Created_For_Transferee() 
	{
		Document_Info document = new Document_Info();
		new FileManage().CreateWordFile_func(document.file_name, document.description);
		
	FileManage file_mnage  = new FileManage();
	
	file_mnage.CreateWordFile_func(document.file_name, document.description);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Admin.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
	
	}
	
	
	@Test
	@Title("TC_021_Verify Transferee Document Can Be Updated")
	public void TC_021_Verify_Transferee_Document_Can_Be_Updated() 
	{
		Document_Info document = new Document_Info();
		
		new FileManage().CreateWordFile_func(document.file_name, document.description);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
				
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		//Update document
		document.ReGenerate_Info();
		
		Admin.DocumentManage.Update_Existing_Document(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_022_Verify Transferee Document Can Be Removed")
	public void TC_022_Verify_Transferee_Document_Can_Be_Removed() 
	{
		Document_Info document = new Document_Info();
		
		new FileManage().CreateWordFile_func(document.file_name, document.description);
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
				
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		//Remove document
	
		Admin.DocumentManage.Remove_Document(document.file_name);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_NOT_Shown_On_List(document);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC_023_Verify Transferee Document file Can Be Downloaded")
	public void TC_023_Verify_Document_Can_Be_Downloaded() 
	{
		//==TEST DATA
		//String destination_file_name = "DownloadFile_doc.docx";
		
		String Expect_content = "Document's Testing Content";
		
		Document_Info document = new Document_Info();
		
		new FileManage().CreateWordFile_func(document.file_name, Expect_content);
		
		//=======Steps:
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info, true);
		
		//Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		//VErify
		if(TestConfigs.glb_TCStatus!=false)
		{
			Admin.DocumentManage.Download_Document(document.file_name, "");
			
			Admin.DocumentManage.Verify_Downloaded_File_Content_Is_Correct(document.file_name,Expect_content);
			
			//Post-condition-Delete the downloaded file:
			new FileManage().Delete_Downloaded_File_func(document.file_name);
		
		}
		
		
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	@Test
	@Title("TC_024_Verify Transferee Document Can Be Search For With Title")
	public void TC_024_Verify_Document_Can_Be_Search_For_With_Title() 
	{
		String Expect_content = "Document's Testing Content";
		
		Document_Info document = new Document_Info();
		
		Document_Info document_2 = new Document_Info();
		document_2.file_name = "Testing2doc.docx";
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		for(int i =0;i<2;i++)
		{
			new FileManage().CreateWordFile_func(document.file_name, Expect_content);
			
			Admin.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		}
		
		Admin.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Admin.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Admin.DocumentManage.Verify_Document_Info_NOT_Shown_On_List(document_2);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	
	}
	
	
	

	
	

}
