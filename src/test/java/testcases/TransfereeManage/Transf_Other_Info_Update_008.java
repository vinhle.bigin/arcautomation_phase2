package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class})
public class Transf_Other_Info_Update_008  extends TestBase{

	

	public Transf_Other_Info_Update_008(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	public Transferee_Info transf;
	

	@Before
	public void setup()
	{
		super.setup();
		
		transf = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		
	}

	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_008: Verify Transferee - Other info can be editted successfully")
	
	public void TC_008_Verify_Transferee_Other_info_can_be_editted() {
      
		Admin.TransfManage.Update_transfree_Other_Info(transf.Other_info);
		
		Admin.TransfManage.Verify_transfee_OtherInfo_is_correct(transf);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
    }
	

	
	

}
