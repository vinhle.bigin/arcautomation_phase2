package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Note_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import stepsDefiniton.VendorUser_Steps;
import testcases.Catagory_Notes;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Notes.class})
public class Transf_Note_Display_VendorPortal_TC_207  extends TestBase{

	
	public Transf_Note_Display_VendorPortal_TC_207(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	public Transferee_Info transf_info;
	
	public Note_Info note_Info;
	
	public Contact_Info contact_info;
	
	public Vendor_Info vendor_info;

	
	
	@Steps
	public TransfUser_Steps Transferee;
	
	@Steps
	public VendorUser_Steps Vendor;
	
	

	@Before
	public void setup()
	{
		super.setup();
		
		
		
		transf_info = new Transferee_Info();
		
		note_Info = new Note_Info();
				
		vendor_info = new Vendor_Info();
				
		Admin.Login_to_ARC_Portal_as_admin();
			
	}
	

	
	@Test
	@Title("TC_207_Vendor_Can_View_New_Note_In_VendorPortal")
	public void TC_207_Vendor_Can_View_New_Note_In_VendorPortal() {
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor_info);	
		
		Contact_Info ctact_info = vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		ctact_info.AddrInfo.City = transf_info.OriResAddr_info.City;
		
		transf_info.HHG_service.Type = "Household goods";
		
		Service_Order_Info order = new Service_Order_Info();
		
		order.InIt_International_Order_Info();
		
		order.InIt_VendorSelected(ctact_info.VendorName);
		
		new Service_Order_Info();
		
		Admin.ContactManage.Create_New_Contact(ctact_info,true);
                   
		Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
        
		Admin.ContactManage.Update_Contact_Profile_Account(ctact_info);
 
    	Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info, true);
    	
    	Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf_info);
    	
    	Admin.TransfManage.Update_transfree_Address_Info(transf_info);
    	
    	Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf_info.HHG_service.Type, ctact_info);
    	
    	Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,false);
		
		Vendor.Login_to_VendorPortal(ctact_info.username, ctact_info.password);

		Vendor.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Vendor.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);

    }
	
}