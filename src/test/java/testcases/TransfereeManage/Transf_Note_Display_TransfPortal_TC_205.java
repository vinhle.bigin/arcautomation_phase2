package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Catagory_Notes;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Notes.class})
public class Transf_Note_Display_TransfPortal_TC_205  extends TestBase{
	
	
	
	public Transf_Note_Display_TransfPortal_TC_205(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	public Transferee_Info transf_info;
	
	public Note_Info note_Info;

	
	
	@Steps
	TransfUser_Steps Transferee;

	



	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		note_Info = new Note_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
	}


	
	@Test
	@Title("TC_205_TransfereeUser_Can_View_New_Note")
	public void TC_205_TransfereeUser_Can_View_New_Note() {
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
      
		Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,false);
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		Transferee.Go_to_Note_Tab();
		
		Transferee.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Transferee.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
}
