package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Notes;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Notes.class})
public class Transf_Delete_New_Note_TC_203  extends TestBase{
	
	public Transf_Delete_New_Note_TC_203(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	public Transferee_Info transf_info;
	public Note_Info note_Info;

	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		note_Info = new Note_Info();
		
		
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		
	}


	@Category({Catagory_SmokeTest.class,Category_E2E.class})
	@Test
	@Title("TC_203_New Note Can Be Delete successfully")
	public void TC_203_Verify_New_Note_Can_Be_Deleted_From_Transferee() {
				
		Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
		
		//Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info);
		
		Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
		
		Admin.NoteManage.Delete_Note_func(note_Info);
		
		Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
		
		
		Admin.NoteManage.Verify_Data_NOT_Shown_On_Table(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);

    }

}
