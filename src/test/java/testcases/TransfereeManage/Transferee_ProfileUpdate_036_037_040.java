package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Activity_Info;
import businessObjects.Document_Info;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;



@Category({Category_TransfManage.class})
public class Transferee_ProfileUpdate_036_037_040  extends TestBase {
	
	
	
	public Transferee_ProfileUpdate_036_037_040(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Transferee_Info transf_info;
	
	@Steps
	TransfUser_Steps Transferee;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		Transferee.GetConfig(exec_driver);
		
		transf_info = new Transferee_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		transf_info.permision_CreateDoc = "Allow";
		
		transf_info.permision_EditDoc = "Allow";
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Go_to_Transferees_Page();
		
	}
	
	
	

	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_036: Verify Transferee's Allow Create Activity Permission Work Correctly")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_036_Verify_Transferee_Allow_Create_Activity_Permission_Work() {
      
		Activity_Info activity = new Activity_Info();
		activity.Assignee = transf_info.FullName;
		
		Transferee.ActivityManage.Create_New_Activity_From_Activity_Tab(activity);
		
		Transferee.ActivityManage.Search_For_Activity_On_Activity_Tab(activity);
		
		Transferee.ActivityManage.Verify_Activity_Is_Shown_On_Table(activity);
		
		Transferee.ActivityManage.Verify_Activity_Detasil_Is_Correct_ViewMode(activity);
		
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_037: Verify Transferee's Allow Create Document Permission Work Correctly")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_037_Verify_Transferee_Allow_Create_Doc_Permission_Work() {
      
		Document_Info document = new Document_Info();
		
		
	
		
		//CREATE DOC AND VERIFY
		Transferee.DocumentManage.Create_New_Document_from_Doc_Tab(document);
		
		Transferee.DocumentManage.Search_For_Document_With_Title(document.file_name);
		
		Transferee.DocumentManage.Verify_Document_Info_Is_Shown_On_List(document);
		
		Transferee.DocumentManage.Verify_Modal_Document_Info_Is_Correct(document);
		
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	
	@Test
	@Title("TC_040: Verify Transferee's Allow Create Note Permission Work Correctly")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_040_Verify_Transferee_Allow_Create_Note_Permission_Work() {
      
		Note_Info note_Info = new Note_Info();
				
		//CREATE NOTE AND VERIFY
		Transferee.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
		
		Transferee.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
		
		Transferee.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
			
		Transferee.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
}
