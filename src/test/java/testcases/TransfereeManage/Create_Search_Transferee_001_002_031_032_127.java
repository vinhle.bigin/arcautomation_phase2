package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Issues;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Parallel_1;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;



@Category(Category_TransfManage.class)
@Issues({"#AA-45"})
public class Create_Search_Transferee_001_002_031_032_127  extends TestBase{
	
	public Create_Search_Transferee_001_002_031_032_127(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	TestConfigs test_cfig;
	Transferee_Info transf_info;
	
	
	@Before
	public void setup()
	{
		
		super.setup();
		
		transf_info = new Transferee_Info();
		
	}
	
	//Yen
	@Category({Catagory_SmokeTest.class,Catagory_Parallel_1.class})
	@Test
	@Title("TC_001: Verify New Transferee is created successfully with Active")
	@Issues({"#AA-45"})
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_001_Verify_New_Transferee_is_created_successfully_with_Active() {
      
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		Admin.TransfManage.Search_for_Transferee_With_Name(transf_info);
		
		Admin.TransfManage.Verify_transfee_is_shown_on_Tranferee_List(transf_info);
		
		Admin.TransfManage.Verify_transfee_GeneralInfo_is_correct(true,transf_info);
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	

	@Test
	@Title("TC_031: Verify New Transferee is created successfully via Shortcut icon on the Top menu")
	
	@Issues({"#AA-45"})
	@Category(Catagory_Parallel_1.class)
	public void TC_031_Verify_New_Transferee_is_created_via_Top_Shortcut() {
      
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_via_Top_Shortcut(transf_info,true);
			
		Admin.TransfManage.Search_for_Transferee_With_Name(transf_info);
		
		Admin.TransfManage.Verify_transfee_is_shown_on_Tranferee_List(transf_info);
		
		Admin.TransfManage.Verify_transfee_GeneralInfo_is_correct(true,transf_info);
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	  }
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_032: Verify Account Is Auto-Generated For New Transfere with Active Status")
	@Issues({"#AA-45"})
	public void TC_032_Verify_Account_Is_Auto_Generated_For_New_Transfere_with_Active_Status() {
      
		Admin.Login_to_ARC_Portal_as_admin();
				
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,false);
				
		Admin.TransfManage.Verify_Modal_Account_Info_Correct_func(transf_info.FullName);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_002: Transferee can searched successfully with Name")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_002_Transferee_can_searched_successfully_with_Name() {
      
		Admin.Login_to_ARC_Portal_as_admin();
				
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
				
		Admin.TransfManage.Search_for_Transferee_With_Name(transf_info);
		
		Admin.TransfManage.Verify_transfee_is_shown_on_Tranferee_List(transf_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	
	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_127: Verify New Transferee is created successfully with Prospect Status")
	
	public void TC_127_Verify_New_Transferee_is_created_successfully_with_Prospect_Status() {
      
		Admin.Login_to_ARC_Portal_as_admin();
		
		transf_info.Status = "Prospect";
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,false);
				
		//Verify Account modal is not displayed
		Admin.TransfManage.Verify_Modal_Account_Info_NOT_Displayed_Prospect_Transferee();
		
		Admin.TransfManage.Search_for_Transferee_With_Name(transf_info);
		
		Admin.TransfManage.Verify_transfee_is_shown_on_Tranferee_List(transf_info);
		
		Admin.TransfManage.Verify_transfee_GeneralInfo_is_correct(true,transf_info);
				
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
    }
	
	
	
	

}
