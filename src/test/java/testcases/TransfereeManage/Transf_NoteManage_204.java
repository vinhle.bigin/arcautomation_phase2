package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_Notes;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;



@Category({Category_TransfManage.class,Catagory_Notes.class})
public class Transf_NoteManage_204  extends TestBase{
	
	public Transf_NoteManage_204(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}




	public Transferee_Info transf_info;
	public Note_Info note_Info;



	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		note_Info = new Note_Info();
		
		note_Info.Assignee = transf_info.FullName;
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
				
	}


		
		
		@Category(Catagory_SmokeTest.class)
		@Test
		@Title("TC_204_Note Can Be Created Via Transferee List Page - Top Note Shortcut")
		public void TC_204_Verify_Note_Can_Be_Created_From_Top_Menu_Tranf_List() {
	      
			Admin.Go_to_Transferees_Page();
			
			Admin.NoteManage.Create_Note_via_Top_Note_Shortcut(note_Info);
			
			Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
			
			Admin.NoteManage.Search_Existing_Note_On_Note_Tab(note_Info.Title);
			
			Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true); 
				
			Admin.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
			
	    }
		


}