package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Catagory_Parallel_1;
import testcases.Catagory_SmokeTest;
import testcases.Category_E2E;
import testcases.Category_TransfManage;



@Category({Category_TransfManage.class})
public class Transferee_ProfileUpdate_033_034_035  extends TestBase{
	
	
	

	public Transferee_ProfileUpdate_033_034_035(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	Transferee_Info transf_info;
	
	
	@Steps
	TransfUser_Steps Transferee;
	

	
	@Before
	public void setup()
	{
		super.setup();
		
		Transferee.GetConfig(exec_driver);
		
		
		transf_info = new Transferee_Info();
	}
	
	
	

	@Category({Catagory_SmokeTest.class,Catagory_Parallel_1.class})
	@Test
	@Title("TC_033: Verify Transferee Account Tab can be updated")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_033_Verify_Transferee_Account_Tab_can_be_updated() {
      
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,false);
		
		//Verify the Default value:
		Transferee_Info tranf_copied = transf_info;
		
		tranf_copied = Admin.TransfManage.Get_Transferee_Login_Account_Info(tranf_copied);
		
		Admin.Close_Account_Information_modal();
		
		Admin.TransfManage.Verify_Transferee_Account_Info_Correct_ViewMode(tranf_copied," - Before Update");
		
		//Update info and verify
		transf_info.acc_Active_Status = "Inactive";
		
		transf_info.permision_CreateDoc = "Allow";
		
		transf_info.permision_EditDoc = "Allow";
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
		Admin.TransfManage.Verify_Transferee_Account_Info_Correct_ViewMode(transf_info," - After Update");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
    }
	
	@Test
	@Title("TC_034: Verify Transferee Account Can Be Locked")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_034_Verify_Transferee_Account_Can_Be_Locked()
	{
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		//Update info and verify
		transf_info.acc_Active_Status = "Inactive";
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Verify_Account_is_Locked("After InActive on ARC Portal");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}
	
	@Category(Category_E2E.class)
	@Test
	@Title("TC_035: Verify Transferee Account Can Be UnLocked")
	//@Issues({"#ARC-449", "#ARC-450"})
	public void TC_035_Verify_Transferee_Account_Can_Be_UnLocked()
	{
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
		
		//INACTIVE ACCOUNT
		transf_info.acc_Active_Status = "Inactive";
		
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
		Admin.Log_Out_From_Portal();
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Verify_Account_is_Locked("After InActive on ARC Portal");
				
		//ACTIVE ACCOUNT AGAIN
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		transf_info.acc_Active_Status = "Active";
		Admin.TransfManage.Update_Transfree_Account_Tab(transf_info);
		
		Transferee.Login_to_TransfereePortal(transf_info.username, transf_info.password);
		
		Transferee.Verify_Account_Can_Login("After Active Account again on ARC Portal");
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
		
	}

	
	
	
	

}
