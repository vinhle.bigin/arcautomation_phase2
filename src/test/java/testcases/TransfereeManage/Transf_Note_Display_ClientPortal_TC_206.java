package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import businessObjects.Note_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.ClientUser_Steps;
import testcases.Catagory_Notes;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Notes.class})
public class Transf_Note_Display_ClientPortal_TC_206  extends TestBase{
	
	
	
	public Transf_Note_Display_ClientPortal_TC_206(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}



	public Transferee_Info transf_info;
	
	public Note_Info note_Info;
	
	public Client_Info client_info;
	
	public Contact_Info contact_info;

	
@Steps
ClientUser_Steps Client;


	@Before
	public void setup()
	{
		super.setup();
		
		transf_info = new Transferee_Info();
		
		note_Info = new Note_Info();
		
		client_info = new Client_Info();
			
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.ClientManage.Create_New_Client(client_info);
			
	}

	
	
	@Test
	@Title("TC_206_Client_Can_View_New_Note_In_ClientPortal")
	public void TC_206_Client_Can_View_New_Note_In_ClientPortal() {
		
		transf_info.Client =  client_info;
		
		Contact_Info ctact_info = client_info.Contacts.get(0);
		
		Admin.ContactManage.Create_New_Contact(ctact_info, true);
		
		Admin.ContactManage.Update_Contact_Profile_Account(ctact_info);
		
		Admin.ContactManage.Verify_Contact_Is_Shown_on_Table(ctact_info);
 
    	Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info, true);
    	
    	Admin.NoteManage.Create_New_Note_From_Note_Tab(note_Info);
		
		Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,false);
		
		Client.Login_to_ClientPortal(ctact_info.username, ctact_info.password );
		
		Client.TransfManage.Go_To_Transfeee_Details(transf_info);
		
		Client.NoteManage.Verify_Note_Is_Shown_On_Table(note_Info,true);
		
		Client.NoteManage.Verify_Modal_Note_Details_Correct(note_Info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);

    }

}
