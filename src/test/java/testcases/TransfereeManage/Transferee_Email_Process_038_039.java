package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Mail_Info;
import businessObjects.Note_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.TransfUser_Steps;
import testcases.Catagory_SmokeTest;
import testcases.Catagory_Template;
import testcases.Category_E2E;
import testcases.Category_TransfManage;


@Category({Category_TransfManage.class,Catagory_Template.class})
public class Transferee_Email_Process_038_039  extends TestBase{
		
	public Transferee_Email_Process_038_039(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

	public Transferee_Info transf;

	public Template_Info newtpl;
	
	public Mail_Info data_mail;

	
	@Steps
	public TransfUser_Steps transf_user;


	@Before
	public void setup()
	{
		super.setup();
		
		transf = new Transferee_Info();
		
		newtpl = new Template_Info();
		
		transf.MailList.get(0).EmailAddr =  glb_RefData.glb_TestMailAccount;
		
		transf.MailList.get(0).Map_Template_Info_func(newtpl, true);

		data_mail = transf.MailList.get(0);
			
		Admin.Login_to_ARC_Portal_as_admin();
		
		//Create new template
		Admin.TemplateManage.Create_New_Template(newtpl);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
	}

		@Category({Catagory_SmokeTest.class,Category_E2E.class})
		@Test
		@Title("TC_039_Verify The sent Email is saved as a note after user receive email")
		public void TC_039_Verify_The_Sent_Email_Is_Saved_As_A_Note_After_User_Receive_Email()
		{
				
			Note_Info note = data_mail.Convert_To_Note_Info();
			
			//Send email to transfere
			Admin.TransfManage.Send_Email_to_transfree(data_mail,false);
			
			//Verify Email is sent successfully
			transf_user.Verify_User_Receives_Email_Gmail(data_mail, "","Equals");
			
			if(TestConfigs.glb_TCStatus!=true)
			{
				Admin.Go_to_Note_Tab();
				
				Admin.NoteManage.Verify_Note_Is_Shown_On_Table(note,true);
					
				Admin.NoteManage.Verify_Modal_Note_Details_Correct(note);
			}
			
			Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		}

}
