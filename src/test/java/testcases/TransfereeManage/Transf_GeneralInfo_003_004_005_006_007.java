package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;


@Category(Category_TransfManage.class)
public class Transf_GeneralInfo_003_004_005_006_007  extends TestBase{

	

	public Transf_GeneralInfo_003_004_005_006_007(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	public Transferee_Info transf;
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		transf = new Transferee_Info();
		
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		
	}
	

	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC_003: Verify Transferee - General Tab - Transfere Information section can be editted")
	
	public void TC_003_Verify_Transf_General_Transf_info_section_can_be_editted() {
      
		transf.ReGenerate_GeneralInfo_func();
		
		Admin.TransfManage.Update_transfree_General_Info(transf);
		
		Admin.TransfManage.Verify_transfee_GeneralInfo_is_correct(true,transf);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
		
    }
	
	
	
	
	

}
