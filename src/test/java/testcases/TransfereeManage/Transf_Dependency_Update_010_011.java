package testcases.TransfereeManage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Dependent_Info;
import businessObjects.Transferee_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Title;
import testcases.Category_TransfManage;


@Category(Category_TransfManage.class)
public class Transf_Dependency_Update_010_011  extends TestBase{


public Transf_Dependency_Update_010_011(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}

public Transferee_Info transf_info;


@Before
public void setup()

{
	super.setup();
	
	transf_info = new Transferee_Info();
	
	Admin.Login_to_ARC_Portal_as_admin();
	
	Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf_info,true);
	
	
}


	
	@Test
	@Title("TC_010_Verify Transferee - Dependency can be added successfully")
	public void TC_010_Verify_Transferees_Dependency_can_be_added() {
      
		Admin.TransfManage.Create_transfree_Dependency_Info(transf_info);
		
		Admin.TransfManage.Verify_transfee_Dependency_Info_Is_Shown_On_List(transf_info.Dependent_list.get(0));
		
		Admin.TransfManage.Verify_transfee_Dependency_Info_is_correct(transf_info.Dependent_list.get(0));
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	
	@Test
	@Title("TC_011_Verify Transferee - Dependency can be updated successfully")
	public void TC_011_Verify_Transferees_Dependency_can_be_updated() {
      
		Dependent_Info new_depInfo = new Dependent_Info();
		
		String old_name = transf_info.Dependent_list.get(0).firstname;
		
		Admin.TransfManage.Create_transfree_Dependency_Info(transf_info);
		
		Admin.TransfManage.Update_transfree_Dependency_Info(old_name, new_depInfo);
		
		Admin.TransfManage.Verify_transfee_Dependency_Info_Is_Shown_On_List(new_depInfo);
		
		Admin.TransfManage.Verify_transfee_Dependency_Info_is_correct(new_depInfo);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
    }
	


}
