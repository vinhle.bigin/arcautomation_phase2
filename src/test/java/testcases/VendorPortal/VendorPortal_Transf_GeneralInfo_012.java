package testcases.VendorPortal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Transferee_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.VendorUser_Steps;
import testcases.Catagory_SmokeTest;
import testcases.Category_TransfManage;
import testcases.Category_VendorManage;


@Category({Category_VendorManage.class,Category_TransfManage.class})
public class VendorPortal_Transf_GeneralInfo_012  extends TestBase{

	
	
	public VendorPortal_Transf_GeneralInfo_012(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Vendor_Info vendor;
	
	Transferee_Info transf;
	
	Contact_Info ctact;
	
	@Steps
	VendorUser_Steps Vendor;
	
	
	@Before
	public void setup()
	{
		super.setup();
		
		Vendor.GetConfig(exec_driver);
		
		transf = new Transferee_Info();
		
		vendor = new Vendor_Info();
		vendor.SearchOpt_IsUSA = true;
		
		ctact = new Contact_Info();
		
		ctact = vendor.Contacts.get(0);
		
		ctact.ServiceType = "Van Lines";
		
		ctact.AddrInfo.City = transf.OriResAddr_info.City;
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor);
		
		Admin.VendorManage.Update_Vendor_Search_Option_Tab(vendor);
		
		Admin.ContactManage.Create_New_Contact(ctact,true);
		
		Vendor.ContactManage.Update_Contact_Profile_Account(ctact);
		
		Admin.TransfManage.Create_new_transfree_from_Transferees_List(transf,true);
		
		Admin.TransfManage.Update_transfree_Address_Info(transf);	
		
		Admin.ServiceManage.Update_transfree_ServiceTab_Info(transf);
		
		Admin.ServiceManage.Assign_Vendor_Contact_To_ServiceDetails(transf.HHG_service.Type, ctact);
		
		Admin.Log_Out_From_Portal();
		
		Vendor.Login_to_VendorPortal(ctact.username, ctact.password);
		
	}

	
	@Category(Catagory_SmokeTest.class)
	@Test
	@Title("TC 012 Vendor Portal - Update User Profile Section")
	public void TC_012_Vendor_Portal_Update_User_Profile_Section() 
	{
		Vendor.Go_to_Transferees_Page();
		
		Admin.TransfManage.Search_for_Transferee_With_Firstname_Client_Portal(transf);
		
		Vendor.TransfManage.Verify_transfee_GeneralInfo_is_correct(false,transf);
		//test
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

}
