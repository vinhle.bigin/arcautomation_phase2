package testcases.VendorPortal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.remote.DesiredCapabilities;

import baseClasses.TestBase;
import businessObjects.Contact_Info;
import businessObjects.Vendor_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import stepsDefiniton.VendorUser_Steps;
import testcases.Category_VendorManage;


@Category({Category_VendorManage.class})
public class VendorPortal_UserProfile_Manage_009  extends TestBase{

	
	
	public VendorPortal_UserProfile_Manage_009(DesiredCapabilities glb_capacities) {
		super(glb_capacities);
		// TODO Auto-generated constructor stub
	}


	Vendor_Info vendor_info;
	
	@Steps
	VendorUser_Steps Vendor;
	
	@Before
	public void setup()
	{
		super.setup();
		
		Vendor.GetConfig(exec_driver);
		
		vendor_info = new Vendor_Info();
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Create_New_Vendor_Via_Top_Shortcut(vendor_info);	
		
		Contact_Info ctact_info = vendor_info.Contacts.get(0);
		
		ctact_info.ServiceType = "Van Lines";
		
		//Create Vendor Contact	
		Vendor.ContactManage.Create_New_Contact(ctact_info,true);
		
		Vendor.ContactManage.Update_Contact_Profile_Account(ctact_info);
		
		Admin.Log_Out_From_Portal();
		
		Vendor.Login_to_VendorPortal(ctact_info.username, ctact_info.password);
		
	}
	
	
	@Test
	@Title("TC 009 Vendor Portal - Update User Profile Section")
	public void TC_009_Vendor_Portal_Update_User_Profile_Section() 
	{
		Vendor.Go_to_Profile_Page();
		
//		vendor_info.ReGenenrateInfo();
//		
//		Vendor.TransfManage.Update_User_Profile_Section(vendor_info);	
//		
//		Vendor.TransfManage.Update_PhoneEmail_Section(vendor_info);
//		
//		Vendor.TransfManage.Verify_User_Profile(vendor_info);
//		
//		Vendor.TransfManage.Verify_transfee_GeneralInfo_is_correct(vendor_info);
//		
//		Vendor.TransfManage.Verify_Fullname_in_Sidebar(vendor_info);
//		
//		Vendor.TransfManage.Verify_Fullname_in_TopMenu(vendor_info);
		
		Admin.Login_to_ARC_Portal_as_admin();
		
		Admin.VendorManage.Search_For_Existing_VendorName(vendor_info);
		
		Vendor.VendorManage.Verify_Vendor_Is_Displayed_On_Vendors_List(vendor_info);
		
		Assert.assertTrue(TestConfigs.glb_TCFailedMessage, TestConfigs.glb_TCStatus);
	}

}
