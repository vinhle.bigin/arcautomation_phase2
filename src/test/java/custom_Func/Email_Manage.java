package custom_Func;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;


import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;

import org.junit.Assert;

import businessObjects.Mail_Info;
import configuration.TestConfigs;
import dataDriven.glb_RefData;

public class Email_Manage {

	public List<Mail_Info> Maillist;
	private String host;
	private String username;
	private String password;
	private String port;
	private String pop3_type;
	private FileManage mail_attachment_temp;
	public Email_Manage() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Email_Manage(String mail_type) {

		Maillist = new ArrayList<>();
		
		switch(mail_type)
		{
		case "Outlook" :
			host = "mail.bigin.vn";// change accordingly
			username = "vinh.le@bigin.vn";// change accordingly
			password = "QNqhPZ93";// change accordingly
			pop3_type = "pop3";
			break;
		

		case "Gmail" :
			host = "imap.gmail.com";// change accordingly
			username = glb_RefData.glb_TestMailAccount;// change accordingly
			password = glb_RefData.glb_TestMailPass;// change accordingly
			port = "993";
			pop3_type = "imaps";
			break;
			
		case "Yopmail":
			host = "pop3.yopmail.com";// change accordingly
			username = "vinhle@yopmail.com";// change accordingly
			password = "Password@1234";// change accordingly
			port = "995";
			pop3_type = "pop3s";

		}

		mail_attachment_temp= new FileManage();
	}


	private String OptimizeEmailContent(String input_str) {
		//input_str = input_str.replace("\n\n","\n")

		int i = input_str.indexOf("<html xmlns:");

		String a;
		if(i>0)
			a = input_str.substring(0,i);
		else
			a = input_str;
		/*
		a = a.replaceAll("</a>", "");
		a = a.replaceAll("</td>", "");
		a = a.replaceAll("</tr>", "");
		a = a.replaceAll("</tbody>", "");
		a = a.replaceAll("</table>", "");
		a = a.replaceAll("</title>", "");
		a = a.replaceAll("</style>", "");
		a = a.replaceAll("</head>", "");
		a = a.replaceAll("</body>", "");
		a = a.replaceAll("</html>", "");
		a = a.replaceAll("</div>", "");
		a = a.replaceAll("</i>", "");
		a = a.replaceAll("</span>", "");
		a = a.replaceAll("</p>", "");
		a = a.replaceAll("</h2>", "");
		a = a.replaceAll("</em>", "");
		a = a.replaceAll("</strong>", "");
		a = a.replaceAll("<i>", "");
		a = a.replaceAll("<strong>", "");
		a = a.replaceAll("<td.+", "");
		a = a.replaceAll("<td.+?>", "");
		a = a.replaceAll("<tr.+", "");
		a = a.replaceAll("<tr.+?>", "");
		a = a.replaceAll("<tbody.+", "");
		a = a.replaceAll("<table.+>", "");

		a = a.replaceAll("<style.+", "");
		a = a.replaceAll("style.+>", "");
		a = a.replaceAll("<span.+>", "");
		a = a.replaceAll("<title.+","");
		a = a.replaceAll("<head.+", "");
		a = a.replaceAll("<div.+", "");
		a = a.replaceAll("<p.+>", "");
		a = a.replaceAll("<p>", "");
		a = a.replaceAll("<br.+", "");
		a = a.replaceAll("<h2.+>", "");
		a = a.replaceAll("&nbsp;", "");

		a = a.replaceAll("<body.+", "");
		a = a.replaceAll("<html.+", "");
		a = a.replaceAll("<link.+>", "");


		a = a.replaceAll(".+word-break.+}", "");
		a = a.replaceAll("word-break.+}", "");
		a = a.replace("<!-- Start Nav -->", "");
		a = a.replaceAll("body.+", "");
		a = a.replaceAll(".+html.+", "");
		a = a.replaceAll(".+?}", "");
		a = a.replaceAll("<base.+", "");
		a = a.replaceAll("td[class].+", "");
		a = a.replaceAll("<img src=\"https://u10082166.+", "");
		a = a.replaceAll("<meta.+", "");
		a = a.replaceAll(" {2,50}", "");

		a = a.replaceAll("}\\s", "");
		a = a.replaceAll("\\s}", "");
		a = a.replaceAll("\n\\s+", "");
*/
		a = a.replaceAll("<br[^>]*>", "\n");
		a = a.replaceAll("<[^>]*>", "");
		
		System.out.println("Email Content after optimized:"+a);

		return a;

	}

	
	public void FetchEmail_func(long millis) {
		try {
		Thread.sleep(millis);			
			// create properties field
			Properties properties = new Properties();

			properties.put("mail.imap.host", this.host);
			properties.put("mail.imap.port", this.port);
			properties.put("mail.imap.starttls.enable", "true");
			properties.put("mail.imap.ssl.trust", "true");
			Session emailSession = Session.getDefaultInstance(properties);

			//create the POP3 store object and connect with the pop server
			Store  store = emailSession.getStore(pop3_type);

			System.out.println("------------\nStart to Login Email with Info:\nAccount: "+username+"\nPassword: "+password+"\n-----------");

			
			store.connect(host, username, password);


			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();

			for (int i = 0; i < messages.length; i++) {
				Mail_Info tmpmail = new Mail_Info();

				Message message = messages[i];
				tmpmail.sentDate = message.getSentDate();

				List<String> from_tmp = GetAddressFrom(message);
				tmpmail.From =  from_tmp.get(0);

				tmpmail.To_list =   GetAddressTo(message);

				tmpmail.Subject =   GetSubject(message);

				tmpmail.Content = writePart(message);

				tmpmail.Content = OptimizeEmailContent(tmpmail.Content).trim();

				System.out.println("Email Content: "+tmpmail.Content);
				
				tmpmail.download_files.add(mail_attachment_temp);
				
				System.out.println("Attachment '"+mail_attachment_temp.File_Name+"': "+mail_attachment_temp.Content);
				
				Maillist.add(tmpmail);
			}//end for
			System.out.println("Message length:"+messages.length);


		} catch (NoSuchProviderException e) {

			e.printStackTrace();

			TestConfigs.glb_TCFailedMessage+= e.getMessage();
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		} catch (MessagingException e) {
			e.printStackTrace();
			TestConfigs.glb_TCFailedMessage+= e.getMessage();
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		} catch (IOException e) {
			e.printStackTrace();
			TestConfigs.glb_TCFailedMessage+= e.getMessage();
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		} catch (Exception e) {
			e.printStackTrace();
			TestConfigs.glb_TCFailedMessage+= e.getMessage();
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}

		//System.out.println(Maillist.get(0).Content)
	}

	private String writePart(Part p) throws Exception {

		String temp_content = "";
		byte[] bArray = null;
		//check if the content is plain text
		if (p.isMimeType("text/plain")) {

			temp_content += (String) p.getContent();
		}

		//check if the content has attachment
		else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart) p.getContent();
			int count = mp.getCount();
			System.out.println("MultiPart count:"+count);
			for (int i = 0; i < count; i++)
				temp_content +=writePart(mp.getBodyPart(i));
		}
		//check if the content is a nested message
		else if (p.isMimeType("message/rfc822")) {

			temp_content +=writePart((Part) p.getContent());
		}
		//check if the content is an inline image
		else if (p.isMimeType("image/jpeg")) {

			Object o = p.getContent();

			InputStream x = (InputStream) o;
			while (((int) ((InputStream) x).available()) > 0) {
				bArray = new byte[x.available()];
				int result = (int) (((InputStream) x).read(bArray));
				if (result == -1) {
				}
				break;

			}//while
			FileOutputStream f2 = new FileOutputStream("/tmp/image.jpg");
			f2.write(bArray);
			x.close();
			f2.close();
		} // end if (p.isMimeType("image/jpeg")) 
		else if (p.getContentType().contains("image/")) {


			//===download attachment
			System.out.println("content type" + p.getContentType());
			String file_name = "image" + new Date().getTime() + ".jpg";

			this.DownloadAttachment_func(p,file_name);
		}//end if (p.getContentType().contains("image/"))
		else {
			System.out.println("Start to get content of Attachment:");
			Object o = p.getContent();
			System.out.println("Attachment File Name:"+p.getFileName());
			System.out.println("File Type:"+p.getContentType());
			System.out.println("File Content:"+p.getContent());
			if (o instanceof String) {

				temp_content +=(String) o;
			}// end if (o instanceof String) 
			else if (o instanceof InputStream) {
				//==================
				InputStream is = (InputStream) o;
				//int c;

				String file_name  = "Expected_" + p.getFileName();
				this.DownloadAttachment_func(p,file_name);
				
				//Save the content of the attachment into the FileManage
				this.mail_attachment_temp = new FileManage();
								
				this.mail_attachment_temp.File_Name = file_name;
				
				this.mail_attachment_temp.ReadFileContent_func(mail_attachment_temp.File_Name);
				
				is.close();
			}// end if (o instanceof InputStream)
			else {
				temp_content += o.toString();
			}
		}

		//System.out.println(temp_content)
		return temp_content;
	}



	/*
	 * This method would print FROM,TO and SUBJECT of the message
	 */
	
	
	
	private  List<String> GetAddressFrom(Message m) throws Exception {
		List<String> temp_addr = new ArrayList<>();
		Address[] a;

		// FROM
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++)
				temp_addr.add(a[j].toString());
		}
		return temp_addr;

	}


	private  List<String> GetAddressTo(Message m) throws Exception {

		List<String> temp_addr = new ArrayList<>();

		Address[] a;

		// TO
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++)
				temp_addr.add(a[j].toString());
		}

		return temp_addr;


	}

	private  String GetSubject(Message m) throws Exception {

		// SUBJECT
		String tmp_subj = "";
		if (m.getSubject() != null);
			tmp_subj = m.getSubject();

		return tmp_subj;

	}

	public void VerifyEmailExist(Mail_Info email, String emailtype)
	{
		//**NOTE: 
		//Value list of emailtype = "Activate" | "Welcome" | ""Reset""
		//Mail_Info email_var = email;

		int count = this.Maillist.size();

		if(count==0)
		{
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "Mail box is empty.";
			return;
		}

		Mail_Info resultmail = this.FindExistEmail(email);

		if(resultmail==null)
		{
			TestConfigs.glb_TCStatus = false;
			TestConfigs.glb_TCFailedMessage += "No "+emailtype+" email sent from Agoyu System.";
		}

		else
		{
			//System.out.println(mtemp.Content)

			if(emailtype == "Activate")
				resultmail.VerifyActivateEmail();

			else if(emailtype == "Welcome")
				resultmail.VerifyWelcomeEmail();

		}

	}



	private Mail_Info GetEmailContent(Mail_Info expect_email)
	{
		Mail_Info expect_email_var = expect_email;
				
		int count = Maillist.size();

		if(count==0)
		{
			return null;
		}
		for(int i =0;i<count;i++)
		{
			Mail_Info mtemp = Maillist.get (i);
			System.out.println("From Info:"+expect_email_var.From+" - "+mtemp.From);
			System.out.println("Subject: "+expect_email_var.Subject+"-"+mtemp.Subject);
			System.out.println("Email Content: "+expect_email_var.Content+"-"+mtemp.Content);
			System.out.println("Expected Month: "+expect_email_var.sentDate.getMonth()+"-Fetched Month: "+mtemp.sentDate.getMonth());
			System.out.println("Expected Date: "+expect_email_var.sentDate.getDate()+"-Fetched Date: "+mtemp.sentDate.getDate());
			System.out.println("Expected Hour: "+expect_email_var.sentDate.getHours()+"- Fetched Hour: "+mtemp.sentDate.getHours());
			System.out.println("Expected Minutes :"+expect_email_var.sentDate.getMinutes()+"-Fetched Minutes: "+mtemp.sentDate.getMinutes());

			if(mtemp.From.contains(expect_email_var.From)
			&& mtemp.Subject.contains(expect_email_var.Subject)
			&& mtemp.Content.contains(expect_email_var.Content)
			&&(expect_email_var.sentDate.getMonth()==mtemp.sentDate.getMonth())
			&&(expect_email_var.sentDate.getDate()==mtemp.sentDate.getDate())
			&&(expect_email_var.sentDate.getHours()==mtemp.sentDate.getHours())
			&&(Math.abs(expect_email_var.sentDate.getMinutes()-mtemp.sentDate.getMinutes())<4)
			)
			{
				expect_email_var = mtemp;
				break;
			}

		}//end for

		return expect_email_var;
	}


	public Mail_Info FindExistEmail(Mail_Info expect_email)
	{
		long mail_waitime = 30000;
		Mail_Info email_index = null;
		int count = Maillist.size();

		if(count==0)
		{
			return email_index;
		}

		email_index = GetEmailContent(expect_email);
		
		//Re-Fetch email in 3 times
		for(int i =0;i<2;i++)
		{
			if(email_index==null)
			{
				mail_waitime = mail_waitime/(i+1);
				
				FetchEmail_func(mail_waitime);
				
				email_index = GetEmailContent(expect_email);

			}	
			else
			{
				break;
			}
		}
		
	return email_index;
	}


	//==METHOD: DOWNLOAD ATTACHMENT
	public void DownloadAttachment_func(Part p,String file_name)
	{
		try {
			String full_file_path = TestConfigs.Download_full_folder_path+File.separator+file_name;
		File f = new File(full_file_path);
		DataOutputStream output;
		
		output = new DataOutputStream(	new BufferedOutputStream(new FileOutputStream(f)));
		
		com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p.getContent();
		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = test.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);

		}
		output.close();
		} catch (IOException | MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
