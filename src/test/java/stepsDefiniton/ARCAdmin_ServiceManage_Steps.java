package stepsDefiniton;

import java.util.List;

import baseClasses.StepsBase;
import businessObjects.Address_Info;
import businessObjects.AppraisalService_Info;
import businessObjects.Contact_Info;
import businessObjects.HBOService_Info;
import businessObjects.HBO_Offer_Info;
import businessObjects.HSService_Info;
import businessObjects.HomeInspectService_Info;
import businessObjects.InventManageService_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Services_Info;
import businessObjects.Transferee_Info;
import net.thucydides.core.annotations.Step;
import pages.Order_AutoTransport_section;
import pages.Order_BillStorage_section;
import pages.Order_FacilityInfo_section;
import pages.Order_Insurance_section;
import pages.Order_ServiceDates_section;
import pages.Order_ServicesCharge_section;
import pages.Order_ShipmentInfo_section;
import pages.Order_StorageInfo_section;
import pages.Order_StorageTransit_section;

import pages.OrderCommonInfo_section;

import pages.Transf_Appraisal_details;

import pages.Transf_Domestic_tab;
import pages.Transf_HBO_details;
import pages.Transf_HomeInspect_details;
import pages.Transf_HomePurchase_details;
import pages.Transf_HomeSale_details;
import pages.Transf_InventManage_details;
import pages.Transf_Mortgage_details;

import pages.Transf_Service_tab;
import pages.Transf_TQ_details;

//import pages.Transf_Appraisal_details;

public class ARCAdmin_ServiceManage_Steps extends StepsBase{
	
	Transf_Domestic_tab domesticTab;
	Transf_HomeInspect_details InspectDetailsPage;
	Transf_Service_tab ServiceTab;
	Transf_Appraisal_details AppraisailDetailsPage;
	Transf_HBO_details HBODetails;
	OrderCommonInfo_section OrderCommonInfo_section;
	Order_ServiceDates_section OrderServiceDates_section;
	Order_ShipmentInfo_section OrderShipInfo_section;
	Order_FacilityInfo_section OrderFacilityInfo_section;
	Order_Insurance_section OrderInsurance_section;
	Order_AutoTransport_section OrderAutoTransport_section;
	Order_StorageTransit_section OrderStorgTransit_section;
	Order_FacilityInfo_section OrderFacility_section;
	Order_BillStorage_section OrderBillStorage_section;
	Order_ServicesCharge_section OrderServiceCharge_section;
	Order_StorageInfo_section OrderStorageInfo_section;
	Transf_InventManage_details InventManagePage;
	Transf_HomePurchase_details HPDetailsPage;
	Transf_HomeSale_details HSDetailsPage;
	Transf_Mortgage_details MGDetailsPage;
	Transf_TQ_details TQDetailsPage;
	
	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		 domesticTab = new Transf_Domestic_tab(driver);
		 InspectDetailsPage = new Transf_HomeInspect_details(driver);
		 ServiceTab = new Transf_Service_tab(driver);
		 AppraisailDetailsPage = new Transf_Appraisal_details(driver);
		 HBODetails = new Transf_HBO_details(driver);
		 OrderCommonInfo_section = new OrderCommonInfo_section(driver);
		 OrderServiceDates_section = new Order_ServiceDates_section(driver);
		 OrderShipInfo_section = new Order_ShipmentInfo_section(driver);
		 OrderFacilityInfo_section = new Order_FacilityInfo_section(driver);
		 OrderInsurance_section = new Order_Insurance_section(driver);
		 OrderAutoTransport_section = new Order_AutoTransport_section(driver);
		 OrderStorgTransit_section = new Order_StorageTransit_section(driver);
		 OrderFacility_section = new Order_FacilityInfo_section(driver);
		 OrderBillStorage_section = new Order_BillStorage_section(driver);
		 OrderServiceCharge_section = new Order_ServicesCharge_section(driver);
		 OrderStorageInfo_section = new Order_StorageInfo_section(driver);
		 InventManagePage = new Transf_InventManage_details(driver);
		 HPDetailsPage = new Transf_HomePurchase_details(driver);
		 HSDetailsPage = new Transf_HomeSale_details(driver);
		 MGDetailsPage = new Transf_Mortgage_details(driver);
		 TQDetailsPage = new Transf_TQ_details(driver);
		
	}//
	
	//METHODS
	
	@Step("Go to Service Details{0}")
	public void Go_To_Service_Details(String trasnf_service_name)
	{
		domesticTab.Go_To_Service_Details(trasnf_service_name); 
	}
		


	@Step("When #actor Updates transfree-Service Tab Info")
	public void Update_transfree_ServiceTab_Info(Transferee_Info transf)
	{
		ServiceTab.EditService_func(transf.ServiceList);
	}//end void
	
	@Step("Admin Turn-Off transfree-Service On Service Tab")
	public void Turn_Off_transfree_Service_On_Service_Tab(String service_type)
	{
		ServiceTab.Turn_Off_Service_func(service_type);
	}//end void
	
	
	@Step("When #actor Updates transfree-HHG Info")
	public void Update_transfree_HHG_Info(Transferee_Info transf)
	{
		//domesticTab.UpdateHHGService_func(transf.HHG_service, "Submit");
	}//end void
	
	@Step("When #actor Updates transfree-HomeSale Info")
	public void Update_transfree_HomeSale_Info(Transferee_Info transf)
	{
		domesticTab.UpdateHSService_func(transf.HS_service, "Submit");
	}//end void
	
	@Step("When #actor Updates transfree-HomePurchase Info")
	public void Update_transfree_HomePurchase_Info(Transferee_Info transf)
	{
		domesticTab.UpdateHPService_func(transf.HP_service, "Submit");
	}//end void
	
	@Step("When #actor Updates Service {0}-Property Section")
	public void Update_ServiceDetails_Property_Section(String service_name, Address_Info addr_info)
	{
		domesticTab.Update_ServiceDetails_Property_section_func(addr_info , "Submit");
		
	}//end void
	
	@Step("When #actor Updates transfree-Appraisal Info")
	public void Update_transfree_Appraisal_Info(AppraisalService_Info appraisal_info)
	{
		domesticTab.Update_ServiceDetails_Property_section_func(appraisal_info.Addr_info , "Submit");	
	}//end void
	
	@Step("When #actor Updates transfree-Morgage Info")
	public void Update_transfree_Morgage_Info(Transferee_Info transf)
	{
		domesticTab.UpdateMGService_func(transf.MG_service, "Submit");
	}//end void
	
	
	@Step("When #actor Updates transfree-TemporaryQuater Info")
	public void Update_transfree_TQ_Info(Transferee_Info transf)
	{
		domesticTab.UpdateTQService_func(transf.TQ_service, "Submit");
	}//end void
	
	@Step("When #actor Updates transfree-InventoryManagement Info")
	public void Update_transfree_IventoryManage_Info()
	{
		
	}//end void
	
	@Step("When #actor Updates transfree-HomeInspection Info")
	public void Update_transfree_HomeInspect_Info()
	{
		
	}//end void
	
	@Step("Admin Assigns Vendor Contact to Service Details")
	public void Assign_Vendor_Contact_To_ServiceDetails(String transf_servicename, Contact_Info vendor_contact)
	{
		domesticTab.Go_To_Service_Details(transf_servicename); 		
		
		domesticTab.Select_VendorContact_On_VendorList_func(vendor_contact);
	}
	
	
	@Step("Admin Sets Vendor Contact as Selected Provideder on Service Details")
	public void Set_Vendor_Contact_as_Provideder_Service_Details(String ctact_FirstName)
	{
		domesticTab.Set_VendorContact_As_Provider_func(ctact_FirstName);
		
	}
	@Step("Admin Updates Apprasail Contact info on Modal")
	public void Update_Appraisal_ContactDetails_On_Modal(Contact_Info contact_info)
	{
		AppraisailDetailsPage.Update_Appraisal_CtactModal_Info_func(contact_info,"submit");
		
	}
	
	
	
	@Step("Admin Adds New HBO - Offer")
	public void Add_New_HBO_Offer(HBOService_Info service)
	{
		for ( HBO_Offer_Info offer : service.Offer_list) {
			HBODetails.Add_New_Offer_func(offer,"submit");
			
		}//end for
		
	}//end void
	
	@Step("Admin Updates HBO Transaction Info")
	public void Updates_HBO_Transaction_Info(HBOService_Info service)
	{
		HBODetails.Edit_TransactionDetails_func(service, "submit");
	}
	
	@Step("Admin HBO Create New Concession")
	public void HBO_Create_New_Concession(HBOService_Info service)
	{
		HBODetails.Create_New_Concessions_func(service, "submit");
	}
	
	@Step("Admin HBO Edit Concession")
	public void HBO_Edit_Concession(HBOService_Info service)
	{
		HBODetails.Edit_Concessions_func(service.Seller_AdjustExplain, service, "submit");
	}
	
	@Step("Admin HBO Remove Concession in the Table")
	public void HBO_Remove_Concession_IntheTable(HBOService_Info service)
	{
		HBODetails.Remove_Concession_In_The_Table_func(service.Seller_AdjustExplain);
	}
	
	@Step("Admin HBO Create New Inspection")
	public void HBO_Create_New_Inspection(HBOService_Info service)
	{
		HBODetails.Create_New_Inspection_func(service, "submit");
	}
	
	@Step("Admin HBO Edit Inspection")
	public void HBO_Edit_Inspection(HBOService_Info service)
	{
		HBODetails.Edit_Inspection_func(service, "submit");
	}
	
	@Step("Admin HBO Remove Inspection in the Table")
	public void HBO_Remove_Inspection_IntheTable(HBOService_Info service)
	{
		HBODetails.Remove_Inspection_In_The_Table_func(service.Ins_Type);
	}
	
	@Step("Admin HBO Create New Repair")
	public void HBO_Create_New_Repair(HBOService_Info service)
	{
		HBODetails.Create_New_Repair_func(service, "submit");
	}
	
	@Step("Admin HBO Edit Repair")
	public void HBO_Edit_Repair(HBOService_Info service)
	{
		HBODetails.Edit_Repair_func(service, "submit");
	}
	
	@Step("Admin HBO Remove Repair in the Table")
	public void HBO_Remove_Repair_IntheTable(HBOService_Info service)
	{
		HBODetails.Remove_Repair_In_The_Table_func(service.Seller_AdjustExplain);
	}
	
	@Step("Admin Creates New HHG Service's Order")
	public void Create_New_HHG_Service_Order(Service_Order_Info order )
	{
		domesticTab.Create_New_HHG_Service_Order(order);
	}
	
	@Step("Admin Duplicate HHG Service's Order")
	public void Duplicate_New_HHG_Service_Order(Service_Order_Info order )
	{
		OrderCommonInfo_section.Copy_Order_func(order.Description);
	}
	
	@Step("Admin Remove Vendor Contact On VendorList")
	public void Remove_VendorContact_On_VendorList(Contact_Info ctact_info)
	{
		domesticTab.Remove_VendorContact_On_VendorList_func(ctact_info.FullName);
	}
	
	@Step("Admin HHG - Update Vendor Selected Section")
	public void Update_HHG_VendorName_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderCommonInfo_section.Select_Vendor_To_Order_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Order Info Section")
	public void Update_HHG_OrderInfo_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderCommonInfo_section.Update_OrderInfo_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Service Dates Section")
	public void Update_HHG_ServiceDates_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderServiceDates_section.Update_ServiceDates_Section_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Billing and Invoice Section")
	public void Update_HHG_BillingInvoice_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderCommonInfo_section.Update_BillingInvoice_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Insurance Section")
	public void Update_HHG_Insurance_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderInsurance_section.Update_Insurance_Section_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Auto Transport Section")
	public void Update_HHG_AutoTransport_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderAutoTransport_section.Update_AutoTransport_section_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Facility Info Section")
	public void Update_HHG_FacilityInfo_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderFacility_section.Update_FacilityInfo_Section_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Storage in Transit Section")
	public void Update_HHG_StorageTransit_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderStorgTransit_section.Update_StorageTransit_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Shipment Info Section")
	public void Update_HHG_ShipmentInfo_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderShipInfo_section.Update_ShipmentInfo_func(order, "submit");
	}//end void
	
	@Step("Admin HHG - Update Service Charge Section")
	public void Update_HHG_ServiceCharge_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderServiceCharge_section.Update_ServiceCharge_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Storage Info Section")
	public void Update_HHG_StorageInfo_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderStorageInfo_section.Update_StorageInfo_Section_func(order,"submit");
	}//end void
	
	@Step("Admin HHG - Update Billing Storage Info Section")
	public void Update_HHG_BillStorage_Section(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderBillStorage_section.Update_BillStorage_Section_func(order,"submit");
	}//end void
	
	@Step("Admin Creates HomeInspect Service - Inspection Need section")
	public void Create_HomeInspect_Service_Inspection_Need(HomeInspectService_Info HomeInspect_info)
	{
		InspectDetailsPage.Create_New_InspectNeed_func(HomeInspect_info,"submit");
	}
	
	@Step("Admin Updates HomeInspect Service - Existing Inspection Need")
	public void Update_HomeInspect_Service_InspectionNeed(String Old_InspectType, HomeInspectService_Info HomeInspect_info)
	{
		InspectDetailsPage.Update_Existing_InspectNeed_func(Old_InspectType,HomeInspect_info,"submit");
	}
	
	@Step("Admin Updates HomeInspect Service - SpecialList")
	public void Update_HomeInspect_Specialist(HomeInspectService_Info HomeInspect_info)
	{
		InspectDetailsPage.Update_Inspection_Specialist_func(HomeInspect_info,"submit");
	}
	
	
	@Step("Admin Updates HomeInspect Service - Inspection Result section")
	public void Update_HomeInspect_Service_Inspection_Result(HomeInspectService_Info Inspec_info)
	{
		
		InspectDetailsPage.Update_Inspection_Result_func(Inspec_info,"submit");
		
	
	}//end void
	
	@Step("Admin Removes HomeInspect Service - InspectionNeed")
	public void Remove_HomeInspect_Service_InspectionNeed(String InspectType)
	{
		
		InspectDetailsPage.Remove_InspectNeed_func(InspectType);
		
	
	}//end void
	
	@Step("Admin Update Appraisal Analysis Detail section")
	public void Update_Appraisal_Analysis_Detail(AppraisalService_Info service)
	{
		AppraisailDetailsPage.FillAnalysisDetail_func(service, "submit");
	}
	
	@Step("Admin Update Appraisal SpecialList section")
	public void Update_AppraisalSpecialist_Info(AppraisalService_Info service)
	{
		AppraisailDetailsPage.UpdateSpecialist_func(service);
	}
	
	@Step("Admin Select Vendor Contact From Service Details - CustomSearch Modal")
	public void Select_VendorContact_On_CustomSearch(Contact_Info ctact_info)
	{
		domesticTab.Select_VendorContact_On_CustomSearch_func(ctact_info);
	}
	
	
	@Step("Copy InventManage - List and Sale Info from Home Sale Service")
	public void Copy_InventManage_List_Sale_Info_from_HomeSale_Service()
	{
		InventManagePage.Update_Home_Listing_Sale_section_func(null,true,"submit");
	}
	
	
	@Step("Update InventManage - List and Sale Info section")
	public void Update_InventManage_List_and_Sale_Info_Section(HSService_Info hs_service_info)
	{
		InventManagePage.Update_Home_Listing_Sale_section_func(hs_service_info,false,"submit");
	}
	
	@Step("Update InventManage - HomeSale Analysis Info section")
	public void Update_InventManage_HomeSaleAnalysis_Info(InventManageService_Info InventMnage_info)
	{
		InventManagePage.Update_Home_Sale_Analysis_section_func(InventMnage_info, "submit");
	}
	
	
	//==============================================VERIFYCATION STEPS
	@Step("Verify Appraisal Analysis must be shown correctly on ViewMode")
	public void Verify_Appraisal_Analysis_Info_is_correct_ViewMode(AppraisalService_Info apprsaisal_info)
	{
		AppraisailDetailsPage.Verify_AppraisalAnalysis_section_ViewMode_func(apprsaisal_info);
		
	}//end void
	
	@Step("Verify transfee's Bank Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_Bank_Info_is_correct(Transferee_Info tranf)
	{
		
	}//end void
	
	@Step("Verify transfee's Service Info on Service Tab is correct on ViewMode")
	public void Verify_transfee_ServiceTab_Is_Correct_ViewMode(List<Services_Info> service_list)
	{
		ServiceTab.VerifyServiceInfoViewMode_func(service_list);
	}//end void
	
	@Step("Verify Vendor Contact is Shown on the Service Assigned Vendor Contact table")
	public void Verify_Vendor_Contact_Shown_Service_Assigned_Contact_Table(String Transf_serviceType, Contact_Info ctact_info)
	{
		domesticTab.Go_To_Service_Details(Transf_serviceType); 
		
		domesticTab.Verify_Vendor_Contact_Shown_Correctly_AssignedList_func(ctact_info);
	
	}//end void
	
	@Step("Verify Vendor Contact is NOT Shown On Service Assigned Vendor Contact table")
	public void Verify_Vendor_Contact_Not_Shown_Service_Assigned_Contact_Table(String Transf_serviceType, Contact_Info ctact_info)
	{
		domesticTab.Go_To_Service_Details(Transf_serviceType); 
		
		domesticTab.Verify_Vendor_Contact_Not_Shown_AssignedList_func(ctact_info);
	
	}//end void
	
	
	@Step("Verify Vendor Contact Can Be Assigned to Order")
	public void Verify_Vendor_Contact_Can_Assigned_Order(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderCommonInfo_section.Verify_OrderDetail_VendorSelected_func(order_info.VendorName);
	}//end void
	
	@Step("Verify {0} - Property Address Info Is Correct on ViewMode")
	public void Verify_ServiceDetails_Property_Address_Is_Correct_ViewMode(String servicename, Address_Info Add_info)
	{
		
		//Resuse this method due to Property Address section is the same with Appraisal
		AppraisailDetailsPage.Verify_PropertyAddress_Detail_Is_Correct_func(servicename,Add_info);
	}//end void
	
	@Step("Verify Appraisal - Specialist must be show correctly in ViewMode ")
	public void Verify_AppraisalSpecialist_Correct_ViewMode(AppraisalService_Info service)
	{
		AppraisailDetailsPage.Verify_AppraisalSpecialist_func(service);
	}
	
	@Step("Verify HHG - OrderInfo Section must be shown correctly on ViewMode")
	public void Verify_HHG_OrderInfo_Section_Is_Correct(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderCommonInfo_section.Verify_OrderDetail_OrderInfo_func(order_info);
	}//end void
	
	@Step("Verify HHG - ServiceDates Section must be shown correctly on ViewMode")
	public void Verify_HHG_ServiceDates_Section_Is_Correct(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderServiceDates_section.Verify_OrderDetail_ServiceDates_Section_func(order_info);
	}//end void
	
	@Step("Verify HHG - ShipmentInfo Section must be shown correctly on ViewMode")
	public void Verify_HHG_ShipmentInfo_Section_Is_Correct(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderShipInfo_section.Verify_OrderDetail_ShipInfo_func(order_info);
	}//end void
	
	@Step("Admin HHG - Verify Billing Storage Info Section")
	public void Verify_HHG_BillStorage_Section_Is_Correct(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderBillStorage_section.Verify_BillStorage_section_func(order);
	}//end void
	
	@Step("Admin HHG - Verify Facility Info Section")
	public void Verify_HHG_FacilityInfo_Section_Is_Correct(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderFacility_section.Verify_FacilityInfo_section_func(order);
	}//end void
	
	@Step("Admin HHG - Verify Storage Info Section")
	public void Verify_HHG_StorageInfo_Section_Is_Correct(Service_Order_Info order)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order.Description);
		
		OrderStorageInfo_section.Verify_StorageInfo_section_func(order);
		
	}//end void
	
	@Step("Verify HHG - Insurance Section must be shown correctly on ViewMode")
	public void Verify_HHG_Insurance_Section_Is_Correct(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderInsurance_section.Verify_OrderDetail_Insurance_func(order_info);
	}//end void
	
	@Step("Verify HHG - Auto Transport Section must be shown correctly on ViewMode")
	public void Verify_HHG_AutoTransport_Section_Is_Correct(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderAutoTransport_section.Verify_AutoTransport_section_func(order_info);
	}//end void
	
	@Step("Verify HHG - StorageInTransit Section must be shown correctly on ViewMode")
	public void Verify_HHG_StorageInTransit_Section_Is_Correct(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderStorgTransit_section.Verify_OrderDetail_StorageTransit_func(order_info);
	}//end void
	
	@Step("Verify HHG - ServiceCharge Section must be shown correctly on ViewMode")
	public void Verify_HHG_ServiceCharge_Section_Is_Correct(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderServiceCharge_section.Verify_OrderDetail_ServiceCharge_func(order_info);
	}//end void
	
	@Step("Verify HHG - Billing and Invoice Section must be shown correctly on ViewMode")
	public void Verify_HHG_BillingInvoice_Section_Is_Correct(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderCommonInfo_section.Verify_OrderDetail_BillInvoice_func(order_info);
	}//end void
	
	@Step("Verify HHG - Billing and Invoice Section must be shown correctly in Client Portal")
	public void Verify_HHG_BillingInvoice_Section_Is_Correct_in_ClientPortal(Service_Order_Info order_info)
	{
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);
		
		OrderCommonInfo_section.Verify_OrderDetail_BillInvoice_in_ClientPortal_func(order_info);
	}//end void
	
	@Step("Verify transfee's HomeInspect Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_HomeInspect_Info_is_correct(Transferee_Info tranf)
	{
		
	}//end void
	
	@Step("Verify transfee's HomePurchase Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_HomePurchase_Info_is_correct(Transferee_Info tranf)
	{
		HPDetailsPage.Verify_HPServiceDetail_func(tranf.HP_service);
	}//end void
	
	@Step("Verify transfee's HomeSale Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_HomeSale_Info_is_correct(Transferee_Info tranf)
	{
		HSDetailsPage.Verify_HSServiceDetail_func(tranf.HS_service);
	}//end void
	
	@Step("Verify HBO Summary Section Is Correct On ViewMode")
	public void Verify_HBO_Summary_Section_Is_Correct_ViewMode(double HBO_OfferPrice_str, String HS_ListPrice, String HS_ListStartDate_str, String Invent_AnlysAvrDay_str,String Invent_EstSellPrice_str)
	{
		HBODetails.Verify_HBO_Summary_Section_func(HBO_OfferPrice_str, HS_ListPrice, HS_ListStartDate_str, Invent_AnlysAvrDay_str,Invent_EstSellPrice_str);
	}//end void
	
	@Step("HBO Verify Transaction Details section - Info must be shown correctly in Table")
	public void HBO_Verify_Transaction_Details_Section_is_correct(HBOService_Info service)
	{
		HBODetails.Verify_Transaction_Detail_func(service);
	}//end void
	
	@Step("HBO Verify Offer Detail section - Info must be shown correctly in Table")
	public void HBO_Verify_Offer_Detail_Section_is_correct(HBOService_Info service)
	{
		HBODetails.Verify_Offer_Detail_func(service.Offer_list.get(0));
	}//end void
	
	@Step("HBO Verify Required Repair section - Concession must be shown correctly in Table")
	public void HBO_Verify_NewRepair_is_correct(HBOService_Info service)
	{
		HBODetails.Verify_Repairs_In_Table_func(service);
	}//end void
	
	@Step("Admin HBO Verify Repair Not Show in the Table")
	public void HBO_Verify_Repair_Not_Show_IntheTable(HBOService_Info service)
	{
		HBODetails.Verify_Repair_Not_Show_IntheTable_func(service);
	}
	
	@Step("HBO Verify Seller Concessions section - Concession must be shown correctly in Table")
	public void HBO_Verify_NewConcession_is_correct(HBOService_Info service)
	{
		HBODetails.Verify_Concessions_In_Table_func(service);
	}//end void
	
	@Step("Admin HBO Verify Concession Not Show in the Table")
	public void HBO_Verify_Concession_Not_Show_IntheTable(HBOService_Info service)
	{
		HBODetails.Verify_Concession_Not_Show_IntheTable_func(service);
	}
	
	@Step("HBO Verify Inspections Need section - Inspection must be shown correctly in Table")
	public void HBO_Verify_NewInspection_is_correct(HBOService_Info service)
	{
		HBODetails.Verify_Ins_In_Table_func(service);
	}//end void
	
	@Step("Admin HBO Verify Inspection Not Show in the Table")
	public void HBO_Verify_Inspection_Not_Show_IntheTable(HBOService_Info service)
	{
		HBODetails.Verify_Ins_Not_Show_IntheTable_func(service);
	}
	
	@Step("Verify transfee's InventoryManage Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_InventoryManage_Info_is_correct(Transferee_Info tranf)
	{
		
	}//end void
	
	@Step("Verify transfee's Morgage Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_Morgage_Info_is_correct(Transferee_Info tranf)
	{
		MGDetailsPage.Verify_MGServiceDetail_func(tranf.MG_service);
	}//end void
	
	@Step("Verify transfee's TQ Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_TQ_Info_is_correct(Transferee_Info tranf)
	{
		TQDetailsPage.Verify_TQServiceDetail_func(tranf.TQ_service);
	}//end void

	
	@Step("Verify transfee's Service List is shown on Domestic Tab")
	public void Verify_Transfee_Service_List_shown_correct_on_Domestic_Tab(List<Services_Info> ServiceList)
	{
		domesticTab.Domestic_tab().click();
		
		int size_tmp = ServiceList.size();
		
		for(int i =0;i<size_tmp;i++)
		
		domesticTab.VerifyItemExistDropdown(domesticTab.Menu_List(), ServiceList.get(i).Type);
	}//end void
	
	@Step("Verify transfee's Unselected Service is Not shown on Domestic Tab")
	public void Verify_Unselected_Service_Not_shown_on_Domestic_Tab(String service_type)
	{
		if(domesticTab.Menu_List()==null)
			domesticTab.Domestic_tab().click();
		
		domesticTab.VerifyItemNotExistDropdown(domesticTab.Menu_List(), service_type);
	}//end void
	
	
	@Step("Verify Service Details - Vendor Selection List Is Correct")
	public void Verify_Service_Details_Vendor_Selection_List_Is_Correct(String transf_service_name, Contact_Info contact)
	{
		domesticTab.Verify_Vendor_Selection_List_Is_Correct_func(transf_service_name, contact);
	}
	
	@Step("Verify Service Details - Vendor Not shown on Selection List")
	public void Verify_Service_Details_Vendor_Not_Shown_On_Selection_List(String transf_service_name, Contact_Info contact)
	{
		domesticTab.Verify_Vendor_Not_Shown_Selection_List_func(transf_service_name, contact);
	}
	
	//===========================VERIFY METHODS
	@Step("Verify HomeInspect Service - InspectNeed Modal Details - ViewMode")
	public void Verify_Modal_InspectNeed_Info_ViewMode(HomeInspectService_Info HomeInspect_info,String note)
	{

		InspectDetailsPage.Verify_Modal_InspectNeed_Info_ViewMode_func(HomeInspect_info,note);
			
	}//end void
	
	@Step("Verify HomeInspect Service - SpecialList - ViewMode")
	public void Verify_Inspection_Specialist_ViewMode(HomeInspectService_Info HomeInspect_info)
	{

		InspectDetailsPage.Verify_Inspection_Specialist_ViewMode_func(HomeInspect_info);
		
	}//end void
	
	@Step("Verify HomeInspect Service - InspectResult - ViewMode")
	public void Verify_Inspection_Result_ViewMode(HomeInspectService_Info HomeInspect_info)
	{

		InspectDetailsPage.Verify_Inspection_Result_ViewMode_func(HomeInspect_info);
		
	}//end void
	
	@Step("Verify Inspectneed Shown Correctly On Table")
	public void Verify_InspectNeed_Shown_On_Table(HomeInspectService_Info HomeInspect_info)
	{
		InspectDetailsPage.Verify_InspectNeed_Shown_On_Table(HomeInspect_info);
	}
	
	
	@Step("Verify Inspectneed NOT Shown On Table")
	public void Verify_InspectNeed_Not_Shown_On_Table(String InspectType,String note)
	{
		InspectDetailsPage.VerifyDataNotExistTable_func(InspectDetailsPage.InspectNeed_InspectList_tbl(), InspectType, note);
	}
	
	@Step("Verify InventManage - Home Listing and Sale Info Is Correct on View Mode")
	public void Verify_InventManage_HomeList_Sale_Info_Correct_ViewMode(HSService_Info HSservice_info)
	{
		HSDetailsPage.Verify_HSServiceDetail_func(HSservice_info);
	}
	
	@Step("Verify InventManage - Home Sale Management Info Is Correct on View Mode")
	public void Verify_Invent_HSManage_Info_Correct_ViewMode(InventManageService_Info InventMnage_info,double BuyOutOffer_str)
	{
		InventManagePage.Verify_HSManage_Info_Is_Correct_func(InventMnage_info, BuyOutOffer_str);
	}



	
	

	//END SERVICE MANAGE====================================
	
	
	
	
}
