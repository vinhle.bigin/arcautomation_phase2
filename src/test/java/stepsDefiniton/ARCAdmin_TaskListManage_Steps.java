package stepsDefiniton;


import baseClasses.StepsBase;
import businessObjects.Activity_Info;
import net.thucydides.core.annotations.Step;
import pages.ActivityDetails_mdal;
import pages.LeftMenu_page;

import pages.TaskList_page;

public class ARCAdmin_TaskListManage_Steps extends StepsBase{
	LeftMenu_page LeftMenuPage;
	TaskList_page TasklistPage;
	ActivityDetails_mdal ActivityModal;
	
	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		LeftMenuPage = new LeftMenu_page(driver);
		
		TasklistPage = new TaskList_page(driver);
		
		ActivityModal = new ActivityDetails_mdal(driver);
		
	}
		
	
	//1. Task List MANAGE====================================
	@Step("Admin Searches for Activity With Title")
	public void Search_Activity_With_Title(String activity_title)
	{
		LeftMenuPage.GotoTaskListPage();
		
		TasklistPage.Search_Activity_Title_func(activity_title);
		
	}//end void
	
	@Step("Admin Creates New Activity Info From Task List page")
	public void Create_Activity(Activity_Info activity)
	{
		LeftMenuPage.GotoTaskListPage();
		
		TasklistPage.CreateActivity_func(activity);
	}
	
	@Step("Admin Updates Activity Info From Task List page")
	public void Update_Activity(String oldtitle, Activity_Info new_activity)
	{
		TasklistPage.Search_Activity_Title_func(oldtitle);
		
		TasklistPage.EditTaskList_func(oldtitle,new_activity);
	}
	
	
	@Step("Admin Remove Activity From Task List page")
	public void Remove_Activity(String title)
	{
		TasklistPage.Search_Activity_Title_func(title);
		
		TasklistPage.RemoveActivity_func(title);
	}
	
	
	
		//VERIFICATION POINT
	@Step("Verify Activity Exists In The Activity List")
	public void Verify_Activity_Exist_In_The_Activity_List(Activity_Info Act)
	{
		TasklistPage.Select_Max_Table_Entries_Numb_func(TasklistPage.Entry_Limit());
		
		TasklistPage.VerifyActivityExistTable_func(Act);
		
	}//end void
	
	@Step("Verify Activity Is Not Exists In The Activity List")
	public void Verify_Activity_Not_Exist_In_The_Activity_List(Activity_Info Act)
	{
		TasklistPage.VerifyActivityNotExistTable_func(Act);
		
		
	}//end void
	
	@Step("Verify Activity Modal - Details Is Correct")
	public void Verify_Activity_Modal_Details_Is_Correct(Activity_Info Act)
	{
		TasklistPage.VerifyModalDetailsViewMode_func(Act);
		
		
	}//end void
	
	
	@Step("Admin Searches for Existing Activity")
	public void Search_For_Existing_Activity(String SearchType, String Search_vlue)
	{
			
		TasklistPage.SearchTaskList_func(SearchType, Search_vlue);
	}
	
	@Step("Verify Task Details Can NOT Be Editable")
	public void Verify_Task_Details_Can_NOT_Be_Editable(Activity_Info activity, String note)
	{
		ActivityModal.OpenDetailsModal_func(activity);
		
		//PageObjects.Wait_For_ElementDisplay(ActivityModal.Edit_btn());
		
		String fieldname_str = "Edit_Button";
		
		ActivityModal.VerifyFieldNotDisplayed_func(fieldname_str, ActivityModal.Edit_btn(), note);
				
		Activity_Info new_activity = new Activity_Info();
		
		ActivityModal.Verify_Activity_Modal_Not_Editable(new_activity, note);
		
	}//end void

	@Step("Search Activity with Status")
	public void Search_Status_All_For_Task_func(String activity_status) {
		
		TasklistPage.Search_Activity_Status_func(activity_status);
	}
	
	@Step("Search Activity with All Conditions")
	public void Search_Activity_AllConditions_func(String fromdate, String todate, String title, String type, String priority, String status)
	{
		TasklistPage.Search_Activity_AllConditions_func(fromdate, todate, title, type, priority, status);
		//test
	}

	
	
	

}
