package stepsDefiniton;

import java.util.Date;

import baseClasses.StepsBase;
import businessObjects.AuthApproval_Config;
import businessObjects.Auth_Info;
import businessObjects.Transferee_Info;
import custom_Func.DateTime_Manage;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Step;
import pages.AuthDetails_page;
import pages.Authorization_page;
import pages.LeftMenu_page;
import pages.NewAuth_mdal;
import pages.AuthGeInfo_page;


public class AuthManage_Steps extends StepsBase{
	LeftMenu_page LeftMenuPage;
	NewAuth_mdal NewAuth_mdal;
	Authorization_page Authorization_page;
	AuthDetails_page AuthDetails_page; 
	
	
	@Override
	public void InitStepsDefinition() {
		LeftMenuPage = new LeftMenu_page(driver);
		NewAuth_mdal = new NewAuth_mdal(driver);
		Authorization_page = new Authorization_page(driver); 
		AuthDetails_page = new AuthDetails_page(driver);
		

	}
	
	@Step("Given Admin finish Creating Transferee File From Auth")
	public Transferee_Info Finish_Creating_Transferee_File_from_Auth(Auth_Info auth_info)
	{
		Search_For_Existing_Auth(auth_info);
		
		Goto_Detail_Auth(auth_info);
		
		Assign_Auth(auth_info);
		
		Create_TransfFile(auth_info);
				
		//Note: Transfer Auth info to Transferee Info: All info include the status is transferred correctly
		
		Transferee_Info transf = new Transferee_Info();
		
		transf = auth_info;
		
		transf.Status = "Prospect";
		
		Date user_dt = DateTime_Manage.GetLocalDatetime(); 
		
		transf.FileEnterDate = DateTime_Manage.FormatDateTime_WithTimeZone(glb_RefData.glb_ARCUser_TimeZone,user_dt,"MM/dd/yyyy");
		
		return transf;
	}
	
	
	@Step("When Clients Search The Existing Auth")
	public void Search_For_Existing_Auth(Auth_Info auth_info)
	{
		new Authorization_page(driver).SearchAuth_func(auth_info);
	}//end void
	
	@Step("Given Admin Turn-on Auth Permission For User")
	public void Turn_On_Auth_Permission_For_User(String userfullname)
	{
		//Put page's method here
	}//end void
	
	@Step("Given Admin Config Auth Approval For Client")
	public void Config_Auth_Approval_For_Client(String clientName,AuthApproval_Config ApprovalConfig)
	{
		//Put page's method here
	}//end void
		
	@Step("When Clients Create New Auth")
	public void Create_New_Auth(Auth_Info auth_info)
	{		
		new Authorization_page(driver).CreateAuth_func(auth_info,"Submit");
		
	}//end void

	@Step("When Client Updates Existing Auth")
	public void Update_Existing_Auth(Auth_Info new_AuthInfo, NewAuth_mdal auth)
	{
		AuthDetails_page authDetail_page = new AuthDetails_page(driver);
		
		authDetail_page.GotoAuthDetail(new_AuthInfo.FullName);
		
		authDetail_page.EditAuth_func(new_AuthInfo);
			
	}//end void
	
	
	@Step("When Client Updates Existing Auth")
	public void Goto_Detail_Auth(Auth_Info auth_info)
	{
		new AuthDetails_page(driver).GotoAuthDetail(auth_info.FullName);
	}//end void
	
	@Step("When Client Deletes Existing Auth")
	public void Delete_Existing_Auth(Auth_Info auth_info)
	{
		AuthDetails_page.Delete_Auth_func(auth_info);
	}//end void
		
	@Step("When Admin Rejects Auth")
	public Auth_Info Reject_Auth(Auth_Info auth_info)
	{
		
		
		return auth_info;
		//Put page's method here
	}//end void
	
	@Step("When Admin Approves Auth")
	public void Approve_Auth(Auth_Info auth_info)
	{
		AuthDetails_page.Approve_Auth_func(auth_info);
		//Put page's method here
	}//end void
	
	@Step("When Admin Assigns Auth")
	public void Assign_Auth(Auth_Info auth_info)
	{
		AuthDetails_page.Assign_Auth_func(auth_info);
	}//end void
	
	@Step("When Admin Create Transf File")
	public void Create_TransfFile(Auth_Info auth_info)
	{
		AuthDetails_page.Create_TransfFile_func(auth_info);
	}//end void
	
	//===================================================================VERIFYCATION STEPS
	@Step("Verify Auth Details Info On View Mode")
	public void Verify_Auth_Details_Info_ViewMode(Auth_Info auth_info)
	{
		if(Authorization_page.AuthList_tbl()!=null)
		{
			new AuthDetails_page(driver).GotoAuthDetail(auth_info.FullName);
		}
		
		new AuthGeInfo_page(driver).VerifyAuthInfoViewMode_func(auth_info);
		
	}//end void
	
//	@Step("Verify Auth Details Info On View Mode 2")
//	public void Verify_Auth_Details_Info_ViewMode(Auth_Info auth_info, Authorization_page auth_page, NewAuth_mdal auth)
//	{		
//		AuthDetails_page.VerifyAuth_func(auth_info, auth_page, auth);
		
//	}//end void
	
	
	@Step("Verify Auth is shown on the table")
	public void Verify_Auth_Is_Displayed_On_Auth_List(Auth_Info auth_info)
	{
		new Authorization_page(driver).VerifyAuthExistTable_func(auth_info);
	}//end void
	
	@Step("Verify Auth is not shown on the table")
	public void Verify_Auth_Is_Not_Displayed_On_Auth_List(Auth_Info auth_info)
	{
		Authorization_page.Verify_Auth_Not_Exist_Table_func(auth_info, "After being deleted");
	}//end void


	


	//END  MANAGE====================================
	
	
	
	
}
