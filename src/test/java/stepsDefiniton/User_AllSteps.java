package stepsDefiniton;



import baseClasses.StepsBase;
import businessObjects.Mail_Info;
import configuration.TestConfigs;
import custom_Func.DateTime_Manage;
import custom_Func.Email_Manage;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pages.AccountProfile_page;
import pages.LeftMenu_page;
import pages.Messages_Notification;
import pages.NewAccount_mdal;
import pages.Notes_tab;
import pages.login_page;

public class User_AllSteps extends StepsBase{
	
	private String actor;
	private AccountProfile_page AccountProfile_page;
	private login_page login_page ;
	private LeftMenu_page LeftMenu_page;
	private Notes_tab Notes_tab;
	private NewAccount_mdal NewAccount_mdal;
	private Messages_Notification Messages_Notification;
	
	@Steps
	public AuthManage_Steps AuthManage;
	@Steps
	public ARCAdmin_ClientManage_Steps ClientManage;
	@Steps
	public ARCAdmin_CustomFieldManage_Steps CustomFieldManage;
	@Steps
	public ARCAdmin_SmartTaskManage_Steps SmarTaskManage;
	@Steps
	public ARCAdmin_TaskListManage_Steps TaskListManage;
	@Steps
	public ARCAdmin_TemplateManage_Steps TemplateManage;
	@Steps
	public ARCAdmin_TransfManage_Steps TransfManage;
	@Steps
	public ARCAdmin_UserRoleManage_Steps UserRoleManage;
	@Steps
	public ARCAdmin_VendorManage_Steps VendorManage;
	@Steps
	public ActivityManage_Steps ActivityManage;
	
	@Steps
	public NoteManage_Steps NoteManage;
	
	@Steps
	public DocumentManage_Steps DocumentManage;
	
	@Steps
	public ContactManage_Steps ContactManage;
	
	@Steps
	public ARCAdmin_ServiceManage_Steps ServiceManage;
	
	@Steps
	public ExpenseManage_Steps ExpenseManage;
	
	@Override
	public String toString()
	{
		return actor;
	}
	
	
	@Override
	public void InitStepsDefinition() {
		
		AccountProfile_page = new AccountProfile_page(driver);
		login_page = new login_page(driver);
		LeftMenu_page = new LeftMenu_page(driver);
		Notes_tab = new Notes_tab(driver);
		NewAccount_mdal = new NewAccount_mdal(driver);
		Messages_Notification = new Messages_Notification(driver);
		
		// TODO Auto-generated method stub
		AuthManage.GetConfig(driver);
		ClientManage.GetConfig(driver);
		CustomFieldManage.GetConfig(driver);
		SmarTaskManage.GetConfig(driver);
		TaskListManage.GetConfig(driver);
		TemplateManage.GetConfig(driver);
		TransfManage.GetConfig(driver);
		UserRoleManage.GetConfig(driver);
		VendorManage.GetConfig(driver);
		ActivityManage.GetConfig(driver);
		NoteManage.GetConfig(driver);
		DocumentManage.GetConfig(driver);
		ContactManage.GetConfig(driver);
		ServiceManage.GetConfig(driver);
		ExpenseManage.GetConfig(driver);
		
	}

	
	//1. ARC PORTAL MANAGE====================================
		@Step("Admin goes to ARC Portal page")
		public void Login_to_ARC_Portal_as_admin()
		{
			login_page = new login_page(driver);
			
			String url = glb_RefData.ARCPortal;
			String username = glb_RefData.glb_ARC_username;
			
			String pass = glb_RefData.glb_ARC_pass;
		
			login_page.Login_func(url,username,pass);
			
		}
		
		@Step("User Resets Password")
		public void Reset_Password(String portal_url,String email)
		{
			login_page = new login_page(driver);
			
			login_page.Reset_Password_func(portal_url,email);
			
		}
		
		
				
		@Step("Admin LogOut From Portal")
		public void Log_Out_From_Portal()
		{
			AccountProfile_page = new AccountProfile_page(driver);
			
			AccountProfile_page.log_out_from_Portal_func();
		}
		
		@Step("Verify user receives email with correct content")
		public void Verify_User_Receives_Email_Gmail(Mail_Info expect_email_info,String Attachment_content,String emailcontent_operator)
		{
			//NOTE: emailcontent_operator = "Equals", "Contains";
			
			Email_Manage mail_connector = new Email_Manage("Gmail");
			
			expect_email_info.sentDate = DateTime_Manage.GetLocalDatetime();
			
			Mail_Info temp = null;
			
			mail_connector.FetchEmail_func(60000);
						
			temp = mail_connector.FindExistEmail(expect_email_info);		
			
			if(temp!=null)
			{
				temp.VerifyContentEmail(expect_email_info,emailcontent_operator);

				//VERIFY ATTACHMENT IS VALID
				temp.VerifyAttachmentContent_func(Attachment_content);
			}
			else
			{
				TestConfigs.glb_TCStatus=false;
				TestConfigs.glb_TCFailedMessage+="Email["+expect_email_info.Subject+"] Is Not Found on MailBox.\n";
			}
					
		}
		
		@Step("Go To Task List Page")
		public void Go_To_Task_List_Page()
		{
			LeftMenu_page.GotoTaskListPage();
		}
		
		@Step("Admin Go To Note Tab")
		public void Go_to_Note_Tab()
		{
			Notes_tab.GotoNoteTab_func();
		}
		
		@Step("Go to Document Tab")
		public void Go_To_Document_Tab()
		{
			LeftMenu_page.GotoDocumentTab_func();
		}//end void
		
		@Step("Admin Goes To Vendor Page")
		public void Go_to_Vendor_Page()
		{
			LeftMenu_page.GotoVendorPage();
		}
		
		@Step("Admin Go To Client Page")
		public void Go_to_Client_Page()
		{
			LeftMenu_page.GotoClientPage();
		}
		
		@Step("Admin Go To Transferees Page")
		public void Go_to_Transferees_Page()
		{
			LeftMenu_page.GotoTransfereePage();
		}
		
		@Step("Admin Go To Profile Page")
		public void Go_to_Profile_Page()
		{
			AccountProfile_page.Go_To_Profile_func();
		}
		
		@Step("Admin When admin Close Login-Account Information modal")
		public void Close_Account_Information_modal()
		{
			
			NewAccount_mdal.Close_Modal_func();
		}//end void
		
		@Step("User LogOut From Portal")
		public void LogOutFromPortal()
		{
			
		}
		
		@Step("Verify Error Message Is Shown")
		public void Verify_Error_Message_Is_Shown(String Expected_msg,String note)
		{
			Messages_Notification.VerifyNotificationMessageDisplay_func(Expected_msg, note);
		}

		@Step("Verify Account Is Locked")
		public void Verify_Account_is_Locked(String note_str)
		{
			login_page.Verify_Account_is_Locked_func( note_str+".\n");
		}
		
		@Step("Verify Account Can Login Successfully")
		public void Verify_Account_Can_Login(String note_str)
		{
			login_page.Verify_Account_Can_Login_func(note_str+".\n");
		}
		
		@Step("Wait For SM Action is trigger effectly")
		public void Wait_For_SM_Action_Is_Trigger_Effectly()
		{
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		


		@Step("Client Go To Authorization Page")
		public void Go_To_Authorization_Page()
		{
			LeftMenu_page.GotoAuthPage_ClienPortal();
		}

}
