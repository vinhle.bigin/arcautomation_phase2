package stepsDefiniton;

import baseClasses.StepsBase;
import businessObjects.Mail_Info;
import businessObjects.Note_Info;
import configuration.TestConfigs;
import custom_Func.Email_Manage;
import net.thucydides.core.annotations.Step;
import pages.ARCLoading;

import pages.NoteDetails_mdal;
import pages.Notes_tab;


public class NoteManage_Steps extends StepsBase{
	Notes_tab Notes_tab;
	ARCLoading ARCLoading;
	NoteDetails_mdal NoteDetails_mdal;
	
	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		 Notes_tab = new Notes_tab(driver);
		 ARCLoading = new ARCLoading(driver);
		 NoteDetails_mdal = new NoteDetails_mdal(driver);
		
	}



	
	@Step("When admin creates New Note From Note Tab")
	public void Create_New_Note_From_Note_Tab(Note_Info note_Info)
	{
		Notes_tab.GotoNoteTab_func();
		
		Notes_tab.Create_New_Note_func(note_Info);
		
	}//end void
	
	@Step("When user creates Note Info via Top Note-Shortcut")
	public void Create_Note_via_Top_Note_Shortcut(Note_Info note_Info)
	{
		Notes_tab.Create_New_Note_Transf_Info_Top_Menu(note_Info);
	}
	
	@Step("When #actor delete Note")
	public void Delete_Note_func(Note_Info note_Info)
	{
		Notes_tab.Delete_Note_func(note_Info);
	}//end void
	
	@Step("When #actor updates Existing Note Info")
	public void Update_Exiting_Note(String oldtitle, Note_Info note_Info)
	{
		Notes_tab.Update_Note_Info_func(oldtitle, note_Info, "Submit");
	}//end void
	
	
	@Step("Search Existing Note on Note Tab")
	public void Search_Existing_Note_On_Note_Tab(String title)
	{
		Notes_tab.GotoNoteTab_func();
		
		Notes_tab.SearchNote_func(title);
		
	}
	
	@Step("Quick Search Existing Note on Note Tab")
	public void Quick_Search_Existing_Note_On_Note_Tab(String title)
	{
		Notes_tab.GotoNoteTab_func();
		
		Notes_tab.Quick_Search_Note_func(title);
		
	}
	
	
	//===================================================================VERIFYCATION STEPS
	@Step("Verify User Can't Create Note")
	public void Verify_User_Cant_Create_Note()
	{
		Notes_tab.Optimize_ElementClick(Notes_tab.Note_tab());
		
		String fieldname_str = "CreateNew_btn";
		
		Notes_tab.VerifyFieldNotDisplayed_func(fieldname_str, Notes_tab.CreateNew_btn(), "Due to Deny Permission");
		
	}
	
	@Step("Verify Note is shown on the table")
	public void Verify_Note_Is_Shown_On_Table(Note_Info note_Info,boolean StopRun)
	{
		Notes_tab.GotoNoteTab_func();
		
		Notes_tab.Search_Note_func(note_Info);
		
		Notes_tab.VerifyNoteInfoCorrectExistTable_func(note_Info,StopRun);
	}
	
	@Step("Verify Note is shown on the table")
	public void Verify_Note_Is_Shown_On_Transferee(Note_Info note_Info,boolean StopRun)
		{
			Notes_tab.VerifyNoteInfoCorrectExistTable_func(note_Info,StopRun);
		}	
	
	@Step("Verify Modal Note Details With Email Content Is Correct")
	public void Verify_Modal_Note_Email_Content_Correct(Mail_Info mail)
	{
		try {
			Note_Info note = mail.Convert_To_Note_Info();
			
			Thread.sleep(60000);
			
			Email_Manage mail_connector = new Email_Manage("Gmail");
			
			mail_connector.FetchEmail_func(60000);
			
			Mail_Info temp = mail_connector.FindExistEmail(mail);
			
			if(temp!=null)
			{
				Notes_tab.VerifyModalDetailsInfo_func(note);
			}
			
			else
			{
				TestConfigs.glb_TCStatus=false;
				TestConfigs.glb_TCFailedMessage+="Email["+mail.Subject+"] Is Not Found on MailBox.\n";
			}
			
					
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//end void

	@Step("Verify Modal Note Detail Is Correct")
	public void Verify_Modal_Note_Details_Correct(Note_Info note)
		{
			Notes_tab.VerifyModalDetailsInfo_func(note);
		}//end void
		
	
	@Step("Verify Note Details Can NOT Be Editable")
	public void Verify_Note_Details_Can_NOT_Be_Editable(Note_Info Note, String note_str)
	{
		Notes_tab.Optimize_ElementClick(Notes_tab.noteTitle_lnk(Note.Title));

		ARCLoading.Wait_Util_FinishedPageLoading();
		
		//PageObjects.Wait_For_ElementDisplay(NoteDetails_mdal.Edit_btn());
		
		if(NoteDetails_mdal.Edit_btn()!=null)
			NoteDetails_mdal.Edit_btn().click();
		
		Note_Info new_Note = new Note_Info();
		
		NoteDetails_mdal.Verify_Note_Modal_Not_Editable(new_Note,note_str);
		
	}//end void

	@Step("Verify Note IS NOT Shown On The Table")
	public void Verify_Data_NOT_Shown_On_Table(Note_Info note_Info) {
		// TODO Auto-generated method stub
		
		Notes_tab.VerifyDataNotExistTable_func(Notes_tab.NoteList_tbl(), note_Info.Title, "");
	}



	

	//END  MANAGE====================================
	
	
	
	
}
