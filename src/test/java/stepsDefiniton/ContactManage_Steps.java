package stepsDefiniton;

import baseClasses.StepsBase;
import businessObjects.Contact_Info;
import net.thucydides.core.annotations.Step;
import pages.Contacts_tab;
import pages.NewAccount_mdal;


public class ContactManage_Steps extends StepsBase{
	Contacts_tab Contacts_tab;
	NewAccount_mdal NewAccount_mdal;
	

	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		Contacts_tab = new Contacts_tab(driver);
		NewAccount_mdal = new NewAccount_mdal(driver);
		
		
	}
	
	
	@Step("When Admin Creates New Contact")
	public void Create_New_Contact(Contact_Info ctact_info,boolean close_accountmodal)
	{
		Contacts_tab.CreateContact_func(ctact_info,"submit");
		
		if(close_accountmodal==true)
		{
			NewAccount_mdal.Close_Modal_func();
		}
	}
	
	@Step("Admin Updates Contact")
	public void Update_Contact(String old_fullname, Contact_Info ctact_info)
	{
		Contacts_tab.UpdateContact_func(old_fullname, ctact_info,"submit");
		
		
	}
	
	@Step("Admin Removes Contact")
	public void Remove_Contact(String fullname)
	{
		Contacts_tab.RemoveContact_func(fullname);
	}
	
	@Step("Admin Sets Contact As Default")
	public void Set_Contact_As_Default(String fullname)
	{
		Contacts_tab.Set_Contact_As_Default(fullname);
		
		
	}
	
	@Step("Admin Updates Contact Profile Account")
	public void Update_Contact_Profile_Account(Contact_Info contact)
	{
		Contacts_tab.Update_AccountInfo_func(contact, "submit");
		
	}
	
	@Step("Admin Updates Contact Account")
	public void Update_Contact_Account(Contact_Info ctact_info) {
		
		Contacts_tab.Update_AccountInfo_func(ctact_info, "submit");
		
	}
	
//VERIFICATION====================================
	@Step("Verify Contact Is Shown on Table")
	public void Verify_Contact_Is_Shown_on_Table(Contact_Info ctact_info)
	{
				
		boolean is_exist = Contacts_tab.IsItemExistTable(Contacts_tab.ContactList_tbl(), ctact_info.FullName);
		
		if(is_exist==false)
		{
			
			Contacts_tab.Search_Contact(ctact_info.FullName);
		}
		
		Contacts_tab.VerifyDataExistTable_func(Contacts_tab.ContactList_tbl(), ctact_info.FullName, "");
		
	}
	
	@Step("Verify Contact Is NOT Shown on Table")
	public void Verify_Contact_NOT_Shown_On_Table(Contact_Info ctact_info)
	{
				
		//boolean is_exist = PageObjects.IsItemExistTable(Contacts_tab.ContactList_tbl(), ctact_info.FullName);
		
		Contacts_tab.VerifyDataNotExistTable_func(Contacts_tab.ContactList_tbl(), ctact_info.FullName, "");
	}
	
	@Step("Verify Modal Contact Detail Is Correct")
	public void Verify_Modal_Contact_Detail_Is_Correct(Contact_Info ctact_info)
	{
				
		boolean is_exist = Contacts_tab.IsItemExistTable(Contacts_tab.ContactList_tbl(), ctact_info.FullName);
		
		if(is_exist==false)
		{
			
			Contacts_tab.Search_Contact(ctact_info.FullName);
		}
		
		Contacts_tab.VerifyModalViewMode_func(ctact_info);
	}
	
	@Step("Verify Contact Is Currently Set As Default")
	public void Verify_Contact_Is_Currently_Set_As_Default(String fullname)
	{
		Contacts_tab.Verify_Contact_Is_Currently_Set_As_Default(fullname);
	}
	
	@Step("Verify Contact Is Currently Set As Default")
	public void Verify_Contact_Is_NOT_Set_As_Default(String fullname)
	{
		Contacts_tab.Verify_Contact_Is_Not_Set_As_Default(fullname);
	}	
	
	@Step("Verify Remove Icon is not shown for the unique contact")
	public void Verify_Remove_Icon_NOT_shown_For_The_Unique_Contact(String fullname)
	{
		Contacts_tab.VerifyFieldNotDisplayed_func("Contact-RemoveIcon", Contacts_tab.Remove_icon(fullname), "for the unique Contact");
	}

	
	
}
