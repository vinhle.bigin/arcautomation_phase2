package stepsDefiniton;

import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import pages.login_page;


public class ClientUser_Steps extends User_AllSteps{
	
	public static String actor = "Client User";
	
	login_page login_page;
	
	@Steps
	public AuthManage_Steps AuthManage;

	public ClientUser_Steps() {
		
		// TODO Auto-generated constructor stub
	}

	@Override
	public void InitStepsDefinition() {
		
		super.InitStepsDefinition();
		AuthManage.GetConfig(driver);
		
		
	}
	
	//MANAGE CLIENT PORTAL-------------------------------------------------------------
	
	@Step("Client Login Client Portal page")
	public void Login_to_ClientPortal(String username, String password)
	{
		login_page = new login_page(driver);
		String url = glb_RefData.ClientPortal;
		
		String pass_str = password;
		
		if(!username.equals(""))
		{
			pass_str = password;
		}
		
		login_page.Login_func(url,username,pass_str);
		
	}
	
	@Step("Client Login Client Portal page")
	public void Client_login_to_Client_Portal()
	{
		login_page = new login_page(driver);
		
		String url = glb_RefData.ClientPortal;
		String username = glb_RefData.Client.username;
		
		String pass = glb_RefData.Client.password;
	
		login_page.Login_func(url,username,pass);
		
	}
	
	/*@Step("Client Go To Authorization Page")
	public void Go_To_Authorization_Page()
	{
		new Clients_page(driver).GotoAuthPageatCP();
	}*/

}
