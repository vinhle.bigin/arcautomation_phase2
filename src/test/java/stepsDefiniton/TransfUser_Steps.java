package stepsDefiniton;

import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Step;
import pages.login_page;

public class TransfUser_Steps extends User_AllSteps{
	public static String actor = "Transf User";
	


	
	
	@Override
	public void InitStepsDefinition() {
		
		super.InitStepsDefinition();
		
		
		
	}
	
	
	@Step("Transferee Login Transferee Portal page")
	public void Login_to_TransfereePortal(String username, String password)
	{
		String url = glb_RefData.TransfereePortal;
		
		new login_page(driver).Login_func(url,username,password);
		
		
		
	}
	
	
	

}
