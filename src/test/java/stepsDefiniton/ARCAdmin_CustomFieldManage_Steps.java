package stepsDefiniton;

import java.util.List;

import baseClasses.StepsBase;
import businessObjects.CustomField_Info;
import businessObjects.CustomGroup_Info;
import net.thucydides.core.annotations.Step;
import pages.CF_ManageGroup_tab;
import pages.CustomFieldManage_tab;
import pages.CustomFields;
import pages.LeftMenu_page;

public class ARCAdmin_CustomFieldManage_Steps extends StepsBase{
	
	CustomFieldManage_tab customfield_tab ;
	CF_ManageGroup_tab group_tab;
	CustomFields customfield;
	LeftMenu_page leftmenuPage;
	

	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		 customfield_tab = new CustomFieldManage_tab(driver);
		 leftmenuPage = new LeftMenu_page(driver);
		 customfield = new CustomFields(driver);
		
	}
	
	
	//1. Custome Field MANAGE====================================
	@Step("Admin Search For Custom Field")
	public void Search_Custom_Field(String fieldname_str)
	{
		customfield_tab = new CustomFieldManage_tab(driver);
		customfield_tab.SearchCustomField_func(fieldname_str);
		
	}//end void
	
	@Step("Admin Create New Custom Field")
	public void Create_New_Custom_Field(CustomField_Info field_info)
	{
		customfield_tab = new CustomFieldManage_tab(driver);
		leftmenuPage = new LeftMenu_page(driver);
		
		leftmenuPage.GotoCustomFieldPage();
		
		customfield_tab.Create_func(field_info,"Submit");
		
	}//end void
	
	@Step("Admin Update Custom Field")
	public void Update_Custom_Field(String oldfieldname, CustomField_Info new_field)
	{
		customfield_tab = new CustomFieldManage_tab(driver);
		
		Search_Custom_Field(oldfieldname);
		
		customfield_tab.Update_func(oldfieldname,new_field,"Submit");
		
	}//end void
	
	@Step("Admin Remove Custom Field")
	public void Remove_Custom_Field(CustomField_Info field_info)
	{	
		customfield_tab = new CustomFieldManage_tab(driver);
		
		Search_Custom_Field(field_info.FieldName);
		
		customfield_tab.Remove_func(field_info);
		
	}//end void
	
	@Step("Admin Create New Custom Field Group")
	public void Create_New_Custom_Field_Group(CustomGroup_Info group_info)
	{	
		leftmenuPage = new LeftMenu_page(driver);
		
		group_tab = new CF_ManageGroup_tab(driver);
		
		leftmenuPage.GotoCustomFieldPage();
		
		group_tab.Create_CFGroup_func(group_info, "Submit");
		
	}//end void
	
	@Step("Admin Search for New CF Group")
	public void Search_for_New_CFGroup(CustomGroup_Info group_info)
	{
		if(group_tab.NewGroup_btn()==null)
		{
			leftmenuPage.GotoCustomFieldPage();
		}		
		group_tab.Search_New_CFGroup_func(group_info, group_info.Name);
		
	}//end void
	
	
	
	
	
	//VERIFYCATION STEPS
	
	@Step("Verify Custom Field Details Is Correct on Transferee Details")
	public void Verify_CustomField_Display_TransferDetails(CustomField_Info cus_field)
	{
		customfield_tab = new CustomFieldManage_tab(driver);
		
		customfield_tab.Verify_CustomField_Display_TransferDetails(cus_field);

	}//end void
	
	@Step("Verify Custom Field Details Is Correct on ViewMode")
	public void Verify_Custom_Field_Details_Is_Correct_On_ViewMode(CustomField_Info cus_field)
	{
		customfield_tab = new CustomFieldManage_tab(driver);
		
		customfield_tab.VerifyInfoDetailPage_func(cus_field);

	}//end void
	
	
	//VERIFY OPTION IS DISPLAYED IN THE OPTION LIST IN DETAILS PAGE
	@Step("Verify Option is Displayed in Detals Page - Option List")
	public void Verify_Option_Displayed_On_Details_Page_Option_List(String opt_name)
	{
		customfield_tab = new CustomFieldManage_tab(driver);
		
		customfield_tab.VerifyOptionExistDetails(opt_name);
	}
	
	@Step("Verify Custom Fields")
	public void Verify_Custom_Fields(List<CustomField_Info> customfields_list)
	{
		customfield = new CustomFields(driver);
		
		customfield.VerifyCustomFields(customfields_list);
	}

	@Step("Verify Custom Field Group Exist Table")
	public void Verify_CFGroup_ExistTable(CustomGroup_Info group_info)
	{
		group_tab = new CF_ManageGroup_tab(driver);
		
		group_tab.Verify_CFGourp_ExistTable_func(group_info);
	}
	
	@Step("Verify Custom Field Group")
	public void Verify_CFGroup(CustomGroup_Info group_info)
	{
		group_tab = new CF_ManageGroup_tab(driver);
		
		group_tab.Verify_New_CFGroup(group_info);
	}

}
