package stepsDefiniton;

import java.util.List;

import baseClasses.StepsBase;
import businessObjects.Address_Info;
import businessObjects.Auth_Info;
import businessObjects.Contact_Info;
import businessObjects.CustomField_Info;
import businessObjects.Dependent_Info;
import businessObjects.Mail_Info;
import businessObjects.Other_Info;
import businessObjects.Print_Info;
import businessObjects.Service_Order_Info;
import businessObjects.Template_Info;
import businessObjects.Transferee_Info;
import businessObjects.UserAccount_Info;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Step;
import pages.AccountProfile_page;
import pages.EmailProcess_mdal;
import pages.LeftMenu_page;
import pages.NewAccount_mdal;
import pages.Order_Insurance_section;
import pages.Order_ServiceDates_section;
import pages.Print_tab;
import pages.OrderCommonInfo_section;
import pages.Shortcuts_menu;
import pages.Transf_Account_tab;
import pages.Transf_Address_tab;

import pages.Transf_CustomField_tab;
import pages.Transf_Depend_tab;
import pages.Transf_Domestic_tab;
import pages.Transf_GenInfo_tab;

import pages.Transf_OtherInfo_tab;

import pages.Transferees_page;

//import pages.Transf_Appraisal_details;

public class ARCAdmin_TransfManage_Steps extends StepsBase {
	LeftMenu_page LeftMenuPage;
	Transferees_page TransfereePage;
	NewAccount_mdal NewAccountModal;
	Shortcuts_menu ShortcutMenu;
	Transf_GenInfo_tab TransGenInfoTab;
	AccountProfile_page AccountProfilePage;
	Transf_OtherInfo_tab TransfOtherInfoTab;
	Transf_Address_tab TransfAddressTab;
	Transf_Depend_tab TransfDenpendTab;
	Transf_CustomField_tab TransfCustomFieldTab;
	EmailProcess_mdal EmailProcessModal;
	Transf_Account_tab TransfAccountTab;
	Transf_Domestic_tab TransfDomesticTab;
	OrderCommonInfo_section OrderCommonInfo_section;
	Order_ServiceDates_section Order_ServiceDates_section;
	Order_Insurance_section Order_Insurance_section;
	Print_tab PrintTab;

	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		LeftMenuPage = new LeftMenu_page(driver);
		TransfereePage = new Transferees_page(driver);
		NewAccountModal = new NewAccount_mdal(driver);
		ShortcutMenu = new Shortcuts_menu(driver);
		TransGenInfoTab = new Transf_GenInfo_tab(driver);
		AccountProfilePage = new AccountProfile_page(driver);
		TransfOtherInfoTab = new Transf_OtherInfo_tab(driver);
		TransfAddressTab = new Transf_Address_tab(driver);
		TransfDenpendTab = new Transf_Depend_tab(driver);
		TransfCustomFieldTab = new Transf_CustomField_tab(driver);
		EmailProcessModal = new EmailProcess_mdal(driver);
		TransfAccountTab = new Transf_Account_tab(driver);
		TransfDomesticTab = new Transf_Domestic_tab(driver);
		OrderCommonInfo_section = new OrderCommonInfo_section(driver);
		Order_ServiceDates_section = new Order_ServiceDates_section(driver);
		Order_Insurance_section = new Order_Insurance_section(driver);
		PrintTab = new Print_tab(driver);
	}

	@Step("Get Transferee Login Account Info")
	public Transferee_Info Get_Transferee_Login_Account_Info(Transferee_Info input_transf) {
		Transferee_Info info = input_transf;

		info.username = NewAccountModal.Get_UserName_func();
		info.password = NewAccountModal.Get_Temp_Password_func();

		return info;
	}

	@Step("Get Transferee Login Account Info")
	public void Goto_AddressTab() {
		TransfAddressTab.Goto_Address_tab();
	}

	@Step("When #actor creates new transfree from Transferee List")
	public void Create_new_transfree_from_Transferees_List(Transferee_Info tranf, boolean close_account_modal) {
		LeftMenuPage.GotoTransfereePage();

		TransfereePage.CreateTransf_func(tranf, true);

		if (close_account_modal == true) {
			Close_Account_Information_modal();
		}

	}// end void

	@Step("When #actor creates new transfree from Transferee List")
	public void Create_new_transfree_via_Top_Shortcut(Transferee_Info tranf, boolean close_account_modal) {
		LeftMenuPage.GotoTransfereePage();

		TransfereePage.CreateTransf_via_Shortcut_func(tranf, true);

		if (close_account_modal == true) {
			Close_Account_Information_modal();
		}

	}// end void

	@Step("When #actor Close Login-Account Information modal")
	public void Close_Account_Information_modal() {
		NewAccountModal.Close_Modal_func();
	}// end void

	@Step("When #actor Update User Profile section")
	public void Update_User_Profile_Section(Transferee_Info user) {
		AccountProfilePage.Update_User_Profile_Section(user, "submit");
	}// end void

	@Step("When #actor Update Time Zone section")
	public void Update_TimeZone_Section(Transferee_Info transf) {
		AccountProfilePage.Update_TimeZone_Section(transf, "submit");
	}// end void

	@Step("When #actor Update Password section")
	public void Update_Password_Section(Transferee_Info transf) {
		AccountProfilePage.Update_Password_Section(transf, "submit");
	}// end void

	@Step("When #actor Cancel Password section Updates")
	public void Cancel_Password_Section_Updates(Transferee_Info transf) {
		AccountProfilePage.Update_Password_Section(transf, "cancel");
	}// end void

	@Step("When #actor Update Phones & Emails section")
	public void Update_PhoneEmail_Section(Transferee_Info transf) {
		AccountProfilePage.Update_Phones_Emails_Section(transf, "submit");
	}// end void

	@Step("Admin Search for Transferee With First Name")
	public void Search_for_Transferee_With_Name(Transferee_Info transf) {
		if (TransfereePage.NewTransf_btn() == null) {
			LeftMenuPage.GotoTransfereePage();
		}
		TransfereePage.SearchTransferee_with_Name_func(transf.firstname);

	}// end void
	

	@Step("Admin Search for Transferee With Fullname On Client Portal")
	public void Search_for_Transferee_With_Firstname_Client_Portal(Transferee_Info transf) {
		if (TransfereePage.NewTransf_btn() == null) {
			LeftMenuPage.GotoTransfereePage();
		}

		TransfereePage.SearchTransferee_with_Name_func(transf.firstname);

		TransfereePage.GotoTransDetailsPage(transf.FullName);

	}// end void

	@Step("When #actor goes to transfree Details")
	public void Go_To_Transfeee_Details(Transferee_Info transf) {

		if (TransfereePage.TransfList_tbl() == null) {
			LeftMenuPage.GotoTransfereePage();
		}

		if (TransfereePage.SearchTranf_FirstName_txt() != null)

			TransfereePage.SearchTransferee_with_Name_func(transf.firstname);

		TransfereePage.GotoTransDetailsPage(transf.FullName);

	}// end void

	@Step("When #actor Updates transfree-General info")
	public void Update_transfree_General_Info(Transferee_Info transf_info) {
		TransGenInfoTab.EditGenInfo_func(transf_info);
	}// end void

	@Step("Admin Updates transfree-General tab - Transferee Info section")
	public void Update_transfree_Transferee_Info_section(Transferee_Info transf_info) {
		TransGenInfoTab.EditGenInfo_func(transf_info);
	}// end void

	@Step("Admin Updates transfree-General tab - Client section")
	public void Update_transfree_Client_section(Transferee_Info transf_info) {
		TransGenInfoTab.EditGenInfo_func(transf_info);
	}// end void

	@Step("Admin Updates transfree-General tab - Status section")
	public void Update_transfree_Status_section(Transferee_Info transf_info) {
		TransGenInfoTab.EditGenInfo_func(transf_info);
	}// end void

	@Step("Admin Add transfree-General tab - Phone section")
	public void Add_transfree_Phone_section(Transferee_Info transf_info) {
		TransGenInfoTab.EditGenInfo_func(transf_info);
	}// end void

	@Step("Admin Add transfree-General tab - Email section")
	public void Update_transfree_Email_section(Mail_Info mail) {
		TransGenInfoTab.Update_Transferee_Email(mail);
	}// end void

	@Step("Admin Update transfree-General tab - Phone section")
	public void Update_transfree_Phone_section(Transferee_Info transf_info) {
		TransGenInfoTab.EditGenInfo_func(transf_info);
	}// end void

	@Step("Admin Remove transfree-General tab - Phone section")
	public void Remove_transfree_Phone_section(Transferee_Info transf_info) {
		TransGenInfoTab.EditGenInfo_func(transf_info);
	}// end void

	@Step("Admin Remove transfree-General tab - Email section")
	public void Remove_transfree_Email_section(Transferee_Info transf_info) {
		TransGenInfoTab.EditGenInfo_func(transf_info);
	}// end void

	@Step("When #actor Updates transfree-Other Info")
	public void Update_transfree_Other_Info(Other_Info info) {
		TransfOtherInfoTab.Edit_TransfOtherInfo_func(info);
	}// end void

	@Step("When #actor Updates transfree-Address Info")
	public void Update_transfree_Address_Info(Transferee_Info new_transf_info) {
		TransfAddressTab.EditAddressInfo_func(new_transf_info);
	}// end void

	@Step("When #actor Creates transfree-Dependency Info")
	public void Create_transfree_Dependency_Info(Transferee_Info transf) {
		int count = transf.Dependent_list.size();

		for (int i = 0; i < count; i++) {
			TransfDenpendTab.CreateDepedent_func(transf.Dependent_list.get(i));
		}

	}// end void

	@Step("When #actor Updates transfree-Dependency Info")
	public void Update_transfree_Dependency_Info(String old_name, Dependent_Info newinfo) {

		TransfDenpendTab.UpdateDependent_func(old_name, newinfo);

	}// end void

	@Step("When #actor Updates transfree-Bank Info")
	public void Update_transfree_Bank_Info(Transferee_Info new_transf_info) {

	}// end void

	@Step("When #actor Updates transfree-CustomField Tab Info")
	public void Update_transfree_CustomFieldTab_Info(CustomField_Info field) {
		TransfCustomFieldTab.EditCustomField_func(field);
	}// end void

	@Step("When admin Send email to transfree")
	public void Send_Email_to_transfree(Mail_Info mail_info, boolean is_overrided_content) {
		if (is_overrided_content == false)
			ShortcutMenu.Send_Email_With_Template_Content_func(mail_info, "Submit");
		else
			ShortcutMenu.SendEmail_With_Customed_Content_func(mail_info, "Submit");
	}// end void

	@Step("Admin Updates Transfree Account Tab")
	public void Update_Transfree_Account_Tab(UserAccount_Info account_info) {
		TransfAccountTab.Update_AccountInfo_func(account_info, "submit");

	}

	@Step("Admin Assigns Vendor Contact to Service Details")
	public void Assign_Vendor_Contact_To_ServiceDetails(String transf_servicename, Contact_Info vendor_contact) {

		TransfDomesticTab.Go_To_Service_Details(transf_servicename);

		TransfDomesticTab.Select_VendorContact_On_VendorList_func(vendor_contact);

	}

	@Step("Admin Sets Vendor Contact as Selected Provideder on Service Details")
	public void Set_Vendor_Contact_as_Provideder_Service_Details(String ctact_FirstName) {

		TransfDomesticTab.Set_VendorContact_As_Provider_func(ctact_FirstName);

	}

	@Step("Go to Service Detail")
	public void Go_To_Service_Detail(String service_name) {
		TransfDomesticTab.Go_To_Service_Details(service_name);
	}

	@Step("Admin Creates New HHG Service's Order")
	public void Create_New_HHG_Service_Order(Service_Order_Info order) {
		TransfDomesticTab.Create_New_HHG_Service_Order(order);
	}

	@Step("Admin Duplicate HHG Service's Order")
	public void Duplicate_New_HHG_Service_Order(Service_Order_Info order) {
		OrderCommonInfo_section.Copy_Order_func(order.Description);
	}

	@Step("Admin Remove Vendor Contact On VendorList")
	public void Remove_VendorContact_On_VendorList(Contact_Info ctact_info) {
		TransfDomesticTab.Remove_VendorContact_On_VendorList_func(ctact_info.FullName);
	}

	@Step("Admin HHG - Update Vendor Selected Section")
	public void Update_HHG_VendorName(Service_Order_Info order) {
		OrderCommonInfo_section.Select_Vendor_To_Order_func(order, "submit");
	}

	@Step("Admin HHG - Update Order Info Section")
	public void Update_HHG_OrderInfo(Service_Order_Info order) {
		OrderCommonInfo_section.Update_OrderInfo_func(order, "submit");
	}

	@Step("Admin HHG - Update Service Dates Section")
	public void Update_HHG_ServiceDates(Service_Order_Info order) {
		Order_ServiceDates_section.Update_ServiceDates_Section_func(order, "submit");
	}

	@Step("Admin HHG - Update Billing and Invoice Section")
	public void Update_HHG_BillingInvoice(Service_Order_Info order) {
		OrderCommonInfo_section.Update_BillingInvoice_func(order, "submit");
	}

	@Step("Admin HHG - Update Insurance Section")
	public void Update_HHG_Insurance(Service_Order_Info order) {
		Order_Insurance_section.Update_Insurance_Section_func(order, "submit");
	}

	@Step("Admin Select Vendor Contact From Service Details - CustomSearch Modal")
	public void Select_VendorContact_On_CustomSearch(Contact_Info ctact_info) {
		TransfDomesticTab.Select_VendorContact_On_CustomSearch_func(ctact_info);
	}

	@Step("Admin Print Via ShortCut For PDF and Current Search")
	public void Print_Via_ShortCut_with_Current_Search(Print_Info print_info) {
		PrintTab.Print_Via_ShortCut_with_Current_Search(print_info, "Submit");
	}

	@Step("Admin Print Via ShortCut For PDF and Current Search")
	public void Print_Via_ShortCut_with_Some_Transf(Print_Info print_info, List<Transferee_Info> transf_list) {
		PrintTab.Print_Via_ShortCut_with_Some_Transfs(print_info, transf_list, "Submit");
	}

	// ==============================================VERIFYCATION STEPS
	@Step("Verify Transferee Account Info Is Correct on ViewMode")
	public void Verify_Transferee_Account_Info_Correct_ViewMode(Transferee_Info tranf, String note) {
		if (TransfAccountTab.Edit_btn() == null) {
			TransfAccountTab.Heading_tab().click();

		}

		TransfAccountTab.Verify_AccountInfo_ViewMode_func(tranf, note);
	}// end void

	@Step("Verify transfee must be shown on Tranferee List")
	public void Verify_transfee_is_shown_on_Tranferee_List(Transferee_Info tranf) {
		TransfereePage.VerifyTranfExistTable_func(tranf);
	}// end void

	@Step("Verify transfee's General Info  must be shown correctly on ViewMode")
	public void Verify_transfee_GeneralInfo_is_correct(boolean IsARCPortal, Transferee_Info tranf) {
		// PageObjects.wait_For_PageLoad();

		if (TransGenInfoTab.Generall_tab() == null) {
			Go_To_Transfeee_Details(tranf);
		}

		TransGenInfoTab.VerifyGeneralInfoViewMode_func(IsARCPortal, tranf);
	}// end void
	
	
	@Step("Verify transfee's General Info  must be shown correctly on ViewMode")
	public void Verify_transfee_GeneralInfo_is_correct(boolean IsARCPortal, Auth_Info auth) {
		// PageObjects.wait_For_PageLoad();

		if (TransGenInfoTab.Generall_tab() == null) {
			Go_To_Transfeee_Details(auth);
		}

		TransGenInfoTab.VerifyGeneralInfoViewMode_func(IsARCPortal, auth);
	}// end void
	

	@Step("Verify transfee's Other Info  must be shown correctly on ViewMode")
	public void Verify_transfee_OtherInfo_is_correct(Transferee_Info tranf) {
		TransfOtherInfoTab.VerifyInfoViewMode_func(tranf.Other_info);
	}// end void

	@Step("Verify transfee's Address Info must be shown correctly on ViewMode")
	public void Verify_transfee_Addressinfo_is_correct(Transferee_Info tranf) {
		TransfAddressTab.VerifyInfoViewMode_func(tranf);
	}// end void

	@Step("Verify transfee's Origin Residence Address Info must be shown correctly on ViewMode")
	public void Verify_transf_Addr_OrgRes_is_correct(Address_Info addr_info) {
		TransfAddressTab.Verify_OrgResViewMode_func(addr_info);
	}// end void

	@Step("Verify transfee's Destination Residence Address Info must be shown correctly on ViewMode")
	public void Verify_transf_Addr_DesRes_is_correct(Address_Info addr_info) {
		TransfAddressTab.Verify_DesResViewMode_func(addr_info);
	}// end void

	@Step("Verify transfee's Bank Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_Bank_Info_is_correct(Transferee_Info tranf) {

	}// end void

	@Step("Verify transfee's CustomField Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_CustomField_Info_is_correct(CustomField_Info field) {

		TransfCustomFieldTab.VerifyViewModePerElement(field);
	}// end void

	@Step("Verify transfee's Dependency Info must be shown correctly on ViewMode")
	public void Verify_transfee_Dependency_Info_is_correct(Dependent_Info dep_info) {
		TransfDenpendTab.VerifyModalDetailsViewMode_func(dep_info);
	}// end void

	@Step("Verify transfee's Dependency Info must be shown on Dependecies list")
	public void Verify_transfee_Dependency_Info_Is_Shown_On_List(Dependent_Info dep_info) {
		TransfDenpendTab.VerifyItemDisplayInTable_func(dep_info);
	}// end void

	@Step("Verify HHG - Billing and Invoice Section must be shown correctly on ViewMode")
	public void Verify_HHG_BillingInvoice_Section_Is_Correct(Service_Order_Info order_info) {
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);

		OrderCommonInfo_section.Verify_OrderDetail_BillInvoice_func(order_info);
	}// end void

	@Step("Verify HHG - Billing and Invoice Section must be shown correctly in Client Portal")
	public void Verify_HHG_BillingInvoice_Section_Is_Correct_in_ClientPortal(Service_Order_Info order_info) {
		OrderCommonInfo_section.Open_Order_Details_func(order_info.Description);

		OrderCommonInfo_section.Verify_OrderDetail_BillInvoice_in_ClientPortal_func(order_info);
	}// end void

	@Step("Verify transfee's HomeInspect Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_HomeInspect_Info_is_correct(Transferee_Info tranf) {

	}// end void

	@Step("Verify transfee's InventoryManage Info transfee must be shown correctly on ViewMode")
	public void Verify_transfee_InventoryManage_Info_is_correct(Transferee_Info tranf) {

	}// end void

	@Step("Verify Modal Account Info Is Correct After Transferee Is Created")
	public void Verify_Modal_Account_Info_Correct_func(String full_name) {
		NewAccountModal.Verify_Modal_Account_Info_Correct_func(full_name);

	}// end void

	@Step("Verify Modal Account Info Is NOT Displayed for New Prospect Transferee")
	public void Verify_Modal_Account_Info_NOT_Displayed_Prospect_Transferee() {
		String fieldname_str = "Account Modal - UserName_text";

		NewAccountModal.VerifyFieldNotDisplayed_func(fieldname_str, NewAccountModal.UserName_text(),
				"When creating Prospect Transferee File.\n");

		fieldname_str = "Account Modal - TempPass_text";

		NewAccountModal.VerifyFieldNotDisplayed_func(fieldname_str, NewAccountModal.TempPass_text(),
				"When creating Prospect Transferee File.\n");

	}// end void

	@Step("Verify the InActive Template NOT displayed on Email Modal-Template Dropdown")
	public void Verify_Template_NOT_Displayed_On_Email_Modal(Template_Info template) {
		ShortcutMenu.Email_btn().click();

		String note = "Email Process Modal - Service_ddl";

		EmailProcessModal.SelectDropdownItem(EmailProcessModal.Service_ddl(), template.ServiceType, note);

		EmailProcessModal.SelectDropdownItem(EmailProcessModal.Recipient_ddl(), template.ServiceType, note);

		EmailProcessModal.VerifyItemNotExistDropdown(EmailProcessModal.Template_ddl(), template.Name);

	}// end void

	@Step("Verify the InActive Template NOT displayed on Email Modal-Template Dropdown")
	public void Verify_Template_Displayed_On_Email_Modal(Template_Info template) {
		ShortcutMenu.Email_btn().click();

		String note = "Email Process Modal - Service_ddl";

		EmailProcessModal.SelectDropdownItem(EmailProcessModal.Service_ddl(), template.ServiceType, note);

		EmailProcessModal.SelectDropdownItem(EmailProcessModal.Recipient_ddl(), template.ServiceType, note);

		EmailProcessModal.VerifyItemExistDropdown(EmailProcessModal.Template_ddl(), template.Name);

	}// end void

	@Step("When #actor Verify User Profile section")
	public void Verify_User_Profile(Transferee_Info transf) {
		AccountProfilePage.Verify_User_Profile(transf);
	}// end void

	@Step("When #actor Verify User Profile section")
	public void Verify_PhonesEmails_Section(Transferee_Info transf) {
		AccountProfilePage.Verify_PhoneEmail_Section(transf);
	}// end void

	@Step("TransfPortal Verify Fullname in Sidebar")
	public void Verify_Fullname_in_Sidebar(Transferee_Info transf) {
		AccountProfilePage.Verify_Fullname_in_Sidebar(transf);
	}// end void

	@Step("When #actor Verify User Profile section")
	public void Verify_Fullname_in_TopMenu(Transferee_Info transf) {
		AccountProfilePage.Verify_Fullname_in_TopMenu(transf);
	}// end void

	@Step("TransfPortal Verify Time Zone Section")
	public void Verify_TimeZone_Section(Transferee_Info transf) {
		AccountProfilePage.Verify_TimeZone_Section(transf);
	}// end void

	@Step("Verify Email Modal Details")
	public void Verify_Email_Modal_Details(Mail_Info mail_info) {
		Open_Modal_Sending_Email_Form(mail_info);

		EmailProcessModal.VerifyModalDetails_func(mail_info);

	}// end void

	@Step("Open Modal Sending Email Form")
	public void Open_Modal_Sending_Email_Form(Mail_Info mail_info) {
		ShortcutMenu.Send_Email_With_Template_Content_func(mail_info, "");

	}// end void

	@Step("Verify Email Modal- Document Content Is Correct")
	public void Verify_Email_Modal_Document_Content_Is_Correct(String attach_name, String expected_content) {

		FileManage download_file = new FileManage();
		download_file.File_Name = "Download_File.docx";
		download_file.DownloadFile_func(EmailProcessModal.DocReview_lnk(attach_name), download_file.File_Name);

		download_file.VerifyFileContent_func(download_file.File_Name, expected_content);

		download_file.Delete_Downloaded_File_func(download_file.File_Name);

	}// end void
	

	@Step("When #actor goes to transfree Details")
	public void Go_To_Auth_Transfeee_Details(Auth_Info auth_tranf) {

		if (TransfereePage.TransfList_tbl() == null) {
			LeftMenuPage.GotoTransfereePage();
		}

		if (TransfereePage.SearchTranf_FirstName_txt() != null)

			TransfereePage.SearchTransferee_with_Name_func(auth_tranf.firstname);

		TransfereePage.GotoTransDetailsPage(auth_tranf.FullName);
	}
	
	
		// END TRANSFEREE MANAGE====================================

}
