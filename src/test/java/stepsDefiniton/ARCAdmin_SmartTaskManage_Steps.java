package stepsDefiniton;


import baseClasses.StepsBase;
import businessObjects.SmartTask_Action;
import businessObjects.SmartTask_Info;
import net.thucydides.core.annotations.Step;
import pages.LeftMenu_page;
import pages.Messages_Notification;
import pages.SmartTask_page;

public class ARCAdmin_SmartTaskManage_Steps extends StepsBase{
	LeftMenu_page LeftMenuPage;

	Messages_Notification notification;

	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		
		
	}

	
	//1. SMART TASK MANAGE====================================
	@Step("Admin Search Smart Task")
	public void Search_Smart_Task(String search_str)
	{
		new LeftMenu_page(driver).GotoSmartTaskPage();
		
		new SmartTask_page(driver).SearchSmartTask_func( search_str);
		
	}//end void
	
	
	@Step("Admin Creates Smart Task")
	public void Create_Smart_Task(SmartTask_Info task)
	{
		new SmartTask_page(driver).CreateSmartTask_func( task,"Submit");
		
	} //end void
	
	
	@Step("Admin Update Smart Task General Info")
	public void Update_Smart_Task_General_Info(String old_SMName, SmartTask_Info new_Smarttask)
	{
		//NOTE: This method only update Smart Task General Info but no Action updating:
			
		new SmartTask_page(driver).GotoSmartTaskDetails_func(old_SMName);
		
		//No update Action Info
		new_Smarttask.action.Type = "";
						
		new SmartTask_page(driver).FillDetailsInfo_func(new_Smarttask, "Submit");
		
	} //end void
	
	
	@Step("Admin Add Smart Task Action then Save It")
	public void Add_Smart_Task_Action_Then_Save_It(String SM_name, SmartTask_Action SM_action)
	{
		//NOTE: This method only add new action into existing Smart Task, no updating for General Info
		new SmartTask_page(driver).GotoSmartTaskDetails_func(SM_name);
									
		Add_New_Action_to_Smart_Task_Edit_Mode(SM_action);
		
		new SmartTask_page(driver).Dtail_Submit_btn().click();
		
		notification = new Messages_Notification(driver);
		
		notification.VerifyNotifyMsgNotDisplay_func(notification.SystemError_msg, "When adding new SmartTask",false);
		
		
	} //end void
	
	
	
	@Step("Admin Add New Action to Smart Task on Edit Mode")
	public void Add_New_Action_to_Smart_Task_Edit_Mode(SmartTask_Action SM_action)
	{
		new SmartTask_page(driver).AddAction_func(SM_action,"Submit");
		
	} //end void
	
	
	
	
	
	//VERIFYCATION STEPS
	
	@Step("Verify Smart Task Info: General Info + Action Info is Correct")
	public void Verify_Smart_Task_Info_is_Correct(SmartTask_Info Smarttask)
	{
		new SmartTask_page(driver).VerifyDetailsInfo(Smarttask);
	}//end void



	
	
	
}
