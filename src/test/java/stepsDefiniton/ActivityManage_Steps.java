package stepsDefiniton;

import org.junit.Assert;

import baseClasses.StepsBase;
import businessObjects.Activity_Info;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Step;
import pages.Activities_tab;
import pages.ActivityDetails_mdal;


public class ActivityManage_Steps extends StepsBase{
	Activities_tab Activity_tab;
	ActivityDetails_mdal activtymodal;

	

	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		Activity_tab = new Activities_tab(driver);
		activtymodal = new ActivityDetails_mdal(driver);
	}
	
	@Step("When user create new Activity Info")
	public void Create_New_Activity_From_Activity_Tab(Activity_Info activity)
	{
		Activity_tab.CreateActivity_func(activity);
	}//end void

		
	@Step("When User Search For Activity Info")
	public void Search_For_Activity_On_Activity_Tab(Activity_Info activity)
	{
		Activity_tab.searchType("All");
		
	}//end void
	
	@Step("When User delete Activity Info")
	public void Delete_Activity(Activity_Info activity)
	{
		boolean is_exist = Activity_tab.IsItemExistTable(Activity_tab.ActivityList_tbl(), activity.Title);
		
		if(is_exist==false)
		{
			TestConfigs.glb_TCFailedMessage += "Activity["+activity.Title+"] Not FOUND.\n";
			
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
		
		Activity_tab.DeleteActivity_func(activity.Title);
	}//end void
	
	@Step("When admin create new Activity Via Heading Shortcut")
	public void Create_Activity_Via_Heading_Shortcut(Activity_Info activity)
	{
		Activity_tab.Create_Activity_Via_ShortCut(activity);
	}//end void
	
	@Step("When admin updates Activity Info")
	public void Update_Activity(String oldtitle, Activity_Info new_activity)
	{
		
		boolean is_exist = Activity_tab.IsItemExistTable(Activity_tab.ActivityList_tbl(), oldtitle);
		
		if(is_exist==false)
		{
			TestConfigs.glb_TCFailedMessage += "Activity["+oldtitle+"] Not FOUND.\n";
			
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
		
		Activity_tab.EditActivity_func(oldtitle, new_activity);
	
	}//end void
	

	
	
	//===================================================================VERIFYCATION STEPS
	@Step("Verify User Can't Create Activity")
	public void Verify_User_Cant_Create_Activity()
	{
		Activity_tab.Optimize_ElementClick(Activity_tab.activity_tab());
		
		String fieldname_str = "CreateNew_btn";
		
		Activity_tab.VerifyFieldNotDisplayed_func(fieldname_str, Activity_tab.CreateNew_btn(), "Due to Deny Permission");
		
	}
	
	
	@Step("Verify Activity is shown on the table")
	public void Verify_Activity_Is_Shown_On_Table(Activity_Info activity)
	{
		Activity_tab.VerifyActivityExistTable_func(activity);
	}//end void
	
	@Step("Verify Activity is NOT shown on the table")
	public void Verify_Activity_Is_NOT_Shown_On_Table(Activity_Info activity,String note)
	{
	
		Activity_tab.Verify_Activity_Not_Exist_Table_func(activity,note);
		
	}//end void
	
	@Step("Verify Activity Detasil Is Correct On ViewMode")
	public void Verify_Activity_Detasil_Is_Correct_ViewMode(Activity_Info activity)
	{
		activtymodal.VerifyModalDetailsViewMode_func(activity);
	}//end void
	
	
	@Step("Verify Activity Details Can NOT Be Editable")
	public void Verify_Activity_Details_Can_NOT_Be_Editable(Activity_Info activity, String note)
	{
		activtymodal.OpenDetailsModal_func(activity);
		
	//	Activity_tab.Wait_For_ElementDisplay(activtymodal.Edit_btn());
		
		if(activtymodal.Edit_btn()!=null)
		{
			Activity_tab.Optimize_ElementClick(activtymodal.Edit_btn());
		}
			
		
		Activity_Info new_activity = new Activity_Info();
		
		activtymodal.Verify_Activity_Modal_Not_Editable(new_activity, note);
		
	}//end void

	


	//END  MANAGE====================================
	
	
	
	
}
