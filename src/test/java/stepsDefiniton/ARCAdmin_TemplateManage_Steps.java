package stepsDefiniton;


import baseClasses.StepsBase;
import businessObjects.Template_Info;
import net.thucydides.core.annotations.Step;
import pages.LeftMenu_page;
import pages.TemplateDetails_page;
import pages.Template_page;

public class ARCAdmin_TemplateManage_Steps extends StepsBase{
	LeftMenu_page LeftMenuPage;
	Template_page TemplatePage;
	TemplateDetails_page TemplateDetailsPage;
	
	
	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		LeftMenuPage = new LeftMenu_page(driver);
		TemplatePage = new Template_page(driver);
		TemplateDetailsPage = new TemplateDetails_page(driver);
	}
		
	//1. TEMPLATE MANAGE====================================
	@Step("Admin Searches for Existing Template")
	public void Search_For_Existing_Template(String SearchType, Template_Info template)
	{
			
		TemplatePage.SearchTemplate_func(SearchType, template);
	}
	
	@Step("Admin Creates New Template")
	public void Create_New_Template(Template_Info template)
	{
	
		LeftMenuPage.GotoTemplatePage();
		
		TemplatePage.CreateTemplate_func(template, "Submit");
	}
	
		
	@Step("Admin Remove Existing Template")
	public void Remove_Existing_Template(Template_Info template)
	{
		TemplatePage.RemoveTemplate_func(template, "Submit");
	}
	
	@Step("Admin Update Existing Template")
	public void Update_Existing_Template(String oldtemplname, Template_Info newTemplate)
	{
		LeftMenuPage.GotoTemplatePage();
		
		Template_Info template_temp = new Template_Info();
				
		template_temp.Name = oldtemplname;
		
		Search_For_Existing_Template("Type", template_temp);
		
		TemplatePage.UpdateTemplate_func(oldtemplname, newTemplate,"Submit");
	}
	
	
	//VERIFYCATION STEPS
	@Step("Verify Template is not displayed on Template List")
	public void Verify_Template_Not_Displayed_On_List(Template_Info tpl)
	{
		TemplatePage.VeifyTemplateNotExistTable_func(tpl);
	}
	
	@Step("Verify Template is displayed on Template List")
	public void Verify_Template_Displayed_On_List(Template_Info tpl,String note)
	{
		TemplatePage.VerifyTemplateExistTable_func(tpl,note);
	}
	
	@Step("Verify Template Details Is Correct")
	public void Verify_Template_Details_Is_Correct(Template_Info tpl)
	{
		
		TemplatePage.GotoTemplateDetailsPage_func(tpl.Name);
		
		TemplateDetailsPage.VerifyDetails_func(tpl);
	}

	
	
	
		
}
