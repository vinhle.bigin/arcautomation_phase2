package stepsDefiniton;

import baseClasses.StepsBase;
import businessObjects.Mail_Info;
import businessObjects.Vendor_Info;
import net.thucydides.core.annotations.Step;
import pages.Vendors_page;
import pages.LeftMenu_page;
import pages.Shortcuts_menu;
import pages.Vendor_GenInfo_tab;
import pages.Vendor_SearchOption_tab;

public class ARCAdmin_VendorManage_Steps extends StepsBase{
	
	
	LeftMenu_page LeftMenu_page;
	Vendors_page Vendors_page;
	Vendor_SearchOption_tab Vendor_SearchOption_tab;
	Vendor_GenInfo_tab Vendor_GenInfo_tab;
	Shortcuts_menu Shortcuts_menu;

	
	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		LeftMenu_page = new LeftMenu_page(driver);
		Vendors_page = new Vendors_page(driver);
		Vendor_SearchOption_tab = new Vendor_SearchOption_tab(driver);
		Vendor_GenInfo_tab = new Vendor_GenInfo_tab(driver);
		Shortcuts_menu = new Shortcuts_menu(driver);
		
	}



	
	//===========================END Vendor MANAGE
	
	
	//3. Vendor MANAGE
	
	//	
	@Step("Admin Search For Existing Vendor with VendorName")
	public void Search_For_Existing_VendorName(Vendor_Info vendor_info)
	{
		LeftMenu_page.GotoVendorPage();
		
		Vendors_page.Search_For_VendorName_func(vendor_info);
	}
	
	@Step("Admin Create New Vendor Via Top Shortcut")
	public void Create_New_Vendor_Via_Top_Shortcut(Vendor_Info info)
	{
		LeftMenu_page.GotoVendorPage();
		
		Vendors_page.CreateVendor_Via_Top_Shortcut_func(info, "submit");
	}
	
	@Step("Admin Create New Vendor Via New Vendor Button")
	public void Create_New_Vendor_Via_New_Vendor_Button(Vendor_Info info)
	{
		LeftMenu_page.GotoVendorPage();
		
		Vendors_page.Create_Vendor_Via_New_Button_func(info, "submit");
		
		
	}
	
	
	@Step("Admin Updates Info For Existing Vendor")
	public void Update_Info_For_Existing_Vendor(String oldname, Vendor_Info info)
	{
		
		
		if(Vendor_GenInfo_tab.Heading_GenralInfo_tab()==null)
		
			LeftMenu_page.gotoVendorDetailsPage(oldname);
		
		Vendor_GenInfo_tab.Update_Existing_Vendor_Info_func(info, "submit");
	}
	
	@Step("Admin Update Vendor Search Option Tab")
	public void Update_Vendor_Search_Option_Tab(Vendor_Info vendor)
	{
		LeftMenu_page.gotoVendorDetailsPage(vendor.VendorName);
		
		Vendor_SearchOption_tab.UpdateSearchOption_fun(vendor, "submit");
		
		
	}
	
	
	
	
	
	@Step("When #actor Send Email To Vendor")
	public void Send_Email_to_Vendor(Mail_Info mail_info)
	{
		Shortcuts_menu.Send_Email_With_Template_Content_func(mail_info,"Submit");
	}

	
	//===========VERIFICATION METHODS
	
	@Step("Verify Vendor is displayed on Vendor List")
	public void Verify_Vendor_Is_Displayed_On_Vendors_List(Vendor_Info vendor)
	{
		Vendors_page.Verify_Vendor_Exist_Table_func(vendor);
	}
	
	@Step("Verify Vendor General Info Is Correct on ViewMode")
	public void Verify_Vendor_General_Info_Is_Correct_On_ViewMode(Vendor_Info vendor)
	{
		if(Vendor_GenInfo_tab.vendorName_txt() == null) {
			LeftMenu_page.gotoVendorDetailsPage(vendor.VendorName);
		}

		Vendor_GenInfo_tab.Verify_Vendor_Info_Is_Correct_ViewMode(vendor);
	}

	public void Update_Status_For_Existing_Vendor(Vendor_Info vendor_info) {
		Vendors_page.Update_Status_Vendor(vendor_info);
		
	}


	
	
}