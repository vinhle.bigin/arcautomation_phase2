package stepsDefiniton;

import baseClasses.StepsBase;
import businessObjects.Address_Info;
import businessObjects.Client_Info;
import businessObjects.Contact_Info;
import businessObjects.Mail_Info;
import businessObjects.Policy_Info;
import net.thucydides.core.annotations.Step;
import pages.ClientAddr_tab;
import pages.Client_GenInfo_page;
import pages.Clients_page;
import pages.LeftMenu_page;
import pages.Shortcuts_menu;
import pages.Policy_page;


public class ARCAdmin_ClientManage_Steps extends StepsBase{
	
	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		
	}
	

	
		//==========================2. CLIENT MANAGE
	@Step("Admin Search For Existing Client")
	public void Search_For_Existing_Client(Client_Info client)
	{
		new Clients_page(driver).SearchClient_func(client);
	}
	
	@Step("Admin Create New Client")
	public void Create_New_Client(Client_Info info)
	{
		new LeftMenu_page(driver).GotoClientPage();
		
		new Clients_page(driver).CreateClient_func(info,"Submit");
	}
	
	@Step("When admin creates new client via Top Shortcut")
	public void Create_new_client_via_Top_Shortcut(Client_Info client)
	{
		new LeftMenu_page(driver).GotoClientPage();
		
		new Clients_page(driver).CreateClient_via_Shortcut_func(client, "submit");
		
		

	}//end void
	
	@Step("Admin Update Client General Info")
	public void Update_Client_Company_Info(Client_Info client_info)
	{
		new Clients_page(driver).Update_ClientCompanyProfile(client_info);
	}//end void
	
	public void Update_Client_Company_Status(Client_Info client_info) {
		new Clients_page(driver).Update_ClientCompanyStatus(client_info);
	}//end void
	
	@Step("Admin Update Client Address")
	public void Update_Client_Address_Info(String old_code,Address_Info newaddress)
	{
		new ClientAddr_tab(driver).UpdateClientAddress_func(old_code,newaddress,"Submit");
	}
	
	@Step("Admin Create Client Address")
	public void Create_Client_Address_Info(Address_Info address)
	{
		new ClientAddr_tab(driver).CreateClientAddress_func(address,"Submit");
	}
	
	@Step("Admin Remove Client Address")
	public void Remove_Client_Address_Info(Address_Info address)
	{
		new ClientAddr_tab(driver).RemoveClientAddress_func(address,"Submit");
	}
	
	@Step("Admin Edit Client Preference Info NoApprove")
	public void Edit_Client_Preference_Info_NoApprove(Client_Info client, Contact_Info ctact_info)
	{
		//new LeftMenu_page(driver).GotoClientPage();
		
		new Clients_page(driver).ChangePreferences_NoApprove_func(client, ctact_info);
	}
	
	@Step("Admin Edit Client Preference Info AnyContact")
	public void Edit_Client_Preference_Info_AnyContact(Client_Info client)
	{
		new LeftMenu_page(driver).GotoClientPage();
		
		new Clients_page(driver).ChangePreferences_AnyContact_func(client);
	}
	
	@Step("Admin Edit Client Preference Info Oneofselected")
	public void Edit_Client_Preference_Info_Oneofselected(Client_Info client)
	{
		new LeftMenu_page(driver).GotoClientPage();
		
		new Clients_page(driver).ChangePreferences_Oneofselected(client);
	}
	
	@Step("Admin Edit Client Preference Info Entering")
	public void Edit_Client_Preference_Info_Entering(Client_Info client)
	{
		new LeftMenu_page(driver).GotoClientPage();
		
		new Clients_page(driver).ChangePreferences_Entering_func(client);
	}
	

	
	@Step("Admin Update Client Custom Field Tab")
	public void Update_Client_CustomField_Info()
	{
		
	}
	
	@Step("When #actor send email to client")
	public void Send_Email_to_client(Mail_Info mail_info)
	{
		new Shortcuts_menu(driver).Send_Email_With_Template_Content_func(mail_info,"Submit");
	}

 //==========================2. POLICY MANAGE
    
    @Step("Admin Create New Policy")
    public void Create_New_Policy(Policy_Info p_info)
    {
        new LeftMenu_page(driver).GotoPoliciesPage();
        
        new Policy_page(driver).CreateRelocationPolicy_func("Create", p_info, "submit");
        
    }//end void
    
    @Step("When Admin Search For Policy With Title")
    public void Search_For_Policy_With_Title(String policy_name) {
        
        new Policy_page(driver).SearchPolicy_func(policy_name);
        
    }//end void
    
    
    @Step("Verify Modal Policy Info Is Correct")
    public void Verify_Modal_Policy_Info_Is_Correct(Policy_Info p_info)
    {
        new Policy_page(driver).VerifyModalPolicyInfoViewMode_func(p_info);
        
    }//end void
    
    @Step("Verify Policy is displayed on Policies List")
    public void Verify_Policy_Is_Displayed_On_Policies_List(Policy_Info policy)
    {
        new Policy_page(driver).VerifyPolicyExistTable_func(policy);
    }
    
    @Step("Verify Policy is updated ")
    public void Update_Policy_Info(Policy_Info policy)
    {
        new Policy_page(driver).UpdateThePolicy(policy,"submit");
    }

	
	//=============================================================
	//===========================VERIFICATION METHODS
	@Step("Verify Client is displayed on Clients List")
	public void Verify_Client_Is_Displayed_On_Clients_List(Client_Info client)
	{
		new Clients_page(driver).VerifyClientExistTable_func(client);
	}
	
	@Step("Verify Client General Info Is Correct on ViewMode")
	public void Verify_Client_General_Info_Is_Correct_On_ViewMode(Client_Info client)
	{
		Client_GenInfo_page clientGenInfo_tab = new Client_GenInfo_page(driver);
		
		if(clientGenInfo_tab.Generall_tab() == null) {
			new LeftMenu_page(driver).GotoClientDetailsPage(client.CompanyName);
		}

		clientGenInfo_tab.VerifyGeneralInfoViewMode_func(client);
	}
	
	@Step("Go to Client Details page")
	public void Go_To_Client_Details_Page(String fullname)
	{
		new LeftMenu_page(driver).GotoClientDetailsPage(fullname);
	}
	
	@Step("Verify Client Address Info Is Correct on ViewMode")
	public void Verify_Client_Address_Info_Is_Correct_On_ViewMode(Address_Info addr)
	{
				
		new ClientAddr_tab(driver).VerifyInfoViewMode_func( addr);
	}
	
	@Step("Verify Client Address Info Is Shown on the Address Table")
	public void Verify_Client_Address_Info_Shown_On_Table(Address_Info addr)
	{
		new ClientAddr_tab(driver).VerifyDataExistTable_func(new ClientAddr_tab(driver).AddressList_tbl(), addr.Code, "");	
		
		
	}
		
	@Step("Verify Address Is Auto Generated After Client Is Created")
	public void Verify_Address_Auto_Generated_After_Client_Created()
	{
		new ClientAddr_tab(driver).Verify_Address_Auto_Generated_After_Client_Created();
	}

	
}
