package stepsDefiniton;

import baseClasses.StepsBase;
import businessObjects.Document_Info;
import custom_Func.FileManage;
import net.thucydides.core.annotations.Step;
import pages.Document_tab;


public class DocumentManage_Steps extends StepsBase{
	
	Document_tab Document_tab;
	
	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		Document_tab = new Document_tab(driver);
		
	}
		
	@Step("When user creates New Document from Document Tab")
	public void Create_New_Document_from_Doc_Tab(Document_Info doc_Info)
	{
		Document_tab.CreateDoc_func(doc_Info);
	}//end void
	
	@Step("When admin updates Existing Document Info")
	public void Update_Existing_Document(Document_Info doc_Info)
	{
		Document_tab.EditDoc_func(doc_Info);
	}//end void

	@Step("When Admin Search For Document With Title")
	public void Search_For_Document_With_Title(String file_name)
	{
		Document_tab.SearchDoc_func(file_name);
	}//end void
	
	@Step("When Admin Remove Document")
	public void Remove_Document(String file_name)
	{
		Document_tab.DeleteDoc_func(file_name);
	}//end void
	
	@Step("When Download  Document")
	public void Download_Document(String file_name,String destination_file_name)
	{
	
		try {
			Document_tab.Optimize_ElementClick(Document_tab.DownLoad_icon(file_name));
			
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//No need to use method download file function due to the config browser profile is already initially
		//FileManage.DownloadFile_func(Document_tab.DownLoad_icon(file_name), destination_file_name);	
				
	}//end void
	
	
	
	//===================================================================VERIFYCATION STEPS

	@Step("Verify User Can't Create Document")
	public void Verify_User_Cant_Create_Document()
	{
		Document_tab.Optimize_ElementClick(Document_tab.Navigate_Tab());
		
		String fieldname_str = "CreateNew_btn";
		
		Document_tab.VerifyFieldNotDisplayed_func(fieldname_str, Document_tab.CreateNew_btn(), "Due to Deny Permission");
		
	}
	
	
	@Step("Verify Document Is Shown On the List")
	public void Verify_Document_Info_Is_Shown_On_List(Document_Info document)
	{
		Document_tab.VerifyDocExistTable_func(document);
		
	}//end void
	
	@Step("Verify Document Is NOT Shown On the List")
	public void Verify_Document_Info_NOT_Shown_On_List(Document_Info document)
	{
		Document_tab.VerifyDocNotExistTable_func(document);
		
	}//end void
	
	@Step("Verify Modal Document Info Is Correct")
	public void Verify_Modal_Document_Info_Is_Correct(Document_Info document)
	{
		Document_tab.VerifyModalInfoViewMode_func(document);
		
	}//end void
	
	@Step("Verify Downloaded File Content is Correct")
	public void Verify_Downloaded_File_Content_Is_Correct(String file_name, String expected_content)
	{
		FileManage download_file = new FileManage();
		download_file.VerifyFileContent_func(file_name, expected_content);
		
	}//end void
	
	
	
	@Step("Verify Document Details Can NOT Be Editable")
	public void Verify_Document_Details_Can_NOT_Be_Editable(Document_Info Document, String note)
	{
		Document_tab.Edit_icon(Document.file_name).click();
		
		//PageObjects.Wait_For_ElementDisplay(Document_tab.UpdateDescriptionDoc_txt());
		
		if(Document_tab.Modal_Edit_btn()!=null)
			Document_tab.Modal_Edit_btn().click();
		
		
		Document_tab.Verify_Document_Modal_Not_Editable(note);
		
	}//end void

	

	//END  MANAGE====================================
	
	
	
	
}
