package stepsDefiniton;

import baseClasses.StepsBase;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Step;
import pages.login_page;

public class ARCAdmin_UserRoleManage_Steps extends StepsBase{
	login_page login_page;


	
	
	//1. TRANSFEREE MANAGE====================================
	@Step("Given Admin go to ARC Portal-Transferee page")
	public void Login_to_ARC_Portal_as_admin()
	{
		String url = glb_RefData.ARCPortal;
		String username = glb_RefData.glb_ARC_username;
		String pass = glb_RefData.glb_ARC_pass;
		login_page.Login_func(url,username,pass);
		
	}



	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		login_page = new login_page(driver);
	}
	
	
	
	
}
