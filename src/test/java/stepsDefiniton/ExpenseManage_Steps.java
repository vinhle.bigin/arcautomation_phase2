package stepsDefiniton;

import org.junit.Assert;

import baseClasses.StepsBase;
import configuration.TestConfigs;
import net.thucydides.core.annotations.Step;
import businessObjects.Activity_Info;
import businessObjects.BankAccount_Info;
import businessObjects.ReimBurs_Info;
import businessObjects.SubExpense_Info;
import pages.LeftMenu_page;
import pages.SmartTask_page;
import pages.expenseFeature.ExpenseReim_modal;
import pages.expenseFeature.ExpenseReim_page;

public class ExpenseManage_Steps extends StepsBase{
	LeftMenu_page LeftMenuPage;
	ExpenseReim_page ExpReimPage;
	ExpenseReim_modal ExpReimmdal;
	
	@Override
	public void InitStepsDefinition() {
		// TODO Auto-generated method stub
		LeftMenuPage = new LeftMenu_page(driver);
		ExpReimPage = new ExpenseReim_page(driver);
		ExpReimmdal = new ExpenseReim_modal(driver);
	}
	
	
//===========================
	
	@Step("Transf Creates New Expense Reimbursement")
	public void Create_New_ExpReim(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment) 
	{
		LeftMenuPage.GotoExpReimPage();
		
		ExpReimPage.CreateExpReim_func(reim, subexp, payment, "Submit");
	}// end void
	
	@Step("New Expense Reimbursement is saved draft")
	public void New_ExpReim_is_Saved_Draft(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment) 
	{
		LeftMenuPage.GotoExpReimPage();
		
		ExpReimPage.CreateExpReim_func(reim, subexp, payment, "SaveDraft");
	}// end void

	@Step("Transf Searches Expense Reimbursement")
	public void Search_ExpReim(String ReportName)
	{
		LeftMenuPage.GotoExpReimPage();
		
		ExpReimPage.SearchExpReim_func(ReportName);
	}//end void

	@Step("Transf updates Expense Reimbursement")
	public void Update_ExpReim(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment)
	{
		boolean is_exist = ExpReimPage.IsItemExistTable(ExpReimPage.ExpenseReim_tbl(), reim.Name);
		
		if(is_exist==false)
		{
			TestConfigs.glb_TCFailedMessage += "Expense Reimbursement ["+reim.Name+"] Not FOUND.\n";
			
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
		
		ExpReimPage.EditExpReim_func(reim, subexp, payment, "Submit");
	
	}//end void

	@Step("When Transf deletes Expense Reimbursement")
	public void Delete_ExpReim(ReimBurs_Info reim)
	{
		boolean is_exist = ExpReimPage.IsItemExistTable(ExpReimPage.ExpenseReim_tbl(), reim.Name);
		
		if(is_exist==false)
		{
			TestConfigs.glb_TCFailedMessage += "Expense Reimbursement ["+reim.Name+"] Not FOUND.\n";
			
			Assert.fail(TestConfigs.glb_TCFailedMessage);
		}
		
		ExpReimPage.DeleteExpReim_func(reim.Name);
	}//end void

//====================VERIFY
	@Step("Transf Verifies Expense Reimbursement Exist Table")
	public void Verify_ExpReimExistTable(ReimBurs_Info reim) 
	{
		ExpReimPage.VerifyExpReimExistTable_func(reim);
	}// end void
	
	@Step("Transf Verifies Expense Reimbursement Detail")
	public void Verify_ExpReimDetail(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment) 
	{
		ExpReimPage.VerifyExpReimDetail(reim, subexp, payment);
	}// end void
	
	@Step("Verify Expense Reimbursement Details Can NOT Be Editable")
	public void Verify_ExpReim_Details_Can_NOT_Be_Editable(ReimBurs_Info reim, SubExpense_Info subexp, BankAccount_Info payment)
	{
		ExpReimPage.GotoExpReimDetail();
		
		if(ExpReimmdal.EditExpReim_btn()!=null)
		{
			ExpReimmdal.Optimize_ElementClick(ExpReimmdal.EditExpReim_btn());
		}
		
		ExpReimPage.Verify_ExpReim_Modal_Not_Editable(reim, subexp, payment);
		
	}//end void
	
	@Step("Verify Expense Reimbursement is NOT shown on the table")
	public void Verify_ExpReim_Is_NOT_Shown_On_Table(ReimBurs_Info reim,String note)
	{
	
		ExpReimPage.Verify_ExpReim_Not_Exist_Table_func(reim, note);
		
	}//end void
	
}
