package stepsDefiniton;

import businessObjects.Contact_Info;
import dataDriven.glb_RefData;
import net.thucydides.core.annotations.Step;
import pages.Vendors_page;
import pages.login_page;

public class VendorUser_Steps extends User_AllSteps{
	public static String actor = "Vendor User";
	
	
	public VendorUser_Steps() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void InitStepsDefinition() {
		
		super.InitStepsDefinition();
		
	}
	
	@Step("Vendor Login Vendor Portal page")
	public void Login_to_VendorPortal(String username, String password)
	{
		String url = glb_RefData.VendorPortal;
		
	
		
		new login_page(driver).Login_func(url,username,password);
		
	}
	
	@Step("Register Contact Email On Vendor Portal")
	public void Register_ContactEmail_VendorPortal(Contact_Info ctact_info)
	{
		new Vendors_page(driver).Register_ContactEnail_func(ctact_info);
	}
}
