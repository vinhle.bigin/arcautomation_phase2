# syntax=docker/dockerfile:1

FROM maven:3.8.2-jdk-11

USER root
WORKDIR /AutomationProject

COPY . ./

RUN mvn -f ./pom.xml dependency:go-offline

RUN mvn -f ./pom.xml compile

#this is for keepting container always running: CMD tail -f /dev/null
